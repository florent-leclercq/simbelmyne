///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- include/read_param.h
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file read_param.h
 *  \brief Header for read_param.c
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2013-2023
 */
#ifndef header_read_param
#define header_read_param

    // libSBMY
    #include "config.h"
    #include "utils.h"

    #define DOUBLE 1
    #define STRING 2
    #define INT 3
    #define BOOL 1
    #define MAXTAGS 300
    #define TAG 50

    // Setup the hashtable
    struct __nlist               // an entry of the hashtable
    {
        struct __nlist *next;    // next entry in chain
        char *tag;               // the entry tag
        bool usedtag, mandatorytag;
        int id;
        void *addr;
    };

    #define HASHSIZE 20029       // a prime number, at least 1.3 times the number of expected tags

    Param_File read_parameterfile(char *fname);

#endif
