///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- include/pmcola.h
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file pmcola.h
 *  \brief Header for pmcola.c
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2014-2023
 */
#ifndef header_cola
#define header_cola

    // libSBMY
    #include "density.h"
    #include "gravpot.h"
    #include "snapshot.h"
    #include "structures.h"
    #include "utils.h"

    // specific
    #include "lpt.h"

    #define initializePMCOLATimers(PMCOLATimers) initializePMCOLATimers_(PMCOLATimers, __POS__)
    void initializePMCOLATimers_(double PMCOLATimers[12], char *file, char *func, int line);
    #define printPMCOLATimers(PMCOLATimers, type) printPMCOLATimers_(PMCOLATimers, type, __POS__)
    void printPMCOLATimers_(double PMCOLATimers[12], char *type, char *file, char *func, int line);
    #define updateBoxTimers(TimestepTimers, BoxTimers) updateBoxTimers_(TimestepTimers, BoxTimers, __POS__)

    void initialize_one_box(const particleID_t Np, particle_pos_t x[Np], particle_vel_t p[Np], const Snapshot *P, const int EvolutionMode, const double ai, const double af, const double cosmo[N_COSMOPAR]);
    void finalize_one_box(const particleID_t Np, particle_pos_t x[Np], particle_vel_t p[Np], Snapshot *P,  const double af, const double cosmo[N_COSMOPAR]);

    void get_timestepping(const int nsteps, const int TimeStepDistribution, const double ai, const double af, const bool WriteSnapshots, const bool WriteDensities, int *Nkick, int *Ndrift, double **aiKickArray, double **afKickArray, double **aDriftArray, double **aiDriftArray, double **afDriftArray, double **aKickArray, double **aKickOutput, double **aDriftOutput);
    #define free_timestepping(aiKickArray, afKickArray, aDriftArray, aiDriftArray, afDriftArray, aKickArray, aKickOutput, aDriftOutput) free_timestepping_(aiKickArray, afKickArray, aDriftArray, aiDriftArray, afDriftArray, aKickArray, aKickOutput, aDriftOutput, __POS__)
    void free_timestepping_(double *aiKickArray, double *afKickArray, double *aDriftArray, double *aiDriftArray, double *afDriftArray, double *aKickArray, double *aKickOutput, double *aDriftOutput, char *file, char *func, int line);
    void evolve_one_box(Snapshot *P, Field FCs_field, float_t *density_and_Phi, const particleID_t Np, particle_pos_t x[Np], particle_vel_t p[Np], const particle_lpt_t PsiLPT[Np], const int EvolutionMode, const int box, const int nboxes, double BoxTimers[12], const int nsteps, const double ai, const double af, const int Ndrift, const int Nkick, const double aiKick[Nkick], const double afKick[Nkick], const double aDrift[Nkick], const double aiDrift[Ndrift], const double afDrift[Ndrift], const double aKick[Ndrift], const double aKickOutput[nsteps], const double aDriftOutput[nsteps], const bool ModifiedDiscretization, const double n_LPT, const bool WriteSnapshots, const char *OutputSnapshotsBase, const char *OutputSnapshotsExt, const int NumFilesPerSnapshot, const int SnapFormat, const bool WriteDensities, const char *OutputDensitiesBase, const char *OutputDensitiesExt, const bool WriteReferenceFrame, const char *OutputKickBase, const char *OutputKickExt, const char *OutputDriftBase, const char *OutputDriftExt, const bool ReadReferenceFrame, const char *InputKickBase, const char *InputKickExt, const char *InputDriftBase, const char *InputDriftExt, const int Np0, const int Np1, const int Np2, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, const int boundary_conditions, const int Npm0_pad, const int Npm1_pad, const int Npm2_pad, float_t **phiBCs, const double cosmo[N_COSMOPAR]);

    void evolve_pm_cola(Snapshot *P, Field FCs_field, const particleID_t Np, particle_lpt_t PsiLPT[Np], const int EvolutionMode, const int nsteps, const int TimeStepDistribution, const double RedshiftFCs, const bool ModifiedDiscretization, const double n_LPT, const bool WriteSnapshots, const char *OutputSnapshotsBase, const char *OutputSnapshotsExt, const int NumFilesPerSnapshot, const int SnapFormat, const bool WriteDensities, const char *OutputDensitiesBase, const char *OutputDensitiesExt, const bool WriteReferenceFrame, const char *OutputKickBase, const char *OutputKickExt, const char *OutputDriftBase, const char *OutputDriftExt, const bool ReadReferenceFrame, const char *InputKickBase, const char *InputKickExt, const char *InputDriftBase, const char *InputDriftExt, const int Np0, const int Np1, const int Np2, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, const double cosmo[N_COSMOPAR]);

#endif
