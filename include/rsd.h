///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- include/rsd.h
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file rsd.h
 *  \brief Header for rsd.c
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2016-2023
 */
#ifndef header_rsd
#define header_rsd

    // libSBMY
    #include "structures.h"
    #include "snapshot.h"
    #include "utils.h"

    void update_snapshot_RSD(Snapshot *P, const particleID_t Np, const particle_lpt_t PsiLPT[Np], const double v_observer[3], const bool DoNonLinearMapping, const double L0, const double L1, const double L2, const double corner0, const double corner1, const double corner2, const double alpha1RSD, const bool doLPTSplit, const double alpha2RSD, const double cosmo[N_COSMOPAR]);

#endif
