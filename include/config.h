///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- include/config.h
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file config.h
 *  \brief Header defining the configuration file
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2013-2023
 */
#ifndef header_config
#define header_config

    #include <stdbool.h>
    # define MAXSTRINGLENGTH 500
    // Structures
    struct __param_file
    {
/* ----------------------------------------------------------------------------------------------------------------------- */
/* Setup ----------------------------------------------------------------------------------------------------------------- */
/* ----------------------------------------------------------------------------------------------------------------------- */
        int SnapFormat, NumFilesPerSnapshot;

/* ----------------------------------------------------------------------------------------------------------------------- */
/* Module LPT ------------------------------------------------------------------------------------------------------------ */
/* ----------------------------------------------------------------------------------------------------------------------- */
        int ModuleLPT;
        char InputRngStateLPT[MAXSTRINGLENGTH], OutputRngStateLPT[MAXSTRINGLENGTH];

        int Particles, Mesh;
        double BoxSize, corner0, corner1, corner2;

        int ICsMode;
        int WriteICsRngState; char OutputICsRngState[MAXSTRINGLENGTH];
        int WriteWhiteNoise; char OutputWhiteNoise[MAXSTRINGLENGTH];
        char InputWhiteNoise[MAXSTRINGLENGTH];
        char InputInitialConditions[MAXSTRINGLENGTH];
        int WriteInitialConditions; char OutputInitialConditions[MAXSTRINGLENGTH];

        char InputPowerSpectrum[MAXSTRINGLENGTH];

        double RedshiftLPT;
        int WriteLPTSnapshot; char OutputLPTSnapshot[MAXSTRINGLENGTH];
        int WriteLPTDensity; char OutputLPTDensity[MAXSTRINGLENGTH];

/* ----------------------------------------------------------------------------------------------------------------------- */
/* Module PM/COLA -------------------------------------------------------------------------------------------------------- */
/* ----------------------------------------------------------------------------------------------------------------------- */
        int ModulePMCOLA;
        int EvolutionMode;
        int ParticleMesh, NumberOfTimeSteps, TimeStepDistribution, ModifiedDiscretization;
        double n_LPT;

        int WriteSnapshots; char OutputSnapshotsBase[MAXSTRINGLENGTH], OutputSnapshotsExt[MAXSTRINGLENGTH];
        int WriteDensities; char OutputDensitiesBase[MAXSTRINGLENGTH], OutputDensitiesExt[MAXSTRINGLENGTH];

        double RedshiftFCs;
        int WriteFinalSnapshot; char OutputFinalSnapshot[MAXSTRINGLENGTH];
        int WriteFinalDensity; char OutputFinalDensity[MAXSTRINGLENGTH];

        bool WriteReferenceFrame; char OutputKickBase[MAXSTRINGLENGTH]; char OutputKickExt[MAXSTRINGLENGTH]; char OutputDriftBase[MAXSTRINGLENGTH]; char OutputDriftExt[MAXSTRINGLENGTH];
        bool ReadReferenceFrame; char InputKickBase[MAXSTRINGLENGTH]; char InputKickExt[MAXSTRINGLENGTH]; char InputDriftBase[MAXSTRINGLENGTH]; char InputDriftExt[MAXSTRINGLENGTH];

        int NumberOfTilesPerDimension, NumberOfParticlesInBuffer;
        char OutputLPTPotential1[MAXSTRINGLENGTH], OutputLPTPotential2[MAXSTRINGLENGTH];
        char OutputTilesBase[MAXSTRINGLENGTH], OutputTilesExt[MAXSTRINGLENGTH];

/* ----------------------------------------------------------------------------------------------------------------------- */
/* Module RSD ------------------------------------------------------------------------------------------------------------ */
/* ----------------------------------------------------------------------------------------------------------------------- */
        int ModuleRSD, DoNonLinearMapping;
        double vobs0, vobs1, vobs2;
        double alpha1RSD; int DoLPTSplit; double alpha2RSD;

        int WriteRSSnapshot; char OutputRSSnapshot[MAXSTRINGLENGTH];
        int WriteRSDensity; char OutputRSDensity[MAXSTRINGLENGTH];

/* ----------------------------------------------------------------------------------------------------------------------- */
/* Module Mock Catalogs -------------------------------------------------------------------------------------------------- */
/* ----------------------------------------------------------------------------------------------------------------------- */
        int ModuleMocks;
        char InputRngStateMocks[MAXSTRINGLENGTH], OutputRngStateMocks[MAXSTRINGLENGTH];
        int WriteMocksRngState; char OutputMocksRngState[MAXSTRINGLENGTH];
        int NoiseModel, NumberOfNoiseRealizations;

        char InputDensityMocks[MAXSTRINGLENGTH], InputSurveyGeometry[MAXSTRINGLENGTH], InputSummaryStatskGrid[MAXSTRINGLENGTH];

        int WriteMocks; char OutputMockBase[MAXSTRINGLENGTH], OutputMockExt[MAXSTRINGLENGTH];
        int WriteSummaryStats; char OutputSummaryStats[MAXSTRINGLENGTH];

/* ----------------------------------------------------------------------------------------------------------------------- */
/* Cosmological model ---------------------------------------------------------------------------------------------------- */
/* ----------------------------------------------------------------------------------------------------------------------- */
        double h, Omega_r, Omega_q, Omega_b, Omega_m, Omega_k, n_s, sigma8, w0_fld, wa_fld;
    };
    typedef struct __param_file Param_File;

#endif
