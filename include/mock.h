///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- include/mock.h
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file mock.h
 *  \brief Header for mock.c
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2016-2023
 */
#ifndef header_mock
#define header_mock

    // libSBMY
    #include "correlations.h"
    #include "io.h"
    #include "random.h"
    #include "structures.h"
    #include "utils.h"

    void produce_mocks_and_summaries(const float_t *DELTA, const char *fname_SG, const char *fname_k_grid, const char *InputRngStateMocks, const int WriteMocksRngState, const char *OutputMocksRngState, const char *OutputRngStateMocks, const int NoiseModel, const int N_NOISE, const bool WriteMocks, const char *OutputMockBase, const char *OutputMockExt, const bool WriteSummaryStats, const char *OutputSummaryStats);

#endif
