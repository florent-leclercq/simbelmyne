///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- include/power.h
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file power.h
 *  \brief Header for power.c
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2014-2023
 */
#ifndef header_power
#define header_power

    // libSBMY
    #include "cosmology.h"
    #include "io.h"
    #include "structures.h"
    #include "utils.h"

    double fix_spectrum_amplitude(const double params[N_COSMOPAR+1], const int WhichSpectrum);
    double get_power(const double k, const double params[N_COSMOPAR+1], const int WhichSpectrum, const double Norm);
    void setup_Pk(const int Nk, const float_t k_modes[Nk], const double params[N_COSMOPAR+1], const int WhichSpectrum, float_t powerspectrum[Nk]);
    PowerSpectrum initialize_power_spectrum(const char *InputPowerSpectrum);

#endif
