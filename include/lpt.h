///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- include/lpt.h
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file lpt.h
 *  \brief Header for lpt.c
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2012-2023
 */
#ifndef header_lpt
#define header_lpt

    // libSBMY
    #include "grf.h"
    #include "io.h"
    #include "poisson_solvers.h"
    #include "utils.h"

    // specific
    #include "power.h"

    Field get_initial_conditions(const int ICsMode, const char *InputRngStateLPT, const int WriteICsRngState, const char *OutputICsRngState, const char *OutputRngStateLPT, const char *InputWhiteNoise, const int WriteWhiteNoise, const char *OutputWhiteNoise, const char *InputPowerSpectrum, const char *InputInitialConditions, const int WriteInitialConditions, const char *OutputInitialConditions, const int N0, const int N1, const int N2, const double corner0, const double corner1, const double corner2, const double L0, const double L1, const double L2);

    void get_potentials_periodic(const float_t *DELTA, float_t *phi1, float_t *phi2, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2);
    void get_potentials_zerobc(const float_t *DELTA, float_t *phi1, float_t *phi2, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2);

    void compute_LPT_displacements(const particleID_t Np, particle_lpt_t PsiLPT[Np], const int N0, const int N1, const int N2, const double L0, const double L1, const double L2, const int Np0, const int Np1, const int Np2, const int boundary_conditions, const int Nphi0_pad, const int Nphi1_pad, const int Nphi2_pad, float_t phi1_pad[(N0+2*Nphi0_pad)*(N1+2*Nphi1_pad)*(N2+2*Nphi2_pad)], float_t phi2_pad[(N0+2*Nphi0_pad)*(N1+2*Nphi1_pad)*(N2+2*Nphi2_pad)], const int Ng0_pad, const int Ng1_pad, const int Ng2_pad);
    void evolve_lpt(Snapshot *P, Field ICs_field, const particleID_t Np, particle_lpt_t PsiLPT[Np], const double RedshiftLPT, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2, const int Np0, const int Np1, const int Np2, const double cosmo[N_COSMOPAR]);

#endif
