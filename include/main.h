///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- include/main.h
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file main.h
 *  \brief Header for main.c
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2012-2023
 */
#ifndef header_main
#define header_main

    // libSBMY
    #include "utils.h"

    // specific
    #include "lpt.h"
    #include "mock.h"
    #include "pmcola.h"
    #include "read_param.h"
    #include "rsd.h"
    #include "scola.h"

    int main_sbmy(int argc, char **argv);
    int main_scola(int argc, char **argv);

#endif
