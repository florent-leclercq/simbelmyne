///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- include/scola.h
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file scola.h
 *  \brief Header for scola.c
 *  \author Florent Leclercq, Baptiste Faure
 *  \version 0.5.0
 *  \date 2017-2023
 */
#ifndef header_scola
#define header_scola

    // libSBMY
    #include "config.h"
    #include "io.h"
    #include "utils.h"

    // specific
    #include "lpt.h"
    #include "pmcola.h"

    void process_scola_box(const char *OutputLPTPotential1, const char *OutputLPTPotential2, const char *OutputTilesBase, const char *OutputTilesExt, const int THIS_TILE, const int N_TILES, const int Np_BUFFER, const int nsteps, const int TimeStepDistribution, const double RedshiftLPT, const double RedshiftFCs, const bool ModifiedDiscretization, const double n_LPT, const int N0, const int N1, const int N2, const int Np0, const int Np1, const int Np2, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, const double cosmo[N_COSMOPAR]);
    void evolve_scola(Snapshot *P, Field *ICs_field, Field *FCs_field, particle_lpt_t *PsiLPT, const char *OutputLPTPotential1, const char *OutputLPTPotential2, const char *OutputTilesBase, const char *OutputTilesExt, const int N_TILES, const int Np_BUFFER, const int nsteps, const int TimeStepDistribution, const double RedshiftLPT, const double RedshiftFCs, const bool ModifiedDiscretization, const double n_LPT, const int N0, const int N1, const int N2, const int Np0, const int Np1, const int Np2, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, const double corner0, const double corner1, const double corner2, const double cosmo[N_COSMOPAR]);

#endif
