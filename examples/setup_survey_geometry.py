#!/usr/bin/env python
# -*- coding: latin-1 -*-
#-------------------------------------------------------------------------------------
# Simbelmynë v0.5.0 -- examples/setup_survey_geometry.py
# Copyright (C) 2012-2023 Florent Leclercq.
#
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# The text of the license is located in the root directory of the source package.
#-------------------------------------------------------------------------------------

"""Simbelynë example: set up a mock survey
geometry
"""

__author__  = "Florent Leclercq"
__version__ = "0.5.0"
__date__    = "2016-2023"
__license__ = "GPLv3"

import numpy as np
from pysbmy.cosmology import redshift_from_comoving_distance
from pysbmy.survey_geometry import SurveyGeometry
from pysbmy.utils import PrintPythonCommand

PrintPythonCommand()

# basic setup
from example_setup import L0,L1,L2,corner0,corner1,corner2,N0,N1,N2,N_CAT,cosmo

# observational setup
bright_cut=np.array(((0.),))
faint_cut=np.array(((0.),))
rmin=np.array(((0.),))
rmax=np.array(((L0/2*np.sqrt(3)),))
zmin=redshift_from_comoving_distance(rmin, **cosmo)
zmax=redshift_from_comoving_distance(rmax, **cosmo)
N_BIAS=3
galaxy_bias_mean=np.array(((1.2,0.,0.),))
galaxy_bias_std=np.array(((0.,0.,0.),))
galaxy_nmean_mean=np.array(((3.),))
galaxy_nmean_std=np.array(((0.),))
galaxy_sel_window=np.ones((N_CAT,N0,N1,N2))

# setup coordinates
x=np.arange(corner0,corner0+L0,L0/N0)
x=np.tile(x,(N2,N1,1))+1/2.*L0/N0

y=np.arange(corner1,corner1+L1,L1/N1)
y=np.tile(y,(N2,N0,1))
y=np.swapaxes(y,1,2)+1/2.*L1/N1

z=np.arange(corner2,corner2+L2,L2/N2)
z=np.tile(z,(N1,N0,1))
z=np.swapaxes(z,0,2)+1/2.*L2/N2

r=np.sqrt(x**2+y**2+z**2)
dec=np.arcsin(z/r)*180/np.pi
ra=np.arctan2(y,x)*180/np.pi
ra[np.where(ra<0)]+=360.

# setup basic selection function
def aux_selection(r):
    return np.exp(-1./2.*(r-50.)*(r-50.)/15**2)
selection=np.vectorize(aux_selection)

# setup basic mask
mask=np.ones((N0,N1,N2))
mask[np.where(((ra<120)*(ra>60))|((ra>240)*(ra<300)))]=0.
mask[np.where((dec<-10)|(dec>20))]=0.

# setup galaxy selection window
galaxy_sel_window[0]*=selection(r)*mask
galaxy_sel_window[0]/=galaxy_sel_window[0].max()

S=SurveyGeometry(L0,L1,L2,corner0,corner1,corner2,N0,N1,N2,N_CAT,cosmo,bright_cut,faint_cut,rmin,rmax,zmin,zmax,N_BIAS,galaxy_bias_mean,galaxy_bias_std,galaxy_nmean_mean,galaxy_nmean_std,galaxy_sel_window)
S.write("input_survey_geometry.h5")
