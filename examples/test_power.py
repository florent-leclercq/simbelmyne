#!/usr/bin/env python
# -*- coding: latin-1 -*-
#-------------------------------------------------------------------------------------
# Simbelmynë v0.5.0 -- examples/test_power.py
# Copyright (C) 2012-2023 Florent Leclercq.
#
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# The text of the license is located in the root directory of the source package.
#-------------------------------------------------------------------------------------

"""Simbelynë example: plot power spectra
of output density fields
"""

__author__  = "Florent Leclercq"
__version__ = "0.5.0"
__date__    = "2016-2023"
__license__ = "GPLv3"

import numpy as np
import h5py as h5
import matplotlib.pyplot as plt
from scipy.interpolate import InterpolatedUnivariateSpline
from pysbmy.field import read_field
from pysbmy.fft import read_FourierGrid
from pysbmy.power import read_powerspectrum
from pysbmy.correlations import get_autocorrelation
from pysbmy.utils import PrintInfo, PrintMessage

PrintInfo("This is the Simbelmyne test_power script.")

# Read fields and get power spectra
from example_setup import L0,L1,L2,N0,N1,N2,cosmo
G=read_FourierGrid("input_ss_k_grid.h5")
k=G.k_modes[1:]
AliasingCorr=True

DELTA_LPT=read_field("lpt_density.h5")
Pk_LPT,Vk_LPT=get_autocorrelation(DELTA_LPT,G,AliasingCorr)
Pk_LPT,Vk_LPT=Pk_LPT[1:],Vk_LPT[1:]
Sk_LPT=np.sqrt(Vk_LPT)

DELTA_PM=read_field("final_density_pm.h5")
Pk_PM,Vk_PM=get_autocorrelation(DELTA_PM,G,AliasingCorr)
Pk_PM,Vk_PM=Pk_PM[1:],Vk_PM[1:]
Sk_PM=np.sqrt(Vk_PM)

DELTA_COLA=read_field("final_density_tcola.h5")
Pk_COLA,Vk_COLA=get_autocorrelation(DELTA_COLA,G,AliasingCorr)
Pk_COLA,Vk_COLA=Pk_COLA[1:],Vk_COLA[1:]
Sk_COLA=np.sqrt(Vk_COLA)

DELTA_RS=read_field("initial_density.h5")
Pk_RS,Vk_RS=get_autocorrelation(DELTA_RS,G,AliasingCorr)
Pk_RS,Vk_RS=Pk_RS[1:],Vk_RS[1:]
Sk_RS=np.sqrt(Vk_RS)

with h5.File("output_ss.h5",'r') as f:
    k_MOCK,Nk_MOCK,Pk_MOCK,Vk_MOCK=np.array(f['info']['scalars']['k_modes']),np.array(f['info']['scalars']['k_modes']).shape[0],f['scalars']['Pk'][0][0],f['scalars']['Vk'][0][0]
    k_MOCK=k_MOCK[1:]
    Pk_MOCK,Vk_MOCK=Pk_MOCK[1:],Vk_MOCK[1:]
    Sk_MOCK=np.sqrt(Vk_MOCK)

REF=read_powerspectrum("input_power.h5")
REF_Spline=InterpolatedUnivariateSpline(REF.k_modes, REF.powerspectrum, k=2)
Pk_th=REF_Spline(k)

PrintMessage(4, "Producing plots...")

# First plot: power spectra
fig = plt.figure()
ax = fig.add_subplot(1,1,1)
ax.tick_params(which="both",direction="in")
ax.set_xscale("log")
ax.set_xlabel("$k$ [$h/\mathrm{Mpc}$]")
ax.set_xlim([1e-2,2e0])
ax.set_yscale("log")
ax.set_ylabel("$P(k)$ [$(\mathrm{Mpc}/h)^3$]")
ax.set_ylim([1e1,1e5])

ax.plot(k,Pk_th,color='black',linewidth=1.5,linestyle='--',label="linear")
ax.plot(k,Pk_LPT,label="LPT")
ax.plot(k,Pk_PM,label="PM")
ax.plot(k,Pk_COLA,label="tCOLA")
ax.plot(k,Pk_RS,label="tCOLA (RS)")
ax.plot(k,Pk_MOCK,label="MOCK")
ax.legend(loc='best', frameon=False)

plt.savefig("power.pdf", dpi=300, bbox_inches="tight")

# Second plot: power spectra ratio
fig = plt.figure(figsize=(8,8))
ax = fig.add_subplot(1,1,1)
ax.set_xscale("log")
ax.set_xlim([3e-2,3e0])
ax.set_xlabel("$k$ [$h/\mathrm{Mpc}$]")
ax.set_ylabel("$P(k)/P_\mathrm{linear}(k)$")

ax.plot(k,Pk_LPT/Pk_th,label="LPT")
ax.fill_between(k,(Pk_LPT-2*Sk_LPT)/Pk_th,(Pk_LPT+2*Sk_LPT)/Pk_th,alpha=0.5)
ax.plot(k,Pk_PM/Pk_th,label="PM")
ax.fill_between(k,(Pk_PM-2*Sk_PM)/Pk_th,(Pk_PM+2*Sk_PM)/Pk_th,alpha=0.5)
ax.plot(k,Pk_COLA/Pk_th,label="tCOLA")
ax.fill_between(k,(Pk_COLA-2*Sk_COLA)/Pk_th,(Pk_COLA+2*Sk_COLA)/Pk_th,alpha=0.5)
ax.plot([1e-3,1e1],[1.,1.],color="black",linestyle="--")
ax.legend(loc='best', frameon=False)

plt.savefig("power_ratio.pdf", dpi=300, bbox_inches="tight")

PrintMessage(4, "Producing plots done.")
PrintInfo("Everything done successfully, exiting Simbelmyne test_power script.")
