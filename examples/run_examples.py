#!/usr/bin/env python
# -*- coding: latin-1 -*-
#-------------------------------------------------------------------------------------
# Simbelmynë v0.3.2 -- examples/run_examples.py
# Copyright (C) 2012-2020 Florent Leclercq.
# 
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
# 
# This program is free software: you can redistribute it and/or modify  
# it under the terms of the GNU General Public License as published by  
# the Free Software Foundation, version 3.
# 
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranty of 
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU 
# General Public License for more details.
# 
# The text of the license is located in the root directory of the source package.
#-------------------------------------------------------------------------------------

"""Simbelynë example: run the simulations
and show slices of output fields
"""

__author__  = "Florent Leclercq"
__version__ = "0.3.2"
__date__    = "2016-2020"
__license__ = "GPLv3"

from pysbmy import pySbmy
from pysbmy.utils import PrintInfo

PrintInfo("This is the Simbelmynë run_examples script.")

# Run Simbelmynë
pySbmy("example_lpt.sbmy", "logs_lpt.txt")
pySbmy("example_pm.sbmy", "logs_pm.txt")
pySbmy("example_tcola.sbmy", "logs_tcola.txt")
pySbmy("example_rsd.sbmy", "logs_rsd.txt")
pySbmy("example_mock.sbmy", "logs_mock.txt")

PrintInfo("Everything done successfully, exiting Simbelmynë run_examples script.")
