#!/usr/bin/env python
# -*- coding: latin-1 -*-
#-------------------------------------------------------------------------------------
# Simbelmynë v0.5.0 -- examples/example_setup.py
# Copyright (C) 2012-2023 Florent Leclercq.
#
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# The text of the license is located in the root directory of the source package.
#-------------------------------------------------------------------------------------

"""Setup for the Simbelynë example
"""

__author__  = "Florent Leclercq"
__version__ = "0.5.0"
__date__    = "2016-2023"
__license__ = "GPLv3"

L0=L1=L2=250.
corner0=corner1=corner2=-125.
N0=N1=N2=128
Np0=Np1=Np2=128
Npm0=Npm1=Npm2=128
N_CAT=1
cosmo={'h':0.6774, 'Omega_r':0., 'Omega_q':0.6911, 'Omega_b':0.0486, 'Omega_m':0.3089, 'm_ncdm':0., 'Omega_k':0., 'tau_reio':0.066, 'n_s':0.9667, 'sigma8':0.8159, 'w0_fld':-1., 'wa_fld':0., 'k_max':10.0, 'WhichSpectrum':"EH"}
