#!/usr/bin/env python
# -*- coding: latin-1 -*-
#-------------------------------------------------------------------------------------
# Simbelmynë v0.5.0 -- examples/setup_power.py
# Copyright (C) 2012-2023 Florent Leclercq.
#
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# The text of the license is located in the root directory of the source package.
#-------------------------------------------------------------------------------------

"""Simbelynë example: set up the initial
power spectrum
"""

__author__  = "Florent Leclercq"
__version__ = "0.5.0"
__date__    = "2016-2023"
__license__ = "GPLv3"

import numpy as np
from pysbmy.utils import PrintPythonCommand
from pysbmy.fft import FourierGrid
from pysbmy.power import PowerSpectrum

PrintPythonCommand()

# basic setup
from example_setup import L0,L1,L2,N0,N1,N2,cosmo

Pk=PowerSpectrum(L0,L1,L2,N0,N1,N2,cosmo)
Pk.write("input_power.h5")

kmin=0.
kmax=1.
Nk=100
k_modes=np.linspace(kmin,kmax,Nk+1)[:-1]
Gk=FourierGrid(L0,L1,L2,N0,N1,N2,k_modes=k_modes,kmax=kmax)
Gk.write("input_ss_k_grid.h5")
