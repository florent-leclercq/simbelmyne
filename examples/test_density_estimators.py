#!/usr/bin/env python
# -*- coding: latin-1 -*-
#-------------------------------------------------------------------------------------
# Simbelmynë v0.5.0 -- examples/test_density_estimators.py
# Copyright (C) 2012-2023 Florent Leclercq.
#
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# The text of the license is located in the root directory of the source package.
#-------------------------------------------------------------------------------------

"""Simbelynë example: test different density estimators and show slices
"""

__author__  = "Florent Leclercq"
__version__ = "0.5.0"
__date__    = "2016-2023"
__license__ = "GPLv3"

from pysbmy.utils import PrintInfo, ExecuteBashCommandMute
from example_setup import N0, corner0, corner1, corner2

PrintInfo("This is the Simbelmyne test_density_estimators script.")

corners=" --corner0="+str(corner0)+" --corner1="+str(corner1)+" --corner2="+str(corner2)
ExecuteBashCommandMute("snapshot_to_density_masstracers lpt_particles.gadget3 "+str(N0)+" lpt_density_masstracers.h5"+corners)
ExecuteBashCommandMute("snapshot_to_density_tetrahedra lpt_particles.gadget3 "+str(N0)+" lpt_density_tetrahedra.h5"+corners)

PrintInfo("Everything done successfully, exiting Simbelmyne test_density_estimators script.")
