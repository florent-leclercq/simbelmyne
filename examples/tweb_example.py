#!/usr/bin/env python
# -*- coding: latin-1 -*-
#-------------------------------------------------------------------------------------
# Simbelmynë v0.5.0 -- examples/tweb_example.py
# Copyright (C) 2012-2023 Florent Leclercq.
#
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# The text of the license is located in the root directory of the source package.
#-------------------------------------------------------------------------------------

"""Simbelynë example: classify structures with the T-web
"""

__author__  = "Florent Leclercq"
__version__ = "0.5.0"
__date__    = "2014-2023"
__license__ = "GPLv3"

from pysbmy.utils import PrintInfo, ExecuteBashCommandMute

PrintInfo("This is the Simbelmyne tweb_example script.")

ExecuteBashCommandMute("tweb lpt_density.h5 tweb_ .h5")

PrintInfo("Everything done successfully, exiting Simbelmyne tweb_example script.")
