#!/usr/bin/env python
# -*- coding: latin-1 -*-
#-------------------------------------------------------------------------------------
# Simbelmynë v0.5.0 -- examples/setup_parfiles.py
# Copyright (C) 2012-2023 Florent Leclercq.
#
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# The text of the license is located in the root directory of the source package.
#-------------------------------------------------------------------------------------

"""Simbelynë example: set up the Simbelynë
parameter files
"""

__author__  = "Florent Leclercq"
__version__ = "0.5.0"
__date__    = "2016-2023"
__license__ = "GPLv3"

from pysbmy import param_file
from pysbmy.utils import PrintPythonCommand

PrintPythonCommand()

# basic setup
from example_setup import L0,L1,L2,corner0,corner1,corner2,N0,N1,N2,Np0,Np1,Np2,Npm0,Npm1,Npm2,cosmo
Particles=Np0
Mesh=N0
BoxSize=L0
ParticleMesh=Npm0
RedshiftLPT=19.
NumberOfTilesPerDimension=2

S=param_file(Particles=Particles, Mesh=Mesh, BoxSize=BoxSize, corner0=corner0, corner1=corner1, corner2=corner2, WriteInitialConditions=1)
S.write("example_lpt.sbmy")

S=param_file(Particles=Particles, Mesh=Mesh, BoxSize=BoxSize, corner0=corner0, corner1=corner1, corner2=corner2, ICsMode=2, RedshiftLPT=RedshiftLPT, WriteLPTSnapshot=0, WriteLPTDensity=0, ModulePMCOLA=1, EvolutionMode=1, ParticleMesh=ParticleMesh, NumberOfTimeSteps=20, WriteFinalDensity=1, OutputFinalDensity="final_density_pm.h5")
S.write("example_pm.sbmy")

S=param_file(Particles=Particles, Mesh=Mesh, BoxSize=BoxSize, corner0=corner0, corner1=corner1, corner2=corner2, ICsMode=2, RedshiftLPT=RedshiftLPT, WriteLPTSnapshot=0, WriteLPTDensity=0, ModulePMCOLA=1, EvolutionMode=2, ParticleMesh=ParticleMesh, NumberOfTimeSteps=10, WriteFinalDensity=1, OutputFinalDensity="final_density_tcola.h5")
S.write("example_tcola.sbmy")

S=param_file(Particles=Particles, Mesh=Mesh, BoxSize=BoxSize, corner0=corner0, corner1=corner1, corner2=corner2, ICsMode=2, RedshiftLPT=RedshiftLPT, WriteLPTSnapshot=0, WriteLPTDensity=0, ModulePMCOLA=1, EvolutionMode=3, ParticleMesh=ParticleMesh//NumberOfTilesPerDimension, NumberOfTimeSteps=10, NumberOfTilesPerDimension=NumberOfTilesPerDimension, NumberOfParticlesInBuffer=16, WriteFinalDensity=1, OutputFinalDensity="final_density_scola.h5")
S.write("example_scola.sbmy")

S=param_file(Particles=Particles, Mesh=Mesh, BoxSize=BoxSize, corner0=corner0, corner1=corner1, corner2=corner2, ICsMode=2, RedshiftLPT=RedshiftLPT, WriteLPTSnapshot=0, WriteLPTDensity=0, ModulePMCOLA=1, EvolutionMode=2, ParticleMesh=ParticleMesh, NumberOfTimeSteps=3, WriteFinalDensity=0, ModuleRSD=1, DoNonLinearMapping=1, WriteRSDensity=1, OutputRSDensity="rs_density_tcola.h5")
S.write("example_rsd.sbmy")

S=param_file(Mesh=Mesh, ModuleLPT=0, ModuleMocks=1, NumberOfNoiseRealizations=1, InputDensityMocks="rs_density_tcola.h5")
S.write("example_mock.sbmy")
