#!/usr/bin/env python
# -*- coding: latin-1 -*-
#-------------------------------------------------------------------------------------
# Simbelmynë v0.5.0 -- setup.py
# Copyright (C) 2012-2023 Florent Leclercq.
#
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# The text of the license is located in the root directory of the source package.
#-------------------------------------------------------------------------------------

"""Setup script for Simbelmynë
"""

__author__  = "Florent Leclercq"
__version__ = "0.5.0"
__date__    = "2019-2023"
__license__ = "GPLv3"

import setuptools

# Open the README
def readme():
    import io
    with io.open('README.md', mode='r', encoding='ISO-8859-1') as f:
        return f.read()

# Find extensions to be installed as packages
def find_packages():
    found_packages=setuptools.find_packages()
    found_packages=["pysbmy"]+[package for package in found_packages if "pysbmy" in package and "extensions" in package]
    packages=[] 
    package_dir={} 
    for package in found_packages: 
        this_package_dir=package.replace(".","/") 
        this_package=package.replace("extensions.","pysbmy.").replace(".pysbmy","") 
        packages.append(this_package) 
        package_dir[this_package]=this_package_dir
    return packages, package_dir

# Find scripts to be installed
def find_scripts(): 
    def append_python_tools(scripts,fdir):
        import os
        for fname in os.listdir(fdir):
            if os.path.isfile(fdir+fname):
                with open(fdir+fname) as f:
                    try:
                        if "!/usr/bin/env python" in f.readline():
                            scripts.append(fdir+fname)
                    except:
                        continue
    scripts=[]
    append_python_tools(scripts,"pysbmy/tools/")
    found_packages=setuptools.find_packages() 
    found_packages=[package for package in found_packages if "pysbmy" in package and "extensions" in package] 
    for package in found_packages: 
        this_package_dir=package.replace(".","/")
        append_python_tools(scripts,this_package_dir+"/tools/")
    return scripts

# Setup
setuptools.setup(
    name="pysbmy",
    version="0.5",
    author="Florent Leclercq",
    author_email="florent.leclercq@polytechnique.org",
    description="A python wrapper for Simbelmynë",
    long_description=readme(),
    url="http://simbelmyne.florent-leclercq.eu",
    packages=find_packages()[0],
    package_dir=find_packages()[1],
    package_data={'': ['build/simbelmyne.so']},
    scripts=find_scripts(),
    entry_points={
        'console_scripts': [
            'simbelmyne = pysbmy.__main__:simbelmyne',
            'scola = pysbmy.__main__:scola'
        ]
    },
    install_requires=[
        "numpy",
        "scipy",
        "matplotlib",
        "h5py",
        "pathlib",
        "astropy"
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        "Operating System :: OS Independent",
    ],
)
