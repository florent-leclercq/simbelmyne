#!/usr/bin/env python
# -*- coding: latin-1 -*-
#-------------------------------------------------------------------------------------
# Simbelmynë v0.5.0 -- scripts/simbelmyne_tools.py
# Copyright (C) 2012-2023 Florent Leclercq.
#
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# The text of the license is located in the root directory of the source package.
#-------------------------------------------------------------------------------------

"""Script to automatically import all Simbelmynë tools
"""

__author__  = "Florent Leclercq"
__version__ = "0.5.0"
__date__    = "2013-2023"
__license__ = "GPLv3"

import os, pysbmy

import numpy as np
import matplotlib
if not "DISPLAY" in os.environ:
    matplotlib.use("Agg")
    pysbmy.utils.PrintWarning("No display environment found for matplotlib.pyplot.")
import matplotlib.pyplot as plt
if pysbmy.module_exists("healpy"):
    import healpy as hp
else:
    pysbmy.utils.PrintWarning("Module healpy not found.")
if pysbmy.module_exists("h5py"):
    import h5py as h5
else:
    pysbmy.utils.PrintWarning("Module h5py not found.")

from pysbmy.correlations import *
from pysbmy.cosmology import *
from pysbmy.density import *
from pysbmy.fft import *
from pysbmy.field import *
from pysbmy.grf import *
from pysbmy.lpt import *
from pysbmy.power import *
from pysbmy.rgen import *
from pysbmy.snapshot import *
from pysbmy.survey_geometry import *
from pysbmy.utils import *
from pysbmy.velocity import *
