#!/usr/bin/env python

"""pysbmy __init__
"""

__author__  = "Florent Leclercq"
__version__ = "SBMY_VERSION"
__date__    = "2018-SBMY_YEAR"
__license__ = "GPLv3"

# Do not remove this file!
