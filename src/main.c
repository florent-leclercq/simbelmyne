///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- src/main.c
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file main.c
 *  \brief File containing main functions
 *  \authors Florent Leclercq
 *  \version 0.5.0
 *  \date 2012-2023
 */
#include "main.h"
///-------------------------------------------------------------------------------------
/** @fn print_ascii_art
 * Auxiliary function
 */
static inline void print_ascii_art()
{
    PrintInfo("");
    PrintInfo("            .-~~-.--.");
    PrintInfo("           :         )");
    PrintInfo("     .~ ~ -.\\       /.- ~~ .");
    PrintInfo("     >       `.   .'       <");
    PrintInfo("    (         .- -.         )");
    PrintInfo("     `- -.-~  `- -'  ~-.- -'");
    PrintInfo("       (        :        )           _ _ .-:        ___________________________________");
    sprintf(G__msg__, "        ~--.    :    .--~        .-~  .-~  }                    %sSIMBELMYNË%s", FONT_SIMBELMYNE, FONT_NORMAL);
    PrintInfo(G__msg__);
    PrintInfo("            ~-.-^-.-~ \\_      .~  .-~   .~           (c) Florent Leclercq 2012 - 2023 ");
    PrintInfo("                     \\ \'     \\ '_ _ -~              ___________________________________");
    PrintInfo("                      `.`.    //");
    PrintInfo("             . - ~ ~-.__`.`-.//");
    PrintInfo("         .-~   . - ~  }~ ~ ~-.~-.");
    PrintInfo("       .' .-~      .-~       :/~-.~-./:");
    PrintInfo("      /_~_ _ . - ~                 ~-.~-._");
    PrintInfo("                                       ~-.<");
    PrintInfo("");
}
///-------------------------------------------------------------------------------------
/** @fn main_sbmy
 * Main function
 */
int main_sbmy(int argc, char **argv)
{
    /* Open logs */
    if(argc == 3)
    {
        stdlog = fopen(argv[2], "w");
        ISSET_LOGS=true;
    }
    else
        ISSET_LOGS=false;

    /* Initialization */
    Begin();

    /* Prints the command line */
    PrintCommandLine(argc, argv);

    /* Prints ASCII art */
    print_ascii_art();

    /* Prints date, time, and commit hash */
    time_t timer; struct tm* tm_info; time(&timer); tm_info = localtime(&timer); char buf[26];
    strftime(buf, 26, "%Y-%m-%d %H:%M:%S", tm_info);
    sprintf(G__msg__, "%s: Starting SIMBELMYNË, commit hash %s", buf, HASH);
    PrintInfo(G__msg__);

    /* Verifies command line */
    if(argc != 2 && argc != 3)
    {
        sprintf(G__msg__, "SIMBELMYNË requires one or two arguments (given %d): the parameter file and the name of the log file (optional).", argc-1);
        FatalError(G__msg__);
    }

    /* Prints debug status */
    debugPrintStatus();

    /* Reads and verifies the parameter file */
    Param_File Params;
    if(exists(argv[1]))
    {
        Params = read_parameterfile(argv[1]);
    }
    else
    {
        sprintf(G__msg__, "Parameter file '%s' does not exist.", argv[1]);
        PrintError(G__msg__);
        sprintf(G__msg__, "SIMBELMYNË requires one or two arguments: the parameter file and the name of the log file (optional).");
        FatalError(G__msg__);
    }

    /* Declare variables */
    int Nthreads = omp_get_max_threads();
    const bool ModuleLPT=Params.ModuleLPT, WriteICsRngState=Params.WriteICsRngState, WriteWhiteNoise=Params.WriteWhiteNoise, WriteInitialConditions=Params.WriteInitialConditions, WriteLPTDensity=Params.WriteLPTDensity, WriteLPTSnapshot=Params.WriteLPTSnapshot, ModulePMCOLA=Params.ModulePMCOLA, WriteSnapshots=Params.WriteSnapshots, WriteDensities=Params.WriteDensities, WriteFinalSnapshot=Params.WriteFinalSnapshot, WriteFinalDensity=Params.WriteFinalDensity, WriteReferenceFrame=Params.WriteReferenceFrame, ReadReferenceFrame=Params.ReadReferenceFrame, ModuleRSD=Params.ModuleRSD, DoNonLinearMapping=Params.DoNonLinearMapping, DoLPTSplit=Params.DoLPTSplit, WriteRSSnapshot=Params.WriteRSSnapshot, WriteRSDensity=Params.WriteRSDensity, ModuleMocks=Params.ModuleMocks, WriteMocksRngState=Params.WriteMocksRngState, WriteMocks=Params.WriteMocks, WriteSummaryStats=Params.WriteSummaryStats;
    const char *InputRngStateLPT=Params.InputRngStateLPT, *OutputICsRngState=Params.OutputICsRngState, *OutputRngStateLPT=Params.OutputRngStateLPT, *InputWhiteNoise=Params.InputWhiteNoise, *OutputWhiteNoise=Params.OutputWhiteNoise, *InputPowerSpectrum=Params.InputPowerSpectrum, *InputInitialConditions=Params.InputInitialConditions, *OutputInitialConditions=Params.OutputInitialConditions, *OutputLPTSnapshot=Params.OutputLPTSnapshot, *OutputLPTDensity=Params.OutputLPTDensity, *OutputSnapshotsBase=Params.OutputSnapshotsBase, *OutputSnapshotsExt=Params.OutputSnapshotsExt, *OutputDensitiesBase=Params.OutputDensitiesBase, *OutputDensitiesExt=Params.OutputDensitiesExt, *OutputFinalSnapshot=Params.OutputFinalSnapshot, *OutputFinalDensity=Params.OutputFinalDensity, *OutputKickBase=Params.OutputKickBase, *OutputKickExt=Params.OutputKickExt, *OutputDriftBase=Params.OutputDriftBase, *OutputDriftExt=Params.OutputDriftExt, *InputKickBase=Params.InputKickBase, *InputKickExt=Params.InputKickExt, *InputDriftBase=Params.InputDriftBase, *InputDriftExt=Params.InputDriftExt,    *OutputLPTPotential1=Params.OutputLPTPotential1, *OutputLPTPotential2=Params.OutputLPTPotential2, *OutputTilesBase=Params.OutputTilesBase, *OutputTilesExt=Params.OutputTilesExt, *OutputRSSnapshot=Params.OutputRSSnapshot, *OutputRSDensity=Params.OutputRSDensity, *InputDensityMocks=Params.InputDensityMocks, *InputSurveyGeometry=Params.InputSurveyGeometry, *InputSummaryStatskGrid=Params.InputSummaryStatskGrid, *InputRngStateMocks=Params.InputRngStateMocks, *OutputMocksRngState=Params.OutputMocksRngState, *OutputRngStateMocks=Params.OutputRngStateMocks, *OutputMockBase=Params.OutputMockBase, *OutputMockExt=Params.OutputMockExt, *OutputSummaryStats=Params.OutputSummaryStats;
    const int SnapFormat=Params.SnapFormat, NumFilesPerSnapshot=Params.NumFilesPerSnapshot, ICsMode=Params.ICsMode, EvolutionMode=Params.EvolutionMode, NumberOfTimeSteps=Params.NumberOfTimeSteps, TimeStepDistribution=Params.TimeStepDistribution, NumberOfTilesPerDimension = Params.NumberOfTilesPerDimension, NumberOfParticlesInBuffer=Params.NumberOfParticlesInBuffer;
    const double RedshiftLPT=Params.RedshiftLPT, RedshiftFCs=Params.RedshiftFCs, n_LPT=Params.n_LPT, alpha1RSD=Params.alpha1RSD, alpha2RSD=Params.alpha2RSD;

    const int N0=Params.Mesh, N1=Params.Mesh, N2=Params.Mesh;
    const int Np0=Params.Particles, Np1=Params.Particles, Np2=Params.Particles; const particleID_t Np=Np0*Np1*Np2;
    const int Npm0=Params.ParticleMesh, Npm1=Params.ParticleMesh, Npm2=Params.ParticleMesh;
    const int NoiseModel=Params.NoiseModel, NumberOfNoiseRealizations=Params.NumberOfNoiseRealizations;
    const double L0=Params.BoxSize, L1=Params.BoxSize, L2=Params.BoxSize;
    const double corner0=Params.corner0, corner1=Params.corner1, corner2=Params.corner2;
    const double v_observer[3] = {Params.vobs0, Params.vobs1, Params.vobs2};
    const double cosmo[N_COSMOPAR]={Params.h, Params.Omega_r, Params.Omega_q, Params.Omega_b, Params.Omega_m, Params.Omega_k, Params.n_s, Params.sigma8, Params.w0_fld, Params.wa_fld}; // cosmological parameters, respect the standard ordering here!
    const bool ModifiedDiscretization=EvolutionMode==1?false:Params.ModifiedDiscretization; // do not use modified discretization in case PM is selected

    Field ICs_field, FCs_field;
    Snapshot P;
    particle_lpt_t *PsiLPT;
    PsiLPT = (particle_lpt_t *)p_malloc(Np*sizeof(particle_lpt_t), VARNAME(PsiLPT));

    /* Allocate memory or read field */
    const bool doSimulation = ModuleLPT && (WriteLPTDensity || WriteLPTSnapshot || (ModulePMCOLA && (WriteFinalSnapshot || WriteFinalDensity)) || (ModuleRSD && (WriteRSSnapshot || WriteRSDensity)) || ModuleMocks);
    if(doSimulation)
        FCs_field = allocate_scalar_field(N0, N1, N2, corner0, corner1, corner2, L0, L1, L2, a_ref, VARNAME(FCs_field));
    else
        FCs_field = read_field(InputDensityMocks, VARNAME(FCs_field));

    /* Declare variables for timers */
    CPUtime startm; Walltime startw; double diff_t_cpu, diff_t_wall;

    /* ----------------------------------------------------------------------------------------------------------------------- */
    /* Module LPT ------------------------------------------------------------------------------------------------------------ */
    /* ----------------------------------------------------------------------------------------------------------------------- */
    if(ModuleLPT)
    {
        /* Start module timer */
        StartModuleTimers();

        /* Generate a snapshot containing a regular grid of particles */
        StartTimers(&startm, &startw);
        PrintModule("ModuleLPT: Initializing snapshot...");
        INDENT;
        P = return_grid_snapshot_DM(Np0, Np1, Np2, L0, NumFilesPerSnapshot, cosmo);
        UNINDENT;
        PrintModule("ModuleLPT: Initializing snapshot done.");
        StopTimers(startm, startw, &diff_t_cpu, &diff_t_wall);
        PrintTimers("LPT snapshot initialization", diff_t_cpu, diff_t_wall);

        /* Read or generate the initial conditions */
        StartTimers(&startm, &startw);
        PrintModule("ModuleLPT: Returning initial conditions...");
        INDENT;
        ICs_field = get_initial_conditions(ICsMode, InputRngStateLPT, WriteICsRngState, OutputICsRngState, OutputRngStateLPT, InputWhiteNoise, WriteWhiteNoise, OutputWhiteNoise, InputPowerSpectrum, InputInitialConditions, WriteInitialConditions, OutputInitialConditions, N0, N1, N2, corner0, corner1, corner2, L0, L1, L2);
        UNINDENT;
        PrintModule("ModuleLPT: Returning initial conditions done.");
        StopTimers(startm, startw, &diff_t_cpu, &diff_t_wall);
        PrintTimers("LPT initial conditions", diff_t_cpu, diff_t_wall);

        if(!(ModulePMCOLA && EvolutionMode == 3)) // Evolution with LPT is not necessary with sCOLA
        {
            /* Evolve with LPT */
            StartTimers(&startm, &startw);
            sprintf(G__msg__, "ModuleLPT: Evolving with Lagrangian perturbation theory (using %d cores)...", Nthreads);
            PrintModule(G__msg__);
            INDENT;
            evolve_lpt(&P, ICs_field, Np, PsiLPT, RedshiftLPT, N0, N1, N2, L0, L1, L2, Np0, Np1, Np2, cosmo);
            UNINDENT;
            sprintf(G__msg__, "ModuleLPT: Evolving with Lagrangian perturbation theory (using %d cores) done.", Nthreads);
            PrintModule(G__msg__);
            StopTimers(startm, startw, &diff_t_cpu, &diff_t_wall);
            PrintTimers("LPT evolution", diff_t_cpu, diff_t_wall);

            /* Write outputs if required */
            StartTimers(&startm, &startw);
            const bool UpdateLPTDensity = WriteLPTDensity || (ModuleMocks && !ModulePMCOLA && !ModuleRSD);
            if(WriteLPTSnapshot || UpdateLPTDensity)
            {
                PrintModule("ModuleLPT: Computing outputs...");
                INDENT;
                update_and_output_density(FCs_field, P, UpdateLPTDensity, WriteLPTDensity, z2a(RedshiftLPT), OutputLPTDensity);
                output_snapshot(WriteLPTSnapshot, P, OutputLPTSnapshot, NumFilesPerSnapshot, SnapFormat);
                UNINDENT;
                PrintModule("ModuleLPT: Computing outputs done.");
            }
            StopTimers(startm, startw, &diff_t_cpu, &diff_t_wall);
            PrintTimers("LPT output", diff_t_cpu, diff_t_wall);
        }

        /* Stop module timer and print result */
        StopPrintModuleTimers("ModuleLPT");
    }

    /* ----------------------------------------------------------------------------------------------------------------------- */
    /* Module PM/COLA -------------------------------------------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------------------------------------------------------- */
    if(ModulePMCOLA)
    {
        /* Start module timer */
        StartModuleTimers();

        switch(EvolutionMode)
        {
            case 1:
                /* Evolve with PM */
                PrintModule("ModulePMCOLA: Evolving with PM...");
                INDENT;
                evolve_pm_cola(&P, FCs_field, Np, PsiLPT, EvolutionMode, NumberOfTimeSteps, TimeStepDistribution, RedshiftFCs, ModifiedDiscretization, n_LPT, WriteSnapshots, OutputSnapshotsBase, OutputSnapshotsExt, NumFilesPerSnapshot, SnapFormat, WriteDensities, OutputDensitiesBase, OutputDensitiesExt, WriteReferenceFrame, OutputKickBase, OutputKickExt, OutputDriftBase, OutputDriftExt, ReadReferenceFrame, InputKickBase, InputKickExt, InputDriftBase, InputDriftExt, Np0, Np1, Np2, Npm0, Npm1, Npm2, L0, L1, L2, cosmo);
                UNINDENT;
                PrintModule("ModulePMCOLA: Evolving with PM done.");
                break;
            case 2:
                /* Evolve with tCOLA */
                PrintModule("ModulePMCOLA: Evolving with tCOLA...");
                INDENT;
                evolve_pm_cola(&P, FCs_field, Np, PsiLPT, EvolutionMode, NumberOfTimeSteps, TimeStepDistribution, RedshiftFCs, ModifiedDiscretization, n_LPT, WriteSnapshots, OutputSnapshotsBase, OutputSnapshotsExt, NumFilesPerSnapshot, SnapFormat, WriteDensities, OutputDensitiesBase, OutputDensitiesExt, WriteReferenceFrame, OutputKickBase, OutputKickExt, OutputDriftBase, OutputDriftExt, ReadReferenceFrame, InputKickBase, InputKickExt, InputDriftBase, InputDriftExt, Np0, Np1, Np2, Npm0, Npm1, Npm2, L0, L1, L2, cosmo);
                UNINDENT;
                PrintModule("ModulePMCOLA: Evolving with tCOLA done.");
                break;
            case 3:
                /* Evolve with tCOLA */
                PrintModule("ModulePMCOLA: Evolving with sCOLA...");
                INDENT;
                evolve_scola(&P, &ICs_field, &FCs_field, PsiLPT, OutputLPTPotential1, OutputLPTPotential2, OutputTilesBase, OutputTilesExt, NumberOfTilesPerDimension, NumberOfParticlesInBuffer, NumberOfTimeSteps, TimeStepDistribution, RedshiftLPT, RedshiftFCs, ModifiedDiscretization, n_LPT, N0, N1, N2, Np0, Np1, Np2, Npm0, Npm1, Npm2, L0, L1, L2, corner0, corner1, corner2, cosmo);
                UNINDENT;
                PrintModule("ModulePMCOLA: Evolving with sCOLA done.");
                break;
        }

        /* Write outputs if required */
        StartTimers(&startm, &startw);
        const bool UpdateFinalDensity = WriteFinalDensity || (ModuleMocks && !ModuleRSD);
        if(WriteFinalSnapshot || UpdateFinalDensity)
        {
            PrintModule("ModulePMCOLA: Computing outputs...");
            INDENT;
            update_and_output_density(FCs_field, P, UpdateFinalDensity, WriteFinalDensity, z2a(RedshiftFCs), OutputFinalDensity);
            output_snapshot(WriteFinalSnapshot, P, OutputFinalSnapshot, NumFilesPerSnapshot, SnapFormat);
            UNINDENT;
            PrintModule("ModulePMCOLA: Computing outputs done.");
        }
        StopTimers(startm, startw, &diff_t_cpu, &diff_t_wall);
        PrintTimers("PMCOLA output", diff_t_cpu, diff_t_wall);

        /* Stop module timer and print result */
        StopPrintModuleTimers("ModulePMCOLA");
    }

    if(ModuleLPT)
        free_field(ICs_field, VARNAME(ICs_field));

    /* ----------------------------------------------------------------------------------------------------------------------- */
    /* Module RSD ------------------------------------------------------------------------------------------------------------ */
    /* ----------------------------------------------------------------------------------------------------------------------- */
    if(ModuleRSD)
    {
        /* Start module timer */
        StartModuleTimers();

        /* Put particles in redshift space */
        sprintf(G__msg__, "ModuleRSD: Computing redshift space distortions (using %d cores)...", Nthreads);
        PrintModule(G__msg__);
        INDENT;
        update_snapshot_RSD(&P, Np, PsiLPT, v_observer, DoNonLinearMapping, L0, L1, L2, corner0, corner1, corner2, alpha1RSD, DoLPTSplit, alpha2RSD, cosmo);
        UNINDENT;
        sprintf(G__msg__, "ModuleRSD: Computing redshift space distortions (using %d cores) done.", Nthreads);
        PrintModule(G__msg__);

        /* Write outputs if required */
        const bool UpdateRSDensity = WriteRSDensity || ModuleMocks;
        if(WriteRSSnapshot || UpdateRSDensity)
        {
            PrintModule("ModuleRSD: Computing outputs...");
            INDENT;
            update_and_output_density(FCs_field, P, UpdateRSDensity, WriteRSDensity, z2a(FCs_field.time), OutputRSDensity);
            output_snapshot(WriteRSSnapshot, P, OutputRSSnapshot, NumFilesPerSnapshot, SnapFormat);
            UNINDENT;
            PrintModule("ModuleRSD: Computing outputs done.");
        }

        /* Stop module timer and print result */
        StopPrintModuleTimers("ModuleRSD");
    }
    p_free(PsiLPT, VARNAME(PsiLPT));

    /* ----------------------------------------------------------------------------------------------------------------------- */
    /* Module Mock Catalogs -------------------------------------------------------------------------------------------------- */
    /* ----------------------------------------------------------------------------------------------------------------------- */
    if(ModuleMocks)
    {
        /* Start module timer */
        StartModuleTimers();

        /* Produce mocks and summary statistics */
        sprintf(G__msg__, "ModuleMocks: Producing mock galaxy catalogs (using %d cores)...", Nthreads);
        PrintModule(G__msg__);
        INDENT;
        produce_mocks_and_summaries(FCs_field.data, InputSurveyGeometry, InputSummaryStatskGrid, InputRngStateMocks, WriteMocksRngState, OutputMocksRngState, OutputRngStateMocks, NoiseModel, NumberOfNoiseRealizations, WriteMocks, OutputMockBase, OutputMockExt, WriteSummaryStats, OutputSummaryStats);
        UNINDENT;
        sprintf(G__msg__, "ModuleMocks: Producing mock galaxy catalogs (using %d cores) done.", Nthreads);
        PrintModule(G__msg__);

        /* Stop module timer and print result */
        StopPrintModuleTimers("ModuleMocks");
    }

    /* Free memory */
    if(ModuleLPT)
        free_snapshot(P, VARNAME(P));
    free_field(FCs_field, VARNAME(FCs_field));

    /* Ends */
    EndSuccess();
    return EXIT_SUCCESS;
} //main_sbmy
///-------------------------------------------------------------------------------------
/** @fn main_scola
 * Main function
 */
int main_scola(int argc, char **argv)
{
    /* Open logs */
    if(argc == 4)
    {
        stdlog = fopen(argv[3], "w");
        ISSET_LOGS=true;
    }
    else
        ISSET_LOGS=false;

    /* Initialization */
    Begin();

    /* Prints the command line */
    PrintCommandLine(argc, argv);

    /* Prints ASCII art */
    print_ascii_art();

    /* Prints date, time, and commit hash */
    time_t timer; struct tm* tm_info; time(&timer); tm_info = localtime(&timer); char buf[26];
    strftime(buf, 26, "%Y-%m-%d %H:%M:%S", tm_info);
    sprintf(G__msg__, "%s: Starting SIMBELMYNË/SCOLA, commit hash %s", buf, HASH);
    PrintInfo(G__msg__);

    /* Verifies command line */
    if(argc != 3 && argc != 4)
    {
        sprintf(G__msg__, "SCOLA requires two or three arguments (given %d): the parameter file, the ID of the sCOLA box to evolve, and the name of the log file (optional).", argc-1);
        FatalError(G__msg__);
    }

    /* Prints debug status */
    debugPrintStatus();

    /* Reads and verifies the parameter file */
    Param_File Params;
    if(exists(argv[1]))
    {
        Params = read_parameterfile(argv[1]);
    }
    else
    {
        sprintf(G__msg__, "Parameter file '%s' does not exist.", argv[1]);
        PrintError(G__msg__);
        sprintf(G__msg__, "SCOLA requires two or three arguments (given %d): the parameter file, the ID of the sCOLA box to evolve, and the name of the log file (optional).", argc-1);
        FatalError(G__msg__);
    }
    int THIS_TILE;
    if(sscanf(argv[2], "%i", &THIS_TILE) != 1)
    {
        sprintf(G__msg__, "Second argument (sCOLA box ID) must be an integer, given '%s'.", argv[2]);
        FatalError(G__msg__);
    }
    if(THIS_TILE<1 || THIS_TILE>Params.NumberOfTilesPerDimension*Params.NumberOfTilesPerDimension*Params.NumberOfTilesPerDimension)
    {
        sprintf(G__msg__, "Given the parameter file, the second argument (sCOLA box ID) must be between 1 and %d, given %d.", Params.NumberOfTilesPerDimension*Params.NumberOfTilesPerDimension*Params.NumberOfTilesPerDimension, THIS_TILE);
        FatalError(G__msg__);
    }

    /* Declare variables */
    const int EvolutionMode=Params.EvolutionMode, NumberOfTimeSteps=Params.NumberOfTimeSteps, TimeStepDistribution=Params.TimeStepDistribution, NumberOfTilesPerDimension = Params.NumberOfTilesPerDimension, NumberOfParticlesInBuffer=Params.NumberOfParticlesInBuffer;
    const char *OutputLPTPotential1=Params.OutputLPTPotential1, *OutputLPTPotential2=Params.OutputLPTPotential2, *OutputTilesBase=Params.OutputTilesBase, *OutputTilesExt=Params.OutputTilesExt;
    const double RedshiftLPT=Params.RedshiftLPT, RedshiftFCs=Params.RedshiftFCs, n_LPT=Params.n_LPT;

    const int N0=Params.Mesh, N1=Params.Mesh, N2=Params.Mesh;
    const int Np0=Params.Particles, Np1=Params.Particles, Np2=Params.Particles;
    const int Npm0=Params.ParticleMesh, Npm1=Params.ParticleMesh, Npm2=Params.ParticleMesh;
    const double L0=Params.BoxSize, L1=Params.BoxSize, L2=Params.BoxSize;
    const double cosmo[N_COSMOPAR]={Params.h, Params.Omega_r, Params.Omega_q, Params.Omega_b, Params.Omega_m, Params.Omega_k, Params.n_s, Params.sigma8, Params.w0_fld, Params.wa_fld}; // cosmological parameters, respect the standard ordering here!
    const bool ModifiedDiscretization=EvolutionMode==1?false:Params.ModifiedDiscretization; // do not use modified discretization in case PM is selected

    process_scola_box(OutputLPTPotential1, OutputLPTPotential2, OutputTilesBase, OutputTilesExt, THIS_TILE, NumberOfTilesPerDimension, NumberOfParticlesInBuffer, NumberOfTimeSteps, TimeStepDistribution, RedshiftLPT, RedshiftFCs, ModifiedDiscretization, n_LPT, N0,N1,N2, Np0,Np1,Np2, Npm0,Npm1,Npm2, L0,L1,L2, cosmo);

    /* Ends */
    EndSuccess();
    return EXIT_SUCCESS;
} //main_scola
///-------------------------------------------------------------------------------------
