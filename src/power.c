///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- src/power.c
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file power.c
 *  \brief Routines for computing the power spectrum
 *  \authors Florent Leclercq
 *  \version 0.5.0
 *  \date 2014-2023
 */
#include "power.h"
///-------------------------------------------------------------------------------------
/** @fn aux_tophatwindow
 * A subroutine giving a tophat window filter in Fourier space
 */
static inline double aux_tophatwindow(double rsmooth, double k)
{
    double aux1, window;

    aux1 = rsmooth * k;
    window= 3.0 * (sin(aux1) - aux1*cos(aux1))/aux1/aux1/aux1;
    if(k<1e-6) window=1.0;

    return window;
} //aux_tophatwindow
///-------------------------------------------------------------------------------------
/** @fn PL_spectrum
 * A subroutine for power-law power spectrum
 */
static double PL_spectrum(const double k, const double params[N_COSMOPAR+1])
{
    double result;
    double n_s=params[6];

    result = pow(k,n_s);

    return result;
} //PL_spectrum
///-------------------------------------------------------------------------------------
/** @fn aux_sigma8_PL
 * A subroutine for normalization of power spectrum, power-law only
 */
static double aux_sigma8_PL(double k, void *params)
{
    double result;
    double *aux=(double *)params;
    double rsmooth=aux[10];
    double window = aux_tophatwindow(rsmooth, k);

    result = PL_spectrum(k,aux) * gsl_pow_int(k * window,2);

    return result;
} //aux_sigma8_PL
///-------------------------------------------------------------------------------------
/** @fn BBKS_transfer
 * A subroutine for cdm transfer
 * (pure cdm transfer function)
 * by BBKS (Bardeen et al. 1986)
 */
static double BBKS_transfer(const double k, const double params[N_COSMOPAR+1])
{
    double h=params[0];
    double Omega_r=params[1];
    double Omega_q=params[2];
    double Omega_b=params[3];
    double Omega_m=params[4];
    double Omega_k=params[5];
    double n_s=params[6];
    double sigma8=params[7];
    double w0_fld=params[8];
    double wa_fld=params[9];
    double rsmooth=params[10];
    UNUSED(w0_fld);UNUSED(wa_fld);UNUSED(Omega_r); UNUSED(Omega_k);UNUSED(Omega_q);UNUSED(rsmooth);UNUSED(n_s);UNUSED(sigma8);

    double result,q,shape,fb,aux;
    double alpha = 2.34, beta = 3.89, gamma = 16.1, delta = 5.46, epsilon = 6.71;

    fb = Omega_b / Omega_m;
    shape = Omega_m * h * exp(-Omega_b - sqrt(2.0*h) * fb);
    q = k / shape;

    aux = 1.0 + beta * q + gsl_pow_int(gamma * q,2) + gsl_pow_int(delta * q,3) + gsl_pow_int(epsilon * q,4);
    result = log(1.0 + alpha * q) / (alpha * q) * pow(aux,-1.0/4.0);

    return result;
} //BBKS_transfer
///-------------------------------------------------------------------------------------
/** @fn BBKS_spectrum
 * A subroutine for cdm transfer
 */
static double BBKS_spectrum(const double k, const double params[N_COSMOPAR+1])
{
    double result;
    double n_s=params[6];

    result = pow(k,n_s) * gsl_pow_int(BBKS_transfer(k,params),2);

    return result;
} //BBKS_spectrum
///-------------------------------------------------------------------------------------
/** @fn aux_sigma8_BBKS
 * A subroutine for normalization of power spectrum, cdm only
 */
static double aux_sigma8_BBKS(double k, void *params)
{
    double result;
    double *aux=(double *)params;
    double rsmooth=aux[10];
    double window = aux_tophatwindow(rsmooth, k);

    result = BBKS_spectrum(k,aux) * gsl_pow_int(k * window,2);

    return result;
} //aux_sigma8_BBKS
///-------------------------------------------------------------------------------------
/** @fn EH_transfer
 * A subroutine for baryon transfer
 * (cdm transfer function including baryonic acoustic oscillations)
 * by Eisenstein & Hu 1998, courtesy of W. Percival
 */
static double EH_transfer(const double k, const double params[N_COSMOPAR+1])
{
    double h=params[0];
    double Omega_r=params[1];
    double Omega_q=params[2];
    double Omega_b=params[3];
    double Omega_m=params[4];
    double Omega_k=params[5];
    double n_s=params[6];
    double sigma8=params[7];
    double w0_fld=params[8];
    double wa_fld=params[9];
    double rsmooth=params[10];
    UNUSED(w0_fld);UNUSED(wa_fld);UNUSED(Omega_r);UNUSED(Omega_k);UNUSED(Omega_q);UNUSED(rsmooth);UNUSED(n_s);UNUSED(sigma8);

    double result;
    double rk,e,thet,thetsq,thetpf,b1,b2,zd,ze,rd,re,rke,s,rks,q,y,g;
    double ab,a1,a2,ac,bc,f,c1,c2,tc,bb,bn,ss,tb/*,tk_eh*/;
    double hsq,om_mhsq/*,om_b,om_m*/;
    double fb,x,q2;

    // convert k to Mpc-1 rather than hMpc-1
    rk = k * h;
    hsq = gsl_pow_int(h,2);
    om_mhsq = Omega_m * hsq;
    fb = Omega_b / Omega_m;

    // constants
    e = exp(1.0);
    thet = 2.728 / 2.7;
    thetsq = gsl_pow_int(thet,2);
    thetpf = gsl_pow_int(thetsq,2);

    // Equation 4 - redshift of drag epoch
    b1 = 0.313 * pow(om_mhsq,-0.419) * (1.0 + 0.607 * pow(om_mhsq,0.674));
    b2 = 0.238 * pow(om_mhsq,0.223);
    zd = 1291.* (1.0 + b1 * pow(Omega_b * hsq,b2)) *
        pow(om_mhsq,0.251) / (1.0 + 0.659 * pow(om_mhsq,0.828));

    // Equation 2 - redshift of matter-radiation equality
    ze = 2.50e4 * om_mhsq / thetpf;

    // value of R=(ratio of baryon-photon momentum density) at drag epoch
    rd = 31500.0 * Omega_b * hsq / (thetpf * zd);

    // value of R=(ratio of baryon-photon momentum density) at epoch of matter-radiation equality
    re = 31500.0 * Omega_b * hsq / (thetpf * ze);

    // Equation 3 - scale of ptcle horizon at matter-radiation equality
    rke = 7.46e-2 * om_mhsq / thetsq;

    // Equation 6 - sound horizon at drag epoch
    s = (2.0 / 3.0 / rke) * sqrt(6.0 / re) * log((sqrt(1.0 + rd) + sqrt(rd + re)) / (1.0 + sqrt(re)));

    // Equation 7 - silk damping scale
    rks = 1.6 * pow(Omega_b * hsq,0.52) * pow(om_mhsq,0.73) * (1.0 + pow(10.4 * om_mhsq,-0.95));

    // Equation 10  - define q
    q = rk / 13.41 / rke;

    // Equations 11 - CDM transfer function fits
    a1 = pow(46.9 * om_mhsq,0.670) * (1.0 + pow(32.1 * om_mhsq,-0.532));
    a2 = pow(12.0 * om_mhsq,0.424) * (1.0 + pow(45.0 * om_mhsq,-0.582));
    ac = pow(a1,(-fb)) * pow(a2,gsl_pow_int(-(fb),3));

    // Equations 12 - CDM transfer function fits
    b1 = 0.944 / (1.0 + pow(458.0 * om_mhsq,-0.708));
    b2 = pow(0.395 * om_mhsq,-0.0266);
    bc = 1.0 / (1.0 + b1 * (pow(1.0 - fb,b2) - 1.0));

    // Equation 18
    f = 1.0 / (1.0 + gsl_pow_int(rk * s / 5.4,4));

    // Equation 20
    c1 = 14.2 + 386.0 / (1.0 + 69.9 * pow(q,1.08));
    c2 = 14.2 / ac + 386.0 / (1.0 + 69.9 * pow(q,1.08));

    // Equation 17 - CDM transfer function
    q2 = gsl_pow_int(q,2);
    x = log(e + 1.8 * bc * q);
    tc = f * x / (x + c1 * q2) + (1.0 - f) * x / (x + c2 * q2);

    // Equation 15
    y = (1.0 + ze) / (1.0 + zd);
    g = y * (-6.0 * sqrt(1.0 + y) + (2.0 + 3.0 * y) * log((sqrt(1.0 + y) + 1.0) / (sqrt(1.0 + y) - 1.0)));

    // Equation 14
    ab = g * 2.07 * rke * s / pow(1.0 + rd,0.75);

    // Equation 23
    bn = 8.41 * pow(om_mhsq,0.435);

    // Equation 22
    ss = s / pow(1.0 + gsl_pow_int(bn / rk / s,3),1.0 / 3.0);

    // Equation 24
    bb = 0.5 + (fb) + (3.0 - 2.0 * fb) * sqrt(gsl_pow_int(17.2 * om_mhsq,2) + 1.0);

    // Equations 19 & 21
    tb = log(e + 1.8 * q) / (log(e + 1.8 * q) + c1 *q2) / (1.0 + gsl_pow_int(rk * s /5.2,2));
    tb = (tb + ab * exp(-pow(rk / rks,1.4)) / (1.0 + gsl_pow_int(bb / rk / s,3))) * sin(rk * ss) / rk / ss;

    // Equation 8
    if(fabs(fb)>0.) result = fb * tb + (1.0 - fb) * tc;
    else
    {
        FatalError("Try to calculate Baryon Transfer function, but Baryons are switched off.");
    }
    return result;

} //EH_transfer
///-------------------------------------------------------------------------------------
/** @fn EH_spectrum
 * A subroutine for baryon transfer
 */
static double EH_spectrum(const double k, const double params[N_COSMOPAR+1])
{
    double n_s=params[6];
    double result;

    result = pow(k,n_s) * gsl_pow_int(EH_transfer(k,params),2);

    return result;
} //EH_spectrum
///-------------------------------------------------------------------------------------
/** @fn aux_sigma8_EH
 * A subroutine for baryon transfer
 */
static double aux_sigma8_EH(double k, void *params)
{
    double result;
    double *aux=(double *)params;
    double rsmooth=aux[10];
    double window = aux_tophatwindow(rsmooth, k);

    result = EH_spectrum(k,aux) * gsl_pow_int(k * window,2);

    return result;
} //aux_sigma8_EH
///-------------------------------------------------------------------------------------
/** @fn eval_sigma8
 * A subroutine for normalization of power spectrum, cdm only
 */
static double eval_sigma8(const double params[N_COSMOPAR+1], double (*integrand)(double, void*))
{
    double result,aux,error;
    result = 0.;
    gsl_integration_workspace *work = gsl_integration_workspace_alloc(NEVAL);
    gsl_function F;
    F.function = integrand;
    F.params = (void *)params;

    gsl_integration_qagiu(&F,0.0,eps,eps,NEVAL,work,&aux,&error);
    // Fourier convention for the normalization of the power spectrum
    double fac = 1./(2.*M_PI*M_PI);
    result = aux * fac;

    gsl_integration_workspace_free(work);

    return result;
} //eval_sigma8
///-------------------------------------------------------------------------------------
/** @fn fix_spectrum_amplitude
 * The routine for normalization of power spectrum, cdm only
 */
double fix_spectrum_amplitude(const double params[N_COSMOPAR+1], const int WhichSpectrum)
{
    double sig8b;
    switch(WhichSpectrum)
    {
        // power-law power spectrum, k**n_S
        case -1:
        {
            sig8b = eval_sigma8(params, aux_sigma8_PL);
        }
        break;
        // BBKS no-wiggle CDM power spectrum
        case 0:
        {
            sig8b = eval_sigma8(params, aux_sigma8_BBKS);
        }
        break;
        // Eisenstein & Hu power spectrum with baryonic wiggles
        case 1:
        {
            sig8b = eval_sigma8(params, aux_sigma8_EH);
        }
        break;
        default:
            sprintf(G__msg__, "Spectrum type does not exist: %d", WhichSpectrum);
            FatalError(G__msg__);
        break;
    }
    double sigma8 = params[7];
    double Norm = sigma8*sigma8 / sig8b;
    return Norm;
} //fix_spectrum_amplitude
///-------------------------------------------------------------------------------------
/** @fn get_power
 * Routine to get the power spectrum P given a k mode
 */
double get_power(const double k, const double params[N_COSMOPAR+1], const int WhichSpectrum, const double Norm)
{
    // k should be in units h/Mpc
    double P_of_k=0.;
    switch(WhichSpectrum)
    {
        // power-law power spectrum, k**n_S
        case -1:
        {
            P_of_k=Norm*PL_spectrum(k,params);
        }
        break;
        // BBKS no-wiggle CDM power spectrum
        case 0:
        {
            P_of_k=Norm*BBKS_spectrum(k,params);
        }
        break;
        // Eisenstein & Hu power spectrum with baryonic wiggles
        case 1:
        {
            P_of_k=Norm*EH_spectrum(k,params);
        }
        break;
        default:
            sprintf(G__msg__, "Spectrum type does not exist: %d", WhichSpectrum);
            FatalError(G__msg__);
        break;
    }

    return P_of_k;
} //get_power
///-------------------------------------------------------------------------------------
/** @fn setup_Pk
 * Compute the power spectrum in the required k_modes
 * Rescale the power spectrum to starting redshift
 */
void setup_Pk(const int Nk, const float_t k_modes[Nk], const double params[N_COSMOPAR+1], const int WhichSpectrum, float_t powerspectrum[Nk])
{
    /* 1- Compute normalization */
    PrintMessage(4, "Computing normalization of the power spectrum...");

    double sigma8 = params[7];
    const double Norm = fix_spectrum_amplitude(params, WhichSpectrum);
    debugPrintDouble(Norm);
    debugPrintDouble(sigma8);

    PrintMessage(4, "Computing normalization of the power spectrum done.");

    /* 2- Compute power spectrum */
    PrintMessage(4, "Computing power spectrum...");

    #pragma omp parallel for schedule(static)
    for(int ik=0; ik<Nk; ik++)
    {
        // Look up k mode
        double kmod = (double)k_modes[ik];

        // Compute power spectrum and fill table
        double P_of_k = get_power(kmod, params, WhichSpectrum, Norm);
        powerspectrum[ik] = (float_t)P_of_k;

        // Jeans's swindle
        if(kmod<=0.)
            powerspectrum[ik] = (float_t)0.;
    }

    debugPrintFloattArray((float_t *)k_modes,Nk);
    debugPrintFloattArray(powerspectrum,Nk);

    PrintMessage(4, "Computing power spectrum done.");
} //setup_Pk
///-------------------------------------------------------------------------------------
/** @fn initialize_power_spectrum
 * Principal routine to initialize power spectrum:
 * @param InputPowerSpectrum name of the input power spectrum file
 * @return structure containing the power spectrum to be used for the generation of the ICs
 */
PowerSpectrum initialize_power_spectrum(const char *InputPowerSpectrum)
{
    sprintf(G__msg__, "Reading power spectrum...");
    PrintMessage(3, G__msg__);
    INDENT;

    PowerSpectrum Pk = read_PowerSpectrum((char*)InputPowerSpectrum, VARNAME(Pk));
    debugPrintInt(Pk.NUM_MODES);
    debugPrintInt(Pk.N_HC);
    debugPrintFloattArray(Pk.k_modes,Pk.NUM_MODES);
    debugPrintIntArray(Pk.k_nmodes,Pk.NUM_MODES);
    debugPrintIntArray(Pk.k_keys,Pk.N_HC);
    debugPrintFloattArray(Pk.powerspectrum,Pk.NUM_MODES);

    UNINDENT;
    sprintf(G__msg__, "Reading power spectrum done.");
    PrintMessage(3, G__msg__);

    return Pk;
} //initialize_power_spectrum
///-------------------------------------------------------------------------------------
