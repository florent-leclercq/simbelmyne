///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- src/read_param.c
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file read_param.c
 *  \brief Routine for reading the parameter file
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2013-2023
 */
#include "read_param.h"
///-------------------------------------------------------------------------------------
/** @fn hash_key
 * Return hash key value for string s
 */
unsigned hash_key(char *s)
{
    unsigned hashval;
    for(hashval = 0; *s != '\0'; s++)
        hashval = *s + 1999 * hashval;
    return hashval % HASHSIZE;
} //hash_key
///-------------------------------------------------------------------------------------
/** @fn hash_lookup
 * Look for s in hashtab
 */
struct __nlist *hash_lookup(struct __nlist *hashtab[HASHSIZE], char *tag)
{
    struct __nlist *h;
    unsigned int hashval = hash_key(tag);
    for(h = hashtab[hashval]; h != NULL; h = h->next)
        if(strcmp(tag, h->tag) == 0)
            return h; // found
    return NULL; // not found
} //hash_lookup
///-------------------------------------------------------------------------------------
/** @fn hash_set
 * Puts an element in hashtab
 */
void hash_set(struct __nlist *hashtab[HASHSIZE], char *tag, bool usedtag, bool mandatorytag, int id, void *addr)
{
    struct __nlist *h;
    unsigned int hashval;
    if((h = hash_lookup(hashtab, tag)) == NULL)
    { // not found
        h = (struct __nlist *) p_malloc(sizeof(*h), tag);
        if(h != NULL && (h->tag = strdup(tag)) != NULL)
        {
            hashval = hash_key(tag);
            h->next = hashtab[hashval];
            hashtab[hashval] = h;
        }
    }
    else
    { // already there, reset previous values
        h->usedtag = false;
        h->mandatorytag = true;
        h->id = 0;
    }
    h->usedtag = usedtag;
    h->mandatorytag = mandatorytag;
    h->id = id;
    h->addr = addr;
} //hash_set
///-------------------------------------------------------------------------------------
/** @fn hash_set
 * Removes an element in hashtab
 */
void hash_unset(struct __nlist *hashtab[HASHSIZE], char *tag)
{
    struct __nlist *h;
    if((h = hash_lookup(hashtab, tag)) != NULL)
    { // found
        p_free(h, tag);
        h->usedtag = false;
        h->mandatorytag = true;
        h->id = 0;
    }
} //hash_unset
///-------------------------------------------------------------------------------------
/** @fn hash_set_used
 * Sets usedtag=true for an element in hashtab
 */
void hash_set_used(struct __nlist *hashtab[HASHSIZE], char *tag)
{
    if(hash_lookup(hashtab, tag))
        hashtab[hash_key(tag)]->usedtag = true;
} //hash_set_used
///-------------------------------------------------------------------------------------
/** @fn hash_set_mandatory
 * Sets mandatorytag=true for an element in hashtab
 */
void hash_set_mandatory(struct __nlist *hashtab[HASHSIZE], char *tag)
{
    if(hash_lookup(hashtab, tag))
        hashtab[hash_key(tag)]->mandatorytag = true;
} //hash_set_mandatory
///-------------------------------------------------------------------------------------
/** @fn define_mandatory_params
 * Define the list of mandatory parameters
 */
static void define_mandatory_params(struct __nlist *hashtab[HASHSIZE], Param_File Params)
{
    if(Params.ModuleLPT)
    {
        hash_set_mandatory(hashtab, "InputRngStateLPT");
        hash_set_mandatory(hashtab, "OutputRngStateLPT");

        hash_set_mandatory(hashtab, "Particles");
        hash_set_mandatory(hashtab, "Mesh");
        hash_set_mandatory(hashtab, "BoxSize");
        hash_set_mandatory(hashtab, "corner0");
        hash_set_mandatory(hashtab, "corner1");
        hash_set_mandatory(hashtab, "corner2");

        hash_set_mandatory(hashtab, "ICsMode");
        if(Params.ICsMode == 0)
        {
            hash_set_mandatory(hashtab, "WriteICsRngState");
            if(Params.WriteICsRngState)
                hash_set_mandatory(hashtab, "OutputICsRngState");
            hash_set_mandatory(hashtab, "WriteWhiteNoise");
            if(Params.WriteWhiteNoise)
                hash_set_mandatory(hashtab, "OutputWhiteNoise");
        }
        else if(Params.ICsMode == 1)
            hash_set_mandatory(hashtab, "InputWhiteNoise");
        else if(Params.ICsMode == 2)
            hash_set_mandatory(hashtab, "InputInitialConditions");
        hash_set_mandatory(hashtab, "WriteInitialConditions");
        if(Params.WriteInitialConditions)
            hash_set_mandatory(hashtab, "OutputInitialConditions");

        if(Params.ICsMode == 0 || Params.ICsMode == 1)
            hash_set_mandatory(hashtab, "InputPowerSpectrum");

        hash_set_mandatory(hashtab, "RedshiftLPT");
        hash_set_mandatory(hashtab, "WriteLPTSnapshot");
        if(Params.WriteLPTSnapshot)
            hash_set_mandatory(hashtab, "OutputLPTSnapshot");
        hash_set_mandatory(hashtab, "WriteLPTDensity");
        if(Params.WriteLPTDensity)
            hash_set_mandatory(hashtab, "OutputLPTDensity");
    }
    if(Params.ModulePMCOLA)
    {
        hash_set_mandatory(hashtab, "EvolutionMode");
        hash_set_mandatory(hashtab, "ParticleMesh");
        hash_set_mandatory(hashtab, "NumberOfTimeSteps");
        hash_set_mandatory(hashtab, "TimeStepDistribution");

        if(Params.EvolutionMode != 1)
        {
            hash_set_mandatory(hashtab, "ModifiedDiscretization");
            hash_set_mandatory(hashtab, "n_LPT");
        }

        hash_set_mandatory(hashtab, "WriteSnapshots");
        if(Params.WriteSnapshots == 1)
        {
            hash_set_mandatory(hashtab, "OutputSnapshotsBase");
            hash_set_mandatory(hashtab, "OutputSnapshotsExt");
        }
        if(Params.WriteDensities == 1)
        {
            hash_set_mandatory(hashtab, "OutputDensitiesBase");
            hash_set_mandatory(hashtab, "OutputDensitiesExt");
        }

        hash_set_mandatory(hashtab, "RedshiftFCs");
        hash_set_mandatory(hashtab, "WriteFinalSnapshot");
        if(Params.WriteFinalSnapshot)
            hash_set_mandatory(hashtab, "OutputFinalSnapshot");
        hash_set_mandatory(hashtab, "WriteFinalDensity");
        if(Params.WriteFinalDensity)
            hash_set_mandatory(hashtab, "OutputFinalDensity");

        hash_set_mandatory(hashtab, "WriteReferenceFrame");
        if(Params.WriteReferenceFrame)
        {
            hash_set_mandatory(hashtab, "OutputKickBase");
            hash_set_mandatory(hashtab, "OutputKickExt");
            hash_set_mandatory(hashtab, "OutputDriftBase");
            hash_set_mandatory(hashtab, "OutputDriftExt");
        }
        hash_set_mandatory(hashtab, "ReadReferenceFrame");
        if(Params.ReadReferenceFrame)
        {
            hash_set_mandatory(hashtab, "InputKickBase");
            hash_set_mandatory(hashtab, "InputKickExt");
            hash_set_mandatory(hashtab, "InputDriftBase");
            hash_set_mandatory(hashtab, "InputDriftExt");
        }

        if(Params.EvolutionMode == 3)
        {
            hash_set_mandatory(hashtab, "NumberOfTilesPerDimension");
            hash_set_mandatory(hashtab, "NumberOfParticlesInBuffer");
            hash_set_mandatory(hashtab, "OutputLPTPotential1");
            hash_set_mandatory(hashtab, "OutputLPTPotential2");
            hash_set_mandatory(hashtab, "OutputTilesBase");
            hash_set_mandatory(hashtab, "OutputTilesExt");
        }
    }
    if(Params.ModuleRSD)
    {
        hash_set_mandatory(hashtab, "DoNonLinearMapping");
        hash_set_mandatory(hashtab, "vobs0");
        hash_set_mandatory(hashtab, "vobs1");
        hash_set_mandatory(hashtab, "vobs2");
        hash_set_mandatory(hashtab, "alpha1RSD");
        hash_set_mandatory(hashtab, "DoLPTSplit");
        hash_set_mandatory(hashtab, "alpha2RSD");

        hash_set_mandatory(hashtab, "WriteRSSnapshot");
        if(Params.WriteRSSnapshot)
            hash_set_mandatory(hashtab, "OutputRSSnapshot");
        hash_set_mandatory(hashtab, "WriteRSDensity");
        if(Params.WriteRSDensity)
            hash_set_mandatory(hashtab, "OutputRSDensity");
    }
    if(Params.ModuleMocks)
    {
        hash_set_mandatory(hashtab, "InputRngStateMocks");
        hash_set_mandatory(hashtab, "OutputRngStateMocks");
        hash_set_mandatory(hashtab, "WriteMocksRngState");
        hash_set_mandatory(hashtab, "OutputMocksRngState");
        hash_set_mandatory(hashtab, "NoiseModel");
        hash_set_mandatory(hashtab, "NumberOfNoiseRealizations");

        if(Params.ModuleLPT == 0)
            hash_set_mandatory(hashtab, "InputDensityMocks");

        hash_set_mandatory(hashtab, "InputSurveyGeometry");

        hash_set_mandatory(hashtab, "WriteMocks");
        if(Params.WriteMocks)
        {
            hash_set_mandatory(hashtab, "OutputMockBase");
            hash_set_mandatory(hashtab, "OutputMockExt");
        }
        hash_set_mandatory(hashtab, "WriteSummaryStats");
        if(Params.WriteSummaryStats)
        {
            hash_set_mandatory(hashtab, "InputSummaryStatskGrid");
            hash_set_mandatory(hashtab, "OutputSummaryStats");
        }
    }
}
///-------------------------------------------------------------------------------------
/** @fn check_mandatory_params
 * Checks the existence of all mandatory parameters in parameter file
 * @param foundtags list of defined tags
 * @param Ntags number of tags
 * @param *fname parameter file name
 * @param Params structure containing parameters
 */
static void check_mandatory_params(struct __nlist *hashtab[HASHSIZE], bool foundtags[MAXTAGS], char tag[MAXTAGS][TAG], int Ntags, char *fname)
{
    // Check if the parameter file misses any tag
    for(int i = 0; i < Ntags; i++)
    {
        if(!foundtags[i] && hashtab[hash_key(tag[i])]->mandatorytag)
        {
            sprintf(G__msg__, "Missing a value for tag '%.100s' in parameter file '%.3000s'.", tag[i], fname);
            FatalError(G__msg__);
        }
    }
} //check_mandatory_params
///-------------------------------------------------------------------------------------
/** @fn check_params
 * Checks the parameter file
 * @param tag list of tags
 * @param Ntags number of tags
 * @param *fname parameter file name
 * @param Params structure containing parameters
 */
static void check_params(Param_File Params)
{
    if(Params.ICsMode != 0 && Params.ICsMode != 1 && Params.ICsMode!= 2)
    {
        sprintf(G__msg__, "Incorrect value for ICsMode: given %d, expected 0, 1 or 2.", Params.ICsMode);
        FatalError(G__msg__);
    }
    if(Params.ModulePMCOLA && !Params.ModuleLPT)
    {
        sprintf(G__msg__, "ModuleLPT should be switched on for ModulePMCOLA.");
        FatalError(G__msg__);
    }
    if(Params.EvolutionMode != 1 && Params.EvolutionMode !=2 && Params.EvolutionMode != 3)
    {
        sprintf(G__msg__, "Incorrect value for EvolutionMode: given %d, expected 1, 2 or 3.", Params.TimeStepDistribution);
        FatalError(G__msg__);
    }
    if(Params.TimeStepDistribution != 0 && Params.TimeStepDistribution != 1 && Params.TimeStepDistribution != 2)
    {
        sprintf(G__msg__, "Incorrect value for TimeStepDistribution: given %d, expected 0, 1 or 2.", Params.TimeStepDistribution);
        FatalError(G__msg__);
    }
    if(Params.EvolutionMode == 3 && p_mod(Params.Particles, Params.NumberOfTilesPerDimension) != 0)
    {
        sprintf(G__msg__, "Particles size must be a multiple of NumberOfTilesPerDimension for sCOLA, given Particles=%d, NumberOfTilesPerDimension=%d.", Params.Particles, Params.NumberOfTilesPerDimension);
        FatalError(G__msg__);
    }
    if(Params.EvolutionMode == 3 && !(Params.ParticleMesh*Params.NumberOfTilesPerDimension >= Params.Mesh))
    {
        sprintf(G__msg__, "Total particle-mesh size must be larger than mesh size for sCOLA, given Mesh=%d, ParticleMesh*NumberOfTilesPerDimension=%d.", Params.Mesh, Params.ParticleMesh*Params.NumberOfTilesPerDimension);
        FatalError(G__msg__);
    }
    if(Params.ModuleRSD && !Params.ModuleLPT)
    {
        sprintf(G__msg__, "ModuleLPT should be switched on for ModuleRSD.");
        FatalError(G__msg__);
    }
} //check_params
///-------------------------------------------------------------------------------------
/** @fn read_parameterfile
 * Reads the parameter file
 * @param file name of the file to be read
 * @return structure containing all parameters in the file
 */
Param_File read_parameterfile(char *fname)
{
    sprintf(G__msg__, "Reading parameter file in '%s'...", fname);
    PrintMessage(3, G__msg__);
    INDENT;

    struct __nlist **hashtab; /* pointer table */
    hashtab = (struct __nlist **)p_malloc(HASHSIZE*sizeof(struct __nlist *), VARNAME(hashtab));
    for(int h=0; h<HASHSIZE; h++)
        hashtab[h] = NULL;
    Param_File Params;

    FILE *fd;
    char buf[BUFLENGTH], buf1[BUFLENGTH], buf2[BUFLENGTH], buf3[BUFLENGTH];
    int i, j, nt;
    int id[MAXTAGS];
    void *addr[MAXTAGS];
    char tag[MAXTAGS][TAG];
    bool foundtags[MAXTAGS];

    nt = 0;

/* ----------------------------------------------------------------------------------------------------------------------- */
/* Setup ----------------------------------------------------------------------------------------------------------------- */
/* ----------------------------------------------------------------------------------------------------------------------- */
    /* */ strcpy(tag[nt], "SnapFormat");
    addr[nt] = &Params.SnapFormat;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,true,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "NumFilesPerSnapshot");
    addr[nt] = &Params.NumFilesPerSnapshot;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,true,id[nt],addr[nt]); nt++;

/* ----------------------------------------------------------------------------------------------------------------------- */
/* Module LPT ------------------------------------------------------------------------------------------------------------ */
/* ----------------------------------------------------------------------------------------------------------------------- */
    /* */ strcpy(tag[nt], "ModuleLPT");
    addr[nt] = &Params.ModuleLPT;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,true,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "InputRngStateLPT");
    addr[nt] = &Params.InputRngStateLPT;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "OutputRngStateLPT");
    addr[nt] = &Params.OutputRngStateLPT;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;

    /* */ strcpy(tag[nt], "Particles");
    addr[nt] = &Params.Particles;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "Mesh");
    addr[nt] = &Params.Mesh;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "BoxSize");
    addr[nt] = &Params.BoxSize;
    id[nt] = DOUBLE;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "corner0");
    addr[nt] = &Params.corner0;
    id[nt] = DOUBLE;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "corner1");
    addr[nt] = &Params.corner1;
    id[nt] = DOUBLE;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "corner2");
    addr[nt] = &Params.corner2;
    id[nt] = DOUBLE;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;

    /* */ strcpy(tag[nt], "ICsMode");
    addr[nt] = &Params.ICsMode;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "WriteICsRngState");
    addr[nt] = &Params.WriteICsRngState;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "OutputICsRngState");
    addr[nt] = &Params.OutputICsRngState;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "WriteWhiteNoise");
    addr[nt] = &Params.WriteWhiteNoise;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "OutputWhiteNoise");
    addr[nt] = &Params.OutputWhiteNoise;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "InputWhiteNoise");
    addr[nt] = &Params.InputWhiteNoise;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "InputInitialConditions");
    addr[nt] = &Params.InputInitialConditions;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "WriteInitialConditions");
    addr[nt] = &Params.WriteInitialConditions;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "OutputInitialConditions");
    addr[nt] = &Params.OutputInitialConditions;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;

    /* */ strcpy(tag[nt], "InputPowerSpectrum");
    addr[nt] = &Params.InputPowerSpectrum;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;

    /* */ strcpy(tag[nt], "RedshiftLPT");
    addr[nt] = &Params.RedshiftLPT;
    id[nt] = DOUBLE;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "WriteLPTSnapshot");
    addr[nt] = &Params.WriteLPTSnapshot;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "OutputLPTSnapshot");
    addr[nt] = &Params.OutputLPTSnapshot;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "WriteLPTDensity");
    addr[nt] = &Params.WriteLPTDensity;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "OutputLPTDensity");
    addr[nt] = &Params.OutputLPTDensity;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;

/* ----------------------------------------------------------------------------------------------------------------------- */
/* Module PM/COLA -------------------------------------------------------------------------------------------------------- */
/* ----------------------------------------------------------------------------------------------------------------------- */

    /* */ strcpy(tag[nt], "ModulePMCOLA");
    addr[nt] = &Params.ModulePMCOLA;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,true,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "EvolutionMode");
    addr[nt] = &Params.EvolutionMode;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "ParticleMesh");
    addr[nt] = &Params.ParticleMesh;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "NumberOfTimeSteps");
    addr[nt] = &Params.NumberOfTimeSteps;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "TimeStepDistribution");
    addr[nt] = &Params.TimeStepDistribution;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "ModifiedDiscretization");
    addr[nt] = &Params.ModifiedDiscretization;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "n_LPT");
    addr[nt] = &Params.n_LPT;
    id[nt] = DOUBLE;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;

    /* */ strcpy(tag[nt], "WriteSnapshots");
    addr[nt] = &Params.WriteSnapshots;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "OutputSnapshotsBase");
    addr[nt] = &Params.OutputSnapshotsBase;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "OutputSnapshotsExt");
    addr[nt] = &Params.OutputSnapshotsExt;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "WriteDensities");
    addr[nt] = &Params.WriteDensities;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "OutputDensitiesBase");
    addr[nt] = &Params.OutputDensitiesBase;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "OutputDensitiesExt");
    addr[nt] = &Params.OutputDensitiesExt;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;

    /* */ strcpy(tag[nt], "RedshiftFCs");
    addr[nt] = &Params.RedshiftFCs;
    id[nt] = DOUBLE;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "WriteFinalSnapshot");
    addr[nt] = &Params.WriteFinalSnapshot;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "OutputFinalSnapshot");
    addr[nt] = &Params.OutputFinalSnapshot;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "WriteFinalDensity");
    addr[nt] = &Params.WriteFinalDensity;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "OutputFinalDensity");
    addr[nt] = &Params.OutputFinalDensity;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;

    /* */ strcpy(tag[nt], "WriteReferenceFrame");
    addr[nt] = &Params.WriteReferenceFrame;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "OutputKickBase");
    addr[nt] = &Params.OutputKickBase;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "OutputKickExt");
    addr[nt] = &Params.OutputKickExt;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "OutputDriftBase");
    addr[nt] = &Params.OutputDriftBase;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "OutputDriftExt");
    addr[nt] = &Params.OutputDriftExt;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "ReadReferenceFrame");
    addr[nt] = &Params.ReadReferenceFrame;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "InputKickBase");
    addr[nt] = &Params.InputKickBase;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "InputKickExt");
    addr[nt] = &Params.InputKickExt;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "InputDriftBase");
    addr[nt] = &Params.InputDriftBase;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "InputDriftExt");
    addr[nt] = &Params.InputDriftExt;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;

    /* */ strcpy(tag[nt], "NumberOfTilesPerDimension");
    addr[nt] = &Params.NumberOfTilesPerDimension;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "NumberOfParticlesInBuffer");
    addr[nt] = &Params.NumberOfParticlesInBuffer;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "OutputLPTPotential1");
    addr[nt] = &Params.OutputLPTPotential1;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "OutputLPTPotential2");
    addr[nt] = &Params.OutputLPTPotential2;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "OutputTilesBase");
    addr[nt] = &Params.OutputTilesBase;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "OutputTilesExt");
    addr[nt] = &Params.OutputTilesExt;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;

/* ----------------------------------------------------------------------------------------------------------------------- */
/* Module RSD ------------------------------------------------------------------------------------------------------------ */
/* ----------------------------------------------------------------------------------------------------------------------- */
    /* */ strcpy(tag[nt], "ModuleRSD");
    addr[nt] = &Params.ModuleRSD;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,true,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "DoNonLinearMapping");
    addr[nt] = &Params.DoNonLinearMapping;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "vobs0");
    addr[nt] = &Params.vobs0;
    id[nt] = DOUBLE;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "vobs1");
    addr[nt] = &Params.vobs1;
    id[nt] = DOUBLE;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "vobs2");
    addr[nt] = &Params.vobs2;
    id[nt] = DOUBLE;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "alpha1RSD");
    addr[nt] = &Params.alpha1RSD;
    id[nt] = DOUBLE;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "DoLPTSplit");
    addr[nt] = &Params.DoLPTSplit;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "alpha2RSD");
    addr[nt] = &Params.alpha2RSD;
    id[nt] = DOUBLE;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;

    /* */ strcpy(tag[nt], "WriteRSSnapshot");
    addr[nt] = &Params.WriteRSSnapshot;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "OutputRSSnapshot");
    addr[nt] = &Params.OutputRSSnapshot;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "WriteRSDensity");
    addr[nt] = &Params.WriteRSDensity;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "OutputRSDensity");
    addr[nt] = &Params.OutputRSDensity;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;

/* ----------------------------------------------------------------------------------------------------------------------- */
/* Module Mock Catalogs -------------------------------------------------------------------------------------------------- */
/* ----------------------------------------------------------------------------------------------------------------------- */
    /* */ strcpy(tag[nt], "ModuleMocks");
    addr[nt] = &Params.ModuleMocks;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,true,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "InputRngStateMocks");
    addr[nt] = &Params.InputRngStateMocks;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "OutputRngStateMocks");
    addr[nt] = &Params.OutputRngStateMocks;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "WriteMocksRngState");
    addr[nt] = &Params.WriteMocksRngState;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,true,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "OutputMocksRngState");
    addr[nt] = &Params.OutputMocksRngState;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "NoiseModel");
    addr[nt] = &Params.NoiseModel;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "NumberOfNoiseRealizations");
    addr[nt] = &Params.NumberOfNoiseRealizations;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;

    /* */ strcpy(tag[nt], "InputDensityMocks");
    addr[nt] = &Params.InputDensityMocks;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "InputSurveyGeometry");
    addr[nt] = &Params.InputSurveyGeometry;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "InputSummaryStatskGrid");
    addr[nt] = &Params.InputSummaryStatskGrid;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;

    /* */ strcpy(tag[nt], "WriteMocks");
    addr[nt] = &Params.WriteMocks;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "OutputMockBase");
    addr[nt] = &Params.OutputMockBase;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "OutputMockExt");
    addr[nt] = &Params.OutputMockExt;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;

    /* */ strcpy(tag[nt], "WriteSummaryStats");
    addr[nt] = &Params.WriteSummaryStats;
    id[nt] = INT;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "OutputSummaryStats");
    addr[nt] = &Params.OutputSummaryStats;
    id[nt] = STRING;
    hash_set(hashtab,tag[nt],false,false,id[nt],addr[nt]); nt++;

/* ----------------------------------------------------------------------------------------------------------------------- */
/* Cosmological model ---------------------------------------------------------------------------------------------------- */
/* ----------------------------------------------------------------------------------------------------------------------- */
    /* */ strcpy(tag[nt], "h");
    addr[nt] = &Params.h;
    id[nt] = DOUBLE;
    hash_set(hashtab,tag[nt],false,true,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "Omega_r");
    addr[nt] = &Params.Omega_r;
    id[nt] = DOUBLE;
    hash_set(hashtab,tag[nt],false,true,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "Omega_q");
    addr[nt] = &Params.Omega_q;
    id[nt] = DOUBLE;
    hash_set(hashtab,tag[nt],false,true,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "Omega_b");
    addr[nt] = &Params.Omega_b;
    id[nt] = DOUBLE;
    hash_set(hashtab,tag[nt],false,true,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "Omega_m");
    addr[nt] = &Params.Omega_m;
    id[nt] = DOUBLE;
    hash_set(hashtab,tag[nt],false,true,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "Omega_k");
    addr[nt] = &Params.Omega_k;
    id[nt] = DOUBLE;
    hash_set(hashtab,tag[nt],false,true,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "n_s");
    addr[nt] = &Params.n_s;
    id[nt] = DOUBLE;
    hash_set(hashtab,tag[nt],false,true,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "sigma8");
    addr[nt] = &Params.sigma8;
    id[nt] = DOUBLE;
    hash_set(hashtab,tag[nt],false,true,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "w0_fld");
    addr[nt] = &Params.w0_fld;
    id[nt] = DOUBLE;
    hash_set(hashtab,tag[nt],false,true,id[nt],addr[nt]); nt++;
    /* */ strcpy(tag[nt], "wa_fld");
    addr[nt] = &Params.wa_fld;
    id[nt] = DOUBLE;
    hash_set(hashtab,tag[nt],false,true,id[nt],addr[nt]); nt++;

    if((fd = fopen(fname, "r")))
    {
        while(true)
        {
            *buf = 0;
            char *result = fgets(buf, 200, fd);

            if(!result)
            {
                debugPrintDiagnostic("Reached the end of the parameter file.");
                break;
            }

            if(sscanf(buf, "%s%s%s", buf1, buf2, buf3) < 2)
                continue;

            if(buf1[0] == '#') // this is a comment line
                continue;

            for(i = 0, j = -1; i < nt; i++)
                if(strcmp(buf1, tag[i]) == 0)
                {
                    j = i;
                    foundtags[i] = true;
                    if(hashtab[hash_key(buf1)]->usedtag)
                    {
                        sprintf(G__msg__, "in file '%s': Tag '%s' multiply defined.", fname, buf1);
                        FatalError(G__msg__);
                    }
                    sprintf(G__msg__, "Found tag: '%s'.", tag[i]);
                    debugPrintDiagnostic(G__msg__);
                    hash_set_used(hashtab, buf1);
                    break;
                }

            if(j >= 0)
            {
                switch(id[j])
                {
                    case DOUBLE:
                        *((double *) addr[j]) = atof(buf2);
                    break;
                    case STRING:
                        strcpy(addr[j], buf2);
                    break;
                    case INT:
                        *((int *) addr[j]) = atoi(buf2);
                    break;
                }
            }
            else
            {
                sprintf(G__msg__, "in file '%s': Unrecognized tag '%s'.", fname, buf1);
                PrintWarning(G__msg__);
            }

            if(feof(fd))
            {
                debugPrintDiagnostic("Reached the end of the parameter file.");
                break;
            }
        }
        fclose(fd);
    }
    else
    {
        sprintf(G__msg__, "Failed to open parameter file '%s'.", fname);
        FatalError(G__msg__);
    }

    // check consistency
    define_mandatory_params(hashtab, Params);
    check_mandatory_params(hashtab, foundtags, tag, nt, fname);
    check_params(Params);

    #undef DOUBLE
    #undef STRING
    #undef INT
    #undef BOOL
    #undef MAXTAGS

    // free memory
    for(i = 0; i < nt; i++)
        hash_unset(hashtab,tag[i]);
    p_free(hashtab, VARNAME(hashtab));

    UNINDENT;
    sprintf(G__msg__, "Reading parameter file in '%s' done.", fname);
    PrintMessage(3, G__msg__);

    return Params;
} //read_parameterfile
///-------------------------------------------------------------------------------------
