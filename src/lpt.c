///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- src/lpt.c
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file lpt.c
 *  \brief General routines for Lagrangian perturbation theory
 *   and routines for calculation of LPT displacement fields
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2012-2023
 */
#include "lpt.h"
///-------------------------------------------------------------------------------------
/** @fn get_initial_conditions
 * Routine to read or generate the initial density field
 * @param ICsMode initial conditions mode (see documentation)
 * @param *InputRngStateLPT input state of the random number generator
 * @param WriteICsRngState boolean: write state of the random number generator?
 * @param *OutputICsRngState output state of the random number generator (before use)
 * @param *OutputRngStateLPT output state of the random number generator (after use)
 * @param *InputWhiteNoise input white noise field
 * @param WriteWhiteNoise boolean: write white noise field?
 * @param *OutputWhiteNoise output white noise field
 * @param *InputPowerSpectrum input power spectrum
 * @param *InputInitialConditions input initial conditions
 * @param WriteInitialConditions boolean: write initial conditions?
 * @param *OutputInitialConditions output initial conditions
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param corner0 x-position of the corner
 * @param corner1 y-position of the corner
 * @param corner2 z-position of the corner
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 * @return the initial density field
 */
Field get_initial_conditions(const int ICsMode, const char *InputRngStateLPT, const int WriteICsRngState, const char *OutputICsRngState, const char *OutputRngStateLPT, const char *InputWhiteNoise, const int WriteWhiteNoise, const char *OutputWhiteNoise, const char *InputPowerSpectrum, const char *InputInitialConditions, const int WriteInitialConditions, const char *OutputInitialConditions, const int N0, const int N1, const int N2, const double corner0, const double corner1, const double corner2, const double L0, const double L1, const double L2)
{
    Field ICs_field, WN_field;

    switch(ICsMode)
    {
        case 0: /// cases 0 and 1 : initial conditions from white noise
        case 1:
        {
            /* 1- Read or generate the white noise field */
            switch(ICsMode)
            {
                case 0: /// case 0 : generate white noise
                {
                    // Set a random generator
                    gsl_rng *random_generator;
                    random_generator = gsl_rng_alloc(gsl_rng_ranlxd2);
                    set_rng(random_generator, InputRngStateLPT);

                    // Save the random number generator state before doing anything
                    if(WriteICsRngState)
                        save_rng_state(random_generator, OutputICsRngState);

                    // Generate the seed table
                    uint64_t *seedtable;
                    int Nthreads = omp_get_max_threads();
                    seedtable = generate_seed_table(random_generator, Nthreads, VARNAME(seedtable));

                    // Save the random number generator state
                    save_rng_state(random_generator, OutputRngStateLPT);

                    // Free the random generator
                    gsl_rng_free(random_generator);

                    // Allocate white noise field
                    WN_field = allocate_scalar_field(N0, N1, N2, corner0, corner1, corner2, L0, L1, L2, a_ref, VARNAME(WN_field));

                    // Generate white noise
                    generate_white_noise_array(WN_field.data, N0, N1, N2, seedtable, Nthreads);

                    // Write white noise
                    if(WriteWhiteNoise)
                        write_field(WN_field, OutputWhiteNoise);

                    // Free seed table
                    p_free(seedtable, VARNAME(seedtable));
                }
                break;
                case 1: /// case 1 : read external white noise
                {
                    WN_field = read_field(InputWhiteNoise, VARNAME(WN_field));
                    if(N0!=WN_field.N0 || N1!=WN_field.N1 || N2!=WN_field.N2)
                    {
                        sprintf(G__msg__, "Dimensions of input field must match parameter file. Read N0=%d, N1=%d, N2=%d, expected N0=%d, N1=%d, N2=%d.", WN_field.N0, WN_field.N1, WN_field.N2, N0, N1, N2);
                        FatalError(G__msg__);
                    }
                    if(corner0!=WN_field.corner0 || corner1!=WN_field.corner1 || corner2!=WN_field.corner2)
                    {
                        sprintf(G__msg__, "Corners of input field must match parameter file. Read corner0=%g, corner1=%g, corner2=%g, expected corner0=%g, corner1=%g, corner2=%g.", WN_field.corner0, WN_field.corner1, WN_field.corner2, corner0, corner1, corner2);
                        FatalError(G__msg__);
                    }
                }
                break;
            } // end generate white noise

            /* 2- Generate the initial density field from the white noise field */
            // Read power spectrum
            PowerSpectrum Pk = initialize_power_spectrum(InputPowerSpectrum);

            // Tint the white noise field
            if(N0!=Pk.N0 || N1!=Pk.N1 || N2!=Pk.N2)
            {
                sprintf(G__msg__, "Dimensions of field must match input power spectrum. Field: N0=%d, N1=%d, N2=%d, Power spectrum: N0=%d, N1=%d, N2=%d.", Pk.N0, Pk.N1, Pk.N2, N0, N1, N2);
                FatalError(G__msg__);
            }
            const bool tint=true;
            tint_or_whiten_GRF(tint, WN_field.data, Pk.k_keys, Pk.powerspectrum, Pk.NUM_MODES, Pk.N0, Pk.N1, Pk.N2, L0, L1, L2);
            ICs_field = scalar_field_from_data(WN_field.data, N0, N1, N2, corner0, corner1, corner2, L0, L1, L2, a_ref);

            // Free power spectrum
            free_PowerSpectrum(Pk);
        }
        break;
        case 2: /// case 2 : read external initial conditions
        {
            ICs_field = read_field(InputInitialConditions, VARNAME(ICs_field));
            if(N0!=ICs_field.N0 || N1!=ICs_field.N1 || N2!=ICs_field.N2)
            {
                sprintf(G__msg__, "Dimensions of input field must match parameter file. Read N0=%d, N1=%d, N2=%d, expected N0=%d, N1=%d, N2=%d.", ICs_field.N0, ICs_field.N1, ICs_field.N2, N0, N1, N2);
                FatalError(G__msg__);
            }
            if(corner0!=ICs_field.corner0 || corner1!=ICs_field.corner1 || corner2!=ICs_field.corner2)
            {
                sprintf(G__msg__, "Corners of input field must match parameter file. Read corner0=%g, corner1=%g, corner2=%g, expected corner0=%g, corner1=%g, corner2=%g.", ICs_field.corner0, ICs_field.corner1, ICs_field.corner2, corner0, corner1, corner2);
                FatalError(G__msg__);
            }
        }
        break;
        default:
            FatalError("ICsMode does not exist");
        break;
    } // end switch(ICsMode)

    /* Write the initial density field (scaled at redshift 0) */
    if(WriteInitialConditions == 1)
    {
        write_field(ICs_field, OutputInitialConditions);
    }

    return ICs_field;
} //get_initial_conditions
///-------------------------------------------------------------------------------------
/** @fn get_phi2_source
 * Subroutine to compute the source of the second order Lagrangian potential
 */
static inline void get_phi2_source(float_t *phi2, float_t *PHI00, float_t *PHI11, float_t *PHI22, float_t *PHI01, float_t *PHI02, float_t *PHI12, const int N0, const int N1, const int N2)
{
    #pragma omp parallel for schedule(static) collapse(3)
    for(int i=0; i<N0; i++)
        for(int j=0; j<N1; j++)
            for(int k=0; k<N2; k++)
            {
                cellIndex_t mc = get_index(i,j,k,N0,N1,N2);
                phi2[mc] = PHI00[mc]*PHI11[mc] + PHI00[mc]*PHI22[mc] + PHI11[mc]*PHI22[mc] - PHI01[mc]*PHI01[mc] - PHI02[mc]*PHI02[mc] - PHI12[mc]*PHI12[mc];
            }
} //get_phi2_source
///-------------------------------------------------------------------------------------
/** @fn get_potentials_periodic
 * Routine to compute the first and second order Lagrangian potentials
 * Assumes periodic boundary conditions
 * @param *DELTA input initial density contrast on the grid
 * @param *phi1 output first Lagrangian potential on the grid
 * @param *phi2 output second Lagrangian potential on the grid
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 */
void get_potentials_periodic(const float_t *DELTA, float_t *phi1, float_t *phi2, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2)
{
    int Nthreads = omp_get_max_threads();
    sprintf(G__msg__, "Computing Lagrangian potentials, periodic boundary conditions (using %d cores)...", Nthreads);
    PrintMessage(3, G__msg__);
    INDENT;

    const cellIndex_t N=N0*N1*N2;
    const bool smooth=false;
    #ifndef ONLY_ZA
    const int N2_HC=N2/2+1;
    const cellIndex_t N_HC=N0*N1*N2_HC;
    #else
    UNUSED(phi2);
    #endif

    // copy the initial density field in order not to overwrite
    #pragma omp parallel for schedule(static)
    for(cellIndex_t mc=0; mc<N; mc++)
        phi1[mc] = DELTA[mc];

    // compute the first order (Zel'dovich) potential phi1 by solving the Poisson equation (equation (B.69) in F.Leclercq PhD Thesis, 2015)
    #ifndef ONLY_ZA
    fftw_complex_t *phi1_fs; phi1_fs = (fftw_complex_t *)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(phi1_fs));
    fftw_complex_t *AUX; AUX = (fftw_complex_t *)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(AUX));

    // transform input field to Fourier space
    FFT_r2c_3d(N0,N1,N2,phi1,AUX,Nthreads);

    // mutiply by discrete Green function in Fourier space
    #pragma omp parallel for schedule(static) collapse(3)
    for(int mf0=0; mf0<N0; mf0++)
        for(int mf1=0; mf1<N1; mf1++)
            for(int mf2=0; mf2<N2_HC; mf2++)
            {
                // multiply by discrete Green's function
                fourierMode_t mf = get_FourierMode_Id(mf0,mf1,mf2,N0,N1,N2);
                AUX[mf] *= dgf_laplace_DFT(mf0,mf1,mf2, N0,N1,N2, L0,L1,L2, smooth, 1.);

                // save potential in Fourier space
                phi1_fs[mf] = AUX[mf];
            }

    // transform back to real space
    FFT_c2r_3d(N0,N1,N2,AUX,phi1,Nthreads);

    // free memory
    p_fftw_free(AUX, VARNAME(AUX));
    #else
    solve_poisson_DFT(phi1,N0,N1,N2,L0,L1,L2,smooth);
    #endif

    debugPrintFloattArray(phi1,N);

    // calculate its second order derivatives (equation (B.70) in F.Leclercq PhD Thesis, 2015)
    #ifndef ONLY_ZA
    float_t *PHI00, *PHI11, *PHI22, *PHI01, *PHI02, *PHI12;
    PHI00 = (float_t *)p_malloc(N*sizeof(float_t), VARNAME(PHI00));
    PHI11 = (float_t *)p_malloc(N*sizeof(float_t), VARNAME(PHI11));
    PHI22 = (float_t *)p_malloc(N*sizeof(float_t), VARNAME(PHI22));
    PHI01 = (float_t *)p_malloc(N*sizeof(float_t), VARNAME(PHI01));
    PHI02 = (float_t *)p_malloc(N*sizeof(float_t), VARNAME(PHI02));
    PHI12 = (float_t *)p_malloc(N*sizeof(float_t), VARNAME(PHI12));

    const bool do2ndderivativeDFT = 1;
    if(do2ndderivativeDFT)
    {
        get_2ndderivative_potential_rs_DFT(phi1_fs,PHI00,N0,N1,N2,L0,L1,L2,0,0); // (0,0) component
        get_2ndderivative_potential_rs_DFT(phi1_fs,PHI11,N0,N1,N2,L0,L1,L2,1,1); // (1,1) component
        get_2ndderivative_potential_rs_DFT(phi1_fs,PHI22,N0,N1,N2,L0,L1,L2,2,2); // (2,2) component
        get_2ndderivative_potential_rs_DFT(phi1_fs,PHI01,N0,N1,N2,L0,L1,L2,0,1); // (0,1) component
        get_2ndderivative_potential_rs_DFT(phi1_fs,PHI02,N0,N1,N2,L0,L1,L2,0,2); // (0,2) component
        get_2ndderivative_potential_rs_DFT(phi1_fs,PHI12,N0,N1,N2,L0,L1,L2,1,2); // (1,2) component
    }
    else
    {
        float_t *PHI0, *PHI1, *PHI2;
        PHI0 = (float_t *)p_malloc(N*sizeof(float_t), VARNAME(PHI0));
        PHI1 = (float_t *)p_malloc(N*sizeof(float_t), VARNAME(PHI1));
        PHI2 = (float_t *)p_malloc(N*sizeof(float_t), VARNAME(PHI2));

        const int boundary_conditions = 1; // periodic boundary conditions
        const double d0=L0/(double)N0;
        const double d1=L1/(double)N1;
        const double d2=L2/(double)N2;
        #pragma omp parallel for schedule(static) collapse(3)
        for(int mc0=0; mc0<N0; mc0++)
            for(int mc1=0; mc1<N1; mc1++)
                for(int mc2=0; mc2<N2; mc2++)
                {
                    PHI0[get_index(mc0,mc1,mc2,N0,N1,N2)] = gradient_rs_1d(phi1,0,mc0,mc1,mc2,N0,N1,N2,d0,d1,d2,boundary_conditions,0,0,0); // 0 gradient
                    PHI1[get_index(mc0,mc1,mc2,N0,N1,N2)] = gradient_rs_1d(phi1,1,mc0,mc1,mc2,N0,N1,N2,d0,d1,d2,boundary_conditions,0,0,0); // 1 gradient
                    PHI2[get_index(mc0,mc1,mc2,N0,N1,N2)] = gradient_rs_1d(phi1,2,mc0,mc1,mc2,N0,N1,N2,d0,d1,d2,boundary_conditions,0,0,0); // 2 gradient
                } // end loop on grid cells
        #pragma omp parallel for schedule(static) collapse(3)
        for(int mc0=0; mc0<N0; mc0++)
            for(int mc1=0; mc1<N1; mc1++)
                for(int mc2=0; mc2<N2; mc2++)
                {
                    PHI00[get_index(mc0,mc1,mc2,N0,N1,N2)] = gradient_rs_1d(PHI0,0,mc0,mc1,mc2,N0,N1,N2,d0,d1,d2,boundary_conditions,0,0,0); // (0,0) component
                    PHI11[get_index(mc0,mc1,mc2,N0,N1,N2)] = gradient_rs_1d(PHI1,1,mc0,mc1,mc2,N0,N1,N2,d0,d1,d2,boundary_conditions,0,0,0); // (1,1) component
                    PHI22[get_index(mc0,mc1,mc2,N0,N1,N2)] = gradient_rs_1d(PHI2,2,mc0,mc1,mc2,N0,N1,N2,d0,d1,d2,boundary_conditions,0,0,0); // (2,2) component
                    PHI01[get_index(mc0,mc1,mc2,N0,N1,N2)] = gradient_rs_1d(PHI0,1,mc0,mc1,mc2,N0,N1,N2,d0,d1,d2,boundary_conditions,0,0,0); // (0,1) component
                    PHI02[get_index(mc0,mc1,mc2,N0,N1,N2)] = gradient_rs_1d(PHI0,2,mc0,mc1,mc2,N0,N1,N2,d0,d1,d2,boundary_conditions,0,0,0); // (0,2) component
                    PHI12[get_index(mc0,mc1,mc2,N0,N1,N2)] = gradient_rs_1d(PHI1,2,mc0,mc1,mc2,N0,N1,N2,d0,d1,d2,boundary_conditions,0,0,0); // (1,2) component
                } // end loop on grid cells

        p_free(PHI0, VARNAME(PHI0));
        p_free(PHI1, VARNAME(PHI1));
        p_free(PHI2, VARNAME(PHI2));
    }

    p_fftw_free(phi1_fs, VARNAME(phi1_fs));

    debugPrintFloattArray(PHI00,N);
    debugPrintFloattArray(PHI11,N);
    debugPrintFloattArray(PHI22,N);
    debugPrintFloattArray(PHI01,N);
    debugPrintFloattArray(PHI02,N);
    debugPrintFloattArray(PHI12,N);

    // compute the source term for phi2 in configuration space (equation (B.71) in F.Leclercq PhD Thesis, 2015)
    get_phi2_source(phi2,PHI00,PHI11,PHI22,PHI01,PHI02,PHI12,N0,N1,N2);

    debugPrintFloattArray(phi2,N);

    p_free(PHI00, VARNAME(PHI00));
    p_free(PHI11, VARNAME(PHI11));
    p_free(PHI22, VARNAME(PHI22));
    p_free(PHI01, VARNAME(PHI01));
    p_free(PHI02, VARNAME(PHI02));
    p_free(PHI12, VARNAME(PHI12));

    // solve Poisson equation for 2LPT potential (equation (B.72) in F.Leclercq PhD Thesis, 2015)
    solve_poisson_DFT(phi2,N0,N1,N2,L0,L1,L2,smooth);

    debugPrintFloattArray(phi2,N);
    #endif

    UNINDENT;
    sprintf(G__msg__, "Computing Lagrangian potentials, periodic boundary conditions (using %d cores) done.", Nthreads);
    PrintMessage(3, G__msg__);
} //get_potentials_periodic
///-------------------------------------------------------------------------------------
/** @fn get_potentials_zerobc
 * Routine to compute the first and second order Lagrangian potentials
 * Assumes zero boundary conditions
 * @param *DELTA input initial density contrast on the grid
 * @param *phi1 output first Lagrangian potential on the grid
 * @param *phi2 output second Lagrangian potential on the grid
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 */
void get_potentials_zerobc(const float_t *DELTA, float_t *phi1, float_t *phi2, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2)
{
    int Nthreads = omp_get_max_threads();
    sprintf(G__msg__, "Computing Lagrangian potentials, zero boundary conditions (using %d cores)...", Nthreads);
    PrintMessage(3, G__msg__);
    INDENT;

    const cellIndex_t N=N0*N1*N2;
    const bool smooth=false;
    #ifdef ONLY_ZA
    UNUSED(phi2);
    #endif

    // copy the initial density field in order not to overwrite
    #pragma omp parallel for schedule(static)
    for(cellIndex_t mc=0; mc<N; mc++)
        phi1[mc] = DELTA[mc];

    // compute the first order (Zel'dovich) potential phi1 by solving the Poisson equation (equation (B.69) in F.Leclercq PhD Thesis, 2015)
    #ifndef ONLY_ZA
    float_t phi1_fs[N], AUX[N];

    // transform input field to sine space (triple forward DST-I transform)
    FFT_forward_r2r_3d(N0,N1,N2,phi1,AUX,FFTW_RODFT00,FFTW_RODFT00,FFTW_RODFT00,Nthreads);

    // mutiply by the Green function in sine space
    #pragma omp parallel for schedule(static) collapse(3)
    for(int l=0; l<N0; l++)
        for(int m=0; m<N1; m++)
            for(int n=0; n<N2; n++)
            {
                // multiply by discrete Green's function
                fourierMode_t mf = get_index(l,m,n,N0,N1,N2);
                AUX[mf] *= dgf_laplace_RODFT00(l,m,n, N0,N1,N2, L0,L1,L2, smooth, 1.);

                // save potential in sine space
                phi1_fs[mf] = AUX[mf];
            }

    // transform back to real space (triple backward DST-I transform)
    FFT_backward_r2r_3d(N0,N1,N2,AUX,phi1,FFTW_RODFT00,FFTW_RODFT00,FFTW_RODFT00,Nthreads);
    #else
    solve_poisson_RODFT00(phi1,N0,N1,N2,L0,L1,L2,smooth);
    #endif

    debugPrintFloattArray(phi1,N);

    // calculate its second order derivatives (equation (B.70) in F.Leclercq PhD Thesis, 2015)
    #ifndef ONLY_ZA
    float_t *PHI00, *PHI11, *PHI22, *PHI01, *PHI02, *PHI12;
    PHI00 = (float_t *)p_malloc(N*sizeof(float_t), VARNAME(PHI00));
    PHI11 = (float_t *)p_malloc(N*sizeof(float_t), VARNAME(PHI11));
    PHI22 = (float_t *)p_malloc(N*sizeof(float_t), VARNAME(PHI22));
    PHI01 = (float_t *)p_malloc(N*sizeof(float_t), VARNAME(PHI01));
    PHI02 = (float_t *)p_malloc(N*sizeof(float_t), VARNAME(PHI02));
    PHI12 = (float_t *)p_malloc(N*sizeof(float_t), VARNAME(PHI12));

    const bool do2ndderivativeRODFT00 = 1;
    if(do2ndderivativeRODFT00)
    {
        get_2ndderivative_potential_rs_RODFT00(phi1_fs,PHI00,N0,N1,N2,L0,L1,L2,0,0); // (0,0) component
        get_2ndderivative_potential_rs_RODFT00(phi1_fs,PHI11,N0,N1,N2,L0,L1,L2,1,1); // (1,1) component
        get_2ndderivative_potential_rs_RODFT00(phi1_fs,PHI22,N0,N1,N2,L0,L1,L2,2,2); // (2,2) component
        get_2ndderivative_potential_rs_RODFT00(phi1_fs,PHI01,N0,N1,N2,L0,L1,L2,0,1); // (0,1) component
        get_2ndderivative_potential_rs_RODFT00(phi1_fs,PHI02,N0,N1,N2,L0,L1,L2,0,2); // (0,2) component
        get_2ndderivative_potential_rs_RODFT00(phi1_fs,PHI12,N0,N1,N2,L0,L1,L2,1,2); // (1,2) component
    }
    else
    {
        float_t *PHI0, *PHI1, *PHI2;
        PHI0 = (float_t *)p_malloc(N*sizeof(float_t), VARNAME(PHI0));
        PHI1 = (float_t *)p_malloc(N*sizeof(float_t), VARNAME(PHI1));
        PHI2 = (float_t *)p_malloc(N*sizeof(float_t), VARNAME(PHI2));

        const int boundary_conditions = 2; // zero boundary conditions
        const double d0=L0/(double)N0;
        const double d1=L1/(double)N1;
        const double d2=L2/(double)N2;
        #pragma omp parallel for schedule(static) collapse(3)
        for(int mc0=0; mc0<N0; mc0++)
            for(int mc1=0; mc1<N1; mc1++)
                for(int mc2=0; mc2<N2; mc2++)
                {
                    PHI0[get_index(mc0,mc1,mc2,N0,N1,N2)] = gradient_rs_1d(phi1,0,mc0,mc1,mc2,N0,N1,N2,d0,d1,d2,boundary_conditions,0,0,0); // 0 gradient
                    PHI1[get_index(mc0,mc1,mc2,N0,N1,N2)] = gradient_rs_1d(phi1,1,mc0,mc1,mc2,N0,N1,N2,d0,d1,d2,boundary_conditions,0,0,0); // 1 gradient
                    PHI2[get_index(mc0,mc1,mc2,N0,N1,N2)] = gradient_rs_1d(phi1,2,mc0,mc1,mc2,N0,N1,N2,d0,d1,d2,boundary_conditions,0,0,0); // 2 gradient
                } // end loop on grid cells
        #pragma omp parallel for schedule(static) collapse(3)
        for(int mc0=0; mc0<N0; mc0++)
            for(int mc1=0; mc1<N1; mc1++)
                for(int mc2=0; mc2<N2; mc2++)
                {
                    PHI00[get_index(mc0,mc1,mc2,N0,N1,N2)] = gradient_rs_1d(PHI0,0,mc0,mc1,mc2,N0,N1,N2,d0,d1,d2,boundary_conditions,0,0,0); // (0,0) component
                    PHI11[get_index(mc0,mc1,mc2,N0,N1,N2)] = gradient_rs_1d(PHI1,1,mc0,mc1,mc2,N0,N1,N2,d0,d1,d2,boundary_conditions,0,0,0); // (1,1) component
                    PHI22[get_index(mc0,mc1,mc2,N0,N1,N2)] = gradient_rs_1d(PHI2,2,mc0,mc1,mc2,N0,N1,N2,d0,d1,d2,boundary_conditions,0,0,0); // (2,2) component
                    PHI01[get_index(mc0,mc1,mc2,N0,N1,N2)] = gradient_rs_1d(PHI0,1,mc0,mc1,mc2,N0,N1,N2,d0,d1,d2,boundary_conditions,0,0,0); // (0,1) component
                    PHI02[get_index(mc0,mc1,mc2,N0,N1,N2)] = gradient_rs_1d(PHI0,2,mc0,mc1,mc2,N0,N1,N2,d0,d1,d2,boundary_conditions,0,0,0); // (0,2) component
                    PHI12[get_index(mc0,mc1,mc2,N0,N1,N2)] = gradient_rs_1d(PHI1,2,mc0,mc1,mc2,N0,N1,N2,d0,d1,d2,boundary_conditions,0,0,0); // (1,2) component
                } // end loop on grid cells

        p_free(PHI0, VARNAME(PHI0));
        p_free(PHI1, VARNAME(PHI1));
        p_free(PHI2, VARNAME(PHI2));
    }

    debugPrintFloattArray(PHI00,N);
    debugPrintFloattArray(PHI11,N);
    debugPrintFloattArray(PHI22,N);
    debugPrintFloattArray(PHI01,N);
    debugPrintFloattArray(PHI02,N);
    debugPrintFloattArray(PHI12,N);

    // compute the source term for phi2 in configuration space (equation (B.71) in F.Leclercq PhD Thesis, 2015)
    get_phi2_source(phi2,PHI00,PHI11,PHI22,PHI01,PHI02,PHI12,N0,N1,N2);

    debugPrintFloattArray(phi2,N);

    p_free(PHI00, VARNAME(PHI00));
    p_free(PHI11, VARNAME(PHI11));
    p_free(PHI22, VARNAME(PHI22));
    p_free(PHI01, VARNAME(PHI01));
    p_free(PHI02, VARNAME(PHI02));
    p_free(PHI12, VARNAME(PHI12));

    // solve Poisson equation for 2LPT potential (equation (B.72) in F.Leclercq PhD Thesis, 2015)
    solve_poisson_RODFT00(phi2,N0,N1,N2,L0,L1,L2,smooth);

    debugPrintFloattArray(phi2,N);
    #endif

    UNINDENT;
    sprintf(G__msg__, "Computing Lagrangian potentials, zero boundary conditions (using %d cores) done.", Nthreads);
    PrintMessage(3, G__msg__);
} //get_potentials_zerobc
///-------------------------------------------------------------------------------------
/** @fn compute_LPT_displacements
 * This routine computes the LPT displacement field for
 * particles, first and second order terms
 * @param Np number of particles
 * @param PsiLPT output LPT displacement field
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 * @param Np0 particle grid size x
 * @param Np1 particle grid size y
 * @param Np2 particle grid size z
 * @param Nphi0_pad number of additional cells to the West and East
 * @param Nphi1_pad number of additional cells to the South and North
 * @param Nphi2_pad number of additional cells to the Bottom and Top
 * @param phi1_pad[N0+2*Nphi0_pad][N1+2*Nphi1_pad][N2+2*Nphi2_pad] input first Lagrangian potential
 * @param phi2_pad[N0+2*Nphi0_pad][N1+2*Nphi1_pad][N2+2*Nphi2_pad] input second Lagrangian potential
 */
void compute_LPT_displacements(const particleID_t Np, particle_lpt_t PsiLPT[Np], const int N0, const int N1, const int N2, const double L0, const double L1, const double L2, const int Np0, const int Np1, const int Np2, const int boundary_conditions, const int Nphi0_pad, const int Nphi1_pad, const int Nphi2_pad, float_t phi1_pad[(N0+2*Nphi0_pad)*(N1+2*Nphi1_pad)*(N2+2*Nphi2_pad)], float_t phi2_pad[(N0+2*Nphi0_pad)*(N1+2*Nphi1_pad)*(N2+2*Nphi2_pad)], const int Ng0_pad, const int Ng1_pad, const int Ng2_pad)
{
    int Nthreads = omp_get_max_threads();
    sprintf(G__msg__, "Computing Lagrangian displacement field (using %d cores)...", Nthreads);
    PrintMessage(3, G__msg__);
    INDENT;

    const int Ng_pad=(N0+2*Ng0_pad)*(N1+2*Ng1_pad)*(N2+2*Ng2_pad);
    const double dp0=L0/(double)Np0;
    const double dp1=L1/(double)Np1;
    const double dp2=L2/(double)Np2;
    const double d0=L0/(double)N0;
    const double d1=L1/(double)N1;
    const double d2=L2/(double)N2;

    #ifdef ONLY_ZA
    UNUSED(phi2_pad);
    #endif

    // assign LPT displacements to particles
    for(int d=0; d<3; d++)
    {
        // allocate memory for (padded) LPT displacements
        float_t *Psi1_pad, *Psi2_pad;
        Psi1_pad = (float_t *)p_malloc(Ng_pad*sizeof(float_t), VARNAME(Psi1_pad));
        #ifndef ONLY_ZA
        Psi2_pad = (float_t *)p_malloc(Ng_pad*sizeof(float_t), VARNAME(Psi2_pad));
        #else
        UNUSED(Psi2_pad);
        #endif

        // get LPT displacements on the grid
        #pragma omp parallel for schedule(static) collapse(3)
        for(int mc0=-Ng0_pad; mc0<N0+Ng0_pad; mc0++)
            for(int mc1=-Ng1_pad; mc1<N1+Ng1_pad; mc1++)
                for(int mc2=-Ng2_pad; mc2<N2+Ng2_pad; mc2++)
                {
                    const cellIndex_t mc = get_index_in_paddedbox(mc0,mc1,mc2,N0,N1,N2,Ng0_pad,Ng1_pad,Ng2_pad);
                    // ZA displacement vector
                    Psi1_pad[mc] = gradient_rs_1d(phi1_pad,d,mc0,mc1,mc2,N0,N1,N2,d0,d1,d2,boundary_conditions,Nphi0_pad,Nphi1_pad,Nphi2_pad);

                    // 2LPT displacement vector
                    #ifndef ONLY_ZA
                    Psi2_pad[mc] = gradient_rs_1d(phi2_pad,d,mc0,mc1,mc2,N0,N1,N2,d0,d1,d2,boundary_conditions,Nphi0_pad,Nphi1_pad,Nphi2_pad);
                    #endif
                } // end loop on grid cells

        sprintf(G__msg__,"Psi1_%d",d);
        debugPrintFloattArray_(G__msg__,Psi1_pad,Ng_pad);
        #ifndef ONLY_ZA
        sprintf(G__msg__,"Psi2_%d",d);
        debugPrintFloattArray_(G__msg__,Psi2_pad,Ng_pad);
        #endif

        // interpolate to get displacements at the particles' positions
        #pragma omp parallel for schedule(static) collapse(3)
        for(int mp0=0; mp0<Np0; mp0++)
            for(int mp1=0; mp1<Np1; mp1++)
                for(int mp2=0; mp2<Np2; mp2++)
                {
                    // sort particles on equidistant grid
                    // this should correspond to the generation of the initial snapshot!
                    const float_t q0 = mp0*dp0;
                    const float_t q1 = mp1*dp1;
                    const float_t q2 = mp2*dp2;
                    const particleID_t mp = get_Lagrangian_Id(mp0,mp1,mp2,Np0,Np1,Np2);

                    // ZA displacement vector
                    PsiLPT[mp].Psi1[d] = mesh_to_particle(Psi1_pad,N0,N1,N2,d0,d1,d2,q0,q1,q2,1.,boundary_conditions,Ng0_pad,Ng1_pad,Ng2_pad);

                    // 2LPT displacement vector
                    #ifndef ONLY_ZA
                    PsiLPT[mp].Psi2[d] = mesh_to_particle(Psi2_pad,N0,N1,N2,d0,d1,d2,q0,q1,q2,1.,boundary_conditions,Ng0_pad,Ng1_pad,Ng2_pad);
                    #endif
                } // end loop on particles

        // free memory
        p_free(Psi1_pad, VARNAME(Psi1_pad));
        #ifndef ONLY_ZA
        p_free(Psi2_pad, VARNAME(Psi2_pad));
        #endif
    } // end loop on axes

    UNINDENT;
    sprintf(G__msg__, "Computing Lagrangian displacement field (using %d cores) done.", Nthreads);
    PrintMessage(3, G__msg__);
} //compute_LPT_displacements
///-------------------------------------------------------------------------------------
/** @fn evolve_lpt
 * Principal routine to evolve particles forward
 * with Lagrangian perturbation theory
 * @param *P input snapshot
 * @param ICs_field input initial conditions field
 * @param Np number of particles
 * @param PsiLPT output displacement field of particles
 * @param RedshiftLPT final LPT redshift
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 * @param Np0 particle grid size x
 * @param Np1 particle grid size y
 * @param Np2 particle grid size z
 * @param cosmo cosmological parameters
 */
void evolve_lpt(Snapshot *P, Field ICs_field, const particleID_t Np, particle_lpt_t PsiLPT[Np], const double RedshiftLPT, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2, const int Np0, const int Np1, const int Np2, const double cosmo[N_COSMOPAR])
{
    const int boundary_conditions = 1; // periodic boundary conditions
    const cellIndex_t N=N0*N1*N2;
    const int Nphi0_pad=0, Nphi1_pad=0, Nphi2_pad=0;
    const int Ng0_pad=0, Ng1_pad=0, Ng2_pad=0;

    // allocate memory for LPT potentials
    float_t *phi1, *phi2;
    phi1 = (float_t *)p_malloc(N*sizeof(float_t), VARNAME(phi1));
    #ifndef ONLY_ZA
    phi2 = (float_t *)p_malloc(N*sizeof(float_t), VARNAME(phi2));
    #else
    phi2 = NULL;
    #endif

    // get the LPT potentials
    switch(boundary_conditions)
    {
        default:
        case 1: // periodic boundary conditions
            get_potentials_periodic(ICs_field.data,phi1,phi2,N0,N1,N2,L0,L1,L2);
        break;
        case 2: // zero-potential boundary conditions
            get_potentials_zerobc(ICs_field.data,phi1,phi2,N0,N1,N2,L0,L1,L2);
        break;
    }

    // compute the LPT displacement fields
    compute_LPT_displacements(Np,PsiLPT, N0,N1,N2, L0,L1,L2, Np0,Np1,Np2, boundary_conditions, Nphi0_pad,Nphi1_pad,Nphi2_pad, phi1, phi2, Ng0_pad,Ng1_pad,Ng2_pad);

    // free memory
    p_free(phi1, VARNAME(phi1));
    #ifndef ONLY_ZA
    p_free(phi2, VARNAME(phi2));
    #endif

    // compute LPT prefactors
    const double z_init = RedshiftLPT;
    const double a_init = z2a(z_init);

    double *LPT_pref = lpt_prefactors(a_init, cosmo);
    double D1 = LPT_pref[0], D2 = LPT_pref[1], Vel1 = LPT_pref[2], Vel2 = LPT_pref[3];

    debugPrintDouble(D1);
    debugPrintDouble(D2);
    debugPrintDouble(Vel1);
    debugPrintDouble(Vel2);

    Vel1 *= sqrt(a_init);    // convert to Gadget velocity
    Vel2 *= sqrt(a_init);    // convert to Gadget velocity

    // compute kicks and drifts to be applied
    float_t *KICK_X, *KICK_Y, *KICK_Z;
    float_t *DRIFT_X, *DRIFT_Y, *DRIFT_Z;
    KICK_X = (float_t *)p_malloc(Np*sizeof(float_t), VARNAME(KICK_X));
    KICK_Y = (float_t *)p_malloc(Np*sizeof(float_t), VARNAME(KICK_Y));
    KICK_Z = (float_t *)p_malloc(Np*sizeof(float_t), VARNAME(KICK_Z));
    DRIFT_X = (float_t *)p_malloc(Np*sizeof(float_t), VARNAME(DRIFT_X));
    DRIFT_Y = (float_t *)p_malloc(Np*sizeof(float_t), VARNAME(DRIFT_Y));
    DRIFT_Z = (float_t *)p_malloc(Np*sizeof(float_t), VARNAME(DRIFT_Z));

    #pragma omp parallel for schedule(static)
    for(particleID_t mp=0; mp<Np; mp++)
    {
        #ifndef ONLY_ZA
        // compute the LPT drift (equation (B.75) in F.Leclercq PhD Thesis, 2015)
        DRIFT_X[mp]    = -D1 * PsiLPT[mp].Psi1[0] + D2 * PsiLPT[mp].Psi2[0];
        DRIFT_Y[mp]    = -D1 * PsiLPT[mp].Psi1[1] + D2 * PsiLPT[mp].Psi2[1];
        DRIFT_Z[mp]    = -D1 * PsiLPT[mp].Psi1[2] + D2 * PsiLPT[mp].Psi2[2];
        // compute the LPT kick (equations (B.73-B.74) in F.Leclercq PhD Thesis, 2015)
        KICK_X[mp]    = Vel1 * PsiLPT[mp].Psi1[0] + Vel2 * PsiLPT[mp].Psi2[0];
        KICK_Y[mp]    = Vel1 * PsiLPT[mp].Psi1[1] + Vel2 * PsiLPT[mp].Psi2[1];
        KICK_Z[mp]    = Vel1 * PsiLPT[mp].Psi1[2] + Vel2 * PsiLPT[mp].Psi2[2];
        #else
        UNUSED(D2); UNUSED(Vel2);
        // compute the LPT drift (equation (B.75) in F.Leclercq PhD Thesis, 2015)
        DRIFT_X[mp]    = -D1 * PsiLPT[mp].Psi1[0];
        DRIFT_Y[mp]    = -D1 * PsiLPT[mp].Psi1[1];
        DRIFT_Z[mp]    = -D1 * PsiLPT[mp].Psi1[2];
        // compute the LPT kick (equations (B.73-B.74) in F.Leclercq PhD Thesis, 2015)
        KICK_X[mp]    = Vel1 * PsiLPT[mp].Psi1[0];
        KICK_Y[mp]    = Vel1 * PsiLPT[mp].Psi1[1];
        KICK_Z[mp]    = Vel1 * PsiLPT[mp].Psi1[2];
        #endif
    }

    // change velocities, displace particles and update redshift
    // in LPT, change velocities before displacing particles!!
    modify_snapshot_kick_mesh(P, KICK_X, KICK_Y, KICK_Z, Np0, Np1, Np2);
    modify_snapshot_drift_mesh(P, DRIFT_X, DRIFT_Y, DRIFT_Z, Np0, Np1, Np2);
    modify_snapshot_update_time(P, z_init);

    p_free(KICK_X, VARNAME(KICK_X));
    p_free(KICK_Y, VARNAME(KICK_Y));
    p_free(KICK_Z, VARNAME(KICK_Z));
    p_free(DRIFT_X, VARNAME(DRIFT_X));
    p_free(DRIFT_Y, VARNAME(DRIFT_Y));
    p_free(DRIFT_Z, VARNAME(DRIFT_Z));
} //evolve_lpt
///-------------------------------------------------------------------------------------
