///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- src/pmcola.c
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file pmcola.c
 *  \brief Routines specific to PM/COLA
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2014-2023
 *  \note for notations, see appendix B in Leclercq, PhD thesis 2015
 *        and in Tassev, Zaldarriaga, Einsenstein 2013 (TZE)
 */
#include "pmcola.h"
///-------------------------------------------------------------------------------------
static char* Tab_EvolutionMode_Labels[] = {"UNDEFINED", "PM", "tCOLA", "sCOLA"};
///-------------------------------------------------------------------------------------
/** @fn initializePMCOLATimers_
 * This subroutine initializes timers
 */
void initializePMCOLATimers_(double PMCOLATimers[12], char *file, char *func, int line)
{
    UNUSED(file); UNUSED(func); UNUSED(line);
    // order: density_cpu, density_wall, potential_Fcpu, potential_wall, accelerations_cpu, accelerations_wall, kick_cpu, kick_wall, drift_cpu, drift_wall, output_cpu, output_wall
    for(int c=0; c<12; c++)
        PMCOLATimers[c]=0.;
} //initializePMCOLATimers_
///-------------------------------------------------------------------------------------
/** @fn updateDensityTimer
 * This subroutine updates the density timer
 */
static inline void updateDensityTimer(double PMCOLATimers[12], double diff_t_cpu, double diff_t_wall)
{
    #if TIMERS
        PMCOLATimers[0]+=diff_t_cpu;
        PMCOLATimers[1]+=diff_t_wall;
    #else
        UNUSED(PMCOLATimers); UNUSED(diff_t_cpu); UNUSED(diff_t_wall);
    #endif
} //updateDensityTimer
///-------------------------------------------------------------------------------------
/** @fn updatePotentialTimer
 * This subroutine updates the potential timer
 */
static inline void updatePotentialTimer(double PMCOLATimers[12], double diff_t_cpu, double diff_t_wall)
{
    #if TIMERS
        PMCOLATimers[2]+=diff_t_cpu;
        PMCOLATimers[3]+=diff_t_wall;
    #else
        UNUSED(PMCOLATimers); UNUSED(diff_t_cpu); UNUSED(diff_t_wall);
    #endif
} //updatePotentialTimer
///-------------------------------------------------------------------------------------
/** @fn updateAccelerationTimer
 * This subroutine updates the accelerations timer
 */
static inline void updateAccelerationTimer(double PMCOLATimers[12], double diff_t_cpu, double diff_t_wall)
{
    #if TIMERS
        PMCOLATimers[4]+=diff_t_cpu;
        PMCOLATimers[5]+=diff_t_wall;
    #else
        UNUSED(PMCOLATimers); UNUSED(diff_t_cpu); UNUSED(diff_t_wall);
    #endif
} //updateAccelerationTimer
///-------------------------------------------------------------------------------------
/** @fn updateKickTimer
 * This subroutine updates the kick timer
 */
static inline void updateKickTimer(double PMCOLATimers[12], double diff_t_cpu, double diff_t_wall)
{
    #if TIMERS
        PMCOLATimers[6]+=diff_t_cpu;
        PMCOLATimers[7]+=diff_t_wall;
    #else
        UNUSED(PMCOLATimers); UNUSED(diff_t_cpu); UNUSED(diff_t_wall);
    #endif
} //updateKickTimer
///-------------------------------------------------------------------------------------
/** @fn updateDriftTimer
 * This subroutine updates the drift timer
 */
static inline void updateDriftTimer(double PMCOLATimers[12], double diff_t_cpu, double diff_t_wall)
{
    #if TIMERS
        PMCOLATimers[8]+=diff_t_cpu;
        PMCOLATimers[9]+=diff_t_wall;
    #else
        UNUSED(PMCOLATimers); UNUSED(diff_t_cpu); UNUSED(diff_t_wall);
    #endif
} //updateDriftTimer
///-------------------------------------------------------------------------------------
/** @fn updateOutputTimer
 * This subroutine updates the outputs timer
 */
static inline void updateOutputTimer(double PMCOLATimers[12], double diff_t_cpu, double diff_t_wall)
{
    #if TIMERS
        PMCOLATimers[10]+=diff_t_cpu;
        PMCOLATimers[11]+=diff_t_wall;
    #else
        UNUSED(PMCOLATimers); UNUSED(diff_t_cpu); UNUSED(diff_t_wall);
    #endif
} //updateOutputTimer
///-------------------------------------------------------------------------------------
/** @fn updateBoxTimers_
 * This subroutine updates the box timers given timestep timers
 */
static inline void updateBoxTimers_(double TimestepTimers[12], double BoxTimers[12], char *file, char *func, int line)
{
    UNUSED(file); UNUSED(func); UNUSED(line);
    #if TIMERS
    for(int c=0; c<12; c++)
        BoxTimers[c] += TimestepTimers[c];
    #else
    UNUSED(TimestepTimers); UNUSED(BoxTimers);
    #endif
} //updateBoxTimers_
///-------------------------------------------------------------------------------------
/** @fn printPMCOLATimers_
 * This subroutine prints timers
 */
void printPMCOLATimers_(double PMCOLATimers[12], char *type, char *file, char *func, int line)
{
    #if TIMERS
        char buf[BUFLENGTH];
        sprintf(buf, "%s: Density", type);
        PrintTimers_(buf, PMCOLATimers[0], PMCOLATimers[1], file, func, line);
        sprintf(buf, "%s: Potential", type);
        PrintTimers_(buf, PMCOLATimers[2], PMCOLATimers[3], file, func, line);
        sprintf(buf, "%s: Accelerations", type);
        PrintTimers_(buf, PMCOLATimers[4], PMCOLATimers[5], file, func, line);
        sprintf(buf, "%s: Kick", type);
        PrintTimers_(buf, PMCOLATimers[6], PMCOLATimers[7], file, func, line);
        sprintf(buf, "%s: Drift", type);
        PrintTimers_(buf, PMCOLATimers[8], PMCOLATimers[9], file, func, line);
        sprintf(buf, "%s: Outputs", type);
        PrintTimers_(buf, PMCOLATimers[10], PMCOLATimers[11], file, func, line);
        sprintf(buf, "%s: Total Evolution", type);
        PrintTimers_(buf, PMCOLATimers[0]+PMCOLATimers[2]+PMCOLATimers[4]+PMCOLATimers[6]+PMCOLATimers[8]+PMCOLATimers[10] , PMCOLATimers[1]+PMCOLATimers[3]+PMCOLATimers[5]+PMCOLATimers[7]+PMCOLATimers[9]+PMCOLATimers[11], file, func, line);
    #else
        UNUSED(PMCOLATimers); UNUSED(type); UNUSED(file); UNUSED(func); UNUSED(line);
    #endif
} //printPMCOLATimers_
///-------------------------------------------------------------------------------------
/** @fn convert_P_to_codeunits
 * This subroutine converts Gadget units to PM code units
 * and initializes the velocities
 * @param Np number of particles
 * @param x[Np] output particle positions
 * @param p[Np] output particle momenta
 * @param *P input snapshot
 * @param ai initial scale factor
 * @param hubble_ai Hubble factor at ai
 * @param D_ai linear growth factor at ai
 */
static inline void convert_P_to_codeunits(const particleID_t Np, particle_pos_t x[Np], particle_vel_t p[Np], const Snapshot *P, const double ai, const double hubble_ai, const double D_ai)
{
    #pragma omp parallel for schedule(static)
    for(particleID_t mp=0; mp<Np; mp++)
    {
        x[mp].Pos[0] = P->positions[mp].Pos[0];
        x[mp].Pos[1] = P->positions[mp].Pos[1];
        x[mp].Pos[2] = P->positions[mp].Pos[2];

        // convert from Gadget velocity (/sqrt(a))
        // convert dx/dt to dx/da (/aH) and dx/da to p (/D_ai)
        p[mp].Vel[0] = P->velocities[mp].Vel[0]/sqrt(ai) / (ai*hubble_ai) / D_ai;
        p[mp].Vel[1] = P->velocities[mp].Vel[1]/sqrt(ai) / (ai*hubble_ai) / D_ai;
        p[mp].Vel[2] = P->velocities[mp].Vel[2]/sqrt(ai) / (ai*hubble_ai) / D_ai;
    }
} //convert_P_to_codeunits
///-------------------------------------------------------------------------------------
/** @fn convert_codeunits_to_P
 * This subroutine adds back the large-scale LPT velocities (operator L_+ in TZE)
 * and converts PM code units to Gadget units
 * @param Np number of particles
 * @param x[Np] input particle positions
 * @param p[Np] input particle momenta
 * @param *P output snapshot
 * @param af final scale factor
 * @param hubble_af Hubble factor at af
 * @param D_af linear growth factor at af
 */
static inline void convert_codeunits_to_P(const particleID_t Np, const particle_pos_t x[Np], const particle_vel_t p[Np], Snapshot *P, const double af, const double hubble_af, const double D_af)
{
    #pragma omp parallel for schedule(static)
    for(particleID_t mp=0; mp<Np; mp++)
    {
        P->positions[mp].Pos[0] = x[mp].Pos[0];
        P->positions[mp].Pos[1] = x[mp].Pos[1];
        P->positions[mp].Pos[2] = x[mp].Pos[2];

        // convert p to dx/da (*D_af) and dx/da to dx/dt (*aH)
        // convert to Gadget velocity (*sqrt(a))
        P->velocities[mp].Vel[0] = p[mp].Vel[0] * D_af * (af*hubble_af) * sqrt(af);
        P->velocities[mp].Vel[1] = p[mp].Vel[1] * D_af * (af*hubble_af) * sqrt(af);
        P->velocities[mp].Vel[2] = p[mp].Vel[2] * D_af * (af*hubble_af) * sqrt(af);
    }
} //convert_codeunits_to_P
///-------------------------------------------------------------------------------------
/** @fn L_minus
 * This subroutine initializes velocities in COLA (operator L- in TZE)
 * @param Np number of particles
 * @param p[Np] particle momenta
 * @param PsiLPT[Np] Lagrangian displacement field of particles
 * @param ai initial scale factor
 * @param hubble_ai Hubble factor at ai
 * @param D_ai linear growth factor at ai
 * @param Vel1_ai prefactor for velocities in 1LPT at ai
 * @param Vel2_ai prefactor for velocities in 2LPT at ai
 */
static inline void L_minus(const particleID_t Np, particle_vel_t p[Np], const particle_lpt_t PsiLPT[Np], const double ai, const double hubble_ai, const double D_ai, const double Vel1_ai, const double Vel2_ai)
{
    #pragma omp parallel for schedule(static)
    for(particleID_t mp=0; mp<Np; mp++)
    {
        // subtract initial LPT velocity (operator L- in TZE)
        // to avoid short-scale noise, one can smooth psi_1 and psi_2 (section A.3. in TEWZ)
        // however, that smoothing should not affect the initial velocities, which would be the case
        // by setting them to zero. so, we first add the full velocity, then subtract the same but smoothed
        #ifndef ONLY_ZA
        p[mp].Vel[0] -= (Vel1_ai * PsiLPT[mp].Psi1[0] + Vel2_ai * PsiLPT[mp].Psi2[0]) / (ai*hubble_ai) / D_ai;
        p[mp].Vel[1] -= (Vel1_ai * PsiLPT[mp].Psi1[1] + Vel2_ai * PsiLPT[mp].Psi2[1]) / (ai*hubble_ai) / D_ai;
        p[mp].Vel[2] -= (Vel1_ai * PsiLPT[mp].Psi1[2] + Vel2_ai * PsiLPT[mp].Psi2[2]) / (ai*hubble_ai) / D_ai;
        #else
        UNUSED(Vel2_ai);
        p[mp].Vel[0] -= (Vel1_ai * PsiLPT[mp].Psi1[0]) / (ai*hubble_ai) / D_ai;
        p[mp].Vel[1] -= (Vel1_ai * PsiLPT[mp].Psi1[1]) / (ai*hubble_ai) / D_ai;
        p[mp].Vel[2] -= (Vel1_ai * PsiLPT[mp].Psi1[2]) / (ai*hubble_ai) / D_ai;
        #endif
    }
} //L_minus
///-------------------------------------------------------------------------------------
/** @fn L_plus
 * This subroutine adds back LPT velocities in COLA (operator L+ in TZE)
 * @param Np number of particles
 * @param p[Np] particle momenta
 * @param PsiLPT[Np] Lagrangian displacement field of particles
 * @param af final scale factor
 * @param hubble_af Hubble factor at af
 * @param D_af linear growth factor at af
 * @param Vel1_af prefactor for velocities in 1LPT at af
 * @param Vel2_af prefactor for velocities in 2LPT at af
 */
static inline void L_plus(const particleID_t Np, particle_vel_t p[Np], const particle_lpt_t PsiLPT[Np], const double af, const double hubble_af, const double D_af, const double Vel1_af, const double Vel2_af)
{
    #pragma omp parallel for schedule(static)
    for(particleID_t mp=0; mp<Np; mp++)
    {
        // add back final LPT velocity (operator L+ in TZE)
        #ifndef ONLY_ZA
        p[mp].Vel[0] += (Vel1_af * PsiLPT[mp].Psi1[0] + Vel2_af * PsiLPT[mp].Psi2[0]) / (af*hubble_af) / D_af;
        p[mp].Vel[1] += (Vel1_af * PsiLPT[mp].Psi1[1] + Vel2_af * PsiLPT[mp].Psi2[1]) / (af*hubble_af) / D_af;
        p[mp].Vel[2] += (Vel1_af * PsiLPT[mp].Psi1[2] + Vel2_af * PsiLPT[mp].Psi2[2]) / (af*hubble_af) / D_af;
        #else
        UNUSED(Vel2_af);
        p[mp].Vel[0] += (Vel1_af * PsiLPT[mp].Psi1[0]) / (af*hubble_af) / D_af;
        p[mp].Vel[1] += (Vel1_af * PsiLPT[mp].Psi1[1]) / (af*hubble_af) / D_af;
        p[mp].Vel[2] += (Vel1_af * PsiLPT[mp].Psi1[2]) / (af*hubble_af) / D_af;
        #endif
    }
} //L_plus
///-------------------------------------------------------------------------------------
/** @fn extract_dirichlet_bcs
 * This subroutine extracts the boundary gravitational potential
 * on the PM grid given its value on the mesh
 */
static inline void extract_dirichlet_bcs(const int Npm0, const int Npm1, const int Npm2, const int Nphi0_pad, const int Nphi1_pad, const int Nphi2_pad, const float_t *phiBCs, float_t *phi_W, float_t *phi_E, float_t *phi_S, float_t *phi_N, float_t *phi_B, float_t *phi_T)
{
    #pragma omp parallel for schedule(static) collapse(3)
    for(int mg0=0; mg0<Nghost; mg0++)
        for(int mg1=0; mg1<Npm1; mg1++)
            for(int mg2=0; mg2<Npm2; mg2++)
            {
                const int mg0_W=-mg0-1;
                const int mg0_E=mg0+Npm0;
                phi_W[get_index(mg0,mg1,mg2,Nghost,Npm1,Npm2)] = phiBCs[get_index_in_paddedbox(mg0_W,mg1,mg2,Npm0,Npm1,Npm2,Nphi0_pad,Nphi1_pad,Nphi2_pad)];
                phi_E[get_index(mg0,mg1,mg2,Nghost,Npm1,Npm2)] = phiBCs[get_index_in_paddedbox(mg0_E,mg1,mg2,Npm0,Npm1,Npm2,Nphi0_pad,Nphi1_pad,Nphi2_pad)];
            }
    #pragma omp parallel for schedule(static) collapse(3)
    for(int mg1=0; mg1<Nghost; mg1++)
        for(int mg0=0; mg0<Npm0; mg0++)
            for(int mg2=0; mg2<Npm2; mg2++)
            {
                const int mg1_S=-mg1-1;
                const int mg1_N=mg1+Npm1;
                phi_S[get_index(mg1,mg0,mg2,Nghost,Npm0,Npm2)] = phiBCs[get_index_in_paddedbox(mg0,mg1_S,mg2,Npm0,Npm1,Npm2,Nphi0_pad,Nphi1_pad,Nphi2_pad)];
                phi_N[get_index(mg1,mg0,mg2,Nghost,Npm0,Npm2)] = phiBCs[get_index_in_paddedbox(mg0,mg1_N,mg2,Npm0,Npm1,Npm2,Nphi0_pad,Nphi1_pad,Nphi2_pad)];
            }
    #pragma omp parallel for schedule(static) collapse(3)
    for(int mg2=0; mg2<Nghost; mg2++)
        for(int mg0=0; mg0<Npm0; mg0++)
            for(int mg1=0; mg1<Npm1; mg1++)
            {
                const int mg2_B=-mg2-1;
                const int mg2_T=mg2+Npm2;
                phi_B[get_index(mg2,mg0,mg1,Nghost,Npm0,Npm1)] = phiBCs[get_index_in_paddedbox(mg0,mg1,mg2_B,Npm0,Npm1,Npm2,Nphi0_pad,Nphi1_pad,Nphi2_pad)];
                phi_T[get_index(mg2,mg0,mg1,Nghost,Npm0,Npm1)] = phiBCs[get_index_in_paddedbox(mg0,mg1,mg2_T,Npm0,Npm1,Npm2,Nphi0_pad,Nphi1_pad,Nphi2_pad)];
            }
} //extract_dirichlet_bcs
///-------------------------------------------------------------------------------------
/** @fn solve_poisson_pm_cola
 * This subroutine is a wrapper around different
 * Poisson solvers to get the gravitational potential
 */
static inline void solve_poisson_pm_cola(float_t *density, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, const int boundary_conditions, const int Nphi0_pad, const int Nphi1_pad, const int Nphi2_pad, const float_t phiBCs[(Npm0+2*Nphi0_pad)*(Npm1+2*Nphi1_pad)*(Npm2+2*Nphi2_pad)], double *PMCOLATimers)
{
    const bool smooth = true;

    CPUtime startm; Walltime startw; double diff_t_cpu, diff_t_wall;
    StartTimers(&startm, &startw);
    switch(boundary_conditions)
    {
        case 1: // periodic boundary conditions
            solve_poisson_periodic(density,Npm0,Npm1,Npm2,L0,L1,L2,smooth);
        break;
        case 2: // zero-potential boundary conditions
            solve_poisson_zerobc(density,Npm0,Npm1,Npm2,L0,L1,L2,smooth);
        break;
        case 3: // Dirichlet boundary conditions
        {
            float_t *phi_W, *phi_E, *phi_S, *phi_N, *phi_B, *phi_T;
            phi_W = (float_t *)p_malloc(Nghost*Npm1*Npm2*sizeof(float_t), VARNAME(phi_W));
            phi_E = (float_t *)p_malloc(Nghost*Npm1*Npm2*sizeof(float_t), VARNAME(phi_E));
            phi_S = (float_t *)p_malloc(Nghost*Npm0*Npm2*sizeof(float_t), VARNAME(phi_S));
            phi_N = (float_t *)p_malloc(Nghost*Npm0*Npm2*sizeof(float_t), VARNAME(phi_N));
            phi_B = (float_t *)p_malloc(Nghost*Npm1*Npm1*sizeof(float_t), VARNAME(phi_B));
            phi_T = (float_t *)p_malloc(Nghost*Npm1*Npm1*sizeof(float_t), VARNAME(phi_T));

            // extract Dirichlet boundary conditions given phiBCs
            extract_dirichlet_bcs(Npm0,Npm1,Npm2,Nphi0_pad,Nphi1_pad,Nphi2_pad,phiBCs,phi_W,phi_E,phi_S,phi_N,phi_B,phi_T);

            // solve the Poisson equation
            solve_poisson_dirichletbc(density,Npm0,Npm1,Npm2,L0,L1,L2,smooth,phi_W,phi_E,phi_S,phi_N,phi_B,phi_T);

            p_free(phi_W, VARNAME(phi_W));
            p_free(phi_E, VARNAME(phi_E));
            p_free(phi_S, VARNAME(phi_S));
            p_free(phi_N, VARNAME(phi_N));
            p_free(phi_B, VARNAME(phi_B));
            p_free(phi_T, VARNAME(phi_T));
        }
        break;
    }
    StopTimers(startm, startw, &diff_t_cpu, &diff_t_wall);
    updatePotentialTimer(PMCOLATimers, diff_t_cpu, diff_t_wall);
} //solve_poisson_pm_cola
///-------------------------------------------------------------------------------------
/** @fn pad_phi
 * This subroutine adds boundary conditions to the
 * gravitational potential in the sCOLA box, given
 * the linear gravitational potential in the full box
 */
static inline void pad_phi(const int Npm0, const int Npm1, const int Npm2, const int Nphi0_pad, const int Nphi1_pad, const int Nphi2_pad, const float_t phi[Npm0*Npm1*Npm2], const float_t phiBCs[(Npm0+2*Nphi0_pad)*(Npm1+2*Nphi1_pad)*(Npm2+2*Nphi2_pad)], float_t phi_pad[(Npm0+2*Nphi0_pad)*(Npm1+2*Nphi1_pad)*(Npm2+2*Nphi2_pad)])
{
    #pragma omp parallel for schedule(static) collapse(3)
    for(int mg0=-Nphi0_pad; mg0<Npm0+Nphi0_pad; mg0++)
        for(int mg1=-Nphi1_pad; mg1<Npm1+Nphi1_pad; mg1++)
            for(int mg2=-Nphi2_pad; mg2<Npm2+Nphi2_pad; mg2++)
            {
                const cellIndex_t mg_pad = get_index_in_paddedbox(mg0,mg1,mg2,Npm0,Npm1,Npm2,Nphi0_pad,Nphi1_pad,Nphi2_pad);
                const bool is_in_interior = mg0>=0 && mg0<Npm0 && mg1>=0 && mg1<Npm1 && mg2>= 0 && mg2<Npm2;

                // in the interior, just copy the value of phi_interior
                if(is_in_interior)
                {
                    const cellIndex_t mg = get_index(mg0,mg1,mg2,Npm0,Npm1,Npm2);
                    phi_pad[mg_pad] = phi[mg];
                }
                // at the boundaries, use the Dirichlet boundary conditions
                else
                    phi_pad[mg_pad] = phiBCs[mg_pad];
            }
} //pad_phi
///-------------------------------------------------------------------------------------
/** @fn acceleration_loop
 * This subroutine computes the accelerations
 * by finite differencing on the PM grid
 */
static inline void acceleration_loop(const int d, const int Npm0, const int Npm1, const int Npm2, const int Ng0_pad, const int Ng1_pad, const int Ng2_pad, float_t g[(Npm0+2*Ng0_pad)*(Npm1+2*Ng1_pad)*(Npm2+2*Ng2_pad)], const int Nphi0_pad, const int Nphi1_pad, const int Nphi2_pad, const float_t phi[(Npm0+2*Nphi0_pad)*(Npm1+2*Nphi1_pad)*(Npm2+2*Nphi2_pad)], const double dpm0, const double dpm1, const double dpm2, const int boundary_conditions)
{
    sprintf(G__msg__, "Acceleration loop, direction %d...", d);
    PrintMessage(5, G__msg__);
    INDENT;

    // get accelerations on the PM grid
    // in PM/tCOLA, use the gravitational potential in the full box
    // in sCOLA, use the gravitational potential in the small box

    #pragma omp parallel for schedule(static) collapse(3)
    for(int mg0=-Ng0_pad; mg0<Npm0+Ng0_pad; mg0++)
        for(int mg1=-Ng1_pad; mg1<Npm1+Ng1_pad; mg1++)
            for(int mg2=-Ng2_pad; mg2<Npm2+Ng2_pad; mg2++)
            {
                g[get_index_in_paddedbox(mg0,mg1,mg2,Npm0,Npm1,Npm2,Ng0_pad,Ng1_pad,Ng2_pad)] = gradient_rs_1d(phi,d,mg0,mg1,mg2,Npm0,Npm1,Npm2,dpm0,dpm1,dpm2,boundary_conditions,Nphi0_pad,Nphi1_pad,Nphi2_pad);
            }

    UNINDENT;
    sprintf(G__msg__, "Acceleration loop, direction %d done.", d);
    PrintMessage(5, G__msg__);
} //acceleration_loop
///-------------------------------------------------------------------------------------
/** @fn kick_loop
 * This subroutine kicks particles and adds the contribution
 * from the large-scale displacements in COLA
 */
static inline void kick_loop(const particleID_t Np, particle_pos_t x[Np], particle_vel_t p_new[Np], particle_vel_t p_old[Np], const particle_lpt_t PsiLPT[Np], const particle_pos_t *gp_ext, const int Npm0, const int Npm1, const int Npm2, const int Ng0_pad, const int Ng1_pad, const int Ng2_pad, const float_t g[(Npm0+2*Ng0_pad)*(Npm1+2*Ng1_pad)*(Npm2+2*Ng2_pad)], const int EvolutionMode, const int d, const double dpm0, const double dpm1, const double dpm2, const int boundary_conditions, const double beta_delta, const double beta_lpt_1, const double beta_lpt_2, const double beta_ext)
{
    sprintf(G__msg__, "Kick loop, direction %d...", d);
    PrintMessage(5, G__msg__);
    INDENT;
    UNUSED(EvolutionMode);

    #pragma omp parallel for schedule(static)
    for(particleID_t mp=0; mp<Np; mp++)
    {
        // interpolate to get accelerations at the particles' positions
        // warning the interpolation scheme should be consistent with computation of density here (to ensure absence of artificial self-forces)
        // mesh_to_particle uses periodic boundary conditions for PM/tCOLA, Dirichlet boundary conditions for sCOLA
        #ifndef NOGRAVITY
        float_t gp = mesh_to_particle(g,Npm0,Npm1,Npm2,dpm0,dpm1,dpm2,x[mp].Pos[0],x[mp].Pos[1],x[mp].Pos[2],1.,boundary_conditions,Ng0_pad,Ng1_pad,Ng2_pad);
        #else
        UNUSED(dpm0); UNUSED(dpm1); UNUSED(dpm2); UNUSED(x); UNUSED(g); UNUSED(boundary_conditions);
        #endif

        // update velocities with accelerations (equation (B.65) in F.Leclercq PhD Thesis, 2015)
        // in tCOLA, update also with LPT displacements of the full box
        // in sCOLA, update also with LPT displacements of the small box
        #ifndef ONLY_ZA
            #ifndef NOGRAVITY
            p_new[mp].Vel[d] = p_old[mp].Vel[d] + beta_delta * gp + beta_lpt_1 * PsiLPT[mp].Psi1[d] + beta_lpt_2 * PsiLPT[mp].Psi2[d];
            #else
            UNUSED(beta_delta);
            p_new[mp].Vel[d] = p_old[mp].Vel[d] + beta_lpt_1 * PsiLPT[mp].Psi1[d] + beta_lpt_2 * PsiLPT[mp].Psi2[d];
            #endif
        #else
            UNUSED(beta_lpt_2);
            #ifndef NOGRAVITY
            p_new[mp].Vel[d] = p_old[mp].Vel[d] + beta_delta * gp + beta_lpt_1 * PsiLPT[mp].Psi1[d];
            #else
            UNUSED(beta_delta);
            p_new[mp].Vel[d] = p_old[mp].Vel[d] + beta_lpt_1 * PsiLPT[mp].Psi1[d];
            #endif
        #endif
    }

    // add contribution from external frame of reference if specified
    if(gp_ext)
    {
        #pragma omp parallel for schedule(static)
        for(particleID_t mp=0; mp<Np; mp++)
            p_new[mp].Vel[d] += beta_ext * gp_ext[mp].Pos[d];
    }

    UNINDENT;
    sprintf(G__msg__, "Kick loop, direction %d done.", d);
    PrintMessage(5, G__msg__);
} //kick_loop
///-------------------------------------------------------------------------------------
/** @fn kick_pm_cola
 * This subroutine update particles' velocities
 */
static inline void kick_pm_cola(const particleID_t Np, particle_pos_t x[Np], particle_vel_t p[Np], const particle_lpt_t PsiLPT[Np], const float_t *phi, const int EvolutionMode, const int Np0, const int Np1, const int Np2, const int Npm0, const int Npm1, const int Npm2, const double dpm0, const double dpm1, const double dpm2, const double beta_delta, const double beta_lpt_1, const double beta_lpt_2, const double beta_ext, const int boundary_conditions, const int Nphi0_pad, const int Nphi1_pad, const int Nphi2_pad, const float_t phiBCs[(Npm0+2*Nphi0_pad)*(Npm1+2*Nphi1_pad)*(Npm2+2*Nphi2_pad)], const bool WriteReferenceFrame, const char *OutputKickBase, const char *OutputKickExt, const bool ReadReferenceFrame, const char *InputKickBase, const char *InputKickExt, const int nkick, double *PMCOLATimers)
{
    int Nthreads = omp_get_max_threads();
    sprintf(G__msg__, "Kicking particles (using %d cores)...", Nthreads);
    PrintMessage(4, G__msg__);
    INDENT;

    CPUtime startm; Walltime startw; double diff_t_cpu, diff_t_wall;
    int Ng0_pad, Ng1_pad, Ng2_pad;
    float_t *g_pad;
    float_t *phi_pad;
    particle_vel_t *p_new, *p_old, *dp;
    particle_pos_t *gp_ext = NULL;

    // prepare gravitational potential and acceleration on the grid
    switch(EvolutionMode)
    {
        case 1: // PM
        case 2: // tCOLA
        {
            Ng0_pad=0, Ng1_pad=0, Ng2_pad=0;
            const int Ng_pad=(Npm0+2*Ng0_pad)*(Npm1+2*Ng1_pad)*(Npm2+2*Ng2_pad);
            g_pad = (float_t *)p_malloc(Ng_pad*sizeof(float_t), VARNAME(g_pad));

            // the potential is simply the input
            phi_pad = (float_t *)phi;
        }
        break;
        case 3: // sCOLA
        {
            Ng0_pad=Nghost, Ng1_pad=Nghost, Ng2_pad=Nghost;
            const int Ng_pad=(Npm0+2*Ng0_pad)*(Npm1+2*Ng1_pad)*(Npm2+2*Ng2_pad);
            g_pad = (float_t *)p_malloc(Ng_pad*sizeof(float_t), VARNAME(g_pad));

            // add boundary conditions to the potential, necessary for the computation of accelerations
            const int Nphi_pad=(Npm0+2*Nphi0_pad)*(Npm1+2*Nphi1_pad)*(Npm2+2*Nphi2_pad);
            phi_pad = (float_t *)p_malloc(Nphi_pad*sizeof(float_t), VARNAME(phi_pad));
            pad_phi(Npm0,Npm1,Npm2,Nphi0_pad,Nphi1_pad,Nphi2_pad,phi,phiBCs,phi_pad);
        }
        break;
    }

    // prepare arrays for kicks
    if(WriteReferenceFrame)
    {
        // copy p_old to a new array, as it will be needed later
        p_old = (particle_vel_t *)p_malloc(Np*sizeof(particle_vel_t), VARNAME(p_old));
        #pragma omp parallel for schedule(static)
        for(particleID_t mp=0; mp<Np; mp++)
            for(int d=0; d<3; d++)
                p_old[mp].Vel[d] = p[mp].Vel[d];
    }
    else
    {
        // no need to copy p_old
        p_old = p;
    }
    p_new = p;

    // read external reference frame if needed
    if(ReadReferenceFrame)
    {
        dp = (particle_vel_t *)p_malloc(Np*sizeof(particle_vel_t), VARNAME(dp));
        char fname[500]; sprintf(fname,"%s%d%s",InputKickBase,nkick,InputKickExt);
        read_particle_vel_hdf5(Np, dp, Np0,Np1,Np2, fname);

        // compute additional acceleration corresponding to the kick from the external reference frame
        gp_ext = (particle_pos_t *)p_malloc(Np*sizeof(particle_pos_t), VARNAME(gp_ext));
        for(particleID_t mp=0; mp<Np; mp++)
            for(int d=0; d<3; d++)
                gp_ext[mp].Pos[d] = - 1./beta_delta * dp[mp].Vel[d];

        p_free(dp,VARNAME(dp));
    }

    // perform acceleration and kick loops
    // loop on dimensions
    for(int d=0; d<3; d++)
    {
        // acceleration loop
        StartTimers(&startm, &startw);
        acceleration_loop(d,Npm0,Npm1,Npm2,Ng0_pad,Ng1_pad,Ng2_pad,g_pad,Nphi0_pad,Nphi1_pad,Nphi2_pad,phi_pad,dpm0,dpm1,dpm2,boundary_conditions);
        StopTimers(startm, startw, &diff_t_cpu, &diff_t_wall);
        updateAccelerationTimer(PMCOLATimers, diff_t_cpu, diff_t_wall);

        // kick loop
        StartTimers(&startm, &startw);
        kick_loop(Np,x,p_new,p_old,PsiLPT,gp_ext,Npm0,Npm1,Npm2,Ng0_pad,Ng1_pad,Ng2_pad,g_pad,EvolutionMode,d,dpm0,dpm1,dpm2,boundary_conditions,beta_delta,beta_lpt_1,beta_lpt_2,beta_ext);
        StopTimers(startm, startw, &diff_t_cpu, &diff_t_wall);
        updateKickTimer(PMCOLATimers, diff_t_cpu, diff_t_wall);
    } // end loop on dimensions

    // save kicks if needed
    if(WriteReferenceFrame)
    {
        dp = p_old;
        #pragma omp parallel for schedule(static)
        for(particleID_t mp=0; mp<Np; mp++)
            for(int d=0; d<3; d++)
                dp[mp].Vel[d] = p_new[mp].Vel[d] - p_old[mp].Vel[d];

        // subtract contribution from external frame of reference if specified, to get back to the PM/LPT frame
        if(gp_ext)
        {
            #pragma omp parallel for schedule(static)
            for(particleID_t mp=0; mp<Np; mp++)
                for(int d=0; d<3; d++)
                    dp[mp].Vel[d] -= beta_ext * gp_ext[mp].Pos[d];
        }

        char fname[500]; sprintf(fname,"%s%d%s",OutputKickBase,nkick,OutputKickExt);
        write_particle_vel_hdf5(Np, dp, Np0,Np1,Np2, fname);
    }

    // free memory
    p_free(g_pad,VARNAME(g_pad));
    if(EvolutionMode == 3)
        p_free(phi_pad,VARNAME(phi_pad));
    if(WriteReferenceFrame)
        p_free(p_old,VARNAME(p_old));
    if(ReadReferenceFrame)
        p_free(gp_ext,VARNAME(gp_ext));

    UNINDENT;
    sprintf(G__msg__, "Kicking particles (using %d cores) done.", Nthreads);
    PrintMessage(4, G__msg__);
} //kick_cola
///-------------------------------------------------------------------------------------
/** @fn drift_loop
 * This subroutine drifts particles and adds the contribution
 * from the large-scale displacements in COLA
 */
static inline void drift_loop(const particleID_t Np, particle_pos_t x_new[Np], particle_pos_t x_old[Np], particle_vel_t p[Np], const particle_lpt_t PsiLPT[Np], const particle_vel_t *p_ext, const int EvolutionMode, const int d, const double Ld, const double alpha_p, const double alpha_lpt_1, const double alpha_lpt_2, const double alpha_ext)
{
    sprintf(G__msg__, "Drift loop, direction %d...", d);
    PrintMessage(5, G__msg__);
    INDENT;

    // update positions using momenta (equation (B.64) in F.Leclercq PhD Thesis, 2015)
    // in tCOLA/sCOLA, also update positions using LPT displacements in the full box
    #pragma omp parallel for schedule(static)
    for(particleID_t mp=0; mp<Np; mp++)
    {
        #ifndef ONLY_ZA
            #ifndef NOGRAVITY
            x_new[mp].Pos[d] = x_old[mp].Pos[d] + alpha_p * p[mp].Vel[d] + alpha_lpt_1 * PsiLPT[mp].Psi1[d] + alpha_lpt_2 * PsiLPT[mp].Psi2[d];
            #else
            UNUSED(p); UNUSED(alpha_p);
            x_new[mp].Pos[d] = x_old[mp].Pos[d] + alpha_lpt_1 * PsiLPT[mp].Psi1[d] + alpha_lpt_2 * PsiLPT[mp].Psi2[d];
            #endif
        #else
        UNUSED(alpha_lpt_2);
            #ifndef NOGRAVITY
            x_new[mp].Pos[d] = x_old[mp].Pos[d] + alpha_p * p[mp].Vel[d] + alpha_lpt_1 * PsiLPT[mp].Psi1[d];
            #else
            UNUSED(p); UNUSED(alpha_p);
            x_new[mp].Pos[d] = x_old[mp].Pos[d] + alpha_lpt_1 * PsiLPT[mp].Psi1[d];
            #endif
        #endif
    }

    // add contribution from external frame of reference if specified
    if(p_ext)
    {
        #pragma omp parallel for schedule(static)
        for(particleID_t mp=0; mp<Np; mp++)
            x_new[mp].Pos[d] += alpha_ext * p_ext[mp].Vel[d];
    }

    // deal with boundary conditions
    switch(EvolutionMode)
    {
        case 1: // PM
        case 2: // tCOLA
            #pragma omp parallel for schedule(static)
            for(particleID_t mp=0; mp<Np; mp++)
            {
                // in tCOLA, wrap particles assuming periodic boundary conditions
                x_new[mp].Pos[d] = periodic_wrap(x_new[mp].Pos[d], 0., Ld);
            }
        break;
        case 3: // sCOLA
                // in sCOLA, we do not ensure that particles are in the box
        break;
    }

    UNINDENT;
    sprintf(G__msg__, "Drift loop, direction %d done.", d);
    PrintMessage(5, G__msg__);
} //drift_loop
///-------------------------------------------------------------------------------------
/** @fn drift_pm_cola
 * This subroutine update particles' positions
 */
static inline void drift_pm_cola(const particleID_t Np, particle_pos_t x[Np], particle_vel_t p[Np], const particle_lpt_t PsiLPT[Np], const int EvolutionMode, const int Np0, const int Np1, const int Np2, const double L0, const double L1, const double L2, const double alpha_p, const double alpha_lpt_1, const double alpha_lpt_2, const double alpha_ext, const bool WriteReferenceFrame, const char *OutputDriftBase, const char *OutputDriftExt, const bool ReadReferenceFrame, const char *InputDriftBase, const char *InputDriftExt, const int ndrift, double *PMCOLATimers)
{
    int Nthreads = omp_get_max_threads();
    const double L[3]={L0,L1,L2};
    particle_pos_t *x_new, *x_old, *dx;
    particle_vel_t *p_ext = NULL;

    sprintf(G__msg__, "Drifting particles (using %d cores)...", Nthreads);
    PrintMessage(4, G__msg__);
    INDENT;

    // prepare arrays for drifts
    if(WriteReferenceFrame)
    {
        // copy x_old to a new array, as it will be needed later
        x_old = (particle_pos_t *)p_malloc(Np*sizeof(particle_pos_t), VARNAME(x_old));
        #pragma omp parallel for schedule(static)
        for(particleID_t mp=0; mp<Np; mp++)
            for(int d=0; d<3; d++)
                x_old[mp].Pos[d] = x[mp].Pos[d];
    }
    else
    {
        // no need to copy x_old
        x_old = x;
    }
    x_new = x;

    // read external reference frame if needed
    if(ReadReferenceFrame)
    {
        dx = (particle_pos_t *)p_malloc(Np*sizeof(particle_pos_t), VARNAME(dx));
        char fname[500]; sprintf(fname,"%s%d%s",InputDriftBase,ndrift,InputDriftExt);
        read_particle_pos_hdf5(Np, dx, Np0,Np1,Np2, fname);

        // compute additional momentum corresponding to the drift from the external reference frame
        p_ext = (particle_vel_t *)p_malloc(Np*sizeof(particle_vel_t), VARNAME(p_ext));
        for(particleID_t mp=0; mp<Np; mp++)
            for(int d=0; d<3; d++)
            {
                #ifndef ONLY_ZA
                    p_ext[mp].Vel[d] = 1./alpha_p * (dx[mp].Pos[d] - alpha_lpt_1 * PsiLPT[mp].Psi1[d] - alpha_lpt_2 * PsiLPT[mp].Psi2[d]);
                #else
                    p_ext[mp].Vel[d] = 1./alpha_p * (dx[mp].Pos[d] - alpha_lpt_1 * PsiLPT[mp].Psi1[d]);
                #endif
            }

        p_free(dx,VARNAME(dx));
    }

    // drift loop
    CPUtime startm; Walltime startw; double diff_t_cpu, diff_t_wall;
    StartTimers(&startm, &startw);
    for(int d=0; d<3; d++)
        drift_loop(Np,x_new,x_old,p,PsiLPT,p_ext,EvolutionMode,d,L[d],alpha_p,alpha_lpt_1,alpha_lpt_2,alpha_ext);
    StopTimers(startm, startw, &diff_t_cpu, &diff_t_wall);
    updateDriftTimer(PMCOLATimers, diff_t_cpu, diff_t_wall);

    // save drifts if needed
    if(WriteReferenceFrame)
    {
        dx = x_old;
        #pragma omp parallel for schedule(static)
        for(particleID_t mp=0; mp<Np; mp++)
            for(int d=0; d<3; d++)
                dx[mp].Pos[d] = x_new[mp].Pos[d] - x_old[mp].Pos[d];

        char fname[500]; sprintf(fname,"%s%d%s",OutputDriftBase,ndrift,OutputDriftExt);
        write_particle_pos_hdf5(Np, dx, Np0,Np1,Np2, fname);
    }

    // free memory
    if(WriteReferenceFrame)
        p_free(x_old,VARNAME(x_old));
    if(ReadReferenceFrame)
        p_free(p_ext,VARNAME(p_ext));

    UNINDENT;
    sprintf(G__msg__, "Drifting particles (using %d cores) done.", Nthreads);
    PrintMessage(4, G__msg__);
} //drift_cola
///-------------------------------------------------------------------------------------
/** @fn get_density_pm_cola
 * This subroutine computes the density field from particles' positions
 */
static inline void get_density_pm_cola(const particleID_t Np, const particle_pos_t x[Np], const int boundary_conditions, const int Npm0, const int Npm1, const int Npm2, const double dpm0, const double dpm1, const double dpm2, float_t *DELTA)
{
    switch(boundary_conditions)
    {
        default:
        case 1: // periodic boundary conditions
            get_density_pm(Np,x, Npm0,Npm1,Npm2, dpm0,dpm1,dpm2, DELTA);
        break;
        case 2: // zero-potential boundary conditions
        case 3: // Dirichlet boundary conditions
            // non periodic particle-mesh assignment: particles out of the box do not contribute
            // warning, this does not conserve mass!
            get_density_pm_nonperiodic(Np,x, Npm0,Npm1,Npm2, dpm0,dpm1,dpm2, DELTA);
        break;
    }
} //get_density_pm_cola
///-------------------------------------------------------------------------------------
/** @fn initialize_one_box
 * Initializes one (sub-)box to be evolved with PM/tCOLA/sCOLA
 * This routine should be called before evolve_one_box()
 * @param Np number of particles
 * @param x[Np] particle positions
 * @param p[Np] particle momenta
 * @param *P input snapshot
 * @param EvolutionMode 1=PM, 2=tCOLA, 3=sCOLA
 * @param ai initial scale factor
 * @param af final scale factor
 * @param cosmo cosmological parameters
 */
void initialize_one_box(const particleID_t Np, particle_pos_t x[Np], particle_vel_t p[Np], const Snapshot *P, const int EvolutionMode, const double ai, const double af, const double cosmo[N_COSMOPAR])
{
    // compute some physical variables
    double hubble_ai = hubble(ai, cosmo);
    double D_ai = D_of_a(ai, cosmo);

    // convert positions and velocities to code variables
    if(ai>=af)
    {
        sprintf(G__msg__, "Initial time for %s evolution should be smaller than final time, given ai=%g, af=%g.", Tab_EvolutionMode_Labels[EvolutionMode], ai, af);
        FatalError(G__msg__);
    }

    convert_P_to_codeunits(Np,x,p,P, ai,hubble_ai,D_ai);
} //initialize_one_box
///-------------------------------------------------------------------------------------
/** @fn finalize_one_box
 * Finalizes one (sub-)box evolved with PM/tCOLA/sCOLA
 * This routine should be called after evolve_one_box()
 * @param Np number of particles
 * @param x[Np] particle positions
 * @param p[Np] particle momenta
 * @param *P input snapshot
 * @param af final scale factor
 * @param cosmo cosmological parameters
 */
void finalize_one_box(const particleID_t Np, particle_pos_t x[Np], particle_vel_t p[Np], Snapshot *P,  const double af, const double cosmo[N_COSMOPAR])
{
    // compute some physical variables
    double hubble_af = hubble(af, cosmo);
    double D_af = D_of_a(af, cosmo);

    // convert to snapshot
    convert_codeunits_to_P(Np,x,p,P, af,hubble_af,D_af);

    // update time
    modify_snapshot_update_time(P, a2z(af));
} //finalize_one_box
///-------------------------------------------------------------------------------------
/** @fn get_timestepping
 * Precompute the timestepping for the simulation
 * @param nsteps number of timesteps
 * @param TimeStepDistribution distribution of timesteps (see documentation)
 * @param ai initial scale factor
 * @param af final scale factor
 * @param WriteSnapshots boolean: write intermediate snapshots?
 * @param WriteDensities boolean: write intermediate density fields?
 * @param Nkick output: number of calls to kick routine
 * @param Ndrift output: number of calls to drift routine
 * @param aiKickArray[Nkick] output
 * @param afKickArray[Nkick] output
 * @param aDriftArray[Nkick] output
 * @param aiDriftArray[Ndrift] output
 * @param afDriftArray[Ndrift] output
 * @param aKickArray[Ndrift] output
 * @param aKickOutput[nsteps] output
 * @param aDriftOutput[nsteps] output
 */
void get_timestepping(const int nsteps, const int TimeStepDistribution, const double ai, const double af, const bool WriteSnapshots, const bool WriteDensities, int *Nkick, int *Ndrift, double **aiKickArray, double **afKickArray, double **aDriftArray, double **aiDriftArray, double **afDriftArray, double **aKickArray, double **aKickOutput, double **aDriftOutput)
{
    // set time-stepping
    const double ti = a2t(TimeStepDistribution,ai);
    const double tf = a2t(TimeStepDistribution,af);
    const double dt = (tf-ti)/(double)nsteps;
    double aiKick  = ai; double tiKick   = ti;
    double aKick   = ai; double tKick    = ti;
    double afKick  = 0.; double tfKick   = 0.;
    double aiDrift = ai; double tiDrift  = ti;
    double aDrift  = ai; double tDrift   = ti;
    double afDrift = 0.; double tfDrift  = 0.;

    // allocate memory
    *Ndrift=nsteps;
    *Nkick=(WriteSnapshots || WriteDensities) ? 2*nsteps : nsteps+1;
    *aiKickArray = (double *)p_malloc(*Nkick*sizeof(double), VARNAME(aiKickArray));
    *afKickArray = (double *)p_malloc(*Nkick*sizeof(double), VARNAME(afKickArray));
    *aDriftArray = (double *)p_malloc(*Nkick*sizeof(double), VARNAME(aDriftArray));
    *aiDriftArray = (double *)p_malloc(*Ndrift*sizeof(double), VARNAME(aiDriftArray));
    *afDriftArray = (double *)p_malloc(*Ndrift*sizeof(double), VARNAME(afDriftArray));
    *aKickArray = (double *)p_malloc(*Ndrift*sizeof(double), VARNAME(aKickArray));
    *aKickOutput = (double *)p_malloc(nsteps*sizeof(double), VARNAME(aKickOutput));
    *aDriftOutput = (double *)p_malloc(nsteps*sizeof(double), VARNAME(aDriftOutput));

    // main loop
    int ndrift=0, nkick=0;
    for(int step=1; step<=nsteps; step++)
    {
        // ##########
        // # KICK   #
        // ##########
        {
            // Update time variables
            if(step==1 || WriteSnapshots || WriteDensities)        // give only a half-kick at the first timestep, or if outputs are required
                tfKick = tiKick + dt/2.;
            else
                tfKick = tiKick + dt;                // give a full kick otherwise
            afKick = t2a(TimeStepDistribution,tfKick);

            // ---- KICK -----
            (*aiKickArray)[nkick]=aiKick;
            (*afKickArray)[nkick]=afKick;
            (*aDriftArray)[nkick]=aDrift;
            nkick++;
            // ---- KICK -----

            // Update time variables
            tKick  = tfKick; aKick  = t2a(TimeStepDistribution, tKick);
            tiKick = tfKick; aiKick = t2a(TimeStepDistribution, tiKick);
        } // end kick

        // ##########
        // # DRIFT  #
        // ##########
        {
            if(step<=nsteps)
            {
                // Update time variables
                tfDrift = tiDrift + dt; afDrift = t2a(TimeStepDistribution,tfDrift);

                // ---- DRIFT -----
                (*aiDriftArray)[ndrift]=aiDrift;
                (*afDriftArray)[ndrift]=afDrift;
                (*aKickArray)[ndrift]=aKick;
                ndrift++;
                // ---- DRIFT -----

                // Update time variables
                tiDrift = tfDrift; aiDrift = t2a(TimeStepDistribution,tiDrift);
                tDrift  = tfDrift; aDrift  = t2a(TimeStepDistribution,tDrift);
            }
        } // end drift

        // ##########
        // # KICK   #
        // ##########
        {
            if(step==nsteps || WriteSnapshots || WriteDensities)    // give another half-kick at the last timestep, or if outputs are required
            {
                // Update time variables
                tfKick = tiKick + dt/2.; afKick = t2a(TimeStepDistribution,tfKick);

                // ---- KICK -----
                (*aiKickArray)[nkick]=aiKick;
                (*afKickArray)[nkick]=afKick;
                (*aDriftArray)[nkick]=aDrift;
                nkick++;
                // ---- KICK -----

                // Update time variables
                tKick  = tfKick; aKick  = t2a(TimeStepDistribution, tKick);
                tiKick = tfKick; aiKick = t2a(TimeStepDistribution, tiKick);
            }
        } // end kick

        // store time variables at the end of the timestep
        (*aKickOutput)[step-1] = aKick;
        (*aDriftOutput)[step-1] = aDrift;
    } // end main loop
} //get_timestepping
///-------------------------------------------------------------------------------------
/** @fn free_timestepping_
 * Free structures allocated for timestepping calculation
 * @param aiKickArray[Nkick] input
 * @param afKickArray[Nkick] input
 * @param aDriftArray[Nkick] input
 * @param aiDriftArray[Ndrift] input
 * @param afDriftArray[Ndrift] input
 * @param aKickArray[Ndrift] input
 * @param aKickOutput[nsteps] input
 * @param aDriftOutput[nsteps] input
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
void free_timestepping_(double *aiKickArray, double *afKickArray, double *aDriftArray, double *aiDriftArray, double *afDriftArray, double *aKickArray, double *aKickOutput, double *aDriftOutput, char *file, char *func, int line)
{
    p_free_(aiKickArray, VARNAME(aiKickArray), file, func, line);
    p_free_(afKickArray, VARNAME(afKickArray), file, func, line);
    p_free_(aDriftArray, VARNAME(aDriftArray), file, func, line);
    p_free_(aiDriftArray, VARNAME(aiDriftArray), file, func, line);
    p_free_(afDriftArray, VARNAME(afDriftArray), file, func, line);
    p_free_(aKickArray, VARNAME(aKickArray), file, func, line);
    p_free_(aKickOutput, VARNAME(aKickOutput), file, func, line);
    p_free_(aDriftOutput, VARNAME(aDriftOutput), file, func, line);
} //free_timestepping_
///-------------------------------------------------------------------------------------
/** @fn evolve_one_box
 * This routine evolves particles in one (sub-)box with PM/tCOLA/sCOLA
 * @param *P input/output snapshot
 * @param FCs_field output final conditions field
 * @param density_or_Phi array to store the density and gravitational potential
 * @param Np number of particles
 * @param x[Np] particles positions
 * @param p[Np] particles momenta
 * @param PsiLPT[Np] Lagrangian displacement field (for COLA)
 * @param EvolutionMode 1=PM, 2=tCOLA, 3=sCOLA
 * @param box index of this box
 * @param nboxes total number of boxes to be evolved
 * @param BoxTimers timers for the box
 * @param nsteps number of timesteps
 * @param ai initial scale factor
 * @param af final scale factor
 * @param Ndrift number of drift operator applications
 * @param Nkick number of kick operator applications
 * @param aiKick[Nkick] time-stepping array
 * @param afKick[Nkick] time-stepping array
 * @param aDrift[Nkick] time-stepping array
 * @param aiDrift[Ndrift] time-stepping array
 * @param afDrift[Ndrift] time-stepping array
 * @param aKick[Ndrift] time-stepping array
 * @param aKickOutput[nsteps] time-stepping array
 * @param aDriftOutput[nsteps] time-stepping array
 * @param ModifiedDiscretization boolean: modified discretization for COLA? (see documentation)
 * @param n_LPT parameter for modified discretization
 * @param WriteSnapshots boolean: write intermediate snapshots?
 * @param *OutputSnapshotsBase filename of intermediate snapshots (prefix)
 * @param *OutputSnapshotsExt filename of intermediate snapshots (suffix)
 * @param NumFilesPerSnapshot number of files per snapshot
 * @param SnapFormat format of snapshots
 * @param WriteDensities boolean: write intermediate density fields?
 * @param *OutputDensitiesBase filename of intermediate density fields (prefix)
 * @param *OutputDensitiesExt filename of intermediate density fields (suffix)
 * @param WriteReferenceFrame boolean: write reference frame?
 * @param *OutputKickBase output reference frame kick (prefix)
 * @param *OutputKickExt output reference frame kick (suffix)
 * @param *OutputDriftBase output reference frame drift (prefix)
 * @param *OutputDriftExt output reference frame drift (suffix)
 * @param ReadReferenceFrame boolean: read reference frame?
 * @param *InputKickBase input reference frame kick (prefix)
 * @param *InputKickExt input reference frame kick (suffix)
 * @param *InputDriftBase input reference frame drift (prefix)
 * @param *InputDriftExt input reference frame drift (suffix)
 * @param Np0 particle grid size x
 * @param Np1 particle grid size y
 * @param Np2 particle grid size z
 * @param Npm0 particle-mesh grid size x
 * @param Npm1 particle-mesh grid size y
 * @param Npm2 particle-mesh grid size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 * @param boundary_conditions 1=periodic, 2=zero, 3=Dirichlet
 * @param Npm0_pad number of additional cells to the West and East
 * @param Npm1_pad number of additional cells to the South and North
 * @param Npm2_pad number of additional cells to the Bottom and Top
 * @param phiBCs[Nkick][(Npm0_sCOLA+2*Npm0_pad)*(Npm1_sCOLA+2*Npm1_pad)*(Npm2_sCOLA+2*Npm2_pad)] gravitational potential to be used for the boundary conditions of the sCOLA box at each timestep
 * @param cosmo cosmological parameters
 */
void evolve_one_box(Snapshot *P, Field FCs_field, float_t *density_or_Phi, const particleID_t Np, particle_pos_t x[Np], particle_vel_t p[Np], const particle_lpt_t PsiLPT[Np], const int EvolutionMode, const int box, const int nboxes, double BoxTimers[12], const int nsteps, const double ai, const double af, const int Ndrift, const int Nkick, const double aiKick[Nkick], const double afKick[Nkick], const double aDrift[Nkick], const double aiDrift[Ndrift], const double afDrift[Ndrift], const double aKick[Ndrift], const double aKickOutput[nsteps], const double aDriftOutput[nsteps], const bool ModifiedDiscretization, const double n_LPT, const bool WriteSnapshots, const char *OutputSnapshotsBase, const char *OutputSnapshotsExt, const int NumFilesPerSnapshot, const int SnapFormat, const bool WriteDensities, const char *OutputDensitiesBase, const char *OutputDensitiesExt, const bool WriteReferenceFrame, const char *OutputKickBase, const char *OutputKickExt, const char *OutputDriftBase, const char *OutputDriftExt, const bool ReadReferenceFrame, const char *InputKickBase, const char *InputKickExt, const char *InputDriftBase, const char *InputDriftExt, const int Np0, const int Np1, const int Np2, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, const int boundary_conditions, const int Npm0_pad, const int Npm1_pad, const int Npm2_pad, float_t **phiBCs, const double cosmo[N_COSMOPAR])
{
    UNUSED(Np0); UNUSED(Np1); UNUSED(Np2);
    UNUSED(Npm0_pad); UNUSED(Npm1_pad); UNUSED(Npm2_pad);

    // set code units
    const double dpm0=L0/(double)Npm0;
    const double dpm1=L1/(double)Npm1;
    const double dpm2=L2/(double)Npm2;

    debugPrintDouble(dpm0);
    debugPrintDouble(dpm1);
    debugPrintDouble(dpm2);

    const int Nphi0_pad = (EvolutionMode == 3) ? 2*Nghost : 0;
    const int Nphi1_pad = (EvolutionMode == 3) ? 2*Nghost : 0;
    const int Nphi2_pad = (EvolutionMode == 3) ? 2*Nghost : 0;

    // declare variables for timers
    CPUtime startm; Walltime startw; double diff_t_cpu, diff_t_wall;

    // compute some physical variables
    double hubble_ai = hubble(ai, cosmo);
    double D_ai = D_of_a(ai, cosmo);
    double *LPT_pref, Vel1_ai, Vel2_ai, Vel1_af, Vel2_af;
    double hubble_af = hubble(af, cosmo);
    double D_af = D_of_a(af, cosmo);
    if(EvolutionMode == 2 || EvolutionMode == 3)
    {
        LPT_pref = lpt_prefactors(ai, cosmo);
        Vel1_ai = LPT_pref[2], Vel2_ai = LPT_pref[3];
        LPT_pref = lpt_prefactors(af, cosmo);
        Vel1_af = LPT_pref[2], Vel2_af = LPT_pref[3];
    }

    // initialize velocities in tCOLA/sCOLA
    if(EvolutionMode == 2 || EvolutionMode == 3)
        L_minus(Np,p,PsiLPT, ai,hubble_ai,D_ai,Vel1_ai,Vel2_ai);

    // main loop, leapfrog integration scheme
    int ndrift=0, nkick=0;
    for(int step=1; step<=nsteps; step++)
    {
        if(nboxes>1)
            sprintf(G__msg__, "ModulePMCOLA: Box %d/%d: Begin %s step %d/%d, time_kick:%f, time_drift=%f.", box, nboxes, Tab_EvolutionMode_Labels[EvolutionMode], step, nsteps, aiKick[ndrift], aiDrift[nkick]);
        else
            sprintf(G__msg__, "ModulePMCOLA: Begin %s step %d/%d, time_kick:%f, time_drift=%f.", Tab_EvolutionMode_Labels[EvolutionMode], step, nsteps, aiKick[ndrift], aiDrift[nkick]);
        PrintMessage(3, G__msg__);
        INDENT;

        // initialize timestep timers
        double TimestepTimers[12]; initializePMCOLATimers(TimestepTimers);

        // ##########
        // # FORCES #
        // ##########
        #ifndef NOGRAVITY
        {
            if(step==1 || !(WriteSnapshots || WriteDensities))    // otherwise, force calculation has already been done at the last timestep
            {
                // compute the density field (section (B.3) in F.Leclercq PhD Thesis, 2015)
                StartTimers(&startm, &startw);
                get_density_pm_cola(Np,x, boundary_conditions, Npm0,Npm1,Npm2, dpm0,dpm1,dpm2, density_or_Phi);
                StopTimers(startm, startw, &diff_t_cpu, &diff_t_wall);
                updateDensityTimer(TimestepTimers, diff_t_cpu, diff_t_wall);

                // get the gravitational potential on the mesh by solving the Poisson equation (section (B.4) in F.Leclercq PhD Thesis, 2015)
                solve_poisson_pm_cola(density_or_Phi, Npm0,Npm1,Npm2, L0,L1,L2, boundary_conditions, Nphi0_pad,Nphi1_pad,Nphi2_pad, phiBCs[nkick], TimestepTimers);
            }
        } // end forces
        #endif

        // ##########
        // # KICK   #
        // ##########
        {
            // Kick prefactors
            // (cf appendix B of F.Leclercq PhD Thesis, 2015)
            double beta_delta = dtp(ModifiedDiscretization, aiKick[nkick], afKick[nkick], aDrift[nkick], n_LPT, cosmo);
            debugPrintDouble(beta_delta);

            double *beta_lpt, beta_lpt_1, beta_lpt_2, beta_ext;
            switch(EvolutionMode)
            {
                case 1:
                    beta_lpt_1 = beta_lpt_2 = 0.;
                break;
                case 2:
                case 3:
                    beta_lpt = dp_lpt(beta_delta, aDrift[nkick], cosmo);
                    beta_lpt_1 = beta_lpt[0]; beta_lpt_2 = beta_lpt[1];
                    debugPrintDouble(beta_lpt_1);
                    debugPrintDouble(beta_lpt_2);
                break;
            }
            beta_ext = ReadReferenceFrame ? beta_delta : 0.;
            debugPrintDouble(beta_ext);

            // Kick
            // In tCOLA, also add velocity contribution from LPT in the full box
            // In sCOLA, also add velocity contribution from LPT in the sCOLA box
            // In the present implementation, we have PsiLPT_tCOLA = PsiLPT_sCOLA by construction,
            // due to the Dirichlet boundary conditions used. We can therefore save memory.
            kick_pm_cola(Np,x,p,PsiLPT,density_or_Phi, EvolutionMode, Np0,Np1,Np2, Npm0,Npm1,Npm2, dpm0,dpm1,dpm2, beta_delta,beta_lpt_1,beta_lpt_2,beta_ext, boundary_conditions, Nphi0_pad,Nphi1_pad,Nphi2_pad, phiBCs[nkick], WriteReferenceFrame, OutputKickBase, OutputKickExt, ReadReferenceFrame, InputKickBase, InputKickExt, nkick, TimestepTimers);
            nkick++;
        } // end kick

        // ##########
        // # DRIFT  #
        // ##########
        {
            if(step<=nsteps)
            {
                // Drift prefactors
                // (cf appendix B of F.Leclercq PhD Thesis, 2015)
                double alpha_p            = dtx(ModifiedDiscretization, aiDrift[ndrift], afDrift[ndrift], aKick[ndrift], n_LPT, cosmo);
                debugPrintDouble(alpha_p);

                double *alpha_lpt, alpha_lpt_1, alpha_lpt_2, alpha_ext;
                switch(EvolutionMode)
                {
                    case 1:
                        alpha_lpt_1 = alpha_lpt_2 = 0.;
                    break;
                    case 2:
                    case 3:
                        alpha_lpt = dx_lpt(aiDrift[ndrift], afDrift[ndrift], cosmo);
                        alpha_lpt_1 = alpha_lpt[0]; alpha_lpt_2 = alpha_lpt[1];
                        debugPrintDouble(alpha_lpt_1);
                        debugPrintDouble(alpha_lpt_2);
                    break;
                }
                alpha_ext = ReadReferenceFrame ? alpha_p : 0.;
                debugPrintDouble(alpha_ext);

                // Drift
                // In tCOLA/sCOLA, also add the LPT displacement in the full box
                drift_pm_cola(Np,x,p,PsiLPT, EvolutionMode, Np0,Np1,Np2, L0,L1,L2, alpha_p,alpha_lpt_1,alpha_lpt_2,alpha_ext, WriteReferenceFrame, OutputDriftBase, OutputDriftExt, ReadReferenceFrame, InputDriftBase, InputDriftExt, ndrift, TimestepTimers);
                ndrift++;
            }
        } // end drift

        // ##########
        // # FORCES #
        // ##########
        #ifndef NOGRAVITY
        {
            if(step==nsteps || WriteSnapshots || WriteDensities)    // force calculation is only required here if another half-kick will be given
            {
                // compute the density field
                StartTimers(&startm, &startw);
                get_density_pm_cola(Np,x, boundary_conditions, Npm0,Npm1,Npm2, dpm0,dpm1,dpm2, density_or_Phi);
                StopTimers(startm, startw, &diff_t_cpu, &diff_t_wall);
                updateDensityTimer(TimestepTimers, diff_t_cpu, diff_t_wall);

                // get the gravitational potential on the mesh by solving the Poisson equation
                solve_poisson_pm_cola(density_or_Phi, Npm0,Npm1,Npm2, L0,L1,L2, boundary_conditions, Nphi0_pad,Nphi1_pad,Nphi2_pad, phiBCs[nkick], TimestepTimers);
            }
        } // end forces
        #endif

        // ##########
        // # KICK   #
        // ##########
        {
            if(step==nsteps || WriteSnapshots || WriteDensities)    // give another half-kick at the last timestep, or if outputs are required
            {
                // Kick prefactors
                // (cf appendix B of F.Leclercq PhD Thesis, 2015)
                double beta_delta = dtp(ModifiedDiscretization, aiKick[nkick], afKick[nkick], aDrift[nkick], n_LPT, cosmo);
                debugPrintDouble(beta_delta);

                double *beta_lpt, beta_lpt_1, beta_lpt_2, beta_ext;
                switch(EvolutionMode)
                {
                    case 1:
                        beta_lpt_1 = beta_lpt_2 = 0.;
                    break;
                    case 2:
                    case 3:
                        beta_lpt = dp_lpt(beta_delta, aDrift[nkick], cosmo);
                        beta_lpt_1 = beta_lpt[0]; beta_lpt_2 = beta_lpt[1];
                        debugPrintDouble(beta_lpt_1);
                        debugPrintDouble(beta_lpt_2);
                    break;
                }
                beta_ext = ReadReferenceFrame ? beta_delta : 0.;
                debugPrintDouble(beta_ext);

                // Kick
                // In tCOLA, also add velocity contribution from LPT in the full box
                // In sCOLA, also add velocity contribution from LPT in the sCOLA box
                // In the present implementation, we have PsiLPT_tCOLA = PsiLPT_sCOLA by construction,
                // due to the Dirichlet boundary conditions used. We can therefore save memory.
                kick_pm_cola(Np,x,p,PsiLPT,density_or_Phi, EvolutionMode, Np0,Np1,Np2, Npm0,Npm1,Npm2, dpm0,dpm1,dpm2, beta_delta,beta_lpt_1,beta_lpt_2,beta_ext, boundary_conditions, Nphi0_pad,Nphi1_pad,Nphi2_pad, phiBCs[nkick], WriteReferenceFrame, OutputKickBase, OutputKickExt, ReadReferenceFrame, InputKickBase, InputKickExt, nkick, TimestepTimers);
                nkick++;

                // At this point positions and velocities are synchronized, one can write output
            }
        } // end kick

        // ##########
        // # OUTPUT #
        // ##########
        {
            if((EvolutionMode == 1 || EvolutionMode == 2) && step<nsteps && (WriteSnapshots || WriteDensities))
            {
                StartTimers(&startm, &startw);
                double hubble_aKick = hubble(aKick[ndrift], cosmo);
                double D_aKick = D_of_a(aKick[ndrift], cosmo);

                // in tCOLA, add back large-scale momenta
                if(EvolutionMode == 2)
                {
                    double* LPT_pref = lpt_prefactors(aKick[ndrift], cosmo);
                    double Vel1_aKick = LPT_pref[2], Vel2_aKick = LPT_pref[3];
                    L_plus(Np,p,PsiLPT, aKick[ndrift],hubble_aKick,D_aKick,Vel1_aKick,Vel2_aKick);
                }

                // convert to snapshot
                convert_codeunits_to_P(Np,x,p,P, aKick[ndrift],hubble_aKick,D_aKick);

                if(WriteSnapshots)
                {
                    char OutputSnapshot[500];
                    sprintf(OutputSnapshot, "%s%d%s", OutputSnapshotsBase, step, OutputSnapshotsExt);
                    output_snapshot(WriteSnapshots, *P, OutputSnapshot, NumFilesPerSnapshot, SnapFormat);
                }
                if(WriteDensities)
                {
                    char OutputDensity[500];
                    sprintf(OutputDensity, "%s%d%s", OutputDensitiesBase, step, OutputDensitiesExt);
                    update_and_output_density(FCs_field, *P, true, WriteDensities, aKick[ndrift], OutputDensity);
                }

                StopTimers(startm, startw, &diff_t_cpu, &diff_t_wall);
                updateOutputTimer(TimestepTimers, diff_t_cpu, diff_t_wall);
            }
        } // end output

        UNINDENT;
        if(nboxes>1)
            sprintf(G__msg__, "ModulePMCOLA: Box %d/%d: End %s step %d/%d, time_kick:%f, time_drift=%f.", box, nboxes, Tab_EvolutionMode_Labels[EvolutionMode], step, nsteps, aKickOutput[step-1], aDriftOutput[step-1]);
        else
            sprintf(G__msg__, "ModulePMCOLA: End %s step %d/%d, time_kick:%f, time_drift=%f.", Tab_EvolutionMode_Labels[EvolutionMode], step, nsteps, aKickOutput[step-1], aDriftOutput[step-1]);
        PrintMessage(3, G__msg__);

        // print timestep timers
        updateBoxTimers(TimestepTimers, BoxTimers);
        char type[50];
        if(nboxes>1)
            sprintf(type, "Box %d/%d, step %d/%d", box, nboxes, step, nsteps);
        else
            sprintf(type, "Step %d/%d", step, nsteps);
        printPMCOLATimers(TimestepTimers, type);
    } // end main loop

    // in tCOLA/sCOLA, add back large-scale momenta
    if(EvolutionMode == 2 || EvolutionMode == 3)
        L_plus(Np,p,PsiLPT, af,hubble_af,D_af,Vel1_af,Vel2_af);
} //evolve_one_box
///-------------------------------------------------------------------------------------
/** @fn evolve_pm_cola
 * Principal routine to evolve particles forward with PM/tCOLA
 * @param *P input/output snapshot
 * @param FCs_field output final conditions field
 * @param Np number of particles
 * @param PsiLPT[Np] Lagrangian displacement field (for COLA)
 * @param EvolutionMode 1=PM, 2=tCOLA, 3=sCOLA
 * @param nsteps number of timesteps
 * @param TimeStepDistribution distribution of timesteps (see documentation)
 * @param RedshiftFCs redshift of final conditions
 * @param ModifiedDiscretization boolean: modified discretization for COLA? (see documentation)
 * @param n_LPT parameter for modified discretization
 * @param WriteSnapshots boolean: write intermediate snapshots?
 * @param *OutputSnapshotsBase filename of intermediate snapshots (prefix)
 * @param *OutputSnapshotsExt filename of intermediate snapshots (suffix)
 * @param NumFilesPerSnapshot number of files per snapshot
 * @param SnapFormat format of snapshots
 * @param WriteDensities boolean: write intermediate density fields?
 * @param *OutputDensitiesBase filename of intermediate density fields (prefix)
 * @param *OutputDensitiesExt filename of intermediate density fields (suffix)
 * @param WriteReferenceFrame boolean: write reference frame?
 * @param *OutputKickBase output reference frame kick (prefix)
 * @param *OutputKickExt output reference frame kick (suffix)
 * @param *OutputDriftBase output reference frame drift (prefix)
 * @param *OutputDriftExt output reference frame drift (suffix)
 * @param ReadReferenceFrame boolean: read reference frame?
 * @param *InputKickBase input reference frame kick (prefix)
 * @param *InputKickExt input reference frame kick (suffix)
 * @param *InputDriftBase input reference frame drift (prefix)
 * @param *InputDriftExt input reference frame drift (suffix)
 * @param Np0 particle grid size x
 * @param Np1 particle grid size y
 * @param Np2 particle grid size z
 * @param Npm0 particle-mesh grid size x
 * @param Npm1 particle-mesh grid size y
 * @param Npm2 particle-mesh grid size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 * @param cosmo cosmological parameters
 */
void evolve_pm_cola(Snapshot *P, Field FCs_field, const particleID_t Np, particle_lpt_t PsiLPT[Np], const int EvolutionMode, const int nsteps, const int TimeStepDistribution, const double RedshiftFCs, const bool ModifiedDiscretization, const double n_LPT, const bool WriteSnapshots, const char *OutputSnapshotsBase, const char *OutputSnapshotsExt, const int NumFilesPerSnapshot, const int SnapFormat, const bool WriteDensities, const char *OutputDensitiesBase, const char *OutputDensitiesExt, const bool WriteReferenceFrame, const char *OutputKickBase, const char *OutputKickExt, const char *OutputDriftBase, const char *OutputDriftExt, const bool ReadReferenceFrame, const char *InputKickBase, const char *InputKickExt, const char *InputDriftBase, const char *InputDriftExt, const int Np0, const int Np1, const int Np2, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, const double cosmo[N_COSMOPAR])
{
    // set constants
    const int boundary_conditions = 1; // periodic boundary conditions
    const double ai = z2a(P->header.redshift);
    const double af = z2a(RedshiftFCs);
    const cellIndex_t Npm=Npm0*Npm1*Npm2;

    // set time-stepping
    int Ndrift, Nkick;
    double *aDrift, *aiDrift, *afDrift;
    double *aKick, *aiKick, *afKick;
    double *aKickOutput, *aDriftOutput;
    get_timestepping(nsteps,TimeStepDistribution,ai,af,WriteSnapshots,WriteDensities,&Nkick,&Ndrift,&aiKick,&afKick,&aDrift,&aiDrift,&afDrift,&aKick,&aKickOutput,&aDriftOutput);
    debugPrintInt(Ndrift);
    debugPrintInt(Nkick);
    debugPrintDoubleArray(aiKick,Nkick);
    debugPrintDoubleArray(afKick,Nkick);
    debugPrintDoubleArray(aDrift,Nkick);
    debugPrintDoubleArray(aiDrift,Ndrift);
    debugPrintDoubleArray(afDrift,Ndrift);
    debugPrintDoubleArray(aKick,Ndrift);
    debugPrintDoubleArray(aKickOutput,nsteps);
    debugPrintDoubleArray(aDriftOutput,nsteps);

    // set dummy variables
    const int Nphi0_pad=0, Nphi1_pad=0, Nphi2_pad=0;
    float_t **DUMMY;
    DUMMY = (float_t **)p_malloc(Nkick*sizeof(NULL), VARNAME(DUMMY));

    // declare internal variables and allocate memory
    particle_pos_t *x;           //positions
    particle_vel_t *p;           //momenta
    float_t *density_or_Phi;    //density and gravitational potential
    x = (particle_pos_t *)p_malloc(Np*sizeof(particle_pos_t), VARNAME(x));
    p = (particle_vel_t *)p_malloc(Np*sizeof(particle_vel_t), VARNAME(p));
    density_or_Phi = (float_t *)p_malloc(Npm*sizeof(float_t), VARNAME(density_or_Phi));

    // initialize box timers
    double BoxTimers[12]; initializePMCOLATimers(BoxTimers);

    // evolve the box with PM/tCOLA
    initialize_one_box(Np,x,p,P, EvolutionMode, ai, af, cosmo);
    evolve_one_box(P, FCs_field, density_or_Phi, Np, x, p, PsiLPT, EvolutionMode, 1, 1, BoxTimers, nsteps,ai,af,Ndrift,Nkick,aiKick,afKick,aDrift,aiDrift,afDrift,aKick,aKickOutput,aDriftOutput, ModifiedDiscretization, n_LPT, WriteSnapshots, OutputSnapshotsBase, OutputSnapshotsExt, NumFilesPerSnapshot, SnapFormat, WriteDensities, OutputDensitiesBase, OutputDensitiesExt, WriteReferenceFrame, OutputKickBase, OutputKickExt, OutputDriftBase, OutputDriftExt, ReadReferenceFrame, InputKickBase, InputKickExt, InputDriftBase, InputDriftExt, Np0,Np1,Np2, Npm0,Npm1,Npm2, L0,L1,L2, boundary_conditions, Nphi0_pad,Nphi1_pad,Nphi2_pad, (float_t **)DUMMY, cosmo);
    finalize_one_box(Np,x,p,P, af, cosmo);

    // free memory
    p_free(DUMMY, VARNAME(DUMMY));
    p_free(x, VARNAME(x));
    p_free(p, VARNAME(p));
    p_free(density_or_Phi, VARNAME(density_or_Phi));

    // print box timers
    printPMCOLATimers(BoxTimers, "Box");

    // free memory allocated for timstepping structures
    free_timestepping(aiKick,afKick,aDrift,aiDrift,afDrift,aKick,aKickOutput,aDriftOutput);
} //evolve_pm_cola
///-------------------------------------------------------------------------------------
