///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- src/rsd.c
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file rsd.c
 *  \brief Routines for redshift-space distortions
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2016-2023
 */
#include "rsd.h"
///-------------------------------------------------------------------------------------
/** @fn update_snapshot_RSD_nonlinear
 * Update snapshot with RSD, non-linear mapping
 * RSD model:
 * if doLPTSplit=false:
 *     z_obs = z_cosmo + (1+z_cosmo) * alpha1RSD * z_pec
 * else:
 *     z_obs = z_cosmo + (1+z_cosmo) * (alpha1RSD * z_pec_LPT + alpha2RSD * (z_pec-z_pec_LPT))
 */
static void update_snapshot_RSD_nonlinear(Snapshot *P, const particleID_t Np, const particle_lpt_t PsiLPT[Np], const double v_observer[3], const double L0, const double L1, const double L2, const double corner0, const double corner1, const double corner2, const double alpha1RSD, const bool doLPTSplit, const double alpha2RSD, const double cosmo[N_COSMOPAR])
{
    int Nthreads = omp_get_max_threads();
    const double a=z2a(P->header.redshift);

    // Compute required prefactors
    double* LPT_pref; double Vel1, Vel2;
    if(doLPTSplit)
    {
        LPT_pref = lpt_prefactors(a, cosmo);
        Vel1 = LPT_pref[2], Vel2 = LPT_pref[3];
    }

    // Build a look-up table for the distance-redshift relation
    sprintf(G__msg__, "Building a look-up table for the distance-redshift relation (using %d cores)...", Nthreads);
    PrintMessage(4, G__msg__);
    INDENT;

    const float_t r_step=0.5; // in Mpc/h
    const int l=(int)(floor((L0+10)/r_step)+1);
    float_t r_array[l], z_array[l];
    #pragma omp parallel for schedule(static)
    for(int k=0; k<l; k++)
    {
        float_t this_r = -5.+r_step*k;
        r_array[k] = this_r;
        z_array[k] = dcom2z(this_r, cosmo);
    }

    UNINDENT;
    sprintf(G__msg__, "Building a look-up table for the distance-redshift relation (using %d cores) done.", Nthreads);
    PrintMessage(4, G__msg__);

    // Main loop on particles
    sprintf(G__msg__, "Updating positions of particles (using %d cores)...", Nthreads);
    PrintMessage(4, G__msg__);
    INDENT;

    #pragma omp parallel for schedule(static)
    for(particleID_t mp=0; mp<Np; mp++)
    {
        float_t x0g,x1g,x2g,x0,x1,x2,v0g,v1g,v2g,v0,v1,v2,r_los,r_los_obs,v_los,zcosmo,zpec,zobs,A;

        // Real space comoving position
        x0g = P->positions[mp].Pos[0];
        x1g = P->positions[mp].Pos[1];
        x2g = P->positions[mp].Pos[2];
        x0 = x0g+corner0;
        x1 = x1g+corner1;
        x2 = x2g+corner2;
        r_los = sqrt(x0*x0 + x1*x1 + x2*x2);

        // Cosmological redshift
        zcosmo = linear_interpolation(r_array, z_array, l, r_los);

        // Real space comoving velocity
        v0g = P->velocities[mp].Vel[0];
        v1g = P->velocities[mp].Vel[1];
        v2g = P->velocities[mp].Vel[2];
        v0g /= sqrt(a);                            // converts from Gadget velocity
        v1g /= sqrt(a);                            // converts from Gadget velocity
        v2g /= sqrt(a);                            // converts from Gadget velocity

        // Add the observer's proper velocity
        v0 = v0g + v_observer[0];
        v1 = v1g + v_observer[1];
        v2 = v2g + v_observer[2];

        // Apparent redshift from peculiar velocity
        v_los = v0*x0 + v1*x1 + v2*x2;
        zpec = (v_los/r_los) / (P_c_light_cm_s / P_UnitVelocity_in_cm_per_s);

        if(doLPTSplit)
        {
            double v0g_LPT,v1g_LPT,v2g_LPT,v0_LPT,v1_LPT,v2_LPT,v_LPT_los,zpec_LPT;

            // Real space LPT velocity
            #ifndef ONLY_ZA
            v0g_LPT = Vel1 * PsiLPT[mp].Psi1[0] + Vel2 * PsiLPT[mp].Psi2[0];
            v1g_LPT = Vel1 * PsiLPT[mp].Psi1[1] + Vel2 * PsiLPT[mp].Psi2[1];
            v2g_LPT = Vel1 * PsiLPT[mp].Psi1[2] + Vel2 * PsiLPT[mp].Psi2[2];
            #else
            UNUSED(Vel2);
            v0g_LPT = Vel1 * PsiLPT[mp].Psi1[0];
            v1g_LPT = Vel1 * PsiLPT[mp].Psi1[1];
            v2g_LPT = Vel1 * PsiLPT[mp].Psi1[2];
            #endif

            // Add the observer's proper velocity to the LPT velocity
            v0_LPT = v0g_LPT + v_observer[0];
            v1_LPT = v1g_LPT + v_observer[1];
            v2_LPT = v2g_LPT + v_observer[2];

            // Apparent redshift from peculiar LPT velocity
            v_LPT_los = v0_LPT*x0 + v1_LPT*x1 + v2_LPT*x2;
            zpec_LPT = (v_LPT_los/r_los) / (P_c_light_cm_s / P_UnitVelocity_in_cm_per_s);

            // RSD model
            zobs = zcosmo + (1.+zcosmo) * (alpha1RSD * zpec_LPT + alpha2RSD * (zpec-zpec_LPT));
        }
        else
        {
            // RSD model
            zobs = zcosmo + (1.+zcosmo) * alpha1RSD * zpec; // the factor (1.+zcosmo) accounts for the Doppler effect: solution to 1+zobs = (1+zcosmo)*(1+zpec) in ΛCDM
        }
        zobs = zobs>=z_array[0] ? zobs : z_array[0]; // correction close to redshift zero

        // Observed distance, including distortions
        r_los_obs = linear_interpolation(z_array, r_array, l, zobs);
        A = r_los_obs/r_los;

        // Redshift space comoving position
        x0 *= A;
        x1 *= A;
        x2 *= A;
        x0g = x0-corner0;
        x1g = x1-corner1;
        x2g = x2-corner2;

        // Update position
        P->positions[mp].Pos[0] = periodic_wrap(x0g, 0., L0);
        P->positions[mp].Pos[1] = periodic_wrap(x1g, 0., L1);
        P->positions[mp].Pos[2] = periodic_wrap(x2g, 0., L2);
    }

    UNINDENT;
    sprintf(G__msg__, "Updating positions of particles (using %d cores) done.", Nthreads);
    PrintMessage(4, G__msg__);
} // update_snapshot_RSD_nonlinear
///-------------------------------------------------------------------------------------
/** @fn update_snapshot_RSD_linear
 * Update snapshot with RSD, linear model
 * RSD model:
 * if doLPTSplit=false:
 *     s = r + alpha1RSD * dot(v,r)*r/(H*|r|^2)
 * else:
 *     s = r + alpha1RSD * dot(vLPT,r)*r/(H*|r|^2) + alpha2RSD * dot(v-vLPT,r)*r/(H*|r|^2)
 *     (same as the previous case if alpha2RSD=alpha1RSD)
 */
static void update_snapshot_RSD_linear(Snapshot *P, const particleID_t Np, const particle_lpt_t PsiLPT[Np], const double v_observer[3], const double L0, const double L1, const double L2, const double corner0, const double corner1, const double corner2, const double alpha1RSD, const bool doLPTSplit, const double alpha2RSD, const double cosmo[N_COSMOPAR])
{
    int Nthreads = omp_get_max_threads();
    const double a=z2a(P->header.redshift);
    const double H=hubble(a, cosmo);
    const double fac1RSD = alpha1RSD * 1./H;

    // Compute required prefactors
    double fac2RSD;
    double* LPT_pref; double Vel1, Vel2;
    if(doLPTSplit)
    {
        LPT_pref = lpt_prefactors(a, cosmo);
        Vel1 = LPT_pref[2], Vel2 = LPT_pref[3];
        fac2RSD = alpha2RSD * 1./H;
    }

    // Main loop on particles
    sprintf(G__msg__, "Updating positions of particles (using %d cores)...", Nthreads);
    PrintMessage(4, G__msg__);
    INDENT;

    #pragma omp parallel for schedule(static)
    for(particleID_t mp=0; mp<Np; mp++)
    {
        double x0g,x1g,x2g,x0,x1,x2,v0g,v1g,v2g,r2_los,A;

        // Real space comoving position
        x0g = P->positions[mp].Pos[0];
        x1g = P->positions[mp].Pos[1];
        x2g = P->positions[mp].Pos[2];
        x0 = x0g+corner0;
        x1 = x1g+corner1;
        x2 = x2g+corner2;
        r2_los = x0*x0 + x1*x1 + x2*x2;

        // Real space comoving velocity
        v0g = P->velocities[mp].Vel[0];
        v1g = P->velocities[mp].Vel[1];
        v2g = P->velocities[mp].Vel[2];
        v0g /= sqrt(a); // converts from Gadget velocity
        v1g /= sqrt(a); // converts from Gadget velocity
        v2g /= sqrt(a); // converts from Gadget velocity

        if(doLPTSplit)
        {
            double v0g_LPT,v1g_LPT,v2g_LPT,v0_LPT,v1_LPT,v2_LPT,v_LPT_los,v0_res,v1_res,v2_res,v_res_los;

            // Real space LPT velocity
            #ifndef ONLY_ZA
            v0g_LPT = Vel1 * PsiLPT[mp].Psi1[0] + Vel2 * PsiLPT[mp].Psi2[0];
            v1g_LPT = Vel1 * PsiLPT[mp].Psi1[1] + Vel2 * PsiLPT[mp].Psi2[1];
            v2g_LPT = Vel1 * PsiLPT[mp].Psi1[2] + Vel2 * PsiLPT[mp].Psi2[2];
            #else
            UNUSED(Vel2);
            v0g_LPT = Vel1 * PsiLPT[mp].Psi1[0];
            v1g_LPT = Vel1 * PsiLPT[mp].Psi1[1];
            v2g_LPT = Vel1 * PsiLPT[mp].Psi1[2];
            #endif
            v0_res = v0g - v0g_LPT;
            v1_res = v1g - v1g_LPT;
            v2_res = v2g - v2g_LPT;

            // Add the observer's proper velocity to the LPT velocity
            v0_LPT = v0g_LPT + v_observer[0];
            v1_LPT = v1g_LPT + v_observer[1];
            v2_LPT = v2g_LPT + v_observer[2];

            // RSD model
            v_LPT_los = v0_LPT*x0 + v1_LPT*x1 + v2_LPT*x2;
            v_res_los = v0_res*x0 + v1_res*x1 + v2_res*x2;
            A = (fac1RSD * v_LPT_los + fac2RSD * v_res_los)/r2_los;
        }
        else
        {
            double v0,v1,v2,v_los;

            // Add the observer's proper velocity
            v0 = v0g + v_observer[0];
            v1 = v1g + v_observer[1];
            v2 = v2g + v_observer[2];

            // RSD model
            v_los = v0*x0 + v1*x1 + v2*x2;
            A = fac1RSD * v_los/r2_los;
        }

        // Redshift space comoving position
        x0g += A*x0;
        x1g += A*x1;
        x2g += A*x2;

        // Update position
        P->positions[mp].Pos[0] = periodic_wrap(x0g, 0., L0);
        P->positions[mp].Pos[1] = periodic_wrap(x1g, 0., L1);
        P->positions[mp].Pos[2] = periodic_wrap(x2g, 0., L2);
    }

    UNINDENT;
    sprintf(G__msg__, "Updating positions of particles (using %d cores) done.", Nthreads);
    PrintMessage(4, G__msg__);
} // update_snapshot_RSD_linear
///-------------------------------------------------------------------------------------
/** @fn update_snapshot_RSD
 * Wrapper to update snapshot with RSD
 */
void update_snapshot_RSD(Snapshot *P, const particleID_t Np, const particle_lpt_t PsiLPT[Np], const double v_observer[3], const bool DoNonLinearMapping, const double L0, const double L1, const double L2, const double corner0, const double corner1, const double corner2, const double alpha1RSD, const bool doLPTSplit, const double alpha2RSD, const double cosmo[N_COSMOPAR])
{
    if(DoNonLinearMapping)
    {
        PrintMessage(3, "Putting particles in redshift space (non-linear mapping)...");
        INDENT;
        update_snapshot_RSD_nonlinear(P, Np, PsiLPT, v_observer, L0, L1, L2, corner0, corner1, corner2, alpha1RSD, doLPTSplit, alpha2RSD, cosmo);
        UNINDENT;
        PrintMessage(3, "Putting particles in redshift space (non-linear mapping) done.");
    }
    else
    {
        PrintMessage(3, "Putting particles in redshift space (linear mapping)...");
        INDENT;
        update_snapshot_RSD_linear(P, Np, PsiLPT, v_observer, L0, L1, L2, corner0, corner1, corner2, alpha1RSD, doLPTSplit, alpha2RSD, cosmo);
        UNINDENT;
        PrintMessage(3, "Putting particles in redshift space (linear mapping) done.");
    }
} //update_snapshot_RSD
///-------------------------------------------------------------------------------------
