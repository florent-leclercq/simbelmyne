///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- src/mock.c
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file mock.c
 *  \brief Routine for generation of mock catalogs
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2016-2023
 */
#include "mock.h"
///-------------------------------------------------------------------------------------
const double EPSILON_GSW = 1e-5;
const double EPSILON_VOIDS = 1e-6;
///-------------------------------------------------------------------------------------
/** @fn LinearBias
 * Subroutine for a linear bias model
 */
static double LinearBias(const double delta, const double *galaxy_bias)
{
    return 1. + EPSILON_VOIDS + galaxy_bias[0]*delta;
} //LinearBias
///-------------------------------------------------------------------------------------
/** @fn PowerLawBias
 * Subroutine for a power-law bias model
 */
static double PowerLawBias(const double delta, const double *galaxy_bias)
{
    return pow(1.+ EPSILON_VOIDS + delta, galaxy_bias[0]);
} //PowerLawBias
///-------------------------------------------------------------------------------------
/** @fn GaussianMean
 * Subroutine to compute the mean for a Gaussian process
 */
static double GaussianMean(const double delta, const double surveyResponse, const double *galaxy_bias, const double galaxy_nmean)
{
    return surveyResponse*galaxy_nmean*LinearBias(delta,galaxy_bias);
} //GaussianMean
///-------------------------------------------------------------------------------------
/** @fn GaussianNoiseIntensity
 * Subroutine to compute the noise intensity for a Gaussian process
 */
static double GaussianNoiseIntensity(const double surveyResponse, const double galaxy_nmean)
{
    return 1e-2*sqrt(surveyResponse*galaxy_nmean);
} //GaussianNoiseIntensity
///-------------------------------------------------------------------------------------
/** @fn PoissonIntensity
 * Subroutine to compute the Poisson intensity field
 */
static double PoissonIntensity(const double delta, const double surveyResponse, const double *galaxy_bias, const double galaxy_nmean)
{
    return surveyResponse*galaxy_nmean*PowerLawBias(delta,galaxy_bias);
} //PoissonIntensity
///-------------------------------------------------------------------------------------
/** @fn generate_mock
 * Subroutine to generate one mock realization
 */
static void generate_mock(const int NoiseModel, const float_t *DELTA, float_t *GSW, const double *galaxy_bias, const double galaxy_nmean, const uint64_t *seedtable, const cellIndex_t N, float_t *M)
{
    #if DEBUG
        long long Ngal=0;
    #endif

    // Sets the random generators
    #if RANDOM_PARALLEL
        int Nthreads = omp_get_max_threads();
        gsl_rng *random_generator[Nthreads];
        for(int n=0; n<Nthreads; n++)
        {
            random_generator[n] = gsl_rng_alloc(gsl_rng_ranlxd2);
            gsl_rng_set(random_generator[n], (unsigned long int)seedtable[n]);
        }
    #else
        gsl_rng *random_generator;
        random_generator = gsl_rng_alloc(gsl_rng_ranlxd2);
        gsl_rng_set(random_generator, (unsigned long int)seedtable[0]);
    #endif

    // Generates galaxy number count
    #if RANDOM_PARALLEL
    #pragma omp parallel
    {
    #endif
        #if RANDOM_PARALLEL
            int rank = omp_get_thread_num(); // get rank of current process
            gsl_rng *this_rng;
            this_rng = random_generator[rank];
            #if DEBUG
                #pragma omp parallel for schedule(static) reduction(+:Ngal)
            #else
                #pragma omp parallel for schedule(static)
            #endif
        #else
            gsl_rng *this_rng;
            this_rng = random_generator;
        #endif
        for(cellIndex_t mc=0; mc<N; mc++)
        {
            switch(NoiseModel)
            {
                // Gaussian noise
                case 0:
                    if(GSW[mc]>EPSILON_GSW)
                    {
                        double mean = GaussianMean(DELTA[mc], GSW[mc], galaxy_bias, galaxy_nmean);
                        double noise_intensity = GaussianNoiseIntensity(GSW[mc], galaxy_nmean);
                        M[mc] = mean + gsl_ran_gaussian(this_rng, noise_intensity);
                    }
                    else
                        M[mc] = 0.;
                break;
                // Poisson noise
                case 1:
                    if(GSW[mc]>EPSILON_GSW)
                    {
                        double lambda = PoissonIntensity(DELTA[mc], GSW[mc], galaxy_bias, galaxy_nmean);
                        M[mc] = gsl_ran_poisson(this_rng, lambda);
                        #if DEBUG
                            Ngal += M[mc];
                        #endif
                    }
                    else
                        M[mc] = 0.;
                break;
                // Default: give error message
                default:
                    FatalError("Noise model unknown.");
                break;
            }
        }
    #if RANDOM_PARALLEL
    }
    #endif

    // Frees the random generators
    #if RANDOM_PARALLEL
        for(int n=0; n<Nthreads; n++)
        {
            gsl_rng_free(random_generator[n]);
        }
    #else
        gsl_rng_free(random_generator);
    #endif

    #if DEBUG
        debugPrintLongLong(Ngal);
    #endif
} //generate_mock
///-------------------------------------------------------------------------------------
/** @fn produce_mocks_and_summaries
 * Main routine to produce mock catalogs and summary statistics
 * @param *DELTA input density contrast field
 * @param fname_SG input survey geometry filename
 * @param fname_k_grid input k_grid filename
 * @param *InputRngStateMocks input random number generator state
 * @param WriteMocksRngState boolean: write state of the random number generator?
 * @param *OutputMocksRngState output state of the random number generator
 * @param *OutputRngStateMocks output random number generator state
 * @param NoiseModel noise model (see documentation)
 * @param N_NOISE number of noise realizations
 * @param WriteMocks boolean: write mock catalogs?
 * @param *OutputMockBase output mock catalogs filename (prefix)
 * @param *OutputMockExt output mock catalogs filename (suffix)
 * @param WriteSummaryStats boolean: write summary statistics?
 * @param *OutputSummaryStats output summary statistics filename
 */
void produce_mocks_and_summaries(const float_t *DELTA, const char *fname_SG, const char *fname_k_grid, const char *InputRngStateMocks, const int WriteMocksRngState, const char *OutputMocksRngState, const char *OutputRngStateMocks, const int NoiseModel, const int N_NOISE, const bool WriteMocks, const char *OutputMockBase, const char *OutputMockExt, const bool WriteSummaryStats, const char *OutputSummaryStats)
{
    // Load information in survey geometry file
    SurveyGeometry SG = read_survey_geometry_header(fname_SG, VARNAME(SG));
    const int N0=SG.N0,N1=SG.N1,N2=SG.N2;
    const cellIndex_t N=N0*N1*N2;
    const double corner0=SG.corner0, corner1=SG.corner1, corner2=SG.corner2;
    const double L0=SG.L0, L1=SG.L1, L2=SG.L2;
    const int N_BIAS=SG.N_BIAS, N_CAT=SG.N_CAT;

    // Load information from Fourier grid file
    FourierGrid Gk; int NUM_MODES=0;
    if(WriteSummaryStats)
    {
        Gk = read_FourierGrid(fname_k_grid, VARNAME(Gk));
        NUM_MODES = Gk.NUM_MODES;
    }
    float_t Pk[N_CAT][N_NOISE][NUM_MODES], Vk[N_CAT][N_NOISE][NUM_MODES];

    // Set variables
    int Nthreads = omp_get_max_threads();
    double galaxy_bias[N_CAT][N_NOISE][N_BIAS], galaxy_nmean[N_CAT][N_NOISE];
    Mock M = allocate_mock(N0, N1, N2, corner0, corner1, corner2, L0, L1, L2, galaxy_bias[0][0], N_BIAS, galaxy_nmean[0][0], VARNAME(M));
    char fname_mock[100];

    // Set a random generator
    gsl_rng *random_generator;
    random_generator = gsl_rng_alloc(gsl_rng_ranlxd2);
    set_rng(random_generator, InputRngStateMocks);

    // Save the random number generator state before doing anything
    if(WriteMocksRngState)
        save_rng_state(random_generator, OutputMocksRngState);

    // Produce mocks, loop on subcatalogs
    for(int ICAT=0; ICAT<N_CAT; ICAT++)
    {
        sprintf(G__msg__, "ModuleMocks: Begin subcatalog %d", ICAT);
        PrintMessage(3, G__msg__);
        INDENT;

        // Read galaxy selection window
        GalaxySelectionWindow GSW = read_galaxy_selection_window(fname_SG, ICAT, VARNAME(GSW));

        // Loop on noise realizations for the current subcatalog
        for(int INOISE=0; INOISE<N_NOISE; INOISE++)
        {
            // Draw bias/noise parameters
            for(int b=0; b<N_BIAS; b++)
            {
                galaxy_bias[ICAT][INOISE][b] = gsl_ran_gaussian(random_generator, GSW.galaxy_bias_std[b]) + GSW.galaxy_bias_mean[b];
                M.galaxy_bias[b] = galaxy_bias[ICAT][INOISE][b];
            }
            galaxy_nmean[ICAT][INOISE] = gsl_ran_gaussian(random_generator, GSW.galaxy_nmean_std) + GSW.galaxy_nmean_mean;
            M.galaxy_nmean = galaxy_nmean[ICAT][INOISE];

            // Generate seed table
            uint64_t* seedtable = generate_seed_table(random_generator, Nthreads, VARNAME(seedtable));

            // Generate mock catalog
            sprintf(G__msg__, "Generating mock catalog number %d for subcatalog %d (using %d cores)...", INOISE, ICAT, Nthreads);
            PrintMessage(4, G__msg__);

            generate_mock(NoiseModel, DELTA, GSW.galaxy_sel_window, galaxy_bias[ICAT][INOISE], galaxy_nmean[ICAT][INOISE], seedtable, N, M.data);

            sprintf(G__msg__, "Generating mock catalog number %d for subcatalog %d (using %d cores) done.", INOISE, ICAT, Nthreads);
            PrintMessage(4, G__msg__);

            // Free seedtable
            p_free(seedtable, VARNAME(seedtable));

            // Write mock catalog if desired
            if(WriteMocks)
            {
                sprintf(fname_mock, "%sc%d_n%d%s", OutputMockBase, ICAT, INOISE, OutputMockExt);
                write_mock(M, fname_mock);
            }

            // Compute summary statistics if desired
            if(WriteSummaryStats)
            {
                get_autocorrelation_fs(M.data, Gk.NUM_MODES, Gk.k_keys, Gk.k_nmodes, Pk[ICAT][INOISE], Vk[ICAT][INOISE], N0, N1, N2, L0, L1, L2, true);
            }
        } //end loop on noise realizations for the current subcatalog

        // Free galaxy selection window
        free_galaxy_selection_window(GSW);

        UNINDENT;
        sprintf(G__msg__, "ModuleMocks: End subcatalog %d", ICAT);
        PrintMessage(3, G__msg__);
    } //end loop on subcatalogs

    // Write summary statistics
    if(WriteSummaryStats)
        write_ss(Gk, N_CAT, N_NOISE, N_BIAS, galaxy_bias, galaxy_nmean, Pk, Vk, OutputSummaryStats);

    // Save the random number generator state
    save_rng_state(random_generator, OutputRngStateMocks);

    // Free the random generator
    gsl_rng_free(random_generator);

    // Free memory
    free_survey_geometry_header(SG);
    free_mock(M);
    if(WriteSummaryStats)
        free_FourierGrid(Gk);

} //produce_mocks_and_summaries
///-------------------------------------------------------------------------------------
