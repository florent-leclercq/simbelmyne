///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- src/main_sbmy.c
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file main_sbmy.c
 *  \brief Main file for Simbelmynë executable
 *  \authors Florent Leclercq
 *  \version 0.5.0
 *  \date 2020-2023
 */
#include "main.h"
///-------------------------------------------------------------------------------------
/** @fn main
 * Main function wrapper
 */
int main(int argc, char **argv)
{
    return main_sbmy(argc,argv);
} //main
///-------------------------------------------------------------------------------------
