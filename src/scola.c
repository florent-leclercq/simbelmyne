///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- src/scola.c
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file pmcola.c
 *  \brief Routines specific to sCOLA
 *  \author Florent Leclercq, Baptiste Faure
 *  \version 0.5.0
 *  \date 2017-2023
 *  \note for notations, see appendix B in Leclercq, PhD thesis 2015
 *        in Tassev, Zaldarriaga, Einsenstein 2013 (TZE)
 *        in Tassev, Einsenstein, Wandelt and Zaldarriaga 2015 (TEWZ)
 *        and Leclercq, Faure et al. 2020
 */
#include "scola.h"
///-------------------------------------------------------------------------------------
/** @fn get_index_in_superbox
 * This subroutine gets the index of a cell of the sCOLA box in a superbox
 */
static inline cellIndex_t get_index_in_superbox(const cellIndex_t mg_sub_indices[3], const int mg_sub_corner_indices[3], const int Ng0, const int Ng1, const int Ng2)
{
    int mg0 = (int)p_mod(mg_sub_indices[0] + mg_sub_corner_indices[0], Ng0);
    int mg1 = (int)p_mod(mg_sub_indices[1] + mg_sub_corner_indices[1], Ng1);
    int mg2 = (int)p_mod(mg_sub_indices[2] + mg_sub_corner_indices[2], Ng2);
    cellIndex_t mg = get_index(mg0,mg1,mg2,Ng0,Ng1,Ng2);

    return mg;
} //get_index_in_superbox
///-------------------------------------------------------------------------------------
/** @fn get_Lagrangian_Id_in_superbox
 * This subroutine gets the Lagragian Id of a particle of the sCOLA box in a superbox
 */
static inline particleID_t get_Lagrangian_Id_in_superbox(const int mp_sub_indices[3], const int mp_sub_corner_indices[3], const int Np0, const int Np1, const int Np2)
{
    int mp0 = (int)p_mod(mp_sub_indices[0] + mp_sub_corner_indices[0], Np0);
    int mp1 = (int)p_mod(mp_sub_indices[1] + mp_sub_corner_indices[1], Np1);
    int mp2 = (int)p_mod(mp_sub_indices[2] + mp_sub_corner_indices[2], Np2);
    particleID_t mp = get_Lagrangian_Id(mp0,mp1,mp2,Np0,Np1,Np2);

    return mp;
} //get_Lagrangian_Id_in_superbox
///-------------------------------------------------------------------------------------
/** @fn get_phi_in_sCOLA_box
 * This subroutine copies one the Lagrangian potentials from full box to sCOLA box
 */
static inline void get_phi_in_sCOLA_box(const int N0, const int N1, const int N2, const float_t phi[N0*N1*N2], const int N0_sCOLA, const int N1_sCOLA, const int N2_sCOLA, const int N0_pad, const int N1_pad, const int N2_pad, float_t phi_sCOLA_padded[(N0_sCOLA+2*N0_pad)*(N1_sCOLA+2*N1_pad)*(N2_sCOLA+2*N2_pad)], const int mc_sCOLA_corner_full)
{
    int mc_sCOLA_corner_full_indices[3]; get_indices(mc_sCOLA_corner_full,N0,N1,N2,mc_sCOLA_corner_full_indices);

    #pragma omp parallel for schedule(static) collapse(3)
    for(int mc0_sCOLA=-N0_pad; mc0_sCOLA<N0_sCOLA+N0_pad; mc0_sCOLA++)
        for(int mc1_sCOLA=-N1_pad; mc1_sCOLA<N1_sCOLA+N1_pad; mc1_sCOLA++)
            for(int mc2_sCOLA=-N2_pad; mc2_sCOLA<N2_sCOLA+N2_pad; mc2_sCOLA++)
            {
                cellIndex_t mc_sCOLA_pad = get_index_in_paddedbox(mc0_sCOLA,mc1_sCOLA,mc2_sCOLA,N0_sCOLA,N1_sCOLA,N2_sCOLA,N0_pad,N1_pad,N2_pad);
                cellIndex_t mc_sCOLA_indices[3] = {mc0_sCOLA,mc1_sCOLA,mc2_sCOLA};
                cellIndex_t mc = get_index_in_superbox(mc_sCOLA_indices,mc_sCOLA_corner_full_indices,N0,N1,N2);

                phi_sCOLA_padded[mc_sCOLA_pad] = phi[mc];
            }
} //get_phi_in_sCOLA_box
///-------------------------------------------------------------------------------------
/** @fn get_Psi_in_sCOLA_box
 * This subroutine copies the Lagrangian displacement vector from full box to sCOLA box
 * (it's not necessary if workers do this computation locally)
 */
static inline void get_Psi_in_sCOLA_box(const particleID_t Np, const particle_lpt_t PsiLPT[Np], const particleID_t Np_sCOLA, particle_lpt_t PsiLPT_sCOLA[Np_sCOLA], const int Np0, const int Np1, const int Np2, const int Np0_sCOLA, const int Np1_sCOLA, const int Np2_sCOLA, const particleID_t mp_sCOLA_corner_full)
{
    int mp_sCOLA_corner_full_indices[3]; get_Lagrangian_indices(mp_sCOLA_corner_full, Np0, Np1, Np2, mp_sCOLA_corner_full_indices);

    #pragma omp parallel for schedule(static)
    for(particleID_t mp_sCOLA=0; mp_sCOLA<Np_sCOLA; mp_sCOLA++)
    {
        int mp_sCOLA_indices[3]; get_Lagrangian_indices(mp_sCOLA, Np0_sCOLA, Np1_sCOLA, Np2_sCOLA, mp_sCOLA_indices);
        particleID_t mp = get_Lagrangian_Id_in_superbox(mp_sCOLA_indices,mp_sCOLA_corner_full_indices,Np0,Np1,Np2);

        PsiLPT_sCOLA[mp_sCOLA].Psi1[0] = PsiLPT[mp].Psi1[0];
        PsiLPT_sCOLA[mp_sCOLA].Psi1[1] = PsiLPT[mp].Psi1[1];
        PsiLPT_sCOLA[mp_sCOLA].Psi1[2] = PsiLPT[mp].Psi1[2];
        #ifndef ONLY_ZA
        PsiLPT_sCOLA[mp_sCOLA].Psi2[0] = PsiLPT[mp].Psi2[0];
        PsiLPT_sCOLA[mp_sCOLA].Psi2[1] = PsiLPT[mp].Psi2[1];
        PsiLPT_sCOLA[mp_sCOLA].Psi2[2] = PsiLPT[mp].Psi2[2];
        #endif
    }
} //get_Psi_in_sCOLA_box
///-------------------------------------------------------------------------------------
/** @fn get_dirichlet_bcs_LEP
 * This subroutine gets the boundary gravitational potential
 * on the PM grid given its value on the LPT mesh.
 * Uses the Linearly-Evolving Potential (LEP) approximation
 */
static inline void get_dirichlet_bcs_LEP(const int N0_sCOLA, const int N1_sCOLA, const int N2_sCOLA, const int N0_pad, const int N1_pad, const int N2_pad, const float_t phi1_sCOLA_pad[(N0_sCOLA+2*N0_pad)*(N1_sCOLA+2*N1_pad)*(N2_sCOLA+2*N2_pad)], const int Nkick, const double aDrift[Nkick], const double cosmo[N_COSMOPAR], const double d0, const double d1, const double d2, const double dpm0, const double dpm1, const double dpm2, const int Nphi0, const int Nphi1, const int Nphi2, const int Nphi0_pad, const int Nphi1_pad, const int Nphi2_pad, float_t **phiBCs)
{
    const int boundary_conditions = 3; // Dirichlet boundary conditions

    for(int nkick=0; nkick<Nkick; nkick++)
    {
        const double a=aDrift[nkick];
        const double growth_factor_a = d_plus(a_ref, a, cosmo);
        debugPrintInt(nkick);
        debugPrintDouble(growth_factor_a);

        #pragma omp parallel for schedule(static)
        for(int mg0_pm=-Nphi0_pad; mg0_pm<Nphi0+Nphi0_pad; mg0_pm++)
            for(int mg1_pm=-Nphi1_pad; mg1_pm<Nphi1+Nphi1_pad; mg1_pm++)
                for(int mg2_pm=-Nphi2_pad; mg2_pm<Nphi2+Nphi2_pad; mg2_pm++)
                {
                    const cellIndex_t mg_sCOLA_pad = get_index_in_paddedbox(mg0_pm,mg1_pm,mg2_pm,Nphi0,Nphi1,Nphi2,Nphi0_pad,Nphi1_pad,Nphi2_pad);
                    const float_t x0 = mg0_pm*dpm0;
                    const float_t x1 = mg1_pm*dpm1;
                    const float_t x2 = mg2_pm*dpm2;
                    float_t phi1 = mesh_to_particle(phi1_sCOLA_pad,N0_sCOLA,N1_sCOLA,N2_sCOLA,d0,d1,d2,x0,x1,x2,1.,boundary_conditions,N0_pad,N1_pad,N2_pad);
                    phiBCs[nkick][mg_sCOLA_pad] = growth_factor_a*phi1;
                }
    }
} //get_dirichlet_bcs_LEP
///-------------------------------------------------------------------------------------
/** @fn initialize_one_box_sCOLA
 * Initializes one sub-box to be evolved with sCOLA
 * This routine should be called before evolve_one_box()
 * @param Np_sCOLA number of particles in sCOLA box
 * @param x_sCOLA[Np_sCOLA] particle positions in sCOLA box
 * @param p_sCOLA[Np_sCOLA] particle momenta in sCOLA box
 * @param PsiLPT_sCOLA[Np_sCOLA] Lagrangian displacement field in sCOLA box
 * @param ai initial scale factor
 * @param af final scale factor
 * @param Np0 particle grid size x (full box)
 * @param Np1 particle grid size y (full box)
 * @param Np2 particle grid size z (full box)
 * @param Np0_sCOLA particle grid size x (sCOLA box)
 * @param Np1_sCOLA particle grid size y (sCOLA box)
 * @param Np2_sCOLA particle grid size z (sCOLA box)
 * @param mp_sCOLA_corner_full particleID of sCOLA box corner in the full box
 * @param L0 box size x (full box)
 * @param L1 box size y (full box)
 * @param L2 box size z (full box)
 * @param L0_sCOLA box size x (sCOLA box)
 * @param L1_sCOLA box size y (sCOLA box)
 * @param L2_sCOLA box size z (sCOLA box)
 * @param D1 first-order growth factor for 1LPT positions at ai
 * @param D2 second-order growth factor for 2LPT positions at ai
 * @param Vel1 prefactor for velocities in 1LPT at ai
 * @param Vel2 prefactor for velocities in 2LPT at ai
 * @param hubble_ai Hubble factor at ai
 * @param D_ai linear growth factor at ai
 */
static inline void initialize_one_box_sCOLA(const particleID_t Np_sCOLA, particle_pos_t x_sCOLA[Np_sCOLA], particle_vel_t p_sCOLA[Np_sCOLA], const particle_lpt_t PsiLPT_sCOLA[Np_sCOLA], const double ai, const double af, const int Np0, const int Np1, const int Np2, const int Np0_sCOLA, const int Np1_sCOLA, const int Np2_sCOLA, const particleID_t mp_sCOLA_corner_full, const double L0, const double L1, const double L2, const double D1, const double D2, const double Vel1, const double Vel2, const double hubble_ai, const double D_ai)
{
    const double dp0=L0/(double)Np0, dp1=L1/(double)Np1, dp2=L2/(double)Np2;
    int mp_sCOLA_corner_full_indices[3]; get_Lagrangian_indices(mp_sCOLA_corner_full, Np0, Np1, Np2, mp_sCOLA_corner_full_indices);

    // convert positions and velocities to code variables
    if(ai>=af)
    {
        sprintf(G__msg__, "Initial time for sCOLA evolution should be smaller than final time, given ai=%g, af=%g.", ai, af);
        FatalError(G__msg__);
    }

    #pragma omp parallel for schedule(static)
    for(particleID_t mp_sCOLA=0; mp_sCOLA<Np_sCOLA; mp_sCOLA++)
    {
        // compute kicks and drifts to be applied
        float_t Psi[3], Vel[3];
        #ifndef ONLY_ZA
        Psi[0] = -D1 * PsiLPT_sCOLA[mp_sCOLA].Psi1[0] + D2 * PsiLPT_sCOLA[mp_sCOLA].Psi2[0];
        Psi[1] = -D1 * PsiLPT_sCOLA[mp_sCOLA].Psi1[1] + D2 * PsiLPT_sCOLA[mp_sCOLA].Psi2[1];
        Psi[2] = -D1 * PsiLPT_sCOLA[mp_sCOLA].Psi1[2] + D2 * PsiLPT_sCOLA[mp_sCOLA].Psi2[2];
        Vel[0] = Vel1 * PsiLPT_sCOLA[mp_sCOLA].Psi1[0] + Vel2 * PsiLPT_sCOLA[mp_sCOLA].Psi2[0];
        Vel[1] = Vel1 * PsiLPT_sCOLA[mp_sCOLA].Psi1[1] + Vel2 * PsiLPT_sCOLA[mp_sCOLA].Psi2[1];
        Vel[2] = Vel1 * PsiLPT_sCOLA[mp_sCOLA].Psi1[2] + Vel2 * PsiLPT_sCOLA[mp_sCOLA].Psi2[2];
        #else
        UNUSED(D2); UNUSED(Vel2);
        Psi[0] = -D1 * PsiLPT_sCOLA[mp_sCOLA].Psi1[0];
        Psi[1] = -D1 * PsiLPT_sCOLA[mp_sCOLA].Psi1[1];
        Psi[2] = -D1 * PsiLPT_sCOLA[mp_sCOLA].Psi1[2];
        Vel[0] = Vel1 * PsiLPT_sCOLA[mp_sCOLA].Psi1[0];
        Vel[1] = Vel1 * PsiLPT_sCOLA[mp_sCOLA].Psi1[1];
        Vel[2] = Vel1 * PsiLPT_sCOLA[mp_sCOLA].Psi1[2];
        #endif

        // define the Lagrangian grid position in sCOLA box coordinates
        int mp_sCOLA_indices[3]; get_Lagrangian_indices(mp_sCOLA, Np0_sCOLA, Np1_sCOLA, Np2_sCOLA, mp_sCOLA_indices);
        float_t initialPosition_sCOLA[3]; get_Lagrangian_grid_position(mp_sCOLA_indices[0],mp_sCOLA_indices[1],mp_sCOLA_indices[2],dp0,dp1,dp2,initialPosition_sCOLA);

        // define initial positions
        // at this stage, we do not ensure that particles are in the sCOLA box
        x_sCOLA[mp_sCOLA].Pos[0] = initialPosition_sCOLA[0] + Psi[0];
        x_sCOLA[mp_sCOLA].Pos[1] = initialPosition_sCOLA[1] + Psi[1];
        x_sCOLA[mp_sCOLA].Pos[2] = initialPosition_sCOLA[2] + Psi[2];

        // define initial velocities
        // convert from Gadget velocity (/sqrt(a))
        // convert dx/dt to dx/da (/aH) and dx/da to p (/D_ai)
        p_sCOLA[mp_sCOLA].Vel[0] = Vel[0] /sqrt(ai) / (ai*hubble_ai) / D_ai;
        p_sCOLA[mp_sCOLA].Vel[1] = Vel[1] /sqrt(ai) / (ai*hubble_ai) / D_ai;
        p_sCOLA[mp_sCOLA].Vel[2] = Vel[2] /sqrt(ai) / (ai*hubble_ai) / D_ai;
    }
} //initialize_one_box_sCOLA
///-------------------------------------------------------------------------------------
/** @fn extract_tile_results
 * This subroutine extracts the displacement field and velocity for tile particles,
 * discarding buffer particles
 */
static inline void extract_tile_results(const particleID_t Np_sCOLA, const particle_pos_t x_sCOLA[Np_sCOLA], const particle_vel_t p_sCOLA[Np_sCOLA], const particleID_t Np_tile, particle_pos_t Psi_tile[Np_tile], particle_vel_t p_tile[Np_tile], const particleID_t mp_tile_corner_sCOLA, const int Np0, const int Np1, const int Np2, const int Np0_tile, const int Np1_tile, const int Np2_tile, const int Np0_sCOLA, const int Np1_sCOLA, const int Np2_sCOLA, const double L0, const double L1, const double L2, const double L0_sCOLA, const double L1_sCOLA, const double L2_sCOLA)
{
    const double dp0=L0/(double)Np0, dp1=L1/(double)Np1, dp2=L2/(double)Np2;
    int mp_tile_corner_sCOLA_indices[3]; get_Lagrangian_indices(mp_tile_corner_sCOLA, Np0_sCOLA, Np1_sCOLA, Np2_sCOLA, mp_tile_corner_sCOLA_indices);

    #pragma omp parallel for schedule(static)
    for(particleID_t mp_tile=0; mp_tile<Np_tile; mp_tile++)
    {
        int mp_tile_indices[3]; get_Lagrangian_indices(mp_tile, Np0_tile, Np1_tile, Np2_tile, mp_tile_indices);
        particleID_t mp_sCOLA = get_Lagrangian_Id_in_superbox(mp_tile_indices, mp_tile_corner_sCOLA_indices, Np0_sCOLA, Np1_sCOLA, Np2_sCOLA);

        // get the displacement field in the tile box from particles' final positions
        int mp_sCOLA_indices[3]; get_Lagrangian_indices(mp_sCOLA, Np0_sCOLA, Np1_sCOLA, Np2_sCOLA, mp_sCOLA_indices);
        float_t initialPosition_sCOLA[3]; get_Lagrangian_grid_position(mp_sCOLA_indices[0],mp_sCOLA_indices[1],mp_sCOLA_indices[2],dp0,dp1,dp2,initialPosition_sCOLA);
        float_t Psi[3]; get_Psi(x_sCOLA[mp_sCOLA].Pos, initialPosition_sCOLA, L0_sCOLA,L1_sCOLA,L2_sCOLA, Psi);

        // define final positions, apply period wrap in the full box
        Psi_tile[mp_tile].Pos[0] = Psi[0];
        Psi_tile[mp_tile].Pos[1] = Psi[1];
        Psi_tile[mp_tile].Pos[2] = Psi[2];

        // copy velocities from tile to full box
        p_tile[mp_tile].Vel[0] = p_sCOLA[mp_sCOLA].Vel[0];
        p_tile[mp_tile].Vel[1] = p_sCOLA[mp_sCOLA].Vel[1];
        p_tile[mp_tile].Vel[2] = p_sCOLA[mp_sCOLA].Vel[2];
    }

} //extract_tile_results
///-------------------------------------------------------------------------------------
/** @fn fill_particles_from_tile
 * This subroutine broadcasts the results of the evolution of a sCOLA box to the full box
 */
static inline void fill_particles_from_tile(const particleID_t Np_tile, const particle_pos_t Psi_tile[Np_tile], const particle_vel_t p_tile[Np_tile], const particleID_t Np, particle_pos_t x[Np], particle_vel_t p[Np], const particleID_t mp_tile_corner_full, const int Np0, const int Np1, const int Np2, const int Np0_tile, const int Np1_tile, const int Np2_tile, const double L0, const double L1, const double L2)
{
    const double dp0=L0/(double)Np0, dp1=L1/(double)Np1, dp2=L2/(double)Np2;
    int mp_tile_corner_full_indices[3]; get_Lagrangian_indices(mp_tile_corner_full, Np0, Np1, Np2, mp_tile_corner_full_indices);

    #pragma omp parallel for schedule(static)
    for(particleID_t mp_tile=0; mp_tile<Np_tile; mp_tile++)
    {
        int mp_tile_indices[3]; get_Lagrangian_indices(mp_tile, Np0_tile, Np1_tile, Np2_tile, mp_tile_indices);
        particleID_t mp = get_Lagrangian_Id_in_superbox(mp_tile_indices, mp_tile_corner_full_indices, Np0, Np1, Np2);

        // get the initial positions in the full box
        int mp_indices[3]; get_Lagrangian_indices(mp, Np0, Np1, Np2, mp_indices);
        float_t initialPosition[3]; get_Lagrangian_grid_position(mp_indices[0],mp_indices[1],mp_indices[2],dp0,dp1,dp2,initialPosition);

        // define final positions, apply period wrap in the full box
        x[mp].Pos[0] = periodic_wrap(initialPosition[0] + Psi_tile[mp_tile].Pos[0], 0., L0);
        x[mp].Pos[1] = periodic_wrap(initialPosition[1] + Psi_tile[mp_tile].Pos[1], 0., L1);
        x[mp].Pos[2] = periodic_wrap(initialPosition[2] + Psi_tile[mp_tile].Pos[2], 0., L2);

        // copy velocities from tile to full box
        p[mp].Vel[0] = p_tile[mp_tile].Vel[0];
        p[mp].Vel[1] = p_tile[mp_tile].Vel[1];
        p[mp].Vel[2] = p_tile[mp_tile].Vel[2];
    }
} //fill_particles_from_tile
///-------------------------------------------------------------------------------------
/** @fn write_tile
 * Wrapper of write_sCOLA_box_hdf5
 */
static inline void write_tile(particleID_t Np_tile, particle_pos_t Psi_tile[Np_tile], particle_vel_t p_tile[Np_tile], particleID_t mp_tile_corner_full, int Np0, int Np1, int Np2, int Np0_tile, int Np1_tile, int Np2_tile, double L0, double L1, double L2, double L0_tile, double L1_tile, double L2_tile, const int THIS_TILE, const int N_TILES_TOT, char *fname)
{
    sprintf(G__msg__, "ModulePMCOLA: Writing sCOLA results to '%s' (Box %d/%d)...", fname, THIS_TILE, N_TILES_TOT);
    PrintMessage(4, G__msg__);
    INDENT;

    write_tile_hdf5(Np_tile,Psi_tile,p_tile,mp_tile_corner_full,Np0,Np1,Np2,Np0_tile,Np1_tile,Np2_tile,L0,L1,L2,L0_tile,L1_tile,L2_tile,fname);

    UNINDENT;
    sprintf(G__msg__, "ModulePMCOLA: Writing sCOLA results to '%s' (Box %d/%d) done.", fname, THIS_TILE, N_TILES_TOT);
    PrintMessage(4, G__msg__);
} //write_tile
///-------------------------------------------------------------------------------------
/** @fn read_tile
 * Wrapper of read_sCOLA_box_hdf5
 */
static inline void read_tile(particleID_t Np_tile, particle_pos_t Psi_tile[Np_tile], particle_vel_t p_tile[Np_tile], particleID_t mp_tile_corner_full, int Np0, int Np1, int Np2, int Np0_tile, int Np1_tile, int Np2_tile, double L0, double L1, double L2, double L0_tile, double L1_tile, double L2_tile, const int THIS_TILE, const int N_TILES_TOT, char *fname)
{
    sprintf(G__msg__, "ModulePMCOLA: Reading sCOLA results from '%s' (Box %d/%d)...", fname, THIS_TILE, N_TILES_TOT);
    PrintMessage(4, G__msg__);
    INDENT;

    read_tile_hdf5(Np_tile,Psi_tile,p_tile,mp_tile_corner_full,Np0,Np1,Np2,Np0_tile,Np1_tile,Np2_tile,L0,L1,L2,L0_tile,L1_tile,L2_tile,fname);

    UNINDENT;
    sprintf(G__msg__, "ModulePMCOLA: Reading sCOLA results from '%s' (Box %d/%d) done.", fname, THIS_TILE, N_TILES_TOT);
    PrintMessage(4, G__msg__);
} //read_tile
///-------------------------------------------------------------------------------------
/** @fn have_all_results
 * Tool to check if all results are present on disk
 */
static inline bool have_all_results(const char *OutputTilesBase, const char *OutputTilesExt, const int N_TILES_TOT)
{
    int THIS_TILE=1;
    while(THIS_TILE<=N_TILES_TOT)
    {
        char fname[500]; sprintf(fname,"%s%d%s",OutputTilesBase,THIS_TILE,OutputTilesExt);
        if(!exists(fname))
            return false;
        THIS_TILE++;
    }
    return true;
} //have_all_results
///-------------------------------------------------------------------------------------
/** @fn process_scola_box
 * Principal routine to process one sCOLA box (the perfectly parallel block)
 * @param *InputLPTPotential1 filename of first-order LPT potential file
 * @param *InputLPTPotential2 filename of second-order LPT potential file
 * @param *OutputTilesBase filename of tile outputs (prefix)
 * @param *OutputTilesExt filename of tile outputs (suffix)
 * @param THIS_TILE ID of the desired tile
 * @param N_TILES number of tiles per dimension
 * @param Np_BUFFER number of particles in buffer
 * @param nsteps number of timesteps
 * @param TimeStepDistribution distribution of timesteps (see documentation)
 * @param RedshiftLPT LPT redshift before COLA evolution
 * @param RedshiftFCs redshift of final conditions
 * @param ModifiedDiscretization boolean: modified discretization for COLA? (see documentation)
 * @param n_LPT parameter for modified discretization
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param Np0 particle grid size x
 * @param Np1 particle grid size y
 * @param Np2 particle grid size z
 * @param Npm0 particle-mesh grid size x
 * @param Npm1 particle-mesh grid size y
 * @param Npm2 particle-mesh grid size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 * @param cosmo cosmological parameters
 */
void process_scola_box(const char *InputLPTPotential1, const char *InputLPTPotential2, const char *OutputTilesBase, const char *OutputTilesExt, const int THIS_TILE, const int N_TILES, const int Np_BUFFER, const int nsteps, const int TimeStepDistribution, const double RedshiftLPT, const double RedshiftFCs, const bool ModifiedDiscretization, const double n_LPT, const int N0, const int N1, const int N2, const int Np0, const int Np1, const int Np2, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, const double cosmo[N_COSMOPAR])
{
    const int N_TILES_TOT=N_TILES*N_TILES*N_TILES;
    int tile_indices[3]; get_indices(THIS_TILE-1,N_TILES,N_TILES,N_TILES,tile_indices);

    sprintf(G__msg__, "ModulePMCOLA: Begin sCOLA box %d/%d (%d,%d,%d).", THIS_TILE, N_TILES_TOT, tile_indices[0], tile_indices[1], tile_indices[2]);
    PrintMessage(3, G__msg__);
    INDENT;

    // declare variables for timers
    CPUtime startm_lpt_potential; Walltime startw_lpt_potential; double diff_t_cpu_lpt_potential, diff_t_wall_lpt_potential;
    CPUtime startm_lpt_displacements; Walltime startw_lpt_displacements; double diff_t_cpu_lpt_displacements, diff_t_wall_lpt_displacements;
    CPUtime startm_Dirichlet; Walltime startw_Dirichlet; double diff_t_cpu_Dirichlet, diff_t_wall_Dirichlet;
    CPUtime startm_output; Walltime startw_output; double diff_t_cpu_output, diff_t_wall_output;

    // set code units
    const double f0=(double)Np0/(double)N0, f1=(double)Np1/(double)N1, f2=(double)Np2/(double)N2;
    const double dp0=L0/(double)Np0, dp1=L1/(double)Np1, dp2=L2/(double)Np2;
    const double d0=L0/(double)N0, d1=L1/(double)N1, d2=(double)L2/N2;

    // set constants
    const cellIndex_t N=N0*N1*N2;
    const int EvolutionMode = 3; //sCOLA
    const int boundary_conditions = 3; // Dirichlet boundary conditions
    const double ai = z2a(RedshiftLPT);
    const double af = z2a(RedshiftFCs);
    const particleID_t Np=(particleID_t)Np0*Np1*Np2;
    const cellIndex_t Npm=Npm0*Npm1*Npm2;
    const int Nphi0_pad=2*Nghost, Nphi1_pad=2*Nghost, Nphi2_pad=2*Nghost;
    const int Ng0_pad=Nghost, Ng1_pad=Nghost, Ng2_pad=Nghost;
    char *DUMMY="0";
    Field DUMMY_FIELD;
    Snapshot DUMMY_P;

    // set public tiling variables
    const int Np0_tile = Np0/N_TILES, Np1_tile = Np1/N_TILES, Np2_tile = Np2/N_TILES; const particleID_t Np_tile = Np0_tile*Np1_tile*Np2_tile;
    const int Np0_sCOLA = Np0_tile + 2*Np_BUFFER, Np1_sCOLA = Np1_tile + 2*Np_BUFFER, Np2_sCOLA = Np2_tile + 2*Np_BUFFER; const particleID_t Np_sCOLA = Np0_sCOLA*Np1_sCOLA*Np2_sCOLA;

    const int N0_tile = (int)ceil(Np0_tile/f0), N1_tile = (int)ceil(Np1_tile/f1), N2_tile = (int)ceil(Np2_tile/f2), N_tile = N0_tile*N1_tile*N2_tile;
    const int N0_sCOLA = N0_tile + 2*(int)ceil(Np_BUFFER/f0), N1_sCOLA = N1_tile + 2*(int)ceil(Np_BUFFER/f1), N2_sCOLA =  N2_tile + 2*(int)ceil(Np_BUFFER/f2), N_sCOLA =  N0_sCOLA*N1_sCOLA*N2_sCOLA;

    const double L0_tile = (double)Np0_tile*dp0, L1_tile = (double)Np1_tile*dp1, L2_tile = (double)Np2_tile*dp2;
    const double L0_sCOLA = (double)Np0_sCOLA*dp0, L1_sCOLA = (double)Np1_sCOLA*dp1, L2_sCOLA = (double)Np2_sCOLA*dp2;

    // For efficiency, it is important to ensure that Npm0,Npm1,Npm2 are odd, so that DST-Is
    // used in the Poisson solver of sCOLA boxes have N+1 even
    const int Nphi0 = Npm0-1, Nphi1 = Npm1-1, Nphi2 = Npm2-1, Nphi = Nphi0*Nphi1*Nphi2;
    const int Nphi0_padded = Nphi0 + 2*Nphi0_pad, Nphi1_padded = Nphi1 + 2*Nphi1_pad, Nphi2_padded = Nphi2 + 2*Nphi2_pad, Nphi_padded = Nphi0_padded*Nphi1_padded*Nphi2_padded;
    const double dpm0 = L0_sCOLA/(double)Nphi0, dpm1 = L1_sCOLA/(double)Nphi1, dpm2 = L2_sCOLA/(double)Nphi2;

    const int N0_pad=max(2*Nghost,(int)ceil(Nphi0_pad*dpm0/d0)), N1_pad=max(2*Nghost,(int)ceil(Nphi1_pad*dpm1/d1)), N2_pad=max(2*Nghost,(int)ceil(Nphi2_pad*dpm2/d2));
    const int N0_sCOLA_padded = N0_sCOLA + 2*N0_pad, N1_sCOLA_padded = N1_sCOLA + 2*N1_pad, N2_sCOLA_padded = N2_sCOLA + 2*N2_pad, N_sCOLA_padded = N0_sCOLA_padded*N1_sCOLA_padded*N2_sCOLA_padded;

    debugPrintInt(N0);
    debugPrintInt(N2);
    debugPrintInt(N2);
    debugPrintInt(N);

    debugPrintInt(Np0);
    debugPrintInt(Np1);
    debugPrintInt(Np2);
    debugPrintInt(Np);

    debugPrintInt(Npm0);
    debugPrintInt(Npm1);
    debugPrintInt(Npm2);
    debugPrintInt(Npm);

    debugPrintDouble(f0);
    debugPrintDouble(f1);
    debugPrintDouble(f2);

    debugPrintDouble(dp0);
    debugPrintDouble(dp1);
    debugPrintDouble(dp2);

    debugPrintDouble(d0);
    debugPrintDouble(d1);
    debugPrintDouble(d2);

    debugPrintInt(Np0_tile);
    debugPrintInt(Np1_tile);
    debugPrintInt(Np2_tile);
    debugPrintInt(Np_tile);

    debugPrintInt(Np0_sCOLA);
    debugPrintInt(Np1_sCOLA);
    debugPrintInt(Np2_sCOLA);
    debugPrintInt(Np_sCOLA);

    debugPrintInt(N0_tile);
    debugPrintInt(N1_tile);
    debugPrintInt(N2_tile);
    debugPrintInt(N_tile);

    debugPrintInt(N0_sCOLA);
    debugPrintInt(N1_sCOLA);
    debugPrintInt(N2_sCOLA);
    debugPrintInt(N_sCOLA);

    debugPrintInt(N0_sCOLA_padded);
    debugPrintInt(N1_sCOLA_padded);
    debugPrintInt(N2_sCOLA_padded);
    debugPrintInt(N_sCOLA_padded);

    debugPrintInt(Nphi0);
    debugPrintInt(Nphi1);
    debugPrintInt(Nphi2);
    debugPrintInt(Nphi);

    debugPrintInt(Nphi0_padded);
    debugPrintInt(Nphi1_padded);
    debugPrintInt(Nphi2_padded);
    debugPrintInt(Nphi_padded);

    debugPrintDouble(dpm0);
    debugPrintDouble(dpm1);
    debugPrintDouble(dpm2);

    debugPrintDouble(L0_tile);
    debugPrintDouble(L1_tile);
    debugPrintDouble(L2_tile);

    debugPrintDouble(L0_sCOLA);
    debugPrintDouble(L1_sCOLA);
    debugPrintDouble(L2_sCOLA);

    // set tiling variables for this tile
    const int mp0_sCOLA_corner = (int)p_mod(tile_indices[0]*Np0_tile - Np0_sCOLA/2, Np0);
    const int mp1_sCOLA_corner = (int)p_mod(tile_indices[1]*Np1_tile - Np1_sCOLA/2, Np1);
    const int mp2_sCOLA_corner = (int)p_mod(tile_indices[2]*Np2_tile - Np2_sCOLA/2, Np2);

    const int mc0_sCOLA_corner = (int)floor(mp0_sCOLA_corner/f0);
    const int mc1_sCOLA_corner = (int)floor(mp1_sCOLA_corner/f1);
    const int mc2_sCOLA_corner = (int)floor(mp2_sCOLA_corner/f2);

    const double sCOLA_corner0 = periodic_wrap((double)mp0_sCOLA_corner*dp0, 0., L0);
    const double sCOLA_corner1 = periodic_wrap((double)mp1_sCOLA_corner*dp1, 0., L1);
    const double sCOLA_corner2 = periodic_wrap((double)mp2_sCOLA_corner*dp2, 0., L2);

    const int mp0_tile_corner = (int)p_mod(tile_indices[0]*Np0_tile - Np0_tile/2, Np0);
    const int mp1_tile_corner = (int)p_mod(tile_indices[1]*Np1_tile - Np1_tile/2, Np1);
    const int mp2_tile_corner = (int)p_mod(tile_indices[2]*Np2_tile - Np2_tile/2, Np2);

    const cellIndex_t mc_sCOLA_corner_full = get_index(mc0_sCOLA_corner, mc1_sCOLA_corner, mc2_sCOLA_corner, N0, N1, N2);

    const particleID_t mp_sCOLA_corner_full = get_Lagrangian_Id(mp0_sCOLA_corner, mp1_sCOLA_corner, mp2_sCOLA_corner, Np0, Np1, Np2);
    const particleID_t mp_tile_corner_full = get_Lagrangian_Id(mp0_tile_corner, mp1_tile_corner, mp2_tile_corner, Np0, Np1, Np2);
    const particleID_t mp_tile_corner_sCOLA = get_Lagrangian_Id(Np_BUFFER, Np_BUFFER, Np_BUFFER, Np0_sCOLA, Np1_sCOLA, Np2_sCOLA);

    debugPrintInt(mp0_sCOLA_corner);
    debugPrintInt(mp1_sCOLA_corner);
    debugPrintInt(mp2_sCOLA_corner);

    debugPrintInt(mc0_sCOLA_corner);
    debugPrintInt(mc1_sCOLA_corner);
    debugPrintInt(mc2_sCOLA_corner);

    debugPrintDouble(sCOLA_corner0);
    debugPrintDouble(sCOLA_corner1);
    debugPrintDouble(sCOLA_corner2);

    debugPrintInt(mp0_tile_corner);
    debugPrintInt(mp1_tile_corner);
    debugPrintInt(mp2_tile_corner);

    // set time-stepping
    int Ndrift, Nkick;
    double *aDrift, *aiDrift, *afDrift;
    double *aKick, *aiKick, *afKick;
    double *aKickOutput, *aDriftOutput;
    get_timestepping(nsteps,TimeStepDistribution,ai,af,false,false,&Nkick,&Ndrift,&aiKick,&afKick,&aDrift,&aiDrift,&afDrift,&aKick,&aKickOutput,&aDriftOutput);
    debugPrintInt(Ndrift);
    debugPrintInt(Nkick);
    debugPrintDoubleArray(aiKick,Nkick);
    debugPrintDoubleArray(afKick,Nkick);
    debugPrintDoubleArray(aDrift,Nkick);
    debugPrintDoubleArray(aiDrift,Ndrift);
    debugPrintDoubleArray(afDrift,Ndrift);
    debugPrintDoubleArray(aKick,Ndrift);
    debugPrintDoubleArray(aKickOutput,nsteps);
    debugPrintDoubleArray(aDriftOutput,nsteps);

    // compute some physical variables
    double hubble_ai = hubble(ai, cosmo);
    double D_ai = D_of_a(ai, cosmo);
    double *LPT_pref = lpt_prefactors(ai, cosmo);
    double D1 = LPT_pref[0], D2 = LPT_pref[1], Vel1 = LPT_pref[2], Vel2 = LPT_pref[3];

    debugPrintDouble(D1);
    debugPrintDouble(D2);
    debugPrintDouble(Vel1);
    debugPrintDouble(Vel2);

    Vel1 *= sqrt(ai);    // convert to Gadget velocity
    Vel2 *= sqrt(ai);    // convert to Gadget velocity

    // declare tile private variables
    particle_pos_t *x_sCOLA;        // positions in the sCOLA box
    particle_vel_t *p_sCOLA;        // momenta in the sCOLA box
    particle_lpt_t *PsiLPT_sCOLA;   // (tCOLA) LPT displacements of the full box, downgraded to the sCOLA box
    float_t *phi1_sCOLA_pad;                 // first-order Lagrangian potential in the sCOLA box
    float_t *density_and_Phi_sCOLA_pad;      // density and gravitational potential in the sCOLA box
    particle_pos_t *Psi_tile;        // final displacement field in the tile
    particle_vel_t *p_tile;          // momenta in the tile

    // cut LPT potentials in sCOLA box from full box
    StartTimers(&startm_lpt_potential, &startw_lpt_potential);

    int mc_sCOLA_corner_full_indices[3]; get_indices(mc_sCOLA_corner_full,N0,N1,N2,mc_sCOLA_corner_full_indices);
    int offset_0=mc_sCOLA_corner_full_indices[0]-N0_pad;
    int offset_1=mc_sCOLA_corner_full_indices[1]-N1_pad;
    int offset_2=mc_sCOLA_corner_full_indices[2]-N2_pad;
    debugPrintInt(N0_sCOLA_padded);
    debugPrintInt(N1_sCOLA_padded);
    debugPrintInt(N2_sCOLA_padded);
    debugPrintInt(offset_0);
    debugPrintInt(offset_1);
    debugPrintInt(offset_2);

    Field PHI1_CHUNK = read_field_chunk_3D_periodic(InputLPTPotential1, N0_sCOLA_padded,N1_sCOLA_padded,N2_sCOLA_padded, offset_0,offset_1,offset_2, VARNAME(PHI1_CHUNK)); phi1_sCOLA_pad=PHI1_CHUNK.data;
    debugPrintFloattArray(phi1_sCOLA_pad,N_sCOLA_padded);
    float_t *phi2_sCOLA_pad;                 //second-order Lagrangian potential in the sCOLA box
    #ifndef ONLY_ZA
    Field PHI2_CHUNK = read_field_chunk_3D_periodic(InputLPTPotential2, N0_sCOLA_padded,N1_sCOLA_padded,N2_sCOLA_padded, offset_0,offset_1,offset_2, VARNAME(PHI2_CHUNK)); phi2_sCOLA_pad=PHI2_CHUNK.data;
    debugPrintFloattArray(phi2_sCOLA_pad,N_sCOLA_padded);
    #else
    UNUSED(InputLPTPotential2);
    #endif

    StopTimers(startm_lpt_potential, startw_lpt_potential, &diff_t_cpu_lpt_potential, &diff_t_wall_lpt_potential);

    // compute the LPT displacements locally by real-space differentiating the LPT displacements
    PsiLPT_sCOLA = (particle_lpt_t *)p_malloc(Np_sCOLA*sizeof(particle_lpt_t), VARNAME(PsiLPT_sCOLA));
    StartTimers(&startm_lpt_displacements, &startw_lpt_displacements);
    compute_LPT_displacements(Np_sCOLA,PsiLPT_sCOLA, N0_sCOLA,N1_sCOLA,N2_sCOLA, L0_sCOLA,L1_sCOLA,L2_sCOLA, Np0_sCOLA,Np1_sCOLA,Np2_sCOLA, boundary_conditions, N0_pad,N1_pad,N2_pad, phi1_sCOLA_pad, phi2_sCOLA_pad, Ng0_pad,Ng1_pad,Ng2_pad);
    StopTimers(startm_lpt_displacements, startw_lpt_displacements, &diff_t_cpu_lpt_displacements, &diff_t_wall_lpt_displacements);

    // compute the Dirichlet boundary conditions to be used in the evolution
    StartTimers(&startm_Dirichlet, &startw_Dirichlet);
    float_t **phiBCs;
    phiBCs = (float_t **)p_malloc(Nkick*sizeof(float_t*), VARNAME(phiBCs));
    for(int nkick=0; nkick<Nkick; nkick++)
        phiBCs[nkick] = (float_t *)p_malloc(Nphi_padded*sizeof(float_t), VARNAME(phiBCs[nkick]));

    get_dirichlet_bcs_LEP(N0_sCOLA,N1_sCOLA,N2_sCOLA,N0_pad,N1_pad,N2_pad,phi1_sCOLA_pad,Nkick,aDrift,cosmo,d0,d1,d2,dpm0,dpm1,dpm2,Nphi0,Nphi1,Nphi2,Nphi0_pad,Nphi1_pad,Nphi2_pad,phiBCs);

    StopTimers(startm_Dirichlet, startw_Dirichlet, &diff_t_cpu_Dirichlet, &diff_t_wall_Dirichlet);

    // free LPT potential fields
    free_field(PHI1_CHUNK, VARNAME(PHI1_CHUNK));
    #ifndef ONLY_ZA
    free_field(PHI2_CHUNK, VARNAME(PHI2_CHUNK));
    #endif

    // initialize box timers
    double BoxTimers[12]; initializePMCOLATimers(BoxTimers);

    // initialize the sCOLA box
    x_sCOLA = (particle_pos_t *)p_malloc(Np_sCOLA*sizeof(particle_pos_t), VARNAME(x_sCOLA));
    p_sCOLA = (particle_vel_t *)p_malloc(Np_sCOLA*sizeof(particle_vel_t), VARNAME(p_sCOLA));
    initialize_one_box_sCOLA(Np_sCOLA,x_sCOLA,p_sCOLA,PsiLPT_sCOLA, ai,af, Np0,Np1,Np2, Np0_sCOLA,Np1_sCOLA,Np2_sCOLA, mp_sCOLA_corner_full, L0,L1,L2, D1,D2,Vel1,Vel2,hubble_ai,D_ai);

    // evolve the sCOLA box
    PrintMessage(4, "ModulePMCOLA: Evolving with sCOLA...");
    INDENT;

    density_and_Phi_sCOLA_pad = (float_t*)p_malloc(Nphi_padded*sizeof(float_t), VARNAME(density_and_Phi_sCOLA_pad));

    evolve_one_box(&DUMMY_P, DUMMY_FIELD, density_and_Phi_sCOLA_pad, Np_sCOLA, x_sCOLA, p_sCOLA, PsiLPT_sCOLA, EvolutionMode, THIS_TILE, N_TILES_TOT, BoxTimers, nsteps,ai,af,Ndrift,Nkick,aiKick,afKick,aDrift,aiDrift,afDrift,aKick,aKickOutput,aDriftOutput, ModifiedDiscretization, n_LPT, false, DUMMY, DUMMY, 0, 0, false, DUMMY, DUMMY, 0, DUMMY, DUMMY, DUMMY, DUMMY, 0, DUMMY, DUMMY, DUMMY, DUMMY, Np0_sCOLA,Np1_sCOLA,Np2_sCOLA, Nphi0,Nphi1,Nphi2, L0_sCOLA,L1_sCOLA,L2_sCOLA, boundary_conditions, Nphi0_pad,Nphi1_pad,Nphi2_pad, phiBCs, cosmo);

    p_free(density_and_Phi_sCOLA_pad, VARNAME(density_and_Phi_sCOLA_pad));
    p_free(PsiLPT_sCOLA, VARNAME(PsiLPT_sCOLA));
    for(int nkick=0; nkick<Nkick; nkick++)
        p_free(phiBCs[nkick], VARNAME(phiBCs[nkick]));
    p_free(phiBCs, VARNAME(phiBCs));

    UNINDENT;
    PrintMessage(4, "ModulePMCOLA: Evolving with sCOLA done.");

    Psi_tile = (particle_pos_t *)p_malloc(Np_tile*sizeof(particle_pos_t), VARNAME(Psi_tile));
    p_tile = (particle_vel_t *)p_malloc(Np_tile*sizeof(particle_vel_t), VARNAME(p_tile));

    // extract results for tile particles only, discard buffer particles
    extract_tile_results(Np_sCOLA,x_sCOLA,p_sCOLA, Np_tile,Psi_tile,p_tile, mp_tile_corner_sCOLA, Np0,Np1,Np2, Np0_tile,Np1_tile,Np2_tile, Np0_sCOLA,Np1_sCOLA,Np2_sCOLA, L0,L1,L2, L0_sCOLA,L1_sCOLA,L2_sCOLA);

    p_free(x_sCOLA, VARNAME(x_sCOLA));
    p_free(p_sCOLA, VARNAME(p_sCOLA));

    // write results to disk
    StartTimers(&startm_output, &startw_output);
    char fname[500]; sprintf(fname,"%s%d%s",OutputTilesBase,THIS_TILE,OutputTilesExt);
    write_tile(Np_tile,Psi_tile,p_tile, mp_tile_corner_full, Np0,Np1,Np2, Np0_tile,Np1_tile,Np2_tile, L0,L1,L2, L0_tile,L1_tile,L2_tile, THIS_TILE, N_TILES_TOT, fname);
    StopTimers(startm_output, startw_output, &diff_t_cpu_output, &diff_t_wall_output);

    p_free(Psi_tile, VARNAME(Psi_tile));
    p_free(p_tile, VARNAME(p_tile));

    UNINDENT;
    sprintf(G__msg__, "ModulePMCOLA: End sCOLA box %d/%d (%d,%d,%d).", THIS_TILE, N_TILES_TOT, tile_indices[0], tile_indices[1], tile_indices[2]);
    PrintMessage(3, G__msg__);

    // print box timers
    char type[50];
    sprintf(type, "Box %d/%d: LPT potentials", THIS_TILE, N_TILES_TOT);
    PrintTimers(type, diff_t_cpu_lpt_potential, diff_t_wall_lpt_potential);
    sprintf(type, "Box %d/%d: LPT displacements", THIS_TILE, N_TILES_TOT);
    PrintTimers(type, diff_t_cpu_lpt_displacements, diff_t_wall_lpt_displacements);
    sprintf(type, "Box %d/%d: Dirichlet boundary conditions", THIS_TILE, N_TILES_TOT);
    PrintTimers(type, diff_t_cpu_Dirichlet, diff_t_wall_Dirichlet);
    sprintf(type, "Box %d/%d", THIS_TILE, N_TILES_TOT);
    printPMCOLATimers(BoxTimers, type);
    sprintf(type, "Box %d/%d: Output", THIS_TILE, N_TILES_TOT);
    PrintTimers(type, diff_t_cpu_output, diff_t_wall_output);
    sprintf(type, "Box %d/%d: Total sCOLA operations", THIS_TILE, N_TILES_TOT);
    PrintTimers(type, diff_t_cpu_lpt_potential+diff_t_cpu_lpt_displacements+diff_t_cpu_Dirichlet+diff_t_cpu_output, diff_t_wall_lpt_potential+diff_t_wall_lpt_displacements+diff_t_wall_Dirichlet+diff_t_wall_output);
    sprintf(type, "Box %d/%d: Total", THIS_TILE, N_TILES_TOT);
    PrintTimers(type, diff_t_cpu_lpt_potential+diff_t_cpu_lpt_displacements+diff_t_cpu_Dirichlet+diff_t_cpu_output+BoxTimers[0]+BoxTimers[2]+BoxTimers[4]+BoxTimers[6]+BoxTimers[8]+BoxTimers[10] , diff_t_wall_lpt_potential+diff_t_wall_lpt_displacements+diff_t_wall_Dirichlet+diff_t_wall_output+BoxTimers[1]+BoxTimers[3]+BoxTimers[5]+BoxTimers[7]+BoxTimers[9]+BoxTimers[11]);

    // free memory allocated for timestepping structures
    free_timestepping(aiKick,afKick,aDrift,aiDrift,afDrift,aKick,aKickOutput,aDriftOutput);
} //process_scola_box
///-------------------------------------------------------------------------------------
/** @fn evolve_scola
 * Principal routine to evolve particles forward with sCOLA
 * @param *P input/output snapshot
 * @param ICs_field initial conditions
 * @param FCs_field final conditions (unused)
 * @param PsiLPT[Np] Lagrangian displacement field (for COLA)
 * @param *OutputLPTPotential1 filename of first-order LPT potential file
 * @param *OutputLPTPotential2 filename of second-order LPT potential file
 * @param *OutputTilesBase filename of tile outputs (prefix)
 * @param *OutputTilesExt filename of tile outputs (suffix)
 * @param N_TILES number of tiles per dimension
 * @param Np_BUFFER number of particles in buffer
 * @param nsteps number of timesteps
 * @param TimeStepDistribution distribution of timesteps (see documentation)
 * @param RedshiftLPT LPT redshift before COLA evolution
 * @param RedshiftFCs redshift of final conditions
 * @param ModifiedDiscretization boolean: modified discretization for COLA? (see documentation)
 * @param n_LPT parameter for modified discretization
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param Np0 particle grid size x
 * @param Np1 particle grid size y
 * @param Np2 particle grid size z
 * @param Npm0 particle-mesh grid size x
 * @param Npm1 particle-mesh grid size y
 * @param Npm2 particle-mesh grid size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 * @param corner0 x-position of the corner
 * @param corner1 y-position of the corner
 * @param corner2 z-position of the corner
 * @param cosmo cosmological parameters
 */
void evolve_scola(Snapshot *P, Field *ICs_field, Field *FCs_field, particle_lpt_t *PsiLPT, const char *OutputLPTPotential1, const char *OutputLPTPotential2, const char *OutputTilesBase, const char *OutputTilesExt, const int N_TILES, const int Np_BUFFER, const int nsteps, const int TimeStepDistribution, const double RedshiftLPT, const double RedshiftFCs, const bool ModifiedDiscretization, const double n_LPT, const int N0, const int N1, const int N2, const int Np0, const int Np1, const int Np2, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, const double corner0, const double corner1, const double corner2, const double cosmo[N_COSMOPAR])
{
    UNUSED(nsteps);UNUSED(TimeStepDistribution);UNUSED(RedshiftLPT);UNUSED(RedshiftFCs);UNUSED(ModifiedDiscretization);UNUSED(n_LPT);
    UNUSED(Npm0);UNUSED(Npm1);UNUSED(Npm2);

    // set constants
    const cellIndex_t N=N0*N1*N2;
    const particleID_t Np=(particleID_t)Np0*Np1*Np2;
    const int N_TILES_TOT=N_TILES*N_TILES*N_TILES;
    const double af = z2a(RedshiftFCs);
    const float_t *ICs=ICs_field->data;

    // declare variables for timers
    CPUtime startm; Walltime startw; double diff_t_cpu, diff_t_wall;

    // main switch
    if(!have_all_results(OutputTilesBase,OutputTilesExt,N_TILES_TOT))
    { // some results are mising, so we compute LPT potentials and invite execution of the sCOLA box processor

        debugPrintFloattArray_(VARNAME(ICs),(float_t *)ICs,N);

        // allocate memory for LPT potentials
        float_t *phi1, *phi2;
        phi1 = (float_t *)p_malloc(N*sizeof(float_t), VARNAME(phi1));
        #ifndef ONLY_ZA
        phi2 = (float_t *)p_malloc(N*sizeof(float_t), VARNAME(phi2));
        #else
        UNUSED(OutputLPTPotential2);
        #endif

        // get LPT potentials in the full box and write them to disk
        StartTimers(&startm, &startw);
        get_potentials_periodic(ICs, phi1, phi2, N0, N1, N2, L0, L1, L2);
        Field PHI1 = scalar_field_from_data(phi1, N0, N1, N2, corner0, corner1, corner2, L0, L1, L2, a_ref);
        write_scalar_field_3D(PHI1, OutputLPTPotential1);

        #ifndef ONLY_ZA
        Field PHI2 = scalar_field_from_data(phi2, N0, N1, N2, corner0, corner1, corner2, L0, L1, L2, a_ref);
        write_scalar_field_3D(PHI2, OutputLPTPotential2);
        #endif
        StopTimers(startm, startw, &diff_t_cpu, &diff_t_wall);
        PrintTimers("tCOLA LPT potentials", diff_t_cpu, diff_t_wall);

        // free memory
        p_free(phi1, VARNAME(phi1));
        #ifndef ONLY_ZA
        p_free(phi2, VARNAME(phi2));
        #endif

        // end sCOLA initialization
        PrintModule("ModulePMCOLA: Finished initializing sCOLA simulation.");
        PrintModule("ModulePMCOLA: Now please run the 'scola' executable for each sCOLA box before calling simbelmyne again.");
        p_free(PsiLPT, VARNAME(PsiLPT));
        free_snapshot(*P, VARNAME(P));
        free_field(*FCs_field, VARNAME(FCs_field));
        free_field(*ICs_field, VARNAME(ICs_field));
        EndSuccess();
        exit(0);
    }
    else
    { // all results are present on disk, so we gather the results and finalize the full run

        // set code units
        const double dp0=L0/(double)Np0, dp1=L1/(double)Np1, dp2=L2/(double)Np2;

        // set public tiling variables
        const int Np0_tile = Np0/N_TILES, Np1_tile = Np1/N_TILES, Np2_tile = Np2/N_TILES; const particleID_t Np_tile = Np0_tile*Np1_tile*Np2_tile;
        const int Np0_sCOLA = Np0_tile + 2*Np_BUFFER, Np1_sCOLA = Np1_tile + 2*Np_BUFFER, Np2_sCOLA = Np2_tile + 2*Np_BUFFER;

        const double L0_tile = (double)Np0_tile*dp0, L1_tile = (double)Np1_tile*dp1, L2_tile = (double)Np2_tile*dp2;

        // declare public internal variables
        particle_pos_t *x;       //positions in the full box
        particle_vel_t *p;       //momenta in the full box
        x = (particle_pos_t *)p_malloc(Np*sizeof(particle_pos_t), VARNAME(x));
        p = (particle_vel_t *)p_malloc(Np*sizeof(particle_vel_t), VARNAME(p));

        // main loop on tiles to get back the results
        StartTimers(&startm, &startw);
        for(int THIS_TILE=1; THIS_TILE<=N_TILES_TOT; THIS_TILE++)
        {
            int tile_indices[3]; get_indices(THIS_TILE-1,N_TILES,N_TILES,N_TILES,tile_indices);

            // declare tile private variables
            particle_pos_t *Psi_tile;        //final displacement field in the tile
            particle_vel_t *p_tile;          //momenta in the tile
            Psi_tile = (particle_pos_t *)p_malloc(Np_tile*sizeof(particle_pos_t), VARNAME(Psi_tile));
            p_tile = (particle_vel_t *)p_malloc(Np_tile*sizeof(particle_vel_t), VARNAME(p_tile));

            // set constants
            const int mp0_sCOLA_corner = (int)p_mod(tile_indices[0]*Np0_tile - Np0_sCOLA/2, Np0);
            const int mp1_sCOLA_corner = (int)p_mod(tile_indices[1]*Np1_tile - Np1_sCOLA/2, Np1);
            const int mp2_sCOLA_corner = (int)p_mod(tile_indices[2]*Np2_tile - Np2_sCOLA/2, Np2);

            const int mp0_tile_corner = (int)p_mod(tile_indices[0]*Np0_tile - Np0_tile/2, Np0);
            const int mp1_tile_corner = (int)p_mod(tile_indices[1]*Np1_tile - Np1_tile/2, Np1);
            const int mp2_tile_corner = (int)p_mod(tile_indices[2]*Np2_tile - Np2_tile/2, Np2);

            const particleID_t mp_tile_corner_full = get_Lagrangian_Id(mp0_tile_corner, mp1_tile_corner, mp2_tile_corner, Np0, Np1, Np2);

            debugPrintInt(mp0_sCOLA_corner);
            debugPrintInt(mp1_sCOLA_corner);
            debugPrintInt(mp2_sCOLA_corner);

            debugPrintInt(mp0_tile_corner);
            debugPrintInt(mp1_tile_corner);
            debugPrintInt(mp2_tile_corner);

            // read results from the curent tile from disk
            char fname[500]; sprintf(fname,"%s%d%s",OutputTilesBase,THIS_TILE,OutputTilesExt);
            read_tile(Np_tile,Psi_tile,p_tile, mp_tile_corner_full, Np0,Np1,Np2, Np0_tile,Np1_tile,Np2_tile, L0,L1,L2, L0_tile,L1_tile,L2_tile, THIS_TILE, N_TILES_TOT, fname);

            // fill Snapshot structure
            fill_particles_from_tile(Np_tile,Psi_tile,p_tile, Np,x,p, mp_tile_corner_full, Np0,Np1,Np2, Np0_tile,Np1_tile,Np2_tile, L0,L1,L2);

            p_free(Psi_tile, VARNAME(Psi_tile));
            p_free(p_tile, VARNAME(p_tile));
        } //end loop on tiles
        StopTimers(startm, startw, &diff_t_cpu, &diff_t_wall);
        PrintTimers("sCOLA Untiling", diff_t_cpu, diff_t_wall);

        // finalize full box
        finalize_one_box(Np,x,p,P, af, cosmo);
        p_free(x, VARNAME(x));
        p_free(p, VARNAME(p));
    }
} //evolve_scola
///-------------------------------------------------------------------------------------
