#!/usr/bin/env python

"""Script to automatically bump a Simbelmynë version
"""

__author__  = "Florent Leclercq"
__version__ = "SBMY_VERSION"
__date__    = "2018-SBMY_YEAR"
__license__ = "GPLv3"

# Define the files to be bumped
c_paths = ["include/*.h", "libSBMY/include/*.h", "libSBMY/src/*.c","src/*.c"]
python_paths = ["*.py", "examples/*.py", "pysbmy/*.py", "pysbmy/tools/*", "scripts/*.py", "extensions/doc/*.py"]
makefile_paths = ["config.mk", "makefile", "examples/makefile"]
excludes = ["pysbmy/tools/upsample_wn.rng"]

import argparse
parser = argparse.ArgumentParser(description="Bumps a version of Simbelmynë for release.")
parser.add_argument("version", help="Version number")
args = parser.parse_args()

def bump_version(version):
    """Bumps Simbelmynë source files to a given version

    Parameters
    ----------
    version (string) : the version number, in the format
    {major}.{minor}.{release}

    """
    def replace_in_file(filepath,text_to_search,replacement_text):
        import fileinput
        result=False
        with fileinput.FileInput(filepath, inplace=True) as file:
            for line in file:
                if text_to_search in line:
                    result=True
                print(line.replace(text_to_search, replacement_text, 1), end='')
        return result

    def get_current_year():
        import datetime
        now = datetime.datetime.now()
        return now.year

    def bump_version_year(filepath,filename,version,year):
        a = replace_in_file(filepath, "{}-SBMY_YEAR".format(str(year)), str(year))
        b = replace_in_file(filepath, "SBMY_YEAR", str(year))
        if not a and not b:
            from pysbmy.utils import PrintWarning
            PrintWarning("Couldn't bump file '{}': couldn't find 'SBMY_YEAR'".format(filename))
        c = replace_in_file(filepath, "SBMY_VERSION", version)
        if not c:
            from pysbmy.utils import PrintWarning
            PrintWarning("Couldn't bump file '{}': couldn't find 'SBMY_VERSION'".format(filename))

    def line_prepender(filepath, line):
        result=False
        with open(filepath, 'r+') as f:
            content = f.read()
            f.seek(0, 0)
            f.write(line.rstrip('\r\n') + '\n' + content)
            result=True
        return result

    def add_header(filepath,filename,filetype,version,year):
        if filetype=="makefile":
            header = """#-------------------------------------------------------------------------------------
# Simbelmynë v{} -- {}
# Copyright (C) 2012-{} Florent Leclercq.
#
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# The text of the license is located in the root directory of the source package.""".format(version,filename,str(year))
            if not line_prepender(filepath, header):
                from pysbmy.utils import PrintWarning
                PrintWarning("Couldn't bump file '{}': couldn't prepender header".format(filename))
                return False

        elif filetype=="python":
            old_header = """# -*- coding: latin-1 -*-"""
            header = """# -*- coding: latin-1 -*-
#-------------------------------------------------------------------------------------
# Simbelmynë v{} -- {}
# Copyright (C) 2012-{} Florent Leclercq.
#
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# The text of the license is located in the root directory of the source package.
#-------------------------------------------------------------------------------------""".format(version,filename,str(year))
            if not replace_in_file(filepath, old_header, header):
                from pysbmy.utils import PrintWarning
                PrintWarning("Couldn't bump file '{}': couldn't find '{}' to prepender header".format(filename,old_header))
                return False

        elif filetype=="c":
            old_header = "/*! \\file"
            header = """/*! Simbelmynë v{} -- {}
 * Copyright (C) 2012-{} Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \\file""".format(version,filename,str(year))
            if not replace_in_file(filepath, old_header, header):
                from pysbmy.utils import PrintWarning
                PrintWarning("Couldn't bump file '{}': couldn't find '{}' to prepender header".format(filename,old_header))
                return False
        return True

    def bump_file(f, filetype, version, year):
        from os.path import join, dirname, realpath
        from pysbmy.utils import PrintMessage
        SBMY_ROOT=realpath(dirname(realpath(__file__))+"/../")
        bump_version_year(join(SBMY_ROOT,f), f, version, year)
        if add_header(join(SBMY_ROOT,f), f, filetype, version, year):
            PrintMessage(4,"Bumped '{}' to version {}".format(f,version))

    def get_filelist(paths):
        import glob
        from os.path import join, relpath, dirname, realpath
        SBMY_ROOT=realpath(dirname(realpath(__file__))+"/../")
        files = []
        for path in paths:
            files.extend(glob.glob(join(SBMY_ROOT,path)))
        files = [relpath(f, SBMY_ROOT) for f in files]
        return files

    from pysbmy.utils import PrintInfo

    PrintInfo("This is the Simbelmynë bump version script.")

    year = get_current_year()

    # loop on C files
    for f in get_filelist(c_paths):
        if f not in excludes:
            bump_file(f, "c", version, year)

    # loop on python files
    for f in get_filelist(python_paths):
        if f not in excludes:
            bump_file(f, "python", version, year)

    # loop on makefiles
    for f in get_filelist(makefile_paths):
        if f not in excludes:
            bump_file(f, "makefile", version, year)

    PrintInfo("Files modified successfully, version bumped to {}.".format(version))

bump_version(args.version)
