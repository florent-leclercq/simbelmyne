///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/src/poisson_solvers.c
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file poisson_solvers.c
 *  \brief Routines to solve the Poisson equation with different boundary conditions
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2019-2023
 */
#include "poisson_solvers.h"
///-------------------------------------------------------------------------------------
/** @fn dgf_laplace_DFT
 * Subroutine for discrete Green's function for the Laplace operator in Fourier space
*/
double dgf_laplace_DFT(const int mf0, const int mf1, const int mf2, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, const bool smooth, const double normphi)
{
    const double dpm0=L0/(double)Npm0;
    const double dpm1=L1/(double)Npm1;
    const double dpm2=L2/(double)Npm2;
    const double L=pow(L0*L1*L2,1./3.);
    const double asmthsq = 2.*M_PI/L * ASMTH * 2.*M_PI/L * ASMTH;

    double Greens = 1.;

    // Jeans's "swindle": put the zero-mode to zero by hand
    if(mf0==0 && mf1==0 && mf2==0)
        Greens = 0.;
    // Green's function
    else
    {
        // calculate k-mode
        double k0=get_kmode(mf0,Npm0,L0);
        double k1=get_kmode(mf1,Npm1,L1);
        double k2=get_kmode(mf2,Npm2,L2);

        // invert Laplacian in Fourier space
        Greens *= 1./laplace_DFT(k0,k1,k2,dpm0,dpm1,dpm2);

        // smooth short-range forces, if needed
        if(smooth)
        {
            double ksquared = k0*k0+k1*k1+k2*k2;
            Greens *= exp(-ksquared * asmthsq);
        }

        // multiply by a physical prefactor, if needed
        Greens *= normphi;
    }
    return Greens;
} //dgf_laplace_DFT
///-------------------------------------------------------------------------------------
/** @fn multiply_by_dgf_laplace_DFT
 * This routine multiplies by a Green's function in Fourier space
 * Assumes periodic boundary conditions
 * @param *AUX input field in Fourier space
 * @param Npm0 mesh size x
 * @param Npm1 mesh size y
 * @param Npm2 mesh size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 * @param smooth smooth gravitational potential?
 * @param normphi normalization factor of the potential
 */
void multiply_by_dgf_laplace_DFT(fftw_complex_t *AUX, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, const bool smooth, const double normphi)
{
    const int Npm2_HC=(Npm2/2+1);
    #pragma omp parallel for schedule(static) collapse(3)
    for(int mf0=0; mf0<Npm0; mf0++)
        for(int mf1=0; mf1<Npm1; mf1++)
            for(int mf2=0; mf2<Npm2_HC; mf2++)
            {
                // multiply by discrete Green's function
                fourierMode_t mf = get_FourierMode_Id(mf0,mf1,mf2,Npm0,Npm1,Npm2);
                AUX[mf] *= dgf_laplace_DFT(mf0,mf1,mf2, Npm0,Npm1,Npm2, L0,L1,L2, smooth, normphi);
            }
} //multiply_by_dgf_laplace_DFT
///-------------------------------------------------------------------------------------
/** @fn solve_poisson_DFT
 * Solves the Poisson equation on the grid, replaces the input field delta
 * This routine assumes periodic boundary conditions
 * @param *IN input/output
 * @param Npm0 particle mesh size x
 * @param Npm1 particle mesh size y
 * @param Npm2 particle mesh size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 * @param smooth smooth gravitational potential?
 */
void solve_poisson_DFT(float_t *IN, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, const bool smooth)
{
    int Nthreads = omp_get_max_threads();
    const int Npm2_HC=(Npm2/2+1);
    const int Npm_HC=Npm0*Npm1*Npm2_HC;

    // transform input field to Fourier space
    fftw_complex_t *AUX; AUX = (fftw_complex_t *)p_fftw_malloc(Npm_HC*sizeof(fftw_complex_t), VARNAME(AUX));
    FFT_r2c_3d(Npm0, Npm1, Npm2, IN, AUX, Nthreads);

    // mutiply by discrete Green function in Fourier space
    multiply_by_dgf_laplace_DFT(AUX, Npm0, Npm1, Npm2, L0, L1, L2, smooth, 1.);

    // transform back to real space
    FFT_c2r_3d(Npm0, Npm1, Npm2, AUX, IN, Nthreads);

    // free memory
    p_fftw_free(AUX, VARNAME(AUX));
} //solve_poisson_DFT
///-------------------------------------------------------------------------------------
/** @fn solve_poisson_periodic
 * Solves the Poisson equation on the grid, replaces the input field delta
 * The quantity returned is Delta^-1 delta without any physical prefactor!
 * This routine assumes periodic boundary conditions
 * (see appendix B of Leclercq, PhD thesis, 2015)
 * @param *density input: the density contrast on the grid, output: phi on the grid
 * @param Npm0 particle mesh size x
 * @param Npm1 particle mesh size y
 * @param Npm2 particle mesh size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 * @param smooth smooth gravitational potential?
 */
void solve_poisson_periodic(float_t *density, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, const bool smooth)
{
    int Nthreads = omp_get_max_threads();
    sprintf(G__msg__, "Getting gravitational potential, periodic boundary conditions (using %d cores)...", Nthreads);
    PrintMessage(4, G__msg__);
    INDENT;

    solve_poisson_DFT(density,Npm0,Npm1,Npm2,L0,L1,L2,smooth);

    UNINDENT;
    sprintf(G__msg__, "Getting gravitational potential, periodic boundary conditions (using %d cores) done.", Nthreads);
    PrintMessage(4, G__msg__);
} //solve_poisson_periodic
///-------------------------------------------------------------------------------------
/** @fn get_2ndderivative_potential_rs_DFT
 * This routine computes the second derivative of the potential in real space,
 * given the potential in Fourier space
 * @param *POT input: potential in Fourier space
 * @param *OUT output: second derivative of the potential in real space
 * @param Npm0 mesh size x
 * @param Npm1 mesh size y
 * @param Npm2 mesh size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 * @param axis_a axis of the first derivative
 * @param axis_b axis of the second derivative
 */
void get_2ndderivative_potential_rs_DFT(fftw_complex_t *POT, float_t *OUT, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, const int axis_a, const int axis_b)
{
    int Nthreads = omp_get_max_threads();
    const int Npm2_HC=Npm2/2+1;
    const int Npm_HC=Npm0*Npm1*Npm2_HC;
    const double dpm0=L0/(double)Npm0;
    const double dpm1=L1/(double)Npm1;
    const double dpm2=L2/(double)Npm2;
    fftw_complex_t *AUX; AUX = (fftw_complex_t *)p_fftw_malloc(Npm_HC*sizeof(fftw_complex_t), VARNAME(AUX));

    // multiply by the discrete Green's function for second order derivative in Fourier space
    #pragma omp parallel for schedule(static) collapse(3)
    for(int mf0=0; mf0<Npm0; mf0++)
        for(int mf1=0; mf1<Npm1; mf1++)
            for(int mf2=0; mf2<Npm2_HC; mf2++)
            {
                fourierMode_t mf = get_FourierMode_Id(mf0,mf1,mf2,Npm0,Npm1,Npm2);
                double grada = get_kmode_grad_DFT(mf0,mf1,mf2,Npm0,Npm1,Npm2,L0,L1,L2,dpm0,dpm1,dpm2,axis_a);
                double gradb = get_kmode_grad_DFT(mf0,mf1,mf2,Npm0,Npm1,Npm2,L0,L1,L2,dpm0,dpm1,dpm2,axis_b);
                AUX[mf]=(fftw_complex_t)(POT[mf]*grada*gradb*(-1.)); // -1 coming from I^2
            }

    // inverse Fourier transform to configuration space
    FFT_c2r_3d(Npm0, Npm1, Npm2, AUX, OUT, Nthreads);

    // free memory
    p_fftw_free(AUX, VARNAME(AUX));
} //get_2ndderivative_potential_rs_DFT
///-------------------------------------------------------------------------------------
/** @fn dgf_laplace_RODFT00
 * Subroutine for discrete Green's function for the Laplace operator in sine space
*/
double dgf_laplace_RODFT00(const int mf0, const int mf1, const int mf2, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, const bool smooth, const double normphi)
{
    const double dpm0=L0/(double)Npm0;
    const double dpm1=L1/(double)Npm1;
    const double dpm2=L2/(double)Npm2;
    const double L=pow(L0*L1*L2,1./3.);
    const double asmthsq = 2.*M_PI/L * ASMTH * 2.*M_PI/L * ASMTH;

    double Greens;

    // calculate k-mode
    double k0=get_kmode_RODFT00(mf0,Npm0,L0);
    double k1=get_kmode_RODFT00(mf1,Npm1,L1);
    double k2=get_kmode_RODFT00(mf2,Npm2,L2);

    // invert Laplacian in sine space
    Greens = 1./laplace_RODFT00(k0,k1,k2,dpm0,dpm1,dpm2,Npm0,Npm1,Npm2);

    // smooth short-range forces, if needed
    if(smooth)
    {
        double ksquared = k0*k0+k1*k1+k2*k2;
        Greens *= exp(-ksquared * asmthsq);
    }

    // multiply by a physical prefactor, if needed
    Greens *= normphi;

    return Greens;
} //dgf_laplace_RODFT00
///-------------------------------------------------------------------------------------
/** @fn multiply_by_dgf_laplace_RODFT00
 * This routine multiplies by a Green's function in sine space
 * Assumes zero boundary conditions
 * @param *AUX input field in sine space
 * @param Npm0 mesh size x
 * @param Npm1 mesh size y
 * @param Npm2 mesh size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 * @param smooth smooth gravitational potential?
 * @param normphi normalization factor of the potential
 */
void multiply_by_dgf_laplace_RODFT00(float_t *AUX, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, const bool smooth, const double normphi)
{
    const int Npm2_HC=Npm2/2+1;
    #pragma omp parallel for schedule(static) collapse(3)
    for(int mf0=0; mf0<Npm0; mf0++)
        for(int mf1=0; mf1<Npm1; mf1++)
            for(int mf2=0; mf2<Npm2_HC; mf2++)
            {
                // multiply by discrete Green's function
                fourierMode_t mf = get_FourierMode_Id(mf0,mf1,mf2,Npm0,Npm1,Npm2);
                AUX[mf] *= dgf_laplace_RODFT00(mf0,mf1,mf2, Npm0,Npm1,Npm2, L0,L1,L2, smooth, normphi);
            }
} //multiply_by_dgf_laplace_RODFT00
///-------------------------------------------------------------------------------------
/** @fn solve_poisson_RODFT00
 * Solves the Poisson equation on the grid, replaces the input field delta
 * This routine assumes zero boundary conditions
 * @param *IN input/output
 * @param Npm0 particle mesh size x
 * @param Npm1 particle mesh size y
 * @param Npm2 particle mesh size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 * @param smooth smooth gravitational potential?
 */
void solve_poisson_RODFT00(float_t *IN, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, const bool smooth)
{
    int Nthreads = omp_get_max_threads();
    const cellIndex_t Npm = Npm0*Npm1*Npm2;

    // transform input field to sine space (triple forward DST-I transform)
    float_t *AUX; AUX = (float_t*)p_fftw_malloc(Npm*sizeof(float_t), VARNAME(AUX));
    FFT_forward_r2r_3d(Npm0, Npm1, Npm2, IN, AUX, FFTW_RODFT00, FFTW_RODFT00, FFTW_RODFT00, Nthreads);

    // mutiply by the Green function in sine space
    multiply_by_dgf_laplace_RODFT00(AUX, Npm0, Npm1, Npm2, L0, L1, L2, smooth, 1.);

    // transform back to real space (triple backward DST-I transform)
    FFT_backward_r2r_3d(Npm0, Npm1, Npm2, AUX, IN, FFTW_RODFT00, FFTW_RODFT00, FFTW_RODFT00, Nthreads);

    // free memory
    p_fftw_free(AUX, VARNAME(AUX));
} //solve_poisson_RODFT00
///-------------------------------------------------------------------------------------
/** @fn solve_poisson_zerobc
 * Solves the Poisson equation on the grid, replaces the input field delta
 * The quantity returned is Delta^-1 delta without any physical prefactor!
 * This routine assumes a Dirichlet problem with zero boundary conditions
 * @param *density input: the density contrast on the grid, output: phi on the grid
 * @param Npm0 particle mesh size x
 * @param Npm1 particle mesh size y
 * @param Npm2 particle mesh size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 * @param smooth smooth gravitational potential?
 */
void solve_poisson_zerobc(float_t *density, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, const bool smooth)
{
    int Nthreads = omp_get_max_threads();
    sprintf(G__msg__, "Getting gravitational potential, zero-potential boundary conditions (using %d cores)...", Nthreads);
    PrintMessage(4, G__msg__);
    INDENT;

    solve_poisson_RODFT00(density,Npm0,Npm1,Npm2,L0,L1,L2,smooth);

    UNINDENT;
    sprintf(G__msg__, "Getting gravitational potential, zero-potential boundary conditions (using %d cores) done.", Nthreads);
    PrintMessage(4, G__msg__);
} //solve_poisson_zerobc
///-------------------------------------------------------------------------------------
/** @fn get_2ndderivative_potential_rs_RODFT00
 * This routine computes the second derivative of the potential in real space,
 * given the potential in sine space
 * @param *POT input: potential in sine space
 * @param *OUT output: second derivative of the potential in real space
 * @param Npm0 mesh size x
 * @param Npm1 mesh size y
 * @param Npm2 mesh size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 * @param axis_a axis of the first derivative
 * @param axis_b axis of the second derivative
 */
void get_2ndderivative_potential_rs_RODFT00(float_t *POT, float_t *OUT, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, const int axis_a, const int axis_b)
{
    int Nthreads = omp_get_max_threads();
    const cellIndex_t Npm = Npm0*Npm1*Npm2;
    const int Npm2_HC=Npm2/2+1;
    const double d0=L0/(double)Npm0;
    const double d1=L1/(double)Npm1;
    const double d2=L2/(double)Npm2;
    float_t *AUX; AUX = (float_t*)p_fftw_malloc(Npm*sizeof(float_t), VARNAME(AUX));

    // multiply by the discrete Green's function for second order derivative in sine/cosine space
    #pragma omp parallel for schedule(static) collapse(3)
    for(int mf0=0; mf0<Npm0; mf0++)
        for(int mf1=0; mf1<Npm1; mf1++)
            for(int mf2=0; mf2<Npm2_HC; mf2++)
            {
                fourierMode_t mf = get_FourierMode_Id(mf0,mf1,mf2,Npm0,Npm1,Npm2);
                double grada = get_kmode_grad_RODFT00(mf0,mf1,mf2,Npm0,Npm1,Npm2,L0,L1,L2,d0,d1,d2,axis_a);
                double gradb = get_kmode_grad_RODFT00(mf0,mf1,mf2,Npm0,Npm1,Npm2,L0,L1,L2,d0,d1,d2,axis_b);
                AUX[mf]=POT[mf]*grada*gradb;
                if(axis_a == axis_b) AUX[mf]*=-1.; // two derivatives sin -> cos -> -sin along the same axis
            }

    // inverse sine/cosine transform to configuration space
    if(axis_a == axis_b)
        FFT_backward_r2r_3d(Npm0, Npm1, Npm2, AUX, OUT, FFTW_RODFT00, FFTW_RODFT00, FFTW_RODFT00, Nthreads);
    else // two derivatives sin -> cos along two different axes
    {
        fftw_r2r_kind kind0=FFTW_RODFT00, kind1=FFTW_RODFT00, kind2=FFTW_RODFT00;
        if(axis_a==0 || axis_b==0) kind0 = FFTW_REDFT00;
        if(axis_a==1 || axis_b==1) kind1 = FFTW_REDFT00;
        if(axis_a==2 || axis_b==2) kind2 = FFTW_REDFT00;
        FFT_backward_r2r_3d(Npm0, Npm1, Npm2, AUX, OUT, kind0, kind1, kind2, Nthreads);
    }

    // free memory
    p_fftw_free(AUX, VARNAME(AUX));
} //get_2ndderivative_potential_rs_RODFT00
///-------------------------------------------------------------------------------------
/** @fn modify_interior_dirichletbc
 * Modifies the interior of a source distribution, in order to solve the Poisson equation
 * with Dirichlet boundary conditions and that source distribution in the interior
 * @param *IN input/output
 * @param Npm0 particle mesh size x
 * @param Npm1 particle mesh size y
 * @param Npm2 particle mesh size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 * @param PhiB_W[Nghost][N1][N2] boundary conditions, west side (x=0)
 * @param PhiB_E[Nghost][N1][N2] boundary conditions, east side (x=xmax)
 * @param PhiB_S[Nghost][N0][N2] boundary conditions, south side (y=0)
 * @param PhiB_N[Nghost][N0][N2] boundary conditions, north side (y=ymax)
 * @param PhiB_B[Nghost][N0][N1] boundary conditions, bottom side (z=0)
 * @param PhiB_T[Nghost][N0][N1] boundary conditions, bottom side (z=zmax)
 */
void modify_interior_dirichletbc(float_t *IN, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, float_t *PhiB_W, float_t *PhiB_E, float_t *PhiB_S, float_t *PhiB_N, float_t *PhiB_B, float_t *PhiB_T)
{
    const double dpm0=L0/(double)Npm0;
    const double dpm1=L1/(double)Npm1;
    const double dpm2=L2/(double)Npm2;

    // modify the mass distribution given the potential at the boundaries (IN -> IN - Laplacian PhiB)
    #pragma omp parallel for schedule(static) collapse(2)
    for(int mg1=0; mg1<Npm1; mg1++)
        for(int mg2=0; mg2<Npm2; mg2++)
        {
            #if FDA==2
            IN[get_index(0,mg1,mg2,Npm0,Npm1,Npm2)] -= 1./(dpm0*dpm0)*PhiB_W[get_index(0,mg1,mg2,Nghost,Npm1,Npm2)];
            IN[get_index(Npm0-1,mg1,mg2,Npm0,Npm1,Npm2)] -= 1./(dpm0*dpm0)*PhiB_E[get_index(0,mg1,mg2,Nghost,Npm1,Npm2)];
            #elif FDA==4
            IN[get_index(0,mg1,mg2,Npm0,Npm1,Npm2)] -= 1./(dpm0*dpm0)*1./12.*16.*PhiB_W[get_index(0,mg1,mg2,Nghost,Npm1,Npm2)];
            IN[get_index(1,mg1,mg2,Npm0,Npm1,Npm2)] += 1./(dpm0*dpm0)*1./12.*PhiB_W[get_index(0,mg1,mg2,Nghost,Npm1,Npm2)];
            IN[get_index(0,mg1,mg2,Npm0,Npm1,Npm2)] += 1./(dpm0*dpm0)*1./12.*PhiB_W[get_index(1,mg1,mg2,Nghost,Npm1,Npm2)];
            IN[get_index(Npm0-1,mg1,mg2,Npm0,Npm1,Npm2)] -= 1./(dpm0*dpm0)*1./12.*16.*PhiB_E[get_index(0,mg1,mg2,Nghost,Npm1,Npm2)];
            IN[get_index(Npm0-2,mg1,mg2,Npm0,Npm1,Npm2)] += 1./(dpm0*dpm0)*1./12.*PhiB_E[get_index(0,mg1,mg2,Nghost,Npm1,Npm2)];
            IN[get_index(Npm0-1,mg1,mg2,Npm0,Npm1,Npm2)] += 1./(dpm0*dpm0)*1./12.*PhiB_E[get_index(1,mg1,mg2,Nghost,Npm1,Npm2)];
            #elif FDA>=6 || FDA==0
            IN[get_index(0,mg1,mg2,Npm0,Npm1,Npm2)] -= 1./(dpm0*dpm0)*1./180.*270.*PhiB_W[get_index(0,mg1,mg2,Nghost,Npm1,Npm2)];
            IN[get_index(1,mg1,mg2,Npm0,Npm1,Npm2)] += 1./(dpm0*dpm0)*1./180.*27.*PhiB_W[get_index(0,mg1,mg2,Nghost,Npm1,Npm2)];
            IN[get_index(2,mg1,mg2,Npm0,Npm1,Npm2)] -= 1./(dpm0*dpm0)*1./180.*2.*PhiB_W[get_index(0,mg1,mg2,Nghost,Npm1,Npm2)];
            IN[get_index(0,mg1,mg2,Npm0,Npm1,Npm2)] += 1./(dpm0*dpm0)*1./180.*27.*PhiB_W[get_index(1,mg1,mg2,Nghost,Npm1,Npm2)];
            IN[get_index(1,mg1,mg2,Npm0,Npm1,Npm2)] -= 1./(dpm0*dpm0)*1./180.*2.*PhiB_W[get_index(1,mg1,mg2,Nghost,Npm1,Npm2)];
            IN[get_index(0,mg1,mg2,Npm0,Npm1,Npm2)] -= 1./(dpm0*dpm0)*1./180.*2.*PhiB_W[get_index(2,mg1,mg2,Nghost,Npm1,Npm2)];
            IN[get_index(Npm0-1,mg1,mg2,Npm0,Npm1,Npm2)] -= 1./(dpm0*dpm0)*1./180.*270.*PhiB_E[get_index(0,mg1,mg2,Nghost,Npm1,Npm2)];
            IN[get_index(Npm0-2,mg1,mg2,Npm0,Npm1,Npm2)] += 1./(dpm0*dpm0)*1./180.*27.*PhiB_E[get_index(0,mg1,mg2,Nghost,Npm1,Npm2)];
            IN[get_index(Npm0-3,mg1,mg2,Npm0,Npm1,Npm2)] -= 1./(dpm0*dpm0)*1./180.*2.*PhiB_E[get_index(0,mg1,mg2,Nghost,Npm1,Npm2)];
            IN[get_index(Npm0-1,mg1,mg2,Npm0,Npm1,Npm2)] += 1./(dpm0*dpm0)*1./180.*27.*PhiB_E[get_index(1,mg1,mg2,Nghost,Npm1,Npm2)];
            IN[get_index(Npm0-2,mg1,mg2,Npm0,Npm1,Npm2)] -= 1./(dpm0*dpm0)*1./180.*2.*PhiB_E[get_index(1,mg1,mg2,Nghost,Npm1,Npm2)];
            IN[get_index(Npm0-1,mg1,mg2,Npm0,Npm1,Npm2)] -= 1./(dpm0*dpm0)*1./180.*2.*PhiB_E[get_index(2,mg1,mg2,Nghost,Npm1,Npm2)];
            #endif
        }
    #pragma omp parallel for schedule(static) collapse(2)
    for(int mg0=0; mg0<Npm0; mg0++)
        for(int mg2=0; mg2<Npm2; mg2++)
        {
            #if FDA==2
            IN[get_index(mg0,0,mg2,Npm0,Npm1,Npm2)] -= 1./(dpm1*dpm1)*PhiB_S[get_index(0,mg0,mg2,Nghost,Npm0,Npm2)];
            IN[get_index(mg0,Npm1-1,mg2,Npm0,Npm1,Npm2)] -= 1./(dpm1*dpm1)*PhiB_N[get_index(0,mg0,mg2,Nghost,Npm0,Npm2)];
            #elif FDA==4
            IN[get_index(mg0,0,mg2,Npm0,Npm1,Npm2)] -= 1./(dpm1*dpm1)*1./12.*16.*PhiB_S[get_index(0,mg0,mg2,Nghost,Npm0,Npm2)];
            IN[get_index(mg0,1,mg2,Npm0,Npm1,Npm2)] += 1./(dpm1*dpm1)*1./12.*PhiB_S[get_index(0,mg0,mg2,Nghost,Npm0,Npm2)];
            IN[get_index(mg0,0,mg2,Npm0,Npm1,Npm2)] += 1./(dpm1*dpm1)*1./12.*PhiB_S[get_index(1,mg0,mg2,Nghost,Npm0,Npm2)];
            IN[get_index(mg0,Npm1-1,mg2,Npm0,Npm1,Npm2)] -= 1./(dpm1*dpm1)*1./12.*16.*PhiB_N[get_index(0,mg0,mg2,Nghost,Npm0,Npm2)];
            IN[get_index(mg0,Npm1-2,mg2,Npm0,Npm1,Npm2)] += 1./(dpm1*dpm1)*1./12.*PhiB_N[get_index(0,mg0,mg2,Nghost,Npm0,Npm2)];
            IN[get_index(mg0,Npm1-1,mg2,Npm0,Npm1,Npm2)] += 1./(dpm1*dpm1)*1./12.*PhiB_N[get_index(1,mg0,mg2,Nghost,Npm0,Npm2)];
            #elif FDA>=6 || FDA==0
            IN[get_index(mg0,0,mg2,Npm0,Npm1,Npm2)] -= 1./(dpm1*dpm1)*1./180.*270.*PhiB_S[get_index(0,mg0,mg2,Nghost,Npm0,Npm2)];
            IN[get_index(mg0,1,mg2,Npm0,Npm1,Npm2)] += 1./(dpm1*dpm1)*1./180.*27.*PhiB_S[get_index(0,mg0,mg2,Nghost,Npm0,Npm2)];
            IN[get_index(mg0,2,mg2,Npm0,Npm1,Npm2)] -= 1./(dpm1*dpm1)*1./180.*2.*PhiB_S[get_index(0,mg0,mg2,Nghost,Npm0,Npm2)];
            IN[get_index(mg0,0,mg2,Npm0,Npm1,Npm2)] += 1./(dpm1*dpm1)*1./180.*27.*PhiB_S[get_index(1,mg0,mg2,Nghost,Npm0,Npm2)];
            IN[get_index(mg0,1,mg2,Npm0,Npm1,Npm2)] -= 1./(dpm1*dpm1)*1./180.*2.*PhiB_S[get_index(1,mg0,mg2,Nghost,Npm0,Npm2)];
            IN[get_index(mg0,0,mg2,Npm0,Npm1,Npm2)] -= 1./(dpm1*dpm1)*1./180.*2.*PhiB_S[get_index(2,mg0,mg2,Nghost,Npm0,Npm2)];
            IN[get_index(mg0,Npm1-1,mg2,Npm0,Npm1,Npm2)] -= 1./(dpm1*dpm1)*1./180.*270.*PhiB_N[get_index(0,mg0,mg2,Nghost,Npm0,Npm2)];
            IN[get_index(mg0,Npm1-2,mg2,Npm0,Npm1,Npm2)] += 1./(dpm1*dpm1)*1./180.*27.*PhiB_N[get_index(0,mg0,mg2,Nghost,Npm0,Npm2)];
            IN[get_index(mg0,Npm1-3,mg2,Npm0,Npm1,Npm2)] -= 1./(dpm1*dpm1)*1./180.*2.*PhiB_N[get_index(0,mg0,mg2,Nghost,Npm0,Npm2)];
            IN[get_index(mg0,Npm1-1,mg2,Npm0,Npm1,Npm2)] += 1./(dpm1*dpm1)*1./180.*27.*PhiB_N[get_index(1,mg0,mg2,Nghost,Npm0,Npm2)];
            IN[get_index(mg0,Npm1-2,mg2,Npm0,Npm1,Npm2)] -= 1./(dpm1*dpm1)*1./180.*2.*PhiB_N[get_index(1,mg0,mg2,Nghost,Npm0,Npm2)];
            IN[get_index(mg0,Npm1-1,mg2,Npm0,Npm1,Npm2)] -= 1./(dpm1*dpm1)*1./180.*2.*PhiB_N[get_index(2,mg0,mg2,Nghost,Npm0,Npm2)];
            #endif
        }
    #pragma omp parallel for schedule(static) collapse(2)
    for(int mg0=0; mg0<Npm0; mg0++)
        for(int mg1=0; mg1<Npm1; mg1++)
        {
            #if FDA==2
            IN[get_index(mg0,mg1,0,Npm0,Npm1,Npm2)] -= 1./(dpm2*dpm2)*PhiB_B[get_index(0,mg0,mg1,Nghost,Npm0,Npm1)];
            IN[get_index(mg0,mg1,Npm2-1,Npm0,Npm1,Npm2)] -= 1./(dpm2*dpm2)*PhiB_T[get_index(0,mg0,mg1,Nghost,Npm0,Npm1)];
            #elif FDA==4
            IN[get_index(mg0,mg1,0,Npm0,Npm1,Npm2)] -= 1./(dpm2*dpm2)*1./12.*16.*PhiB_B[get_index(0,mg0,mg1,Nghost,Npm0,Npm1)];
            IN[get_index(mg0,mg1,1,Npm0,Npm1,Npm2)] += 1./(dpm2*dpm2)*1./12.*PhiB_B[get_index(0,mg0,mg1,Nghost,Npm0,Npm1)];
            IN[get_index(mg0,mg1,0,Npm0,Npm1,Npm2)] += 1./(dpm2*dpm2)*1./12.*PhiB_B[get_index(1,mg0,mg1,Nghost,Npm0,Npm1)];
            IN[get_index(mg0,mg1,Npm2-1,Npm0,Npm1,Npm2)] -= 1./(dpm2*dpm2)*1./12.*16.*PhiB_T[get_index(0,mg0,mg1,Nghost,Npm0,Npm1)];
            IN[get_index(mg0,mg1,Npm2-2,Npm0,Npm1,Npm2)] += 1./(dpm2*dpm2)*1./12.*PhiB_T[get_index(0,mg0,mg1,Nghost,Npm0,Npm1)];
            IN[get_index(mg0,mg1,Npm2-1,Npm0,Npm1,Npm2)] += 1./(dpm2*dpm2)*1./12.*PhiB_T[get_index(1,mg0,mg1,Nghost,Npm0,Npm1)];
            #elif FDA>=6 || FDA==0
            IN[get_index(mg0,mg1,0,Npm0,Npm1,Npm2)] -= 1./(dpm2*dpm2)*1./180.*270.*PhiB_B[get_index(0,mg0,mg1,Nghost,Npm0,Npm1)];
            IN[get_index(mg0,mg1,1,Npm0,Npm1,Npm2)] += 1./(dpm2*dpm2)*1./180.*27.*PhiB_B[get_index(0,mg0,mg1,Nghost,Npm0,Npm1)];
            IN[get_index(mg0,mg1,2,Npm0,Npm1,Npm2)] -= 1./(dpm2*dpm2)*1./180.*2.*PhiB_B[get_index(0,mg0,mg1,Nghost,Npm0,Npm1)];
            IN[get_index(mg0,mg1,0,Npm0,Npm1,Npm2)] += 1./(dpm2*dpm2)*1./180.*27.*PhiB_B[get_index(1,mg0,mg1,Nghost,Npm0,Npm1)];
            IN[get_index(mg0,mg1,1,Npm0,Npm1,Npm2)] -= 1./(dpm2*dpm2)*1./180.*2.*PhiB_B[get_index(1,mg0,mg1,Nghost,Npm0,Npm1)];
            IN[get_index(mg0,mg1,0,Npm0,Npm1,Npm2)] -= 1./(dpm2*dpm2)*1./180.*2.*PhiB_B[get_index(2,mg0,mg1,Nghost,Npm0,Npm1)];
            IN[get_index(mg0,mg1,Npm2-1,Npm0,Npm1,Npm2)] -= 1./(dpm2*dpm2)*1./180.*270.*PhiB_T[get_index(0,mg0,mg1,Nghost,Npm0,Npm1)];
            IN[get_index(mg0,mg1,Npm2-2,Npm0,Npm1,Npm2)] += 1./(dpm2*dpm2)*1./180.*27.*PhiB_T[get_index(0,mg0,mg1,Nghost,Npm0,Npm1)];
            IN[get_index(mg0,mg1,Npm2-3,Npm0,Npm1,Npm2)] -= 1./(dpm2*dpm2)*1./180.*2.*PhiB_T[get_index(0,mg0,mg1,Nghost,Npm0,Npm1)];
            IN[get_index(mg0,mg1,Npm2-1,Npm0,Npm1,Npm2)] += 1./(dpm2*dpm2)*1./180.*27.*PhiB_T[get_index(1,mg0,mg1,Nghost,Npm0,Npm1)];
            IN[get_index(mg0,mg1,Npm2-2,Npm0,Npm1,Npm2)] -= 1./(dpm2*dpm2)*1./180.*2.*PhiB_T[get_index(1,mg0,mg1,Nghost,Npm0,Npm1)];
            IN[get_index(mg0,mg1,Npm2-1,Npm0,Npm1,Npm2)] -= 1./(dpm2*dpm2)*1./180.*2.*PhiB_T[get_index(2,mg0,mg1,Nghost,Npm0,Npm1)];
            #endif
        }
} //modify_interior_dirichletbc
///-------------------------------------------------------------------------------------
/** @fn solve_poisson_dirichletbc
 * Solves the Poisson equation on the grid, replaces the input field delta
 * This routine assumes a Dirichlet problem with specified boundary conditions
 * @param *IN input/output
 * @param Npm0 particle mesh size x
 * @param Npm1 particle mesh size y
 * @param Npm2 particle mesh size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 * @param smooth smooth gravitational potential?
 * @param PhiB_W[Nghost][N1][N2] boundary conditions, west side (x=0)
 * @param PhiB_E[Nghost][N1][N2] boundary conditions, east side (x=xmax)
 * @param PhiB_S[Nghost][N0][N2] boundary conditions, south side (y=0)
 * @param PhiB_N[Nghost][N0][N2] boundary conditions, north side (y=ymax)
 * @param PhiB_B[Nghost][N0][N1] boundary conditions, bottom side (z=0)
 * @param PhiB_T[Nghost][N0][N1] boundary conditions, bottom side (z=zmax)
 */
void solve_poisson_dirichletbc(float_t *IN, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, const bool smooth, float_t *PhiB_W, float_t *PhiB_E, float_t *PhiB_S, float_t *PhiB_N, float_t *PhiB_B, float_t *PhiB_T)
{
    int Nthreads = omp_get_max_threads();
    sprintf(G__msg__, "Getting gravitational potential, Dirichlet boundary conditions (using %d cores)...", Nthreads);
    PrintMessage(4, G__msg__);
    INDENT;

    // modify the mass distribution given the potential at the boundaries (IN -> IN - Laplacian PhiB)
    modify_interior_dirichletbc(IN,Npm0,Npm1,Npm2,L0,L1,L2,PhiB_W,PhiB_E,PhiB_S,PhiB_N,PhiB_B,PhiB_T);

    // solve the Poisson equation with zero boundaries using the modified mass distribution
    solve_poisson_RODFT00(IN,Npm0,Npm1,Npm2,L0,L1,L2,smooth);

    UNINDENT;
    sprintf(G__msg__, "Getting gravitational potential, Dirichlet boundary conditions (using %d cores) done.", Nthreads);
    PrintMessage(4, G__msg__);
} //solve_poisson_dirichletbc
///-------------------------------------------------------------------------------------
