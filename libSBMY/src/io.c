///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/src/io.c
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file io.c
 *  \brief Routines for reading and writing files
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2013-2023
 */
#include "io.h"
///-------------------------------------------------------------------------------------
char Tab_IO_Labels[IO_NBLOCKS][4];
const int blkff2size = sizeof(int) + 4 * sizeof(char);
///-------------------------------------------------------------------------------------
static void add_group(hid_t file_id, const char *group_name)
{
    hid_t group_id;
    group_id = H5Gcreate2(file_id, group_name, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5Gclose(group_id);
} //add_group
///-------------------------------------------------------------------------------------
static void read_attribute_int(hid_t file_id, char *address, int *value)
{
    hid_t attribute_id;
    attribute_id = H5Aopen_name(file_id, address);
    H5Aread(attribute_id, H5T_NATIVE_INT, value);
    H5Aclose(attribute_id);
} //read_attribute_int
///-------------------------------------------------------------------------------------
static void write_attribute_int(hid_t file_id, char *address, int *value)
{
    hid_t dataspace_id, attribute_id;
    dataspace_id = H5Screate(H5S_SCALAR);
    attribute_id = H5Acreate2(file_id, address, H5T_NATIVE_INT, dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(attribute_id, H5T_NATIVE_INT, value);
    H5Aclose(attribute_id);
    H5Sclose(dataspace_id);
} //write_attribute_int
///-------------------------------------------------------------------------------------
static void read_attribute_double(hid_t file_id, char *address, double *value)
{
    hid_t attribute_id;
    attribute_id = H5Aopen_name(file_id, address);
    H5Aread(attribute_id, H5T_NATIVE_DOUBLE, value);
    H5Aclose(attribute_id);
} //read_attribute_double
///-------------------------------------------------------------------------------------
static void write_attribute_double(hid_t file_id, char *address, double *value)
{
    hid_t dataspace_id, attribute_id;
    dataspace_id = H5Screate(H5S_SCALAR);
    attribute_id = H5Acreate2(file_id, address, H5T_NATIVE_DOUBLE, dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(attribute_id, H5T_NATIVE_DOUBLE, value);
    H5Aclose(attribute_id);
    H5Sclose(dataspace_id);
} //write_attribute_double
///-------------------------------------------------------------------------------------
static void read_attribute_particleID_t(hid_t file_id, char *address, particleID_t *value)
{
    hid_t attribute_id;
    attribute_id = H5Aopen_name(file_id, address);
    H5Aread(attribute_id, H5T_PARTICLEID_T, value);
    H5Aclose(attribute_id);
} //read_attribute_particleID_t
///-------------------------------------------------------------------------------------
static void write_attribute_particleID_t(hid_t file_id, char *address, particleID_t *value)
{
    hid_t dataspace_id, attribute_id;
    dataspace_id = H5Screate(H5S_SCALAR);
    attribute_id = H5Acreate2(file_id, address, H5T_PARTICLEID_T, dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(attribute_id, H5T_PARTICLEID_T, value);
    H5Aclose(attribute_id);
    H5Sclose(dataspace_id);
} //write_attribute_particleID_t
///-------------------------------------------------------------------------------------
static void write_attribute_double_array(hid_t file_id, char *address, cellIndex_t length, double data[length])
{
    hid_t dataspace_id, attribute_id;
    hsize_t dims = (hsize_t)length;
    dataspace_id = H5Screate_simple(1, &dims, NULL);
    attribute_id = H5Acreate2(file_id, address, H5T_NATIVE_DOUBLE, dataspace_id, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(attribute_id, H5T_NATIVE_DOUBLE, data);
    H5Aclose(attribute_id);
    H5Sclose(dataspace_id);
} //write_attribute_double_array
///-------------------------------------------------------------------------------------
static void read_attribute_double_in_array(hid_t file_id, char *address, double *value, int index)
{
    hid_t attribute_id, dataspace_file_id;
    int num_elements=1;
    hsize_t coords_a[1]={(hsize_t)index};
    attribute_id = H5Aopen_name(file_id, address);
    dataspace_file_id = H5Aget_space(attribute_id);
    H5Sselect_elements(dataspace_file_id, H5S_SELECT_SET, num_elements, coords_a);
    H5Aread(attribute_id, H5T_NATIVE_DOUBLE, value);
    H5Sclose(dataspace_file_id);
    H5Aclose(attribute_id);
} //read_attribute_double_in_array
///-------------------------------------------------------------------------------------
static void read_attribute_double_array_in_array(hid_t file_id, char *address, cellIndex_t length, double data[length], int index)
{
    hid_t attribute_id, dataspace_file_id;
    hsize_t dims_b[2]={1, (hsize_t)length};
    hsize_t offset_b[2]={(hsize_t)index, 0};
    attribute_id = H5Aopen_name(file_id, address);
    dataspace_file_id = H5Aget_space(attribute_id);
    H5Sselect_hyperslab(dataspace_file_id, H5S_SELECT_SET, offset_b, NULL, dims_b, NULL);
    H5Aread(attribute_id, H5T_NATIVE_DOUBLE, data);
    H5Sclose(dataspace_file_id);
    H5Aclose(attribute_id);
} //read_attribute_double_array_in_array
///-------------------------------------------------------------------------------------
static void write_int_array(hid_t file_id, char *address, int length, int data[length])
{
    hid_t dataspace_id, dataset_id;
    hsize_t dim = length;

    dataspace_id = H5Screate_simple(1, &dim, NULL);
    dataset_id = H5Dcreate2(file_id, address, H5T_NATIVE_INT, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, data);
    H5Dclose(dataset_id);
    H5Sclose(dataspace_id);
} //write_int_array
///-------------------------------------------------------------------------------------
static void write_int_3d_array(hid_t file_id, char *address, int N0, int N1, int N2, int data[N0][N1][N2])
{
    hid_t dataspace_id, dataset_id;
    hsize_t dims[3] = {N0,N1,N2};
    dataspace_id = H5Screate_simple(3, dims, NULL);
    dataset_id = H5Dcreate2(file_id, address, H5T_NATIVE_INT, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, data);
    H5Dclose(dataset_id);
    H5Sclose(dataspace_id);
} //write_int_3d_array
///-------------------------------------------------------------------------------------
static void read_int_array(hid_t file_id, char *address, int *value)
{
    hid_t dataset_id;
    dataset_id = H5Dopen2(file_id, address, H5P_DEFAULT);
    H5Dread(dataset_id, H5T_NATIVE_INT, H5S_ALL, H5S_ALL, H5P_DEFAULT, value);
    H5Dclose(dataset_id);
} //read_int_array
///-------------------------------------------------------------------------------------
static void write_double_2d_array(hid_t file_id, const char *address, int N0, int N1, double data[N0][N1])
{
    hid_t dataspace_id, dataset_id;
    hsize_t dims[2] = {N0,N1};
    dataspace_id = H5Screate_simple(2, dims, NULL);
    dataset_id = H5Dcreate2(file_id, address, H5T_NATIVE_DOUBLE, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(dataset_id, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, data);
    H5Dclose(dataset_id);
    H5Sclose(dataspace_id);
} //write_double_2d_array
///-------------------------------------------------------------------------------------
static void write_double_3d_array(hid_t file_id, const char *address, int N0, int N1, int N2, double data[N0][N1][N2])
{
    hid_t dataspace_id, dataset_id;
    hsize_t dims[3] = {N0,N1,N2};
    dataspace_id = H5Screate_simple(3, dims, NULL);
    dataset_id = H5Dcreate2(file_id, address, H5T_NATIVE_DOUBLE, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5Dwrite(dataset_id, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, data);
    H5Dclose(dataset_id);
    H5Sclose(dataspace_id);
} //write_double_3d_array
///-------------------------------------------------------------------------------------
static void write_float_t_array_to_dataspace(hid_t file_id, hid_t dataspace_id, const char *address, const cellIndex_t length, float_t array[length])
{
    hid_t dataset_id;
    #ifdef DOUBLEPRECISION
    float *FloatBuffer;
    FloatBuffer = (float *)p_malloc(length*sizeof(float), VARNAME(FloatBuffer));
    #pragma omp parallel for schedule(static)
    for(cellIndex_t n=0; n<length; n++)
        FloatBuffer[n] = (float)array[n];
    #endif

    dataset_id = H5Dcreate2(file_id, address, H5T_NATIVE_FLOAT, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    #ifdef DOUBLEPRECISION
    H5Dwrite(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, FloatBuffer);
    p_free(FloatBuffer, VARNAME(FloatBuffer));
    #else
    UNUSED(length);
    H5Dwrite(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, array);
    #endif
    H5Dclose(dataset_id);
} //write_float_t_array_to_dataspace
///-------------------------------------------------------------------------------------
static void read_float_t_array(hid_t file_id, const char *address, const cellIndex_t length, float_t array[length])
{
    hid_t dataset_id;
    dataset_id = H5Dopen2(file_id, address, H5P_DEFAULT);
    #ifdef DOUBLEPRECISION
    float *FloatBuffer;
    FloatBuffer = (float *)p_malloc(length*sizeof(float), VARNAME(FloatBuffer));
    H5Dread(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, FloatBuffer);

    #pragma omp parallel for schedule(static)
    for(cellIndex_t n=0; n<length; n++)
        array[n] = (float_t)FloatBuffer[n];
    p_free(FloatBuffer, VARNAME(FloatBuffer));
    #else
    UNUSED(length);
    H5Dread(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, array);
    #endif
    H5Dclose(dataset_id);
} //read_float_t_array
///-------------------------------------------------------------------------------------
static void write_float_t_array(hid_t file_id, const char *address, cellIndex_t length, float_t array[length])
{
    hid_t dataspace_id;
    hsize_t dim = length;
    dataspace_id = H5Screate_simple(1, &dim, NULL);
    write_float_t_array_to_dataspace(file_id, dataspace_id, address, length, array);
    H5Sclose(dataspace_id);
} //write_float_t_array
///-------------------------------------------------------------------------------------
static void write_float_t_3d_array(hid_t file_id, const char *address, int N0, int N1, int N2, float_t ***array)
{
    hid_t dataspace_id;
    hsize_t dims[3] = {N0,N1,N2};

    dataspace_id = H5Screate_simple(3, dims, NULL);
    hid_t dataset_id;

    #ifdef DOUBLEPRECISION
    float ***FloatBuffer;
    FloatBuffer = (float ***)p_malloc(N0*N1*N2*sizeof(float), VARNAME(FloatBuffer));
    #pragma omp parallel for schedule(static)
    for(int mc0=0; mc0<N0; mc0++)
        for(int mc1=0; mc1<N1; mc1++)
            for(int mc2=0; mc2<N2; mc2++)
                FloatBuffer[mc0][mc1][mc2] = (float)array[mc0][mc1][mc2];
    #endif

    dataset_id = H5Dcreate2(file_id, address, H5T_NATIVE_FLOAT, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    #ifdef DOUBLEPRECISION
    H5Dwrite(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, FloatBuffer);
    p_free(FloatBuffer, VARNAME(FloatBuffer));
    #else
    H5Dwrite(dataset_id, H5T_NATIVE_FLOAT, H5S_ALL, H5S_ALL, H5P_DEFAULT, array);
    #endif
    H5Dclose(dataset_id);

    H5Sclose(dataspace_id);
} //write_float_t_3d_array
///-------------------------------------------------------------------------------------
static void write_field_data(hid_t file_id, char *field_address, float_t *field_data, const int rank, const int N0, const int N1, const int N2)
{
    hid_t dataspace_id;
    if(rank>1)
    {
        hsize_t field_dim[4] = {rank,N0,N1,N2};
        dataspace_id = H5Screate_simple(4, field_dim, NULL);
    }
    else
    {
        hsize_t field_dim[3] = {N0,N1,N2};
        dataspace_id = H5Screate_simple(3, field_dim, NULL);
    }
    const cellIndex_t N=rank*N0*N1*N2;
    write_float_t_array_to_dataspace(file_id, dataspace_id, field_address, N, field_data);
} //write_field_data
///-------------------------------------------------------------------------------------
static void read_field_data(hid_t file_id, char *field_address, float_t *field_data, const int rank, const int N0, const int N1, const int N2)
{
    const cellIndex_t N=rank*N0*N1*N2;
    read_float_t_array(file_id, field_address, N, field_data);
} //read_field_data
///-------------------------------------------------------------------------------------
/** @fn fill_meta_basefield
 * This subroutine fills the metadata of a field
 */
static void fill_meta_basefield(BaseField *F, const int rank, const int N0, const int N1, const int N2, const double corner0, const double corner1, const double corner2, const double L0, const double L1, const double L2)
{
    F->L0=L0;
    F->L1=L1;
    F->L2=L2;
    F->corner0=corner0;
    F->corner1=corner1;
    F->corner2=corner2;
    F->rank=rank;
    F->N0=N0;
    F->N1=N1;
    F->N2=N2;
} //fill_meta_basefield
///-------------------------------------------------------------------------------------
/** @fn fill_meta_field
 * This subroutine fills the metadata of a field
 */
static void fill_meta_field(Field *F, const int rank, const int N0, const int N1, const int N2, const double corner0, const double corner1, const double corner2, const double L0, const double L1, const double L2, const double Time)
{
    F->L0=L0;
    F->L1=L1;
    F->L2=L2;
    F->corner0=corner0;
    F->corner1=corner1;
    F->corner2=corner2;
    F->rank=rank;
    F->N0=N0;
    F->N1=N1;
    F->N2=N2;
    F->time = Time;
} //fill_meta_field
///-------------------------------------------------------------------------------------
/** @fn allocate_basefield_
 * This routine allocates the memory and returns a new basefield,
 * given its metadata
 * @param rank rank
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param corner0 corner x-position
 * @param corner1 corner y-position
 * @param corner2 corner z-position
 * @param L0 box size in x
 * @param L1 box size in y
 * @param L2 box size in z
 * @param *name output variable name
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 * @return basefield
 */
BaseField allocate_basefield_(const int rank, const int N0, const int N1, const int N2, const double corner0, const double corner1, const double corner2, const double L0, const double L1, const double L2, char *name, char *file, char *func, int line)
{
    BaseField BF;
    char vname[50];
    sprintf(vname, "%s.data", name);
    BF.data = (float_t*)p_fftw_malloc_(rank*N0*N1*N2*sizeof(float_t), vname, file, func, line);
    fill_meta_basefield(&BF, rank, N0, N1, N2, corner0, corner1, corner2, L0, L1, L2);
    return BF;
} //allocate_basefield_
///-------------------------------------------------------------------------------------
/** @fn allocate_scalar_basefield_
 * Same as previous routine for a scalar basefield (rank=1)
 */
BaseField allocate_scalar_basefield_(const int N0, const int N1, const int N2, const double corner0, const double corner1, const double corner2, const double L0, const double L1, const double L2, char *name, char *file, char *func, int line)
{
    return allocate_basefield_(1, N0, N1, N2, corner0, corner1, corner2, L0, L1, L2, name, file, func, line);
} //allocate_scalar_basefield_
///-------------------------------------------------------------------------------------
/** @fn allocate_field_
 * This routine allocates the memory and returns a new field,
 * given its metadata
 * @param rank rank
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param corner0 corner x-position
 * @param corner1 corner y-position
 * @param corner2 corner z-position
 * @param L0 box size in x
 * @param L1 box size in y
 * @param L2 box size in z
 * @param Time time
 * @param *name output variable name
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 * @return field
 */
Field allocate_field_(const int rank, const int N0, const int N1, const int N2, const double corner0, const double corner1, const double corner2, const double L0, const double L1, const double L2, const double Time, char *name, char *file, char *func, int line)
{
    Field F;
    char vname[50];
    sprintf(vname, "%s.data", name);
    F.data = (float_t*)p_fftw_malloc_(rank*N0*N1*N2*sizeof(float_t), vname, file, func, line);
    fill_meta_field(&F, rank, N0, N1, N2, corner0, corner1, corner2, L0, L1, L2, Time);
    return F;
} //allocate_field_
///-------------------------------------------------------------------------------------
/** @fn allocate_scalar_field_
 * Same as previous routine for a scalar field (rank=1)
 */
Field allocate_scalar_field_(const int N0, const int N1, const int N2, const double corner0, const double corner1, const double corner2, const double L0, const double L1, const double L2, const double Time, char *name, char *file, char *func, int line)
{
    return allocate_field_(1, N0, N1, N2, corner0, corner1, corner2, L0, L1, L2, Time, name, file, func, line);
} //allocate_scalar_field_
///-------------------------------------------------------------------------------------
/** @fn field_from_data
 * This routine returns a field, given the data and its metadata
 * @param *data the data
 * @param rank rank
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param corner0 corner x-position
 * @param corner1 corner y-position
 * @param corner2 corner z-position
 * @param L0 box size in x
 * @param L1 box size in y
 * @param L2 box size in z
 * @param Time time
 * @return field
 */
Field field_from_data(const float_t *data, const int rank, const int N0, const int N1, const int N2, const double corner0, const double corner1, const double corner2, const double L0, const double L1, const double L2, const double Time)
{
    Field F;
    F.data=(float_t *)data;
    fill_meta_field(&F, rank, N0, N1, N2, corner0, corner1, corner2, L0, L1, L2, Time);
    return F;
} //field_from_data
///-------------------------------------------------------------------------------------
/** @fn scalar_field_from_data
 * Same as previous routine for a scalar field (rank=1)
 * @param *data the data
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param corner0 corner x-position
 * @param corner1 corner y-position
 * @param corner2 corner z-position
 * @param L0 box size in x
 * @param L1 box size in y
 * @param L2 box size in z
 * @param Time time
 * @return field
 */
Field scalar_field_from_data(const float_t *data, const int N0, const int N1, const int N2, const double corner0, const double corner1, const double corner2, const double L0, const double L1, const double L2, const double Time)
{
    return field_from_data(data, 1, N0, N1, N2, corner0, corner1, corner2, L0, L1, L2, Time);
} //scalar_field_from_data
///-------------------------------------------------------------------------------------
/** @fn free_basefield_
 * This routine frees the memory allocated for a field
 * @param F the field in standard structure
 */
void free_basefield_(BaseField BF, char *name, char *file, char *func, int line)
{
    char vname[50];
    sprintf(vname, "%s.data", name);
    p_fftw_free_(BF.data, vname, file, func, line);
} //free_basefield_
///-------------------------------------------------------------------------------------
/** @fn free_field_
 * This routine frees the memory allocated for a field
 * @param F the field in standard structure
 */
void free_field_(Field F, char *name, char *file, char *func, int line)
{
    char vname[50];
    sprintf(vname, "%s.data", name);
    p_fftw_free_(F.data, vname, file, func, line);
} //free_field_
///-------------------------------------------------------------------------------------
/** @fn write_basefield
 * Wrapper for basefield writing routine
 * @param F the field in standard structure
 * @param fname output filename
 */
void write_basefield(BaseField BF, const char *fname)
{
    sprintf(G__msg__, "Writing field to '%s'...", fname);
    PrintMessage(3, G__msg__);

    write_basefield_hdf5(BF, fname);

    sprintf(G__msg__, "Writing field to '%s' done.", fname);
    PrintMessage(3, G__msg__);
} //write_basefield
///-------------------------------------------------------------------------------------
/** @fn read_basefield_
 * Wrapper for field reading routine
 * @param fname input filename
 * @param *name output variable name
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
BaseField read_basefield_(const char *fname, char *name, char *file, char *func, int line)
{
    sprintf(G__msg__, "Reading field in '%s'...", fname);
    PrintMessage(3, G__msg__);

    BaseField BF;
    BF = read_basefield_hdf5_(fname, name, file, func, line);

    sprintf(G__msg__, "Reading field in '%s' done.", fname);
    PrintMessage(3, G__msg__);

    return BF;
} //read_basefield_
///-------------------------------------------------------------------------------------
/** @fn read_basefield_header
 * Wrapper for field metadata reading routine
 * @param fname input filename
 */
BaseField read_basefield_header(const char *fname)
{
    return read_basefield_hdf5_header(fname);
} //read_basefield_header
///-------------------------------------------------------------------------------------
/** @fn write_field
 * Wrapper for field writing routine
 * @param F the field in standard structure
 * @param fname output filename
 */
void write_field(Field F, const char *fname)
{
    sprintf(G__msg__, "Writing field to '%s'...", fname);
    PrintMessage(3, G__msg__);

    write_field_hdf5(F, fname);

    sprintf(G__msg__, "Writing field to '%s' done.", fname);
    PrintMessage(3, G__msg__);
} //write_field
///-------------------------------------------------------------------------------------
/** @fn read_field_
 * Wrapper for field reading routine
 * @param fname input filename
 * @param *name output variable name
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
Field read_field_(const char *fname, char *name, char *file, char *func, int line)
{
    sprintf(G__msg__, "Reading field in '%s'...", fname);
    PrintMessage(3, G__msg__);

    Field F;
    F = read_field_hdf5_(fname, name, file, func, line);

    sprintf(G__msg__, "Reading field in '%s' done.", fname);
    PrintMessage(3, G__msg__);

    return F;
} //read_field_
///-------------------------------------------------------------------------------------
/** @fn read_field_header
 * Wrapper for field metadata reading routine
 * @param fname input filename
 */
Field read_field_header(const char *fname)
{
    return read_field_hdf5_header(fname);
} //read_field_header
///-------------------------------------------------------------------------------------
/** @fn write_basefieldmeta_hdf5
 * Subroutine to write the metadata of a field to hdf5 file
 */
static void write_basefieldmeta_hdf5(hid_t file_id, BASEFIELDMETA_ARGS)
{
    write_attribute_double(file_id, "/info/scalars/L0", &L0);
    write_attribute_double(file_id, "/info/scalars/L1", &L1);
    write_attribute_double(file_id, "/info/scalars/L2", &L2);
    write_attribute_double(file_id, "/info/scalars/corner0", &corner0);
    write_attribute_double(file_id, "/info/scalars/corner1", &corner1);
    write_attribute_double(file_id, "/info/scalars/corner2", &corner2);
    write_attribute_int(file_id, "/info/scalars/N0", &N0);
    write_attribute_int(file_id, "/info/scalars/N1", &N1);
    write_attribute_int(file_id, "/info/scalars/N2", &N2);
    write_attribute_int(file_id, "/info/scalars/rank", &rank);
} //write_basefieldmeta_hdf5
///-------------------------------------------------------------------------------------
/** @fn read_basefieldmeta_hdf5
 * Subroutine to read the metadata of a field in hdf5 file
 */
static void read_basefieldmeta_hdf5(hid_t file_id, BASEFIELDMETA_PTR_ARGS)
{
    read_attribute_double(file_id, "/info/scalars/L0", L0);
    read_attribute_double(file_id, "/info/scalars/L1", L1);
    read_attribute_double(file_id, "/info/scalars/L2", L2);
    read_attribute_double(file_id, "/info/scalars/corner0", corner0);
    read_attribute_double(file_id, "/info/scalars/corner1", corner1);
    read_attribute_double(file_id, "/info/scalars/corner2", corner2);
    read_attribute_int(file_id, "/info/scalars/N0", N0);
    read_attribute_int(file_id, "/info/scalars/N1", N1);
    read_attribute_int(file_id, "/info/scalars/N2", N2);
    read_attribute_int(file_id, "/info/scalars/rank", rank);
} //read_basefieldmeta_hdf5
///-------------------------------------------------------------------------------------
/** @fn write_basefield_hdf5
 * Routine to write HDF5 field files
 * @param BF the basefield in standard structure
 * @param fname output filename
 */
void write_basefield_hdf5(BaseField BF, const char *fname)
{
    hid_t file_id;
    int rank=BF.rank;
    int N0=BF.N0;
    int N1=BF.N1;
    int N2=BF.N2;
    double corner0=BF.corner0, corner1=BF.corner1, corner2=BF.corner2;
    double L0=BF.L0, L1=BF.L1, L2=BF.L2;

    /* Create a new file using default properties. */
    file_id = H5Fcreate(fname, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

        /* Create a groups in the file. */
        add_group(file_id, "/info");
        add_group(file_id, "/info/scalars");
        add_group(file_id, "/scalars");

        /* Write basefield metadata */
        write_basefieldmeta_hdf5(file_id, L0, L1, L2, corner0, corner1, corner2, rank, N0, N1, N2);

        /* Write data */
        write_field_data(file_id, "/scalars/field", BF.data, rank, N0, N1, N2);

    /* Close the file */
    H5Fclose(file_id);
} //write_basefield_hdf5
///-------------------------------------------------------------------------------------
/** @fn read_basefield_hdf5_
 * Routine to read HDF5 field files
 * @param fname input filename
 * @param *name output variable name
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 * @return the basefield in standard format
 */
BaseField read_basefield_hdf5_(const char *fname, char *name, char *file, char *func, int line)
{
    hid_t file_id;
    BaseField BF;

    /* Check that the file exists */
    if(!exists(fname))
    {
        sprintf(G__msg__, "Failed to open file '%s'.", fname);
        FatalError(G__msg__);
    }

    /* Open the file. */
    file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);

        /* Read metadata */
        read_basefieldmeta_hdf5(file_id, &BF.L0, &BF.L1, &BF.L2, &BF.corner0, &BF.corner1, &BF.corner2, &BF.N0, &BF.N1, &BF.N2, &BF.rank);

        /* Allocate memory */
        char vname[50];
        sprintf(vname, "%s.data", name);
        BF.data = (float_t*)p_fftw_malloc_(BF.rank*BF.N0*BF.N1*BF.N2*sizeof(float_t), vname, file, func, line);

        /* Read data */
        read_field_data(file_id, "/scalars/field", BF.data, BF.rank, BF.N0, BF.N1, BF.N2);

    /* Close the file */
    H5Fclose(file_id);

    return BF;
} //read_basefield_hdf5_
///-------------------------------------------------------------------------------------
/** @fn read_basefield_hdf5_header
 * Routine to read the attributes of an HDF5 field file
 * @param fname input filename
 * @return the field in standard format, without the data itself
 */
BaseField read_basefield_hdf5_header(const char *fname)
{
    hid_t file_id;
    BaseField BF;

    /* Check that the file exists */
    if(!exists(fname))
    {
        sprintf(G__msg__, "Failed to open file '%s'.", fname);
        FatalError(G__msg__);
    }

    /* Open the file. */
    file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);

        /* Read metadata */
        read_basefieldmeta_hdf5(file_id, &BF.L0, &BF.L1, &BF.L2, &BF.corner0, &BF.corner1, &BF.corner2, &BF.N0, &BF.N1, &BF.N2, &BF.rank);

    /* Close the file */
    H5Fclose(file_id);

    return BF;
} //read_basefield_hdf5_header
///-------------------------------------------------------------------------------------
/** @fn write_field_hdf5
 * Routine to write HDF5 field files
 * @param F the field in standard structure
 * @param fname output filename
 */
void write_field_hdf5(Field F, const char *fname)
{
    hid_t file_id;
    int rank=F.rank;
    int N0=F.N0;
    int N1=F.N1;
    int N2=F.N2;
    double time=F.time;
    double corner0=F.corner0, corner1=F.corner1, corner2=F.corner2;
    double L0=F.L0, L1=F.L1, L2=F.L2;

    /* Create a new file using default properties. */
    file_id = H5Fcreate(fname, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

        /* Create a groups in the file. */
        add_group(file_id, "/info");
        add_group(file_id, "/info/scalars");
        add_group(file_id, "/scalars");

        /* Write basefield metadata */
        write_basefieldmeta_hdf5(file_id, L0, L1, L2, corner0, corner1, corner2, rank, N0, N1, N2);

        /* Write time */
        write_attribute_double(file_id, "/info/scalars/time", &time);

        /* Write data */
        write_field_data(file_id, "/scalars/field", F.data, rank, N0, N1, N2);

    /* Close the file */
    H5Fclose(file_id);
} //write_field_hdf5
///-------------------------------------------------------------------------------------
/** @fn read_field_hdf5_
 * Routine to read HDF5 field files
 * @param fname input filename
 * @param *name output variable name
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 * @return the field in standard format
 */
Field read_field_hdf5_(const char *fname, char *name, char *file, char *func, int line)
{
    hid_t file_id;
    Field F;

    /* Check that the file exists */
    if(!exists(fname))
    {
        sprintf(G__msg__, "Failed to open file '%s'.", fname);
        FatalError(G__msg__);
    }

    /* Open the file. */
    file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);

        /* Read metadata */
        read_basefieldmeta_hdf5(file_id, &F.L0, &F.L1, &F.L2, &F.corner0, &F.corner1, &F.corner2, &F.N0, &F.N1, &F.N2, &F.rank);

        /* Read time */
        read_attribute_double(file_id, "/info/scalars/time", &F.time);

        /* Allocate memory */
        char vname[50];
        sprintf(vname, "%s.data", name);
        F.data = (float_t*)p_fftw_malloc_(F.rank*F.N0*F.N1*F.N2*sizeof(float_t), vname, file, func, line);

        /* Read data */
        read_field_data(file_id, "/scalars/field", F.data, F.rank, F.N0, F.N1, F.N2);

    /* Close the file */
    H5Fclose(file_id);

    return F;
} //read_field_hdf5_
///-------------------------------------------------------------------------------------
/** @fn read_field_hdf5_header
 * Routine to read the attributes of an HDF5 field file
 * @param fname input filename
 * @return the field in standard format, without the data itself
 */
Field read_field_hdf5_header(const char *fname)
{
    hid_t file_id;
    Field F;

    /* Check that the file exists */
    if(!exists(fname))
    {
        sprintf(G__msg__, "Failed to open file '%s'.", fname);
        FatalError(G__msg__);
    }

    /* Open the file. */
    file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);

        /* Read metadata */
        read_basefieldmeta_hdf5(file_id, &F.L0, &F.L1, &F.L2, &F.corner0, &F.corner1, &F.corner2, &F.N0, &F.N1, &F.N2, &F.rank);

        /* Read time */
        read_attribute_double(file_id, "/info/scalars/time", &F.time);

    /* Close the file */
    H5Fclose(file_id);

    return F;
} //read_field_hdf5_header
///-------------------------------------------------------------------------------------
/** @fn write_scalar_field_3D
 * Wrapper for field writing routine
 * @param F the field in standard structure
 * @param fname output filename
 */
void write_scalar_field_3D(Field F, const char *fname)
{
    sprintf(G__msg__, "Writing field to '%s'...", fname);
    PrintMessage(3, G__msg__);

    write_scalar_field_3D_hdf5(F, fname);

    sprintf(G__msg__, "Writing field to '%s' done.", fname);
    PrintMessage(3, G__msg__);
} //write_scalar_field_3D
///-------------------------------------------------------------------------------------
/** @fn write_scalar_field_3D_hdf5
 * Write a scalar field as 3D array in HDF5 file
 * @param F the field in standard structure
 * @param fname output filename
 */
void write_scalar_field_3D_hdf5(Field F, const char *fname)
{
    hid_t file_id;
    int rank=1;
    int N0=F.N0;
    int N1=F.N1;
    int N2=F.N2;
    double time=F.time;
    double corner0=F.corner0, corner1=F.corner1, corner2=F.corner2;
    double L0=F.L0, L1=F.L1, L2=F.L2;

    /* Create a new file using default properties. */
    file_id = H5Fcreate(fname, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

        /* Create a groups in the file. */
        add_group(file_id, "/info");
        add_group(file_id, "/info/scalars");
        add_group(file_id, "/scalars");

        /* Write basefield metadata */
        write_basefieldmeta_hdf5(file_id, L0, L1, L2, corner0, corner1, corner2, rank, N0, N1, N2);

        /* Write time */
        write_attribute_double(file_id, "/info/scalars/time", &time);

        /* Write data */
        write_float_t_3d_array(file_id, "scalars/field", N0, N1, N2, (float_t ***)F.data);

    /* Close the file */
    H5Fclose(file_id);
} //write_scalar_field_3D_hdf5
///-------------------------------------------------------------------------------------
/** @fn read_field_chunk_3D_periodic_
 * Wrapper for read_field_chunk_3D_periodic_hdf5
 * @param fname input filename
 * @param N0_chunk chunk size x
 * @param N1_chunk chunk size y
 * @param N2_chunk chunk size z
 * @param offset_0 offset x
 * @param offset_1 offset y
 * @param offset_2 offset z
 * @param *name output variable name
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
Field read_field_chunk_3D_periodic_(const char *fname, const int N0_chunk, const int N1_chunk, const int N2_chunk, const int offset_0, const int offset_1, const int offset_2, char *name, char *file, char *func, int line)
{
    sprintf(G__msg__, "Reading field chunk in '%s'...", fname);
    PrintMessage(3, G__msg__);

    Field F;
    F = read_field_chunk_3D_periodic_hdf5_(fname, N0_chunk, N1_chunk, N2_chunk, offset_0, offset_1, offset_2, name, file, func, line);

    sprintf(G__msg__, "Reading field chunk in '%s' done.", fname);
    PrintMessage(3, G__msg__);

    return F;
} //read_field_chunk_3D_periodic_
///-------------------------------------------------------------------------------------
/** @fn read_field_chunk_3D_periodic_hdf5_
 * @param fname input filename
 * @param N0_chunk chunk size x
 * @param N1_chunk chunk size y
 * @param N2_chunk chunk size z
 * @param offset_0 offset x
 * @param offset_1 offset y
 * @param offset_2 offset z
 * @param *name output variable name
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
Field read_field_chunk_3D_periodic_hdf5_(const char *fname, const int N0_chunk, const int N1_chunk, const int N2_chunk, const int offset_0, const int offset_1, const int offset_2, char *name, char *file, char *func, int line)
{
    hid_t file_id, dataspace_id, dataset_id, memspace_id;
    double L0, L1, L2, corner0, corner1, corner2;
    int N0, N1, N2;
    Field F;

    /* Check that the file exists */
    if(!exists(fname))
    {
        sprintf(G__msg__, "Failed to open file '%s'.", fname);
        FatalError(G__msg__);
    }

    /* Open the file. */
    file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);

        /* Read metadata */
        read_basefieldmeta_hdf5(file_id, &L0, &L1, &L2, &corner0, &corner1, &corner2, &N0, &N1, &N2, &F.rank);
        if(F.rank!=1)
        {
            sprintf(G__msg__, "Field rank must be one, read '%d'.", F.rank);
            FatalError(G__msg__);
        }
        F.N0=N0_chunk; F.N1=N1_chunk; F.N2=N2_chunk;
        F.L0=L0*(double)N0_chunk/((double)N0); F.L1=L1*(double)N1_chunk/((double)N1); F.L2=L2*(double)N2_chunk/((double)N2);
        F.corner0=corner0+L0*(double)offset_0/((double)N0); F.corner1=corner1+L1*(double)offset_1/((double)N1); F.corner2=corner2+L2*(double)offset_2/((double)N2);

        p_assert(0<=offset_0 && 0<=offset_1 && 0<=offset_2);
        p_assert(offset_0<N0 && offset_1<N1 && offset_2<N2);

        /* Read time */
        read_attribute_double(file_id, "/info/scalars/time", &F.time);

        /* Allocate memory and create memory space */
        char vname[50];
        sprintf(vname, "%s.data", name);
        F.data = (float_t *)p_fftw_malloc_(N0_chunk*N1_chunk*N2_chunk*sizeof(float_t), vname, file, func, line);
        hsize_t dimsm[3] = {N0_chunk, N1_chunk, N2_chunk};
        memspace_id = H5Screate_simple(3, dimsm, NULL);

        /* Read data, account for periodic boundary conditions */
        dataset_id = H5Dopen(file_id, "/scalars/field", H5P_DEFAULT);
        dataspace_id = H5Dget_space(dataset_id);

        const int N0_a = min(N0 - offset_0, N0_chunk); const int N0_b = min(N0_chunk - N0_a, offset_0);
        const int N1_a = min(N1 - offset_1, N1_chunk); const int N1_b = min(N1_chunk - N1_a, offset_1);
        const int N2_a = min(N2 - offset_2, N2_chunk); const int N2_b = min(N2_chunk - N2_a, offset_2);
        debugPrintInt(N0_a);
        debugPrintInt(N1_a);
        debugPrintInt(N2_a);
        debugPrintInt(N0_b);
        debugPrintInt(N1_b);
        debugPrintInt(N2_b);

        // Block 0,0,0
        hsize_t offset[3] = {offset_0, offset_1, offset_2};
        hsize_t count[3] = {N0_a, N1_a, N2_a};
        H5Sselect_hyperslab(dataspace_id, H5S_SELECT_SET, offset, NULL, count, NULL);

        hsize_t offset_out[3] = {0, 0, 0};
        hsize_t count_out[3] = {N0_a, N1_a, N2_a};
        H5Sselect_hyperslab(memspace_id, H5S_SELECT_SET, offset_out, NULL, count_out, NULL);

        H5Dread(dataset_id, H5T_FLOAT_T, memspace_id, dataspace_id, H5P_DEFAULT, (float_t ***)F.data);

        // Block 1,0,0
        if(N0_b>0)
        {
            hsize_t offset[3] = {0, offset_1, offset_2};
            hsize_t count[3] = {N0_b, N1_a, N2_a};
            H5Sselect_hyperslab(dataspace_id, H5S_SELECT_SET, offset, NULL, count, NULL);

            hsize_t offset_out[3] = {N0_a, 0, 0};
            hsize_t count_out[3] = {N0_b, N1_a, N2_a};
            H5Sselect_hyperslab(memspace_id, H5S_SELECT_SET, offset_out, NULL, count_out, NULL);

            H5Dread(dataset_id, H5T_FLOAT_T, memspace_id, dataspace_id, H5P_DEFAULT, (float_t ***)F.data);
        }

        // Block 0,1,0
        if(N1_b>0)
        {
            hsize_t offset[3] = {offset_0, 0, offset_2};
            hsize_t count[3] = {N0_a, N1_b, N2_a};
            H5Sselect_hyperslab(dataspace_id, H5S_SELECT_SET, offset, NULL, count, NULL);

            hsize_t offset_out[3] = {0, N1_a, 0};
            hsize_t count_out[3] = {N0_a, N1_b, N2_a};
            H5Sselect_hyperslab(memspace_id, H5S_SELECT_SET, offset_out, NULL, count_out, NULL);

            H5Dread(dataset_id, H5T_FLOAT_T, memspace_id, dataspace_id, H5P_DEFAULT, (float_t ***)F.data);
        }

        // Block 0,0,1
        if(N2_b>0)
        {
            hsize_t offset[3] = {offset_0, offset_1, 0};
            hsize_t count[3] = {N0_a, N1_a, N2_b};
            H5Sselect_hyperslab(dataspace_id, H5S_SELECT_SET, offset, NULL, count, NULL);

            hsize_t offset_out[3] = {0, 0, N2_a};
            hsize_t count_out[3] = {N0_a, N1_a, N2_b};
            H5Sselect_hyperslab(memspace_id, H5S_SELECT_SET, offset_out, NULL, count_out, NULL);

            H5Dread(dataset_id, H5T_FLOAT_T, memspace_id, dataspace_id, H5P_DEFAULT, (float_t ***)F.data);
        }

        // Block 1,1,0
        if(N0_b>0 && N1_b>0)
        {
            hsize_t offset[3] = {0, 0, offset_2};
            hsize_t count[3] = {N0_b, N1_b, N2_a};
            H5Sselect_hyperslab(dataspace_id, H5S_SELECT_SET, offset, NULL, count, NULL);

            hsize_t offset_out[3] = {N0_a, N1_a, 0};
            hsize_t count_out[3] = {N0_b, N1_b, N2_a};
            H5Sselect_hyperslab(memspace_id, H5S_SELECT_SET, offset_out, NULL, count_out, NULL);

            H5Dread(dataset_id, H5T_FLOAT_T, memspace_id, dataspace_id, H5P_DEFAULT, (float_t ***)F.data);
        }

        // Block 1,0,1
        if(N0_b>0 && N2_b>0)
        {
            hsize_t offset[3] = {0, offset_1, 0};
            hsize_t count[3] = {N0_b, N1_a, N2_b};
            H5Sselect_hyperslab(dataspace_id, H5S_SELECT_SET, offset, NULL, count, NULL);

            hsize_t offset_out[3] = {N0_a, 0, N2_a};
            hsize_t count_out[3] = {N0_b, N1_a, N2_b};
            H5Sselect_hyperslab(memspace_id, H5S_SELECT_SET, offset_out, NULL, count_out, NULL);

            H5Dread(dataset_id, H5T_FLOAT_T, memspace_id, dataspace_id, H5P_DEFAULT, (float_t ***)F.data);
        }

        // Block 0,1,1
        if(N1_b>0 && N2_b>0)
        {
            hsize_t offset[3] = {offset_0, 0, 0};
            hsize_t count[3] = {N0_a, N1_b, N2_b};
            H5Sselect_hyperslab(dataspace_id, H5S_SELECT_SET, offset, NULL, count, NULL);

            hsize_t offset_out[3] = {0, N1_a, N2_a};
            hsize_t count_out[3] = {N0_a, N1_b, N2_b};
            H5Sselect_hyperslab(memspace_id, H5S_SELECT_SET, offset_out, NULL, count_out, NULL);

            H5Dread(dataset_id, H5T_FLOAT_T, memspace_id, dataspace_id, H5P_DEFAULT, (float_t ***)F.data);
        }

        // Block 1,1,1
        if(N0_b>0 && N1_b>0 && N2_b>0)
        {
            hsize_t offset[3] = {0, 0, 0};
            hsize_t count[3] = {N0_b, N1_b, N2_b};
            H5Sselect_hyperslab(dataspace_id, H5S_SELECT_SET, offset, NULL, count, NULL);

            hsize_t offset_out[3] = {N0_a, N1_a, N2_a};
            hsize_t count_out[3] = {N0_b, N1_b, N2_b};
            H5Sselect_hyperslab(memspace_id, H5S_SELECT_SET, offset_out, NULL, count_out, NULL);

            H5Dread(dataset_id, H5T_FLOAT_T, memspace_id, dataspace_id, H5P_DEFAULT, (float_t ***)F.data);
        }

        H5Sclose(dataspace_id);
        H5Sclose(memspace_id);
        H5Dclose(dataset_id);

    /* Close the file */
    H5Fclose(file_id);

    return F;
} //read_field_chunk_3D_periodic_hdf5_
///-------------------------------------------------------------------------------------
/** @fn replace_field_chunk_3D_periodic
 * Wrapper for replace_field_chunk_3D_periodic_hdf5
 * @param F field structure containing the chunk
 * @param offset_0 offset x
 * @param offset_1 offset y
 * @param offset_2 offset z
 * @param fname output filename
 */
void replace_field_chunk_3D_periodic(Field F, int offset_0, int offset_1, int offset_2, const char *fname)
{
    sprintf(G__msg__, "Replacing field chunk in '%s'...", fname);
    PrintMessage(3, G__msg__);

    replace_field_chunk_3D_periodic_hdf5(F, offset_0, offset_1, offset_2, fname);

    sprintf(G__msg__, "Replacing field chunk in '%s' done.", fname);
    PrintMessage(3, G__msg__);
} //replace_field_chunk_3D_periodic
///-------------------------------------------------------------------------------------
/** @fn replace_field_chunk_3D_periodic_hdf5
 * @param F field structure containing the chunk
 * @param offset_0 offset x
 * @param offset_1 offset y
 * @param offset_2 offset z
 * @param fname output filename
 */
void replace_field_chunk_3D_periodic_hdf5(Field F, int offset_0, int offset_1, int offset_2, const char *fname)
{
    hid_t file_id, dataspace_id, dataset_id, memspace_id;
    int N0, N1, N2; double L0, L1, L2; double corner0, corner1, corner2; int rank;
    int N0_chunk=F.N0, N1_chunk=F.N1, N2_chunk=F.N2;

    /* Check that the file exists */
    if(!exists(fname))
    {
        sprintf(G__msg__, "Failed to open file '%s'.", fname);
        FatalError(G__msg__);
    }

    /* Open the file. */
    file_id = H5Fopen(fname, H5F_ACC_RDWR, H5P_DEFAULT);

        /* Check metadata */
        read_basefieldmeta_hdf5(file_id, &L0, &L1, &L2, &corner0, &corner1, &corner2, &N0, &N1, &N2, &rank);
        if(F.rank!=1)
        {
            sprintf(G__msg__, "Field rank must be one, got '%d'.", F.rank);
            FatalError(G__msg__);
        }

        /* Copy chunck, checking for periodic boundary conditions */
        float_t data_out[N0_chunk][N1_chunk][N2_chunk];
        #pragma omp parallel for schedule(static) collapse(3)
        for(int mc0=0; mc0<N0_chunk; mc0++)
            for(int mc1=0; mc1<N1_chunk; mc1++)
                for(int mc2=0; mc2<N2_chunk; mc2++)
                    data_out[mc0][mc1][mc2] = F.data[get_index(mc0,mc1,mc2,N0_chunk,N1_chunk,N2_chunk)];

        /* Create memory space */
        hsize_t dimsm[3] = {N0_chunk, N1_chunk, N2_chunk};
        memspace_id = H5Screate_simple(3, dimsm, NULL);

        /* Write data, account for periodic boundary conditions */
        dataset_id = H5Dopen(file_id, "/scalars/field", H5P_DEFAULT);
        dataspace_id = H5Dget_space(dataset_id);

        int N0_a = min(N0-offset_0,N0_chunk); int N0_b = N0_chunk - N0_a;
        int N1_a = min(N1-offset_1,N1_chunk); int N1_b = N1_chunk - N1_a;
        int N2_a = min(N2-offset_2,N2_chunk); int N2_b = N2_chunk - N2_a;

        // Block 0,0,0
        hsize_t offset[3] = {offset_0, offset_1, offset_2};
        hsize_t count[3] = {N0_a, N1_a, N2_a};
        H5Sselect_hyperslab(dataspace_id, H5S_SELECT_SET, offset, NULL, count, NULL);

        hsize_t offset_out[3] = {0, 0, 0};
        hsize_t count_out[3] = {N0_a, N1_a, N2_a};
        H5Sselect_hyperslab(memspace_id, H5S_SELECT_SET, offset_out, NULL, count_out, NULL);

        H5Dwrite(dataset_id, H5T_FLOAT_T, memspace_id, dataspace_id, H5P_DEFAULT, data_out);

        // Block 1,0,0
        if(N0_b>0)
        {
            hsize_t offset[3] = {0, offset_1, offset_2};
            hsize_t count[3] = {N0_b, N1_a, N2_a};
            H5Sselect_hyperslab(dataspace_id, H5S_SELECT_SET, offset, NULL, count, NULL);

            hsize_t offset_out[3] = {N0_a, 0, 0};
            hsize_t count_out[3] = {N0_b, N1_a, N2_a};
            H5Sselect_hyperslab(memspace_id, H5S_SELECT_SET, offset_out, NULL, count_out, NULL);

            H5Dwrite(dataset_id, H5T_FLOAT_T, memspace_id, dataspace_id, H5P_DEFAULT, data_out);
        }

        // Block 0,1,0
        if(N1_b>0)
        {
            hsize_t offset[3] = {offset_0, 0, offset_2};
            hsize_t count[3] = {N0_a, N1_b, N2_a};
            H5Sselect_hyperslab(dataspace_id, H5S_SELECT_SET, offset, NULL, count, NULL);

            hsize_t offset_out[3] = {0, N1_a, 0};
            hsize_t count_out[3] = {N0_a, N1_b, N2_a};
            H5Sselect_hyperslab(memspace_id, H5S_SELECT_SET, offset_out, NULL, count_out, NULL);

            H5Dwrite(dataset_id, H5T_FLOAT_T, memspace_id, dataspace_id, H5P_DEFAULT, data_out);
        }

        // Block 0,0,1
        if(N2_b>0)
        {
            hsize_t offset[3] = {offset_0, offset_1, 0};
            hsize_t count[3] = {N0_a, N1_a, N2_b};
            H5Sselect_hyperslab(dataspace_id, H5S_SELECT_SET, offset, NULL, count, NULL);

            hsize_t offset_out[3] = {0, 0, N2_a};
            hsize_t count_out[3] = {N0_a, N1_a, N2_b};
            H5Sselect_hyperslab(memspace_id, H5S_SELECT_SET, offset_out, NULL, count_out, NULL);

            H5Dwrite(dataset_id, H5T_FLOAT_T, memspace_id, dataspace_id, H5P_DEFAULT, data_out);
        }

        // Block 1,1,0
        if(N0_b>0 && N1_b>0)
        {
            hsize_t offset[3] = {0, 0, offset_2};
            hsize_t count[3] = {N0_b, N1_b, N2_a};
            H5Sselect_hyperslab(dataspace_id, H5S_SELECT_SET, offset, NULL, count, NULL);

            hsize_t offset_out[3] = {N0_a, N1_a, 0};
            hsize_t count_out[3] = {N0_b, N1_b, N2_a};
            H5Sselect_hyperslab(memspace_id, H5S_SELECT_SET, offset_out, NULL, count_out, NULL);

            H5Dwrite(dataset_id, H5T_FLOAT_T, memspace_id, dataspace_id, H5P_DEFAULT, data_out);
        }

        // Block 1,0,1
        if(N0_b>0 && N2_b>0)
        {
            hsize_t offset[3] = {0, offset_1, 0};
            hsize_t count[3] = {N0_b, N1_a, N2_b};
            H5Sselect_hyperslab(dataspace_id, H5S_SELECT_SET, offset, NULL, count, NULL);

            hsize_t offset_out[3] = {N0_a, 0, N2_a};
            hsize_t count_out[3] = {N0_b, N1_a, N2_b};
            H5Sselect_hyperslab(memspace_id, H5S_SELECT_SET, offset_out, NULL, count_out, NULL);

            H5Dwrite(dataset_id, H5T_FLOAT_T, memspace_id, dataspace_id, H5P_DEFAULT, data_out);
        }

        // Block 0,1,1
        if(N1_b>0 && N2_b>0)
        {
            hsize_t offset[3] = {offset_0, 0, 0};
            hsize_t count[3] = {N0_a, N1_b, N2_b};
            H5Sselect_hyperslab(dataspace_id, H5S_SELECT_SET, offset, NULL, count, NULL);

            hsize_t offset_out[3] = {0, N1_a, N2_a};
            hsize_t count_out[3] = {N0_a, N1_b, N2_b};
            H5Sselect_hyperslab(memspace_id, H5S_SELECT_SET, offset_out, NULL, count_out, NULL);

            H5Dwrite(dataset_id, H5T_FLOAT_T, memspace_id, dataspace_id, H5P_DEFAULT, data_out);
        }

        // Block 1,1,1
        if(N0_b>0 && N1_b>0 && N2_b>0)
        {
            hsize_t offset[3] = {0, 0, 0};
            hsize_t count[3] = {N0_b, N1_b, N2_b};
            H5Sselect_hyperslab(dataspace_id, H5S_SELECT_SET, offset, NULL, count, NULL);

            hsize_t offset_out[3] = {N0_a, N1_a, N2_a};
            hsize_t count_out[3] = {N0_b, N1_b, N2_b};
            H5Sselect_hyperslab(memspace_id, H5S_SELECT_SET, offset_out, NULL, count_out, NULL);

            H5Dwrite(dataset_id, H5T_FLOAT_T, memspace_id, dataspace_id, H5P_DEFAULT, data_out);
        }

        H5Sclose(dataspace_id);
        H5Sclose(memspace_id);
        H5Dclose(dataset_id);

    /* Close the file */
    H5Fclose(file_id);
} //replace_field_chunk_3D_periodic_hdf5
///-------------------------------------------------------------------------------------
/** @fn write_particle_pos_hdf5
 * Routine to write particle's positions to a HDF5 file
 * @param Np number of particles
 * @param x[Np] particle positions
 * @param Np0 particle grid size x
 * @param Np1 particle grid size y
 * @param Np2 particle grid size z
 * @param fname output filename
 */
void write_particle_pos_hdf5(particleID_t Np, particle_pos_t x[Np], int Np0, int Np1, int Np2, char *fname)
{
    hid_t file_id, dataspace_id, dataset_id;

    /* Create a new file using default properties. */
    file_id = H5Fcreate(fname, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

        /* Create a groups in the file. */
        add_group(file_id, "/info");
        add_group(file_id, "/info/scalars");
        add_group(file_id, "/scalars");

        /* Write metadata */
        write_attribute_int(file_id, "/info/scalars/Np0", &Np0);
        write_attribute_int(file_id, "/info/scalars/Np1", &Np1);
        write_attribute_int(file_id, "/info/scalars/Np2", &Np2);
        write_attribute_particleID_t(file_id, "/info/scalars/Np", &Np);

        /* Define datatypes and create dataspace */
        hid_t H5T_PARTICLE_POS_T = H5Tcreate(H5T_COMPOUND, sizeof(particle_pos_t));
        H5Tinsert(H5T_PARTICLE_POS_T, "Pos0", HOFFSET(particle_pos_t, Pos[0]), H5T_FLOAT_T);
        H5Tinsert(H5T_PARTICLE_POS_T, "Pos1", HOFFSET(particle_pos_t, Pos[1]), H5T_FLOAT_T);
        H5Tinsert(H5T_PARTICLE_POS_T, "Pos2", HOFFSET(particle_pos_t, Pos[2]), H5T_FLOAT_T);

        hsize_t dim = (hsize_t)Np;
        dataspace_id = H5Screate_simple(1, &dim, NULL);

            /* Write data */
            dataset_id = H5Dcreate2(file_id, "/scalars/particle_pos", H5T_PARTICLE_POS_T, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
            H5Dwrite(dataset_id, H5T_PARTICLE_POS_T, H5S_ALL, H5S_ALL, H5P_DEFAULT, x);
            H5Dclose(dataset_id);

        H5Sclose(dataspace_id);

    /* Close the file */
    H5Fclose(file_id);
} //write_particle_pos_hdf5
///-------------------------------------------------------------------------------------
/** @fn read_particle_pos_hdf5
 * Routine to read particle's positions from a HDF5 file
 * @param Np number of particles
 * @param x[Np] particle positions
 * @param Np0 particle grid size x
 * @param Np1 particle grid size y
 * @param Np2 particle grid size z
 * @param fname input filename
 */
void read_particle_pos_hdf5(particleID_t Np, particle_pos_t x[Np], int Np0, int Np1, int Np2, char *fname)
{
    hid_t file_id, dataset_id;
    int Np0_read, Np1_read, Np2_read;
    particleID_t Np_read;

    /* Open the file. */
    file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);

        /* Read metadata */
        read_attribute_int(file_id, "/info/scalars/Np0", &Np0_read);
        p_assert(Np0==Np0_read);
        read_attribute_int(file_id, "/info/scalars/Np1", &Np1_read);
        p_assert(Np1==Np1_read);
        read_attribute_int(file_id, "/info/scalars/Np2", &Np2_read);
        p_assert(Np2==Np2_read);
        read_attribute_particleID_t(file_id, "/info/scalars/Np", &Np_read);
        p_assert(Np==Np_read);

        /* Read data */
        hid_t H5T_PARTICLE_POS_T = H5Tcreate(H5T_COMPOUND, sizeof(particle_pos_t));
        H5Tinsert(H5T_PARTICLE_POS_T, "Pos0", HOFFSET(particle_pos_t, Pos[0]), H5T_FLOAT_T);
        H5Tinsert(H5T_PARTICLE_POS_T, "Pos1", HOFFSET(particle_pos_t, Pos[1]), H5T_FLOAT_T);
        H5Tinsert(H5T_PARTICLE_POS_T, "Pos2", HOFFSET(particle_pos_t, Pos[2]), H5T_FLOAT_T);

        dataset_id = H5Dopen2(file_id, "/scalars/particle_pos", H5P_DEFAULT);
        H5Dread(dataset_id, H5T_PARTICLE_POS_T, H5S_ALL, H5S_ALL, H5P_DEFAULT, x);
        H5Dclose(dataset_id);

    /* Close the file */
    H5Fclose(file_id);
} //read_particle_pos_hdf5
///-------------------------------------------------------------------------------------
/** @fn write_particle_vel_hdf5
 * Routine to write particle's velocities to a HDF5 file
 * @param Np number of particles
 * @param p[Np] particle velocities/momenta
 * @param Np0 particle grid size x
 * @param Np1 particle grid size y
 * @param Np2 particle grid size z
 * @param fname output filename
 */
void write_particle_vel_hdf5(particleID_t Np, particle_vel_t p[Np], int Np0, int Np1, int Np2, char *fname)
{
    hid_t file_id, dataspace_id, dataset_id;

    /* Create a new file using default properties. */
    file_id = H5Fcreate(fname, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

        /* Create a groups in the file. */
        add_group(file_id, "/info");
        add_group(file_id, "/info/scalars");
        add_group(file_id, "/scalars");

        /* Write metadata */
        write_attribute_int(file_id, "/info/scalars/Np0", &Np0);
        write_attribute_int(file_id, "/info/scalars/Np1", &Np1);
        write_attribute_int(file_id, "/info/scalars/Np2", &Np2);
        write_attribute_particleID_t(file_id, "/info/scalars/Np", &Np);

        /* Define datatypes and create dataspace */
        hid_t H5T_PARTICLE_VEL_T = H5Tcreate(H5T_COMPOUND, sizeof(particle_vel_t));
        H5Tinsert(H5T_PARTICLE_VEL_T, "Vel0", HOFFSET(particle_vel_t, Vel[0]), H5T_FLOAT_T);
        H5Tinsert(H5T_PARTICLE_VEL_T, "Vel1", HOFFSET(particle_vel_t, Vel[1]), H5T_FLOAT_T);
        H5Tinsert(H5T_PARTICLE_VEL_T, "Vel2", HOFFSET(particle_vel_t, Vel[2]), H5T_FLOAT_T);

        hsize_t dim = (hsize_t)Np;
        dataspace_id = H5Screate_simple(1, &dim, NULL);

            /* Write data */
            dataset_id = H5Dcreate2(file_id, "/scalars/particle_vel", H5T_PARTICLE_VEL_T, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
            H5Dwrite(dataset_id, H5T_PARTICLE_VEL_T, H5S_ALL, H5S_ALL, H5P_DEFAULT, p);
            H5Dclose(dataset_id);

        H5Sclose(dataspace_id);

    /* Close the file */
    H5Fclose(file_id);
} //write_particle_vel_hdf5
///-------------------------------------------------------------------------------------
/** @fn read_particle_vel_hdf5
 * Routine to read particle's velocities from a HDF5 file
 * @param Np number of particles
 * @param p[Np] particle velocities/momenta
 * @param Np0 particle grid size x
 * @param Np1 particle grid size y
 * @param Np2 particle grid size z
 * @param fname input filename
 */
void read_particle_vel_hdf5(particleID_t Np, particle_vel_t p[Np], int Np0, int Np1, int Np2, char *fname)
{
    hid_t file_id, dataset_id;
    int Np0_read, Np1_read, Np2_read;
    particleID_t Np_read;

    /* Open the file. */
    file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);

        /* Read metadata */
        read_attribute_int(file_id, "/info/scalars/Np0", &Np0_read);
        p_assert(Np0==Np0_read);
        read_attribute_int(file_id, "/info/scalars/Np1", &Np1_read);
        p_assert(Np1==Np1_read);
        read_attribute_int(file_id, "/info/scalars/Np2", &Np2_read);
        p_assert(Np2==Np2_read);
        read_attribute_particleID_t(file_id, "/info/scalars/Np", &Np_read);
        p_assert(Np==Np_read);

        /* Read data */
        hid_t H5T_PARTICLE_VEL_T = H5Tcreate(H5T_COMPOUND, sizeof(particle_vel_t));
        H5Tinsert(H5T_PARTICLE_VEL_T, "Vel0", HOFFSET(particle_vel_t, Vel[0]), H5T_FLOAT_T);
        H5Tinsert(H5T_PARTICLE_VEL_T, "Vel1", HOFFSET(particle_vel_t, Vel[1]), H5T_FLOAT_T);
        H5Tinsert(H5T_PARTICLE_VEL_T, "Vel2", HOFFSET(particle_vel_t, Vel[2]), H5T_FLOAT_T);

        dataset_id = H5Dopen2(file_id, "/scalars/particle_vel", H5P_DEFAULT);
        H5Dread(dataset_id, H5T_PARTICLE_VEL_T, H5S_ALL, H5S_ALL, H5P_DEFAULT, p);
        H5Dclose(dataset_id);

    /* Close the file */
    H5Fclose(file_id);
} //read_particle_vel_hdf5
///-------------------------------------------------------------------------------------
/** @fn write_tile_hdf5
 * Routine to write the result of a tile to a HDF5 file
 * @param Np_tile number of particles in the tile
 * @param Psi_tile[Np_tile] displacement field in the tile
 * @param p_tile[Np_tile] particle momenta in the tile
 * @param mp_tile_corner_full particleID of tile corner in the full box
 * @param Np0 particle grid size x (full box)
 * @param Np1 particle grid size y (full box)
 * @param Np2 particle grid size z (full box)
 * @param Np0_tile particle grid size x (tile)
 * @param Np1_tile particle grid size y (tile)
 * @param Np2_tile particle grid size z (tile)
 * @param L0 box size x (full box)
 * @param L1 box size y (full box)
 * @param L2 box size z (full box)
 * @param L0_tile box size x (tile)
 * @param L1_tile box size y (tile)
 * @param L2_tile box size z (tile)
 * @param fname output filename
 */
void write_tile_hdf5(particleID_t Np_tile, particle_pos_t Psi_tile[Np_tile], particle_vel_t p_tile[Np_tile], particleID_t mp_tile_corner_full, int Np0, int Np1, int Np2, int Np0_tile, int Np1_tile, int Np2_tile, double L0, double L1, double L2, double L0_tile, double L1_tile, double L2_tile, char *fname)
{
    hid_t file_id, dataspace_id, dataset_id;

    /* Create a new file using default properties. */
    file_id = H5Fcreate(fname, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

        /* Create a groups in the file. */
        add_group(file_id, "/info");
        add_group(file_id, "/info/scalars");
        add_group(file_id, "/scalars");

        /* Write metadata */
        write_attribute_double(file_id, "/info/scalars/L0", &L0);
        write_attribute_double(file_id, "/info/scalars/L1", &L1);
        write_attribute_double(file_id, "/info/scalars/L2", &L2);
        write_attribute_double(file_id, "/info/scalars/L0_tile", &L0_tile);
        write_attribute_double(file_id, "/info/scalars/L1_tile", &L1_tile);
        write_attribute_double(file_id, "/info/scalars/L2_tile", &L2_tile);
        write_attribute_int(file_id, "/info/scalars/Np0", &Np0);
        write_attribute_int(file_id, "/info/scalars/Np1", &Np1);
        write_attribute_int(file_id, "/info/scalars/Np2", &Np2);
        write_attribute_int(file_id, "/info/scalars/Np0_tile", &Np0_tile);
        write_attribute_int(file_id, "/info/scalars/Np1_tile", &Np1_tile);
        write_attribute_int(file_id, "/info/scalars/Np2_tile", &Np2_tile);
        write_attribute_particleID_t(file_id, "/info/scalars/Np_tile", &Np_tile);
        write_attribute_particleID_t(file_id, "/info/scalars/mp_tile_corner_full", &mp_tile_corner_full);

        /* Define datatypes and create dataspace */
        hid_t H5T_PARTICLE_PSI_T = H5Tcreate(H5T_COMPOUND, sizeof(particle_pos_t));
        H5Tinsert(H5T_PARTICLE_PSI_T, "Psi0", HOFFSET(particle_pos_t, Pos[0]), H5T_FLOAT_T);
        H5Tinsert(H5T_PARTICLE_PSI_T, "Psi1", HOFFSET(particle_pos_t, Pos[1]), H5T_FLOAT_T);
        H5Tinsert(H5T_PARTICLE_PSI_T, "Psi2", HOFFSET(particle_pos_t, Pos[2]), H5T_FLOAT_T);

        hid_t H5T_PARTICLE_VEL_T = H5Tcreate(H5T_COMPOUND, sizeof(particle_vel_t));
        H5Tinsert(H5T_PARTICLE_VEL_T, "Vel0", HOFFSET(particle_vel_t, Vel[0]), H5T_FLOAT_T);
        H5Tinsert(H5T_PARTICLE_VEL_T, "Vel1", HOFFSET(particle_vel_t, Vel[1]), H5T_FLOAT_T);
        H5Tinsert(H5T_PARTICLE_VEL_T, "Vel2", HOFFSET(particle_vel_t, Vel[2]), H5T_FLOAT_T);

        hsize_t dim = (hsize_t)Np_tile;
        dataspace_id = H5Screate_simple(1, &dim, NULL);

            /* Write data */
            dataset_id = H5Dcreate2(file_id, "/scalars/particle_psi", H5T_PARTICLE_PSI_T, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
            H5Dwrite(dataset_id, H5T_PARTICLE_PSI_T, H5S_ALL, H5S_ALL, H5P_DEFAULT, Psi_tile);
            H5Dclose(dataset_id);

            dataset_id = H5Dcreate2(file_id, "/scalars/particle_vel", H5T_PARTICLE_VEL_T, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
            H5Dwrite(dataset_id, H5T_PARTICLE_VEL_T, H5S_ALL, H5S_ALL, H5P_DEFAULT, p_tile);
            H5Dclose(dataset_id);

        H5Sclose(dataspace_id);

    /* Close the file */
    H5Fclose(file_id);
} //write_tile_hdf5
///-------------------------------------------------------------------------------------
/** @fn read_tile_hdf5
 * Routine to read the result of a tile from a HDF5 file
 * @param Np_tile number of particles in the tile
 * @param Psi_tile[Np_tile] displacement field in the tile
 * @param p_tile[Np_tile] particle momenta in the tile
 * @param mp_tile_corner_full particleID of tile corner in the full box
 * @param Np0 particle grid size x (full box)
 * @param Np1 particle grid size y (full box)
 * @param Np2 particle grid size z (full box)
 * @param Np0_tile particle grid size x (tile)
 * @param Np1_tile particle grid size y (tile)
 * @param Np2_tile particle grid size z (tile)
 * @param L0 box size x (full box)
 * @param L1 box size y (full box)
 * @param L2 box size z (full box)
 * @param L0_tile box size x (tile)
 * @param L1_tile box size y (tile)
 * @param L2_tile box size z (tile)
 * @param fname input filename
 */
void read_tile_hdf5(particleID_t Np_tile, particle_pos_t Psi_tile[Np_tile], particle_vel_t p_tile[Np_tile], particleID_t mp_tile_corner_full, int Np0, int Np1, int Np2, int Np0_tile, int Np1_tile, int Np2_tile, double L0, double L1, double L2, double L0_tile, double L1_tile, double L2_tile, char *fname)
{
    hid_t file_id, dataset_id;
    int Np0_read, Np1_read, Np2_read, Np0_tile_read, Np1_tile_read, Np2_tile_read;
    double L0_read, L1_read, L2_read, L0_tile_read, L1_tile_read, L2_tile_read;
    particleID_t Np_tile_read, mp_tile_corner_full_read;

    /* Open the file. */
    file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);

        /* Read metadata */
        read_attribute_double(file_id, "/info/scalars/L0", &L0_read);
        p_assert(L0==L0_read);
        read_attribute_double(file_id, "/info/scalars/L1", &L1_read);
        p_assert(L1==L1_read);
        read_attribute_double(file_id, "/info/scalars/L2", &L2_read);
        p_assert(L2==L2_read);
        read_attribute_double(file_id, "/info/scalars/L0_tile", &L0_tile_read);
        p_assert(L0_tile==L0_tile_read);
        read_attribute_double(file_id, "/info/scalars/L1_tile", &L1_tile_read);
        p_assert(L1_tile==L1_tile_read);
        read_attribute_double(file_id, "/info/scalars/L2_tile", &L2_tile_read);
        p_assert(L2_tile==L2_tile_read);
        read_attribute_int(file_id, "/info/scalars/Np0", &Np0_read);
        p_assert(Np0==Np0_read);
        read_attribute_int(file_id, "/info/scalars/Np1", &Np1_read);
        p_assert(Np1==Np1_read);
        read_attribute_int(file_id, "/info/scalars/Np2", &Np2_read);
        p_assert(Np2==Np2_read);
        read_attribute_int(file_id, "/info/scalars/Np0_tile", &Np0_tile_read);
        p_assert(Np0_tile==Np0_tile_read);
        read_attribute_int(file_id, "/info/scalars/Np1_tile", &Np1_tile_read);
        p_assert(Np1_tile==Np1_tile_read);
        read_attribute_int(file_id, "/info/scalars/Np2_tile", &Np2_tile_read);
        p_assert( Np2_tile==Np2_tile_read);
        read_attribute_particleID_t(file_id, "/info/scalars/Np_tile", &Np_tile_read);
        p_assert(Np_tile==Np_tile_read);
        read_attribute_particleID_t(file_id, "/info/scalars/mp_tile_corner_full", &mp_tile_corner_full_read);
        p_assert(mp_tile_corner_full==mp_tile_corner_full_read);

        /* Read data */
        hid_t H5T_PARTICLE_PSI_T = H5Tcreate(H5T_COMPOUND, sizeof(particle_pos_t));
        H5Tinsert(H5T_PARTICLE_PSI_T, "Psi0", HOFFSET(particle_pos_t, Pos[0]), H5T_FLOAT_T);
        H5Tinsert(H5T_PARTICLE_PSI_T, "Psi1", HOFFSET(particle_pos_t, Pos[1]), H5T_FLOAT_T);
        H5Tinsert(H5T_PARTICLE_PSI_T, "Psi2", HOFFSET(particle_pos_t, Pos[2]), H5T_FLOAT_T);

        hid_t H5T_PARTICLE_VEL_T = H5Tcreate(H5T_COMPOUND, sizeof(particle_vel_t));
        H5Tinsert(H5T_PARTICLE_VEL_T, "Vel0", HOFFSET(particle_vel_t, Vel[0]), H5T_FLOAT_T);
        H5Tinsert(H5T_PARTICLE_VEL_T, "Vel1", HOFFSET(particle_vel_t, Vel[1]), H5T_FLOAT_T);
        H5Tinsert(H5T_PARTICLE_VEL_T, "Vel2", HOFFSET(particle_vel_t, Vel[2]), H5T_FLOAT_T);

        dataset_id = H5Dopen2(file_id, "/scalars/particle_psi", H5P_DEFAULT);
        H5Dread(dataset_id, H5T_PARTICLE_PSI_T, H5S_ALL, H5S_ALL, H5P_DEFAULT, Psi_tile);
        H5Dclose(dataset_id);

        dataset_id = H5Dopen2(file_id, "/scalars/particle_vel", H5P_DEFAULT);
        H5Dread(dataset_id, H5T_PARTICLE_VEL_T, H5S_ALL, H5S_ALL, H5P_DEFAULT, p_tile);
        H5Dclose(dataset_id);

    /* Close the file */
    H5Fclose(file_id);
} //read_tile_hdf5
///-------------------------------------------------------------------------------------
/** @fn fill_meta_mock
 * This subroutine fills the metadata of a mock
 */
static void fill_meta_mock(Mock *M, const int N0, const int N1, const int N2, const double corner0, const double corner1, const double corner2, const double L0, const double L1, const double L2, const int N_BIAS, const double galaxy_nmean)
{
    M->L0=L0;
    M->L1=L1;
    M->L2=L2;
    M->corner0=corner0;
    M->corner1=corner1;
    M->corner2=corner2;
    M->N0=N0;
    M->N1=N1;
    M->N2=N2;
    M->N_BIAS = N_BIAS;
    M->galaxy_nmean = galaxy_nmean;
} //fill_meta_mock
///-------------------------------------------------------------------------------------
/** @fn allocate_mock_
 * This routine allocates the memory and returns a new field, given its metadata
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param corner0 corner x-position
 * @param corner1 corner y-position
 * @param corner2 corner z-position
 * @param L0 box size in x
 * @param L1 box size in y
 * @param L2 box size in z
 * @param *galaxy_bias galaxy bias (array)
 * @param N_BIAS number of bias parameters
 * @param galaxy_nmean galaxy nmean
 * @param *name output variable name
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 * @return mock
 */
Mock allocate_mock_(const int N0, const int N1, const int N2, const double corner0, const double corner1, const double corner2, const double L0, const double L1, const double L2, const double *galaxy_bias, const int N_BIAS, const double galaxy_nmean, char *name, char *file, char *func, int line)
{
    Mock M;
    char vname[50];
    sprintf(vname, "%s.data", name);
    M.data = (float_t*)p_fftw_malloc_(N0*N1*N2*sizeof(float_t), vname, file, func, line);

    fill_meta_mock(&M, N0, N1, N2, corner0, corner1, corner2, L0, L1, L2, N_BIAS, galaxy_nmean);

    sprintf(vname, "%s.galaxy_bias", name);
    M.galaxy_bias = (double*)p_malloc_(N_BIAS*sizeof(double), vname, file, func, line);

    for(int b=0; b<N_BIAS; b++)
        M.galaxy_bias[b]=galaxy_bias[b];
    return M;
} //allocate_mock_
///-------------------------------------------------------------------------------------
/** @fn free_mock_
 * This routine frees the memory allocated for a mock
 * @param M the mock in standard structure
 */
void free_mock_(Mock M, char *name, char *file, char *func, int line)
{
    char vname[50];
    sprintf(vname, "%s.data", name);
    p_fftw_free_(M.data, vname, file, func, line);
    sprintf(vname, "%s.galaxy_bias", name);
    p_free_(M.galaxy_bias, vname, file, func, line);
} //free_mock_
///-------------------------------------------------------------------------------------
/** @fn write_mock_metadata_hdf5
 * This subroutine writes the metadata of a mock to hdf5
 */
static void write_mock_metadata_hdf5(hid_t file_id, double *galaxy_bias, int N_BIAS, double galaxy_nmean)
{
    write_attribute_double(file_id, "/info/scalars/galaxy_nmean", &galaxy_nmean);
    write_attribute_int(file_id, "/info/scalars/N_BIAS", &N_BIAS);
    write_attribute_double_array(file_id, "/info/scalars/galaxy_bias", N_BIAS, galaxy_bias);
} //write_mock_metadata_hdf5
///-------------------------------------------------------------------------------------
/** @fn write_mock_hdf5
 * Routine to write HDF5 mock files
 * @param M the mock in standard structure
 * @param fname output filename
 */
void write_mock_hdf5(Mock M, const char *fname)
{
    hid_t file_id;
    int rank=1;
    int N0=M.N0;
    int N1=M.N1;
    int N2=M.N2;
    double corner0=M.corner0, corner1=M.corner1, corner2=M.corner2;
    double L0=M.L0, L1=M.L1, L2=M.L2;
    double *galaxy_bias=M.galaxy_bias; int N_BIAS=M.N_BIAS; double galaxy_nmean=M.galaxy_nmean;

    /* Create a new file using default properties. */
    file_id = H5Fcreate(fname, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

        /* Create a groups in the file. */
        add_group(file_id, "/info");
        add_group(file_id, "/info/scalars");
        add_group(file_id, "/scalars");

        /* Write basefield metadata */
        write_basefieldmeta_hdf5(file_id, L0, L1, L2, corner0, corner1, corner2, rank, N0, N1, N2);

        /* Write mock metadata */
        write_mock_metadata_hdf5(file_id, galaxy_bias, N_BIAS, galaxy_nmean);

        /* Write data */
        write_field_data(file_id, "/scalars/field", M.data, rank, N0, N1, N2);

    /* Close the file */
    H5Fclose(file_id);
} //write_mock_hdf5
///-------------------------------------------------------------------------------------
/** @fn write_mock
 * Wrapper for mock writing routine
 * @param M the mock in standard structure
 * @param fname output filename
 */
void write_mock(Mock M, const char *fname)
{
    sprintf(G__msg__, "Writing mock to '%s'...", fname);
    PrintMessage(3, G__msg__);

    write_mock_hdf5(M, fname);

    sprintf(G__msg__, "Writing mock to '%s' done.", fname);
    PrintMessage(3, G__msg__);
} //write_mock
///-------------------------------------------------------------------------------------
/** @fn free_survey_geometry_header_
 * Frees the memory allocated for a survey geometry header structure
 * @param SG survey geometry header in standard format
 */
void free_survey_geometry_header_(SurveyGeometry SG, char *name, char *file, char *func, int line)
{
    char vname[50];
    sprintf(vname, "%s.bright_cut", name);
    p_free_(SG.bright_cut, vname, file, func, line);
    sprintf(vname, "%s.faint_cut", name);
    p_free_(SG.faint_cut, vname, file, func, line);
    sprintf(vname, "%s.rmin", name);
    p_free_(SG.rmin, vname, file, func, line);
    sprintf(vname, "%s.rmax", name);
    p_free_(SG.rmax, vname, file, func, line);
    sprintf(vname, "%s.zmin", name);
    p_free_(SG.zmin, vname, file, func, line);
    sprintf(vname, "%s.zmax", name);
    p_free_(SG.zmax, vname, file, func, line);
    sprintf(vname, "%s.galaxy_bias_mean", name);
    p_free_(SG.galaxy_bias_mean, vname, file, func, line);
    sprintf(vname, "%s.galaxy_bias_std", name);
    p_free_(SG.galaxy_bias_std, vname, file, func, line);
    sprintf(vname, "%s.galaxy_nmean_mean", name);
    p_free_(SG.galaxy_nmean_mean, vname, file, func, line);
    sprintf(vname, "%s.galaxy_nmean_std", name);
    p_free_(SG.galaxy_nmean_std, vname, file, func, line);
} //free_survey_geometry_header_
///-------------------------------------------------------------------------------------
/** @fn read_survey_geometry_header_
 * Routine to read the attributes of an HDF5 survey geometry file
 * @param fname input filename
 * @param *name output variable name
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 * @return the survey geometry header in standard format
 */
SurveyGeometry read_survey_geometry_header_(const char *fname, char *name, char *file, char *func, int line)
{
    hid_t file_id;
    SurveyGeometry SG;
    char vname[50];

    /* Check that the file exists */
    if(!exists(fname))
    {
        sprintf(G__msg__, "Failed to open file '%s'.", fname);
        FatalError(G__msg__);
    }

    /* Open the file. */
    file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);

        /* Read metadata */
        read_basefieldmeta_hdf5(file_id, &SG.L0, &SG.L1, &SG.L2, &SG.corner0, &SG.corner1, &SG.corner2, &SG.N0, &SG.N1, &SG.N2, &SG.rank);

        /* Read attributes */
        read_attribute_double(file_id, "/info/scalars/cosmo", SG.cosmo);
        read_attribute_int(file_id, "/info/scalars/N_CAT", &SG.N_CAT);

        /* Allocate memory */
        sprintf(vname, "%s.bright_cut", name);
        SG.bright_cut = (double*)p_malloc_(SG.N_CAT*sizeof(double), vname, file, func, line);
        sprintf(vname, "%s.faint_cut", name);
        SG.faint_cut = (double*)p_malloc_(SG.N_CAT*sizeof(double), vname, file, func, line);
        sprintf(vname, "%s.rmin", name);
        SG.rmin = (double*)p_malloc_(SG.N_CAT*sizeof(double), vname, file, func, line);
        sprintf(vname, "%s.rmax", name);
        SG.rmax = (double*)p_malloc_(SG.N_CAT*sizeof(double), vname, file, func, line);
        sprintf(vname, "%s.zmin", name);
        SG.zmin = (double*)p_malloc_(SG.N_CAT*sizeof(double), vname, file, func, line);
        sprintf(vname, "%s.zmax", name);
        SG.zmax = (double*)p_malloc_(SG.N_CAT*sizeof(double), vname, file, func, line);

        /* Read attributes */
        read_attribute_double(file_id, "/info/scalars/bright_cut", SG.bright_cut);
        read_attribute_double(file_id, "/info/scalars/faint_cut", SG.faint_cut);
        read_attribute_double(file_id, "/info/scalars/rmin", SG.rmin);
        read_attribute_double(file_id, "/info/scalars/rmax", SG.rmax);
        read_attribute_double(file_id, "/info/scalars/zmin", SG.zmin);
        read_attribute_double(file_id, "/info/scalars/zmax", SG.zmax);
        read_attribute_int(file_id, "/info/scalars/N_BIAS", &SG.N_BIAS);

        /* Allocate memory */
        sprintf(vname, "%s.galaxy_bias_mean", name);
        SG.galaxy_bias_mean = (double*)p_malloc_(SG.N_CAT*SG.N_BIAS*sizeof(double), vname, file, func, line);
        sprintf(vname, "%s.galaxy_bias_std", name);
        SG.galaxy_bias_std = (double*)p_malloc_(SG.N_CAT*SG.N_BIAS*sizeof(double), vname, file, func, line);
        sprintf(vname, "%s.galaxy_nmean_mean", name);
        SG.galaxy_nmean_mean = (double*)p_malloc_(SG.N_CAT*sizeof(double), vname, file, func, line);
        sprintf(vname, "%s.galaxy_nmean_std", name);
        SG.galaxy_nmean_std = (double*)p_malloc_(SG.N_CAT*sizeof(double), vname, file, func, line);

        /* Read attributes */
        read_attribute_double(file_id, "/info/scalars/galaxy_bias_mean", SG.galaxy_bias_mean);
        read_attribute_double(file_id, "/info/scalars/galaxy_bias_std", SG.galaxy_bias_std);
        read_attribute_double(file_id, "/info/scalars/galaxy_nmean_mean", SG.galaxy_nmean_mean);
        read_attribute_double(file_id, "/info/scalars/galaxy_nmean_std", SG.galaxy_nmean_std);

    /* Close the file */
    H5Fclose(file_id);

    return SG;
} //read_survey_geometry_header_
///-------------------------------------------------------------------------------------
/** @fn free_galaxy_selection_window_
 * Frees the memory allocated for a galaxy selection window structure
 * @param GSW galaxy selection window_
 * @param *name input variable name
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
void free_galaxy_selection_window_(GalaxySelectionWindow GSW, char *name, char *file, char *func, int line)
{
    char vname[50];
    sprintf(vname, "%s.galaxy_bias_mean", name);
    p_free_(GSW.galaxy_bias_mean, vname, file, func, line);
    sprintf(vname, "%s.galaxy_bias_std", name);
    p_free_(GSW.galaxy_bias_std, vname, file, func, line);
    sprintf(vname, "%s.galaxy_sel_window", name);
    p_fftw_free_(GSW.galaxy_sel_window, vname, file, func, line);
} //free_galaxy_selection_window_
///-------------------------------------------------------------------------------------
/** @fn read_galaxy_selection_window_
 * Routine to read a galaxy selection window in HDF5 survey geometry files
 * @param fname input filename
 * @param ICAT id of the subcatalog
 * @param *name output variable name
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 * @return the galaxy selection window in standard format
 */
GalaxySelectionWindow read_galaxy_selection_window_(const char *fname, const int ICAT, char *name, char *file, char *func, int line)
{
    hid_t file_id;
    GalaxySelectionWindow GSW;
    char vname[50];

    /* Check that the file exists */
    if(!exists(fname))
    {
        sprintf(G__msg__, "Failed to open file '%s'.", fname);
        FatalError(G__msg__);
    }

    /* Open the file. */
    file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);

        /* Read basefield metadata */
        read_basefieldmeta_hdf5(file_id, &GSW.L0, &GSW.L1, &GSW.L2, &GSW.corner0, &GSW.corner1, &GSW.corner2, &GSW.N0, &GSW.N1, &GSW.N2, &GSW.rank);

        /* Read attributes */
        read_attribute_double(file_id, "/info/scalars/cosmo", GSW.cosmo);
        read_attribute_double_in_array(file_id, "/info/scalars/bright_cut", &GSW.bright_cut, ICAT);
        read_attribute_double_in_array(file_id, "/info/scalars/faint_cut", &GSW.faint_cut, ICAT);
        read_attribute_double_in_array(file_id, "/info/scalars/rmin", &GSW.rmin, ICAT);
        read_attribute_double_in_array(file_id, "/info/scalars/rmax", &GSW.rmax, ICAT);
        read_attribute_double_in_array(file_id, "/info/scalars/zmin", &GSW.zmin, ICAT);
        read_attribute_double_in_array(file_id, "/info/scalars/zmax", &GSW.zmax, ICAT);
        read_attribute_int(file_id, "/info/scalars/N_BIAS", &GSW.N_BIAS);
        read_attribute_double_in_array(file_id, "/info/scalars/galaxy_nmean_mean", &GSW.galaxy_nmean_mean, ICAT);
        read_attribute_double_in_array(file_id, "/info/scalars/galaxy_nmean_std", &GSW.galaxy_nmean_std, ICAT);

        /* Allocate memory */
        sprintf(vname, "%s.galaxy_bias_mean", name);
        GSW.galaxy_bias_mean = (double*)p_malloc_(GSW.N_BIAS*sizeof(double), vname, file, func, line);
        sprintf(vname, "%s.galaxy_bias_std", name);
        GSW.galaxy_bias_std = (double*)p_malloc_(GSW.N_BIAS*sizeof(double), vname, file, func, line);

        /* Read attributes */
        read_attribute_double_array_in_array(file_id, "/info/scalars/galaxy_bias_mean", GSW.N_BIAS, GSW.galaxy_bias_mean, ICAT);
        read_attribute_double_array_in_array(file_id, "/info/scalars/galaxy_bias_std", GSW.N_BIAS, GSW.galaxy_bias_std, ICAT);

        /* Allocate memory */
        sprintf(vname, "%s.galaxy_sel_window", name);
        GSW.galaxy_sel_window = (float_t*)p_fftw_malloc_(GSW.rank*GSW.N0*GSW.N1*GSW.N2*sizeof(float_t), vname, file, func, line);

        /* Read the dataset "/scalars/galaxy_sel_window_ICAT". */
        char field_address[50]; sprintf(field_address, "/scalars/galaxy_sel_window_%d", ICAT);
        read_field_data(file_id, field_address, GSW.galaxy_sel_window, GSW.rank, GSW.N0, GSW.N1, GSW.N2);

    /* Close the file */
    H5Fclose(file_id);

    return GSW;
} //read_galaxy_selection_window_
///-------------------------------------------------------------------------------------
/** @fn free_FourierGrid_
 * This routine frees the memory allocated for a FourierGrid
 * @param Gk the Fourier grid
 * @param *name input variable name
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
void free_FourierGrid_(FourierGrid Gk, char *name, char *file, char *func, int line)
{
    char vname[50];
    sprintf(vname, "%s.k_modes", name);
    p_free_(Gk.k_modes, vname, file, func, line);
    sprintf(vname, "%s.k_nmodes", name);
    p_free_(Gk.k_nmodes, vname, file, func, line);
    sprintf(vname, "%s.k_keys", name);
    p_free_(Gk.k_keys, vname, file, func, line);
} //free_FourierGrid_
///-------------------------------------------------------------------------------------
/** @fn free_PowerSpectrum_
 * This routine frees the memory allocated for a PowerSpectrum
 * @param Pk PowerSpectrum
 * @param *name input variable name
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
void free_PowerSpectrum_(PowerSpectrum Pk, char *name, char *file, char *func, int line)
{
    char vname[50];
    sprintf(vname, "%s.k_modes", name);
    p_free_(Pk.k_modes, vname, file, func, line);
    sprintf(vname, "%s.k_nmodes", name);
    p_free_(Pk.k_nmodes, vname, file, func, line);
    sprintf(vname, "%s.k_keys", name);
    p_free_(Pk.k_keys, vname, file, func, line);
    sprintf(vname, "%s.powerspectrum", name);
    p_free_(Pk.powerspectrum, vname, file, func, line);
} //free_PowerSpectrum_
///-------------------------------------------------------------------------------------
/** @fn write_kgridmeta_hdf5
 * Subroutine to write the data of a kgrid or power spectrum to hdf5 file
 */
static void write_kgridmeta_hdf5(hid_t file_id, KGRIDMETA_ARGS)
{
    write_attribute_double(file_id, "/info/scalars/L0", &L0);
    write_attribute_double(file_id, "/info/scalars/L1", &L1);
    write_attribute_double(file_id, "/info/scalars/L2", &L2);
    write_attribute_int(file_id, "/info/scalars/N0", &N0);
    write_attribute_int(file_id, "/info/scalars/N1", &N1);
    write_attribute_int(file_id, "/info/scalars/N2", &N2);
    write_attribute_int(file_id, "/info/scalars/N2_HC", &N2_HC);
    write_attribute_int(file_id, "/info/scalars/N_HC", &N_HC);
    write_attribute_int(file_id, "/info/scalars/NUM_MODES", &NUM_MODES);
    write_attribute_double(file_id, "/info/scalars/kmax", &kmax);
} //write_kgridmeta_hdf5
///-------------------------------------------------------------------------------------
/** @fn read_kgridmeta_hdf5
 * Subroutine to read the data of a kgrid or power spectrum to hdf5 file
 */
static void read_kgridmeta_hdf5(hid_t file_id, KGRIDMETA_PTR_ARGS)
{
    read_attribute_double(file_id, "/info/scalars/L0", L0);
    read_attribute_double(file_id, "/info/scalars/L1", L1);
    read_attribute_double(file_id, "/info/scalars/L2", L2);
    read_attribute_int(file_id, "/info/scalars/N0", N0);
    read_attribute_int(file_id, "/info/scalars/N1", N1);
    read_attribute_int(file_id, "/info/scalars/N2", N2);
    read_attribute_int(file_id, "/info/scalars/N2_HC", N2_HC);
    read_attribute_int(file_id, "/info/scalars/N_HC", N_HC);
    read_attribute_int(file_id, "/info/scalars/NUM_MODES", NUM_MODES);
    read_attribute_double(file_id, "/info/scalars/kmax", kmax);
} //read_kgridmeta_hdf5
///-------------------------------------------------------------------------------------
/** @fn write_kgrid_hdf5
 * Subroutine to write the data of a kgrid or power spectrum to hdf5 file
 */
static void write_kgrid_hdf5(hid_t file_id, KGRID_ARGS)
{
    /* Write kgrid metadata */
    write_kgridmeta_hdf5(file_id, L0, L1, L2, N0, N1, N2, N2_HC, N_HC, NUM_MODES, kmax);

    /* Write k_modes, k_nmodes, k_keys */
    write_float_t_array(file_id, "/info/scalars/k_modes", NUM_MODES, k_modes);
    write_int_array(file_id, "/info/scalars/k_nmodes", NUM_MODES, k_nmodes);
    write_int_3d_array(file_id, "/info/scalars/k_keys", N0, N1, N2_HC, (int (*)[(int)(N1)][(int)(N2_HC)])k_keys);
} //write_kgrid_hdf5
///-------------------------------------------------------------------------------------
/** @fn read_kgrid_hdf5
 * Subroutine to read the data of a kgrid or power spectrum to hdf5 file
 */
static void read_kgrid_hdf5(hid_t file_id, float_t *k_modes, int* k_nmodes, int* k_keys, const int NUM_MODES)
{
    read_float_t_array(file_id, "/info/scalars/k_modes", NUM_MODES, k_modes);
    read_int_array(file_id, "/info/scalars/k_nmodes", k_nmodes);
    read_int_array(file_id, "/info/scalars/k_keys", k_keys);
} //read_kgrid_hdf5
///-------------------------------------------------------------------------------------
/** @fn write_FourierGrid_hdf5
 * Routine to write HDF5 file containing a Fourier grid
 * @param Gk the Fourier grid in standard structure
 * @param fname input filename
 */
void write_FourierGrid_hdf5(FourierGrid Gk, const char *fname)
{
    hid_t file_id;

    /* Create a new file using default properties. */
    file_id = H5Fcreate(fname, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

        /* Create a groups in the file. */
        add_group(file_id, "/info");
        add_group(file_id, "/info/scalars");

        /* Write kgrid */
        write_kgrid_hdf5(file_id, Gk.L0, Gk.L1, Gk.L2, Gk.N0, Gk.N1, Gk.N2, Gk.N2_HC, Gk.N_HC, Gk.NUM_MODES, Gk.kmax, Gk.k_modes, Gk.k_nmodes, Gk.k_keys);

    /* Close the file */
    H5Fclose(file_id);
} //write_FourierGrid_hdf5
///-------------------------------------------------------------------------------------
/** @fn read_FourierGrid_hdf5_
 * Routine to read HDF5 file containing a Fourier grid
 * @param fname input filename
 * @param *name output variable name
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 * @return the Fourier grid in standard structure
 */
FourierGrid read_FourierGrid_hdf5_(const char *fname, char *name, char *file, char *func, int line)
{
    hid_t file_id;
    FourierGrid Gk;
    char vname[50];

    /* Check that the file exists */
    if(!exists(fname))
    {
        sprintf(G__msg__, "Failed to open file '%s'.", fname);
        FatalError(G__msg__);
    }

    /* Open the file. */
    file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);

        /* Read metadata */
        read_kgridmeta_hdf5(file_id, &Gk.L0, &Gk.L1, &Gk.L2, &Gk.N0, &Gk.N1, &Gk.N2, &Gk.N2_HC, &Gk.N_HC, &Gk.NUM_MODES, &Gk.kmax);

        /* Allocate memory */
        sprintf(vname, "%s.k_keys", name);
        Gk.k_keys = (int*)p_malloc_(Gk.N_HC*sizeof(int), vname, file, func, line);
        sprintf(vname, "%s.k_modes", name);
        Gk.k_modes = (float_t*)p_malloc_(Gk.NUM_MODES*sizeof(float_t), vname, file, func, line);
        sprintf(vname, "%s.k_nmodes", name);
        Gk.k_nmodes = (int*)p_malloc_(Gk.NUM_MODES*sizeof(int), vname, file, func, line);

        /* Read kgrid */
        read_kgrid_hdf5(file_id, Gk.k_modes, Gk.k_nmodes, Gk.k_keys, Gk.NUM_MODES);

    /* Close the file */
    H5Fclose(file_id);

    return Gk;
} //read_FourierGrid_hdf5_
///-------------------------------------------------------------------------------------
/** @fn write_FourierGrid
 * Wrapper for writing routine for FG
 * @param Gk the Fourier grid in standard structure
 * @param fname output filename
 */
void write_FourierGrid(FourierGrid Gk, const char *fname)
{
    sprintf(G__msg__, "Writing Fourier grid to '%s'...", fname);
    PrintMessage(3, G__msg__);

    write_FourierGrid_hdf5(Gk, fname);

    sprintf(G__msg__, "Writing Fourier grid to '%s' done.", fname);
    PrintMessage(3, G__msg__);
} //write_FourierGrid
///-------------------------------------------------------------------------------------
/** @fn read_FourierGrid_
 * Wrapper for reading routine for FG
 * @param fname input filename
 * @param *name output variable name
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 * @return the Fourier grid in standard structure
 */
FourierGrid read_FourierGrid_(const char *fname, char *name, char *file, char *func, int line)
{
    sprintf(G__msg__, "Reading Fourier grid in '%s'...", fname);
    PrintMessage(3, G__msg__);

    FourierGrid Gk;
    Gk = read_FourierGrid_hdf5_(fname, name, file, func, line);

    sprintf(G__msg__, "Reading Fourier grid in '%s' done.", fname);
    PrintMessage(3, G__msg__);

    return Gk;
} //read_FourierGrid_
///-------------------------------------------------------------------------------------
/** @fn write_PowerSpectrum_hdf5
 * Routine to write HDF5 file containing a power spectrum
 * @param Pk the power spectrum in standard structure
 * @param fname output filename
 */
static void write_PowerSpectrum_hdf5(PowerSpectrum Pk, const char *fname)
{
    hid_t file_id;

    /* Create a new file using default properties. */
    file_id = H5Fcreate(fname, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

        /* Create a groups in the file. */
        add_group(file_id, "/info");
        add_group(file_id, "/info/scalars");
        add_group(file_id, "/scalars");

        /* Write kgrid */
        write_kgrid_hdf5(file_id, Pk.L0, Pk.L1, Pk.L2, Pk.N0, Pk.N1, Pk.N2, Pk.N2_HC, Pk.N_HC, Pk.NUM_MODES, Pk.kmax, Pk.k_modes, Pk.k_nmodes, Pk.k_keys);

        /* Write powerspectrum */
        write_float_t_array(file_id, "/scalars/powerspectrum", Pk.NUM_MODES, Pk.powerspectrum);

    /* Close the file */
    H5Fclose(file_id);
} //write_PowerSpectrum_hdf5
///-------------------------------------------------------------------------------------
/** @fn read_PowerSpectrum_hdf5_
 * Routine to read HDF5 file containing a power spectrum
 * @param fname input filename
 * @param *name output variable name
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 * @return the power spectrum in standard structure
 */
static PowerSpectrum read_PowerSpectrum_hdf5_(const char *fname, char *name, char *file, char *func, int line)
{
    hid_t file_id;
    PowerSpectrum Pk;
    char vname[50];

    /* Check that the file exists */
    if(!exists(fname))
    {
        sprintf(G__msg__, "Failed to open file '%s'.", fname);
        FatalError(G__msg__);
    }

    /* Open the file. */
    file_id = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);

        /* Read metadata */
        read_kgridmeta_hdf5(file_id, &Pk.L0, &Pk.L1, &Pk.L2, &Pk.N0, &Pk.N1, &Pk.N2, &Pk.N2_HC, &Pk.N_HC, &Pk.NUM_MODES, &Pk.kmax);

        /* Allocate memory */
        sprintf(vname, "%s.k_keys", name);
        Pk.k_keys = (int*)p_malloc_(Pk.N_HC*sizeof(int), vname, file, func, line);
        sprintf(vname, "%s.k_modes", name);
        Pk.k_modes = (float_t*)p_malloc_(Pk.NUM_MODES*sizeof(float_t), vname, file, func, line);
        sprintf(vname, "%s.k_nmodes", name);
        Pk.k_nmodes = (int*)p_malloc_(Pk.NUM_MODES*sizeof(int), vname, file, func, line);
        sprintf(vname, "%s.powerspectrum", name);
        Pk.powerspectrum = (float_t*)p_malloc_(Pk.NUM_MODES*sizeof(float_t), vname, file, func, line);

        /* Read kgrid */
        read_kgrid_hdf5(file_id, Pk.k_modes, Pk.k_nmodes, Pk.k_keys, Pk.NUM_MODES);

        /* Read powerspectrum */
        read_float_t_array(file_id, "/scalars/powerspectrum", Pk.NUM_MODES, Pk.powerspectrum);

    /* Close the file */
    H5Fclose(file_id);

    return Pk;
} //read_PowerSpectrum_hdf5_
///-------------------------------------------------------------------------------------
/** @fn write_PowerSpectrum
 * Wrapper for writing routine for PowerSpectrum
 * @param Pk the power spectrum in standard structure
 * @param fname output filename
 */
void write_PowerSpectrum(PowerSpectrum Pk, const char *fname)
{
    sprintf(G__msg__, "Writing power spectrum to '%s'...", fname);
    PrintMessage(3, G__msg__);

    write_PowerSpectrum_hdf5(Pk, fname);

    sprintf(G__msg__, "Writing power spectrum to '%s' done.", fname);
    PrintMessage(3, G__msg__);
} //write_PowerSpectrum
///-------------------------------------------------------------------------------------
/** @fn read_PowerSpectrum_
 * Wrapper for reading routine for PowerSpectrum
 * @param fname input filename
 * @param *name output variable name
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 * @return the power spectrum in standard structure
 */
PowerSpectrum read_PowerSpectrum_(const char *fname, char *name, char *file, char *func, int line)
{
    sprintf(G__msg__, "Reading power spectrum in '%s'...", fname);
    PrintMessage(3, G__msg__);

    PowerSpectrum Pk;
    Pk = read_PowerSpectrum_hdf5_(fname, name, file, func, line);

    sprintf(G__msg__, "Reading power spectrum in '%s' done.", fname);
    PrintMessage(3, G__msg__);

    return Pk;
} //read_PowerSpectrum_
///-------------------------------------------------------------------------------------
/** @fn write_ss_hdf5
 * Routine to write HDF5 file containing a Fourier grid and some data
 * @param Gk the Fourier grid in standard structure
 * @param N_CAT number of subcatalogs
 * @param N_NOISE number of noise realization
 * @param N_BIAS number of bias parameters
 * @param *galaxy_bias galaxy bias parameters
 * @param *galaxy_nmean galaxy Nmean parameters
 * @param Pk power spectra means
 * @param Vk power spectra variances
 * @param fname output filename
 */
void write_ss_hdf5(FourierGrid Gk, int N_CAT, int N_NOISE, int N_BIAS, double galaxy_bias[N_CAT][N_NOISE][N_BIAS], double galaxy_nmean[N_CAT][N_NOISE], float_t Pk[N_CAT][N_NOISE][Gk.NUM_MODES], float_t Vk[N_CAT][N_NOISE][Gk.NUM_MODES], const char *fname)
{
    hid_t file_id;

    /* Create a new file using default properties. */
    file_id = H5Fcreate(fname, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

        /* Create a groups in the file. */
        add_group(file_id, "/info");
        add_group(file_id, "/info/scalars");
        add_group(file_id, "/scalars");

        /* Write attributes */
        write_attribute_int(file_id, "/info/scalars/N_CAT", &N_CAT);
        write_attribute_int(file_id, "/info/scalars/N_NOISE", &N_NOISE);
        write_attribute_int(file_id, "/info/scalars/N_BIAS", &N_BIAS);

        /* Write kgrid */
        write_kgrid_hdf5(file_id, Gk.L0, Gk.L1, Gk.L2, Gk.N0, Gk.N1, Gk.N2, Gk.N2_HC, Gk.N_HC, Gk.NUM_MODES, Gk.kmax, Gk.k_modes, Gk.k_nmodes, Gk.k_keys);

        /* Write galaxy_bias and galaxy_nmean */
        write_double_3d_array(file_id, "/info/scalars/galaxy_bias", N_CAT, N_NOISE, N_BIAS, galaxy_bias);
        write_double_2d_array(file_id, "/info/scalars/galaxy_nmean", N_CAT, N_NOISE, galaxy_nmean);

        /* Write data */
        write_float_t_3d_array(file_id, "/scalars/Pk", N_CAT, N_NOISE, Gk.NUM_MODES, (float_t ***)Pk);
        write_float_t_3d_array(file_id, "/scalars/Vk", N_CAT, N_NOISE, Gk.NUM_MODES, (float_t ***)Vk);

    /* Close the file */
    H5Fclose(file_id);
} //write_ss_hdf5
///-------------------------------------------------------------------------------------
/** @fn write_ss
 * Wrapper for writing routine for summary statistics
 * @param Gk the Fourier grid in standard structure
 * @param N_CAT number of subcatalogs
 * @param N_NOISE number of noise realization
 * @param N_BIAS number of bias parameters
 * @param *galaxy_bias galaxy bias parameters
 * @param *galaxy_nmean galaxy Nmean parameters
 * @param Pk power spectra means
 * @param Vk power spectra variances
 * @param fname output filename
 */
void write_ss(FourierGrid Gk, const int N_CAT, const int N_NOISE, const int N_BIAS, double galaxy_bias[N_CAT][N_NOISE][N_BIAS], double galaxy_nmean[N_CAT][N_NOISE], float_t Pk[N_CAT][N_NOISE][Gk.NUM_MODES], float_t Vk[N_CAT][N_NOISE][Gk.NUM_MODES], const char *fname)
{
    sprintf(G__msg__, "Writing summary statistics to '%s'...", fname);
    PrintMessage(3, G__msg__);

    write_ss_hdf5(Gk, N_CAT, N_NOISE, N_BIAS, galaxy_bias, galaxy_nmean, Pk, Vk, fname);

    sprintf(G__msg__, "Writing summary statistics to '%s' done.", fname);
    PrintMessage(3, G__msg__);
} //write_ss
///-------------------------------------------------------------------------------------
/** @fn free_snapshot_
 * This routine frees the memory allocated for a snapshot
 * @param P snapshot
 */
void free_snapshot_(Snapshot P, char *name, char *file, char *func, int line)
{
    char vname[BUFLENGTH];
    #ifdef HAVE_TYPE_AND_MASS
    sprintf(vname, "%s.types", name);
    p_free_(P.types, vname, file, func, line);
    sprintf(vname, "%s.masses", name);
    p_free_(P.masses, vname, file, func, line);
    #endif
    sprintf(vname, "%s.positions", name);
    p_free_(P.positions, vname, file, func, line);
    sprintf(vname, "%s.velocities", name);
    p_free_(P.velocities, vname, file, func, line);
    sprintf(vname, "%s.ids", name);
    p_free_(P.ids, vname, file, func, line);
    #ifdef HAVE_EXTRAS
    sprintf(vname, "%s.extras", name);
    p_free_(P.extras, vname, file, func, line);
    #endif
} //free_snapshot_
///-------------------------------------------------------------------------------------
/** @fn get_numfiles_snapshot
 * This routine gets the number of files into which a
 * snapshot is distributed
 * @param *fname pointer to the filename or file base name (complete path)
 * @return integer giving the number of files
 */
static int get_numfiles_snapshot(const char *fname)
{
    if(exists(fname))
        return 1;
    else
    {
        char buf[BUFLENGTH];
        int i=0;
        sprintf(buf, "%s.%d", fname, i);
        while(exists(buf))
        {
            i++;
            sprintf(buf, "%s.%d", fname, i);
        }
        return i;
    }
    return 0;
} //get_numfiles_snapshot
///-------------------------------------------------------------------------------------
/** @fn get_bytes_per_blockelement
 *  This function tells the size of one data entry in each of the blocks
 *  defined for the output file. If one wants to add a new output-block, this
 *  function should be augmented accordingly.
 * @param blocknr
 */
static int get_bytes_per_blockelement(enum iofields blocknr)
{
    int bytes_per_blockelement = 0;
    switch(blocknr)
    {
        case IO_POS:
        case IO_VEL:
        case IO_ACCEL:
            bytes_per_blockelement = 3 * sizeof(float);
            break;
        case IO_ID:
            bytes_per_blockelement = sizeof(particleID_t);
            break;
        case IO_MASS:
        case IO_U:
        case IO_RHO:
        case IO_HSML:
        case IO_POT:
        case IO_DTENTR:
        case IO_TSTP:
            bytes_per_blockelement = sizeof(float);
            break;
    }
    return bytes_per_blockelement;
} //get_bytes_per_blockelement
///-------------------------------------------------------------------------------------
/** @fn get_values_per_blockelement
 * This function informs about the number of elements stored per particle for
 *  the given block of the output file. If one wants to add a new
 *  output-block, this function should be augmented accordingly.
 * @param blocknr
 */
static int get_values_per_blockelement(enum iofields blocknr)
{
    int values = 0;
    switch(blocknr)
    {
        case IO_POS:
        case IO_VEL:
        case IO_ACCEL:
            values = 3;
            break;
        case IO_ID:
        case IO_MASS:
        case IO_U:
        case IO_RHO:
        case IO_HSML:
        case IO_POT:
        case IO_DTENTR:
        case IO_TSTP:
            values = 1;
            break;
    }
    return values;
} //get_values_per_blockelement
///-------------------------------------------------------------------------------------
/** @fn fill_Tab_IO_Labels
 *  This function associates a short 4-character block name with each block
 *  number.  This is stored in front of each block for snapshot
 *  FileFormat=2. If one wants to add a new output-block, this function should
 *  be augmented accordingly.
 */
static void fill_Tab_IO_Labels(void)
{
    for(enum iofields blocknr = 0; blocknr < IO_NBLOCKS; blocknr++)
        switch(blocknr)
        {
            case IO_POS:
                memcpy(Tab_IO_Labels[IO_POS], "POS ", 4);
                break;
            case IO_VEL:
                memcpy(Tab_IO_Labels[IO_VEL], "VEL ", 4);
                break;
            case IO_ID:
                memcpy(Tab_IO_Labels[IO_ID], "ID  ", 4);
                break;
            case IO_MASS:
                memcpy(Tab_IO_Labels[IO_MASS], "MASS", 4);
                break;
            case IO_U:
                memcpy(Tab_IO_Labels[IO_U], "U   ", 4);
                break;
            case IO_RHO:
                memcpy(Tab_IO_Labels[IO_RHO], "RHO ", 4);
                break;
            case IO_HSML:
                memcpy(Tab_IO_Labels[IO_HSML], "HSML", 4);
                break;
            case IO_POT:
                memcpy(Tab_IO_Labels[IO_POT], "POT ", 4);
                break;
            case IO_ACCEL:
                memcpy(Tab_IO_Labels[IO_ACCEL], "ACCE", 4);
                break;
            case IO_DTENTR:
                memcpy(Tab_IO_Labels[IO_DTENTR], "ENDT", 4);
                break;
            case IO_TSTP:
                memcpy(Tab_IO_Labels[IO_TSTP], "TSTP", 4);
                break;
        }
} //fill_Tab_IO_Labels
///-------------------------------------------------------------------------------------
/** @fn get_dataset_name
 *  This function returns a descriptive character string that describes the
 *  name of the block when the HDF5 file format is used.  If one wants to add
 *  a new output-block, this function should be augmented accordingly.
 * @param blocknr
 * @param buf
 */
static void get_dataset_name(enum iofields blocknr, char *buf)
{
    strcpy(buf, "default");

    switch(blocknr)
    {
        case IO_POS:
            strcpy(buf, "Coordinates");
            break;
        case IO_VEL:
            strcpy(buf, "Velocities");
            break;
        case IO_ID:
            strcpy(buf, "ParticleIDs");
            break;
        case IO_MASS:
            strcpy(buf, "Masses");
            break;
        case IO_U:
            strcpy(buf, "InternalEnergy");
            break;
        case IO_RHO:
            strcpy(buf, "Density");
            break;
        case IO_HSML:
            strcpy(buf, "SmoothingLength");
            break;
        case IO_POT:
            strcpy(buf, "Potential");
            break;
        case IO_ACCEL:
            strcpy(buf, "Acceleration");
            break;
        case IO_DTENTR:
            strcpy(buf, "RateOfChangeOfEntropy");
            break;
        case IO_TSTP:
            strcpy(buf, "TimeStep");
            break;
    }
} //get_dataset_name
///-------------------------------------------------------------------------------------
/** @fn read_header_attributes_in_hdf5
 *  This function reads the header information in case the HDF5 file format is
 *  used.
 * @param hdf5_file
 * @param header
 */
static void read_header_attributes_in_hdf5(hid_t hdf5_file, header_t *header)
{
    hid_t hdf5_headergrp, hdf5_attribute;

    hdf5_headergrp = H5Gopen(hdf5_file, "/Header", H5P_DEFAULT);

    hdf5_attribute = H5Aopen_name(hdf5_headergrp, "NumPart_ThisFile");
    H5Aread(hdf5_attribute, H5T_NATIVE_UINT, header->npart);
    H5Aclose(hdf5_attribute);

    hdf5_attribute = H5Aopen_name(hdf5_headergrp, "MassTable");
    H5Aread(hdf5_attribute, H5T_NATIVE_DOUBLE, header->mass);
    H5Aclose(hdf5_attribute);

    hdf5_attribute = H5Aopen_name(hdf5_headergrp, "Time");
    H5Aread(hdf5_attribute, H5T_NATIVE_DOUBLE, &header->time);
    H5Aclose(hdf5_attribute);

    hdf5_attribute = H5Aopen_name(hdf5_headergrp, "Redshift");
    H5Aread(hdf5_attribute, H5T_NATIVE_DOUBLE, &header->redshift);
    H5Aclose(hdf5_attribute);

    hdf5_attribute = H5Aopen_name(hdf5_headergrp, "Flag_Sfr");
    H5Aread(hdf5_attribute, H5T_NATIVE_INT, &header->flag_sfr);
    H5Aclose(hdf5_attribute);

    hdf5_attribute = H5Aopen_name(hdf5_headergrp, "Flag_Feedback");
    H5Aread(hdf5_attribute, H5T_NATIVE_INT, &header->flag_feedback);
    H5Aclose(hdf5_attribute);

    hdf5_attribute = H5Aopen_name(hdf5_headergrp, "NumPart_Total");
    H5Aread(hdf5_attribute, H5T_NATIVE_UINT, header->npartTotal);
    H5Aclose(hdf5_attribute);

    hdf5_attribute = H5Aopen_name(hdf5_headergrp, "Flag_Cooling");
    H5Aread(hdf5_attribute, H5T_NATIVE_INT, &header->flag_cooling);
    H5Aclose(hdf5_attribute);

    hdf5_attribute = H5Aopen_name(hdf5_headergrp, "NumFilesPerSnapshot");
    H5Aread(hdf5_attribute, H5T_NATIVE_INT, &header->num_files);
    H5Aclose(hdf5_attribute);

    hdf5_attribute = H5Aopen_name(hdf5_headergrp, "BoxSize");
    H5Aread(hdf5_attribute, H5T_NATIVE_DOUBLE, &header->BoxSize);
    H5Aclose(hdf5_attribute);

    hdf5_attribute = H5Aopen_name(hdf5_headergrp, "Omega0");
    H5Aread(hdf5_attribute, H5T_NATIVE_DOUBLE, &header->Omega0);
    H5Aclose(hdf5_attribute);

    hdf5_attribute = H5Aopen_name(hdf5_headergrp, "OmegaLambda");
    H5Aread(hdf5_attribute, H5T_NATIVE_DOUBLE, &header->OmegaLambda);
    H5Aclose(hdf5_attribute);

    hdf5_attribute = H5Aopen_name(hdf5_headergrp, "HubbleParam");
    H5Aread(hdf5_attribute, H5T_NATIVE_DOUBLE, &header->HubbleParam);
    H5Aclose(hdf5_attribute);

    hdf5_attribute = H5Aopen_name(hdf5_headergrp, "Flag_StellarAge");
    H5Aread(hdf5_attribute, H5T_NATIVE_INT, &header->flag_stellarage);
    H5Aclose(hdf5_attribute);

    hdf5_attribute = H5Aopen_name(hdf5_headergrp, "Flag_Metals");
    H5Aread(hdf5_attribute, H5T_NATIVE_INT, &header->flag_metals);
    H5Aclose(hdf5_attribute);

    hdf5_attribute = H5Aopen_name(hdf5_headergrp, "NumPart_Total_HighWord");
    H5Aread(hdf5_attribute, H5T_NATIVE_UINT, header->npartTotalHighWord);
    H5Aclose(hdf5_attribute);

    hdf5_attribute = H5Aopen_name(hdf5_headergrp, "Flag_Entropy_ICs");
    H5Aread(hdf5_attribute, H5T_NATIVE_INT, &header->flag_entropy_instead_u);
    H5Aclose(hdf5_attribute);

    H5Gclose(hdf5_headergrp);
} //read_header_attributes_in_hdf5
///-------------------------------------------------------------------------------------
/** @fn read_hdf5_block
 *  This function reads a block in a Gadget fileformat 3 snapshot file
 * @param blocknr
 * @param hdf5_grp
 * @param *P
 * @param header
 * @param pc
 */
static void read_hdf5_block(enum iofields blocknr, hid_t hdf5_grp[6], Snapshot *P, header_t header, particleID_t pc)
{
    int rank, type, bytes_per_blockelement; particleID_t pc_new; unsigned int n;
    char h5buf[BUFLENGTH];
    hid_t hdf5_dataspace_in_file, hdf5_dataspace_in_memory, hdf5_dataset;
    hsize_t dims[2], offset[2];

    for(type = 0, pc_new = pc; type < 6; type++)
    {
        if(header.npart[type] <= 0)
            continue;
        get_dataset_name(blocknr, h5buf);
        bytes_per_blockelement = get_bytes_per_blockelement(blocknr);
        hdf5_dataset = H5Dopen2(hdf5_grp[type], h5buf, H5P_DEFAULT);

        dims[0] = (hsize_t)header.npart[type];        // read all particles in file
        dims[1] = (hsize_t)get_values_per_blockelement(blocknr);
        if(dims[1] == 1)
            rank = 1;
        else
            rank = 2;
        hdf5_dataspace_in_file = H5Screate_simple(rank, dims, NULL);
        offset[1] = 0;

        n = 0;
        while(n < header.npart[type])
        {
            INDENT;

            // check available memory from system information
            struct sysinfo info;
            if(sysinfo(&info) != 0)
            {
                char buf[BUFLENGTH];
                FormatFunction(buf);
                sprintf(G__msg__, "Unable to access system information in %s%s.", buf, FONT_LIGHTRED);
                FatalError(G__msg__);
            }
            unsigned long freeram = info.freeram * info.mem_unit;
            unsigned long bufferram = info.bufferram * info.mem_unit;
            unsigned long totalavailram = (freeram + bufferram - 10*8*1024*1024); // ensure that at least 10MB will remain

            unsigned int npart_chunk = (unsigned int)floor(totalavailram/bytes_per_blockelement);
            npart_chunk = min(min(npart_chunk,header.npart[type]-n),512*512*512); // maximum 512**3 particles per chunk for safety
            sprintf(G__msg__, "Reading chunk of size %d, current particleID: n=%d.", npart_chunk, n);
            debugPrintDiagnostic(G__msg__);

            dims[0] = (hsize_t)npart_chunk;
            offset[0] = (hsize_t)n;
            hdf5_dataspace_in_memory = H5Screate_simple(rank, dims, NULL);
            H5Sselect_hyperslab(hdf5_dataspace_in_file, H5S_SELECT_SET, offset, NULL, dims, NULL);

            // malloc an array and read in hdf5 dataset
            float *FloatBuffer;
            particleID_t *ParticleIDtypeBuffer;
            switch(blocknr)
            {
                case IO_ID:
                    #ifdef LONGIDS
                        ParticleIDtypeBuffer = p_malloc(npart_chunk*bytes_per_blockelement, VARNAME(ParticleIDtypeBuffer));
                        H5Dread(hdf5_dataset, H5T_NATIVE_UINT64, hdf5_dataspace_in_memory, hdf5_dataspace_in_file, H5P_DEFAULT, ParticleIDtypeBuffer);
                    #else
                        ParticleIDtypeBuffer = p_malloc(npart_chunk*bytes_per_blockelement, VARNAME(ParticleIDtypeBuffer));
                        H5Dread(hdf5_dataset, H5T_NATIVE_UINT, hdf5_dataspace_in_memory, hdf5_dataspace_in_file, H5P_DEFAULT, ParticleIDtypeBuffer);
                    #endif
                    break;
                default:
                    FloatBuffer = p_malloc(npart_chunk*bytes_per_blockelement, VARNAME(FloatBuffer));
                    H5Dread(hdf5_dataset, H5T_NATIVE_FLOAT, hdf5_dataspace_in_memory, hdf5_dataspace_in_file, H5P_DEFAULT, FloatBuffer);
                    break;
            }

            // fill snapshot structure
            switch(blocknr)
            {
                case IO_POS:
                    #pragma omp parallel for schedule(static)
                    for(particleID_t pc=pc_new; pc < pc_new+npart_chunk; pc++)
                    {
                        particleID_t np=pc-pc_new;
                        // cast to double
                        P->positions[pc].Pos[0] = (double)FloatBuffer[3*np+0];
                        P->positions[pc].Pos[1] = (double)FloatBuffer[3*np+1];
                        P->positions[pc].Pos[2] = (double)FloatBuffer[3*np+2];
                    }
                    break;
                case IO_VEL:
                    #pragma omp parallel for schedule(static)
                    for(particleID_t pc=pc_new; pc < pc_new+npart_chunk; pc++)
                    {
                        particleID_t np=pc-pc_new;
                        // cast to double
                        P->velocities[pc].Vel[0] = (double)FloatBuffer[3*np+0];
                        P->velocities[pc].Vel[1] = (double)FloatBuffer[3*np+1];
                        P->velocities[pc].Vel[2] = (double)FloatBuffer[3*np+2];
                    }
                    break;
                case IO_ID:
                    #pragma omp parallel for schedule(static)
                    for(particleID_t pc=pc_new; pc < pc_new+npart_chunk; pc++)
                    {
                        particleID_t np=pc-pc_new;
                        P->ids[pc].Id = (particleID_t)ParticleIDtypeBuffer[np];
                    }
                    break;
                case IO_MASS:
                    #if HAVE_TYPE_AND_MASS
                    #pragma omp parallel for schedule(static)
                    for(particleID_t pc=pc_new; pc < pc_new+npart_chunk; pc++)
                    {
                        particleID_t np=pc-pc_new;
                        P->types[pc].Type = type;
                        if(header.mass[type] == 0)
                            // cast to double
                            P->masses[pc].Mass = (double)FloatBuffer[np];
                        else
                            P->masses[pc].Mass = header.mass[type];
                    }
                    #endif
                    break;
                case IO_U:
                    #if HAVE_EXTRAS
                    #pragma omp parallel for schedule(static)
                    for(particleID_t pc=pc_new; pc < pc_new+npart_chunk; pc++)
                    {
                        particleID_t np=pc-pc_new;
                        // cast to double
                        P->extras[pc].U = (double)FloatBuffer[np];
                    }
                    #endif
                    break;
                case IO_RHO:
                    #if HAVE_EXTRAS
                    #pragma omp parallel for schedule(static)
                    for(particleID_t pc=pc_new; pc < pc_new+npart_chunk; pc++)
                    {
                        particleID_t np=pc-pc_new;
                        // cast to double
                        P->extras[pc].Rho = (double)FloatBuffer[np];
                    }
                    #endif
                    break;
                case IO_HSML:
                    #if HAVE_EXTRAS
                    #pragma omp parallel for schedule(static)
                    for(particleID_t pc=pc_new; pc < pc_new+npart_chunk; pc++)
                    {
                        particleID_t np=pc-pc_new;
                        // cast to double
                        P->extras[pc].Hsml = (double)FloatBuffer[np];
                    }
                    #endif
                    break;
                case IO_POT:
                case IO_ACCEL:
                case IO_DTENTR:
                case IO_TSTP:
                    break;
            }
            pc_new+=npart_chunk;

            // free memory
            switch(blocknr)
            {
                case IO_ID:
                    p_free(ParticleIDtypeBuffer, VARNAME(ParticleIDtypeBuffer));
                    break;
                default:
                    p_free(FloatBuffer, VARNAME(FloatBuffer));
                    break;
            }

            // update current particleID
            n += npart_chunk;

            UNINDENT;
        } //end while

        H5Sclose(hdf5_dataspace_in_memory);
        H5Sclose(hdf5_dataspace_in_file);
        H5Dclose(hdf5_dataset);
    }
} //read_hdf5_block
///-------------------------------------------------------------------------------------
/** @fn read_header
 * This routine loads the header from Gadget's
 * binary file formats.
 * @param *fname pointer to the filename (complete path)
 * @param SnapFormat snapshot format
 * @return the header in default format
 */
header_t read_header(const char *fname, const int SnapFormat)
{
    FILE *fd;
    int blksize;
    char blocklabel[5]={"    "};
    int nextblock;
    header_t header;
    hid_t hdf5_file;

    #define READBLOCKSIZE p_fread(&blksize, sizeof(blksize), 1, fd);

    // open file
    if(!(fd = fopen(fname, "r")))
    {
        sprintf(G__msg__, "Failed to open snapshot for reading in file '%s'.", fname);
        FatalError(G__msg__);
    }

    sprintf(G__msg__, "Reading header in '%s'...", fname);
    PrintMessage(3, G__msg__);
    INDENT;

    // read file: header
    PrintMessage(4, "Reading block: 'HEAD'...");
    if(SnapFormat == 2)
    {
        READBLOCKSIZE;
        p_assert(blksize==blkff2size);
        p_fread(blocklabel, sizeof(char), 4, fd);
        p_assert_(VARNAME(blocklabel=="HEAD"), strncmp(blocklabel,"HEAD",4)==0);
        p_fread(&nextblock, sizeof(int), 1, fd);
        p_assert(nextblock==sizeof(header_t) + 2 * sizeof(int));
        READBLOCKSIZE;
        p_assert(blksize==blkff2size);
    }
    if(SnapFormat == 1 || SnapFormat == 2)
    {
        READBLOCKSIZE;
        p_assert(blksize==sizeof(header_t));
        p_fread(&header, sizeof(header), 1, fd);
        READBLOCKSIZE;
        p_assert(blksize==sizeof(header_t));
    }
    else if(SnapFormat == 3)
    {
        hdf5_file = H5Fopen(fname, H5F_ACC_RDONLY, H5P_DEFAULT);
        read_header_attributes_in_hdf5(hdf5_file, &header);
    }
    PrintMessage(4, "Reading block: 'HEAD' done.");

    UNINDENT;
    sprintf(G__msg__, "Reading header in '%s' done.", fname);
    PrintMessage(3, G__msg__);

    return header;
} //read_header
///-------------------------------------------------------------------------------------
/** @fn read_snapshot
 * This routine loads particle data from Gadget's default
 * binary file formats. (A snapshot may be distributed
 * into multiple files).
 * @param *fname pointer to the filename (complete path)
 * @param SnapFormat snapshot format
 * @return the snapshot in default format
 */
Snapshot read_snapshot(const char *fname, const int SnapFormat)
{
    FILE *fd;
    char buf[BUFLENGTH], h5buf[BUFLENGTH];
    int file, type;
    unsigned int n, npart, ntot_withmasses;
    char blocklabel[5]={"    "};
    Snapshot P;
    header_t header1;
    particleID_t pc, pc_new;
    hid_t hdf5_file, hdf5_grp[6];

    fill_Tab_IO_Labels();

    const int files = get_numfiles_snapshot(fname);
    if(files==0)
    {
        sprintf(G__msg__, "Failed to find file(s) '%s'.", fname);
        FatalError(G__msg__);
    }

    int blksize, nextblock, bytes_per_blockelement;
    #define READBLOCKSIZE p_fread(&blksize, sizeof(blksize), 1, fd);

    for(file = 0, pc = 0; file < files; file++, pc = pc_new)
    {
        // get file name
        if(files > 1)
            sprintf(buf, "%s.%d", fname, file);
        else
            sprintf(buf, "%s", fname);

        // open file
        if(!(fd = fopen(buf, "r")))
        {
            sprintf(G__msg__, "Failed to open snapshot for reading in file '%s'.", buf);
            FatalError(G__msg__);
        }

        sprintf(G__msg__, "Reading header in '%s'...", buf);
        PrintMessage(3, G__msg__);
        INDENT;

        // read file: header
        PrintMessage(4, "Reading block: 'HEAD'...");
        if(SnapFormat == 2)
        {
            READBLOCKSIZE;
            p_assert(blksize==blkff2size);
            p_fread(blocklabel, sizeof(char), 4, fd);
            p_assert_(VARNAME(blocklabel=="HEAD"), strncmp(blocklabel,"HEAD",4)==0);
            p_fread(&nextblock, sizeof(int), 1, fd);
            p_assert(nextblock==sizeof(header_t) + 2 * sizeof(int));
            READBLOCKSIZE;
            p_assert(blksize==blkff2size);
        }
        if(SnapFormat == 1 || SnapFormat ==2)
        {
            READBLOCKSIZE;
            p_assert(blksize==sizeof(header_t));
            p_fread(&header1, sizeof(header1), 1, fd);
            READBLOCKSIZE;
            p_assert(blksize==sizeof(header_t));
        }
        else if(SnapFormat == 3)
        {
            hdf5_file = H5Fopen(buf, H5F_ACC_RDONLY, H5P_DEFAULT);
            read_header_attributes_in_hdf5(hdf5_file, &header1);
            for(type = 0; type < 6; type++)
            {
                if(header1.npart[type] > 0)
                {
                    sprintf(h5buf, "/PartType%d", type);
                    hdf5_grp[type] = H5Gopen(hdf5_file, h5buf, H5P_DEFAULT);
                }
            }
        }
        PrintMessage(4, "Reading block: 'HEAD' done.");

        P.header = header1;

        // checks header
        debugPrintUnsignedIntArray(header1.npart,6);
        debugPrintDoubleArray(header1.mass,6);
        debugPrintDouble(header1.time);
        debugPrintDouble(header1.redshift);
        debugPrintInt(header1.flag_sfr);
        debugPrintInt(header1.flag_feedback);
        debugPrintUnsignedIntArray(header1.npartTotal,6);
        debugPrintInt(header1.flag_cooling);
        debugPrintInt(header1.num_files);
        debugPrintDouble(header1.BoxSize);
        debugPrintDouble(header1.Omega0);
        debugPrintDouble(header1.OmegaLambda);
        debugPrintDouble(header1.HubbleParam);
        debugPrintInt(header1.flag_stellarage);
        debugPrintInt(header1.flag_metals);
        debugPrintUnsignedIntArray(header1.npartTotalHighWord,6);
        debugPrintInt(header1.flag_entropy_instead_u);
        debugPrintInt(header1.flag_doubleprecision);
        debugPrintInt(header1.flag_ic_info);
        debugPrintFloat(header1.lpt_scalingfactor);

        // get number of particles in this file
        npart = header1.npart[0] + header1.npart[1] + header1.npart[2] + header1.npart[3] + header1.npart[4] + header1.npart[5];

        // get number of particle with masses (ntot_withmasses)
        for(type = 0, ntot_withmasses = 0; type < 6; type++)
            if(header1.mass[type] > 0.)
                ntot_withmasses += header1.npart[type];

        // allocate memory
        allocate_snapshot(&P, (particleID_t)npart, VARNAME(P));

        UNINDENT;
        sprintf(G__msg__, "Reading header in '%s' done.", buf);
        PrintMessage(3, G__msg__);

        sprintf(G__msg__, "Reading snapshot in '%s' (%d particles)...", buf, npart);
        PrintMessage(3, G__msg__);
        INDENT;

        // read file: get particle position
        sprintf(G__msg__, "Reading block: '%.4s'...", Tab_IO_Labels[IO_POS]);
        PrintMessage(4, G__msg__);
        if(SnapFormat == 2)
        {
            READBLOCKSIZE;
            p_assert(blksize==blkff2size);

            p_fread(blocklabel, sizeof(char), 4, fd);
            p_assert_(VARNAME(blocklabel==Tab_IO_Labels[IO_POS]), strncmp(blocklabel,Tab_IO_Labels[IO_POS],4)==0);

            p_fread(&nextblock, sizeof(int), 1, fd);
            bytes_per_blockelement = get_bytes_per_blockelement(IO_POS);
            p_assert(nextblock==(int)(npart * bytes_per_blockelement + 2 * sizeof(int)));

            READBLOCKSIZE;
            p_assert(blksize==blkff2size);
        }
        if(SnapFormat == 1 || SnapFormat == 2)
        {
            READBLOCKSIZE;
            bytes_per_blockelement = get_bytes_per_blockelement(IO_POS);
            p_assert(blksize==(int)(npart * bytes_per_blockelement));
            for(type = 0, pc_new = pc; type < 6; type++)
            {
                for(n = 0; n < header1.npart[type]; n++)
                {
                    float Pos[3];
                    p_fread(Pos, sizeof(float), 3, fd);

                    // cast to double
                    P.positions[pc_new].Pos[0] = (double)Pos[0];
                    P.positions[pc_new].Pos[1] = (double)Pos[1];
                    P.positions[pc_new].Pos[2] = (double)Pos[2];

                    pc_new++;
                }
            }
            READBLOCKSIZE;
            p_assert(blksize==(int)(npart * bytes_per_blockelement));
        }
        else if(SnapFormat == 3)
        {
            read_hdf5_block(IO_POS, hdf5_grp, &P, header1, pc);
        }
        sprintf(G__msg__, "Reading block: '%.4s' done.", Tab_IO_Labels[IO_POS]);
        PrintMessage(4, G__msg__);

        // read file: get particle velocity
        sprintf(G__msg__, "Reading block: '%.4s'...", Tab_IO_Labels[IO_VEL]);
        PrintMessage(4, G__msg__);
        if(SnapFormat == 2)
        {
            READBLOCKSIZE;
            p_assert(blksize==blkff2size);

            p_fread(blocklabel, sizeof(char), 4, fd);
            p_assert_(VARNAME(blocklabel==Tab_IO_Labels[IO_VEL]), strncmp(blocklabel,Tab_IO_Labels[IO_VEL],4)==0);

            p_fread(&nextblock, sizeof(int), 1, fd);
            bytes_per_blockelement = get_bytes_per_blockelement(IO_VEL);
            p_assert(nextblock==(int)(npart * bytes_per_blockelement + 2 * sizeof(int)));

            READBLOCKSIZE;
            p_assert(blksize==blkff2size);
        }
        if(SnapFormat == 1 || SnapFormat == 2)
        {
            READBLOCKSIZE;
            bytes_per_blockelement = get_bytes_per_blockelement(IO_VEL);
            p_assert(blksize==(int)(npart * bytes_per_blockelement));
            for(type = 0, pc_new = pc; type < 6; type++)
            {
                for(n = 0; n < header1.npart[type]; n++)
                {
                    float Vel[3];
                    p_fread(Vel, sizeof(float), 3, fd);

                    // cast to double
                    P.velocities[pc_new].Vel[0] = (double)Vel[0];
                    P.velocities[pc_new].Vel[1] = (double)Vel[1];
                    P.velocities[pc_new].Vel[2] = (double)Vel[2];

                    pc_new++;
                }
            }
            READBLOCKSIZE;
            p_assert(blksize==(int)(npart * bytes_per_blockelement));
        }
        else if(SnapFormat == 3)
        {
            read_hdf5_block(IO_VEL, hdf5_grp, &P, header1, pc);
        }
        sprintf(G__msg__, "Reading block: '%.4s' done.", Tab_IO_Labels[IO_VEL]);
        PrintMessage(4, G__msg__);

        // read file: get particle Id
        sprintf(G__msg__, "Reading block: '%.4s'...", Tab_IO_Labels[IO_ID]);
        PrintMessage(4, G__msg__);
        if(SnapFormat == 2)
        {
            READBLOCKSIZE;
            p_assert(blksize==blkff2size);

            p_fread(blocklabel, sizeof(char), 4, fd);
            p_assert_(VARNAME(blocklabel==Tab_IO_Labels[IO_ID]), strncmp(blocklabel,Tab_IO_Labels[IO_ID],4)==0);

            p_fread(&nextblock, sizeof(int), 1, fd);
            bytes_per_blockelement = get_bytes_per_blockelement(IO_ID);
            p_assert(nextblock==(int)(npart * bytes_per_blockelement + 2 * sizeof(int)));

            READBLOCKSIZE;
            p_assert(blksize==blkff2size);
        }
        if(SnapFormat == 1 || SnapFormat == 2)
        {
            READBLOCKSIZE;
            bytes_per_blockelement = get_bytes_per_blockelement(IO_ID);
            p_assert(blksize==(int)(npart * bytes_per_blockelement));
            for(type = 0, pc_new = pc; type < 6; type++)
            {
                for(n = 0; n < header1.npart[type]; n++)
                {
                    p_fread(&P.ids[pc_new].Id, sizeof(particleID_t), 1, fd);
                    pc_new++;
                }
            }
            READBLOCKSIZE;
            p_assert(blksize==(int)(npart * bytes_per_blockelement));
        }
        else if(SnapFormat == 3)
        {
            read_hdf5_block(IO_ID, hdf5_grp, &P, header1, pc);
        }
        sprintf(G__msg__, "Reading block: '%.4s' done.", Tab_IO_Labels[IO_ID]);
        PrintMessage(4, G__msg__);

        // read file: get particle types and masses
        #ifdef HAVE_TYPE_AND_MASS
        debugPrintUnsignedInt(ntot_withmasses);
        if(ntot_withmasses > 0)
        {
            sprintf(G__msg__, "Reading block: '%.4s'...", Tab_IO_Labels[IO_MASS]);
            PrintMessage(4, G__msg__);
            if(SnapFormat == 2)
            {
                READBLOCKSIZE;
                p_assert(blksize==blkff2size);

                p_fread(blocklabel, sizeof(char), 4, fd);
                p_assert_(VARNAME(blocklabel==Tab_IO_Labels[IO_MASS]), strncmp(blocklabel,Tab_IO_Labels[IO_MASS],4)==0);

                p_fread(&nextblock, sizeof(int), 1, fd);
                bytes_per_blockelement = get_bytes_per_blockelement(IO_MASS);
                p_assert(nextblock==(int)(npart * bytes_per_blockelement + 2 * sizeof(int)));

                READBLOCKSIZE;
                p_assert(blksize==blkff2size);
            }
            if(SnapFormat == 1 || SnapFormat == 2)
            {
                READBLOCKSIZE;
                bytes_per_blockelement = get_bytes_per_blockelement(IO_MASS);
                p_assert(blksize==(int)(npart * bytes_per_blockelement));
                for(type = 0, pc_new = pc; type < 6; type++)
                {
                    for(n = 0; n < header1.npart[type]; n++)
                    {
                        P.types[pc_new].Type = type;
                        if(header1.mass[type] == 0)
                        {
                            float Mass;
                            p_fread(&Mass, sizeof(float), 1, fd);

                            // cast to double
                            P.masses[pc_new].Mass = (double)Mass;
                        }
                        else
                            P.masses[pc_new].Mass = header1.mass[type];

                        pc_new++;
                    }
                }
                READBLOCKSIZE;
                p_assert(blksize==(int)(npart * bytes_per_blockelement));
            }
            else if(SnapFormat == 3)
            {
                read_hdf5_block(IO_MASS, hdf5_grp, &P, header1, pc);
            }
            sprintf(G__msg__, "Reading block: '%.4s' done.", Tab_IO_Labels[IO_MASS]);
            PrintMessage(4, G__msg__);
        }
        else
        {
            sprintf(G__msg__, "Skipping block: '%.4s' (if it exists).", Tab_IO_Labels[IO_MASS]);
            PrintMessage(4, G__msg__);
        }
        #endif

        // read file: extras
        #ifdef HAVE_EXTRAS
        debugPrintUnsignedInt(header1.npart[0]);
        if(header1.npart[0] > 0)
        {
            // read file: get internal energy per unit mass (U) (only SPH particles)
            sprintf(G__msg__, "Reading block: '%.4s'...", Tab_IO_Labels[IO_U]);
            PrintMessage(4, G__msg__);
            if(SnapFormat == 2)
            {
                READBLOCKSIZE;
                p_assert(blksize==blkff2size);

                p_fread(blocklabel, sizeof(char), 4, fd);
                p_assert_(VARNAME(blocklabel==Tab_IO_Labels[IO_U]), strncmp(blocklabel,Tab_IO_Labels[IO_U],4)==0);

                p_fread(&nextblock, sizeof(int), 1, fd);
                bytes_per_blockelement = get_bytes_per_blockelement(IO_U);
                p_assert(nextblock==(int)(npart * bytes_per_blockelement + 2 * sizeof(int)));

                READBLOCKSIZE;
                p_assert(blksize==blkff2size);
            }
            if(SnapFormat == 1 || SnapFormat == 2)
            {
                READBLOCKSIZE;
                bytes_per_blockelement = get_bytes_per_blockelement(IO_U);
                p_assert(blksize==(int)(npart * bytes_per_blockelement));
                for(n = 0, pc_new = pc; n < header1.npart[0]; n++)
                {
                    float U;
                    p_fread(&U, sizeof(float), 1, fd);

                    // cast to double
                    P.extras[pc_new].U = (double)U;

                    pc_new++;
                }
                READBLOCKSIZE;
                p_assert(blksize==(int)(npart * bytes_per_blockelement));
            }
            else if(SnapFormat == 3)
            {
                read_hdf5_block(IO_U, hdf5_grp, &P, header1, pc);
            }
            sprintf(G__msg__, "Reading block: '%.4s' done.", Tab_IO_Labels[IO_U]);
            PrintMessage(4, G__msg__);

            // read file: get density (Rho) (only SPH particles)
            sprintf(G__msg__, "Reading block: '%.4s'...", Tab_IO_Labels[IO_RHO]);
            PrintMessage(4, G__msg__);
            if(SnapFormat == 2)
            {
                READBLOCKSIZE;
                p_assert(blksize==blkff2size);

                p_fread(blocklabel, sizeof(char), 4, fd);
                p_assert_(VARNAME(blocklabel==Tab_IO_Labels[IO_RHO]), strncmp(blocklabel,Tab_IO_Labels[IO_RHO],4)==0);

                p_fread(&nextblock, sizeof(int), 1, fd);
                bytes_per_blockelement = get_bytes_per_blockelement(IO_RHO);
                p_assert(nextblock==(int)(npart * bytes_per_blockelement + 2 * sizeof(int)));

                READBLOCKSIZE;
                p_assert(blksize==blkff2size);
            }
            if(SnapFormat == 1 || SnapFormat == 2)
            {
                READBLOCKSIZE;
                bytes_per_blockelement = get_bytes_per_blockelement(IO_RHO);
                p_assert(blksize==(int)(npart * bytes_per_blockelement));
                for(n = 0, pc_new = pc; n < header1.npart[0]; n++)
                {
                    float Rho;
                    p_fread(&Rho, sizeof(float), 1, fd);

                    // cast to double
                    P.extras[pc_new].Rho = (double)Rho;

                    pc_new++;
                }
                READBLOCKSIZE;
                p_assert(blksize==(int)(npart * bytes_per_blockelement));
            }
            else if(SnapFormat == 3)
            {
                read_hdf5_block(IO_RHO, hdf5_grp, &P, header1, pc);
            }
            sprintf(G__msg__, "Reading block: '%.4s' done.", Tab_IO_Labels[IO_RHO]);
            PrintMessage(4, G__msg__);

            // read file: get SPH smoothing length (HSML) (only SPH particles)
            sprintf(G__msg__, "Reading block: '%.4s'...", Tab_IO_Labels[IO_HSML]);
            PrintMessage(4, G__msg__);
            if(SnapFormat == 2)
            {
                READBLOCKSIZE;
                p_assert(blksize==blkff2size);

                p_fread(blocklabel, sizeof(char), 4, fd);
                p_assert_(VARNAME(blocklabel==Tab_IO_Labels[IO_HSML]), strncmp(blocklabel,Tab_IO_Labels[IO_HSML],4)==0);

                p_fread(&nextblock, sizeof(int), 1, fd);
                bytes_per_blockelement = get_bytes_per_blockelement(IO_HSML);
                p_assert(nextblock==(int)(npart * bytes_per_blockelement + 2 * sizeof(int)));

                READBLOCKSIZE;
                p_assert(blksize==blkff2size);
            }
            if(SnapFormat == 1 || SnapFormat == 2)
            {
                READBLOCKSIZE;
                bytes_per_blockelement = get_bytes_per_blockelement(IO_HSML);
                p_assert(blksize==(int)(npart * bytes_per_blockelement));
                for(n = 0, pc_new = pc; n < header1.npart[0]; n++)
                {
                    float Hsml;
                    p_fread(&Hsml, sizeof(float), 1, fd);

                    // cast to double
                    P.extras[pc_new].Hsml = (double)Hsml;

                    pc_new++;
                }
                READBLOCKSIZE;
                p_assert(blksize==(int)(npart * bytes_per_blockelement));
            }
            else if(SnapFormat == 3)
            {
                read_hdf5_block(IO_HSML, hdf5_grp, &P, header1, pc);
            }
            sprintf(G__msg__, "Reading block: '%.4s' done.", Tab_IO_Labels[IO_HSML]);
            PrintMessage(4, G__msg__);
        }
        else
        {
            sprintf(G__msg__, "Skipping blocks: '%.4s', '%.4s', '%.4s' (if they exist).", Tab_IO_Labels[IO_U], Tab_IO_Labels[IO_RHO], Tab_IO_Labels[IO_HSML]);
            PrintMessage(4, G__msg__);
        }
        #endif

        UNINDENT;
        sprintf(G__msg__, "Reading snapshot in '%s' done.", buf);
        PrintMessage(3, G__msg__);

        fclose(fd);
        if(SnapFormat == 3)
        {
            for(type = 5; type >= 0; type--)
                if(header1.npart[type] > 0)
                    H5Gclose(hdf5_grp[type]);
            H5Fclose(hdf5_file);
        }
    }
    return P;
} //read_snapshot
///-------------------------------------------------------------------------------------
/** @fn write_header_attributes_in_hdf5
 *  This function writes the header information in case HDF5 is selected as
 *  file format.
 * @param hdf5_headergrp
 * @param header
 */
void write_header_attributes_in_hdf5(hid_t hdf5_file, header_t header)
{
    hsize_t adim[1] = { 6 };
    hid_t hdf5_headergrp, hdf5_dataspace, hdf5_attribute;

    hdf5_headergrp = H5Gcreate2(hdf5_file, "/Header", 0, H5P_DEFAULT, H5P_DEFAULT);

    hdf5_dataspace = H5Screate(H5S_SIMPLE);
    H5Sset_extent_simple(hdf5_dataspace, 1, adim, NULL);
    hdf5_attribute = H5Acreate2(hdf5_headergrp, "NumPart_ThisFile", H5T_NATIVE_INT, hdf5_dataspace, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(hdf5_attribute, H5T_NATIVE_UINT, header.npart);
    H5Aclose(hdf5_attribute);
    H5Sclose(hdf5_dataspace);

    hdf5_dataspace = H5Screate(H5S_SIMPLE);
    H5Sset_extent_simple(hdf5_dataspace, 1, adim, NULL);
    hdf5_attribute = H5Acreate2(hdf5_headergrp, "MassTable", H5T_NATIVE_DOUBLE, hdf5_dataspace, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(hdf5_attribute, H5T_NATIVE_DOUBLE, header.mass);
    H5Aclose(hdf5_attribute);
    H5Sclose(hdf5_dataspace);

    hdf5_dataspace = H5Screate(H5S_SCALAR);
    hdf5_attribute = H5Acreate2(hdf5_headergrp, "Time", H5T_NATIVE_DOUBLE, hdf5_dataspace, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(hdf5_attribute, H5T_NATIVE_DOUBLE, &header.time);
    H5Aclose(hdf5_attribute);
    H5Sclose(hdf5_dataspace);

    hdf5_dataspace = H5Screate(H5S_SCALAR);
    hdf5_attribute = H5Acreate2(hdf5_headergrp, "Redshift", H5T_NATIVE_DOUBLE, hdf5_dataspace, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(hdf5_attribute, H5T_NATIVE_DOUBLE, &header.redshift);
    H5Aclose(hdf5_attribute);
    H5Sclose(hdf5_dataspace);

    hdf5_dataspace = H5Screate(H5S_SCALAR);
    hdf5_attribute = H5Acreate2(hdf5_headergrp, "Flag_Sfr", H5T_NATIVE_INT, hdf5_dataspace, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(hdf5_attribute, H5T_NATIVE_INT, &header.flag_sfr);
    H5Aclose(hdf5_attribute);
    H5Sclose(hdf5_dataspace);

    hdf5_dataspace = H5Screate(H5S_SCALAR);
    hdf5_attribute = H5Acreate2(hdf5_headergrp, "Flag_Feedback", H5T_NATIVE_INT, hdf5_dataspace, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(hdf5_attribute, H5T_NATIVE_INT, &header.flag_feedback);
    H5Aclose(hdf5_attribute);
    H5Sclose(hdf5_dataspace);

    hdf5_dataspace = H5Screate(H5S_SIMPLE);
    H5Sset_extent_simple(hdf5_dataspace, 1, adim, NULL);
    hdf5_attribute = H5Acreate2(hdf5_headergrp, "NumPart_Total", H5T_NATIVE_UINT, hdf5_dataspace, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(hdf5_attribute, H5T_NATIVE_UINT, header.npartTotal);
    H5Aclose(hdf5_attribute);
    H5Sclose(hdf5_dataspace);

    hdf5_dataspace = H5Screate(H5S_SCALAR);
    hdf5_attribute = H5Acreate2(hdf5_headergrp, "Flag_Cooling", H5T_NATIVE_INT, hdf5_dataspace, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(hdf5_attribute, H5T_NATIVE_INT, &header.flag_cooling);
    H5Aclose(hdf5_attribute);
    H5Sclose(hdf5_dataspace);

    hdf5_dataspace = H5Screate(H5S_SCALAR);
    hdf5_attribute = H5Acreate2(hdf5_headergrp, "NumFilesPerSnapshot", H5T_NATIVE_INT, hdf5_dataspace, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(hdf5_attribute, H5T_NATIVE_INT, &header.num_files);
    H5Aclose(hdf5_attribute);
    H5Sclose(hdf5_dataspace);

    hdf5_dataspace = H5Screate(H5S_SCALAR);
    hdf5_attribute = H5Acreate2(hdf5_headergrp, "Omega0", H5T_NATIVE_DOUBLE, hdf5_dataspace, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(hdf5_attribute, H5T_NATIVE_DOUBLE, &header.Omega0);
    H5Aclose(hdf5_attribute);
    H5Sclose(hdf5_dataspace);

    hdf5_dataspace = H5Screate(H5S_SCALAR);
    hdf5_attribute = H5Acreate2(hdf5_headergrp, "OmegaLambda", H5T_NATIVE_DOUBLE, hdf5_dataspace, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(hdf5_attribute, H5T_NATIVE_DOUBLE, &header.OmegaLambda);
    H5Aclose(hdf5_attribute);
    H5Sclose(hdf5_dataspace);

    hdf5_dataspace = H5Screate(H5S_SCALAR);
    hdf5_attribute = H5Acreate2(hdf5_headergrp, "HubbleParam", H5T_NATIVE_DOUBLE, hdf5_dataspace, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(hdf5_attribute, H5T_NATIVE_DOUBLE, &header.HubbleParam);
    H5Aclose(hdf5_attribute);
    H5Sclose(hdf5_dataspace);

    hdf5_dataspace = H5Screate(H5S_SCALAR);
    hdf5_attribute = H5Acreate2(hdf5_headergrp, "BoxSize", H5T_NATIVE_DOUBLE, hdf5_dataspace, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(hdf5_attribute, H5T_NATIVE_DOUBLE, &header.BoxSize);
    H5Aclose(hdf5_attribute);
    H5Sclose(hdf5_dataspace);

    hdf5_dataspace = H5Screate(H5S_SCALAR);
    hdf5_attribute = H5Acreate2(hdf5_headergrp, "Flag_StellarAge", H5T_NATIVE_INT, hdf5_dataspace, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(hdf5_attribute, H5T_NATIVE_INT, &header.flag_stellarage);
    H5Aclose(hdf5_attribute);
    H5Sclose(hdf5_dataspace);

    hdf5_dataspace = H5Screate(H5S_SCALAR);
    hdf5_attribute = H5Acreate2(hdf5_headergrp, "Flag_Metals", H5T_NATIVE_INT, hdf5_dataspace, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(hdf5_attribute, H5T_NATIVE_INT, &header.flag_metals);
    H5Aclose(hdf5_attribute);
    H5Sclose(hdf5_dataspace);

    hdf5_dataspace = H5Screate(H5S_SIMPLE);
    H5Sset_extent_simple(hdf5_dataspace, 1, adim, NULL);
    hdf5_attribute = H5Acreate2(hdf5_headergrp, "NumPart_Total_HighWord", H5T_NATIVE_UINT, hdf5_dataspace, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(hdf5_attribute, H5T_NATIVE_UINT, header.npartTotalHighWord);
    H5Aclose(hdf5_attribute);
    H5Sclose(hdf5_dataspace);

    hdf5_dataspace = H5Screate(H5S_SIMPLE);
    H5Sset_extent_simple(hdf5_dataspace, 1, adim, NULL);
    hdf5_attribute = H5Acreate2(hdf5_headergrp, "Flag_Entropy_ICs", H5T_NATIVE_UINT, hdf5_dataspace, H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(hdf5_attribute, H5T_NATIVE_UINT, &header.flag_entropy_instead_u);
    H5Aclose(hdf5_attribute);
    H5Sclose(hdf5_dataspace);

    H5Gclose(hdf5_headergrp);
} //write_header_attributes_in_hdf5
///-------------------------------------------------------------------------------------
/** @fn write_hdf5_block
 *  This function writes a block in a Gadget fileformat 3 snapshot file
 * @param blocknr
 * @param hdf5_grp
 * @param P
 * @param header
 * @param pc
 */
static void write_hdf5_block(enum iofields blocknr, hid_t hdf5_grp[6], Snapshot P, header_t header, particleID_t pc)
{
    int rank, type, bytes_per_blockelement; particleID_t pc_new; unsigned int n;
    char h5buf[BUFLENGTH];
    hid_t hdf5_dataspace_in_file, hdf5_dataspace_in_memory, hdf5_dataset;
    hsize_t dims[2], offset[2];

    for(type = 0, pc_new = pc; type < 6; type++)
    {
        if(header.npart[type] <= 0)
            continue;
        get_dataset_name(blocknr, h5buf);
        bytes_per_blockelement = get_bytes_per_blockelement(blocknr);

        dims[0] = (hsize_t)header.npart[type];        // write all particles in file
        dims[1] = (hsize_t)get_values_per_blockelement(blocknr);
        if(dims[1] == 1)
            rank = 1;
        else
            rank = 2;
        hdf5_dataspace_in_file = H5Screate_simple(rank, dims, NULL);
        offset[1] = 0;

        // check available memory from system information
        struct sysinfo info;
        if(sysinfo(&info) != 0)
        {
            char buf[BUFLENGTH];
            FormatFunction(buf);
            sprintf(G__msg__, "Unable to access system information in %s%s.", buf, FONT_LIGHTRED);
            FatalError(G__msg__);
        }
        unsigned long freeram = info.freeram * info.mem_unit;
        unsigned long bufferram = info.bufferram * info.mem_unit;
        unsigned long totalavailram = (freeram + bufferram - 10*8*1024*1024); // ensure that at least 10MB will remain

        unsigned int npart_max = (unsigned int)floor(totalavailram/bytes_per_blockelement);
        npart_max = min(min(npart_max,header.npart[type]),512*512*512); // maximum 512**3 particles per chunk for safety
        debugPrintUnsignedInt(npart_max);

        switch(blocknr)
        {
            case IO_POS:
                hdf5_dataset = H5Dcreate2(hdf5_grp[type], h5buf, H5T_NATIVE_FLOAT, hdf5_dataspace_in_file, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
                break;
            case IO_VEL:
                hdf5_dataset = H5Dcreate2(hdf5_grp[type], h5buf, H5T_NATIVE_FLOAT, hdf5_dataspace_in_file, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
                break;
            case IO_ID:
                #ifdef LONGIDS
                    hdf5_dataset = H5Dcreate2(hdf5_grp[type], h5buf, H5T_NATIVE_UINT64, hdf5_dataspace_in_file, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
                #else
                    hdf5_dataset = H5Dcreate2(hdf5_grp[type], h5buf, H5T_NATIVE_UINT, hdf5_dataspace_in_file, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
                #endif
                break;
            case IO_MASS:
                #if HAVE_TYPE_AND_MASS
                hdf5_dataset = H5Dcreate2(hdf5_grp[type], h5buf, H5T_NATIVE_FLOAT, hdf5_dataspace_in_file, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
                break;
                #endif
            case IO_U:
                #if HAVE_EXTRAS
                hdf5_dataset = H5Dcreate2(hdf5_grp[type], h5buf, H5T_NATIVE_FLOAT, hdf5_dataspace_in_file, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
                break;
                #endif
            case IO_RHO:
                #if HAVE_EXTRAS
                hdf5_dataset = H5Dcreate2(hdf5_grp[type], h5buf, H5T_NATIVE_FLOAT, hdf5_dataspace_in_file, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
                break;
                #endif
            case IO_HSML:
                #if HAVE_EXTRAS
                hdf5_dataset = H5Dcreate2(hdf5_grp[type], h5buf, H5T_NATIVE_FLOAT, hdf5_dataspace_in_file, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
                break;
                #endif
            case IO_POT:
            case IO_ACCEL:
            case IO_DTENTR:
            case IO_TSTP:
                break;
        }

        n = 0;
        while(n < header.npart[type])
        {
            INDENT;

            unsigned int npart_chunk = min(npart_max, header.npart[type]-n);
            sprintf(G__msg__, "Writing chunk of size %d, current particleID: n=%d.", npart_chunk, n);
            debugPrintDiagnostic(G__msg__);

            dims[0] = (hsize_t)npart_chunk;
            offset[0] = (hsize_t)n;
            hdf5_dataspace_in_memory = H5Screate_simple(rank, dims, NULL);
            H5Sselect_hyperslab(hdf5_dataspace_in_file, H5S_SELECT_SET, offset, NULL, dims, NULL);

            // malloc an array
            float *FloatBuffer;
            particleID_t *ParticleIDtypeBuffer;
            switch(blocknr)
            {
                case IO_ID:
                    ParticleIDtypeBuffer = (particleID_t *)p_malloc(npart_chunk*bytes_per_blockelement, VARNAME(ParticleIDtypeBuffer));
                    break;
                default:
                    FloatBuffer = (float *)p_malloc(npart_chunk*bytes_per_blockelement, VARNAME(FloatBuffer));
                    break;
            }

            // fill buffer array and write it to hdf5
            switch(blocknr)
            {
                case IO_POS:
                    #pragma omp parallel for schedule(static)
                    for(particleID_t pc=pc_new; pc < pc_new+npart_chunk; pc++)
                    {
                        particleID_t np=pc-pc_new;
                        // cast to float
                        FloatBuffer[3*np+0] = (float)P.positions[pc].Pos[0];
                        FloatBuffer[3*np+1] = (float)P.positions[pc].Pos[1];
                        FloatBuffer[3*np+2] = (float)P.positions[pc].Pos[2];
                    }
                    H5Dwrite(hdf5_dataset, H5T_NATIVE_FLOAT, hdf5_dataspace_in_memory, hdf5_dataspace_in_file, H5P_DEFAULT, FloatBuffer);
                    break;
                case IO_VEL:
                    #pragma omp parallel for schedule(static)
                    for(particleID_t pc=pc_new; pc < pc_new+npart_chunk; pc++)
                    {
                        particleID_t np=pc-pc_new;
                        // cast to float
                        FloatBuffer[3*np+0] = (float)P.velocities[pc].Vel[0];
                        FloatBuffer[3*np+1] = (float)P.velocities[pc].Vel[1];
                        FloatBuffer[3*np+2] = (float)P.velocities[pc].Vel[2];
                    }
                    H5Dwrite(hdf5_dataset, H5T_NATIVE_FLOAT, hdf5_dataspace_in_memory, hdf5_dataspace_in_file, H5P_DEFAULT, FloatBuffer);
                    break;
                case IO_ID:
                    #pragma omp parallel for schedule(static)
                    for(particleID_t pc=pc_new; pc < pc_new+npart_chunk; pc++)
                    {
                        particleID_t np=pc-pc_new;
                        ParticleIDtypeBuffer[np]= (particleID_t)P.ids[pc].Id;
                    }
                    #ifdef LONGIDS
                        H5Dwrite(hdf5_dataset, H5T_NATIVE_UINT64, hdf5_dataspace_in_memory, hdf5_dataspace_in_file, H5P_DEFAULT, ParticleIDtypeBuffer);
                    #else
                        H5Dwrite(hdf5_dataset, H5T_NATIVE_UINT, hdf5_dataspace_in_memory, hdf5_dataspace_in_file, H5P_DEFAULT, ParticleIDtypeBuffer);
                    #endif
                    break;
                case IO_MASS:
                    #if HAVE_TYPE_AND_MASS
                    #pragma omp parallel for schedule(static)
                    for(particleID_t pc=pc_new; pc < pc_new+npart_chunk; pc++)
                    {
                        particleID_t np=pc-pc_new;
                        if(header.mass[type] == 0)
                            // cast to float
                            FloatBuffer[np] = (float)P.masses[pc].Mass;
                        else
                            FloatBuffer[np] = header.mass[type];
                    }
                    H5Dwrite(hdf5_dataset, H5T_NATIVE_FLOAT, hdf5_dataspace_in_memory, hdf5_dataspace_in_file, H5P_DEFAULT, FloatBuffer);
                    #endif
                    break;
                case IO_U:
                    #if HAVE_EXTRAS
                    #pragma omp parallel for schedule(static)
                    for(particleID_t pc=pc_new; pc < pc_new+npart_chunk; pc++)
                    {
                        particleID_t np=pc-pc_new;
                        // cast to float
                        FloatBuffer[np] = (float)P.extras[pc].U;
                    }
                    H5Dwrite(hdf5_dataset, H5T_NATIVE_FLOAT, hdf5_dataspace_in_memory, hdf5_dataspace_in_file, H5P_DEFAULT, FloatBuffer);
                    #endif
                    break;
                case IO_RHO:
                    #if HAVE_EXTRAS
                    #pragma omp parallel for schedule(static)
                    for(particleID_t pc=pc_new; pc < pc_new+npart_chunk; pc++)
                    {
                        particleID_t np=pc-pc_new;
                        // cast to float
                        FloatBuffer[np] = (float)P.extras[pc].Rho;
                    }
                    H5Dwrite(hdf5_dataset, H5T_NATIVE_FLOAT, hdf5_dataspace_in_memory, hdf5_dataspace_in_file, H5P_DEFAULT, FloatBuffer);
                    #endif
                    break;
                case IO_HSML:
                    #if HAVE_EXTRAS
                    #pragma omp parallel for schedule(static)
                    for(particleID_t pc=pc_new; pc < pc_new+npart_chunk; pc++)
                    {
                        particleID_t np=pc-pc_new;
                        // cast to float
                        FloatBuffer[np] = (float)P.extras[pc].Hsml;
                    }
                    H5Dwrite(hdf5_dataset, H5T_NATIVE_FLOAT, hdf5_dataspace_in_memory, hdf5_dataspace_in_file, H5P_DEFAULT, FloatBuffer);
                    #endif
                    break;
                case IO_POT:
                case IO_ACCEL:
                case IO_DTENTR:
                case IO_TSTP:
                    break;
            }
            pc_new+=npart_chunk;

            // free memory
            switch(blocknr)
            {
                case IO_ID:
                    p_free(ParticleIDtypeBuffer, VARNAME(ParticleIDtypeBuffer));
                    break;
                default:
                    p_free(FloatBuffer, VARNAME(FloatBuffer));
                    break;
            }

            // update current particleID
            n += npart_chunk;

            UNINDENT;
        } //end while

        H5Sclose(hdf5_dataspace_in_memory);
        H5Sclose(hdf5_dataspace_in_file);
        H5Dclose(hdf5_dataset);
    }
} //write_hdf5_block
///-------------------------------------------------------------------------------------
/** Writing routine for Gadget's default binary file format
 * (A snapshot may be distributed into multiple files).
 * @param P a snapshot in default format
 * @param *fname pointer to the output filename (complete path)
 * @param files number of files for distributed snapshots
 * @param SnapFormat format of Gadget snapshot
 */
void write_snapshot(Snapshot P, const char *fname, const int files, const int SnapFormat)
{
    FILE *fd;
    char buf[BUFLENGTH], h5buf[BUFLENGTH];
    int file, type;
    unsigned int n, npart, ntot_withmasses;
    header_t header1 = P.header;
    particleID_t pc, pc_new;
    hid_t hdf5_file, hdf5_grp[6];

    fill_Tab_IO_Labels();

    int blksize, nextblock, bytes_per_blockelement;
    #define WRITEBLOCKSIZE p_fwrite(&blksize, sizeof(blksize), 1, fd);

    for(file = 0, pc = 0; file < files; file++, pc = pc_new)
    {
        // get file name
        if(files > 1)
            sprintf(buf, "%s.%d", fname, file);
        else
            sprintf(buf, "%s", fname);

        // open file
        if(!(fd = fopen(buf, "w")))
        {
            sprintf(G__msg__, "Failed to open snapshot for writing in file '%s'.", buf);
            FatalError(G__msg__);
        }

        sprintf(G__msg__, "Writing header in '%s'...", buf);
        PrintMessage(3, G__msg__);
        INDENT;

        // choose number of particles to write per file
        npart = header1.npart[0] + header1.npart[1] + header1.npart[2] + header1.npart[3] + header1.npart[4] + header1.npart[5];

        // check header
        debugPrintUnsignedIntArray(header1.npart,6);
        debugPrintDoubleArray(header1.mass,6);
        debugPrintDouble(header1.time);
        debugPrintDouble(header1.redshift);
        debugPrintInt(header1.flag_sfr);
        debugPrintInt(header1.flag_feedback);
        debugPrintUnsignedIntArray(header1.npartTotal,6);
        debugPrintInt(header1.flag_cooling);
        debugPrintInt(header1.num_files);
        debugPrintDouble(header1.BoxSize);
        debugPrintDouble(header1.Omega0);
        debugPrintDouble(header1.OmegaLambda);
        debugPrintDouble(header1.HubbleParam);
        debugPrintInt(header1.flag_stellarage);
        debugPrintInt(header1.flag_metals);
        debugPrintUnsignedIntArray(header1.npartTotalHighWord,6);
        debugPrintInt(header1.flag_entropy_instead_u);
        debugPrintInt(header1.flag_doubleprecision);
        debugPrintInt(header1.flag_ic_info);
        debugPrintFloat(header1.lpt_scalingfactor);

        // write header
        if(SnapFormat == 2)
        {
            blksize = blkff2size;
            WRITEBLOCKSIZE;
            p_fwrite("HEAD", sizeof(char), 4, fd);
            nextblock = sizeof(header_t) + 2 * sizeof(int);
            p_fwrite(&nextblock, sizeof(int), 1, fd);
            WRITEBLOCKSIZE;
        }
        if(SnapFormat == 1 || SnapFormat == 2)
        {
            blksize = sizeof(header_t);
            WRITEBLOCKSIZE;
            p_fwrite(&header1, sizeof(header1), 1, fd);
            WRITEBLOCKSIZE;
        }
        else if(SnapFormat == 3)
        {
            hdf5_file = H5Fcreate(buf, H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

            for(type = 0; type < 6; type++)
            {
                if(header1.npart[type] > 0)
                {
                    sprintf(h5buf, "/PartType%d", type);
                    hdf5_grp[type] = H5Gcreate2(hdf5_file, h5buf, 0, H5P_DEFAULT, H5P_DEFAULT);
                }
            }

            write_header_attributes_in_hdf5(hdf5_file, header1);
        }

        // write file: get number of particle with masses (ntot_withmasses)
        for(type = 0, ntot_withmasses = 0; type < 6; type++)
            if(header1.mass[type] > 0.)
                ntot_withmasses += header1.npart[type];

        UNINDENT;
        sprintf(G__msg__, "Writing header in '%s' done.", buf);
        PrintMessage(3, G__msg__);

        sprintf(G__msg__, "Writing snapshot in '%s' (%d particles)...", buf, npart);
        PrintMessage(3, G__msg__);
        INDENT;

        // write file: write particle position
        sprintf(G__msg__, "Writing block: '%.4s'...", Tab_IO_Labels[IO_POS]);
        PrintMessage(4, G__msg__);
        if(SnapFormat == 2)
        {
            blksize = blkff2size;
            WRITEBLOCKSIZE;
            p_fwrite(Tab_IO_Labels[IO_POS], sizeof(char), 4, fd);
            bytes_per_blockelement = get_bytes_per_blockelement(IO_POS);
            nextblock = npart * bytes_per_blockelement + 2 * sizeof(int);
            p_fwrite(&nextblock, sizeof(int), 1, fd);
            WRITEBLOCKSIZE;
        }
        if(SnapFormat == 1 || SnapFormat == 2)
        {
            bytes_per_blockelement = get_bytes_per_blockelement(IO_POS);
            blksize = npart * bytes_per_blockelement;
            WRITEBLOCKSIZE;
            for(type = 0, pc_new = pc; type < 6; type++)
            {
                for(n = 0; n < header1.npart[type]; n++)
                {
                    // cast to float
                    float Pos[3] = {(float)P.positions[pc_new].Pos[0], (float)P.positions[pc_new].Pos[1], (float)P.positions[pc_new].Pos[2]};

                    p_fwrite(Pos, sizeof(float), 3, fd);
                    pc_new++;
                }
            }
            WRITEBLOCKSIZE;
        }
        else if(SnapFormat == 3)
        {
            write_hdf5_block(IO_POS, hdf5_grp, P, header1, pc);
        }
        sprintf(G__msg__, "Writing block: '%.4s' done.", Tab_IO_Labels[IO_POS]);
        PrintMessage(4, G__msg__);

        // write file: write particle velocity
        sprintf(G__msg__, "Writing block: '%.4s'...", Tab_IO_Labels[IO_VEL]);
        PrintMessage(4, G__msg__);
        if(SnapFormat == 2)
        {
            blksize = blkff2size;
            WRITEBLOCKSIZE;
            p_fwrite(Tab_IO_Labels[IO_VEL], sizeof(char), 4, fd);
            bytes_per_blockelement = get_bytes_per_blockelement(IO_VEL);
            nextblock = npart * bytes_per_blockelement + 2 * sizeof(int);
            p_fwrite(&nextblock, sizeof(int), 1, fd);
            WRITEBLOCKSIZE;
        }
        if(SnapFormat == 1 || SnapFormat == 2)
        {
            bytes_per_blockelement = get_bytes_per_blockelement(IO_VEL);
            blksize = npart * bytes_per_blockelement;
            WRITEBLOCKSIZE;
            for(type = 0, pc_new = pc; type < 6; type++)
            {
                for(n = 0; n < header1.npart[type]; n++)
                {
                    // cast to float
                    float Vel[3] = {(float)P.velocities[pc_new].Vel[0], (float)P.velocities[pc_new].Vel[1], (float)P.velocities[pc_new].Vel[2]};

                    p_fwrite(Vel, sizeof(float), 3, fd);
                    pc_new++;
                }
            }
            WRITEBLOCKSIZE;
        }
        else if(SnapFormat == 3)
        {
            write_hdf5_block(IO_VEL, hdf5_grp, P, header1, pc);
        }
        sprintf(G__msg__, "Writing block: '%.4s' done.", Tab_IO_Labels[IO_VEL]);
        PrintMessage(4, G__msg__);

        // write file: write particle Id
        sprintf(G__msg__, "Writing block: '%.4s'...", Tab_IO_Labels[IO_ID]);
        PrintMessage(4, G__msg__);
        if(SnapFormat == 2)
        {
            blksize = blkff2size;
            WRITEBLOCKSIZE;
            p_fwrite(Tab_IO_Labels[IO_ID], sizeof(char), 4, fd);
            bytes_per_blockelement = get_bytes_per_blockelement(IO_ID);
            nextblock = npart * bytes_per_blockelement + 2 * sizeof(int);
            p_fwrite(&nextblock, sizeof(int), 1, fd);
            WRITEBLOCKSIZE;
        }
        if(SnapFormat == 1 || SnapFormat == 2)
        {
            bytes_per_blockelement = get_bytes_per_blockelement(IO_ID);
            blksize = npart * bytes_per_blockelement;
            WRITEBLOCKSIZE;
            for(type = 0, pc_new = pc; type < 6; type++)
            {
                for(n = 0; n < header1.npart[type]; n++)
                {
                    p_fwrite(&P.ids[pc_new].Id, sizeof(particleID_t), 1, fd);
                    pc_new++;
                }
            }
            WRITEBLOCKSIZE;
        }
        else if(SnapFormat == 3)
        {
            write_hdf5_block(IO_ID, hdf5_grp, P, header1, pc);
        }
        sprintf(G__msg__, "Writing block: '%.4s' done.", Tab_IO_Labels[IO_ID]);
        PrintMessage(4, G__msg__);

        // write file: write particle masses
        #ifdef HAVE_TYPE_AND_MASS
        if(ntot_withmasses > 0)
        {
            sprintf(G__msg__, "Writing block: '%.4s'...", Tab_IO_Labels[IO_MASS]);
            PrintMessage(4, G__msg__);
            if(SnapFormat == 2)
            {
                blksize = blkff2size;
                WRITEBLOCKSIZE;
                p_fwrite(Tab_IO_Labels[IO_MASS], sizeof(char), 4, fd);
                bytes_per_blockelement = get_bytes_per_blockelement(IO_MASS);
                nextblock = npart * bytes_per_blockelement + 2 * sizeof(int);
                p_fwrite(&nextblock, sizeof(int), 1, fd);
                WRITEBLOCKSIZE;
            }
            if(SnapFormat == 1 || SnapFormat == 2)
            {
                bytes_per_blockelement = get_bytes_per_blockelement(IO_MASS);
                blksize = npart * bytes_per_blockelement;
                WRITEBLOCKSIZE;
                for(type = 0, pc_new = pc; type < 6; type++)
                {
                    for(n = 0; n < header1.npart[type]; n++)
                    {
                        if(header1.mass[type] == 0)
                        {
                            // cast to float
                            float Mass = (float)P.masses[pc_new].Mass;

                            p_fwrite(&Mass, sizeof(float), 1, fd);
                        }
                        pc_new++;
                    }
                }
                WRITEBLOCKSIZE;
            }
            else if(SnapFormat == 3)
            {
                write_hdf5_block(IO_MASS, hdf5_grp, P, header1, pc);
            }
            sprintf(G__msg__, "Writing block: '%.4s' done.", Tab_IO_Labels[IO_MASS]);
            PrintMessage(4, G__msg__);
        }
        #endif

        // write file: extras
        #ifdef HAVE_EXTRAS
        if(header1.npart[0] > 0)
        {
            // write file: write internal energy per unit mass (U) (only SPH particles)
            sprintf(G__msg__, "Writing block: '%.4s'...", Tab_IO_Labels[IO_U]);
            PrintMessage(4, G__msg__);
            if(SnapFormat == 2)
            {
                blksize = blkff2size;
                WRITEBLOCKSIZE;
                p_fwrite(Tab_IO_Labels[IO_U], sizeof(char), 4, fd);
                bytes_per_blockelement = get_bytes_per_blockelement(IO_U);
                nextblock = header1.npart[0] * bytes_per_blockelement + 2 * sizeof(int);
                p_fwrite(&nextblock, sizeof(int), 1, fd);
                WRITEBLOCKSIZE;
            }
            if(SnapFormat == 1 || SnapFormat == 2)
            {
                bytes_per_blockelement = get_bytes_per_blockelement(IO_U);
                blksize = header1.npart[0] * bytes_per_blockelement;
                WRITEBLOCKSIZE;
                for(n = 0, pc_new = pc; n < header1.npart[0]; n++)
                {
                    // cast to float
                    float U = (float)P.extras[pc_new].U;

                    p_fwrite(&U, sizeof(float), 1, fd);
                    pc_new++;
                }
                WRITEBLOCKSIZE;
            }
            else if(SnapFormat == 3)
            {
                write_hdf5_block(IO_U, hdf5_grp, P, header1, pc);
            }
            sprintf(G__msg__, "Writing block: '%.4s' done.", Tab_IO_Labels[IO_U]);
            PrintMessage(4, G__msg__);

            // write file: write density (Rho) (only SPH particles)
            sprintf(G__msg__, "Writing block: '%.4s'...", Tab_IO_Labels[IO_RHO]);
            PrintMessage(4, G__msg__);
            if(SnapFormat == 2)
            {
                blksize = blkff2size;
                WRITEBLOCKSIZE;
                p_fwrite(Tab_IO_Labels[IO_RHO], sizeof(char), 4, fd);
                bytes_per_blockelement = get_bytes_per_blockelement(IO_RHO);
                nextblock = header1.npart[0] * bytes_per_blockelement + 2 * sizeof(int);
                p_fwrite(&nextblock, sizeof(int), 1, fd);
                WRITEBLOCKSIZE;
            }
            if(SnapFormat == 1 || SnapFormat == 2)
            {
                bytes_per_blockelement = get_bytes_per_blockelement(IO_RHO);
                blksize = header1.npart[0] * bytes_per_blockelement;
                WRITEBLOCKSIZE;
                for(n = 0, pc_new = pc; n < header1.npart[0]; n++)
                {
                    // cast to float
                    float Rho = (float)P.extras[pc_new].Rho;

                    p_fwrite(&Rho, sizeof(float), 1, fd);
                    pc_new++;
                }
                WRITEBLOCKSIZE;
            }
            else if(SnapFormat == 3)
            {
                write_hdf5_block(IO_RHO, hdf5_grp, P, header1, pc);
            }
            sprintf(G__msg__, "Writing block: '%.4s' done.", Tab_IO_Labels[IO_RHO]);
            PrintMessage(4, G__msg__);

            // write file: write SPH smoothing length (HSML) (only SPH particles)
            sprintf(G__msg__, "Writing block: '%.4s'...", Tab_IO_Labels[IO_HSML]);
            PrintMessage(4, G__msg__);
            if(SnapFormat == 2)
            {
                blksize = blkff2size;
                WRITEBLOCKSIZE;
                p_fwrite(Tab_IO_Labels[IO_HSML], sizeof(char), 4, fd);
                bytes_per_blockelement = get_bytes_per_blockelement(IO_HSML);
                nextblock = header1.npart[0] * bytes_per_blockelement + 2 * sizeof(int);
                p_fwrite(&nextblock, sizeof(int), 1, fd);
                WRITEBLOCKSIZE;
            }
            if(SnapFormat == 1 || SnapFormat == 2)
            {
                bytes_per_blockelement = get_bytes_per_blockelement(IO_HSML);
                blksize = header1.npart[0] * bytes_per_blockelement;
                WRITEBLOCKSIZE;
                for(n = 0, pc_new = pc; n < header1.npart[0]; n++)
                {
                    // cast to float
                    float Hsml = (float)P.extras[pc_new].Hsml;

                    p_fwrite(&Hsml, sizeof(float), 1, fd);
                    pc_new++;
                }
                WRITEBLOCKSIZE;
            }
            else if(SnapFormat == 3)
            {
                write_hdf5_block(IO_HSML, hdf5_grp, P, header1, pc);
            }
            sprintf(G__msg__, "Writing block: '%.4s' done.", Tab_IO_Labels[IO_HSML]);
            PrintMessage(4, G__msg__);
        }
        #endif

        UNINDENT;
        sprintf(G__msg__, "Writing snapshot in '%s' done.", buf);
        PrintMessage(3, G__msg__);

        fclose(fd);
        if(SnapFormat == 3)
        {
            for(type = 5; type >= 0; type--)
                if(header1.npart[type] > 0)
                    H5Gclose(hdf5_grp[type]);
            H5Fclose(hdf5_file);
        }
    }
    return;
} //write_snapshot
///-------------------------------------------------------------------------------------
/** @fn output_snapshot
 * Outputs snapshot if necessary
 * @param WriteSnapshot boolean
 * @param P snapshot
 * @param *OutputSnapshot pointer to the output filename (complete path)
 * @param NumFilesPerSnapshot number of files for distributed snapshots
 * @param SnapFormat format of Gadget snapshot
 */
void output_snapshot(const bool WriteSnapshot, const Snapshot P, const char *OutputSnapshot, const int NumFilesPerSnapshot, const int SnapFormat)
{
    /* Write output Snapshot */
    if(WriteSnapshot)
    {
        switch(SnapFormat)
        {
            case 1:
                write_snapshot(P, OutputSnapshot, NumFilesPerSnapshot, 1);
            break;
            case 2:
                write_snapshot(P, OutputSnapshot, NumFilesPerSnapshot, 2);
            break;
            case 3:
                write_snapshot(P, OutputSnapshot, NumFilesPerSnapshot, 3);
            break;
            default:
                FatalError("SnapFormat does not exist");
            break;
        }
    }
} //output_snapshot
///-------------------------------------------------------------------------------------
