///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/src/tweb.c
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file tweb.c
 *  \brief This codes classifies the cosmic web according to the T-web procedure
 * (Hahn, Porciani, Carollo & Dekel, MNRAS 375, 489 (2007), arXiv:astro-ph/0610280)
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2015-2023
 */
#include "tweb.h"
///-------------------------------------------------------------------------------------
static inline void calc_tidal_shear(const float_t *DENSITY, float_t *T00, float_t *T11, float_t *T22, float_t *T01, float_t *T02, float_t *T12, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2)
{
    shear_of_scalar(DENSITY, T00, T11, T22, T01, T02, T12, N0, N1, N2, L0, L1, L2);

    debugPrintFloattArray(T00,N0);
    debugPrintFloattArray(T11,N0);
    debugPrintFloattArray(T22,N0);
    debugPrintFloattArray(T01,N0);
    debugPrintFloattArray(T02,N0);
    debugPrintFloattArray(T12,N0);
} //calc_tidal_shear
///-------------------------------------------------------------------------------------
/** @fn tweb_analysis
 * This routine classifies cells with the T-web scheme
 * @param *DENSITY input: density field
 * @param *OBJECTS output: classification on the Eulerian grid
 * @param *LAMBDA1 output: eigenvalue of the tidal tensor
 * @param *LAMBDA2 output: eigenvalue of the tidal tensor
 * @param *LAMBDA3 output: eigenvalue of the tidal tensor
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 */
void tweb_analysis(const float_t *DENSITY, float_t *OBJECTS, float_t *LAMBDA1, float_t *LAMBDA2, float_t *LAMBDA3, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2)
{
    PrintMessage(3, "T-WEB: computing classification of the cosmic web...");
    INDENT;

    cellIndex_t N=N0*N1*N2;
    float_t *T00, *T11, *T22, *T01, *T02, *T12;
    T00 = (float_t *)p_malloc(N*sizeof(float_t), VARNAME(T00));
    T11 = (float_t *)p_malloc(N*sizeof(float_t), VARNAME(T11));
    T22 = (float_t *)p_malloc(N*sizeof(float_t), VARNAME(T22));
    T01 = (float_t *)p_malloc(N*sizeof(float_t), VARNAME(T01));
    T02 = (float_t *)p_malloc(N*sizeof(float_t), VARNAME(T02));
    T12 = (float_t *)p_malloc(N*sizeof(float_t), VARNAME(T12));

    // 1) calculate elements of shear of Lagrangian displacement field tensor
    calc_tidal_shear(DENSITY, T00, T11, T22, T01, T02, T12, N0, N1, N2, L0, L1, L2);

    // 2) calculate eigenvalues and classify objects
    cellIndex_t NBVOIDS=0, NBSHEETS=0, NBFILAMENTS=0, NBCLUSTERS=0;
    float_t EIGVALS[3];
    for(cellIndex_t mc=0; mc<N; mc++)
    {
        // 2 a) calculate eigenvalues from symmetric tensor
        get_eigvals_real(T00[mc], T11[mc], T22[mc], T01[mc], T02[mc], T12[mc], EIGVALS);

        // 2 b) classify object
        int OBJNR=classify_object(EIGVALS[0], EIGVALS[1], EIGVALS[2], 0.);

        // 2 c) count four web-types
        #if DEBUG
        switch(OBJNR)
        {
            case 0: // void
            NBVOIDS++;
            break;

            case 1: // sheet
            NBSHEETS++;
            break;

            case 2: // filament
            NBFILAMENTS++;
            break;

            case 3: // cluster
            NBCLUSTERS++;
            break;
        }
        #endif

        // 2 d) store web-type in 3d array
        OBJECTS[mc]=(float_t)OBJNR;

        // 2 e) store eigenvalues
        LAMBDA1[mc]=(float_t)EIGVALS[0];
        LAMBDA2[mc]=(float_t)EIGVALS[1];
        LAMBDA3[mc]=(float_t)EIGVALS[2];
    }

    debugPrintInt(NBVOIDS);
    debugPrintInt(NBSHEETS);
    debugPrintInt(NBFILAMENTS);
    debugPrintInt(NBCLUSTERS);

    p_free(T00, VARNAME(T00));
    p_free(T11, VARNAME(T11));
    p_free(T22, VARNAME(T22));
    p_free(T01, VARNAME(T01));
    p_free(T02, VARNAME(T02));
    p_free(T12, VARNAME(T12));
    UNINDENT;
    PrintMessage(3, "T-WEB: computing classification of the cosmic web done.");
} //tweb_analysis
///-------------------------------------------------------------------------------------
