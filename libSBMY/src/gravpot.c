///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/src/gravpot.c
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file gravpot.c
 *  \brief Routines for dealing with the gravitational potential
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2014-2023
 */
#include "gravpot.h"
///-------------------------------------------------------------------------------------
/** @fn get_gravitational_potential_periodic
 * This routine returns the gravitational potential on the grid,
 * does not replace the input density field
 * @param *density the density contrast on the grid
 * @param Npm0 particle mesh size x
 * @param Npm1 particle mesh size y
 * @param Npm2 particle mesh size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 * @param smooth smooth gravitational potential?
 * @param a the current time (scale factor)
 * @param Omega_m cosmological parameter
 */
float_t* get_gravitational_potential_periodic(float_t *density, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, const bool smooth, const double a, const double Omega_m)
{
    int Nthreads = omp_get_max_threads();
    sprintf(G__msg__, "Getting gravitational potential, periodic boundary conditions (using %d cores)...", Nthreads);
    PrintMessage(4, G__msg__);
    INDENT;

    const cellIndex_t N=Npm0*Npm1*Npm2;
    const int N_HC=Npm0*Npm1*(Npm2/2+1);
    float_t *Phi = NULL;

    // allocate memory
    Phi = (float_t*)p_fftw_malloc(N*sizeof(float_t), VARNAME(Phi));

    // transform density field to Fourier space
    fftw_complex_t *PHI; PHI = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(PHI));
    FFT_r2c_3d(Npm0, Npm1, Npm2, density, PHI, Nthreads);

    // mutiply by the Green function in Fourier space
    double normphi=3./2.*Omega_m/a; // physical constant
    multiply_by_dgf_laplace_DFT(PHI, Npm0, Npm1, Npm2, L0, L1, L2, smooth, normphi);

    // transform back to real space
    FFT_c2r_3d(Npm0, Npm1, Npm2, PHI, Phi, Nthreads);

    // free memory
    p_fftw_free(PHI, VARNAME(PHI));

    UNINDENT;
    sprintf(G__msg__, "Getting gravitational potential, periodic boundary conditions (using %d cores) done.", Nthreads);
    PrintMessage(4, G__msg__);

    return Phi;
} //get_gravitational_potential_periodic
///-------------------------------------------------------------------------------------
/** @fn get_gravitational_potential_derivatives_periodic
 * Computes the gravitational potential on the grid, and
 * also stores the derivatives of the gravitational potential
 * @param *density the density contrast on the grid
 * @param *phi the gravitational potential on the grid
 * @param *dphidx dphi/dx calculated in Fourier space
 * @param *dphidy dphi/dy calculated in Fourier space
 * @param *dphidz dphi/dz calculated in Fourier space
 * @param Npm0 particle mesh size x
 * @param Npm1 particle mesh size y
 * @param Npm2 particle mesh size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 * @param smooth smooth gravitational potential?
 * @param a the current time (scale factor)
 * @param Omega_m cosmological parameter
 */
void get_gravitational_potential_derivatives_periodic(float_t *density, float_t *phi, float_t *dphidx, float_t *dphidy, float_t *dphidz, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, const bool smooth, const double a, const double Omega_m)
{
    int Nthreads = omp_get_max_threads();
    sprintf(G__msg__, "Getting gravitational potential, periodic boundary conditions...");
    PrintMessage(4, G__msg__);
    INDENT;

    const int N_HC=Npm0*Npm1*(Npm2/2+1);
    const int Npm2_HC=(Npm2/2+1);

    // transform density field to Fourier space
    fftw_complex_t *PHI, *DPHIDX, *DPHIDY, *DPHIDZ;
    PHI = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(PHI));
    DPHIDX = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(DPHIDX));
    DPHIDY = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(DPHIDY));
    DPHIDZ = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(DPHIDZ));
    FFT_r2c_3d(Npm0, Npm1, Npm2, density, PHI, Nthreads);

    // mutiply by the Green function in Fourier space
    double normphi=3./2.*Omega_m/a; // physical constant

    #pragma omp parallel for schedule(static) collapse(3)
    for(int mf0=0; mf0<Npm0; mf0++)
        for(int mf1=0; mf1<Npm1; mf1++)
            for(int mf2=0; mf2<Npm2_HC; mf2++)
            {
                double k0,k1,k2;
                fourierMode_t mf = get_FourierMode_Id(mf0,mf1,mf2,Npm0,Npm1,Npm2);
                PHI[mf] *= (float_t)dgf_laplace_DFT(mf0,mf1,mf2, Npm0,Npm1,Npm2, L0,L1,L2, smooth, normphi);

                k0=get_kmode(mf0,Npm0,L0);
                k1=get_kmode(mf1,Npm1,L1);
                k2=get_kmode(mf2,Npm2,L2);
                DPHIDX[mf] = -k0 * im(PHI[mf]) + k0 * re(PHI[mf]) * I;
                DPHIDY[mf] = -k1 * im(PHI[mf]) + k1 * re(PHI[mf]) * I;
                DPHIDZ[mf] = -k2 * im(PHI[mf]) + k2 * re(PHI[mf]) * I;

                // put the Nyquist planes to zero by hand
                if(mf0 == Npm0/2)
                    DPHIDX[mf] = 0.;
                if(mf1 == Npm1/2)
                    DPHIDY[mf] = 0.;
                if(mf2 == Npm2/2)
                    DPHIDZ[mf] = 0.;
            }

    // transform back to real space
    FFT_c2r_3d(Npm0, Npm1, Npm2, PHI, phi, Nthreads);
    FFT_c2r_3d(Npm0, Npm1, Npm2, DPHIDX, dphidx, Nthreads);
    FFT_c2r_3d(Npm0, Npm1, Npm2, DPHIDY, dphidy, Nthreads);
    FFT_c2r_3d(Npm0, Npm1, Npm2, DPHIDZ, dphidz, Nthreads);

    // free memory
    p_fftw_free(PHI, VARNAME(PHI));
    p_fftw_free(DPHIDX, VARNAME(DPHIDX));
    p_fftw_free(DPHIDY, VARNAME(DPHIDY));
    p_fftw_free(DPHIDZ, VARNAME(DPHIDZ));

    UNINDENT;
    sprintf(G__msg__, "Getting gravitational potential, periodic boundary conditions done.");
    PrintMessage(4, G__msg__);
} //get_gravitational_potential_derivatives_periodic
///-------------------------------------------------------------------------------------
