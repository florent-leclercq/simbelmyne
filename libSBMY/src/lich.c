///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/src/lich.c
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file lich.c
 *  \brief This codes classifies the cosmic web according to the LICH procedure
 * (Leclercq et al. JCAP 6, 49 (2017), arXiv:1601.00093)
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2016-2023
 */
#include "lich.h"
///-------------------------------------------------------------------------------------
static inline void get_laginv(float_t R00, float_t R11, float_t R22, float_t R01, float_t R02, float_t R12, float_t R10, float_t R20, float_t R21, float_t LAGINV[3])
{
    // If we symmetrize R, then LICH is equivalent to DIVA
    //R10=R01;
    //R20=R02;
    //R21=R12;

    float_t THETA[3][3];
    float_t OMEGA[3][3];

    // equation 6 in Wang et al. 2014
    THETA[0][0]=R00; THETA[1][1]=R11; THETA[2][2]=R22;
    THETA[1][0]=THETA[0][1]=(R01+R10)/2.;
    THETA[2][0]=THETA[0][2]=(R02+R20)/2.;
    THETA[2][1]=THETA[1][2]=(R12+R21)/2.;

    OMEGA[0][0]=OMEGA[1][1]=OMEGA[2][2]=0.;
    OMEGA[1][0]=(R10-R01)/2.; OMEGA[0][1]=-OMEGA[1][0];
    OMEGA[2][0]=(R20-R02)/2.; OMEGA[0][2]=-OMEGA[2][0];
    OMEGA[1][2]=(R12-R21)/2.; OMEGA[2][1]=-OMEGA[1][2];

    // equation 4 in Wang et al. 2014
    float_t s1=0.;
    for(int i=0; i<3; i++)
        s1-=THETA[i][i];

    float_t s2=s1*s1;
    for(int i=0; i<3; i++)
        for(int j=0; j<3; j++)
            s2-=THETA[i][j]*THETA[j][i]+OMEGA[i][j]*OMEGA[j][i];
    s2/=2.;

    float_t s3=-s1*s1*s1+3.*s1*s2;
    for(int i=0; i<3; i++)
        for(int j=0; j<3; j++)
            for(int k=0; k<3; k++)
                s3-=THETA[i][j]*THETA[j][k]*THETA[k][i]-3.*OMEGA[i][j]*OMEGA[j][k]*THETA[k][i];
    s3/=3.;

    LAGINV[0]=s1;
    LAGINV[1]=s2;
    LAGINV[2]=s3;
} //get_laginv
///-------------------------------------------------------------------------------------
static inline int classify_object_lich(const float_t s1, const float_t s2, const float_t s3)
{
    int answer=-1;

    // equations in Appendix A of Wang et al. 2014

    if(s2 > s1*s1/3.) // the equation 27*s3*s3+(4*s1*s1*s1-18*s1*s2)*s3+(4*s2*s2*s2-s1*s1*s2*s2)=0 has no real solutions
    {
        // the flow is necessarily vortical
        // VORTICAL CLUSTER
        if(s1>=0. && 0.<=s3 && s3<=s1*s2)       answer=7;
        // VORTICAL FILAMENT
        else if(s1>=0. && s3<=0.)               answer=6;
        else if(s1<=0. && s3<=s1*s2)            answer=6;
        // VORTICAL SHEET
        else if(s1<=0. && s3>=0.)               answer=5;
        else if(s1>=0. && s3>=s1*s2)            answer=5;
        // VORTICAL VOID
        else if(s1<=0. && s1*s2<=s3 && s3<=0.)  answer=4;
    }
    else // the equation 27*s3*s3+(4*s1*s1*s1-18*s1*s2)*s3+(4*s2*s2*s2-s1*s1*s2*s2)=0 has real solutions
    {
        float_t a=27;
        float_t b=4*s1*s1*s1-18*s1*s2;
        float_t c=4*s2*s2*s2-s1*s1*s2*s2;
        float_t RESULTS[2];
        solve_quadratic(a,b,c,RESULTS);
        float_t s3a=RESULTS[0];
        float_t s3b=RESULTS[1];

        // cases where the flow is potential
        // POTENTIAL VOID
        if(s1>=0. && 0.<=s2 && max(s3a,0.)<=s3 && s3<=s3b)      answer=3;
        // POTENTIAL SHEET
        else if(s1>=0. && s2<=s1*s1/4. && s3a<=s3 && s3<=0)     answer=2;
        else if(s1<=0. && s2<=0. && s3a<=s3 && s3<=0.)          answer=2;
        // POTENTIAL FILAMENT
        else if(s1<=0. && s2<=s1*s1/4. && 0.<=s3 && s3<=s3b)    answer=1;
        else if(s1>=0. && s2<=0. && 0.<=s3 && s3<=s3b)          answer=1;
        // POTENTIAL CLUSTER
        else if(s1<=0. && 0.<=s2 && s3a<=s3 && s3<=min(s3b,0.)) answer=0;

        // cases where the flow is vortical
        // VORTICAL CLUSTER
        else if(s1>=0. && s1*s1/4.<=s2 && 0.<=s3 && s3<=s3a)    answer=7;
        else if(s1>=0. && 0.<=s2 && s3b<=s3 && s3<=s1*s2)       answer=7;
        // VORTICAL FILAMENT
        else if(s1>=0. && s2>=s1*s1/4. && s3<=0.)               answer=6;
        else if(s1>=0. && s2<=s1*s1/4. && s3<=s3a)              answer=6;
        else if(s1<=0. && s2>=0. && s3<=s1*s2)                  answer=6;
        else if(s1<=0. && s2<=0. && s3<=s3a)                    answer=6;
        // VORTICAL SHEET
        else if(s1<=0. && s2>=s1*s1/4. && s3>=0.)               answer=5;
        else if(s1<=0. && s2<=s1*s1/4. && s3>=s3b)              answer=5;
        else if(s1>=0. && s2>=0. && s3>=s1*s2)                  answer=5;
        else if(s1>=0. && s2<=0. && s3>=s3b)                    answer=5;
        // VORTICAL VOID
        else if(s1<=0. && s1*s1/4.<=s2 && s3b<=s3 && s3<=0.)    answer=4;
        else if(s1<=0. && 0.<=s2 && s1*s2<=s3 && s3<=s3a)       answer=4;
    }

    if(answer==-1)
    {
        debugPrintDouble(s1);debugPrintDouble(s2);debugPrintDouble(s3);
        FatalError("LICH: No structure type was found.");
    }
    return answer;
} //classify_object_lich
///-------------------------------------------------------------------------------------
/** @fn lich_analysis
 * This routine classifies particles with the LICH scheme
 * @param *PSI0 input: displacement field
 * @param *PSI1 input: displacement field
 * @param *PSI2 input: displacement field
 * @param *OBJECTS output: classification of particles
 * @param *S1 output: first Lagrangian invariant
 * @param *S2 output: second Lagrangian invariant
 * @param *S3 output: third Lagrangian invariant
 * @param Np0 number of particles x
 * @param Np1 number of particles y
 * @param Np2 number of particles z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 */
void lich_analysis(const float_t *PSI0, const float_t *PSI1, const float_t *PSI2, float_t *OBJECTS, float_t *S1, float_t *S2, float_t *S3, const int Np0, const int Np1, const int Np2, const double L0, const double L1, const double L2)
{
    PrintMessage(3, "LICH: computing classification of the cosmic web...");
    INDENT;

    particleID_t Np=(particleID_t)Np0*Np1*Np2;
    float_t *R00, *R11, *R22, *R01, *R02, *R12, *R10, *R20, *R21;
    R00 = (float_t *)p_malloc(Np*sizeof(float_t), VARNAME(R00));
    R11 = (float_t *)p_malloc(Np*sizeof(float_t), VARNAME(R11));
    R22 = (float_t *)p_malloc(Np*sizeof(float_t), VARNAME(R22));
    R01 = (float_t *)p_malloc(Np*sizeof(float_t), VARNAME(R01));
    R02 = (float_t *)p_malloc(Np*sizeof(float_t), VARNAME(R02));
    R12 = (float_t *)p_malloc(Np*sizeof(float_t), VARNAME(R12));
    R10 = (float_t *)p_malloc(Np*sizeof(float_t), VARNAME(R10));
    R20 = (float_t *)p_malloc(Np*sizeof(float_t), VARNAME(R20));
    R21 = (float_t *)p_malloc(Np*sizeof(float_t), VARNAME(R21));
    bool symmetric = false;

    // 1) calculate elements of shear of Lagrangian displacement field tensor
    get_shearPsi(PSI0, PSI1, PSI2, R00, R11, R22, R01, R02, R12, R10, R20, R21, symmetric, Np0, Np1, Np2, L0, L1, L2);

    // 2) calculate eigenvalues and classify objects
    cellIndex_t NBVOIDS=0, NBSHEETS=0, NBFILAMENTS=0, NBCLUSTERS=0;
    #pragma omp parallel for
    for(particleID_t mp=0; mp<Np; mp++)
    {
        // 2 a) calculate the Lagrangian invariants from the shear of the displacement field
        float_t LAGINV[3];
        get_laginv(R00[mp], R11[mp], R22[mp], R01[mp], R02[mp], R12[mp], R10[mp], R20[mp], R21[mp], LAGINV);

        // 2 b) classify object
        int OBJNR=classify_object_lich(LAGINV[0], LAGINV[1], LAGINV[2]);

        // 2 c) count four web-types
        #if DEBUG
        switch(OBJNR)
        {
            case 0: // void
            case 4:
            #pragma omp atomic
            NBVOIDS++;
            break;

            case 1: // sheet
            case 5:
            #pragma omp atomic
            NBSHEETS++;
            break;

            case 2: // filament
            case 6:
            #pragma omp atomic
            NBFILAMENTS++;
            break;

            case 3: // cluster
            case 7:
            #pragma omp atomic
            NBCLUSTERS++;
            break;
        }
        #endif

        // 2 d) store web-type in 3d array
        OBJECTS[mp]=(float_t)OBJNR;

        // 2 e) store Lagrangian invariants
        S1[mp]=LAGINV[0];
        S2[mp]=LAGINV[1];
        S3[mp]=LAGINV[2];
    }
    debugPrintInt(NBVOIDS);
    debugPrintInt(NBSHEETS);
    debugPrintInt(NBFILAMENTS);
    debugPrintInt(NBCLUSTERS);

    p_free(R00, VARNAME(R00));
    p_free(R11, VARNAME(R11));
    p_free(R22, VARNAME(R22));
    p_free(R01, VARNAME(R01));
    p_free(R02, VARNAME(R02));
    p_free(R12, VARNAME(R12));
    p_free(R10, VARNAME(R10));
    p_free(R20, VARNAME(R20));
    p_free(R21, VARNAME(R21));

    UNINDENT;
    PrintMessage(3, "LICH: computing classification of the cosmic web done.");
} //lich_analysis
///-------------------------------------------------------------------------------------
/** @fn lich_structures_l2e
 * Transform LICH structure classification from Lagrangian to Eulerian coordinates
 * @param OBJECTS classification of particles
 * @param P Snapshot containing particle information
 * @param POTENTIAL_VOIDS output: pdf for potential voids in Eulerian coordinates
 * @param POTENTIAL_SHEETS output: pdf for potential sheets in Eulerian coordinates
 * @param POTENTIAL_FILAMENTS output: pdf for potential filaments in Eulerian coordinates
 * @param POTENTIAL_CLUSTERS output: pdf for potential clusters in Eulerian coordinates
 * @param VORTICAL_VOIDS output: pdf for vortical voids in Eulerian coordinates
 * @param VORTICAL_SHEETS output: pdf for vortical sheets in Eulerian coordinates
 * @param VORTICAL_FILAMENTS output: pdf for vortical filaments in Eulerian coordinates
 * @param VORTICAL_CLUSTERS output: pdf for vortical clusters in Eulerian coordinates
 * @param Np number of particles
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 */
void lich_structures_l2e(const float_t *OBJECTS, const Snapshot P, float_t *POTENTIAL_VOIDS, float_t *POTENTIAL_SHEETS, float_t *POTENTIAL_FILAMENTS, float_t *POTENTIAL_CLUSTERS, float_t *VORTICAL_VOIDS, float_t *VORTICAL_SHEETS, float_t *VORTICAL_FILAMENTS, float_t *VORTICAL_CLUSTERS, const particleID_t Np, const int N0, const int N1, const int N2)
{
    PrintMessage(3, "Translating to Eulerian coordinates...");
    INDENT;

    cellIndex_t N=N0*N1*N2;
    double L0,L1,L2; L0=L1=L2=P.header.BoxSize;
    double d0=L0/N0, d1=L1/N1, d2=L2/N2;
    float_t *DENSITY;
    DENSITY = (float_t *)p_malloc(N*sizeof(float_t), VARNAME(DENSITY));

    // Initialize fields
    #pragma omp parallel for
    for(cellIndex_t mc=0; mc<N; mc++)
    {
        POTENTIAL_VOIDS[mc]=0.;
        POTENTIAL_SHEETS[mc]=0.;
        POTENTIAL_FILAMENTS[mc]=0.;
        POTENTIAL_CLUSTERS[mc]=0.;
        VORTICAL_VOIDS[mc]=0.;
        VORTICAL_SHEETS[mc]=0.;
        VORTICAL_FILAMENTS[mc]=0.;
        VORTICAL_CLUSTERS[mc]=0.;
        DENSITY[mc]=0.;
    }

    for(particleID_t mp=0; mp<Np; mp++)
    {
        int OBJNR = OBJECTS[mp]; int mc0min,mc0max,mc1min,mc1max,mc2min,mc2max;
        if(OBJNR==0) particle_to_mesh_periodic(P.positions[mp].Pos[0],P.positions[mp].Pos[1],P.positions[mp].Pos[2],1.,POTENTIAL_VOIDS,N0,N1,N2,d0,d1,d2,&mc0min,&mc0max,&mc1min,&mc1max,&mc2min,&mc2max);
        if(OBJNR==1) particle_to_mesh_periodic(P.positions[mp].Pos[0],P.positions[mp].Pos[1],P.positions[mp].Pos[2],1.,POTENTIAL_SHEETS,N0,N1,N2,d0,d1,d2,&mc0min,&mc0max,&mc1min,&mc1max,&mc2min,&mc2max);
        if(OBJNR==2) particle_to_mesh_periodic(P.positions[mp].Pos[0],P.positions[mp].Pos[1],P.positions[mp].Pos[2],1.,POTENTIAL_FILAMENTS,N0,N1,N2,d0,d1,d2,&mc0min,&mc0max,&mc1min,&mc1max,&mc2min,&mc2max);
        if(OBJNR==3) particle_to_mesh_periodic(P.positions[mp].Pos[0],P.positions[mp].Pos[1],P.positions[mp].Pos[2],1.,POTENTIAL_CLUSTERS,N0,N1,N2,d0,d1,d2,&mc0min,&mc0max,&mc1min,&mc1max,&mc2min,&mc2max);
        if(OBJNR==4) particle_to_mesh_periodic(P.positions[mp].Pos[0],P.positions[mp].Pos[1],P.positions[mp].Pos[2],1.,VORTICAL_VOIDS,N0,N1,N2,d0,d1,d2,&mc0min,&mc0max,&mc1min,&mc1max,&mc2min,&mc2max);
        if(OBJNR==5) particle_to_mesh_periodic(P.positions[mp].Pos[0],P.positions[mp].Pos[1],P.positions[mp].Pos[2],1.,VORTICAL_SHEETS,N0,N1,N2,d0,d1,d2,&mc0min,&mc0max,&mc1min,&mc1max,&mc2min,&mc2max);
        if(OBJNR==6) particle_to_mesh_periodic(P.positions[mp].Pos[0],P.positions[mp].Pos[1],P.positions[mp].Pos[2],1.,VORTICAL_FILAMENTS,N0,N1,N2,d0,d1,d2,&mc0min,&mc0max,&mc1min,&mc1max,&mc2min,&mc2max);
        if(OBJNR==7) particle_to_mesh_periodic(P.positions[mp].Pos[0],P.positions[mp].Pos[1],P.positions[mp].Pos[2],1.,VORTICAL_CLUSTERS,N0,N1,N2,d0,d1,d2,&mc0min,&mc0max,&mc1min,&mc1max,&mc2min,&mc2max);
        particle_to_mesh_periodic(P.positions[mp].Pos[0],P.positions[mp].Pos[1],P.positions[mp].Pos[2],1.,DENSITY,N0,N1,N2,d0,d1,d2,&mc0min,&mc0max,&mc1min,&mc1max,&mc2min,&mc2max);
    }

    #pragma omp parallel for
    for(cellIndex_t mc=0; mc<N; mc++)
    {
        if(DENSITY[mc]>0)
        {
            POTENTIAL_VOIDS[mc]/=DENSITY[mc];
            POTENTIAL_SHEETS[mc]/=DENSITY[mc];
            POTENTIAL_FILAMENTS[mc]/=DENSITY[mc];
            POTENTIAL_CLUSTERS[mc]/=DENSITY[mc];
            VORTICAL_VOIDS[mc]/=DENSITY[mc];
            VORTICAL_SHEETS[mc]/=DENSITY[mc];
            VORTICAL_FILAMENTS[mc]/=DENSITY[mc];
            VORTICAL_CLUSTERS[mc]/=DENSITY[mc];
        }
    }

    p_free(DENSITY, VARNAME(DENSITY));
    UNINDENT;
    PrintMessage(3, "Translating to Eulerian coordinates done.");
} // lich_structures_l2e
///-------------------------------------------------------------------------------------
