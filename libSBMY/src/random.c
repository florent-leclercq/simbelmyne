///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/src/random.c
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file random.h
 *  \brief Routines for random number generation
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2016-2023
 */
#include "random.h"
///-------------------------------------------------------------------------------------
/** @fn reset_rng
 * Resets a random number generator with to the specified state
 * @param *fname state of rng
 * @param *gBaseRand rng
 */
void reset_rng(const char *fname, gsl_rng *gBaseRand)
{
    FILE *f = NULL;
    if( (f = fopen(fname, "rb")) == NULL )
    {
        sprintf(G__msg__, "Reading error with file '%s'", fname);
        FatalError(G__msg__);
    }

    int err=gsl_rng_fread(f, gBaseRand);
    if(err !=0)
    {
        sprintf(G__msg__, "Failure reading rng file '%s'", fname);
        FatalError(G__msg__);
    }
    fclose(f);
} //reset_rng
///-------------------------------------------------------------------------------------
/** @fn set_rng
 * Sets the state of a random number generator
 * @param *random_generator random number generator
 * @param *InputRngState path to the file where the state is stored, or "0"
 */
void set_rng(gsl_rng *random_generator, const char *InputRngState)
{
    // Set the random generator to the specified state
    if(strcmp(InputRngState,"0") != 0)
    {
        sprintf(G__msg__, "Resetting random number generator to state '%s'...", InputRngState);
        PrintMessage(4, G__msg__);

        if(exists(InputRngState))
            reset_rng(InputRngState, random_generator);
        else
        {
            sprintf(G__msg__, "File '%s' does not exist.", InputRngState);
            FatalError(G__msg__);
        }

        sprintf(G__msg__, "Resetting random number generator to state '%s' done.", InputRngState);
        PrintMessage(4, G__msg__);
    }
    // Reset the random generator
    else
    {
        sprintf(G__msg__, "Resetting random number generator...");
        PrintMessage(4, G__msg__);

        gsl_rng_set(random_generator, (unsigned long int)123456);

        sprintf(G__msg__, "Resetting random number generator done.");
        PrintMessage(4, G__msg__);
    }
} //set_rng
///-------------------------------------------------------------------------------------
/** @fn save_rng_state
 * Saves the state of a random number generator
 * @param *random_generator random number generator
 * @param *OutputRngState path to the file where the state should be stored
 */
void save_rng_state(gsl_rng *random_generator, const char *OutputRngState)
{
    sprintf(G__msg__, "Saving random number generator state to '%s'...", OutputRngState);
    PrintMessage(4, G__msg__);

    // Save the random generator state to a binary file
    FILE *f = NULL;
    if( (f = fopen(OutputRngState, "wb")) == NULL )
    {
        sprintf(G__msg__, "Reading error with file '%s'", OutputRngState);
        FatalError(G__msg__);
    }
    gsl_rng_fwrite(f, random_generator);
    fclose(f);

    sprintf(G__msg__, "Saving random number generator state to '%s' done.", OutputRngState);
    PrintMessage(4, G__msg__);
} //save_rng_state
///-------------------------------------------------------------------------------------
/** @fn generate_seed_table_
 * This routine generates a seed table for generation of random numbers in parallel
 * @param *random_generator random number generator
 * @param length size of the seed table to generate
 * @param *name output variable name
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 * @return pointer to the array containing the seed table
 */
uint64_t* generate_seed_table_(gsl_rng *random_generator, const int length, char *name, char *file, char *func, int line)
{
    sprintf(G__msg__, "Generating seed table...");
    PrintMessage(4, G__msg__);

    uint64_t *seedtable = NULL;
    // Allocates memory
    seedtable = (uint64_t*)p_malloc_(length*sizeof(uint64_t), name, file, func, line);

    // Fills the seed table
    for(int n=0; n<length; n++)
        seedtable[n] = (uint64_t)(UINT64_MAX * gsl_rng_uniform(random_generator));
        //UINT64_MAX is the maximum value for a 64-bits unsigned integer

    debugPrintUInt64Array(seedtable,length);

    sprintf(G__msg__, "Generating seed table done.");
    PrintMessage(4, G__msg__);

    return seedtable;
} //generate_seed_table_
///-------------------------------------------------------------------------------------
/** @fn seed_table_from_rngstate_
 * This routine generates a seed table for generation of random numbers in parallel
 * @param *InputRngState input random number generator state
 * @param length size of the seed table to generate
 * @param *OutputRngState output random number generator state at the end
 * @return pointer to the array containing the seed table
 */
uint64_t* seed_table_from_rngstate_(const char *InputRngState, const int length, const char *OutputRngState, char *name, char *file, char *func, int line)
{
    // Set a random generator
    gsl_rng *random_generator;
    random_generator = gsl_rng_alloc(gsl_rng_ranlxd2);
    set_rng(random_generator, InputRngState);

    // Generate the seed table
    uint64_t *seedtable;
    seedtable = generate_seed_table_(random_generator, length, name, file, func, line);

    // Save the random number generator state
    save_rng_state(random_generator, OutputRngState);

    // Free the random generator
    gsl_rng_free(random_generator);

    return seedtable;
} //seed_table_from_rngstate_
///-------------------------------------------------------------------------------------
