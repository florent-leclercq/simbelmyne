///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/src/correlations.c
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file correlations.c
 *  \brief Routines for computing correlation functions
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2014-2023
 */
#include "correlations.h"
///-------------------------------------------------------------------------------------
/** @fn aux_get_crosscorrelation_fs
 * Subroutine to get auto/cross-correlations in Fourier space
 */
static void aux_get_crosscorrelation_fs(const float_t *A1, const float_t *A2, const int NUM_MODES, const int *k_keys, const int k_nmodes[NUM_MODES], float_t P12k[NUM_MODES], float_t V12k[NUM_MODES], float_t Rk[NUM_MODES], float_t VRk[NUM_MODES], const int N0, const int N1, const int N2, const double L0, const double L1, const double L2, const bool AliasingCorr)
{
    const int Nthreads = omp_get_max_threads();
    const cellIndex_t N=N0*N1*N2;
    const int N2_HC=(N2/2+1); // to use only half of the Fourier space
    const int N_HC=N0*N1*N2_HC;

    // 1) transform field to Fourier space
    fftw_complex_t *A1_FS; A1_FS = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(A1_FS));
    fftw_complex_t *A2_FS; A2_FS = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(A2_FS));
    FFT_r2c_3d(N0, N1, N2, (float_t*)A1, A1_FS, Nthreads);
    FFT_r2c_3d(N0, N1, N2, (float_t*)A2, A2_FS, Nthreads);

    // 2) setup variables
    const double knyq = get_knyq(N0,N1,N2,L0,L1,L2);
    double P1k[NUM_MODES], P2k[NUM_MODES];
    double V1k[NUM_MODES], V2k[NUM_MODES];

    // 3) initialize arrays
    double AUX_P12k[Nthreads][NUM_MODES], AUX_P1k[Nthreads][NUM_MODES], AUX_P2k[Nthreads][NUM_MODES]; bool threadActivated[Nthreads];
    for(int t=0; t<Nthreads; t++)
        threadActivated[t]=false;
    #pragma omp parallel for schedule(static)
    for(int ik=0; ik<NUM_MODES; ik++)
    {
        P12k[ik]=0.;
        P1k[ik]=0.;
        P2k[ik]=0.;
        Rk[ik]=0.;
        V12k[ik]=0.;
        V1k[ik]=0.;
        V2k[ik]=0.;
        VRk[ik]=0.;
        for(int t=0; t<Nthreads; t++)
        {
            AUX_P12k[t][ik]=0.;
            AUX_P1k[t][ik]=0.;
            AUX_P2k[t][ik]=0.;
        }
    }

    // 4) calculate cross-correlation
    #pragma omp parallel
    {
        int t = omp_get_thread_num(); // get rank of current process
        threadActivated[t] = true;

        #pragma omp for schedule(static) collapse(3)
        for(int mf0=0; mf0<N0; mf0++)
            for(int mf1=0; mf1<N1; mf1++)
                for(int mf2=0; mf2<N2_HC; mf2++)
                {
                    double kmod,AliasingCorrFactor,A1_Re,A1_Im,A2_Re,A2_Im; int ik; fourierMode_t mf;

                    // calculate modulus of current k vector and look-up index in k_keys
                    mf = get_FourierMode_Id(mf0,mf1,mf2,N0,N1,N2);
                    kmod = (float_t)get_kmodulus(mf0,mf1,mf2,N0,L1,N2,L0,L1,L2);
                    ik = k_keys[mf];

                    // correct for aliasing
                    AliasingCorrFactor=1.;
                    if(AliasingCorr)
                    {
                        // correction for aliasing, formula (21) from Jing (2005), ApJ 620, 559
                        // assume isotropic aliasing (approx. true for k<kmax=knyquist)
                        #if ASSIGNMENT == NGP
                        UNUSED(kmod); UNUSED(knyq);
                        AliasingCorrFactor = 1.;
                        #elif ASSIGNMENT == CIC
                        AliasingCorrFactor = (1. - 2./3.*sin(M_PI/2.*kmod/knyq)*sin(M_PI/2.*kmod/knyq));
                        #elif ASSIGNMENT == TSC
                        AliasingCorrFactor = (1. - sin(M_PI/2.*kmod/knyq)*sin(M_PI/2.*kmod/knyq) + 2./15.* sin(M_PI/2.*kmod/knyq)*sin(M_PI/2.*kmod/knyq)*sin(M_PI/2.*kmod/knyq)*sin(M_PI/2.*kmod/knyq))
                        #endif
                    }

                    // calculate cross-correlation
                    // P12k = 1/2(<A_1 A_2*> + <A_1* A_2>) =  Re(<A_1 A_2*>)
                    if(ik>-1 && ik<NUM_MODES)
                    {
                        A1_Re=re(A1_FS[mf]);
                        A1_Im=im(A1_FS[mf]);
                        A2_Re=re(A2_FS[mf]);
                        A2_Im=im(A2_FS[mf]);
                        AUX_P12k[t][ik]+=(A1_Re*A2_Re+A1_Im*A2_Im)/AliasingCorrFactor;
                        AUX_P1k[t][ik]+=(A1_Re*A1_Re+A1_Im*A1_Im)/AliasingCorrFactor;
                        AUX_P2k[t][ik]+=(A2_Re*A2_Re+A2_Im*A2_Im)/AliasingCorrFactor;
                    }
                }
    }

    // 5) perform the reduction with a parallel loop on the number of modes
    // since two threads never touch the same mode, no atomic instruction is needed
    #pragma omp for schedule(static)
    for(int ik=0; ik<NUM_MODES; ik++)
        for(int t=0; t<Nthreads; t++)
        {
            if (!threadActivated[t])
                continue;
            P12k[ik]+=AUX_P12k[t][ik];
            P2k[ik]+=AUX_P1k[t][ik];
            P1k[ik]+=AUX_P2k[t][ik];
        }

    // 6) normalize
    const double NORM=(double)N * ((double)N0/L0)*((double)N1/L1)*((double)N2/L2);
    #pragma omp parallel for schedule(static)
    for(int ik=0; ik<NUM_MODES; ik++)
    {
        // Assumes an inverse-Gamma distribution with shape alpha_Γ=alpha-1+k_nmodes[ik]/2 and scale beta_Γ=|delta(k)|^2/NORM/2
        // alpha from the prior: alpha=0 for a flat prior, alpha=1 for Jeyffrey's prior (see Jasche, Kitaura, Wandelt, Ensslin 2010)
        // In the following we adopt alpha=1
        // The mean is beta_Γ/(alpha_Γ-1)
        if(k_nmodes[ik]>2)
        {
            P12k[ik]/=(double)(k_nmodes[ik]-2)*NORM;
            P1k[ik]/=(double)(k_nmodes[ik]-2)*NORM;
            P2k[ik]/=(double)(k_nmodes[ik]-2)*NORM;
            Rk[ik]=P12k[ik]/sqrt(P1k[ik]*P2k[ik]);
        }
        // The variance is beta_Γ^2/((alpha_Γ-1)^2*(alpha_Γ-2)) = Pk^2/(k_nmodes[ik]-1)
        // Then we propagate uncertainties to R
        if(k_nmodes[ik]>4)
        {
            V12k[ik]=P12k[ik]*P12k[ik]/((double)(k_nmodes[ik]/2.-2.));
            V1k[ik]=P1k[ik]*P1k[ik]/((double)(k_nmodes[ik]/2.-2.));
            V2k[ik]=P2k[ik]*P2k[ik]/((double)(k_nmodes[ik]/2.-2.));
            VRk[ik]=V12k[ik]/sqrt(P1k[ik]*P2k[ik]) + 1./2.*Rk[ik]*(V1k[ik]/P1k[ik] + V2k[ik]/P2k[ik]);
        }
    }

    // 7) free memory
    p_fftw_free(A1_FS, VARNAME(A1_FS));
    p_fftw_free(A2_FS, VARNAME(A2_FS));
} //aux_get_crosscorrelation_fs
///-------------------------------------------------------------------------------------
/** @fn get_crosscorrelation_fs
 * Gets cross-correlation of fields A1 and A2 in Fourier space
 * Assumes that A1 and A2 have vanishing means
 * @param *A1 pointer to A1
 * @param *A2 pointer to A2
 * @param NUM_MODES number of k bins
 * @param *k_keys input array containing keys to the power spectrum array
 * @param *k_nmodes input array containing the number of modes in each k bin
 * @param *P12k output cross-correlation between A1 and A2: mean
 * @param *V12k output cross-correlation between A1 and A2: variance
 * @param *Rk output cross-correlation coefficient between A1 and A2: P12k/sqrt(P1k*P2k): mean
 * @param *VRk output cross-correlation coefficient between A1 and A2: P12k/sqrt(P1k*P2k): variance
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param L0 size of the box x
 * @param L1 size of the box y
 * @param L2 size of the box z
 * @param AliasingCorr boolean: use Jing's correction factor: Jing (2005), ApJ 620, 559
 * assumes isotropic aliasing (approx. true for k < kmax=knyquist)
 * the formula is for CIC interpolation scheme, which we assume
 */
void get_crosscorrelation_fs(const float_t *A1, const float_t *A2, const int NUM_MODES, const int *k_keys, const int k_nmodes[NUM_MODES], float_t P12k[NUM_MODES], float_t V12k[NUM_MODES], float_t Rk[NUM_MODES], float_t VRk[NUM_MODES], const int N0, const int N1, const int N2, const double L0, const double L1, const double L2, const bool AliasingCorr)
{
    int Nthreads = omp_get_max_threads();
    sprintf(G__msg__, "Getting cross-correlation in Fourier space (using %d cores)...", Nthreads);
    PrintMessage(4, G__msg__);

    aux_get_crosscorrelation_fs(A1, A2, NUM_MODES, k_keys, k_nmodes, P12k, V12k, Rk, VRk, N0, N1, N2, L0, L1, L2, AliasingCorr);

    sprintf(G__msg__, "Getting cross-correlation in Fourier space (using %d cores) done.", Nthreads);
    PrintMessage(4, G__msg__);
} //get_crosscorrelation_fs
///-------------------------------------------------------------------------------------
/** @fn get_autocorrelation_fs
 * Gets auto-correlation of a quantity A in Fourier space
 * Assumes that A has vanishing mean
 * @param *A pointer to A
 * @param NUM_MODES number of k bins
 * @param *k_keys input array containing keys to the power spectrum array
 * @param *k_nmodes input array containing the number of modes in each k bin
 * @param *Pk output auto-correlation of A: mean
 * @param *Vk output auto-correlation of A: variance
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param L0 size of the box x
 * @param L1 size of the box y
 * @param L2 size of the box z
 * @param AliasingCorr boolean: use Jing's correction factor: Jing (2005), ApJ 620, 559
 * assumes isotropic aliasing (approx. true for k < kmax=knyquist)
 * the formula is for CIC interpolation scheme, which we assume
 */
void get_autocorrelation_fs(const float_t *A, const int NUM_MODES, const int *k_keys, const int k_nmodes[NUM_MODES], float_t Pk[NUM_MODES], float_t Vk[NUM_MODES], const int N0, const int N1, const int N2, const double L0, const double L1, const double L2, const bool AliasingCorr)
{
    int Nthreads = omp_get_max_threads();
    sprintf(G__msg__, "Getting auto-correlation in Fourier space (using %d cores)...", Nthreads);
    PrintMessage(4, G__msg__);

    float_t Rk[NUM_MODES], VRk[NUM_MODES];
    aux_get_crosscorrelation_fs(A, A, NUM_MODES, k_keys, k_nmodes, Pk, Vk, Rk, VRk, N0, N1, N2, L0, L1, L2, AliasingCorr);

    sprintf(G__msg__, "Getting auto-correlation in Fourier space (using %d cores) done.", Nthreads);
    PrintMessage(4, G__msg__);
} //get_autocorrelation_fs
///-------------------------------------------------------------------------------------
