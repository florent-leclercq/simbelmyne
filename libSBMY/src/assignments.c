///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/src/assignments.c
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file assignments.c
 *  \brief Routines for NGP/CiC/TSC assignments and interpolation
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2014-2023
 */
#include "assignments.h"
///-------------------------------------------------------------------------------------
/* -------------------------------------------------------------------------------------
 * ------------------------------------ NGP --------------------------------------------
 * -------------------------------------------------------------------------------------
 */
///-------------------------------------------------------------------------------------
/** @fn aux_particle_to_mesh_ngp_periodic
 * Auxiliary routine for NGP scheme.
 * Assumes periodic boundary conditions
 */
cellIndex_t aux_particle_to_mesh_ngp_periodic(const float_t x0, const float_t x1, const float_t x2, const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, int *mc0, int *mc1, int *mc2)
{
    // get the nearest grid point
    int i = (int)(floor(x0/d0));
    int j = (int)(floor(x1/d1));
    int k = (int)(floor(x2/d2));

    // periodic wrap
    int ip = (int)p_mod(i,N0); *mc0 = ip;
    int jp = (int)p_mod(j,N1); *mc1 = jp;
    int kp = (int)p_mod(k,N2); *mc2 = kp;

    // maximum and minimum indices updated
    cellIndex_t mc = get_index(ip,jp,kp,N0,N1,N2);

    return mc;
} //aux_particle_to_mesh_ngp_periodic
///-------------------------------------------------------------------------------------
/** @fn particle_to_mesh_ngp
 * Assign particle to grid, NGP scheme
 * @param x0 particle position x (code units)
 * @param x1 particle position y (code units)
 * @param x2 particle position z (code units)
 * @param weight particle weight
 * @param *A pointer to array representing the grid
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param d0 conversion factor position to grid x
 * @param d1 conversion factor position to grid y
 * @param d2 conversion factor position to grid z
 * @param boundary_conditions boundary conditions to use
 * @param *mc0min minimum x index (used for parallelization)
 * @param *mc0max maximum x index (used for parallelization)
 * @param *mc1min minimum y index (used for parallelization)
 * @param *mc1max maximum y index (used for parallelization)
 * @param *mc2min minimum z index (used for parallelization)
 * @param *mc2max maximum z index (used for parallelization)
 */
void particle_to_mesh_ngp(const float_t x0, const float_t x1, const float_t x2, const double weight, float_t *A, const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, const int boundary_conditions, int *mc0min, int *mc0max, int *mc1min, int *mc1max, int *mc2min, int *mc2max)
{
    const bool p=(boundary_conditions==1); // periodic boundary conditions
    int i,j,k, ip,jp,kp;
    cellIndex_t mc;
    bool mc_in;

    // get the nearest grid point
    i = (int)(floor(x0/d0));
    j = (int)(floor(x1/d1));
    k = (int)(floor(x2/d2));

    // indices for periodic boundary conditions
    if(p)
    {
        ip = (int)p_mod(i,N0);
        jp = (int)p_mod(j,N1);
        kp = (int)p_mod(k,N2);
    }

    // checks to access values for non-periodic boundary conditions
    mc_in = !(i<0 || i>=N0) && !(j<0 || j>=N1) && !(k<0 || k>=N2);

    // minimum/maximum indices updated
    int mc0min_current=N0-1, mc1min_current=N1-1, mc2min_current=N2-1;
    int mc0max_current=0, mc1max_current=0, mc2max_current=0;

    // assign particle to mesh
    if(mc_in || p)
    {
        mc = mc_in ? get_index(i,j,k,N0,N1,N2) : get_index(ip,jp,kp,N0,N1,N2);
        mc0min_current= mc_in ? min(i,mc0min_current) : min(ip,mc0min_current);
        mc0max_current= mc_in ? max(i,mc0max_current) : max(ip,mc0max_current);
        mc1min_current= mc_in ? min(j,mc1min_current) : min(jp,mc1min_current);
        mc1max_current= mc_in ? max(j,mc1max_current) : max(jp,mc1max_current);
        mc2min_current= mc_in ? min(k,mc2min_current) : min(kp,mc2min_current);
        mc2max_current= mc_in ? max(k,mc2max_current) : max(kp,mc2max_current);
        A[mc] += weight;
    }
    *mc0min = min(mc0min_current,mc0max_current);
    *mc0max = max(mc0min_current,mc0max_current);
    *mc1min = min(mc1min_current,mc1max_current);
    *mc1max = max(mc1min_current,mc1max_current);
    *mc2min = min(mc2min_current,mc2max_current);
    *mc2max = max(mc2min_current,mc2max_current);
} //particle_to_mesh_ngp
///-------------------------------------------------------------------------------------
/** @fn mesh_to_particle_ngp
 * Assign grid value to particle, NGP scheme
 * @param *A pointer to array representing the grid
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param d0 conversion factor position to grid x
 * @param d1 conversion factor position to grid y
 * @param d2 conversion factor position to grid z
 * @param x0 particle position x (code units)
 * @param x1 particle position y (code units)
 * @param x2 particle position z (code units)
 * @param weight particle weight
 * @param boundary_conditions boundary conditions to use
 * @param N0_pad number of additional cells to the East
 * @param N1_pad number of additional cells to the North
 * @param N2_pad number of additional cells to the Top
 */
float_t mesh_to_particle_ngp(const float_t *A, const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, const float_t x0, const float_t x1, const float_t x2, const double weight, const int boundary_conditions, const int N0_pad, const int N1_pad, const int N2_pad)
{
    const bool p=(boundary_conditions==1); // periodic boundary conditions
    const bool D=(boundary_conditions==3); // Dirichlet boundary conditions
    int i,j,k,  ip,jp,kp;
    float_t A_mc;
    bool mc_in;

    // get the nearest grid point
    i = (int)(floor(x0/d0));
    j = (int)(floor(x1/d1));
    k = (int)(floor(x2/d2));

    // indices for periodic boundary conditions
    if(p)
    {
        ip = (int)p_mod(i,N0);
        jp = (int)p_mod(j,N1);
        kp = (int)p_mod(k,N2);
    }

    // checks to access values with Dirichlet boundary_conditions
    mc_in = D && !(i<-N0_pad || i>=N0+N0_pad) && !(j<-N1_pad || j>=N1+N1_pad) && !(k<-N2_pad || k>=N2+N2_pad);

    // array values
    A_mc = p ? A[get_index(ip,jp,kp,N0,N1,N2)] : ( mc_in ? A[get_index_in_paddedbox(i,j,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );

    return A_mc / weight;
} //mesh_to_particle_ngp
///-------------------------------------------------------------------------------------
/* -------------------------------------------------------------------------------------
 * ------------------------------------ CiC --------------------------------------------
 * -------------------------------------------------------------------------------------
 */
///-------------------------------------------------------------------------------------
/** @fn particle_to_mesh_cic
 * Assign particle to grid, CiC scheme
 * @param x0 particle position x (code units)
 * @param x1 particle position y (code units)
 * @param x2 particle position z (code units)
 * @param weight particle weight
 * @param *A pointer to array representing the grid
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param d0 conversion factor position to grid x
 * @param d1 conversion factor position to grid y
 * @param d2 conversion factor position to grid z
 * @param boundary_conditions boundary conditions to use
 * @param *mc0min minimum x index (used for parallelization)
 * @param *mc0max maximum x index (used for parallelization)
 * @param *mc1min minimum y index (used for parallelization)
 * @param *mc1max maximum y index (used for parallelization)
 * @param *mc2min minimum z index (used for parallelization)
 * @param *mc2max maximum z index (used for parallelization)
 * @param *mass_deposited fraction of mass deposited by this particle to the grid
 */
void particle_to_mesh_cic(const float_t x0, const float_t x1, const float_t x2, const double weight, float_t *A, const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, const int boundary_conditions, int *mc0min, int *mc0max, int *mc1min, int *mc1max, int *mc2min, int *mc2max, double *mass_deposited)
{
    const bool p=(boundary_conditions==1); // periodic boundary conditions
    int i,j,k, ip,jp,kp, ip1,jp1,kp1;
    cellIndex_t mc1,mc2,mc3,mc4,mc5,mc6,mc7,mc8;
    double dx,dy,dz, tx,ty,tz, f1,f2,f3,f4,f5,f6,f7,f8;
    bool mc1_in,mc2_in,mc3_in,mc4_in,mc5_in,mc6_in,mc7_in,mc8_in;

    // get the nearest grid point
    i = (int)(floor(x0/d0));
    j = (int)(floor(x1/d1));
    k = (int)(floor(x2/d2));

    // CiC scheme
    dx=(double)(x0)/d0-(double)i; tx=1.-dx;
    dy=(double)(x1)/d1-(double)j; ty=1.-dy;
    dz=(double)(x2)/d2-(double)k; tz=1.-dz;
    f1 = 1.*tx*ty*tz;
    f2 = 1.*dx*ty*tz;
    f3 = 1.*tx*dy*tz;
    f4 = 1.*tx*ty*dz;
    f5 = 1.*dx*dy*tz;
    f6 = 1.*dx*ty*dz;
    f7 = 1.*tx*dy*dz;
    f8 = 1.*dx*dy*dz;

    // indices for periodic boundary conditions
    if(p)
    {
        ip = (int)p_mod(i,N0);
        jp = (int)p_mod(j,N1);
        kp = (int)p_mod(k,N2);
        ip1= (int)p_mod(i+1,N0);
        jp1= (int)p_mod(j+1,N1);
        kp1= (int)p_mod(k+1,N2);
    }

    // checks to access values for non-periodic boundary conditions
    mc1_in = !(i<0 || i>=N0) && !(j<0 || j>=N1) && !(k<0 || k>=N2);
    mc2_in = !(i+1<0 || i+1>=N0) && !(j<0 || j>=N1) && !(k<0 || k>=N2);
    mc3_in = !(i<0 || i>=N0) && !(j+1<0 || j+1>=N1) && !(k<0 || k>=N2);
    mc4_in = !(i<0 || i>=N0) && !(j<0 || j>=N1) && !(k+1<0 || k+1>=N2);
    mc5_in = !(i+1<0 || i+1>=N0) && !(j+1<0 || j+1>=N1) && !(k<0 || k>=N2);
    mc6_in = !(i+1<0 || i+1>=N0) && !(j<0 || j>=N1) && !(k+1<0 || k+1>=N2);
    mc7_in = !(i<0 || i>=N0) && !(j+1<0 || j+1>=N1) && !(k+1<0 || k+1>=N2);
    mc8_in = !(i+1<0 || i+1>=N0) && !(j+1<0 || j+1>=N1) && !(k+1<0 || k+1>=N2);

    // minimum/maximum indices updated
    int mc0min_current=N0-1, mc1min_current=N1-1, mc2min_current=N2-1;
    int mc0max_current=0, mc1max_current=0, mc2max_current=0;

    // assign particle to mesh
    if(mc1_in || p)
    {
        mc1 = mc1_in ? get_index(i,j,k,N0,N1,N2) : get_index(ip,jp,kp,N0,N1,N2);
        mc0min_current= mc1_in ? min(i,mc0min_current) : min(ip,mc0min_current);
        mc0max_current= mc1_in ? max(i,mc0max_current) : max(ip,mc0max_current);
        mc1min_current= mc1_in ? min(j,mc1min_current) : min(jp,mc1min_current);
        mc1max_current= mc1_in ? max(j,mc1max_current) : max(jp,mc1max_current);
        mc2min_current= mc1_in ? min(k,mc2min_current) : min(kp,mc2min_current);
        mc2max_current= mc1_in ? max(k,mc2max_current) : max(kp,mc2max_current);
        A[mc1] += weight * f1;
        *mass_deposited += f1;
    }
    if(mc2_in || p)
    {
        mc2 = mc2_in ? get_index(i+1,j,k,N0,N1,N2) : get_index(ip1,jp,kp,N0,N1,N2);
        mc0min_current= mc2_in ? min(i+1,mc0min_current) : min(ip1,mc0min_current);
        mc0max_current= mc2_in ? max(i+1,mc0max_current) : max(ip1,mc0max_current);
        mc1min_current= mc2_in ? min(j,mc1min_current) : min(jp,mc1min_current);
        mc1max_current= mc2_in ? max(j,mc1max_current) : max(jp,mc1max_current);
        mc2min_current= mc2_in ? min(k,mc2min_current) : min(kp,mc2min_current);
        mc2max_current= mc2_in ? max(k,mc2max_current) : max(kp,mc2max_current);
        A[mc2] += weight * f2;
        *mass_deposited += f2;
    }
    if(mc3_in || p)
    {
        mc3 = mc3_in ? get_index(i,j+1,k,N0,N1,N2) : get_index(ip,jp1,kp,N0,N1,N2);
        mc0min_current= mc3_in ? min(i,mc0min_current) : min(ip,mc0min_current);
        mc0max_current= mc3_in ? max(i,mc0max_current) : max(ip,mc0max_current);
        mc1min_current= mc3_in ? min(j+1,mc1min_current) : min(jp1,mc1min_current);
        mc1max_current= mc3_in ? max(j+1,mc1max_current) : max(jp1,mc1max_current);
        mc2min_current= mc3_in ? min(k,mc2min_current) : min(kp,mc2min_current);
        mc2max_current= mc3_in ? max(k,mc2max_current) : max(kp,mc2max_current);
        A[mc3] += weight * f3;
        *mass_deposited += f3;
    }
    if(mc4_in || p)
    {
        mc4 = mc4_in ? get_index(i,j,k+1,N0,N1,N2) : get_index(ip,jp,kp1,N0,N1,N2);
        mc0min_current= mc4_in ? min(i,mc0min_current) : min(ip,mc0min_current);
        mc0max_current= mc4_in ? max(i,mc0max_current) : max(ip,mc0max_current);
        mc1min_current= mc4_in ? min(j,mc1min_current) : min(jp,mc1min_current);
        mc1max_current= mc4_in ? max(j,mc1max_current) : max(jp,mc1max_current);
        mc2min_current= mc4_in ? min(k+1,mc2min_current) : min(kp1,mc2min_current);
        mc2max_current= mc4_in ? max(k+1,mc2max_current) : max(kp1,mc2max_current);
        A[mc4] += weight * f4;
        *mass_deposited += f4;
    }
    if(mc5_in || p)
    {
        mc5 = mc5_in ? get_index(i+1,j+1,k,N0,N1,N2) : get_index(ip1,jp1,kp,N0,N1,N2);
        mc0min_current= mc5_in ? min(i+1,mc0min_current) : min(ip1,mc0min_current);
        mc0max_current= mc5_in ? max(i+1,mc0max_current) : max(ip1,mc0max_current);
        mc1min_current= mc5_in ? min(j+1,mc1min_current) : min(jp1,mc1min_current);
        mc1max_current= mc5_in ? max(j+1,mc1max_current) : max(jp1,mc1max_current);
        mc2min_current= mc5_in ? min(k,mc2min_current) : min(kp,mc2min_current);
        mc2max_current= mc5_in ? max(k,mc2max_current) : max(kp,mc2max_current);
        A[mc5] += weight * f5;
        *mass_deposited += f5;
    }
    if(mc6_in || p)
    {
        mc6 = mc6_in ? get_index(i+1,j,k+1,N0,N1,N2) : get_index(ip1,jp,kp1,N0,N1,N2);
        mc0min_current= mc6_in ? min(i+1,mc0min_current) : min(ip1,mc0min_current);
        mc0max_current= mc6_in ? max(i+1,mc0max_current) : max(ip1,mc0max_current);
        mc1min_current= mc6_in ? min(j,mc1min_current) : min(jp,mc1min_current);
        mc1max_current= mc6_in ? max(j,mc1max_current) : max(jp,mc1max_current);
        mc2min_current= mc6_in ? min(k+1,mc2min_current) : min(kp1,mc2min_current);
        mc2max_current= mc6_in ? max(k+1,mc2max_current) : max(kp1,mc2max_current);
        A[mc6] += weight * f6;
        *mass_deposited += f6;
    }
    if(mc7_in || p)
    {
        mc7 = mc7_in ? get_index(i,j+1,k+1,N0,N1,N2) : get_index(ip,jp1,kp1,N0,N1,N2);
        mc0min_current= mc7_in ? min(i,mc0min_current) : min(ip,mc0min_current);
        mc0max_current= mc7_in ? max(i,mc0max_current) : max(ip,mc0max_current);
        mc1min_current= mc7_in ? min(j+1,mc1min_current) : min(jp1,mc1min_current);
        mc1max_current= mc7_in ? max(j+1,mc1max_current) : max(jp1,mc1max_current);
        mc2min_current= mc7_in ? min(k+1,mc2min_current) : min(kp1,mc2min_current);
        mc2max_current= mc7_in ? max(k+1,mc2max_current) : max(kp1,mc2max_current);
        A[mc7] += weight * f7;
        *mass_deposited += f7;
    }
    if(mc8_in || p)
    {
        mc8 = mc8_in ? get_index(i+1,j+1,k+1,N0,N1,N2) : get_index(ip1,jp1,kp1,N0,N1,N2);
        mc0min_current= mc8_in ? min(i+1,mc0min_current) : min(ip1,mc0min_current);
        mc0max_current= mc8_in ? max(i+1,mc0max_current) : max(ip1,mc0max_current);
        mc1min_current= mc8_in ? min(j+1,mc1min_current) : min(jp1,mc1min_current);
        mc1max_current= mc8_in ? max(j+1,mc1max_current) : max(jp1,mc1max_current);
        mc2min_current= mc8_in ? min(k+1,mc2min_current) : min(kp1,mc2min_current);
        mc2max_current= mc8_in ? max(k+1,mc2max_current) : max(kp1,mc2max_current);
        A[mc8] += weight * f8;
        *mass_deposited += f8;
    }
    *mc0min = min(mc0min_current,mc0max_current);
    *mc0max = max(mc0min_current,mc0max_current);
    *mc1min = min(mc1min_current,mc1max_current);
    *mc1max = max(mc1min_current,mc1max_current);
    *mc2min = min(mc2min_current,mc2max_current);
    *mc2max = max(mc2min_current,mc2max_current);
    if(p)
        *mass_deposited = 1.;
    else
    {
        *mass_deposited = 0.;
        if(mc1_in) *mass_deposited += f1;
        if(mc2_in) *mass_deposited += f2;
        if(mc3_in) *mass_deposited += f3;
        if(mc4_in) *mass_deposited += f4;
        if(mc5_in) *mass_deposited += f5;
        if(mc6_in) *mass_deposited += f6;
        if(mc7_in) *mass_deposited += f7;
        if(mc8_in) *mass_deposited += f8;
    }
} //particle_to_mesh_cic
///-------------------------------------------------------------------------------------
/** @fn particle_to_mesh_cic_periodic
 * Assign particle to grid, CiC scheme
 * @param x0 particle position x (code units)
 * @param x1 particle position y (code units)
 * @param x2 particle position z (code units)
 * @param weight particle weight
 * @param *A pointer to array representing the grid
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param d0 conversion factor position to grid x
 * @param d1 conversion factor position to grid y
 * @param d2 conversion factor position to grid z
 * @param *mc0min minimum x index (used for parallelization)
 * @param *mc0max maximum x index (used for parallelization)
 * @param *mc1min minimum y index (used for parallelization)
 * @param *mc1max maximum y index (used for parallelization)
 * @param *mc2min minimum z index (used for parallelization)
 * @param *mc2max maximum z index (used for parallelization)
 */
void particle_to_mesh_cic_periodic(const float_t x0, const float_t x1, const float_t x2, const float_t weight, float_t *A, const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, int *mc0min, int *mc0max, int *mc1min, int *mc1max, int *mc2min, int *mc2max)
{
    const int boundary_conditions = 1; // periodic boundary conditions
    double mass_deposited;
    particle_to_mesh_cic(x0,x1,x2,weight,A,N0,N1,N2,d0,d1,d2,boundary_conditions,mc0min,mc0max,mc1min,mc1max,mc2min,mc2max,&mass_deposited);
} //particle_to_mesh_cic_periodic
///-------------------------------------------------------------------------------------
/** @fn mesh_to_particle_cic
 * Assign grid value to particle, CiC scheme
 * @param *A pointer to array representing the grid
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param d0 conversion factor position to grid x
 * @param d1 conversion factor position to grid y
 * @param d2 conversion factor position to grid z
 * @param x0 particle position x (code units)
 * @param x1 particle position y (code units)
 * @param x2 particle position z (code units)
 * @param weight particle weight
 * @param boundary_conditions boundary conditions to use
 * @param N0_pad number of additional cells to the East
 * @param N1_pad number of additional cells to the North
 * @param N2_pad number of additional cells to the Top
 */
float_t mesh_to_particle_cic(const float_t *A, const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, const float_t x0, const float_t x1, const float_t x2, const double weight, const int boundary_conditions, const int N0_pad, const int N1_pad, const int N2_pad)
{
    const bool p=(boundary_conditions==1); // periodic boundary conditions
    const bool D=(boundary_conditions==3); // Dirichlet boundary conditions
    int i,j,k,  ip,jp,kp, ip1,jp1,kp1;
    double dx,dy,dz, tx,ty,tz, f1,f2,f3,f4,f5,f6,f7,f8;
    float_t A_mc1, A_mc2, A_mc3, A_mc4, A_mc5, A_mc6, A_mc7, A_mc8;
    bool mc1_in,mc2_in,mc3_in,mc4_in,mc5_in,mc6_in,mc7_in,mc8_in;

    // get the nearest grid point
    i = (int)(floor(x0/d0));
    j = (int)(floor(x1/d1));
    k = (int)(floor(x2/d2));

    // CiC scheme
    dx = (double)(x0)/d0-(double)i; tx=1.-dx;
    dy = (double)(x1)/d1-(double)j; ty=1.-dy;
    dz = (double)(x2)/d2-(double)k; tz=1.-dz;
    f1 = 1.*tx*ty*tz;
    f2 = 1.*dx*ty*tz;
    f3 = 1.*tx*dy*tz;
    f4 = 1.*tx*ty*dz;
    f5 = 1.*dx*dy*tz;
    f6 = 1.*dx*ty*dz;
    f7 = 1.*tx*dy*dz;
    f8 = 1.*dx*dy*dz;

    // indices for periodic boundary conditions
    if(p)
    {
        ip = (int)p_mod(i,N0);
        jp = (int)p_mod(j,N1);
        kp = (int)p_mod(k,N2);
        ip1= (int)p_mod(i+1,N0);
        jp1= (int)p_mod(j+1,N1);
        kp1= (int)p_mod(k+1,N2);
    }

    // checks to access values with Dirichlet boundary_conditions
    mc1_in = D && !(i<-N0_pad || i>=N0+N0_pad) && !(j<-N1_pad || j>=N1+N1_pad) && !(k<-N2_pad || k>=N2+N2_pad);
    mc2_in = D && !(i+1<-N0_pad || i+1>=N0+N0_pad) && !(j<-N1_pad || j>=N1+N1_pad) && !(k<-N2_pad || k>=N2+N2_pad);
    mc3_in = D && !(i<-N0_pad || i>=N0+N0_pad) && !(j+1<-N1_pad || j+1>=N1+N1_pad) && !(k<-N2_pad || k>=N2+N2_pad);
    mc4_in = D && !(i<-N0_pad || i>=N0+N0_pad) && !(j<-N1_pad || j>=N1+N1_pad) && !(k+1<-N2_pad || k+1>=N2+N2_pad);
    mc5_in = D && !(i+1<-N0_pad || i+1>=N0+N0_pad) && !(j+1<-N1_pad || j+1>=N1+N1_pad) && !(k<-N2_pad || k>=N2+N2_pad);
    mc6_in = D && !(i+1<-N0_pad || i+1>=N0+N0_pad) && !(j<-N1_pad || j>=N1+N1_pad) && !(k+1<-N2_pad || k+1>=N2+N2_pad);
    mc7_in = D && !(i<-N0_pad || i>=N0+N0_pad) && !(j+1<-N1_pad || j+1>=N1+N1_pad) && !(k+1<-N2_pad || k+1>=N2+N2_pad);
    mc8_in = D && !(i+1<-N0_pad || i+1>=N0+N0_pad) && !(j+1<-N1_pad || j+1>=N1+N1_pad) && !(k+1<-N2_pad || k+1>=N2+N2_pad);

    // array values
    A_mc1 = p ? A[get_index(ip,jp,kp,N0,N1,N2)] : ( mc1_in ? A[get_index_in_paddedbox(i,j,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
    A_mc2 = p ? A[get_index(ip1,jp,kp,N0,N1,N2)] : ( mc2_in ? A[get_index_in_paddedbox(i+1,j,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
    A_mc3 = p ? A[get_index(ip,jp1,kp,N0,N1,N2)] : ( mc3_in ? A[get_index_in_paddedbox(i,j+1,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
    A_mc4 = p ? A[get_index(ip,jp,kp1,N0,N1,N2)] : ( mc4_in ? A[get_index_in_paddedbox(i,j,k+1,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
    A_mc5 = p ? A[get_index(ip1,jp1,kp,N0,N1,N2)] : ( mc5_in ? A[get_index_in_paddedbox(i+1,j+1,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
    A_mc6 = p ? A[get_index(ip1,jp,kp1,N0,N1,N2)] : ( mc6_in ? A[get_index_in_paddedbox(i+1,j,k+1,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
    A_mc7 = p ? A[get_index(ip,jp1,kp1,N0,N1,N2)] : ( mc7_in ? A[get_index_in_paddedbox(i,j+1,k+1,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
    A_mc8 = p ? A[get_index(ip1,jp1,kp1,N0,N1,N2)] : ( mc8_in ? A[get_index_in_paddedbox(i+1,j+1,k+1,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );

    return (    A_mc1    * f1 +
            A_mc2    * f2 +
            A_mc3    * f3 +
            A_mc4    * f4 +
            A_mc5    * f5 +
            A_mc6    * f6 +
            A_mc7    * f7 +
            A_mc8    * f8 ) / weight;
} //mesh_to_particle_cic
///-------------------------------------------------------------------------------------
/* -------------------------------------------------------------------------------------
 * ------------------------------------ TSC --------------------------------------------
 * -------------------------------------------------------------------------------------
 */
///-------------------------------------------------------------------------------------
/* -------------------------------------------------------------------------------------
 * ---------------------------------- WRAPPERS -----------------------------------------
 * -------------------------------------------------------------------------------------
 */
///-------------------------------------------------------------------------------------
/** @fn mesh_to_particle_cic_periodic
 * Assign grid value to particle, CiC scheme
 * @param *A pointer to array representing the grid
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param d0 conversion factor position to grid x
 * @param d1 conversion factor position to grid y
 * @param d2 conversion factor position to grid z
 * @param x0 particle position x (code units)
 * @param x1 particle position y (code units)
 * @param x2 particle position z (code units)
 * @param weight particle weight
 */
float_t mesh_to_particle_cic_periodic(const float_t *A, const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, const float_t x0, const float_t x1, const float_t x2, const double weight)
{
    const int boundary_conditions=1; // periodic boundary conditions
    return mesh_to_particle_cic(A,N0,N1,N2,d0,d1,d2,x0,x1,x2,weight,boundary_conditions,0,0,0);
} //mesh_to_particle_cic_periodic
///-------------------------------------------------------------------------------------
/** @fn particle_to_mesh_periodic
 * Assign particle to grid
 * @param x0 particle position x (code units)
 * @param x1 particle position y (code units)
 * @param x2 particle position z (code units)
 * @param weight particle weight
 * @param *A pointer to array representing the grid
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param d0 conversion factor position to grid x
 * @param d1 conversion factor position to grid y
 * @param d2 conversion factor position to grid z
 * @param *mc0min minimum x index (used for parallelization)
 * @param *mc0max maximum x index (used for parallelization)
 * @param *mc1min minimum y index (used for parallelization)
 * @param *mc1max maximum y index (used for parallelization)
 * @param *mc2min minimum z index (used for parallelization)
 * @param *mc2max maximum z index (used for parallelization)
 */
void particle_to_mesh_periodic(const float_t x0, const float_t x1, const float_t x2, const double weight, float_t *A, const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, int *mc0min, int *mc0max, int *mc1min, int *mc1max, int *mc2min, int *mc2max)
{
    #if ASSIGNMENT == NGP
        particle_to_mesh_ngp(x0,x1,x2,weight,A,N0,N1,N2,d0,d1,d2,mc0min,mc0max,mc1min,mc1max,mc2min,mc2max);
    #elif ASSIGNMENT == CIC
        particle_to_mesh_cic_periodic(x0,x1,x2,weight,A,N0,N1,N2,d0,d1,d2,mc0min,mc0max,mc1min,mc1max,mc2min,mc2max);
    #endif
} //particle_to_mesh_periodic
///-------------------------------------------------------------------------------------
/** @fn particle_to_mesh_nonperiodic
 * Assign particle to grid
 * @param x0 particle position x (code units)
 * @param x1 particle position y (code units)
 * @param x2 particle position z (code units)
 * @param weight particle weight
 * @param *A pointer to array representing the grid
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param d0 conversion factor position to grid x
 * @param d1 conversion factor position to grid y
 * @param d2 conversion factor position to grid z
 * @param *mc0min minimum x index (used for parallelization)
 * @param *mc0max maximum x index (used for parallelization)
 * @param *mc1min minimum y index (used for parallelization)
 * @param *mc1max maximum y index (used for parallelization)
 * @param *mc2min minimum z index (used for parallelization)
 * @param *mc2max maximum z index (used for parallelization)
 * @param *mass_deposited fraction of mass deposited by this particle to the grid
 */
void particle_to_mesh_nonperiodic(const float_t x0, const float_t x1, const float_t x2, const double weight, float_t *A, const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, int *mc0min, int *mc0max, int *mc1min, int *mc1max, int *mc2min, int *mc2max, double *mass_deposited)
{
    const int boundary_conditions = -1;
    #if ASSIGNMENT == NGP
        particle_to_mesh_ngp(x0,x1,x2,weight,A,N0,N1,N2,d0,d1,d2,boundary_conditions,mc0min,mc0max,mc1min,mc1max,mc2min,mc2max);
    #elif ASSIGNMENT == CIC
        particle_to_mesh_cic(x0,x1,x2,weight,A,N0,N1,N2,d0,d1,d2,boundary_conditions,mc0min,mc0max,mc1min,mc1max,mc2min,mc2max,mass_deposited);
    #endif
} //particle_to_mesh_nonperiodic
///-------------------------------------------------------------------------------------
/** @fn mesh_to_particle_periodic
 * Assign grid value to particle
 * @param *A pointer to array representing the grid
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param d0 conversion factor position to grid x
 * @param d1 conversion factor position to grid y
 * @param d2 conversion factor position to grid z
 * @param x0 particle position x (code units)
 * @param x1 particle position y (code units)
 * @param x2 particle position z (code units)
 * @param weight particle weight
 */
float_t mesh_to_particle_periodic(const float_t *A, const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, const float_t x0, const float_t x1, const float_t x2, const double weight)
{
    #if ASSIGNMENT == NGP
        return mesh_to_particle_ngp(A,N0,N1,N2,d0,d1,d2,x0,x1,x2,weight);
    #elif ASSIGNMENT == CIC
        return mesh_to_particle_cic_periodic(A,N0,N1,N2,d0,d1,d2,x0,x1,x2,weight);
    #endif
} //mesh_to_particle_periodic
///-------------------------------------------------------------------------------------
/** @fn mesh_to_particle
 * Assign grid value to particle
 * @param *A pointer to array representing the grid
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param d0 conversion factor position to grid x
 * @param d1 conversion factor position to grid y
 * @param d2 conversion factor position to grid z
 * @param x0 particle position x (code units)
 * @param x1 particle position y (code units)
 * @param x2 particle position z (code units)
 * @param weight particle weight
 * @param boundary_conditions boundary conditions to use
 * @param N0_pad number of additional cells to the East
 * @param N1_pad number of additional cells to the North
 * @param N2_pad number of additional cells to the Top
 */
float_t mesh_to_particle(const float_t *A, const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, const float_t x0, const float_t x1, const float_t x2, const double weight, const int boundary_conditions, const int N0_pad, const int N1_pad, const int N2_pad)
{
    #if ASSIGNMENT == NGP
        UNUSED(boundary_conditions);
        return mesh_to_particle_ngp(A,N0,N1,N2,d0,d1,d2,x0,x1,x2,weight);
    #elif ASSIGNMENT == CIC
        return mesh_to_particle_cic(A,N0,N1,N2,d0,d1,d2,x0,x1,x2,weight,boundary_conditions,N0_pad,N1_pad,N2_pad);
    #endif
} //mesh_to_particle
///-------------------------------------------------------------------------------------
