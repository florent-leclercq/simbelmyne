///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/src/snapshot.c
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file snapshot.c
 *  \brief Routines for working with Simbelmynë snapshots
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2013-2023
 */
#include "snapshot.h"
///-------------------------------------------------------------------------------------
/** @fn get_Lagrangian_indices
 * Get indices on the Lagrangian grid from particle Id
 * @param Id particle id
 * @param Np0 particle mesh size x
 * @param Np1 particle mesh size y
 * @param Np2 particle mesh size z
 * @param *indices output indices
 * \warning Assumes row major!
 */
void get_Lagrangian_indices(const particleID_t Id, const int Np0, const int Np1, const int Np2, int *indices)
{
    get_longlong_indices((const long long)Id,Np0,Np1,Np2,indices);
} //get_Lagrangian_indices
///-------------------------------------------------------------------------------------
/** @fn get_Lagrangian_Id
 * Get particle Id from indices on the Lagrangian grid
 * @param mp0 index x
 * @param mp1 index y
 * @param mp2 index z
 * @param Np0 particle mesh size x
 * @param Np1 particle mesh size y
 * @param Np2 particle mesh size z
 * @return particle id
 * \warning Assumes row major!
 */
particleID_t get_Lagrangian_Id(const int mp0, const int mp1, const int mp2, const int Np0, const int Np1, const int Np2)
{
    return (particleID_t)get_index(mp0,mp1,mp2,Np0,Np1,Np2);
} //get_Lagrangian_Id
///-------------------------------------------------------------------------------------
/** @fn get_Lagrangian_grid_position
 * Get initial position of a particle on the Lagrangian grid
 * @param mp0 index x
 * @param mp1 index y
 * @param mp2 index z
 * @param dp0 particle cell size x
 * @param dp1 particle cell size y
 * @param dp2 particle cell size z
 * @param Pos output position
 */
void get_Lagrangian_grid_position(const int mp0, const int mp1, const int mp2, const double dp0, const double dp1, const double dp2, float_t Pos[3])
{
    Pos[0] = (float_t)mp0*dp0; // Particles placed on a regular grid
    Pos[1] = (float_t)mp1*dp1;
    Pos[2] = (float_t)mp2*dp2;
} //get_Lagrangian_grid_position
///-------------------------------------------------------------------------------------
/** @fn get_Psi
 * This routine calculates the Lagrangian displacement field, from a particles' initial
 * and final position, accounting for periodic boundary conditions
 * @param finalPosition input: final position
 * @param initialPosition input: initial position
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 * @param Psi output: displacement field
 */
void get_Psi(const float_t finalPosition[3], const float_t initialPosition[3], const double L0, const double L1, const double L2, float_t Psi[3])
{
    Psi[0] = finalPosition[0] - initialPosition[0];
    Psi[1] = finalPosition[1] - initialPosition[1];
    Psi[2] = finalPosition[2] - initialPosition[2];

    // Check if the particle has crossed the periodic boundaries of the box
    Psi[0] = fabs(L0-fabs(Psi[0])) < fabs(Psi[0]) ? copysign(L0-fabs(Psi[0]),-Psi[0]) : Psi[0];
    Psi[1] = fabs(L1-fabs(Psi[1])) < fabs(Psi[1]) ? copysign(L1-fabs(Psi[1]),-Psi[1]) : Psi[1];
    Psi[2] = fabs(L2-fabs(Psi[2])) < fabs(Psi[2]) ? copysign(L2-fabs(Psi[2]),-Psi[2]) : Psi[2];
} //get_Psi
///-------------------------------------------------------------------------------------
/** @fn periodic_wrap
 * This routine computes a new position making sure that it is
 * inside the box (assuming periodic boundary conditions)
 * @param x old position
 * @param xmin minimum value x
 * @param L box size
 * @return new position
 */
double periodic_wrap(const double x, const double xmin, const double L)
{
    return p_mod(x-xmin,L);
} //periodic_wrap
///-------------------------------------------------------------------------------------
/** @fn clipping_wrap
 * This routine computes a new position making sure that it is
 * inside the box (doesn't assume periodic boundary conditions. clips
 * anything that is not between xmin and L-d)
 * @param x old position
 * @param xmin minimum value x
 * @param L box size
 * @param d cell size
 * @return new position
 */
double clipping_wrap(const double x, const double xmin, const double L, const double d)
{
    if(x<xmin)
        return xmin;
    else if(x>L-d)
        return L-d;
    else
        return x;
} //clipping_wrap
///-------------------------------------------------------------------------------------
/** @fn return_grid_snapshot_DM
 * This routine returns a snapshot containing a regular grid of dark matter particles
 * @param Np0 particle grid size x
 * @param Np1 particle grid size y
 * @param Np2 particle grid size z
 * @param BoxSize box size
 * @param num_files number of files for writing the snapshot
 * @param cosmo cosmological parameters
 * @return the snapshot in default format
 */
Snapshot return_grid_snapshot_DM(const int Np0, const int Np1, const int Np2, const double BoxSize, const int num_files, const double cosmo[N_COSMOPAR])
{
    double h=cosmo[0];
    double Omega_r=cosmo[1];
    double Omega_q=cosmo[2];
    double Omega_b=cosmo[3];
    double Omega_m=cosmo[4];
    double Omega_k=cosmo[5];
    double n_s=cosmo[6];
    double sigma8=cosmo[7];
    double w0_fld=cosmo[8];
    double wa_fld=cosmo[9];
    UNUSED(Omega_r);UNUSED(Omega_b);UNUSED(Omega_k);UNUSED(n_s);UNUSED(sigma8);UNUSED(w0_fld);UNUSED(wa_fld);

    const double dp0=BoxSize/(double)Np0;
    const double dp1=BoxSize/(double)Np1;
    const double dp2=BoxSize/(double)Np2;
    Snapshot P;
    particleID_t Np=Np0*Np1*Np2, N_per_file = Np/num_files;

    P.header.npart[0] = P.header.npart[2] = P.header.npart[3] = P.header.npart[4] = P.header.npart[5] = 0; P.header.npart[1] = (unsigned int)N_per_file;
    P.header.npartTotal[0] = P.header.npartTotal[2] = P.header.npartTotal[3] = P.header.npartTotal[4] = P.header.npartTotal[5] = 0; P.header.npartTotal[1] = (unsigned int)Np;
    P.header.mass[0] = P.header.mass[2] = P.header.mass[3] = P.header.mass[4] = P.header.mass[5] = 1.; // Shouldn't be zero, otherwise interpreted as "variable particle mass" by Gadget
    P.header.mass[1] = Omega_m * 3 * P_Hubble * P_Hubble / (8 * M_PI * P_G) * pow(BoxSize, 3) / Np; // First Friedmann equation in cosmic time
    P.header.time = 0.001; P.header.redshift = 999.; // Assumes redshift z=999
    P.header.flag_sfr = P.header.flag_feedback = P.header.flag_cooling = 0;
    P.header.num_files = num_files;
    P.header.BoxSize = BoxSize; P.header.Omega0 = Omega_m; P.header.OmegaLambda = Omega_q; P.header.HubbleParam = h;
    P.header.flag_stellarage = P.header.flag_metals = 0;
    P.header.npartTotalHighWord[0] = P.header.npartTotalHighWord[1] = P.header.npartTotalHighWord[2] = P.header.npartTotalHighWord[3] = P.header.npartTotalHighWord[4] = P.header.npartTotalHighWord[5] = 0;
    P.header.flag_entropy_instead_u = 0;
    P.header.flag_doubleprecision = 0; P.header.flag_ic_info = 0;
    P.header.lpt_scalingfactor = 0.;

    allocate_snapshot(&P, Np, VARNAME(P));

    #pragma omp parallel for schedule(static) collapse(3)
    for(int mp0=0; mp0<Np0; mp0++)
        for(int mp1=0; mp1<Np1; mp1++)
            for(int mp2=0; mp2<Np2; mp2++)
            {
                particleID_t mp = get_Lagrangian_Id(mp0,mp1,mp2,Np0,Np1,Np2);
                #ifdef HAVE_TYPE_AND_MASS
                P.types[mp].Type = 1; // Dark matter halos only
                P.masses[mp].Mass = P.header.mass[1];
                #endif
                get_Lagrangian_grid_position(mp0,mp1,mp2,dp0,dp1,dp2,P.positions[mp].Pos); // Particles placed on a regular grid
                P.velocities[mp].Vel[0] = P.velocities[mp].Vel[1] = P.velocities[mp].Vel[2] = 0.;
                P.ids[mp].Id = mp;
                #ifdef HAVE_EXTRAS
                P.extras[mp].U = P.extras[mp].Rho = P.extras[mp].Hsml = 0.;
                #endif
            }

    return P;
} //return_grid_snapshot_DM
///-------------------------------------------------------------------------------------
/** @fn allocate_snapshot_
 * This routine allocates the memory for a snapshot
 * @param *P snapshot
 * @param Np number of particles
 * @param *name output variable name
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
void allocate_snapshot_(Snapshot *P, const particleID_t Np, char *name, char *file, char *func, int line)
{
    char vname[50];
    #ifdef HAVE_TYPE_AND_MASS
    sprintf(vname, "%s.types", name);
    P->types = p_malloc_(Np * sizeof(particle_type_t), vname, file, func, line);
    sprintf(vname, "%s.masses", name);
    P->masses = p_malloc_(Np * sizeof(particle_mass_t), vname, file, func, line);
    #endif
    sprintf(vname, "%s.positions", name);
    P->positions = p_malloc_(Np * sizeof(particle_pos_t), vname, file, func, line);
    sprintf(vname, "%s.velocities", name);
    P->velocities = p_malloc_(Np * sizeof(particle_vel_t), vname, file, func, line);
    sprintf(vname, "%s.ids", name);
    P->ids = p_malloc_(Np * sizeof(particle_id_t), vname, file, func, line);
    #ifdef HAVE_EXTRAS
    sprintf(vname, "%s.extras", name);
    P->extras = p_malloc_(Np * sizeof(particle_extras_t), vname, file, func, line);
    #endif
} //allocate_snapshot_
///-------------------------------------------------------------------------------------
/** @fn modify_snapshot_update_time
 * This routine modifies a snapshot to update the redshift and time (scale factor)
 * @param *P the snapshot
 * @param redshift new redshift
 */
void modify_snapshot_update_time(Snapshot *P, const double redshift)
{
    P->header.redshift = redshift;
    P->header.time = z2a(redshift);
} //modify_snapshot_update_time
///-------------------------------------------------------------------------------------
/** @fn modify_snapshot_drift_mesh
 * This routine modifies a snapshot by adding a displacement to all particles
 * The input are fields on the mesh, the routine interpolates
 * @param *P the snapshot
 * @param *DRIFT_X the drift along x
 * @param *DRIFT_Y the drift along y
 * @param *DRIFT_Z the drift along z
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 */
void modify_snapshot_drift_mesh(Snapshot *P, const float_t *DRIFT_X, const float_t *DRIFT_Y, const float_t *DRIFT_Z, const int N0, const int N1, const int N2)
{
    sprintf(G__msg__, "Displacing particles...");
    PrintMessage(4, G__msg__);

    const particleID_t Np=(particleID_t)(P->header.npart[0]+P->header.npart[1]+P->header.npart[2]+P->header.npart[3]+P->header.npart[4]+P->header.npart[5]);
    const double L0=P->header.BoxSize, L1=P->header.BoxSize, L2=P->header.BoxSize;

    double d0=L0/(double)N0;
    double d1=L1/(double)N1;
    double d2=L2/(double)N2;

    #if DEBUG
    double maxdrift=0;
    #endif

    #pragma omp parallel for schedule(static)
    for(particleID_t mp=0; mp<Np; mp++)
    {
        double x0,x1,x2,drift0,drift1,drift2,drift,x0_new,x1_new,x2_new;

        // initial positions
        x0 = P->positions[mp].Pos[0];
        x1 = P->positions[mp].Pos[1];
        x2 = P->positions[mp].Pos[2];

        // get the corresponding drift to be used
        drift0 = mesh_to_particle_periodic(DRIFT_X,N0,N1,N2,d0,d1,d2,x0,x1,x2,1.);
        drift1 = mesh_to_particle_periodic(DRIFT_Y,N0,N1,N2,d0,d1,d2,x0,x1,x2,1.);
        drift2 = mesh_to_particle_periodic(DRIFT_Z,N0,N1,N2,d0,d1,d2,x0,x1,x2,1.);
        drift = sqrt(drift0*drift0 + drift1*drift1 + drift2*drift2);

        // compute new positions
        x0_new = x0+drift0;
        x1_new = x1+drift1;
        x2_new = x2+drift2;

        // correction for periodic boundary conditions
        x0_new = periodic_wrap(x0_new, 0., L0);
        x1_new = periodic_wrap(x1_new, 0., L1);
        x2_new = periodic_wrap(x2_new, 0., L2);

        // update snapshot
        P->positions[mp].Pos[0] = x0_new;
        P->positions[mp].Pos[1] = x1_new;
        P->positions[mp].Pos[2] = x2_new;

        #if DEBUG
        if(drift > maxdrift)
            maxdrift = drift;
        #endif
    }

    #if DEBUG
    debugPrintDouble(maxdrift);
    #endif
    sprintf(G__msg__, "Displacing particles done.");
    PrintMessage(4, G__msg__);
} //modify_snapshot_drift_mesh
///-------------------------------------------------------------------------------------
/** @fn modify_snapshot_drift_particles
 * This routine modifies a snapshot by adding a displacement to all particles
 * The input is the arrays of kicks to be directly applied to the particles.
 * Caution: the ordering in the input arrays should be the same as in the
 * snapshot to be modified
 * @param *P the snapshot
 * @param *DRIFT_X the drift along x
 * @param *DRIFT_Y the drift along y
 * @param *DRIFT_Z the drift along z
 */
void modify_snapshot_drift_particles(Snapshot *P, const float_t *DRIFT_X, const float_t *DRIFT_Y, const float_t *DRIFT_Z)
{
    sprintf(G__msg__, "Displacing particles...");
    PrintMessage(4, G__msg__);

    const particleID_t Np=(particleID_t)(P->header.npart[0]+P->header.npart[1]+P->header.npart[2]+P->header.npart[3]+P->header.npart[4]+P->header.npart[5]);
    const double L0=P->header.BoxSize, L1=P->header.BoxSize, L2=P->header.BoxSize;

    #if DEBUG
    double maxdrift=0;
    #endif

    #pragma omp parallel for schedule(static)
    for(particleID_t mp=0; mp<Np; mp++)
    {
        double x0,x1,x2,drift0,drift1,drift2,drift,x0_new,x1_new,x2_new;

        // initial positions
        x0 = P->positions[mp].Pos[0];
        x1 = P->positions[mp].Pos[1];
        x2 = P->positions[mp].Pos[2];

        // get the corresponding drift to be used
        drift0 = DRIFT_X[mp];
        drift1 = DRIFT_Y[mp];
        drift2 = DRIFT_Z[mp];
        drift = sqrt(drift0*drift0 + drift1*drift1 + drift2*drift2);

        // compute new positions
        x0_new = x0+drift0;
        x1_new = x1+drift1;
        x2_new = x2+drift2;

        // correction for periodic boundary conditions
        x0_new = periodic_wrap(x0_new, 0., L0);
        x1_new = periodic_wrap(x1_new, 0., L1);
        x2_new = periodic_wrap(x2_new, 0., L2);

        // update snapshot
        P->positions[mp].Pos[0] = x0_new;
        P->positions[mp].Pos[1] = x1_new;
        P->positions[mp].Pos[2] = x2_new;

        #if DEBUG
        if(drift > maxdrift)
            maxdrift = drift;
        #endif
    }

    #if DEBUG
    debugPrintDouble(maxdrift);
    #endif
    sprintf(G__msg__, "Displacing particles done.");
    PrintMessage(4, G__msg__);
} //modify_snapshot_drift_particles
///-------------------------------------------------------------------------------------
/** @fn modify_snapshot_kick_mesh
 * This routine modifies a snapshot by adding a velocity to all particles
 * The input are fields on the mesh, the routine interpolates
 * @param *P the snapshot
 * @param *KICK_X the kick along x
 * @param *KICK_Y the kick along x
 * @param *KICK_Z the kick along x
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 */
void modify_snapshot_kick_mesh(Snapshot *P, const float_t *KICK_X, const float_t *KICK_Y, const float_t *KICK_Z, const int N0, const int N1, const int N2)
{
    sprintf(G__msg__, "Changing velocities of particles...");
    PrintMessage(4, G__msg__);

    const particleID_t Np=(particleID_t)(P->header.npart[0]+P->header.npart[1]+P->header.npart[2]+P->header.npart[3]+P->header.npart[4]+P->header.npart[5]);
    const double L0=P->header.BoxSize, L1=P->header.BoxSize, L2=P->header.BoxSize;

    double d0=L0/(double)N0;
    double d1=L1/(double)N1;
    double d2=L2/(double)N2;

    #if DEBUG
    double maxkick=0;
    #endif

    #pragma omp parallel for schedule(static)
    for(particleID_t mp=0; mp<Np; mp++)
    {
        double x0,x1,x2,kick0,kick1,kick2,kick,vel0,vel1,vel2;

        // initial positions
        x0 = P->positions[mp].Pos[0];
        x1 = P->positions[mp].Pos[1];
        x2 = P->positions[mp].Pos[2];

        // get the corresponding velocity field to be used
        kick0 = mesh_to_particle_periodic(KICK_X,N0,N1,N2,d0,d1,d2,x0,x1,x2,1.);
        kick1 = mesh_to_particle_periodic(KICK_Y,N0,N1,N2,d0,d1,d2,x0,x1,x2,1.);
        kick2 = mesh_to_particle_periodic(KICK_Z,N0,N1,N2,d0,d1,d2,x0,x1,x2,1.);
        kick = sqrt(kick0*kick0 + kick1*kick1 + kick2*kick2);

        // initial velocities
        vel0 = P->velocities[mp].Vel[0];
        vel1 = P->velocities[mp].Vel[1];
        vel2 = P->velocities[mp].Vel[2];

        // change velocities
        P->velocities[mp].Vel[0] = (vel0+kick0);
        P->velocities[mp].Vel[1] = (vel1+kick1);
        P->velocities[mp].Vel[2] = (vel2+kick2);

        #if DEBUG
        if(kick > maxkick)
            maxkick = kick;
        #endif
    }

    #if DEBUG
    debugPrintDouble(maxkick);
    #endif
    sprintf(G__msg__, "Changing velocities of particles done.");
    PrintMessage(4, G__msg__);
} //modify_snapshot_kick_mesh
///-------------------------------------------------------------------------------------
/** @fn modify_snapshot_kick_particles
 * This routine modifies a snapshot by adding a velocity to all particles
 * The input is the arrays of kicks to be directly applied to the particles.
 * Caution: the ordering in the input arrays should be the same as in the
 * snapshot to be modified
 * @param *P the snapshot
 * @param *KICK_X the kick along x
 * @param *KICK_Y the kick along x
 * @param *KICK_Z the kick along x
 */
void modify_snapshot_kick_particles(Snapshot *P, const float_t *KICK_X, const float_t *KICK_Y, const float_t *KICK_Z)
{
    sprintf(G__msg__, "Changing velocities of particles...");
    PrintMessage(4, G__msg__);

    const particleID_t Np=(particleID_t)(P->header.npart[0]+P->header.npart[1]+P->header.npart[2]+P->header.npart[3]+P->header.npart[4]+P->header.npart[5]);

    #if DEBUG
    double maxkick=0;
    #endif

    #pragma omp parallel for schedule(static)
    for(particleID_t mp=0; mp<Np; mp++)
    {
        double kick0,kick1,kick2,kick,vel0,vel1,vel2;

        // get the velocity field to be used
        kick0 = KICK_X[mp];
        kick1 = KICK_Y[mp];
        kick2 = KICK_Z[mp];
        kick = sqrt(kick0*kick0 + kick1*kick1 + kick2*kick2);

        // initial velocities
        vel0 = P->velocities[mp].Vel[0];
        vel1 = P->velocities[mp].Vel[1];
        vel2 = P->velocities[mp].Vel[2];

        // change velocities
        P->velocities[mp].Vel[0] = (vel0+kick0);
        P->velocities[mp].Vel[1] = (vel1+kick1);
        P->velocities[mp].Vel[2] = (vel2+kick2);

        #if DEBUG
        if(kick > maxkick)
            maxkick = kick;
        #endif
    }

    #if DEBUG
    debugPrintDouble(maxkick);
    #endif
    sprintf(G__msg__, "Changing velocities of particles done.");
    PrintMessage(4, G__msg__);
} //modify_snapshot_kick_particles
///-------------------------------------------------------------------------------------
