///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/src/fft.c
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file fft.c
 *  \brief Routines for fast Fourier transforms
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2013-2023
 */
#include "fft.h"
///-------------------------------------------------------------------------------------
/** @fn FFT_r2c_3d
 * 3D Forward Fourier transform of a real scalar quantity
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param *in input quantity (float_t*)
 * @param *out output quantity (fftw_complex_t*)
 * @param Nthreads number of threads for parallel computing
 */
void FFT_r2c_3d(int N0, int N1, int N2, float_t *in, fftw_complex_t *out, int Nthreads)
{
    // OUT OF PLACE FOURIER TRANSFORMATION FOR REAL INPUT DATA
    const cellIndex_t N=N0*N1*N2;
    const int N2_HC=N2/2+1;
    const fourierMode_t N_HC=N0*N1*N2_HC;
    fftw_plan_t fftp;

    #ifdef DOUBLEPRECISION_FFTW
        #ifndef DOUBLEPRECISION
            double *buf_in; buf_in = (double*)p_fftw_malloc(N*sizeof(double), VARNAME(buf_in));
            fftw_complex *buf_out; buf_out = (fftw_complex*)p_fftw_malloc(N*sizeof(fftw_complex), VARNAME(buf_out));
            #pragma omp parallel for
            for(cellIndex_t mc=0; mc<N; mc++)
                buf_in[mc]=(double)in[mc];
        #else
            UNUSED(N);UNUSED(N2_HC);UNUSED(N_HC);
            double *buf_in=in; fftw_complex *buf_out=out;
        #endif

        fftw_init_threads();
        fftw_plan_with_nthreads(Nthreads);
        fftp = fftw_plan_dft_r2c_3d(N0,N1,N2,buf_in,buf_out,FFTW_ESTIMATE);
        fftw_execute(fftp);
        fftw_destroy_plan(fftp);
        fftw_cleanup_threads();

        #ifndef DOUBLEPRECISION
            #pragma omp parallel for
            for(fourierMode_t mf=0; mf<N_HC; mf++)
                out[mf]=(fftw_complex_t)buf_out[mf];
            p_fftw_free(buf_in, VARNAME(buf_in));
            p_fftw_free(buf_out, VARNAME(buf_out));
        #endif
    #else
        #ifdef DOUBLEPRECISION
            float *buf_in; buf_in = (float*)p_fftw_malloc(N*sizeof(float), VARNAME(buf_in));
            fftwf_complex *buf_out; buf_out = (fftwf_complex*) * p_fftw_malloc(N*sizeof(fftwf_complex), VARNAME(buf_out));
            #pragma omp parallel for
            for(cellIndex_t mc=0; mc<N; mc++)
                buf_in[mc]=(float)in[mc];
        #else
            UNUSED(N);UNUSED(N2_HC);UNUSED(N_HC);
            float *buf_in=in; fftwf_complex *buf_out=out;
        #endif

        fftwf_init_threads();
        fftwf_plan_with_nthreads(Nthreads);
        fftp = fftwf_plan_dft_r2c_3d(N0,N1,N2,buf_in,buf_out,FFTW_ESTIMATE);
        fftwf_execute(fftp);
        fftwf_destroy_plan(fftp);
        fftwf_cleanup_threads();

        #ifdef DOUBLEPRECISION
            #pragma omp parallel for
            for(fourierMode_t mf=0; mf<N_HC; mf++)
                out[mf]=(fftw_complex_t)buf_out[mf];
            p_fftw_free(buf_in, VARNAME(buf_in));
            p_fftw_free(buf_out, VARNAME(buf_out));
        #endif
    #endif
} //FFT_r2c_3d
///-------------------------------------------------------------------------------------
/** @fn FFT_c2r_3d
 * 3D Inverse Fourier transform of a real scalar quantity
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param *in input quantity (fftw_complex_t*)
 * @param *out output quantity (float_t*)
 * @param Nthreads number of threads for parallel computing
 */
void FFT_c2r_3d(int N0, int N1, int N2, fftw_complex_t *in, float_t *out, int Nthreads)
{
    // OUT OF PLACE INVERSE FOURIER TRANSFORMATION FOR HALF COMPLEX INPUT DATA
    // TAKE CARE !! DESTROYS INPUT DATA !! So don't aplly to arrays which have to be reused
    const cellIndex_t N=N0*N1*N2;
    fftw_plan_t fftp;

    #ifdef DOUBLEPRECISION_FFTW
        #ifndef DOUBLEPRECISION
            fftw_complex *buf_in; buf_in = (fftw_complex*)p_fftw_malloc(N*sizeof(fftw_complex), VARNAME(buf_in));
            double *buf_out; buf_out = (double*)p_fftw_malloc(N*sizeof(double), VARNAME(buf_out));
            #pragma omp parallel for
            for(cellIndex_t mc=0; mc<N; mc++)
                buf_in[mc]=(fftw_complex)in[mc];
        #else
            UNUSED(N);
            fftw_complex *buf_in=in; double *buf_out=out;
        #endif

        fftw_init_threads();
        fftw_plan_with_nthreads(Nthreads);
        fftp = fftw_plan_dft_c2r_3d(N0,N1,N2,buf_in,buf_out,FFTW_ESTIMATE);
        fftw_execute(fftp);
        fftw_destroy_plan(fftp);
        fftw_cleanup_threads();

        #ifndef DOUBLEPRECISION
            #pragma omp parallel for
            for(cellIndex_t mc=0; mc<N; mc++)
                out[mc]=(float_t)buf_out[mc];
            p_fftw_free(buf_in, VARNAME(buf_in));
            p_fftw_free(buf_out, VARNAME(buf_out));
        #endif
    #else
        #ifdef DOUBLEPRECISION
            fftwf_complex *buf_in; buf_in = (fftwf_complex*)p_fftw_malloc(N*sizeof(fftwf_complex), VARNAME(buf_in));
            float *buf_out; buf_out = (float*)p_fftw_malloc(N*sizeof(float), VARNAME(buf_out));
            #pragma omp parallel for
            for(cellIndex_t mc=0; mc<N; mc++)
                buf_in[mc]=(fftwf_complex)in[mc];
        #else
            UNUSED(N);
            fftwf_complex *buf_in=in; float *buf_out=out;
        #endif

        fftwf_init_threads();
        fftwf_plan_with_nthreads(Nthreads);
        fftp = fftwf_plan_dft_c2r_3d(N0,N1,N2,buf_in,buf_out,FFTW_ESTIMATE);
        fftwf_execute(fftp);
        fftwf_destroy_plan(fftp);
        fftwf_cleanup_threads();

        #ifdef DOUBLEPRECISION
            #pragma omp parallel for
            for(cellIndex_t mc=0; mc<N; mc++)
                out[mc]=(float_t)buf_out[mc];
            p_fftw_free(buf_in, VARNAME(buf_in));
            p_fftw_free(buf_out, VARNAME(buf_out));
        #endif
    #endif

    // DO THE NORMALIZATION
    #pragma omp parallel for schedule(static)
    for(cellIndex_t mc=0; mc<N; mc++)
        out[mc]/=(double)N;
} //FFT_c2r_3d
///-------------------------------------------------------------------------------------
/** @fn get_kmode
 * This routine gets k mode for Fourier
 * space computations (power spectrum...)
 * @param l index of the mode
 * @param N grid size
 * @param L box size
 * @return value of wave number k
 */
double get_kmode(const int l, const int N, const double L)
{
    return 2.*M_PI/L * ((l > N/2) ? (l-N) : l);
} //get_kmode
///-------------------------------------------------------------------------------------
/** @fn get_ksquared
 * This routine returns the squared modulus of a k mode
 * @param mf0 index of the mode x
 * @param mf1 index of the mode y
 * @param mf2 index of the mode z
 * @param N0 grid size x
 * @param N1 grid size y
 * @param N2 grid size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 * @return |k|^2
 */
double get_ksquared(const int mf0, const int mf1, const int mf2, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2)
{
    double k0=get_kmode(mf0,N0,L0);
    double k1=get_kmode(mf1,N1,L1);
    double k2=get_kmode(mf2,N2,L2);
    double kmod=k0*k0+k1*k1+k2*k2;
    return kmod;
} //get_k_modulus
///-------------------------------------------------------------------------------------
/** @fn get_kmodulus
 * This routine returns the modulus of a k mode
 * @param mf0 index of the mode x
 * @param mf1 index of the mode y
 * @param mf2 index of the mode z
 * @param N0 grid size x
 * @param N1 grid size y
 * @param N2 grid size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 * @return |k|
 */
double get_kmodulus(const int mf0, const int mf1, const int mf2, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2)
{
    double ksquared=get_ksquared(mf0,mf1,mf2,N0,N1,N2,L0,L1,L2);
    return sqrt(ksquared);
} //get_kmodulus
///-------------------------------------------------------------------------------------
/** @fn get_kmax
 * This routine returns the maximum k mode of the grid
 * @param N0 grid size x
 * @param N1 grid size y
 * @param N2 grid size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 * @return maximum k
 */
double get_kmax(const int N0, const int N1, const int N2, const double L0, const double L1, const double L2)
{
    int mf0=N0/2;
    int mf1=N1/2;
    int mf2=N2/2;
    return get_kmodulus(mf0,mf1,mf2,N0,N1,N2,L0,L1,L2);
} //get_kmax
///-------------------------------------------------------------------------------------
/** @fn get_knyq
 * This routine returns the Nyquist wavenumber of the grid
 * @param N0 grid size x
 * @param N1 grid size y
 * @param N2 grid size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 * @return knyq
 */
double get_knyq(const int N0, const int N1, const int N2, const double L0, const double L1, const double L2)
{
    int mf0=N0/2;
    int mf1=N1/2;
    int mf2=N2/2;
    double k0=get_kmode(mf0,N0,L0);
    double k1=get_kmode(mf1,N1,L1);
    double k2=get_kmode(mf2,N2,L2);
    return 1./3.*(k0+k1+k2);
} //get_knyq
///-------------------------------------------------------------------------------------
/** @fn get_kmode_grad_DFT
 * This routine returns the gradient in Fourier space for a given k mode
 * @param mf0 index of the mode x
 * @param mf1 index of the mode y
 * @param mf2 index of the mode z
 * @param N0 grid size x
 * @param N1 grid size y
 * @param N2 grid size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 * @param d0 mesh spacing x
 * @param d1 mesh spacing y
 * @param d2 mesh spacing z
 * @param axis axis of gradient
 * @return grad
 */
double get_kmode_grad_DFT(const int mf0, const int mf1, const int mf2, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2, const double d0, const double d1, const double d2, const int axis)
{
    double k, grad;
    switch(axis)
    {
        case 0:
            k=get_kmode(mf0,N0,L0);
            grad=gradient_DFT_1d(k,d0);
        break;
        case 1:
            k=get_kmode(mf1,N1,L1);
            grad=gradient_DFT_1d(k,d1);
        break;
        case 2:
            k=get_kmode(mf2,N2,L2);
            grad=gradient_DFT_1d(k,d2);
        break;
    }
    return grad;
} //get_kmode_grad_DFT
///-------------------------------------------------------------------------------------
/** @fn get_FourierMode_Id
 * Gets Fourier mode Id, row major
 * @param l index x
 * @param m index y
 * @param n index z
 * @param N0 grid size x
 * @param N1 grid size y
 * @param N2 grid size z
 */
fourierMode_t get_FourierMode_Id(const int mf0, const int mf1, const int mf2, const int N0, const int N1, const int N2)
{
    const int N2_HC=(N2/2+1);
    return (fourierMode_t)get_index(mf0,mf1,mf2,N0,N1,N2_HC);
} //get_FourierMode_Id
///-------------------------------------------------------------------------------------
/** @fn get_FourierGrid
 * Returns the values of k modes on the entire Fourier grid
 * @param *FOURIERGRID output Fourier grid
 * @param N0 grid size x
 * @param N1 grid size y
 * @param N2 grid size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 */
void get_FourierGrid(float_t *FOURIERGRID, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2)
{
    const int N2_HC=(N2/2+1);
    #pragma omp parallel for schedule(static) collapse(3)
    for(int mf0=0; mf0<N0; mf0++)
        for(int mf1=0; mf1<N1; mf1++)
            for(int mf2=0; mf2<N2_HC; mf2++)
            {
                // calculate modulus of current k vector
                fourierMode_t mf = get_FourierMode_Id(mf0,mf1,mf2,N0,N1,N2);
                float_t kmod = (float_t)get_kmodulus(mf0,mf1,mf2,N0,N1,N2,L0,L1,L2);
                FOURIERGRID[mf] = kmod;
            }
} //get_FourierGrid
///-------------------------------------------------------------------------------------
/** @fn get_FourierGrid_from_k_modes
 * Returns the values of k modes on the entire Fourier grid
 * @param Nk number of k_modes
 * @param k_modes input k modes
 * @param kmax input k_max
 * @param *k_keys output k_keys
 * @param k_nmodes output k_nmodes
 * @param N0 grid size x
 * @param N1 grid size y
 * @param N2 grid size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 */
void get_FourierGrid_from_k_modes(const int Nk, const float_t k_modes[Nk], const float_t kmax, int *k_keys, int k_nmodes[Nk], const int N0, const int N1, const int N2, const double L0, const double L1, const double L2)
{
    const int Nthreads = omp_get_max_threads();
    const int N2_HC=(N2/2+1);

    // initialize arrays
    double AUX_k_nmodes[Nthreads][Nk]; bool threadActivated[Nthreads];
    for(int t=0; t<Nthreads; t++)
        threadActivated[t]=false;
    #pragma omp parallel for schedule(static)
    for(int ik=0; ik<Nk; ik++)
    {
        k_nmodes[ik]=0;
        for(int t=0; t<Nthreads; t++)
        {
            AUX_k_nmodes[t][ik]=0;
        }
    }

    // fill k_keys and k_nmodes in parallel
    #pragma omp parallel
    {
        int t = omp_get_thread_num(); // get rank of current process
        threadActivated[t] = true;

        #pragma omp for schedule(static) collapse(3)
        for(int mf0=0; mf0<N0; mf0++)
            for(int mf1=0; mf1<N1; mf1++)
                for(int mf2=0; mf2<N2_HC; mf2++)
                {
                    // calculate modulus of current k vector
                    fourierMode_t mf = get_FourierMode_Id(mf0,mf1,mf2,N0,N1,N2);
                    float_t kmod = (float_t)get_kmodulus(mf0,mf1,mf2,N0,N1,N2,L0,L1,L2);

                    // look-up k mode bin in k_modes
                    int ik = Nk;
                    if(kmax > kmod)
                    {
                        ik--;
                        while(ik>-1 && k_modes[ik] > kmod)
                            ik--;
                    }

                    // write index in k_keys and add 1 to the count
                    k_keys[mf] = ik;
                    if(ik>-1 && ik<Nk)
                        AUX_k_nmodes[t][ik]++;
                }
    }

    // perform the reduction for k_nmodes
    #pragma omp for schedule(static)
    for(int ik=0; ik<Nk; ik++)
        for(int t=0; t<Nthreads; t++)
        {
            if (!threadActivated[t])
                continue;
            k_nmodes[ik]+=AUX_k_nmodes[t][ik];
        }
} //get_FourierGrid_from_k_modes
///-------------------------------------------------------------------------------------
/** @fn sharpk_filter_func
 * subroutine of sharpk_filter
 */
static inline float_t sharpk_filter_func(const double k, const double k_cut, const bool lowpass)
{
    if(lowpass)
        return k<=k_cut ? 1. : 0.;
    else
        return k>=k_cut ? 1. : 0.;
} //sharpk_filter_func
///-------------------------------------------------------------------------------------
/** @fn sharpk_filter
 * Applies a sharp-k low-pass or high-pass filter to a scalar field
 * @param *OLD input field
 * @param *NEW output field
 * @param N0 grid size x
 * @param N1 grid size y
 * @param N2 grid size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 * @param k_cut scale of the filter
 * @param lowpass low-pass if true, high-pass if false
 */
void sharpk_filter(const float_t *OLD, float_t *NEW, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2, const double k_cut, const bool lowpass)
{
    const int Nthreads = omp_get_max_threads();
    const int N_HC=N0*N1*(N2/2+1);
    const int N2_HC=(N2/2+1);

    // transform field to Fourier space
    fftw_complex_t *AUX; AUX = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(AUX));
    FFT_r2c_3d(N0, N1, N2, (float_t*)OLD, AUX, Nthreads);

    // convolve with the sharp-k low pass or high-pass filter kernel
    #pragma omp parallel for schedule(static) collapse(3)
    for(int mf0=0; mf0<N0; mf0++)
        for(int mf1=0; mf1<N1; mf1++)
            for(int mf2=0; mf2<N2_HC; mf2++)
            {
                // Calculates k-mode
                fourierMode_t mf = get_FourierMode_Id(mf0,mf1,mf2,N0,N1,N2);
                double k = get_kmodulus(mf0,mf1,mf2,N0,N1,N2,L0,L1,L2);

                AUX[mf] *= sharpk_filter_func(k,k_cut,lowpass);
            }
    // Jeans's swindle
    AUX[0]=0.;

    // transform back to real space
    FFT_c2r_3d(N0, N1, N2, AUX, NEW, Nthreads);

    // free memory
    p_fftw_free(AUX, VARNAME(AUX));
} //sharpk_filter
///-------------------------------------------------------------------------------------
/** @fn sharpk_filter
 * Applies a sharp-k low-pass or high-pass filter to a scalar field, in place
 * @param *FIELD field
 * @param N0 grid size x
 * @param N1 grid size y
 * @param N2 grid size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 * @param k_cut scale of the filter
 * @param lowpass low-pass if true, high-pass if false
 */
void sharpk_filter_inplace(float_t *FIELD, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2, const double k_cut, const bool lowpass)
{
    const cellIndex_t N=N0*N1*N2;
    float_t TMP[N];
    sharpk_filter(FIELD, TMP, N0, N1, N2, L0, L1, L2, k_cut, lowpass);

    #pragma omp parallel for schedule(static)
    for(cellIndex_t mc=0; mc<N; mc++)
        FIELD[mc]=TMP[mc];
} //sharpk_filter_inplace
///-------------------------------------------------------------------------------------
/** @fn gaussian_smoother
 * Smoothes a field at a particular scale using a Gaussian filter
 * @param *OLD input field
 * @param *NEW output field
 * @param N0 grid size x
 * @param N1 grid size y
 * @param N2 grid size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 * @param rsmooth radius of the Gaussian filter
 */
void gaussian_smoother(const float_t *OLD, float_t *NEW, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2, const double rsmooth)
{
    const int Nthreads = omp_get_max_threads();
    const int N_HC=N0*N1*(N2/2+1);
    const int N2_HC=(N2/2+1);
    const double ksmooth=2*M_PI/rsmooth;

    // transform field to Fourier space
    fftw_complex_t *AUX; AUX = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(AUX));
    FFT_r2c_3d(N0, N1, N2, (float_t*)OLD, AUX, Nthreads);

    // convolve with the Gaussian filter kernel
    #pragma omp parallel for schedule(static) collapse(3)
    for(int mf0=0; mf0<N0; mf0++)
        for(int mf1=0; mf1<N1; mf1++)
            for(int mf2=0; mf2<N2_HC; mf2++)
            {
                // Calculates k-mode
                fourierMode_t mf = get_FourierMode_Id(mf0,mf1,mf2,N0,N1,N2);
                double ksquared = get_ksquared(mf0,mf1,mf2,N0,N1,N2,L0,L1,L2);
                double ksmoothsquared = ksmooth*ksmooth;
                double gauss = exp(-0.5*ksquared/ksmoothsquared);

                AUX[mf] *= gauss;
            }
    // Jeans's swindle
    AUX[0]=0.;

    // transform back to real space
    FFT_c2r_3d(N0, N1, N2, AUX, NEW, Nthreads);

    // free memory
    p_fftw_free(AUX, VARNAME(AUX));
} //gaussian_smoother
///-------------------------------------------------------------------------------------
/** @fn gaussian_smoother_inplace
 * Smoothes a field at a particular scale using a Gaussian filter, in place
 * @param *FIELD field
 * @param N0 grid size x
 * @param N1 grid size y
 * @param N2 grid size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 * @param rsmooth radius of the Gaussian filter
 */
void gaussian_smoother_inplace(float_t *FIELD, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2, const double rsmooth)
{
    const cellIndex_t N=N0*N1*N2;
    float_t TMP[N];
    gaussian_smoother(FIELD, TMP, N0, N1, N2, L0, L1, L2, rsmooth);

    #pragma omp parallel for schedule(static)
    for(cellIndex_t mc=0; mc<N; mc++)
        FIELD[mc]=TMP[mc];
} //gaussian_smoother_inplace
///-------------------------------------------------------------------------------------
/** @fn boxcar_smoother
 * Smoothes a field using a boxcar filter of one cell width
 * @param *OLD input field
 * @param *NEW output field
 * @param N0 grid size x
 * @param N1 grid size y
 * @param N2 grid size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 * @param rsmooth radius of the filter
 */
void boxcar_smoother(const float_t *OLD, float_t *NEW, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2, const double rsmooth)
{
    UNUSED(L0); UNUSED(L1); UNUSED(L2); UNUSED(rsmooth);
    const cellIndex_t N=N0*N1*N2;

    #pragma omp parallel for schedule(static)
    for(cellIndex_t mc=0; mc<N; mc++)
    {
        int mc_indices[3], mc0,mc1,mc2, mc0m,mc1m,mc2m, mc0p,mc1p,mc2p;
        cellIndex_t mcN01,mcN02,mcN03,mcN04,mcN05,mcN06,mcN07,mcN08,mcN09,mcN10,mcN11,mcN12,mcN13,mcN14,mcN15,mcN16,mcN17,mcN18,mcN19,mcN20,mcN21,mcN22,mcN23,mcN24,mcN25,mcN26,mcN27;

        get_indices(mc, N0, N1, N2, mc_indices);
        mc0  = mc_indices[0];
        mc1  = mc_indices[1];
        mc2  = mc_indices[2];
        mc0m = (int)p_mod(mc0-1, N0);
        mc1m = (int)p_mod(mc1-1, N1);
        mc2m = (int)p_mod(mc2-1, N2);
        mc0p = (int)p_mod(mc0+1, N0);
        mc1p = (int)p_mod(mc1+1, N1);
        mc2p = (int)p_mod(mc2+1, N2);

        mcN01 = get_index(mc0,mc1,mc2,N0,N1,N2);
        mcN02 = get_index(mc0p,mc1,mc2,N0,N1,N2);
        mcN03 = get_index(mc0m,mc1,mc2,N0,N1,N2);
        mcN04 = get_index(mc0,mc1m,mc2,N0,N1,N2);
        mcN05 = get_index(mc0p,mc1m,mc2,N0,N1,N2);
        mcN06 = get_index(mc0m,mc1m,mc2,N0,N1,N2);
        mcN07 = get_index(mc0,mc1p,mc2,N0,N1,N2);
        mcN08 = get_index(mc0p,mc1p,mc2,N0,N1,N2);
        mcN09 = get_index(mc0m,mc1p,mc2,N0,N1,N2);

        mcN10 = get_index(mc0,mc1,mc2m,N0,N1,N2);
        mcN11 = get_index(mc0p,mc1,mc2m,N0,N1,N2);
        mcN12 = get_index(mc0m,mc1,mc2m,N0,N1,N2);
        mcN13 = get_index(mc0,mc1m,mc2m,N0,N1,N2);
        mcN14 = get_index(mc0p,mc1m,mc2m,N0,N1,N2);
        mcN15 = get_index(mc0m,mc1m,mc2m,N0,N1,N2);
        mcN16 = get_index(mc0,mc1p,mc2m,N0,N1,N2);
        mcN17 = get_index(mc0p,mc1p,mc2m,N0,N1,N2);
        mcN18 = get_index(mc0m,mc1p,mc2m,N0,N1,N2);

        mcN19 = get_index(mc0,mc1,mc2p,N0,N1,N2);
        mcN20 = get_index(mc0p,mc1,mc2p,N0,N1,N2);
        mcN21 = get_index(mc0m,mc1,mc2p,N0,N1,N2);
        mcN22 = get_index(mc0,mc1m,mc2p,N0,N1,N2);
        mcN23 = get_index(mc0p,mc1m,mc2p,N0,N1,N2);
        mcN24 = get_index(mc0m,mc1m,mc2p,N0,N1,N2);
        mcN25 = get_index(mc0,mc1p,mc2p,N0,N1,N2);
        mcN26 = get_index(mc0p,mc1p,mc2p,N0,N1,N2);
        mcN27 = get_index(mc0m,mc1p,mc2p,N0,N1,N2);

        NEW[mc] = (OLD[mcN01]+OLD[mcN02]+OLD[mcN03]+OLD[mcN04]+OLD[mcN05]+OLD[mcN06]+OLD[mcN07]+OLD[mcN08]+OLD[mcN09]+OLD[mcN10]+OLD[mcN11]+OLD[mcN12]+OLD[mcN13]+OLD[mcN14]+OLD[mcN15]+OLD[mcN16]+OLD[mcN17]+OLD[mcN18]+OLD[mcN19]+OLD[mcN20]+OLD[mcN21]+OLD[mcN22]+OLD[mcN23]+OLD[mcN24]+OLD[mcN25]+OLD[mcN26]+OLD[mcN27])/27.;
    }
} //boxcar_smoother
///-------------------------------------------------------------------------------------
/** @fn boxcar_smoother_inplace
 * Smoothes a field using a boxcar filter of one cell width, in place
 * @param *FIELD field
 * @param N0 grid size x
 * @param N1 grid size y
 * @param N2 grid size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 * @param rsmooth radius of the filter
 */
void boxcar_smoother_inplace(float_t *FIELD, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2, const double rsmooth)
{
    UNUSED(L0); UNUSED(L1); UNUSED(L2); UNUSED(rsmooth);
    const cellIndex_t N=N0*N1*N2;
    float_t TMP[N];
    boxcar_smoother(FIELD, TMP, N0, N1, N2, L0, L1, L2, rsmooth);

    #pragma omp parallel for schedule(static)
    for(cellIndex_t mc=0; mc<N; mc++)
        FIELD[mc]=TMP[mc];
} //boxcar_smoother_inplace
///-------------------------------------------------------------------------------------
/** @fn gradient_of_scalar
 * Applies gradient operator to a scalar field, applies inverse Laplacian if required
 * @param *SCALAR input: scalar field on the configuration-space grid
 * @param *VECTOR0 output: vector field x component
 * @param *VECTOR1 output: vector field y component
 * @param *VECTOR2 output: vector field z component
 * @param inverseLaplacian boolean: multiply by 1/Laplacian_operator if true
 * @param N0 grid size x
 * @param N1 grid size y
 * @param N2 grid size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 */
void gradient_of_scalar(const float_t *SCALAR, float_t *VECTOR0, float_t *VECTOR1, float_t *VECTOR2, const bool inverseLaplacian, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2)
{
    int Nthreads = omp_get_max_threads();
    const int N_HC = N0*N1*(N2/2+1);
    const int N2_HC=(N2/2+1);
    const double d0=L0/(double)N0;
    const double d1=L1/(double)N1;
    const double d2=L2/(double)N2;

    // transform scalar field to Fourier space
    fftw_complex_t *AUX_SCALAR, *AUX_VECTOR0, *AUX_VECTOR1, *AUX_VECTOR2;
    AUX_SCALAR = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(AUX_SCALAR));
    AUX_VECTOR0 = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(AUX_VECTOR0));
    AUX_VECTOR1 = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(AUX_VECTOR1));
    AUX_VECTOR2 = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(AUX_VECTOR2));
    FFT_r2c_3d(N0, N1, N2, (float_t*)SCALAR, AUX_SCALAR, Nthreads);

    #pragma omp parallel for schedule(static) collapse(3)
    for(int mf0=0; mf0<N0; mf0++)
        for(int mf1=0; mf1<N1; mf1++)
            for(int mf2=0; mf2<N2_HC; mf2++)
            {
                double k0,k1,k2;

                // Calculate k-mode
                k0=get_kmode(mf0,N0,L0);
                k1=get_kmode(mf1,N1,L1);
                k2=get_kmode(mf2,N2,L2);
                fourierMode_t mf = get_FourierMode_Id(mf0,mf1,mf2,N0,N1,N2);

                // Applies gradient operator in Fourier space
                AUX_VECTOR0[mf] = I*k0*AUX_SCALAR[mf];
                AUX_VECTOR1[mf] = I*k1*AUX_SCALAR[mf];
                AUX_VECTOR2[mf] = I*k2*AUX_SCALAR[mf];

                // Invert Laplacian operator if required
                if(inverseLaplacian)
                {
                    double inv_laplace_DFT = 1./laplace_DFT(k0,k1,k2,d0,d1,d2);
                    AUX_VECTOR0[mf] *= inv_laplace_DFT;
                    AUX_VECTOR1[mf] *= inv_laplace_DFT;
                    AUX_VECTOR2[mf] *= inv_laplace_DFT;

                    // put the Nyquist planes to zero by hand
                    if(mf0 == N0/2)
                        AUX_VECTOR0[mf] = 0.;
                    if(mf1 == N1/2)
                        AUX_VECTOR1[mf] = 0.;
                    if(mf2 == N2/2)
                        AUX_VECTOR2[mf] = 0.;
                }
            }
    AUX_VECTOR0[0]=0.;
    AUX_VECTOR1[0]=0.;
    AUX_VECTOR2[0]=0.;

    // transform back to real space
    FFT_c2r_3d(N0, N1, N2, AUX_VECTOR0, VECTOR0, Nthreads);
    FFT_c2r_3d(N0, N1, N2, AUX_VECTOR1, VECTOR1, Nthreads);
    FFT_c2r_3d(N0, N1, N2, AUX_VECTOR2, VECTOR2, Nthreads);

    // free memory
    p_fftw_free(AUX_SCALAR, VARNAME(AUX_SCALAR));
    p_fftw_free(AUX_VECTOR0, VARNAME(AUX_VECTOR0));
    p_fftw_free(AUX_VECTOR1, VARNAME(AUX_VECTOR1));
    p_fftw_free(AUX_VECTOR2, VARNAME(AUX_VECTOR2));
} //gradient_of_scalar
///-------------------------------------------------------------------------------------
/** @fn shear_of_scalar
 * Applies shear operator to a scalar field
 * @param *SCALAR input: scalar field on the configuration-space grid
 * @param *T00 output shear
 * @param *T11 output shear
 * @param *T22 output shear
 * @param *T01 output shear
 * @param *T02 output shear
 * @param *T12 output shear
 * @param N0 grid size x
 * @param N1 grid size y
 * @param N2 grid size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 */
void shear_of_scalar(const float_t *SCALAR, float_t *T00, float_t *T11, float_t *T22, float_t *T01, float_t *T02, float_t *T12, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2)
{
    int Nthreads = omp_get_max_threads();
    const int N_HC = N0*N1*(N2/2+1);
    const int N2_HC=(N2/2+1);

    // transform scalar field to Fourier space
    fftw_complex_t *AUX; AUX = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(AUX));
    FFT_r2c_3d(N0, N1, N2, (float_t*)SCALAR, AUX, Nthreads);

    for(int t=0; t<9; t++)
    {
        // take care this array will be destroyed in every backward transformation
        // make sure it is never used again !!
        fftw_complex_t *AUX1; AUX1 = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(AUX1));

        #pragma omp parallel for collapse(3)
    for(int mf0=0; mf0<N0; mf0++)
        for(int mf1=0; mf1<N1; mf1++)
            for(int mf2=0; mf2<N2_HC; mf2++)
                {
                    double k0,k1,k2,ksquared,kakb=0.;
                    fourierMode_t mf = get_FourierMode_Id(mf0,mf1,mf2,N0,N1,N2);
                    k0=get_kmode(mf0,N0,L0);
                    k1=get_kmode(mf1,N1,L1);
                    k2=get_kmode(mf2,N2,L2);
                    ksquared=k0*k0+k1*k1+k2*k2;

                    if(t==0) kakb=k0*k0;
                    if(t==1) kakb=k1*k1;
                    if(t==2) kakb=k2*k2;
                    if(t==3) kakb=k0*k1;
                    if(t==4) kakb=k0*k2;
                    if(t==5) kakb=k2*k1;

                    AUX1[mf]=AUX[mf]*kakb/ksquared;
                }
        // Jeans' swindle: set zero-mode by hand
        AUX1[0]=0.;

        if(t==0) FFT_c2r_3d(N0, N1, N2, AUX1, T00, Nthreads);
        if(t==1) FFT_c2r_3d(N0, N1, N2, AUX1, T11, Nthreads);
        if(t==2) FFT_c2r_3d(N0, N1, N2, AUX1, T22, Nthreads);
        if(t==3) FFT_c2r_3d(N0, N1, N2, AUX1, T01, Nthreads);
        if(t==4) FFT_c2r_3d(N0, N1, N2, AUX1, T02, Nthreads);
        if(t==5) FFT_c2r_3d(N0, N1, N2, AUX1, T12, Nthreads);

        p_fftw_free(AUX1, VARNAME(AUX1));
    }

    // free memory
    p_fftw_free(AUX, VARNAME(AUX));
} //shear_of_scalar
///-------------------------------------------------------------------------------------
/** @fn divergence_of_vector
 * Computes the divergence of a vector field, applies inverse Laplacian if required
 * @param *VECTOR0 input: vector field x component
 * @param *VECTOR1 input: vector field y component
 * @param *VECTOR2 input: vector field z component
 * @param *SCALAR output: scalar field
 * @param inverseLaplacian boolean: multiply by 1/Laplacian_operator if true
 * @param N0 grid size x
 * @param N1 grid size y
 * @param N2 grid size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 */
void divergence_of_vector(const float_t *VECTOR0, const float_t *VECTOR1, const float_t *VECTOR2, float_t *SCALAR, const bool inverseLaplacian, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2)
{
    int Nthreads = omp_get_max_threads();
    const int N_HC = N0*N1*(N2/2+1);
    const int N2_HC=(N2/2+1);
    const double d0=L0/(double)N0;
    const double d1=L1/(double)N1;
    const double d2=L2/(double)N2;

    // transform vector field to Fourier space
    fftw_complex_t *AUX_VECTOR0, *AUX_VECTOR1, *AUX_VECTOR2;
    AUX_VECTOR0 = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(AUX_VECTOR0));
    AUX_VECTOR1 = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(AUX_VECTOR1));
    AUX_VECTOR2 = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(AUX_VECTOR2));
    FFT_r2c_3d(N0, N1, N2, (float_t*)VECTOR0, AUX_VECTOR0, Nthreads);
    FFT_r2c_3d(N0, N1, N2, (float_t*)VECTOR1, AUX_VECTOR1, Nthreads);
    FFT_r2c_3d(N0, N1, N2, (float_t*)VECTOR2, AUX_VECTOR2, Nthreads);
    fftw_complex_t *AUX_SCALAR; AUX_SCALAR = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(AUX_SCALAR));

    #pragma omp parallel for schedule(static) collapse(3)
    for(int mf0=0; mf0<N0; mf0++)
        for(int mf1=0; mf1<N1; mf1++)
            for(int mf2=0; mf2<N2_HC; mf2++)
            {
                double k0,k1,k2;

                // Calculate k-mode
                k0=get_kmode(mf0,N0,L0);
                k1=get_kmode(mf1,N1,L1);
                k2=get_kmode(mf2,N2,L2);
                fourierMode_t mf = get_FourierMode_Id(mf0,mf1,mf2,N0,N1,N2);

                // Invert Laplacian operator if required
                if(inverseLaplacian)
                {
                    double inv_laplace_DFT = 1./laplace_DFT(k0,k1,k2,d0,d1,d2);
                    AUX_VECTOR0[mf] *= inv_laplace_DFT;
                    AUX_VECTOR1[mf] *= inv_laplace_DFT;
                    AUX_VECTOR2[mf] *= inv_laplace_DFT;

                    // put the Nyquist planes to zero by hand
                    if(mf0 == N0/2)
                        AUX_VECTOR0[mf] = 0.;
                    if(mf1 == N1/2)
                        AUX_VECTOR1[mf] = 0.;
                    if(mf2 == N2/2)
                        AUX_VECTOR2[mf] = 0.;
                }

                // Get divergence in Fourier space
                AUX_SCALAR[mf] = I*k0*AUX_VECTOR0[mf] + I*k1*AUX_VECTOR1[mf] + I*k2*AUX_VECTOR2[mf];
            }
    AUX_SCALAR[0]=0.;

    // transform back to real space
    FFT_c2r_3d(N0, N1, N2, AUX_SCALAR, SCALAR, Nthreads);

    // free memory
    p_fftw_free(AUX_SCALAR, VARNAME(AUX_SCALAR));
    p_fftw_free(AUX_VECTOR0, VARNAME(AUX_VECTOR0));
    p_fftw_free(AUX_VECTOR1, VARNAME(AUX_VECTOR1));
    p_fftw_free(AUX_VECTOR2, VARNAME(AUX_VECTOR2));
} //divergence_of_vector
///-------------------------------------------------------------------------------------
/** @fn rotational_of_vector
 * Computes the divergence of a vector field, applies inverse Laplacian if required
 * @param *VECTOR0 input: vector field x component
 * @param *VECTOR1 input: vector field y component
 * @param *VECTOR2 input: vector field z component
 * @param *ROTATIONAL0 output: rotational x component
 * @param *ROTATIONAL1 output: rotational y component
 * @param *ROTATIONAL2 output: rotational z component
 * @param inverseLaplacian boolean: multiply by 1/Laplacian_operator if true
 * @param N0 grid size x
 * @param N1 grid size y
 * @param N2 grid size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 */
void rotational_of_vector(const float_t *VECTOR0, const float_t *VECTOR1, const float_t *VECTOR2, float_t *ROTATIONAL0, float_t *ROTATIONAL1, float_t *ROTATIONAL2, const bool inverseLaplacian, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2)
{
    int Nthreads = omp_get_max_threads();
    const int N_HC = N0*N1*(N2/2+1);
    const int N2_HC=(N2/2+1);
    const double d0=L0/(double)N0;
    const double d1=L1/(double)N1;
    const double d2=L2/(double)N2;

    // transform vector field to Fourier space
    fftw_complex_t *AUX_VECTOR0, *AUX_VECTOR1, *AUX_VECTOR2;
    AUX_VECTOR0 = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(AUX_VECTOR0));
    AUX_VECTOR1 = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(AUX_VECTOR1));
    AUX_VECTOR2 = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(AUX_VECTOR2));
    FFT_r2c_3d(N0, N1, N2, (float_t*)VECTOR0, AUX_VECTOR0, Nthreads);
    FFT_r2c_3d(N0, N1, N2, (float_t*)VECTOR1, AUX_VECTOR1, Nthreads);
    FFT_r2c_3d(N0, N1, N2, (float_t*)VECTOR2, AUX_VECTOR2, Nthreads);
    fftw_complex_t *AUX_ROTATIONAL0, *AUX_ROTATIONAL1, *AUX_ROTATIONAL2;
    AUX_ROTATIONAL0 = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(AUX_ROTATIONAL0));
    AUX_ROTATIONAL1 = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(AUX_ROTATIONAL1));
    AUX_ROTATIONAL2 = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(AUX_ROTATIONAL2));

    #pragma omp parallel for schedule(static) collapse(3)
    for(int mf0=0; mf0<N0; mf0++)
        for(int mf1=0; mf1<N1; mf1++)
            for(int mf2=0; mf2<N2_HC; mf2++)
            {
                double k0,k1,k2;

                // Calculate k-mode
                k0=get_kmode(mf0,N0,L0);
                k1=get_kmode(mf1,N1,L1);
                k2=get_kmode(mf2,N2,L2);
                fourierMode_t mf = get_FourierMode_Id(mf0,mf1,mf2,N0,N1,N2);

                // Get rotational in Fourier space
                AUX_ROTATIONAL0[mf] = I*k1*AUX_VECTOR2[mf] - I*k2*AUX_VECTOR1[mf];
                AUX_ROTATIONAL1[mf] = I*k2*AUX_VECTOR0[mf] - I*k0*AUX_VECTOR2[mf];
                AUX_ROTATIONAL2[mf] = I*k0*AUX_VECTOR1[mf] - I*k1*AUX_VECTOR0[mf];

                // Invert Laplacian operator if required
                if(inverseLaplacian)
                {
                    double inv_laplace_DFT = 1./laplace_DFT(k0,k1,k2,d0,d1,d2);
                    AUX_ROTATIONAL0[mf] *= inv_laplace_DFT;
                    AUX_ROTATIONAL1[mf] *= inv_laplace_DFT;
                    AUX_ROTATIONAL2[mf] *= inv_laplace_DFT;

                    // put the Nyquist planes to zero by hand
                    if(mf0 == N0/2)
                        AUX_ROTATIONAL0[mf] = 0.;
                    if(mf1 == N1/2)
                        AUX_ROTATIONAL1[mf] = 0.;
                    if(mf2 == N2/2)
                        AUX_ROTATIONAL2[mf] = 0.;
                }
            }
    AUX_ROTATIONAL0[0]=0.;
    AUX_ROTATIONAL1[0]=0.;
    AUX_ROTATIONAL2[0]=0.;

    // transform back to real space
    FFT_c2r_3d(N0, N1, N2, AUX_ROTATIONAL0, ROTATIONAL0, Nthreads);
    FFT_c2r_3d(N0, N1, N2, AUX_ROTATIONAL1, ROTATIONAL1, Nthreads);
    FFT_c2r_3d(N0, N1, N2, AUX_ROTATIONAL2, ROTATIONAL2, Nthreads);

    // free memory
    p_fftw_free(AUX_VECTOR0, VARNAME(AUX_VECTOR0));
    p_fftw_free(AUX_VECTOR1, VARNAME(AUX_VECTOR1));
    p_fftw_free(AUX_VECTOR2, VARNAME(AUX_VECTOR2));
    p_fftw_free(AUX_ROTATIONAL0, VARNAME(AUX_ROTATIONAL0));
    p_fftw_free(AUX_ROTATIONAL1, VARNAME(AUX_ROTATIONAL1));
    p_fftw_free(AUX_ROTATIONAL2, VARNAME(AUX_ROTATIONAL2));
} //rotational_of_vector
///-------------------------------------------------------------------------------------
/** @fn vectorpotential_of_vector
 * Computes the vector potential of the curl part of a vector field
 * @param *VECTOR0 input: vector field x component
 * @param *VECTOR1 input: vector field y component
 * @param *VECTOR2 input: vector field z component
 * @param *VECTOR_POTENTIAL0 output: vector potential field x component
 * @param *VECTOR_POTENTIAL1 output: vector potential field y component
 * @param *VECTOR_POTENTIAL2 output: vector potential field z component
 * @param N0 grid size x
 * @param N1 grid size y
 * @param N2 grid size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 */
void vectorpotential_of_vector(const float_t *VECTOR0, const float_t *VECTOR1, const float_t *VECTOR2, float_t *VECTOR_POTENTIAL0, float_t *VECTOR_POTENTIAL1, float_t *VECTOR_POTENTIAL2, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2)
{
    rotational_of_vector(VECTOR0, VECTOR1, VECTOR2, VECTOR_POTENTIAL0, VECTOR_POTENTIAL1, VECTOR_POTENTIAL2, true, N0, N1, N2, L0, L1, L2);
} //vectorpotential_of_vector
///-------------------------------------------------------------------------------------
/** @fn curlpart_of_vector
 * Computes the curl part of a vector field
 * @param *VECTOR0 input: vector field x component
 * @param *VECTOR1 input: vector field y component
 * @param *VECTOR2 input: vector field z component
 * @param *CURL_PART0 output: vector potential field x component
 * @param *CURL_PART1 output: vector potential field y component
 * @param *CURL_PART2 output: vector potential field z component
 * @param N0 grid size x
 * @param N1 grid size y
 * @param N2 grid size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 */
void curlpart_of_vector(const float_t *VECTOR0, const float_t *VECTOR1, const float_t *VECTOR2, float_t *CURL_PART0, float_t *CURL_PART1, float_t *CURL_PART2, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2)
{
    int Nthreads = omp_get_max_threads();
    const int N_HC = N0*N1*(N2/2+1);
    const int N2_HC=(N2/2+1);

    // transform vector to Fourier space
    fftw_complex_t *AUX_VECTOR0, *AUX_VECTOR1, *AUX_VECTOR2;
    AUX_VECTOR0 = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(AUX_VECTOR0));
    AUX_VECTOR1 = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(AUX_VECTOR1));
    AUX_VECTOR2 = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(AUX_VECTOR2));
    FFT_r2c_3d(N0, N1, N2, (float_t*)VECTOR0, AUX_VECTOR0, Nthreads);
    FFT_r2c_3d(N0, N1, N2, (float_t*)VECTOR1, AUX_VECTOR1, Nthreads);
    FFT_r2c_3d(N0, N1, N2, (float_t*)VECTOR2, AUX_VECTOR2, Nthreads);
    fftw_complex_t *AUX_CURL_PART0, *AUX_CURL_PART1, *AUX_CURL_PART2;
    AUX_CURL_PART0 = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(AUX_CURL_PART0));
    AUX_CURL_PART1 = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(AUX_CURL_PART1));
    AUX_CURL_PART2 = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(AUX_CURL_PART2));

    #pragma omp parallel for schedule(static) collapse(3)
    for(int mf0=0; mf0<N0; mf0++)
        for(int mf1=0; mf1<N1; mf1++)
            for(int mf2=0; mf2<N2_HC; mf2++)
            {
                double k0,k1,k2,ksquared;
                double AUX_VECTOR_POTENTIAL0, AUX_VECTOR_POTENTIAL1, AUX_VECTOR_POTENTIAL2;

                // Calculate k-mode
                k0=get_kmode(mf0,N0,L0);
                k1=get_kmode(mf1,N1,L1);
                k2=get_kmode(mf2,N2,L2);
                ksquared=k0*k0+k1*k1+k2*k2;
                fourierMode_t mf = get_FourierMode_Id(mf0,mf1,mf2,N0,N1,N2);

                // Get vector potential in Fourier space, solving Laplacian A = -I*k x F
                AUX_VECTOR_POTENTIAL0 = (I*k1*AUX_VECTOR2[mf] - I*k2*AUX_VECTOR1[mf])/ksquared;
                AUX_VECTOR_POTENTIAL1 = (I*k2*AUX_VECTOR0[mf] - I*k0*AUX_VECTOR2[mf])/ksquared;
                AUX_VECTOR_POTENTIAL2 = (I*k0*AUX_VECTOR1[mf] - I*k1*AUX_VECTOR0[mf])/ksquared;

                // Get curl part in Fourier space: Ik x A
                AUX_CURL_PART0[mf] = I*k1*AUX_VECTOR_POTENTIAL2 - I*k2*AUX_VECTOR_POTENTIAL1;
                AUX_CURL_PART1[mf] = I*k2*AUX_VECTOR_POTENTIAL0 - I*k0*AUX_VECTOR_POTENTIAL2;
                AUX_CURL_PART2[mf] = I*k0*AUX_VECTOR_POTENTIAL1 - I*k1*AUX_VECTOR_POTENTIAL0;
            }
    AUX_CURL_PART0[0]=0.;
    AUX_CURL_PART1[0]=0.;
    AUX_CURL_PART2[0]=0.;

    // transform back to real space
    FFT_c2r_3d(N0, N1, N2, AUX_CURL_PART0, CURL_PART0, Nthreads);
    FFT_c2r_3d(N0, N1, N2, AUX_CURL_PART1, CURL_PART1, Nthreads);
    FFT_c2r_3d(N0, N1, N2, AUX_CURL_PART2, CURL_PART2, Nthreads);

    // free memory
    p_fftw_free(AUX_VECTOR0, VARNAME(AUX_VECTOR0));
    p_fftw_free(AUX_VECTOR1, VARNAME(AUX_VECTOR1));
    p_fftw_free(AUX_VECTOR2, VARNAME(AUX_VECTOR2));
    p_fftw_free(AUX_CURL_PART0, VARNAME(AUX_CURL_PART0));
    p_fftw_free(AUX_CURL_PART1, VARNAME(AUX_CURL_PART1));
    p_fftw_free(AUX_CURL_PART2, VARNAME(AUX_CURL_PART2));
} //curlpart_of_vector
///-------------------------------------------------------------------------------------
/** @fn normcurlpart_of_vector
 * Computes the curl part of a vector field
 * @param *VECTOR0 input: vector field x component
 * @param *VECTOR1 input: vector field y component
 * @param *VECTOR2 input: vector field z component
 * @param *NORM_CURL_PART output: norm of curl part
 * @param N0 grid size x
 * @param N1 grid size y
 * @param N2 grid size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 */
void normcurlpart_of_vector(const float_t *VECTOR0, const float_t *VECTOR1, const float_t *VECTOR2, float_t *NORM_CURL_PART, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2)
{
    const cellIndex_t N=N0*N1*N2;
    float_t CURL_PART0[N], CURL_PART1[N], CURL_PART2[N];

    curlpart_of_vector(VECTOR0, VECTOR1, VECTOR2, CURL_PART0, CURL_PART1, CURL_PART2, N0, N1, N2, L0, L1, L2);

    // get the norm of the curl part
    #pragma omp parallel for schedule(static)
    for(cellIndex_t mc=0; mc<N; mc++)
    {
        NORM_CURL_PART[mc] = sqrt(CURL_PART0[mc]*CURL_PART0[mc] + CURL_PART1[mc]*CURL_PART1[mc] + CURL_PART2[mc]*CURL_PART2[mc]);
    }
} //normcurlpart_of_vector
///-------------------------------------------------------------------------------------
/** @fn shear_of_vector
 * Calculates shear of a vector field
 * @param *VECTOR0 input: vector field x component
 * @param *VECTOR1 input: vector field y component
 * @param *VECTOR2 input: vector field z component
 * @param *R00 output shear
 * @param *R11 output shear
 * @param *R22 output shear
 * @param *R01 output shear
 * @param *R02 output shear
 * @param *R12 output shear
 * @param *R10 output shear
 * @param *R20 output shear
 * @param *R21 output shear
 * @param symmetric boolean: assume that the shear matrix is symmetric?
 * @param N0 number of particles x
 * @param N1 number of particles y
 * @param N2 number of particles z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 */
void shear_of_vector(const float_t *VECTOR0, const float_t *VECTOR1, const float_t *VECTOR2, float_t *R00, float_t *R11, float_t *R22, float_t *R01, float_t *R02, float_t *R12, float_t *R10, float_t *R20, float_t *R21, const bool symmetric, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2)
{
    int Nthreads = omp_get_max_threads();
    const int N_HC = N0*N1*(N2/2+1);
    const int N2_HC=(N2/2+1);

    // transform vector to Fourier space
    fftw_complex_t *AUX_VECTOR0, *AUX_VECTOR1, *AUX_VECTOR2;
    AUX_VECTOR0 = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(AUX_VECTOR0));
    AUX_VECTOR1 = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(AUX_VECTOR1));
    AUX_VECTOR2 = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(AUX_VECTOR2));
    FFT_r2c_3d(N0, N1, N2, (float_t*)VECTOR0, AUX_VECTOR0, Nthreads);
    FFT_r2c_3d(N0, N1, N2, (float_t*)VECTOR1, AUX_VECTOR1, Nthreads);
    FFT_r2c_3d(N0, N1, N2, (float_t*)VECTOR2, AUX_VECTOR2, Nthreads);

    for(int t=0; t<9; t++)
    {
        if(symmetric == false || t<6)
        {
            // take care this array will be destroyed in every backward transformation
            // make sure it is never used again !!
            fftw_complex_t *AUX; AUX = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(AUX));

            #pragma omp parallel for schedule(static) collapse(3)
            for(int mf0=0; mf0<N0; mf0++)
                for(int mf1=0; mf1<N1; mf1++)
                    for(int mf2=0; mf2<N2_HC; mf2++)
                    {
                        double k0,k1,k2;

                        // Calculate k-mode
                        k0=get_kmode(mf0,N0,L0);
                        k1=get_kmode(mf1,N1,L1);
                        k2=get_kmode(mf2,N2,L2);
                        fourierMode_t mf = get_FourierMode_Id(mf0,mf1,mf2,N0,N1,N2);

                        if(t==0) AUX[mf] = I*k0*AUX_VECTOR0[mf];
                        if(t==1) AUX[mf] = I*k1*AUX_VECTOR1[mf];
                        if(t==2) AUX[mf] = I*k2*AUX_VECTOR2[mf];
                        if(t==3) AUX[mf] = I*k0*AUX_VECTOR1[mf];
                        if(t==4) AUX[mf] = I*k0*AUX_VECTOR2[mf];
                        if(t==5) AUX[mf] = I*k1*AUX_VECTOR2[mf];
                        if(t==6) AUX[mf] = I*k1*AUX_VECTOR0[mf];
                        if(t==7) AUX[mf] = I*k2*AUX_VECTOR0[mf];
                        if(t==8) AUX[mf] = I*k2*AUX_VECTOR1[mf];
                    }
            // Jeans's swindle: set zero-mode by hand
            AUX[0]=0.;

            if(t==0) FFT_c2r_3d(N0, N1, N2, AUX, R00, Nthreads);
            if(t==1) FFT_c2r_3d(N0, N1, N2, AUX, R11, Nthreads);
            if(t==2) FFT_c2r_3d(N0, N1, N2, AUX, R22, Nthreads);
            if(t==3) FFT_c2r_3d(N0, N1, N2, AUX, R01, Nthreads);
            if(t==4) FFT_c2r_3d(N0, N1, N2, AUX, R02, Nthreads);
            if(t==5) FFT_c2r_3d(N0, N1, N2, AUX, R12, Nthreads);
            if(t==6) FFT_c2r_3d(N0, N1, N2, AUX, R10, Nthreads);
            if(t==7) FFT_c2r_3d(N0, N1, N2, AUX, R20, Nthreads);
            if(t==8) FFT_c2r_3d(N0, N1, N2, AUX, R21, Nthreads);

            p_fftw_free(AUX, VARNAME(AUX));
        }
    }

    // free memory
    p_fftw_free(AUX_VECTOR0, VARNAME(AUX_VECTOR0));
    p_fftw_free(AUX_VECTOR1, VARNAME(AUX_VECTOR1));
    p_fftw_free(AUX_VECTOR2, VARNAME(AUX_VECTOR2));
} //shear_of_vector
///-------------------------------------------------------------------------------------
