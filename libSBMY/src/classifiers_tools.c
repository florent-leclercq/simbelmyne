///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/src/classifiers_tools.c
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file classifiers_tools.c
 *  \brief Common routines for web-type classifiers (T-web, DIVA, ORIGAMI)
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2015-2023
 */
#include "classifiers_tools.h"
///-------------------------------------------------------------------------------------
/** @fn classify_object
 * Routine to classify an object given the eigenvalues and a threshold
 * @param EIGVAL0 eigenvalue 0
 * @param EIGVAL1 eigenvalue 1
 * @param EIGVAL2 eigenvalue 2
 * @param EIGVALTH threshold for classification
 * @return classification in {0,1,2,3}
 */
int classify_object(double EIGVAL0, double EIGVAL1, double EIGVAL2, double EIGVALTH)
{
    int OBJNR=0;

    // VOID
    if(EIGVAL2<EIGVALTH) OBJNR=0;
    // SHEET
    else if(EIGVAL2>EIGVALTH && EIGVAL1<EIGVALTH) OBJNR=1;
    // FILAMENT
    else if(EIGVAL1>EIGVALTH && EIGVAL0<EIGVALTH) OBJNR=2;
    // CLUSTER
    else if(EIGVAL0>EIGVALTH) OBJNR=3;

    return OBJNR;
} //classify_object
///-------------------------------------------------------------------------------------
/** @fn structures_l2e
 * Transform structure classification from Lagrangian to Eulerian coordinates
 * @param OBJECTS classification of particles
 * @param P Snapshot containing particle information
 * @param VOIDS output: pdf for voids in Eulerian coordinates
 * @param SHEETS output: pdf for sheets in Eulerian coordinates
 * @param FILAMENTS output: pdf for filaments in Eulerian coordinates
 * @param CLUSTERS output: pdf for clusters in Eulerian coordinates
 * @param Np number of particles
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 */
void structures_l2e(const float_t *OBJECTS, const Snapshot P, float_t *VOIDS, float_t *SHEETS, float_t *FILAMENTS, float_t *CLUSTERS, const particleID_t Np, const int N0, const int N1, const int N2)
{
    PrintMessage(3, "Translating to Eulerian coordinates...");
    INDENT;

    const cellIndex_t N=N0*N1*N2;
    const double L0=P.header.BoxSize;
    const double L1=P.header.BoxSize;
    const double L2=P.header.BoxSize;
    const double d0=L0/N0;
    const double d1=L1/N1;
    const double d2=L2/N2;
    float_t *DENSITY;
    DENSITY = (float_t *)p_malloc(N*sizeof(float_t), VARNAME(DENSITY));

    // Initialize fields
    #pragma omp parallel for
    for(cellIndex_t mc=0; mc<N; mc++)
    {
        VOIDS[mc]=0.;
        SHEETS[mc]=0.;
        FILAMENTS[mc]=0.;
        CLUSTERS[mc]=0.;
        DENSITY[mc]=0.;
    }

    for(particleID_t mp=0; mp<Np; mp++)
    {
        int OBJNR = OBJECTS[mp]; int mc0min,mc0max,mc1min,mc1max,mc2min,mc2max;
        if(OBJNR==0 || OBJNR==4) particle_to_mesh_periodic(P.positions[mp].Pos[0],P.positions[mp].Pos[1],P.positions[mp].Pos[2],1.,VOIDS,N0,N1,N2,d0,d1,d2,&mc0min,&mc0max,&mc1min,&mc1max,&mc2min,&mc2max);
        if(OBJNR==1 || OBJNR==5) particle_to_mesh_periodic(P.positions[mp].Pos[0],P.positions[mp].Pos[1],P.positions[mp].Pos[2],1.,SHEETS,N0,N1,N2,d0,d1,d2,&mc0min,&mc0max,&mc1min,&mc1max,&mc2min,&mc2max);
        if(OBJNR==2 || OBJNR==6) particle_to_mesh_periodic(P.positions[mp].Pos[0],P.positions[mp].Pos[1],P.positions[mp].Pos[2],1.,FILAMENTS,N0,N1,N2,d0,d1,d2,&mc0min,&mc0max,&mc1min,&mc1max,&mc2min,&mc2max);
        if(OBJNR==3 || OBJNR==7) particle_to_mesh_periodic(P.positions[mp].Pos[0],P.positions[mp].Pos[1],P.positions[mp].Pos[2],1.,CLUSTERS,N0,N1,N2,d0,d1,d2,&mc0min,&mc0max,&mc1min,&mc1max,&mc2min,&mc2max);
        particle_to_mesh_periodic(P.positions[mp].Pos[0],P.positions[mp].Pos[1],P.positions[mp].Pos[2],1.,DENSITY,N0,N1,N2,d0,d1,d2,&mc0min,&mc0max,&mc1min,&mc1max,&mc2min,&mc2max);
    }

    #pragma omp parallel for
    for(cellIndex_t mc=0; mc<N; mc++)
    {
        if(DENSITY[mc]>0)
        {
            VOIDS[mc]/=DENSITY[mc];
            SHEETS[mc]/=DENSITY[mc];
            FILAMENTS[mc]/=DENSITY[mc];
            CLUSTERS[mc]/=DENSITY[mc];
        }
    }

    p_free(DENSITY, VARNAME(DENSITY));
    UNINDENT;
    PrintMessage(3, "Translating to Eulerian coordinates done.");
} // structures_l2e
///-------------------------------------------------------------------------------------
