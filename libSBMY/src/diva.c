///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/src/diva.c
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file diva.c
 *  \brief This codes classifies the cosmic web according to the DIVA procedure
 * (Lavaux & Wandelt, MNRAS 403, 5 (2010), arXiv:0906.4101)
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2015-2023
 */
#include "diva.h"
///-------------------------------------------------------------------------------------
/** @fn diva_analysis
 * This routine classifies particles with the DIVA scheme
 * @param *PSI0 input: displacement field
 * @param *PSI1 input: displacement field
 * @param *PSI2 input: displacement field
 * @param *OBJECTS output: classification of particles
 * @param *LAMBDA1 output: eigenvalue of the shear of the displacement
 * @param *LAMBDA2 output: eigenvalue of the shear of the displacement
 * @param *LAMBDA3 output: eigenvalue of the shear of the displacement
 * @param Np0 number of particles x
 * @param Np1 number of particles y
 * @param Np2 number of particles z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 */
void diva_analysis(const float_t *PSI0, const float_t *PSI1, const float_t *PSI2, float_t *OBJECTS, float_t *LAMBDA1, float_t *LAMBDA2, float_t *LAMBDA3, const int Np0, const int Np1, const int Np2, const double L0, const double L1, const double L2)
{
    PrintMessage(3, "DIVA: computing classification of the cosmic web...");
    INDENT;

    particleID_t Np=(particleID_t)Np0*Np1*Np2;
    float_t *R00, *R11, *R22, *R01, *R02, *R12;
    R00 = (float_t *)p_malloc(Np*sizeof(float_t), VARNAME(R00));
    R11 = (float_t *)p_malloc(Np*sizeof(float_t), VARNAME(R11));
    R22 = (float_t *)p_malloc(Np*sizeof(float_t), VARNAME(R22));
    R01 = (float_t *)p_malloc(Np*sizeof(float_t), VARNAME(R01));
    R02 = (float_t *)p_malloc(Np*sizeof(float_t), VARNAME(R02));
    R12 = (float_t *)p_malloc(Np*sizeof(float_t), VARNAME(R12));

    // 1) calculate elements of shear of Lagrangian displacement field tensor
    get_shearPsi_symmetric(PSI0, PSI1, PSI2, R00, R11, R22, R01, R02, R12, Np0, Np1, Np2, L0, L1, L2);

    // 2) calculate eigenvalues and classify objects
    int NBVOIDS=0, NBSHEETS=0, NBFILAMENTS=0, NBCLUSTERS=0;
    #pragma omp parallel for
    for(particleID_t mp=0; mp<Np; mp++)
    {
        // 2 a) calculate eigenvalues from symmetric tensor
        float_t EIGVALS[3];
        get_eigvals_real(R00[mp], R11[mp], R22[mp], R01[mp], R02[mp], R12[mp], EIGVALS);

        // 2 b) classify object
        // the minus sign is important, and one needs to change the ordering here
        int OBJNR=classify_object(-EIGVALS[2], -EIGVALS[1], -EIGVALS[0], 0.);

        // 2 c) count four web-types
        #if DEBUG
        switch(OBJNR)
        {
            case 0: // void
            #pragma omp atomic
            NBVOIDS++;
            break;

            case 1: // sheet
            #pragma omp atomic
            NBSHEETS++;
            break;

            case 2: // filament
            #pragma omp atomic
            NBFILAMENTS++;
            break;

            case 3: // cluster
            #pragma omp atomic
            NBCLUSTERS++;
            break;
        }
        #endif

        // 2 d) store web-type in 3d array
        OBJECTS[mp]=(float_t)OBJNR;

        // 2 e) store eigenvalues
        LAMBDA1[mp]=(float_t)EIGVALS[0];
        LAMBDA2[mp]=(float_t)EIGVALS[1];
        LAMBDA3[mp]=(float_t)EIGVALS[2];
    }
    debugPrintInt(NBVOIDS);
    debugPrintInt(NBSHEETS);
    debugPrintInt(NBFILAMENTS);
    debugPrintInt(NBCLUSTERS);

    p_free(R00, VARNAME(R00));
    p_free(R11, VARNAME(R11));
    p_free(R22, VARNAME(R22));
    p_free(R01, VARNAME(R01));
    p_free(R02, VARNAME(R02));
    p_free(R12, VARNAME(R12));

    UNINDENT;
    PrintMessage(3, "DIVA: computing classification of the cosmic web done.");
} //diva_analysis
///-------------------------------------------------------------------------------------
