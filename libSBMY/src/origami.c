///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/src/origami.c
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file origami.c
 *  \brief This codes classifies the cosmic web according to the ORIGAMI procedure
 * (Falck, Neyrinck & Szalay, ApJ 754, 126 (2012), arXiv:1201.2353)
 *  \author Florent Leclercq, based on the public code of Falck & Neyrinck
 *  \version 0.5.0
 *  \date 2015-2023
 */
#include "origami.h"
///-------------------------------------------------------------------------------------
static inline int isneg(int h)
{
    return (int)(h < 0);
} //isneg
///-------------------------------------------------------------------------------------
/** @fn origami_analysis
 * This routine classifies particles with the ORIGAMI scheme
 * @param P Snapshot containing particle information
 * @param *OBJECTS output: classification of particles
 * @param Np0 number of particles x
 * @param Np1 number of particles y
 * @param Np2 number of particles z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 */
void origami_analysis(Snapshot P, float_t *OBJECTS, int Np0, int Np1, int Np2, double L0, double L1, double L2)
{
    PrintMessage(3, "ORIGAMI: computing classification of the cosmic web...");
    INDENT;
    particleID_t Np=(particleID_t)Np0*Np1*Np2;

    // for the principal axes of the initial Lagrangian grid
    unsigned char *m;
    m = (unsigned char *)p_malloc(Np*sizeof(unsigned char), VARNAME(m));
    // for the diagonals of the initial Lagrangian grid
    unsigned char *m0, *m1, *m2;
    m0 = (unsigned char *)p_malloc(Np*sizeof(unsigned char), VARNAME(m0));
    m1 = (unsigned char *)p_malloc(Np*sizeof(unsigned char), VARNAME(m1));
    m2 = (unsigned char *)p_malloc(Np*sizeof(unsigned char), VARNAME(m2));
    #pragma omp parallel for schedule(static)
    for(particleID_t mp=0; mp<Np; mp++)
    {
        m[mp] = 1;
        m0[mp] = 1;
        m1[mp] = 1;
        m2[mp] = 1;
    }

    // 1) calculate number of shell-crossings
    #pragma omp parallel for collapse(3)
    for(int i=0; i<Np0; i++)
        for(int j=0; j<Np1; j++)
            for(int k=0; k<Np2; k++)
            {
                particleID_t mp = get_Lagrangian_Id(i,j,k,Np0,Np1,Np2);
                // First just along the principal Cartesian axes
                // x
                for(int h=1; h<Np0/4; h++)
                {
                    particleID_t mp2 = get_Lagrangian_Id((i+h)%Np0,j,k,Np0,Np1,Np2);
                    float_t dx = P.positions[mp2].Pos[0]-P.positions[mp].Pos[0];
                    if(dx < -L0/2.) dx += L0;
                    if(dx > L0/2.) dx -= L0;
                    if(dx < 0.)
                    {
                        if(m[mp] % 2 > 0)
                            m[mp] *= 2;
                        if(m[mp2] % 2 > 0)
                            m[mp2] *= 2;
                        break;
                    }
                }
                // y
                for(int h=1; h<Np1/4; h++)
                {
                    particleID_t mp2 = get_Lagrangian_Id(i,(j+h)%Np1,k,Np0,Np1,Np2);
                    float_t dy = P.positions[mp2].Pos[1]-P.positions[mp].Pos[1];
                    if(dy < -L1/2.) dy += L1;
                    if(dy > L1/2.) dy -= L1;
                    if(dy < 0.)
                    {
                        if(m[mp] % 3 > 0)
                            m[mp] *= 3;
                        if(m[mp2] % 3 > 0)
                            m[mp2] *= 3;
                        break;
                    }
                }
                // z
                for(int h=1; h<Np2/4; h++)
                {
                    particleID_t mp2 = get_Lagrangian_Id(i,j,(k+h)%Np2,Np0,Np1,Np2);
                    float_t dz = P.positions[mp2].Pos[2]-P.positions[mp].Pos[2];
                    if(dz < -L2/2.) dz += L2;
                    if(dz > L2/2.) dz -= L2;
                    if(dz < 0.)
                    {
                        if(m[mp] % 5 > 0)
                            m[mp] *= 5;
                        if(m[mp2] % 5 > 0)
                            m[mp2] *= 5;
                        break;
                    }
                }
                // Now do diagonal directions
                // It may require more thinking for non-cubic boxes
                // x
                for(int h=1; h<Np1/4; h = -h + isneg(h))
                {
                    particleID_t mp2 = get_Lagrangian_Id(i,goodmod(j+h,Np1),goodmod(k+h,Np2),Np0,Np1,Np2);
                    float_t d1 = P.positions[mp2].Pos[1]-P.positions[mp].Pos[1];
                    float_t d2 = P.positions[mp2].Pos[2]-P.positions[mp].Pos[2];
                    if(d1 < -L1/2.) d1 += L1;
                    if(d1 > L1/2.) d1 -= L1;
                    if(d2 < -L2/2.) d2 += L2;
                    if(d2 > L2/2.) d2 -= L2;
                    if((d1 + d2)*h < 0.)
                    {
                        m0[mp] *= 2;
                        break;
                    }
                }
                for(int h=1; h<Np1/4; h = -h + isneg(h))
                {
                    particleID_t mp2 = get_Lagrangian_Id(i,goodmod(j+h,Np1),goodmod(k-h,Np2),Np0,Np1,Np2);
                    float_t d1 = P.positions[mp2].Pos[1]-P.positions[mp].Pos[1];
                    float_t d2 = P.positions[mp2].Pos[2]-P.positions[mp].Pos[2];
                    if(d1 < -L1/2.) d1 += L1;
                    if(d1 > L1/2.) d1 -= L1;
                    if(d2 < -L2/2.) d2 += L2;
                    if(d2 > L2/2.) d2 -= L2;
                    if((d1 - d2)*h < 0.)
                    {
                        m0[mp] *= 3;
                        break;
                    }
                }
                // y
                for(int h=1; h<Np0/4; h = -h + isneg(h))
                {
                    particleID_t mp2 = get_Lagrangian_Id(goodmod(i+h,Np0),j,goodmod(k+h,Np2),Np0,Np1,Np2);
                    float_t d0 = P.positions[mp2].Pos[0]-P.positions[mp].Pos[0];
                    float_t d2 = P.positions[mp2].Pos[2]-P.positions[mp].Pos[2];
                    if(d0 < -L0/2.) d0 += L0;
                    if(d0 > L0/2.) d0 -= L0;
                    if(d2 < -L2/2.) d2 += L2;
                    if(d2 > L2/2.) d2 -= L2;
                    if((d0 + d2)*h < 0.)
                    {
                        m1[mp] *= 2;
                        break;
                    }
                }
                for(int h=1; h<Np0/4; h = -h + isneg(h))
                {
                    particleID_t mp2 = get_Lagrangian_Id(goodmod(i+h,Np0),j,goodmod(k-h,Np2),Np0,Np1,Np2);
                    float_t d0 = P.positions[mp2].Pos[0]-P.positions[mp].Pos[0];
                    float_t d2 = P.positions[mp2].Pos[2]-P.positions[mp].Pos[2];
                    if(d0 < -L0/2.) d0 += L0;
                    if(d0 > L0/2.) d0 -= L0;
                    if(d2 < -L2/2.) d2 += L2;
                    if(d2 > L2/2.) d2 -= L2;
                    if((d0 - d2)*h < 0.)
                    {
                        m1[mp] *= 3;
                        break;
                    }
                }
                // z
                for(int h=1; h<Np0/4; h = -h + isneg(h))
                {
                    particleID_t mp2 = get_Lagrangian_Id(goodmod(i+h,Np0),goodmod(j+h,Np2),k,Np0,Np1,Np2);
                    float_t d0 = P.positions[mp2].Pos[0]-P.positions[mp].Pos[0];
                    float_t d1 = P.positions[mp2].Pos[1]-P.positions[mp].Pos[1];
                    if(d0 < -L0/2.) d0 += L0;
                    if(d0 > L0/2.) d0 -= L0;
                    if(d1 < -L1/2.) d1 += L1;
                    if(d1 > L1/2.) d1 -= L1;
                    if((d0 + d1)*h < 0.)
                    {
                        m2[mp] *=2;
                        break;
                    }
                }
                for(int h=1; h<Np0/4; h = -h + isneg(h))
                {
                    particleID_t mp2 = get_Lagrangian_Id(goodmod(i+h,Np0),goodmod(j-h,Np2),k,Np0,Np1,Np2);
                    float_t d0 = P.positions[mp2].Pos[0]-P.positions[mp].Pos[0];
                    float_t d1 = P.positions[mp2].Pos[1]-P.positions[mp].Pos[1];
                    if(d0 < -L0/2.) d0 += L0;
                    if(d0 > L0/2.) d0 -= L0;
                    if(d1 < -L1/2.) d1 += L1;
                    if(d1 > L1/2.) d1 -= L1;
                    if((d0 - d1)*h < 0.)
                    {
                        m2[mp] *= 3;
                        break;
                    }
                }
            }

    // 2) tag and count particles according to the number of orthogonal axes along which they have shell-crossed
    int NBVOIDS=0, NBSHEETS=0, NBFILAMENTS=0, NBCLUSTERS=0;
    #pragma omp parallel for schedule(static)
    for(particleID_t mp=0; mp<Np; mp++)
    {
        // 2 a) classify object
        unsigned char mn  = (unsigned char)(m[mp]%2 == 0) + (unsigned char)(m[mp]%3 == 0)  + (unsigned char)(m[mp]%5 == 0);
        unsigned char m0n = (unsigned char)(m[mp]%2 == 0) + (unsigned char)(m0[mp]%2 == 0) + (unsigned char)(m0[mp]%3 == 0);
        unsigned char m1n = (unsigned char)(m[mp]%3 == 0) + (unsigned char)(m1[mp]%2 == 0) + (unsigned char)(m1[mp]%3 == 0);
        unsigned char m2n = (unsigned char)(m[mp]%5 == 0) + (unsigned char)(m2[mp]%2 == 0) + (unsigned char)(m2[mp]%3 == 0);
        int OBJNR = (int) max(mn,max(m0n,max(m1n,m2n)));

        // 2 b) count four web-types
        #if DEBUG
        switch(OBJNR)
        {
            case 0: // void
            #pragma omp atomic
            NBVOIDS++;
            break;

            case 1: // sheet
            #pragma omp atomic
            NBSHEETS++;
            break;

            case 2: // filament
            #pragma omp atomic
            NBFILAMENTS++;
            break;

            case 3: // cluster
            #pragma omp atomic
            NBCLUSTERS++;
            break;
        }
        #endif

        // 2 c) store web-type in 3d array
        OBJECTS[mp]=(float_t)OBJNR;
    }
    debugPrintInt(NBVOIDS);
    debugPrintInt(NBSHEETS);
    debugPrintInt(NBFILAMENTS);
    debugPrintInt(NBCLUSTERS);

    p_free(m, VARNAME(m));
    p_free(m0, VARNAME(m0));
    p_free(m1, VARNAME(m1));
    p_free(m2, VARNAME(m2));
    UNINDENT;
    PrintMessage(3, "ORIGAMI: computing classification of the cosmic web done.");
} //origami_analysis
///-------------------------------------------------------------------------------------
