///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/src/reo_fft.c
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file reo_fft.c
 *  \brief Routines for real even/odd Fourier transforms (cosine and sine transforms)
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2019-2023
 */
#include "reo_fft.h"
///-------------------------------------------------------------------------------------
/** @fn FFT_r2r_3d
 * 3D real even/odd Fourier transform of a real scalar quantity
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param *in input quantity (float_t*)
 * @param *out output quantity (float_t*)
 * @param kind0 kind of transform (DST/DCT), axis 0
 * @param kind1 kind of transform (DST/DCT), axis 1
 * @param kind2 kind of transform (DST/DCT), axis 2
 * @param Nthreads number of threads for parallel computing
 */
static inline void FFT_r2r_3d(int N0, int N1, int N2, float_t *in, float_t *out, fftw_r2r_kind kind0, fftw_r2r_kind kind1, fftw_r2r_kind kind2, int Nthreads)
{
    // OUT OF PLACE FOURIER TRANSFORMATION FOR REAL INPUT DATA
    const cellIndex_t N=N0*N1*N2;
    fftw_plan_t fftp;

    #ifdef DOUBLEPRECISION_FFTW
        #ifndef DOUBLEPRECISION
            double *buf_in; buf_in = (double*)p_fftw_malloc(N*sizeof(double), VARNAME(buf_in));
            double *buf_out; buf_out = (double*)p_fftw_malloc(N*sizeof(double), VARNAME(buf_out));
            #pragma omp parallel for
            for(cellIndex_t mc=0; mc<N; mc++)
                buf_in[mc]=(double)in[mc];
        #else
            UNUSED(N);
            double *buf_in=in; double *buf_out=out;
        #endif

        fftw_init_threads();
        fftw_plan_with_nthreads(Nthreads);
        fftp = fftw_plan_r2r_3d(N0,N1,N2,buf_in,buf_out,fftw_r2r_kind kind0,fftw_r2r_kind kind1,fftw_r2r_kind kind2,FFTW_ESTIMATE);

        fftw_execute(fftp);
        fftw_destroy_plan(fftp);
        fftw_cleanup_threads();

        #ifndef DOUBLEPRECISION
            #pragma omp parallel for
            for(fourierMode_t mf=0; mf<N; mf++)
                out[mf]=(float_t)buf_out[mf];
            p_fftw_free(buf_in, VARNAME(buf_in));
            p_fftw_free(buf_out, VARNAME(buf_out));
        #endif
    #else
        #ifdef DOUBLEPRECISION
            double *buf_in; buf_in = (double*)p_fftw_malloc(N*sizeof(double), VARNAME(buf_in));
            double *buf_out; buf_out = (double*)p_fftw_malloc(N*sizeof(double), VARNAME(buf_out));
            #pragma omp parallel for
            for(cellIndex_t mc=0; mc<N; mc++)
                buf_in[mc]=(float)in[mc];
        #else
            UNUSED(N);
            float *buf_in=in; float *buf_out=out;
        #endif

        fftwf_init_threads();
        fftwf_plan_with_nthreads(Nthreads);
        fftp = fftwf_plan_r2r_3d(N0,N1,N2,buf_in,buf_out,kind0,kind1,kind2,FFTW_ESTIMATE);
        fftwf_execute(fftp);
        fftwf_destroy_plan(fftp);
        fftwf_cleanup_threads();

        #ifdef DOUBLEPRECISION
            #pragma omp parallel for
            for(fourierMode_t mf=0; mf<(fourierMode_t)N; mf++)
                out[mf]=(float_t)buf_out[mf];
            p_fftw_free(buf_in, VARNAME(buf_in));
            p_fftw_free(buf_out, VARNAME(buf_out));
        #endif
    #endif
} //FFT_r2r_3d
///-------------------------------------------------------------------------------------
static inline int get_normalization_r2r_1d(int N, fftw_r2r_kind kind)
{
    int NORM;
    switch(kind)
    {
        case FFTW_RODFT00: //DST-I
            NORM=2*(N+1);
        break;
        case FFTW_REDFT00: //DCT-I
            NORM=2*(N-1);
        break;
        case FFTW_RODFT10: //DST-II
        case FFTW_RODFT01: //DST-III
        case FFTW_RODFT11: //DST-IV
        case FFTW_REDFT10: //DCT-II
        case FFTW_REDFT01: //DCT-III
        case FFTW_REDFT11: //DCT-IV
        default:
            NORM=2*N;
        break;
    };
    return NORM;
} //get_normalization_r2r_1d
///-------------------------------------------------------------------------------------
static inline int get_normalization_r2r_3d(int N0, int N1, int N2, fftw_r2r_kind kind0, fftw_r2r_kind kind1, fftw_r2r_kind kind2)
{
    return get_normalization_r2r_1d(N0,kind0)*get_normalization_r2r_1d(N1,kind1)*get_normalization_r2r_1d(N2,kind2);
} //get_normalization_r2r_3d
///-------------------------------------------------------------------------------------
/** @fn FFT_forward_r2r_3d
 * 3D Forward real even/odd Fourier transform of a real scalar quantity
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param *in input quantity (float_t*)
 * @param *out output quantity (float_t*)
 * @param kind0 kind of transform (DST/DCT), axis 0
 * @param kind1 kind of transform (DST/DCT), axis 1
 * @param kind2 kind of transform (DST/DCT), axis 2
 * @param Nthreads number of threads for parallel computing
 */
void FFT_forward_r2r_3d(int N0, int N1, int N2, float_t *in, float_t *out, fftw_r2r_kind kind0, fftw_r2r_kind kind1, fftw_r2r_kind kind2, int Nthreads)
{
    FFT_r2r_3d(N0,N1,N2,in,out,kind0,kind1,kind2,Nthreads);
} //FFT_forward_r2r_3d
///-------------------------------------------------------------------------------------
/** @fn FFT_backward_r2r_3d
 * 3D Backward real even/odd Fourier transform of a real scalar quantity
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param *in input quantity (float_t*)
 * @param *out output quantity (float_t*)
 * @param kind0 kind of transform (DST/DCT), axis 0
 * @param kind1 kind of transform (DST/DCT), axis 1
 * @param kind2 kind of transform (DST/DCT), axis 2
 * @param Nthreads number of threads for parallel computing
 */
void FFT_backward_r2r_3d(int N0, int N1, int N2, float_t *in, float_t *out, fftw_r2r_kind kind0, fftw_r2r_kind kind1, fftw_r2r_kind kind2, int Nthreads)
{
    FFT_r2r_3d(N0,N1,N2,in,out,kind0,kind1,kind2,Nthreads);

    // DO THE NORMALIZATION
    const fourierMode_t N=(fourierMode_t)N0*N1*N2;
    const double NORM=(double)get_normalization_r2r_3d(N0,N1,N2,kind0,kind1,kind2);
    #pragma omp parallel for schedule(static)
    for(fourierMode_t mf=0; mf<N; mf++)
        out[mf]/=NORM;
} //FFT_backward_r2r_3d
///-------------------------------------------------------------------------------------
/** @fn get_kmode_RODFT00
 * This subroutine gets k mode in sine space for Fourier
 * space computations (power spectrum...)
 * @param l index of the mode
 * @param N grid size
 * @param L box size
 * @return value of wave number k
 */
double get_kmode_RODFT00(const int l, const int N, const double L)
{
    UNUSED(N);
    return 2.*M_PI/L * (l+1); // index l+1 and not l here, cf. DST-I definition in FFTW
} //get_kmode_RODFT00
///-------------------------------------------------------------------------------------
/** @fn get_kmode_grad_RODFT00
 * This subroutine returns the gradient in sine space for a given k mode
 * @param l index of the mode x
 * @param m index of the mode y
 * @param n index of the mode z
 * @param N0 grid size x
 * @param N1 grid size y
 * @param N2 grid size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 * @param d0 mesh spacing x
 * @param d1 mesh spacing y
 * @param d2 mesh spacing z
 * @param axis axis of gradient
 * @return grad
 */
double get_kmode_grad_RODFT00(const int l, const int m, const int n, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2, const double d0, const double d1, const double d2, const int axis)
{
    double k_axis, grad_axis;
    switch(axis)
    {
        case 0:
            k_axis=get_kmode_RODFT00(l,N0,L0);
            grad_axis=gradient_RODFT00_1d(k_axis,d0,N0);
        break;
        case 1:
            k_axis=get_kmode_RODFT00(m,N1,L1);
            grad_axis=gradient_RODFT00_1d(k_axis,d1,N1);
        break;
        case 2:
            k_axis=get_kmode_RODFT00(n,N2,L2);
            grad_axis=gradient_RODFT00_1d(k_axis,d2,N2);
        break;
    }
    return grad_axis;
} //get_kmode_grad_RODFT00
///-------------------------------------------------------------------------------------
