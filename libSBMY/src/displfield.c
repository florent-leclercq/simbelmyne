///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/src/displfield.c
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file displfield.c
 *  \brief Routines for dealing with the Lagrangian displacement field
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2015-2023
 */
#include "displfield.h"
///-------------------------------------------------------------------------------------
/** @fn get_displfield_onecomponent
 * Gets one component of the displacement field from information in snapshot
 * @param P input: snapshot
 * @param *PSI output: Psi on the Lagrangian grid
 * @param axis axis (0,1,2)
 * @param Np0 number of particles x
 * @param Np1 number of particles y
 * @param Np2 number of particles z
 */
void get_displfield_onecomponent(const Snapshot P, float_t *PSI, const int axis, const int Np0, const int Np1, const int Np2)
{
    int Nthreads = omp_get_max_threads();
    sprintf(G__msg__, "Getting displacement field (using %d cores)...", Nthreads);
    PrintMessage(4, G__msg__);
    INDENT;

    particleID_t Np = (particleID_t)Np0*Np1*Np2;
    double dp0=P.header.BoxSize/(double)Np0;
    double dp1=P.header.BoxSize/(double)Np1;
    double dp2=P.header.BoxSize/(double)Np2;

    #pragma omp parallel for schedule(static)
    for(particleID_t mp=0; mp<Np; mp++)
    {
        // Check the particle Id (particles Ids can be out of order)
        // This loop is thread-safe as long as mp<->Id is a permutation
        particleID_t Id=P.ids[mp].Id;
        int mp_indices[3]; get_Lagrangian_indices(mp, Np0, Np1, Np2, mp_indices);
        float_t initialPosition[3]; get_Lagrangian_grid_position(mp_indices[0],mp_indices[1],mp_indices[2],dp0,dp1,dp2,initialPosition);
        PSI[Id] = P.positions[mp].Pos[axis] - initialPosition[axis];

        // Check if the particle has crossed the periodic boundaries of the box
        PSI[Id] = fabs(P.header.BoxSize-fabs(PSI[Id])) < fabs(PSI[Id]) ? copysign(P.header.BoxSize-fabs(PSI[Id]),-PSI[Id]) : PSI[Id];
    }

    UNINDENT;
    sprintf(G__msg__, "Getting displacement field (using %d cores) done.", Nthreads);
    PrintMessage(4, G__msg__);
} //get_displfield_onecomponent
///-------------------------------------------------------------------------------------
/** @fn get_displfield
 * Gets the displacement field from information in snapshot
 * @param P input: snapshot
 * @param *PSI0 output: Psi x on the Lagrangian grid
 * @param *PSI1 output: Psi y on the Lagrangian grid
 * @param *PSI2 output: Psi z on the Lagrangian grid
 * @param Np0 number of particles x
 * @param Np1 number of particles y
 * @param Np2 number of particles z
 */
void get_displfield(const Snapshot P, float_t *PSI0, float_t *PSI1, float_t *PSI2, const int Np0, const int Np1, const int Np2)
{
    int Nthreads = omp_get_max_threads();
    sprintf(G__msg__, "Getting displacement field (using %d cores)...", Nthreads);
    PrintMessage(4, G__msg__);
    INDENT;

    particleID_t Np = (particleID_t)Np0*Np1*Np2;
    double dp0=P.header.BoxSize/(double)Np0;
    double dp1=P.header.BoxSize/(double)Np1;
    double dp2=P.header.BoxSize/(double)Np2;

    #pragma omp parallel for schedule(static)
    for(particleID_t mp=0; mp<Np; mp++)
    {
        // Check the particle Id (particles Ids can be out of order)
        // This loop is thread-safe as long as mp<->Id is a permutation
        particleID_t Id=P.ids[mp].Id;
        int mp_indices[3]; get_Lagrangian_indices(mp, Np0, Np1, Np2, mp_indices);
        float_t initialPosition[3]; get_Lagrangian_grid_position(mp_indices[0],mp_indices[1],mp_indices[2],dp0,dp1,dp2,initialPosition);
        float_t PSI[3]; get_Psi(P.positions[mp].Pos, initialPosition, P.header.BoxSize, P.header.BoxSize, P.header.BoxSize, PSI);
        PSI0[Id] = PSI[0];
        PSI1[Id] = PSI[1];
        PSI2[Id] = PSI[2];
    }

    UNINDENT;
    sprintf(G__msg__, "Getting displacement field (using %d cores) done.", Nthreads);
    PrintMessage(4, G__msg__);
} //get_displfield
///-------------------------------------------------------------------------------------
/** @fn get_divPsi_fs
 * Computes the divergence of the displacement field in Fourier space
 * @param *PSI0 input: displacement field
 * @param *PSI1 input: displacement field
 * @param *PSI2 input: displacement field
 * @param *DIVPSI output: divergence of Psi on the Lagrangian grid
 * @param Np0 number of particles x
 * @param Np1 number of particles y
 * @param Np2 number of particles z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 */
void get_divPsi_fs(const float_t *PSI0, const float_t *PSI1, const float_t *PSI2, float_t *DIVPSI, const int Np0, const int Np1, const int Np2, const double L0, const double L1, const double L2)
{
    int Nthreads = omp_get_max_threads();
    sprintf(G__msg__, "Getting divergence of the displacement field in Fourier space (using %d cores)...", Nthreads);
    PrintMessage(4, G__msg__);
    INDENT;

    divergence_of_vector(PSI0, PSI1, PSI2, DIVPSI, false, Np0, Np1, Np2, L0, L1, L2);

    UNINDENT;
    sprintf(G__msg__, "Getting divergence of the displacement field in Fourier space (using %d cores) done.", Nthreads);
    PrintMessage(4, G__msg__);
} //get_divPsi_fs
///-------------------------------------------------------------------------------------
/** @fn get_potentialPsi_fs
 * Computes the potential of the displacement field in Fourier space
 * @param *PSI0 input: displacement field
 * @param *PSI1 input: displacement field
 * @param *PSI2 input: displacement field
 * @param *POTENTIALPSI output: divergence of Psi on the Lagrangian grid
 * @param Np0 number of particles x
 * @param Np1 number of particles y
 * @param Np2 number of particles z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 */
void get_potentialPsi_fs(const float_t *PSI0, const float_t *PSI1, const float_t *PSI2, float_t *POTENTIALPSI, const int Np0, const int Np1, const int Np2, const double L0, const double L1, const double L2)
{
    int Nthreads = omp_get_max_threads();
    sprintf(G__msg__, "Getting potential of the displacement field in Fourier space (using %d cores)...", Nthreads);
    PrintMessage(4, G__msg__);
    INDENT;

    divergence_of_vector(PSI0, PSI1, PSI2, POTENTIALPSI, true, Np0, Np1, Np2, L0, L1, L2);

    UNINDENT;
    sprintf(G__msg__, "Getting potential of the displacement field in Fourier space (using %d cores) done.", Nthreads);
    PrintMessage(4, G__msg__);
} //get_potentialPsi_fs
///-------------------------------------------------------------------------------------
/** @fn inversegrad_divPsi
 * Reconstructs the displacement field from its divergence in Fourier space
 * @param *DIVPSI input: DIVPSI on the Lagrangian grid
 * @param *PSI0 output: displacement field x
 * @param *PSI1 output: displacement field y
 * @param *PSI2 output: displacement field z
 * @param Np0 number of particles x
 * @param Np1 number of particles y
 * @param Np2 number of particles z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 */
void inversegrad_divPsi(const float_t *DIVPSI, float_t *PSI0, float_t *PSI1, float_t *PSI2, const int Np0, const int Np1, const int Np2, const double L0, const double L1, const double L2)
{
    int Nthreads = omp_get_max_threads();
    sprintf(G__msg__, "Reconstructing displacement field from divergence in Fourier space (using %d cores)...", Nthreads);
    PrintMessage(4, G__msg__);
    INDENT;

    gradient_of_scalar(DIVPSI, PSI0, PSI1, PSI2, true, Np0, Np1, Np2, L0, L1, L2);

    UNINDENT;
    sprintf(G__msg__, "Reconstructing displacement field from divergence in Fourier space (using %d cores) done.", Nthreads);
    PrintMessage(4, G__msg__);
} //inversegrad_divPsi
///-------------------------------------------------------------------------------------
/** @fn get_normcurlpartPsi_fullminusscalar
 * Gets the norm of the curl of the displacement field as input minus scalar part
 * @param *PSI0 input: displacement field
 * @param *PSI1 input: displacement field
 * @param *PSI2 input: displacement field
 * @param *NORM_CURLPARTPSI output : norm of the curl of the displacement field
 * @param Np0 number of particles x
 * @param Np1 number of particles y
 * @param Np2 number of particles z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 */
void get_normcurlpartPsi_fullminusscalar(const float_t *PSI0, const float_t *PSI1, const float_t *PSI2, float_t *NORM_CURLPARTPSI, const int Np0, const int Np1, const int Np2, const double L0, const double L1, const double L2)
{
    int Nthreads = omp_get_max_threads();
    particleID_t Np = (particleID_t)Np0*Np1*Np2;
    float_t DIVPSI[Np], SCALARPART_PSI0[Np], SCALARPART_PSI1[Np], SCALARPART_PSI2[Np];

    // get the divergence of the displacement field, then the scalar part
    get_divPsi_fs(PSI0, PSI1, PSI2, DIVPSI, Np0, Np1, Np2, L0, L1, L2);
    inversegrad_divPsi(DIVPSI, SCALARPART_PSI0, SCALARPART_PSI1, SCALARPART_PSI2, Np0, Np1, Np2, L0, L1, L2);

    sprintf(G__msg__, "Getting norm of rotational of the displacement field (using %d cores)...", Nthreads);
    PrintMessage(4, G__msg__);
    INDENT;

    // get the curl as full displacement minus scalar part
    #pragma omp parallel for schedule(static)
    for(particleID_t mp=0; mp<Np; mp++)
    {
        float_t CURLPARTPSI[3]; float_t PSI[3]={PSI0[mp], PSI1[mp], PSI2[mp]}; float_t SCALARPART_PSI[3]={SCALARPART_PSI0[mp], SCALARPART_PSI1[mp], SCALARPART_PSI2[mp]};

        // hack routine get_Psi for this computation
        get_Psi(PSI, SCALARPART_PSI, L0, L1, L2, CURLPARTPSI);

        NORM_CURLPARTPSI[mp] = sqrt(CURLPARTPSI[0]*CURLPARTPSI[0] + CURLPARTPSI[1]*CURLPARTPSI[1] + CURLPARTPSI[2]*CURLPARTPSI[2]);
    }

    UNINDENT;
    sprintf(G__msg__, "Getting norm of rotational of the displacement field (using %d cores) done.", Nthreads);
    PrintMessage(4, G__msg__);
} //get_normcurlpartPsi_fullminusscalar
///-------------------------------------------------------------------------------------
/** @fn get_normcurlpartPsi_vector
 * Gets the norm of the curl of the displacement field directly as vector field
 * @param *PSI0 input: displacement field
 * @param *PSI1 input: displacement field
 * @param *PSI2 input: displacement field
 * @param *NORM_CURLPARTPSI output : norm of the curl of the displacement field
 * @param Np0 number of particles x
 * @param Np1 number of particles y
 * @param Np2 number of particles z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 */
void get_normcurlpartPsi_vector(const float_t *PSI0, const float_t *PSI1, const float_t *PSI2, float_t *NORM_CURLPARTPSI, const int Np0, const int Np1, const int Np2, const double L0, const double L1, const double L2)
{
    int Nthreads = omp_get_max_threads();
    sprintf(G__msg__, "Getting norm of rotational of the displacement field (using %d cores)...", Nthreads);
    PrintMessage(4, G__msg__);
    INDENT;

    normcurlpart_of_vector(PSI0, PSI1, PSI2, NORM_CURLPARTPSI, Np0, Np1, Np2, L0, L1, L2);

    UNINDENT;
    sprintf(G__msg__, "Getting norm of rotational of the displacement field (using %d cores) done.", Nthreads);
    PrintMessage(4, G__msg__);
} //get_normcurlpartPsi_vector
///-------------------------------------------------------------------------------------
/** @fn get_normcurlpartPsi
 * Gets the norm of the curl of the displacement field
 * @param *PSI0 input: displacement field
 * @param *PSI1 input: displacement field
 * @param *PSI2 input: displacement field
 * @param *NORM_CURLPARTPSI output : norm of the curl of the displacement field
 * @param Np0 number of particles x
 * @param Np1 number of particles y
 * @param Np2 number of particles z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 */
void get_normcurlpartPsi(const float_t *PSI0, const float_t *PSI1, const float_t *PSI2, float_t *NORM_CURLPARTPSI, const int Np0, const int Np1, const int Np2, const double L0, const double L1, const double L2)
{
//     get_normcurlpartPsi_fullminusscalar(PSI0, PSI1, PSI2, NORM_CURLPARTPSI, Np0, Np1, Np2, L0, L1, L2);
    get_normcurlpartPsi_vector(PSI0, PSI1, PSI2, NORM_CURLPARTPSI, Np0, Np1, Np2, L0, L1, L2);
} //get_normcurlpartPsi
///-------------------------------------------------------------------------------------
/** @fn get_shearPsi
 * Computes the shear of the displacement field
 * @param *PSI0 input: displacement field
 * @param *PSI1 input: displacement field
 * @param *PSI2 input: displacement field
 * @param *R00 output : shear of the displacement field
 * @param *R11 output : shear of the displacement field
 * @param *R22 output : shear of the displacement field
 * @param *R01 output : shear of the displacement field
 * @param *R02 output : shear of the displacement field
 * @param *R12 output : shear of the displacement field
 * @param *R10 output : shear of the displacement field
 * @param *R20 output : shear of the displacement field
 * @param *R21 output : shear of the displacement field
 * @param symmetric boolean : assume that the displacement field is symmetric?
 * @param Np0 number of particles x
 * @param Np1 number of particles y
 * @param Np2 number of particles z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 */
void get_shearPsi(const float_t *PSI0, const float_t *PSI1, const float_t *PSI2, float_t *R00, float_t *R11, float_t *R22, float_t *R01, float_t *R02, float_t *R12, float_t *R10, float_t *R20, float_t *R21, const bool symmetric, const int Np0, const int Np1, const int Np2, const double L0, const double L1, const double L2)
{
    int Nthreads = omp_get_max_threads();
    particleID_t Np = (particleID_t)Np0*Np1*Np2;

    sprintf(G__msg__, "Getting shear of the displacement field (using %d cores)...", Nthreads);
    PrintMessage(4, G__msg__);
    INDENT;

    shear_of_vector(PSI0, PSI1, PSI2, R00, R11, R22, R01, R02, R12, R10, R20, R21, symmetric, Np0, Np1, Np2, L0, L1, L2);

    debugPrintFloattArray(R00,Np);
    debugPrintFloattArray(R11,Np);
    debugPrintFloattArray(R22,Np);
    debugPrintFloattArray(R01,Np);
    debugPrintFloattArray(R02,Np);
    debugPrintFloattArray(R12,Np);
    if(symmetric == false)
    {
        debugPrintFloattArray(R10,Np);
        debugPrintFloattArray(R20,Np);
        debugPrintFloattArray(R21,Np);
    }

    UNINDENT;
    sprintf(G__msg__, "Getting shear of the displacement field (using %d cores) done.", Nthreads);
    PrintMessage(4, G__msg__);
} //get_shearPsi
///-------------------------------------------------------------------------------------
/** @fn get_shearPsi_symmetric
 * Computes the shear of the displacement field, assuming it is symmetric
 * @param *PSI0 input: displacement field
 * @param *PSI1 input: displacement field
 * @param *PSI2 input: displacement field
 * @param *R00 output : shear of the displacement field
 * @param *R11 output : shear of the displacement field
 * @param *R22 output : shear of the displacement field
 * @param *R01 output : shear of the displacement field
 * @param *R02 output : shear of the displacement field
 * @param *R12 output : shear of the displacement field
 * @param Np0 number of particles x
 * @param Np1 number of particles y
 * @param Np2 number of particles z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 */
void get_shearPsi_symmetric(const float_t *PSI0, const float_t *PSI1, const float_t *PSI2, float_t *R00, float_t *R11, float_t *R22, float_t *R01, float_t *R02, float_t *R12, const int Np0, const int Np1, const int Np2, const double L0, const double L1, const double L2)
{
    float_t *DUMMY = NULL;
    bool symmetric = true;
    get_shearPsi(PSI0, PSI1, PSI2, R00, R11, R22, R01, R02, R12, DUMMY, DUMMY, DUMMY, symmetric, Np0, Np1, Np2, L0, L1, L2);
} //get_shearPsi_symmetric
///-------------------------------------------------------------------------------------
/** @fn jacobian_l2e_potential
 * Computes the Jacobian of the transformation from Lagrangian to Eulerian coordinates
 * @param *PSI0 input: displacement field
 * @param *PSI1 input: displacement field
 * @param *PSI2 input: displacement field
 * @param *JACOBIAN output : jacobian of the transformation
 * @param Np0 number of particles x
 * @param Np1 number of particles y
 * @param Np2 number of particles z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 */
void jacobian_l2e_potential(const float_t *PSI0, const float_t *PSI1, const float_t *PSI2, float_t *JACOBIAN, const int Np0, const int Np1, const int Np2, const double L0, const double L1, const double L2)
{
    int Nthreads = omp_get_max_threads();
    sprintf(G__msg__, "Computing Jacobian of the Lagrangian to Eulerian transformation (using %d cores)...", Nthreads);
    PrintMessage(4, G__msg__);
    INDENT;

    particleID_t Np=Np0*Np1*Np2;
    float_t R00[Np];
    float_t R11[Np];
    float_t R22[Np];
    float_t R01[Np];
    float_t R02[Np];
    float_t R12[Np];

    // 1) calculate elements of shear of Lagrangian displacement field tensor
    get_shearPsi_symmetric(PSI0, PSI1, PSI2, R00, R11, R22, R01, R02, R12, Np0, Np1, Np2, L0, L1, L2);

    // 2) go from the tidal tensor to the deformation tensor: add the identity tensor
    #pragma omp parallel for schedule(static)
    for(particleID_t mp=0; mp<Np; mp++)
    {
        R00[mp]+=1.;
        R11[mp]+=1.;
        R22[mp]+=1.;
    }

    // 3) calculate eigenvalues and classify objects
    #pragma omp parallel for schedule(static)
    for(particleID_t mp=0; mp<Np; mp++)
    {
        float_t EIGVALS[3];

        // 3 a) calculate eigenvalues from symmetric tensor
        get_eigvals_real(R00[mp], R11[mp], R22[mp], R01[mp], R02[mp], R12[mp], EIGVALS);

        // 3 b) calculate jacobian from eigenvalues: determinant is the product of eigenvalues
        JACOBIAN[mp] = (float_t)((1.+EIGVALS[0])*(1.+EIGVALS[1])*(1.+EIGVALS[2]));
    }

    UNINDENT;
    sprintf(G__msg__, "Computing Jacobian of the Lagrangian to Eulerian transformation (using %d cores) done.", Nthreads);
    PrintMessage(4, G__msg__);
} //jacobian_l2e_potential
///-------------------------------------------------------------------------------------
