///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/src/math_utils.c
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file math_utils.c
 *  \brief Some math routines
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2015-2023
 */
#include "math_utils.h"
///-------------------------------------------------------------------------------------
bool sameSign(float_t x, float_t y)
{
    double xy = x*y;
    return xy >= 0;
} //sameSign
///-------------------------------------------------------------------------------------
int even_ceil(double x)
{
    int c = (int)ceil(x);
    return fmod(c,2)==0 ? c : c+1;
} //even_ceil
///-------------------------------------------------------------------------------------
int odd_ceil(double x)
{
    int c = (int)ceil(x);
    return fmod(c,2)==1 ? c : c+1;
} //odd_ceil
///-------------------------------------------------------------------------------------
void swap_int(int *x, int *y)
{
    int t = *x;
    *x = *y;
    *y = t;
} //swap_int
///-------------------------------------------------------------------------------------
void swap_float_t(float_t *x, float_t *y)
{
    float_t t = *x;
    *x = *y;
    *y = t;
} //swap_float_t
///-------------------------------------------------------------------------------------
void sort_array(float_t a[], int n)
{
    int swapped;
    float_t *p;
    int i;
    do
    {
        swapped = 0;
        p = a;
        i=0;

        while(i<n-1)
        {
            if(*(p+i) > *(p+i+1))
            {
                swap_float_t(p+i, p+i+1);
                swapped = 1;
            }
            i++;
        }
    }
    while(swapped);
} //sort_array
///-------------------------------------------------------------------------------------
void solve_quadratic(float_t a, float_t b, float_t c, float_t RESULTS[2])
{
    if(a==0)
    {
        debugPrintFloatt(a);
        debugPrintFloatt(b);
        debugPrintFloatt(c);
        FatalError("The coefficient of the square is zero.");
    }

    double b_ = (double)b/a;
    double c_ = (double)c/a;
    double DELTA = 0.25*b_*b_-c_;
    double epsilon=1e-5*min(0.25*b_*b_,fabs(c_));

    // condition for real and different roots
    if(DELTA>=0)
    {
        RESULTS[0] = (float_t)(-0.5*b_ - sqrt(DELTA));
        RESULTS[1] = (float_t)(-0.5*b_ + sqrt(DELTA));
    }
    // condition for real and equal roots
    else if(DELTA<=0 && -DELTA<=epsilon)
    {
        RESULTS[0] = RESULTS[1] = (float_t)(-0.5*b_);
    }
    // if roots are not real
    else
    {
        debugPrintFloatt(a);
        debugPrintFloatt(b);
        debugPrintFloatt(c);
        debugPrintDouble(DELTA);
        FatalError("There exists no real solution.");
    }
} //solve_quadratic
///-------------------------------------------------------------------------------------
float_t sqrt3rd(float_t x)
{
    float_t result=0.;
    if(x>=0) result=pow(x,1./3.);
    else result=-1.*pow(fabs(x),1./3.);

    return(result);
} //sqrt3rd
///-------------------------------------------------------------------------------------
void solve_cubic(float_t a, float_t b, float_t c, float_t d, float_t RESULTS[3])
{
    if(a==0)
    {
        debugPrintFloatt(a);
        debugPrintFloatt(b);
        debugPrintFloatt(c);
        debugPrintFloatt(d);
        FatalError("The coefficient of the cube is zero.");
    }
    double b_ = (double)b/a;
    double c_ = (double)c/a;
    double d_ = (double)d/a;

    double b2_ = b_*b_;
    double p = (b2_-3.*c_)/9.;
    double q = (b_*(2.*b2_-9.*c_) + 27.*d_)/54.;
    double q2 = q*q;
    double p3 = p*p*p;

    // Condition for three real roots
    if(q2 <= (p3 + 1e-5*min(fabs(q2),fabs(p3))))
    {
        double t=q/sqrt(p3);
        if(t<-1.) t=-1.;
        if(t> 1.) t= 1.;
        t=acos(t);
        b_/=3.; p=-2.*sqrt(p);
        RESULTS[0]=(float_t)( p*cos(t/3)-b_ );
        RESULTS[1]=(float_t)( p*cos((t+2.*M_PI)/3)-b_ );
        RESULTS[2]=(float_t)( p*cos((t-2.*M_PI)/3)-b_ );
    }
    else
    {
        double A = -sqrt3rd(fabs(q)+sqrt(q2-p3));
        if(q<0.) A=-A;
        double B = (A==0.? 0. : p/A);
        b_/=3.;
        RESULTS[0] = (float_t) ( (A+B)-b_ );
        RESULTS[1] = (float_t) ( -0.5*(A+B)-b_ );
        RESULTS[2] = (float_t) ( 0.5*sqrt(3.)*(A-B) );
        if(fabs(RESULTS[2]) < 1e-3 || fabs(RESULTS[2]) < 1e-4*fabs(RESULTS[1]))
        {
            // Only two real roots
            RESULTS[2]=RESULTS[1];
        }
        else
        {
            // The three roots are RESULTS[0] and RESULTS[1] ± i*RESULTS[2]
            debugPrintFloatt(a);
            debugPrintFloatt(b);
            debugPrintFloatt(c);
            debugPrintFloatt(d);
            debugPrintDouble(RESULTS[0]);
            debugPrintDouble(RESULTS[1]);
            debugPrintDouble(RESULTS[2]);
            FatalError("There exists no real solution. The three roots are RESULTS[0] and RESULTS[1] ± i*RESULTS[2].");
        }
    }
} //solve_cubic
///-------------------------------------------------------------------------------------
void get_eigvals_real(float_t T00, float_t T11, float_t T22, float_t T01, float_t T02, float_t T12, float_t EIGVALS[3])
{
    // 1) prepare for solving cubic equation
    float_t a=-1.;
    float_t b=T00+T11+T22;
    float_t c=T02*T02+T12*T12+T01*T01-T11*T22-T00*T22-T00*T11;
    float_t d=T00*T11*T22+2.*T01*T12*T02-T02*T02*T11-T12*T12*T00-T01*T01*T22;

    // 2) solve cubic equation analytically
    solve_cubic(a,b,c,d,EIGVALS);

    // 3) sort eigenvalues l1<=l2<=l3
    sort_array(EIGVALS, 3);
} //get_eigvals_real
///-------------------------------------------------------------------------------------
/** Determinant of the 3x3 matrix given in lines, i.e.
 * a b c
 * d e f
 * g h i
 */
float_t determinant3x3(float_t a, float_t b, float_t c, float_t d, float_t e, float_t f, float_t g, float_t h, float_t i)
{
    return a*(e*i-f*h) - b*(d*i-f*g) + c*(d*h-e*g);
} //determinant3x3
///-------------------------------------------------------------------------------------
static float_t bruteforce_det4_helper(float_t cj0, float_t ck1, float_t ck0, float_t cj1, float_t cm2, float_t cn3, float_t cn2, float_t cm3)
{
   return (cj0 * ck1 - ck0 * cj1)
        * (cm2 * cn3 - cn2 * cm3);
} //bruteforce_det4_helper
///-------------------------------------------------------------------------------------
/** Determinant of the 4x4 matrix given in lines, i.e.
 * m00 m01 m02 m03
 * m10 m11 m12 m13
 * m20 m21 m22 m23
 * m30 m31 m32 m33
 */
float_t determinant4x4(float_t m00, float_t m01, float_t m02, float_t m03, float_t m10, float_t m11, float_t m12, float_t m13, float_t m20, float_t m21, float_t m22, float_t m23, float_t m30, float_t m31, float_t m32, float_t m33)
{
     // trick by Martin Costabel to compute 4x4 det with only 30 muls
     return bruteforce_det4_helper(m00,m11,m10,m01,m22,m33,m32,m23)
           - bruteforce_det4_helper(m00,m21,m20,m01,m12,m33,m32,m13)
           + bruteforce_det4_helper(m00,m31,m30,m01,m12,m23,m22,m13)
           + bruteforce_det4_helper(m10,m21,m20,m11,m02,m33,m32,m03)
           - bruteforce_det4_helper(m10,m31,m30,m11,m02,m23,m22,m03)
           + bruteforce_det4_helper(m20,m31,m30,m21,m02,m13,m12,m03);
} //determinant4x4
///-------------------------------------------------------------------------------------
/** Linear interpolation in sorted arrays
 */
float_t linear_interpolation(const float_t* x, const float_t* y, const int l, const float_t xt)
{
    if(xt<x[0])
    {
        sprintf(G__msg__, "Invalid range access in linear interpolation: given %g, minimum %g.", xt, x[0]);
        FatalError(G__msg__);
    }
    int k=0;
    while(k<l-1 && xt<x[k]) k++;
    if(k==l-1)
    {
        sprintf(G__msg__, "Invalid range access in linear interpolation: given %g, maximum %g.", xt, x[l-1]);
        FatalError(G__msg__);
    }
    float_t x0 = x[k], x1 = x[k+1], y0 = y[k], y1 = y[k+1];
    return y0 + ((y1 - y0) / (x1 - x0)) * (xt - x0);
} //linear_interpolation
///-------------------------------------------------------------------------------------
