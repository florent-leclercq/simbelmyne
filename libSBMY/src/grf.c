///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/src/grf.c
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file grf.c
 *  \brief Routines for dealing with Gaussian random fields
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2013-2023
 */
#include "grf.h"
///-------------------------------------------------------------------------------------
/** @fn generate_white_noise_array
 * This routine generates an array containing white noise
 * @param *WN output white noise
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param *seedtable the seeds to be used for random number generation
 * @param Nthreads number of threads
 */
void generate_white_noise_array(float_t *WN, const int N0, const int N1, const int N2, const uint64_t *seedtable, const int Nthreads)
{
    #if RANDOM_PARALLEL
    sprintf(G__msg__, "Generating white noise (using %d cores)...", Nthreads);
    PrintMessage(4, G__msg__);
    INDENT;
    #else
    UNUSED(Nthreads);
    sprintf(G__msg__, "Generating white noise (using 1 core)...");
    PrintMessage(4, G__msg__);
    INDENT;
    #endif

    const cellIndex_t N=N0*N1*N2;

    // Set the random generators
    #if RANDOM_PARALLEL
    gsl_rng *random_generator[Nthreads];
    for(int n=0; n<Nthreads; n++)
    {
        random_generator[n] = gsl_rng_alloc(gsl_rng_ranlxd2);
        gsl_rng_set(random_generator[n], (unsigned long int)seedtable[n]);
    }
    #else
    gsl_rng *random_generator;
    random_generator = gsl_rng_alloc(gsl_rng_ranlxd2);
    gsl_rng_set(random_generator, (unsigned long int)seedtable[0]);
    #endif

    // Generate white noise
    #if RANDOM_PARALLEL
    #pragma omp parallel
    {
        int rank = omp_get_thread_num(); // get rank of current process
        #pragma omp parallel for schedule(static)
        for(cellIndex_t mc=0; mc<N; mc++)
        {
            WN[mc] = (float_t)gsl_ran_gaussian(random_generator[rank], 1.);
        }
    }
    #else
    for(cellIndex_t mc=0; mc<N; mc++)
    {
        WN[mc] = (float_t)gsl_ran_gaussian(random_generator, 1.);
    }
    #endif

    // Free the random number generators
    #if RANDOM_PARALLEL
    for(int n=0; n<Nthreads; n++)
    {
        gsl_rng_free(random_generator[n]);
    }
    #else
    gsl_rng_free(random_generator);
    #endif

    #if RANDOM_PARALLEL
    UNINDENT;
    sprintf(G__msg__, "Generating white noise (using %d cores) done.", Nthreads);
    PrintMessage(4, G__msg__);
    #else
    UNINDENT;
    sprintf(G__msg__, "Generating white noise (using 1 core) done.");
    PrintMessage(4, G__msg__);
    #endif
} //generate_white_noise_array
///-------------------------------------------------------------------------------------
/** @fn upsample_wn
 * This augments a white noise array to higher resolution
 * @param *WN_IN input white noise
 * @param N0 input resolution
 * @param *WN_OUT output upsampled white noise
 * @param N0_target desired output resolution
 * @param *InputRngState input RNG state
 * @param SaveRngState save the RNG state before processing?
 * @param *SavedRngState output RNG state at the beginning
 * @param *OutputRngState output RNG state at the end
 */
void upsample_wn(const float_t *WN_IN, const int N0, float_t *WN_OUT, const int N0_target, const char *InputRngState, const bool SaveRngState, const char *SavedRngState, const char *OutputRngState)
{
    // Upsample original white-noise field
    const int N1=N0, N2=N0;
    const cellIndex_t N=N0*N1*N2;
    const int N1_target=N0_target, N2_target=N0_target;
    const cellIndex_t N_target=N0_target*N1_target*N2_target;
    const double dv=(double)N0_target/(double)N0*(double)N1_target/(double)N1*(double)N2_target/(double)N2;
    debugPrintDouble(dv);
    double aux_target[N_target];
    for(int i=0; i<N0_target; i++)
        for(int j=0; j<N1_target; j++)
            for(int k=0; k<N2_target; k++)
            {
                int ii = floor((double)i*(double)N0/(double)N0_target);
                int jj = floor((double)j*(double)N1/(double)N1_target);
                int kk = floor((double)k*(double)N2/(double)N2_target);
                aux_target[get_index(i,j,k,N0_target,N1_target,N2_target)]=WN_IN[get_index(ii,jj,kk,N0,N1,N2)]*dv;
            }

    // Set a random generator
    gsl_rng *random_generator;
    random_generator = gsl_rng_alloc(gsl_rng_ranlxd2);
    set_rng(random_generator, (char *)InputRngState);

    // Save the random number generator state before doing anything
    if(SaveRngState)
        save_rng_state(random_generator, (char *)SavedRngState);

    // Generate the seed table
    uint64_t *seedtable;
    int Nthreads = omp_get_num_threads();
    seedtable = generate_seed_table(random_generator, Nthreads, VARNAME(seedtable));

    // Generate target white noise field
    generate_white_noise_array(WN_OUT, N0_target, N1_target, N2_target, seedtable, Nthreads);

    // Downsample target white noise field
    float_t auxx[N];
    for(cellIndex_t mc=0; mc<N; mc++) auxx[mc]=0.;
    for(int i=0 ; i<N0_target; i++)
        for(int j=0 ; j<N1_target; j++)
            for(int k=0 ; k<N2_target; k++)
            {
                int ii = floor((double)i*(double)N0/(double)N0_target);
                int jj = floor((double)j*(double)N1/(double)N1_target);
                int kk = floor((double)k*(double)N2/(double)N2_target);
                auxx[get_index(ii,jj,kk,N0,N1,N2)]+=WN_OUT[get_index(i,j,k,N0_target,N1_target,N2_target)]/dv;
            }

    // Translate downsampled white noise field to target resolution
    float_t auxx_target[N_target];
    for(int i=0 ; i<N0_target; i++)
        for(int j=0 ; j<N1_target; j++)
            for(int k=0 ; k<N2_target; k++)
            {
                int ii = floor((double)i*(double)N0/(double)N0_target);
                int jj = floor((double)j*(double)N1/(double)N1_target);
                int kk = floor((double)k*(double)N2/(double)N2_target);
                auxx_target[get_index(i,j,k,N0_target,N1_target,N2_target)]=auxx[get_index(ii,jj,kk,N0,N1,N2)];
            }

    // Modify target white noise field with original white noise
    for(cellIndex_t mc=0; mc<N_target; mc++)
    {
        WN_OUT[mc]+=-auxx_target[mc]+aux_target[mc];
        WN_OUT[mc]/=dv;
    }

    // Save the random number generator state
    save_rng_state(random_generator, (char *)OutputRngState);

    // Free the random generator
    gsl_rng_free(random_generator);

    // Free memory
    p_free(seedtable, VARNAME(seedtable));
} //upsample_wn
///-------------------------------------------------------------------------------------
/** @fn tint_or_whiten_GRF
 * This routine computes the a Gaussian random field with a given power spectrum
 * or whitens a Gaussian random field given a power spectrum
 * @param tint boolean: tint or whiten?
 * @param *FIELD input field
 * @param *k_keys keys on the Fourier grid for power spectrum coefficients
 * @param *powerspectrum power spectrum values
 * @param NUM_MODES number of k modes
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 */
void tint_or_whiten_GRF(const bool tint, float_t *FIELD, const int *k_keys, const float_t *powerspectrum, const int NUM_MODES, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2)
{
    int Nthreads=omp_get_max_threads();
    const int N2_HC=N2/2+1;
    const fourierMode_t N_HC=N0*N1*N2_HC;
    if(tint)
        sprintf(G__msg__, "Generating Gaussian random field (using %d cores)...", Nthreads);
    else
        sprintf(G__msg__, "Whitening Gaussian random field (using %d cores)...", Nthreads);
    PrintMessage(3, G__msg__);
    INDENT;

    // transform white noise field to Fourier space
    fftw_complex_t *AUX; AUX = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(AUX));
    FFT_r2c_3d(N0, N1, N2, FIELD, AUX, Nthreads);

    // Fourier convention for the normalization of the power spectrum
    const double NORM=((double)N0/L0)*((double)N1/L1)*((double)N2/L2);

    // mutiply/divide the field by the square root of the power spectrum, in Fourier space
    if(tint)
    {
        #pragma omp parallel for schedule(static)
        for(fourierMode_t mf=1; mf<N_HC; mf++)
        {
                // Looks up power spectrum
                int ik = k_keys[mf];
                if(ik<0 || ik>NUM_MODES)
                                {
                                    sprintf(G__msg__, "Incorrect key (%d) in k_keys for generation of the Gaussian random field", ik);
                                    FatalError(G__msg__);
                                }
                double P_of_k = (double)powerspectrum[ik]*NORM;

                // Multiples white noise by the square root of the power spectrum
                AUX[mf] *= sqrt(P_of_k);
        }
    }
    else
    {
        #pragma omp parallel for schedule(static)
        for(fourierMode_t mf=1; mf<N_HC; mf++)
        {
                // Looks up power spectrum
                int ik = k_keys[mf];
                double P_of_k = (double)powerspectrum[ik]*NORM;

                // Divides field by the square root of the power spectrum
                AUX[mf] /= sqrt(P_of_k);
        }
    }
    // Jeans's swindle
    AUX[0]=0.;

    // transform back to real space
    FFT_c2r_3d(N0, N1, N2, AUX, FIELD, Nthreads);

    // free memory
    p_fftw_free(AUX, VARNAME(AUX));

    UNINDENT;
    if(tint)
        sprintf(G__msg__, "Generating Gaussian random field (using %d cores) done.", Nthreads);
    else
        sprintf(G__msg__, "Whitening Gaussian random field (using %d cores) done.", Nthreads);
    PrintMessage(3, G__msg__);
} //tint_or_whiten_GRF
///-------------------------------------------------------------------------------------
