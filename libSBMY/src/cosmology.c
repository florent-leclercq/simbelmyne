///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/src/cosmology.c
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file cosmology.c
 *  \brief Physical and cosmological equations
 *  \authors Florent Leclercq
 *  \version 0.5.0
 *  \date 2014-2023
 */
#include "cosmology.h"
///-------------------------------------------------------------------------------------
const double a_ref = 1.;
///-------------------------------------------------------------------------------------
/** @fn a2z
 * Redshift as a function of the scale factor
 * @param a scale factor
 * @return redshift
 */
double a2z(const double a)
{
    return 1./a-1.;
} //a2z
///-------------------------------------------------------------------------------------
/** @fn z2a
 * Scale factor as a function of redshift
 * @param a redshift
 * @return scale factor
 */
double z2a(const double z)
{
    return 1./(z+1.);
} //z2a
///-------------------------------------------------------------------------------------
/** @fn t2a
 * This routine gives the internal time variable as a function of a
 */
double t2a(const int TimeStepDistribution, const double t)
{
    double a;
    switch(TimeStepDistribution)
    {
        case 0:
            a=t;
        break;
        case 1:
            a=exp(t);
        break;
        case 2:
            a=log(t);
        break;
        default:
            FatalError("TimeStepDistribution does not exist.");
        break;
    }
    return a;
} //t2a
///-------------------------------------------------------------------------------------
/** @fn a2t
 * This routine gives a as a function of the internal time variable
 */
double a2t(const int TimeStepDistribution, const double a)
{
    double t;
    switch(TimeStepDistribution)
    {
        case 0:
            t=a;
        break;
        case 1:
            t=log(a);
        break;
        case 2:
            t=exp(a);
        break;
        default:
            FatalError("TimeStepDistribution does not exist.");
        break;
    }
    return t;
} //a2t
///-------------------------------------------------------------------------------------
/** @fn hubble
 * The Hubble parameter as a function of the scale factor
 * in units of h (normalized so that hubble_of_a(1)=100)
 * @param a scale factor
 * @param cosmo cosmological parameters (standard order)
 * @return H(a)
 */
double hubble(const double a, const double cosmo[N_COSMOPAR])
{
    double h=cosmo[0];
    double Omega_r=cosmo[1];
    double Omega_q=cosmo[2];
    double Omega_b=cosmo[3];
    double Omega_m=cosmo[4];
    double Omega_k=cosmo[5];
    double n_s=cosmo[6];
    double sigma8=cosmo[7];
    double w0_fld=cosmo[8];
    double wa_fld=cosmo[9];
    UNUSED(h);UNUSED(Omega_b);UNUSED(n_s);UNUSED(sigma8);

    double result, aux;

    result = Omega_r / gsl_pow_int(a,4); // radiation
    result += Omega_m / gsl_pow_int(a,3); // matter
    result += Omega_k / gsl_pow_int(a,2); // curvature

    aux = (1.0 + w0_fld + wa_fld) * log(a) - wa_fld * (a - 1.0); // this is the result of int((1+w(a)) dlog(a))
    result += Omega_q * exp(-3.0 * aux); // dark energy, parameterised with eos w(a) = w0_fld + (1-a)/a0 * wa_fld

    result = P_Hubble * sqrt(result);

    return result;
} //hubble
///-------------------------------------------------------------------------------------
/** @fn hubble_of_z
 * The Hubble parameter as a function of redshift
 * in units of h (normalized so that hubble_of_z(0)=100)
 * @param z redshift
 * @param cosmo cosmological parameters (standard order)
 * @return H(z)
 */
double hubble_of_z(const double z, const double cosmo[N_COSMOPAR])
{
    return hubble(z2a(z), cosmo);
} //hubble_of_z
///-------------------------------------------------------------------------------------
/** @fn aux_z2dcom
 * Subroutine for the computation of comoving distance
 */
static double aux_z2dcom(double z, void *params)
{
    double *cosmo = (double *)params;
    double h=cosmo[0];

    return P_c_light_Mpc_s / (h * P_H100_s * hubble_of_z(z, cosmo)/P_Hubble);
} // aux_z2dcom
///-------------------------------------------------------------------------------------
/** @fn z2dcom
 * Comoving distance as a function of redshift
 * @param z redshift
 * @param cosmo cosmological parameters (standard order)
 * @return dcom in Mpc
 */
double z2dcom(double z, const double cosmo[N_COSMOPAR])
{
    double result,error;
    gsl_integration_workspace *wspace = gsl_integration_workspace_alloc(NEVAL);
    gsl_function F;

    F.function = &aux_z2dcom;
    F.params = (void *)cosmo;
    gsl_integration_qag(&F,0.,z,epsabs,epsrel,NEVAL,GSL_INTEG_GAUSS61,wspace,&result,&error);

    gsl_integration_workspace_free(wspace);
    return result;
} //z2dcom
///-------------------------------------------------------------------------------------
/** @fn aux_dcom2z
 * Subroutine for the computation of redshift
 */
static double aux_dcom2z(double z, void *params)
{
    double *aux = (double *)params;
    double cosmo[N_COSMOPAR];
    for(int i=0; i<N_COSMOPAR; i++) cosmo[i] = aux[i];
    double dcom=aux[N_COSMOPAR];

    return fabs(dcom - z2dcom(z, cosmo));
} // aux_dcom2z
///-------------------------------------------------------------------------------------
/** @fn dcom2z
 * Redshift as a function of comoving distance
 * @param dcom comoving distance in Mpc
 * @param cosmo cosmological parameters (standard order)
 * @return redshift
 */
double dcom2z(double dcom, const double cosmo[N_COSMOPAR])
{
    int iter=0, max_iter=100, status;
    const gsl_min_fminimizer_type *T; T = gsl_min_fminimizer_brent;
    gsl_min_fminimizer *s; s = gsl_min_fminimizer_alloc(T);
    gsl_function F;

    double params[N_COSMOPAR+1];
    for(int i=0; i<N_COSMOPAR; i++) params[i] = cosmo[i];
    params[N_COSMOPAR] = dcom;

    double result = 0.1; double zmin = -0.1, zmax = 5.0;

    F.function = &aux_dcom2z;
    F.params = (void *)params;
    gsl_min_fminimizer_set(s, &F, result, zmin, zmax);
    do
    {
        iter++;
        status = gsl_min_fminimizer_iterate(s);

        result = gsl_min_fminimizer_x_minimum(s);
        zmin = gsl_min_fminimizer_x_lower(s);
        zmax = gsl_min_fminimizer_x_upper(s);

        status = gsl_min_test_interval(zmin, zmax, epsabs, epsrel);
    }
    while (status == GSL_CONTINUE && iter < max_iter);

    return result;
} //dcom2z
///-------------------------------------------------------------------------------------
/** @fn FHubble
 * Fhubble=H0/adot
 * @param a scale factor
 * @param cosmo cosmological parameters (standard order)
 * @return Fhubble
 */
double FHubble(const double a, const double cosmo[N_COSMOPAR])
{
    return P_Hubble/hubble(a,cosmo)/a;
} //FHubble
///-------------------------------------------------------------------------------------
/** @fn w_of_a
 * Time evolution of the dark energy equation of state parameter
 * parameterisation: w(a) = w0_fld + (1-a)/a0 * wa_fld
 * @param a scale factor
 * @param cosmo cosmological parameters (standard order)
 * @return w(a)
 */
double w_of_a(const double a, const double cosmo[N_COSMOPAR])
{
    double w0_fld=cosmo[8];
    double wa_fld=cosmo[9];
    return w0_fld + wa_fld * (1.0 - a);
} //w_of_a
///-------------------------------------------------------------------------------------
/** @fn aux_x
 * Subroutine for the computation of the growth factor
 * X(a) [auxiliary function, see eq. 9 Linder+Jenkins MNRAS 346, 573-583 (2003) for definition]
 */
static double aux_x(const double a, const double cosmo[N_COSMOPAR])
{
    double Omega_q=cosmo[2];
    double Omega_m=cosmo[4];
    double w0_fld=cosmo[8];
    double wa_fld=cosmo[9];
    return Omega_m / Omega_q * pow(a,3.0 * (w0_fld + wa_fld)) * exp(3.0 * wa_fld * (1.0 - a));
} //aux_x
///-------------------------------------------------------------------------------------
/** @fn aux_dx_da
 * Subroutine for the computation of the growth factor
 * d(X(a))/da [derivative of X(a), d(X(a))/da]
 */
static double aux_dx_da(const double a, const double cosmo[N_COSMOPAR])
{
    return 3.0 * aux_x(a,cosmo) / a * w_of_a(a,cosmo);
} //aux_dx_da
///-------------------------------------------------------------------------------------
/** @fn aux_q
 * Subroutine for the computation of the growth factor [prefactor for D', eq. 8 in Linder+Jenkins 2003]
 */
static double aux_q(const double a, const double cosmo[N_COSMOPAR])
{
    return 3.0 / 2.0 * (1.0 - w_of_a(a,cosmo) / (1.0 + aux_x(a,cosmo))) / a;
} //aux_q
///-------------------------------------------------------------------------------------
/** @fn aux_r
 * Subroutine for the computation of the growth factor [prefactor for -D, eq. 8 in Linder+Jenkins 2003]
 */
static double aux_r(const double a, const double cosmo[N_COSMOPAR])
{
    double xa = aux_x(a,cosmo);
    return 3.0 / 2.0 * xa / (1.0 + xa) / gsl_pow_int(a,2);
} //aux_r
///-------------------------------------------------------------------------------------
/** @fn aux_dq_da
 * Subroutine for the computation of the growth factor
 */
static double aux_dq_da(const double a, const double cosmo[N_COSMOPAR])
{
    double wa_fld=cosmo[9];
    double xa_p = 1.0 + aux_x(a,cosmo);
    return -aux_q(a,cosmo) / a - 3.0 / 2.0 / a / xa_p * (wa_fld - w_of_a(a,cosmo) * aux_dx_da(a,cosmo) / xa_p);
} //aux_dq_da
///-------------------------------------------------------------------------------------
/** @fn aux_dr_da
 * Subroutine for the computation of the growth factor
 */
static double aux_dr_da(const double a, const double cosmo[N_COSMOPAR])
{
    double ra = aux_r(a,cosmo);
    return aux_dx_da(a,cosmo) / (1.0 + aux_x(a,cosmo)) * (3.0 / 2.0 / gsl_pow_int(a,2) - ra) - 2.0 * ra / a;
} //aux_dr_da
///-------------------------------------------------------------------------------------
/** @fn d_plus_function
 * Computation of the growth factor
 * defines f0 = dy1/da and f1 = dy2/da
 */
static int d_plus_function(double a, const double y[], double f[], void *params)
{
    double *cosmo = (double *)params;

    /* derivatives f_i = dy_i/da */
    f[0] = y[1];
    f[1] = aux_r(a,cosmo) * y[0] - aux_q(a,cosmo) * y[1];

    return(GSL_SUCCESS);
} //d_plus_function
///-------------------------------------------------------------------------------------
/** @fn d_plus_jacobian
 * Computation of the growth factor
 */
static int d_plus_jacobian(double a, const double y[], double *dfdy, double dfda[], void *params)
{
    gsl_matrix_view dfdy_mat = gsl_matrix_view_array(dfdy,2,2);
    gsl_matrix *m = &dfdy_mat.matrix;
    double *cosmo = (double *)params;

    /* jacobian df_i(a,y(a)) / dy_j */
    gsl_matrix_set(m,0,0,0.0); //df[0]/dy[0]
    gsl_matrix_set(m,0,1,1.0); //df[0]/dy[1]
//     gsl_matrix_set(m,1,0,0.0); //df[1]/dy[0]
    gsl_matrix_set(m,1,0,aux_r(a,cosmo)); //df[1]/dy[0]
    gsl_matrix_set(m,1,1,-aux_q(a,cosmo)); //df[1]/dy[1]

    /* gradient df_i/da, explicit time dependence */
    dfda[0] = 0.0; //df[0]/da
    dfda[1] = aux_dr_da(a,cosmo) * y[0] - aux_dq_da(a,cosmo) * y[1]; //df[1]/da

    return(GSL_SUCCESS);
} //d_plus_jacobian
///-------------------------------------------------------------------------------------
/** @fn aux_d_plus
 * Computation of the growth factor
 * @param a scale factor
 * @param result_d_plus the result D+
 * @param result_d_plus_prime the result, derivative of D+
 * @param cosmo cosmological parameters (standard order)
 * @return the result D+
 */
static double aux_d_plus(const double a, double *result_d_plus, double *result_d_plus_prime, const double cosmo[N_COSMOPAR])
{
    double result;
    int status;
    const gsl_odeiv_step_type *T = gsl_odeiv_step_bsimp;
    gsl_odeiv_step *s = gsl_odeiv_step_alloc(T,2);
    gsl_odeiv_control *c = gsl_odeiv_control_y_new(0.0,epsrel);
    gsl_odeiv_evolve *e = gsl_odeiv_evolve_alloc(2);
    double adummy=AMIN, habs = 1e-4; /* adummy = initial scale factor, habs = absolute accuracy */
    double y[2] = {1.0,0.0}; /* initial conditions, dy0(0)/da = 1, dy1(0)/da=0 */

    /* result from solution of a 2nd order differential equation, transformed to a system of 2 1st order eqs */
    gsl_odeiv_system sys = { d_plus_function, d_plus_jacobian, 2, (void *)cosmo};

    while(adummy < a)
    {
        status = gsl_odeiv_evolve_apply(e,c,s,&sys,&adummy,a,&habs,y);
        if(status != GSL_SUCCESS) break;
    }

    gsl_odeiv_evolve_free(e);
    gsl_odeiv_control_free(c);
    gsl_odeiv_step_free(s);

    result = *result_d_plus = y[0]; /* d_plus */
    *result_d_plus_prime = y[1]; /* d(d_plus)/da */

    return result;
} //aux_d_plus
///-------------------------------------------------------------------------------------
/** @fn d_plus
 * Computation of the growth factor, main function
 * @param a_start scale factor at starting redshift
 * @param a_end scale factor at final redshift
 * @param cosmo cosmological parameters (standard order)
 * @return GrowthFactor(a_start, a_end)
 */
double d_plus(const double a_start, const double a_end, const double cosmo[N_COSMOPAR])
{
    double dummy,norm,result;
    aux_d_plus(a_end,&result,&dummy,cosmo);
    aux_d_plus(a_start,&norm,&dummy,cosmo);

    result /= norm;

    return result;
} //d_plus
///-------------------------------------------------------------------------------------
/** @fn d2_plus
 * Computes the second order growth factor
 * @param a_start scale factor at starting redshift
 * @param a_end scale factor at final redshift
 * @param cosmo cosmological parameters (standard order)
 * @return GrowthFactor2(a_start, a_end)
 */
double d2_plus(const double a_start, const double a_end, const double cosmo[N_COSMOPAR])
{
    double Omega_m = cosmo[4];
    double D1 = d_plus(a_start, a_end, cosmo);
//     double D2 = -3./7. * D1*D1; // better than 7% accuracy, Bouchet et al 1992
    double D2 = -3./7. * D1*D1 * pow(Omega_m,-1./143.); //for flat models with non-zero cosmological constant Λ, for 0.01 ≤ Ω ≤ 1. better than 0.6% accuracy, Bouchet et al 1995
    return D2;
} //d2_plus
///-------------------------------------------------------------------------------------
/** @fn d_plus_prime
 * Computation of the derivative of the growth factor, main function
 * function d(d_plus)/da
 * @param a_start reference scale factor
 * @param a scale factor
 * @param cosmo cosmological parameters (standard order)
 * @return dD/da at a
 */
double d_plus_prime(const double a_start, const double a, const double cosmo[N_COSMOPAR])
{
    double dummy,result_d_plus_prime,norm;

    aux_d_plus(a,&dummy,&result_d_plus_prime,cosmo);
    aux_d_plus(a_start,&norm,&dummy,cosmo);

    return result_d_plus_prime/norm;
} //d_plus_prime
///-------------------------------------------------------------------------------------
/** @fn g_plus
 * Computation of the logarithmic derivative of the growth factor, main function
 * function g_plus =dln(d_plus(a))/dln(a) = a/d_plus * dd_plus/da
 * @param a scale factor
 * @param cosmo cosmological parameters (standard order)
 * @return dlnD/dlna at a
 */
double g_plus(const double a, const double cosmo[N_COSMOPAR])
{
    double result_d_plus,result_d_plus_prime,result;

    aux_d_plus(a,&result_d_plus,&result_d_plus_prime,cosmo);

    if(a > eps) result = a / result_d_plus * result_d_plus_prime;
    else result = 1.0;

    return result;
} //g_plus
///-------------------------------------------------------------------------------------
/** @fn f2
 * Computes the logarithmic derivative of the second order growth factor
 * fitting function, equation (50) in Bouchet et al 1995, A&A 296, 575
 * or equation (D11)-(D12) in Scoccimarro 1998, MNRAS 299, 1097
 * @param f1 logarithmic derivative of the first order growth factor
 * @return f2
 */
double f2(const double f1)
{
//     return 2 * pow(Omega_a, 20./21.); //for open models with 0.1 ≤ Ω ≤ 1
    return 2 * pow(f1, 54./55.); //for flat models with non-zero cosmological constant Λ, for 0.01 ≤ Ω ≤ 1
} //f2
///-------------------------------------------------------------------------------------
/** @fn lpt_prefactors
 * Computes the prefactors for LPT displacements to get positions and velocities
 * @param a scale factor
 * @param cosmo cosmological parameters (standard order)
 * @return [D1,D2,Vel1,Vel2]
 */
double* lpt_prefactors(const double a, const double cosmo[N_COSMOPAR])
{
    // Displacement prefactors
    double D1 = d_plus(a_ref, a, cosmo);
    #ifndef ONLY_ZA
    double D2 = d2_plus(a_ref, a, cosmo);
    #else
    double D2 = 0.;
    #endif

    // Velocity prefactors
    double hubble_a = hubble(a, cosmo);
    double f1 = g_plus(a, cosmo);
    double Vel1 = -D1 * f1 * hubble_a;
    #ifndef ONLY_ZA
    double f2_ = f2(f1);
    double Vel2 = D2 * f2_ * hubble_a;
    #else
    double Vel2 = 0.;
    #endif

    static double result[4];
    result[0] = D1;
    result[1] = D2;
    result[2] = Vel1;
    result[3] = Vel2;
    return result;
} //lpt_prefactors
///-------------------------------------------------------------------------------------
/** @fn D_of_a
 * "Drift prefactor" (cf appendix B of F. Leclercq PhD Thesis, 2015 - just after equation B.8)
 * @param a scale factor
 * @param cosmo cosmological parameters (standard order)
 */
double D_of_a(double a, const double cosmo[N_COSMOPAR])
{
    double result = FHubble(a, cosmo);
    result /= a*a;
    return result;
} //D_of_a
///-------------------------------------------------------------------------------------
/** @fn aux_dtx_standard
 * Subroutine for the computation of dtx
 */
static double aux_dtx_standard(double a, void *params)
{
    const double *cosmo = (double *)params;
    return D_of_a(a, cosmo);
} //aux_dtx
///-------------------------------------------------------------------------------------
/** @fn dtx_standard
 * Subroutine for the computation of dtx
 */
static double dtx_standard(double ai, double af, const double cosmo[N_COSMOPAR])
{
    double result,error;
    gsl_integration_workspace *wspace = gsl_integration_workspace_alloc(NEVAL);
    gsl_function F;

    F.function = &aux_dtx_standard;
    F.params = (void *)cosmo;
    gsl_integration_qag(&F,ai,af,epsabs,epsrel,NEVAL,GSL_INTEG_GAUSS61,wspace,&result,&error);

    gsl_integration_workspace_free(wspace);
    return result;
} //dtx_standard
///-------------------------------------------------------------------------------------
/** @fn aux_dtx_modified
 * Subroutine for the computation of dtx
 */
static double aux_dtx_modified(double a, void *params)
{
    double *aux = (double *)params;
    double cosmo[N_COSMOPAR];
    for(int i=0; i<N_COSMOPAR; i++) cosmo[i] = aux[i];
    double n_LPT=aux[N_COSMOPAR];

    return pow(a, n_LPT) * D_of_a(a, cosmo);
} //aux_dtx_modified
///-------------------------------------------------------------------------------------
/** @fn dtx_modified
 * Subroutine for the computation of dtx
 */
static double dtx_modified(double ai, double af, double ac, double n_LPT, const double cosmo[N_COSMOPAR])
{
    double result,error;
    gsl_integration_workspace *wspace = gsl_integration_workspace_alloc(NEVAL);
    gsl_function F;

    double params[N_COSMOPAR+1];
    for(int i=0; i<N_COSMOPAR; i++) params[i] = cosmo[i];
    params[N_COSMOPAR] = n_LPT;
    F.function = &aux_dtx_modified;
    F.params = (void *)params;
    gsl_integration_qag(&F,ai,af,epsabs,epsrel,NEVAL,GSL_INTEG_GAUSS61,wspace,&result,&error);

    gsl_integration_workspace_free(wspace);
    return 1./pow(ac, n_LPT) * result;
} //dtx_modified
///-------------------------------------------------------------------------------------
/** @fn dtx
 * Function pm_time-stepping dtx
 * @param ModifiedDiscretization boolean
 * @param ai initial scale factor
 * @param af final scale factor
 * @param ac intermediate scale factor
 * @param n_LPT LPT index for modified discretization of operators in COLA (see section A.3.2 in Tassev, Zaldarriaga & Eisenstein 2015)
 * @param cosmo cosmological parameters (standard order)
 * @return dtx
 */
double dtx(const bool ModifiedDiscretization, double ai, double af, double ac, double n_LPT, const double cosmo[N_COSMOPAR])
{
    if(ModifiedDiscretization)
        return dtx_modified(ai, af, ac, n_LPT, cosmo);
    else
        return dtx_standard(ai, af, cosmo);
} //dtx
///-------------------------------------------------------------------------------------
/** @fn K_of_a
 * "Kick prefactor" (cf appendix B of F. Leclercq PhD Thesis, 2015 - just after equation B.8)
 */
double K_of_a(double a, const double cosmo[N_COSMOPAR])
{
    double Omega_m=cosmo[4];
    double result = FHubble(a, cosmo);
    result /= a;
    result *= -1.5*Omega_m;
    return result;
} //K_of_a
///-------------------------------------------------------------------------------------
/** @fn aux_dtp_standard
 * Subroutine for the computation of dtp
 */
static double aux_dtp_standard(double a, void *params)
{
    const double *cosmo = (double *)params;
    return K_of_a(a, cosmo);
} //aux_dtp_standard
///-------------------------------------------------------------------------------------
/** @fn dtp_standard
 * Subroutine for the computation of dtp
 */
static double dtp_standard(double ai, double af, const double cosmo[N_COSMOPAR])
{
    double result,error;
    gsl_integration_workspace *wspace = gsl_integration_workspace_alloc(NEVAL);
    gsl_function F;

    F.function = &aux_dtp_standard;
    F.params = (void *)cosmo;
    gsl_integration_qag(&F,ai,af,epsabs,epsrel,NEVAL,GSL_INTEG_GAUSS61,wspace,&result,&error);

    gsl_integration_workspace_free(wspace);
    return result;
} //dtp
///-------------------------------------------------------------------------------------
/** @fn dtp_modified
 * Subroutine for the computation of dtp
 */
static double dtp_modified(double ai, double af, double ac, double n_LPT, const double cosmo[N_COSMOPAR])
{
    return K_of_a(ac, cosmo) * (pow(af, n_LPT) - pow(ai, n_LPT)) / (n_LPT * pow(ac, n_LPT-1.));
} //dtp_modified
///-------------------------------------------------------------------------------------
/** @fn dtp
 * Function pm_time-stepping dtp
 * @param ModifiedDiscretization boolean
 * @param ai initial scale factor
 * @param af final scale factor
 * @param ac intermediate scale factor
 * @param n_LPT LPT index for modified discretization of operators in COLA (see section A.3.2 in Tassev, Zaldarriaga & Eisenstein 2015)
 * @param cosmo cosmological parameters (standard order)
 * @return dtp
 */
double dtp(const bool ModifiedDiscretization, double ai, double af, double ac, double n_LPT, const double cosmo[N_COSMOPAR])
{
    if(ModifiedDiscretization)
        return dtp_modified(ai, af, ac, n_LPT, cosmo);
    else
        return dtp_standard(ai, af, cosmo);
} //dtp
///-------------------------------------------------------------------------------------
/** @fn dp_lpt
 * Function kick COLA prefactors
 * (cf appendix B of F. Leclercq, PhD Thesis 2015 - equation B.65)
 * @param beta_a prefactor
 * @param a_c current scale factor at the middle of the timestep (aDrift)
 * @param cosmo cosmological parameters (standard order)
 * @return [beta_lpt_1, beta_lpt_2]
 */
double* dp_lpt(const double beta_a, const double a_c, const double cosmo[N_COSMOPAR])
{
    double D1_c        = d_plus(a_ref, a_c, cosmo);
    double beta_lpt_1    = -beta_a*D1_c;
    #ifndef ONLY_ZA
    double D2_c        = d2_plus(a_ref, a_c, cosmo);
    double beta_lpt_2    = beta_a*(D2_c - D1_c*D1_c);
    #else
    double beta_lpt_2    = 0.;
    #endif

    static double result[2];
    result[0]        = beta_lpt_1;
    result[1]        = beta_lpt_2;
    return result;
} //dp_lpt
///-------------------------------------------------------------------------------------
/** @fn dx_lpt
 * Function drift COLA prefactors
 * (cf appendix B of F. Leclercq, PhD Thesis 2015 - equation B.64)
 * @param ai initial scale factor
 * @param af final scale factor
 * @param cosmo cosmological parameters (standard order)
 * @return [gamma1, gamma2]
 */
double* dx_lpt(const double ai, const double af, const double cosmo[N_COSMOPAR])
{
    double D1_f        = d_plus(a_ref, af, cosmo);
    double D1_i        = d_plus(a_ref, ai, cosmo);
    double gamma1        = -( D1_f - D1_i );
    #ifndef ONLY_ZA
    double D2_f        = d2_plus(a_ref, af, cosmo);
    double D2_i        = d2_plus(a_ref, ai, cosmo);
    double gamma2        = D2_f - D2_i;
    #else
    double gamma2        = 0.;
    #endif

    static double result[2];
    result[0]        = gamma1;
    result[1]        = gamma2;
    return result;
} //dx_lpt
///-------------------------------------------------------------------------------------
