///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/src/dmsheet.c
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file dmsheet.c
 *  \brief Routines for description of the dark matter phase-space sheet
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2016-2023
 */
#include "dmsheet.h"
///-------------------------------------------------------------------------------------
/** @fn get_tetrahedron_indices
 * Gets the particle indices of a tetrahedron
 * @param Id particle id
 * @param itet the id of the tetrahedron (0 to 5)
 * @param Np0 particle mesh size x
 * @param Np1 particle mesh size y
 * @param Np2 particle mesh size z
 * @param *TetrahedronIndices output tetrahedron particle indices
 */
void get_tetrahedron_indices(const particleID_t Id, const int itet, const int Np0, const int Np1, const int Np2, particleID_t *TetrahedronIndices)
{
    int indices[3];
    get_Lagrangian_indices(Id, Np0, Np1, Np2, indices);
    int i = indices[0];
    int j = indices[1];
    int k = indices[2];
    int ii = p_mod(i+1,Np0);
    int jj = p_mod(j+1,Np1);
    int kk = p_mod(k+1,Np2);

    particleID_t mpA, mpB, mpC, mpD;

    switch(itet)
    {
        case 0:
        // Tetrahedron 1: (0,1,3,4)
        mpA = get_Lagrangian_Id(i,j,k, Np0,Np1,Np2); //0
        mpB = get_Lagrangian_Id(ii,j,k,Np0,Np1,Np2); //1
        mpC = get_Lagrangian_Id(i,jj,k,Np0,Np1,Np2); //3
        mpD = get_Lagrangian_Id(i,j,kk,Np0,Np1,Np2); //4
        break;

        case 1:
        // Tetrahedron 2: (1,3,4,7)
        mpA = get_Lagrangian_Id(ii,j,k, Np0,Np1,Np2); //1
        mpB = get_Lagrangian_Id(i,jj,k, Np0,Np1,Np2); //3
        mpC = get_Lagrangian_Id(i,j,kk, Np0,Np1,Np2); //4
        mpD = get_Lagrangian_Id(i,jj,kk,Np0,Np1,Np2); //7
        break;

        case 2:
        // Tetrahedron 3: (1,4,5,7)
        mpA = get_Lagrangian_Id(ii,j,k, Np0,Np1,Np2); //1
        mpB = get_Lagrangian_Id(i,j,kk, Np0,Np1,Np2); //4
        mpC = get_Lagrangian_Id(ii,j,kk,Np0,Np1,Np2); //5
        mpD = get_Lagrangian_Id(i,jj,kk,Np0,Np1,Np2); //7
        break;

        case 3:
        // Tetrahedron 4: (1,2,5,7)
        mpA = get_Lagrangian_Id(ii,j,k, Np0,Np1,Np2); //1
        mpB = get_Lagrangian_Id(ii,jj,k,Np0,Np1,Np2); //2
        mpC = get_Lagrangian_Id(ii,j,kk,Np0,Np1,Np2); //5
        mpD = get_Lagrangian_Id(i,jj,kk,Np0,Np1,Np2); //7
        break;

        case 4:
        // Tetrahedron 5: (1,2,3,7)
        mpA = get_Lagrangian_Id(ii,j,k, Np0,Np1,Np2); //1
        mpB = get_Lagrangian_Id(ii,jj,k,Np0,Np1,Np2); //2
        mpC = get_Lagrangian_Id(i,jj,k, Np0,Np1,Np2); //3
        mpD = get_Lagrangian_Id(i,jj,kk,Np0,Np1,Np2); //7
        break;

        case 5:
        // Tetrahedron 6: (2,5,6,7)
        mpA = get_Lagrangian_Id(ii,jj,k, Np0,Np1,Np2); //2
        mpB = get_Lagrangian_Id(ii,j,kk, Np0,Np1,Np2); //5
        mpC = get_Lagrangian_Id(ii,jj,kk,Np0,Np1,Np2); //6
        mpD = get_Lagrangian_Id(i,jj,kk, Np0,Np1,Np2); //7
        break;
    }

    TetrahedronIndices[0] = mpA;
    TetrahedronIndices[1] = mpB;
    TetrahedronIndices[2] = mpC;
    TetrahedronIndices[3] = mpD;
} //get_tetrahedron_indices
///-------------------------------------------------------------------------------------
/** @fn get_tetrahedron_coords
 * Gets physical coordinates of the tetrahedron vertices
 * @param P Snapshot
 * @param mpA particle Id first vertex
 * @param mpB particle Id second vertex
 * @param mpC particle Id third vertex
 * @param mpD particle Id fourth vertex
 * @param *TetrahedronCoords output tetrahedron vertices coordinates
 */
void get_tetrahedron_coords(const Snapshot P, const int mpA, const int mpB, const int mpC, const int mpD, float_t *TetrahedronCoords)
{
    const double L0 = P.header.BoxSize, L1 = P.header.BoxSize, L2=P.header.BoxSize;
    float_t xA = P.positions[mpA].Pos[0], yA = P.positions[mpA].Pos[1], zA = P.positions[mpA].Pos[2];
    float_t xB = P.positions[mpB].Pos[0], yB = P.positions[mpB].Pos[1], zB = P.positions[mpB].Pos[2];
    float_t xC = P.positions[mpC].Pos[0], yC = P.positions[mpC].Pos[1], zC = P.positions[mpC].Pos[2];
    float_t xD = P.positions[mpD].Pos[0], yD = P.positions[mpD].Pos[1], zD = P.positions[mpD].Pos[2];

    // Correction for periodic boundary conditions
    float_t xmax = max(max(max(xA,xB),xC),xD);
    if((xmax-xA) > (L0-xmax)+xA)
        xA+=L0;
    if((xmax-xB) > (L0-xmax)+xB)
        xB+=L0;
    if((xmax-xC) > (L0-xmax)+xC)
        xC+=L0;
    if((xmax-xD) > (L0-xmax)+xD)
        xD+=L0;
    float_t ymax = max(max(max(yA,yB),yC),yD);
    if((ymax-yA) > (L1-ymax)+yA)
        yA+=L1;
    if((ymax-yB) > (L1-ymax)+yB)
        yB+=L1;
    if((ymax-yC) > (L1-ymax)+yC)
        yC+=L1;
    if((ymax-yD) > (L1-ymax)+yD)
        yD+=L1;
    float_t zmax = max(max(max(zA,zB),zC),zD);
    if((zmax-zA) > (L2-zmax)+zA)
        zA+=L2;
    if((zmax-zB) > (L2-zmax)+zB)
        zB+=L2;
    if((zmax-zC) > (L2-zmax)+zC)
        zC+=L2;
    if((zmax-zD) > (L2-zmax)+zD)
        zD+=L2;

    TetrahedronCoords[0] = xA; TetrahedronCoords[1] = yA; TetrahedronCoords[2] = zA;
    TetrahedronCoords[3] = xB; TetrahedronCoords[4] = yB; TetrahedronCoords[5] = zB;
    TetrahedronCoords[6] = xC; TetrahedronCoords[7] = yC; TetrahedronCoords[8] = zC;
    TetrahedronCoords[9] = xD; TetrahedronCoords[10] = yD; TetrahedronCoords[11] = zD;
} //get_tetrahedron_coords
///-------------------------------------------------------------------------------------
/** @fn get_tetrahedron_volume
 * Gets the volume of a tetrahedron
 * @param *TetrahedronCoords input tetrahedron vertices positions
 * @return volume in UnitLength**3 (typically (Mpc/h)**3)
 */
float_t get_tetrahedron_volume(const float_t *TetrahedronCoords)
{
    float_t xA,yA,zA,xB,yB,zB,xC,yC,zC,xD,yD,zD;
    xA = TetrahedronCoords[0]; yA = TetrahedronCoords[1]; zA = TetrahedronCoords[2];
    xB = TetrahedronCoords[3]; yB = TetrahedronCoords[4]; zB = TetrahedronCoords[5];
    xC = TetrahedronCoords[6]; yC = TetrahedronCoords[7]; zC = TetrahedronCoords[8];
    xD = TetrahedronCoords[9]; yD = TetrahedronCoords[10]; zD = TetrahedronCoords[11];

    // triple scalar product of the vectors
    // a factor of 6 because the cube is tessellated into 6 tetrahedra
    // (section 2.2 in Abel, Hahn & Kaehler 2012)
    float_t Vtet = determinant3x3(xB-xA,xC-xA,xD-xA,yB-yA,yC-yA,yD-yA,zB-zA,zC-zA,zD-zA)/6.;

    return Vtet;
} //get_tetrahedron_volume
///-------------------------------------------------------------------------------------
/** @fn aux_get_masstracer_coords
 * Subroutine of get_masstracer_coords
 */
static void aux_get_masstracer_coords(const float_t xS, const float_t yS, const float_t zS, const float_t xM, const float_t yM, const float_t zM, float_t *MassTracerCoords)
{
    // Matches the monopole and quadrupole moments
    // of the mass distribution of the homogeneous tetrahedron
    // (section 2.2 in Hahn, Abel & Kaehler 2013)
    MassTracerCoords[0] = sqrt(5.)/5.*xS + (1-sqrt(5.)/5.)*xM;
    MassTracerCoords[1] = sqrt(5.)/5.*yS + (1-sqrt(5.)/5.)*yM;
    MassTracerCoords[2] = sqrt(5.)/5.*zS + (1-sqrt(5.)/5.)*zM;
} //aux_get_masstracer_coords
///-------------------------------------------------------------------------------------
/** @fn get_masstracer_coords
 * Gets coordinates of the mass tracer for one side of a tetrahedron
 * @param *TetrahedronCoords input tetrahedron vertices coordinates
 * @param iside the id of the tetrahedron side (0 to 4)
 * @param *MassTracerCoords output coordinates of the mass tracer
 */
void get_masstracer_coords(const float_t *TetrahedronCoords, const int iside, float_t *MassTracerCoords)
{
    float_t xA,yA,zA,xB,yB,zB,xC,yC,zC,xD,yD,zD,xM,yM,zM;
    xA = TetrahedronCoords[0]; yA = TetrahedronCoords[1]; zA = TetrahedronCoords[2];
    xB = TetrahedronCoords[3]; yB = TetrahedronCoords[4]; zB = TetrahedronCoords[5];
    xC = TetrahedronCoords[6]; yC = TetrahedronCoords[7]; zC = TetrahedronCoords[8];
    xD = TetrahedronCoords[9]; yD = TetrahedronCoords[10]; zD = TetrahedronCoords[11];
    xM = (xA+xB+xC+xD)/4.; yM = (yA+yB+yC+yD)/4.; zM = (zA+zB+zC+zD)/4.;

    switch(iside)
    {
        case 0:
            aux_get_masstracer_coords(xA,yA,zA, xM,yM,zM, MassTracerCoords);
        break;

        case 1:
            aux_get_masstracer_coords(xB,yB,zB, xM,yM,zM, MassTracerCoords);
        break;

        case 2:
            aux_get_masstracer_coords(xC,yC,zC, xM,yM,zM, MassTracerCoords);
        break;

        case 3:
            aux_get_masstracer_coords(xD,yD,zD, xM,yM,zM, MassTracerCoords);
        break;
    }
} //get_masstracer_coords
///-------------------------------------------------------------------------------------
/** @fn isInTetrahedron
 * Checks if a spatial position is inside a tetrahedron
 * @param *TetrahedronCoords input tetrahedron vertices coordinates
 * @param D0 oriented volume of the tetrahedron
 * @param xP position x
 * @param yP position y
 * @param zP position z
 */
bool isInTetrahedron(const float_t *TetrahedronCoords, const float_t D0, const float_t xP, const float_t yP, const float_t zP)
{
    float_t xA,yA,zA,xB,yB,zB,xC,yC,zC,xD,yD,zD;
    xA = TetrahedronCoords[0]; yA = TetrahedronCoords[1]; zA = TetrahedronCoords[2];
    xB = TetrahedronCoords[3]; yB = TetrahedronCoords[4]; zB = TetrahedronCoords[5];
    xC = TetrahedronCoords[6]; yC = TetrahedronCoords[7]; zC = TetrahedronCoords[8];
    xD = TetrahedronCoords[9]; yD = TetrahedronCoords[10]; zD = TetrahedronCoords[11];

    // As a consistency check, D0 should be D1+D2+D3+D4
    float_t D1 = (float_t)determinant4x4(xP,yP,zP,1., xB,yB,zB,1., xC,yC,zC,1., xD,yD,zD,1.);
    if(!sameSign(D1,D0))
        return false;

    float_t D2 = (float_t)determinant4x4(xA,yA,zA,1., xP,yP,zP,1., xC,yC,zC,1., xD,yD,zD,1.);
    if(!sameSign(D2,D0))
        return false;

    float_t D3 = (float_t)determinant4x4(xA,yA,zA,1., xB,yB,zB,1., xP,yP,zP,1., xD,yD,zD,1.);
    if(!sameSign(D3,D0))
        return false;

    float_t D4 = (float_t)determinant4x4(xA,yA,zA,1., xB,yB,zB,1., xC,yC,zC,1., xP,yP,zP,1.);
    if(!sameSign(D4,D0))
        return false;

    return true;
} //isInTetrahedron
///-------------------------------------------------------------------------------------
/** @fn project_tetrahedron
 * Projects a tetrahedron to the Eulerian grid
 * @param *TetrahedronCoords input tetrahedron vertices coordinates
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 * @param SWITCHi bool: use grid number i? (1<=i<=9)
 * @param GRIDi Eulerian grid to be updated
 * @param fiA weight for particle A and grid i
 * @param fiB weight for particle B and grid i
 * @param fiC weight for particle C and grid i
 * @param fiD weight for particle D and grid i
 * @param mcMax minimum cell index
 * @param mcMin minimum cell index
 */
void project_tetrahedron(const float_t *TetrahedronCoords, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2, const bool SWITCH1, float_t *GRID1, const float_t f1A, const float_t f1B, const float_t f1C, const float_t f1D, const bool SWITCH2, float_t *GRID2, const float_t f2A, const float_t f2B, const float_t f2C, const float_t f2D, const bool SWITCH3, float_t *GRID3, const float_t f3A, const float_t f3B, const float_t f3C, const float_t f3D, const bool SWITCH4, float_t *GRID4, const float_t f4A, const float_t f4B, const float_t f4C, const float_t f4D, const bool SWITCH5, float_t *GRID5, const float_t f5A, const float_t f5B, const float_t f5C, const float_t f5D, const bool SWITCH6, float_t *GRID6, const float_t f6A, const float_t f6B, const float_t f6C, const float_t f6D, const bool SWITCH7, float_t *GRID7, const float_t f7A, const float_t f7B, const float_t f7C, const float_t f7D, const bool SWITCH8, float_t *GRID8, const float_t f8A, const float_t f8B, const float_t f8C, const float_t f8D, const bool SWITCH9, float_t *GRID9, const float_t f9A, const float_t f9B, const float_t f9C, const float_t f9D, int *mc0Min, int *mc0Max, int *mc1Min, int *mc1Max, int *mc2Min, int *mc2Max)
{
    const double d0 = L0/N0;
    const double d1 = L1/N1;
    const double d2 = L2/N2;

    float_t xA,yA,zA,xB,yB,zB,xC,yC,zC,xD,yD,zD;
    xA = TetrahedronCoords[0]; yA = TetrahedronCoords[1]; zA = TetrahedronCoords[2];
    xB = TetrahedronCoords[3]; yB = TetrahedronCoords[4]; zB = TetrahedronCoords[5];
    xC = TetrahedronCoords[6]; yC = TetrahedronCoords[7]; zC = TetrahedronCoords[8];
    xD = TetrahedronCoords[9]; yD = TetrahedronCoords[10]; zD = TetrahedronCoords[11];

    float_t xmin = min(min(min(xA,xB),xC),xD), xmax = max(max(max(xA,xB),xC),xD);
    float_t ymin = min(min(min(yA,yB),yC),yD), ymax = max(max(max(yA,yB),yC),yD);
    float_t zmin = min(min(min(zA,zB),zC),zD), zmax = max(max(max(zA,zB),zC),zD);

    int imin = (int)(floor(xmin/d0)), imax=(int)(floor(xmax/d0));
    int jmin = (int)(floor(ymin/d1)), jmax=(int)(floor(ymax/d1));
    int kmin = (int)(floor(zmin/d2)), kmax=(int)(floor(zmax/d2));

    float_t D0 = (float_t)determinant4x4(xA,yA,zA,1., xB,yB,zB,1., xC,yC,zC,1., xD,yD,zD,1.);

    // Check if this grid point is in the tetrahedron and project
    /// \note in this loop it is possible to have i>N0, j>N1, k>N2 !!
    for(int i=imin; i<=imax; i++)
        for(int j=jmin; j<=jmax; j++)
            for(int k=kmin; k<=kmax; k++)
            {
                // check the current grid point
                float_t x=i*d0;
                float_t y=j*d1;
                float_t z=k*d2;
                if(isInTetrahedron(TetrahedronCoords,D0, x,y,z))
                {
                    // important: check periodic boundary conditions here
                    int mc0=p_mod(i,N0);
                    int mc1=p_mod(j,N1);
                    int mc2=p_mod(k,N2);
                    cellIndex_t mc = get_index(mc0,mc1,mc2,N0,N1,N2);
                    *mc0Min = min(*mc0Min, mc0);
                    *mc0Max = max(*mc0Max, mc0);
                    *mc1Min = min(*mc1Min, mc1);
                    *mc1Max = max(*mc1Max, mc1);
                    *mc2Min = min(*mc2Min, mc2);
                    *mc2Max = max(*mc2Max, mc2);

                    // interpolate with Shepard's method (1/distance^2 weights)
                    float_t wA = 1/((x-xA)*(x-xA) + (y-yA)*(y-yA) + (z-zA)*(z-zA));
                    float_t wB = 1/((x-xB)*(x-xB) + (y-yB)*(y-yB) + (z-zB)*(z-zB));
                    float_t wC = 1/((x-xC)*(x-xC) + (y-yC)*(y-yC) + (z-zC)*(z-zC));
                    float_t wD = 1/((x-xD)*(x-xD) + (y-yD)*(y-yD) + (z-zD)*(z-zD));
                    float_t w  = wA+wB+wC+wD;

                    if(SWITCH1) GRID1[mc] += (wA*f1A + wB*f1B + wC*f1C + wD*f1D) / w;
                    if(SWITCH2) GRID2[mc] += (wA*f2A + wB*f2B + wC*f2C + wD*f2D) / w;
                    if(SWITCH3) GRID3[mc] += (wA*f3A + wB*f3B + wC*f3C + wD*f3D) / w;
                    if(SWITCH4) GRID4[mc] += (wA*f4A + wB*f4B + wC*f4C + wD*f4D) / w;
                    if(SWITCH5) GRID5[mc] += (wA*f5A + wB*f5B + wC*f5C + wD*f5D) / w;
                    if(SWITCH6) GRID6[mc] += (wA*f6A + wB*f6B + wC*f6C + wD*f6D) / w;
                    if(SWITCH7) GRID7[mc] += (wA*f7A + wB*f7B + wC*f7C + wD*f7D) / w;
                    if(SWITCH8) GRID8[mc] += (wA*f8A + wB*f8B + wC*f8C + wD*f8D) / w;
                    if(SWITCH9) GRID9[mc] += (wA*f9A + wB*f9B + wC*f9C + wD*f9D) / w;
                }
            }
} //project_tetrahedron
///-------------------------------------------------------------------------------------
/** @fn get_masstracers
 * Returns a Snapshot containing mass tracers from flow tracers
 * @param P_FLOW input Snapshot containing the flow tracers
 * @param Np0 particle mesh size x
 * @param Np1 particle mesh size y
 * @param Np2 particle mesh size z
 * @return P_MASS
 * @warning Take care to use the same type for flow tracers and mass tracers IDs in this function!
 */
Snapshot get_masstracers(const Snapshot P_FLOW, const int Np0, const int Np1, const int Np2)
{
    const double L0 = P_FLOW.header.BoxSize;
    const double L1 = P_FLOW.header.BoxSize;
    const double L2 = P_FLOW.header.BoxSize;
    const particleID_t NpF = (particleID_t)(P_FLOW.header.npart[0]+P_FLOW.header.npart[1]+P_FLOW.header.npart[2]+P_FLOW.header.npart[3]+P_FLOW.header.npart[4]+P_FLOW.header.npart[5]);
    const massparticleID_t NpM = 24*NpF;
    // Types should match here!

    // Prepare snapshot for mass tracers
    // There is exactly 24 times more mass tracers than flow tracers
    Snapshot P_MASS;
    P_MASS.header = P_FLOW.header;
    P_MASS.header.npart[1] *= 24;
    P_MASS.header.npartTotal[1] *= 24;
    P_MASS.header.mass[1] /= 24;

    debugPrintDouble(P_FLOW.header.mass[1]);
    debugPrintDouble(P_MASS.header.mass[1]);

    allocate_snapshot(&P_MASS, NpM, VARNAME(P_MASS));

    // Initialize arrays
    #pragma omp parallel for schedule(static)
    for(massparticleID_t mpM=0; mpM<NpM; mpM++)
    {
        #ifdef HAVE_TYPE_AND_MASS
        P_MASS.types[mpM].Type = 1; // Dark matter halos only
        P_MASS.masses[mpM].Mass = P_MASS.header.mass[1];
        #endif
        P_MASS.velocities[mpM].Vel[0] = P_MASS.velocities[mpM].Vel[1] = P_MASS.velocities[mpM].Vel[2] = 0.;
        P_MASS.ids[mpM].Id = mpM;
        #ifdef HAVE_EXTRAS
        P_MASS.extras[mpM].U = P_MASS.extras[mpM].Rho = P_MASS.extras[mpM].Hsml = 0;
        #endif
    }

    // Loop on all particles
    #pragma omp parallel for schedule(static)
    for(particleID_t mpF=0; mpF<NpF; mpF++)
    {
        particleID_t Id = P_FLOW.ids[mpF].Id;
        // Loop on the 6 tetrahedra for this particle
        for(int itet=0; itet<6; itet++)
        {
            particleID_t mpM, mpA, mpB, mpC, mpD;

            // Get the indices of the 4 vertices
            particleID_t TetrahedronIndices[4];
            get_tetrahedron_indices(Id, itet, Np0, Np1, Np2, TetrahedronIndices);
            mpA = TetrahedronIndices[0];
            mpB = TetrahedronIndices[1];
            mpC = TetrahedronIndices[2];
            mpD = TetrahedronIndices[3];

            // Get the coordinates
            float_t TetrahedronCoords[12];
            get_tetrahedron_coords(P_FLOW, mpA, mpB, mpC, mpD, TetrahedronCoords);

            // Loop on the 4 sides for this tetrahedron
            for(int iside=0; iside<4; iside++)
            {
                // Get the positions of the mass tracers
                mpM = 24*mpF + 4*itet + iside;
                float_t MassTracerCoords[3];
                get_masstracer_coords(TetrahedronCoords, iside, MassTracerCoords);

                // check periodic boundary conditions here
                P_MASS.positions[mpM].Pos[0] = p_mod(MassTracerCoords[0],L0);
                P_MASS.positions[mpM].Pos[1] = p_mod(MassTracerCoords[1],L1);
                P_MASS.positions[mpM].Pos[2] = p_mod(MassTracerCoords[2],L2);
            } //end loop on sides
        } //end loop on tetrahedra
    } //end loop on particles

    return P_MASS;
} //get_masstracers
///-------------------------------------------------------------------------------------
/** @fn lagrangian_transport
 * Does the Lagrangian transport of various quantities
 * @param P_FLOW input snapshot containing the flow tracers
 * @param OBJECTS input structure types on the Lagrangian grid
 * @param Np0 particle mesh size x
 * @param Np1 particle mesh size y
 * @param Np2 particle mesh size z
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param doDENSITYMASSTRACERS bool: output density contrast from the mass tracers?
 * @param DENSITYMASSTRACERS output density contrast from the mass tracers on the fly (Eulerian grid)
 * @param doPRIMORDIALSTREAMDENSITY bool: output primordial stream density?
 * @param PRIMORDIALSTREAMDENSITY output primordial stream density (Lagrangian grid)
 * @param doSECONDARYSTREAMDENSITY bool: output secondary stream density?
 * @param SECONDARYSTREAMDENSITY output secondary stream density (Eulerian grid)
 * @param doDENSITYTETRAHEDRA bool: output density from projecting all tetrahedra?
 * @param DENSITYTETRAHEDRA output density from projecting all tetrahedra (Eulerian grid)
 * @param doVELOCITY bool: output velocity field?
 * @param VELOCITY_X output velocity field x (Eulerian grid)
 * @param VELOCITY_Y output velocity field y (Eulerian grid)
 * @param VELOCITY_Z output velocity field z (Eulerian grid)
 * @param doSTRUCTURES bool: output structure types?
 * @param VOIDS output probability voids (Eulerian grid)
 * @param SHEETS output probability sheets (Eulerian grid)
 * @param FILAMENTS output probability filaments (Eulerian grid)
 * @param CLUSTERS output probability clusters (Eulerian grid)
 */
void lagrangian_transport(const Snapshot P_FLOW, const float_t *OBJECTS, const int Np0, const int Np1, const int Np2, const int N0, const int N1, const int N2, const bool doDENSITYMASSTRACERS, float_t *DENSITYMASSTRACERS, const bool doPRIMORDIALSTREAMDENSITY, float_t *PRIMORDIALSTREAMDENSITY, const bool doSECONDARYSTREAMDENSITY, float_t *SECONDARYSTREAMDENSITY, const bool doDENSITYTETRAHEDRA, float_t *DENSITYTETRAHEDRA, const bool doVELOCITY, float_t *VELOCITY_X, float_t *VELOCITY_Y, float_t *VELOCITY_Z, const bool doSTRUCTURES, float_t *VOIDS, float_t *SHEETS, float_t *FILAMENTS, float_t *CLUSTERS)
{
    const int Nthreads_max = omp_get_max_threads();
    const double m = P_FLOW.header.mass[1];
    const double L0 = P_FLOW.header.BoxSize, d0=L0/N0;
    const double L1 = P_FLOW.header.BoxSize, d1=L1/N1;
    const double L2 = P_FLOW.header.BoxSize, d2=L2/N2;
    const double d = L0/Np0;
    const cellIndex_t N=N0*N1*N2;
    const particleID_t NpF = (particleID_t)(P_FLOW.header.npart[0]+P_FLOW.header.npart[1]+P_FLOW.header.npart[2]+P_FLOW.header.npart[3]+P_FLOW.header.npart[4]+P_FLOW.header.npart[5]);

    // check some conditions
    bool needDENSITYTETRAHEDRA = doDENSITYTETRAHEDRA;
    if(doVELOCITY || doSTRUCTURES) needDENSITYTETRAHEDRA = true;

    // declare and initialize auxiliary arrays
    float_t *threadedDENSITYMASSTRACERS[Nthreads_max];
    float_t *threadedPRIMORDIALSTREAMDENSITY[Nthreads_max];
    float_t *threadedSECONDARYSTREAMDENSITY[Nthreads_max];
    float_t *threadedDENSITYTETRAHEDRA[Nthreads_max];
    float_t *threadedVELOCITY_X[Nthreads_max], *threadedVELOCITY_Y[Nthreads_max], *threadedVELOCITY_Z[Nthreads_max];
    float_t *threadedVOIDS[Nthreads_max], *threadedSHEETS[Nthreads_max], *threadedFILAMENTS[Nthreads_max], *threadedCLUSTERS[Nthreads_max];
    bool threadActivated[Nthreads_max];
    int mc0UsedMinDensity[Nthreads_max], mc0UsedMaxDensity[Nthreads_max];
    int mc1UsedMinDensity[Nthreads_max], mc1UsedMaxDensity[Nthreads_max];
    int mc2UsedMinDensity[Nthreads_max], mc2UsedMaxDensity[Nthreads_max];
    int mc0UsedMinTet[Nthreads_max], mc0UsedMaxTet[Nthreads_max];
    int mc1UsedMinTet[Nthreads_max], mc1UsedMaxTet[Nthreads_max];
    int mc2UsedMinTet[Nthreads_max], mc2UsedMaxTet[Nthreads_max];
    for(int t=0; t<Nthreads_max; t++)
    {
        threadActivated[t]=false;
        mc0UsedMinDensity[t]=N0-1; mc1UsedMinDensity[t]=N1-1; mc2UsedMinDensity[t]=N2-1;
        mc0UsedMaxDensity[t]=0; mc1UsedMaxDensity[t]=0; mc2UsedMaxDensity[t]=0;
        mc0UsedMinTet[t]=N0-1; mc1UsedMinTet[t]=N1-1; mc2UsedMinTet[t]=N2-1;
        mc0UsedMaxTet[t]=0; mc1UsedMaxTet[t]=0; mc2UsedMaxTet[t]=0;
    }

    // get number of possible threads from available memory
    const long long  N_arrays = doDENSITYMASSTRACERS*(long long)N + doSECONDARYSTREAMDENSITY*(long long)N + needDENSITYTETRAHEDRA*(long long)N + doVELOCITY*3*(long long)N + doSTRUCTURES*4*(long long)N + doPRIMORDIALSTREAMDENSITY*(long long)NpF;
    int Nthreads = get_Nthreads_from_available_memory(N_arrays, sizeof(float_t), Nthreads_max);

    // initialize principal arrays
    if(doDENSITYMASSTRACERS)
    {
        #pragma omp parallel for schedule(static)
        for(cellIndex_t mc=0; mc<N; mc++)
            DENSITYMASSTRACERS[mc]=0.;
    }
    if(doPRIMORDIALSTREAMDENSITY)
    {
        #pragma omp parallel for schedule(static)
        for(cellIndex_t mc=0; mc<N; mc++)
            SECONDARYSTREAMDENSITY[mc]=0.;
    }
    if(needDENSITYTETRAHEDRA)
    {
        #pragma omp parallel for schedule(static)
        for(cellIndex_t mc=0; mc<N; mc++)
            DENSITYTETRAHEDRA[mc]=0.;
    }
    if(doVELOCITY)
    {
        #pragma omp parallel for schedule(static)
        for(cellIndex_t mc=0; mc<N; mc++)
        {
            VELOCITY_X[mc]=0.;
            VELOCITY_Y[mc]=0.;
            VELOCITY_Z[mc]=0.;
        }
    }
    if(doSTRUCTURES)
    {
        #pragma omp parallel for schedule(static)
        for(cellIndex_t mc=0; mc<N; mc++)
        {
            VOIDS[mc]=0.;
            SHEETS[mc]=0.;
            FILAMENTS[mc]=0.;
            CLUSTERS[mc]=0.;
        }
    }
    if(doPRIMORDIALSTREAMDENSITY)
    {
        #pragma omp parallel for schedule(static)
        for(particleID_t mpF=0; mpF<NpF; mpF++)
        {
            PRIMORDIALSTREAMDENSITY[mpF] = 0.;
        }
    }

    // main business here
    #pragma omp parallel num_threads(Nthreads)
    {
        int t = omp_get_thread_num(); // get rank of current process
        threadActivated[t] = true;

        if(t!=0)
        {
            char name[50];
            // allocate and initialize secondary arrays
            if(doDENSITYMASSTRACERS)
            {
                sprintf(name, "threadedDENSITYMASSTRACERS[%d]", t); threadedDENSITYMASSTRACERS[t] = (float_t*)p_calloc(N, sizeof(float_t), name);
            }
            if(doSECONDARYSTREAMDENSITY)
            {
                sprintf(name, "threadedSECONDARYSTREAMDENSITY[%d]", t); threadedSECONDARYSTREAMDENSITY[t] = (float_t*)p_calloc(N, sizeof(float_t), name);
            }
            if(needDENSITYTETRAHEDRA)
            {
                sprintf(name, "threadedDENSITYTETRAHEDRA[%d]", t); threadedDENSITYTETRAHEDRA[t] = (float_t*)p_calloc(N, sizeof(float_t), name);
            }
            if(doVELOCITY)
            {
                sprintf(name, "threadedVELOCITY_X[%d]", t); threadedVELOCITY_X[t] = (float_t*)p_calloc(N, sizeof(float_t), name);
                sprintf(name, "threadedVELOCITY_Y[%d]", t); threadedVELOCITY_Y[t] = (float_t*)p_calloc(N, sizeof(float_t), name);
                sprintf(name, "threadedVELOCITY_Z[%d]", t); threadedVELOCITY_Z[t] = (float_t*)p_calloc(N, sizeof(float_t), name);
            }
            if(doSTRUCTURES)
            {
                sprintf(name, "threadedVOIDS[%d]", t); threadedVOIDS[t] = (float_t*)p_calloc(N, sizeof(float_t), name);
                sprintf(name, "threadedSHEETS[%d]", t); threadedSHEETS[t] = (float_t*)p_calloc(N, sizeof(float_t), name);
                sprintf(name, "threadedFILAMENTS[%d]", t); threadedFILAMENTS[t] = (float_t*)p_calloc(N, sizeof(float_t), name);
                sprintf(name, "threadedCLUSTERS[%d]", t); threadedCLUSTERS[t] = (float_t*)p_calloc(N, sizeof(float_t), name);
            }
            if(doPRIMORDIALSTREAMDENSITY)
            {
                sprintf(name, "threadedPRIMORDIALSTREAMDENSITY[%d]", t); threadedPRIMORDIALSTREAMDENSITY[t] = (float_t*)p_calloc(NpF, sizeof(float_t), name);
            }
        }
        else if(t==0)
        {
            // use the already existing and initialized arrays for thread #0
            if(doDENSITYMASSTRACERS)
            {
                threadedDENSITYMASSTRACERS[t] = DENSITYMASSTRACERS;
            }
            if(doSECONDARYSTREAMDENSITY)
            {
                threadedSECONDARYSTREAMDENSITY[t] = SECONDARYSTREAMDENSITY;
            }
            if(needDENSITYTETRAHEDRA)
            {
                threadedDENSITYTETRAHEDRA[t] = DENSITYTETRAHEDRA;
            }
            if(doVELOCITY)
            {
                threadedVELOCITY_X[t] = VELOCITY_X;
                threadedVELOCITY_Y[t] = VELOCITY_Y;
                threadedVELOCITY_Z[t] = VELOCITY_Z;
            }
            if(doSTRUCTURES)
            {
                threadedVOIDS[t] = VOIDS;
                threadedSHEETS[t] = SHEETS;
                threadedFILAMENTS[t] = FILAMENTS;
                threadedCLUSTERS[t] = CLUSTERS;
            }
            if(doPRIMORDIALSTREAMDENSITY)
            {
                threadedPRIMORDIALSTREAMDENSITY[t] = PRIMORDIALSTREAMDENSITY;
            }
        }

        // loop on particles
        #pragma omp for schedule(static)
        for(particleID_t mpF=0; mpF<NpF; mpF++)
        {
            particleID_t Id = P_FLOW.ids[mpF].Id;

            // loop on the 6 tetrahedra for this particle
            for(int itet=0; itet<6; itet++)
            {
                particleID_t mpA, mpB, mpC, mpD;

                // get the indices of the 4 vertices
                particleID_t TetrahedronIndices[4];
                get_tetrahedron_indices(Id, itet, Np0, Np1, Np2, TetrahedronIndices);
                mpA = TetrahedronIndices[0];
                mpB = TetrahedronIndices[1];
                mpC = TetrahedronIndices[2];
                mpD = TetrahedronIndices[3];

                // get the coordinates
                float_t TetrahedronCoords[12];
                get_tetrahedron_coords(P_FLOW, mpA, mpB, mpC, mpD, TetrahedronCoords);

                // 1- get the positions of the mass tracers and assign them to the mesh
                if(doDENSITYMASSTRACERS)
                {
                    for(int iside=0; iside<4; iside++)
                    {
                        int mc0min,mc0max,mc1min,mc1max,mc2min,mc2max;
                        float_t MassTracerCoords[3];

                        // get mass tracers coordinates
                        get_masstracer_coords(TetrahedronCoords, iside, MassTracerCoords);

                        // check periodic boundary conditions here
                        float_t x0 = p_mod(MassTracerCoords[0],L0);
                        float_t x1 = p_mod(MassTracerCoords[1],L1);
                        float_t x2 = p_mod(MassTracerCoords[2],L2);

                        // assign particle to the mesh
                        particle_to_mesh_periodic(x0,x1,x2,1.,threadedDENSITYMASSTRACERS[t],N0,N1,N2,d0,d1,d2,&mc0min,&mc0max,&mc1min,&mc1max,&mc2min,&mc2max);

                        // update counters for this thread
                        mc0UsedMinDensity[t] = min(mc0UsedMinDensity[t], mc0min);
                        mc0UsedMaxDensity[t] = max(mc0UsedMaxDensity[t], mc0max);
                        mc1UsedMinDensity[t] = min(mc1UsedMinDensity[t], mc1min);
                        mc1UsedMaxDensity[t] = max(mc1UsedMaxDensity[t], mc1max);
                        mc2UsedMinDensity[t] = min(mc2UsedMinDensity[t], mc2min);
                        mc2UsedMaxDensity[t] = max(mc2UsedMaxDensity[t], mc2max);
                    }
                }

                // 2- get the volume for the following
                float_t Vtet, rhotet;
                if(doPRIMORDIALSTREAMDENSITY || doSECONDARYSTREAMDENSITY || needDENSITYTETRAHEDRA || doVELOCITY || doSTRUCTURES)
                {
                    Vtet = get_tetrahedron_volume(TetrahedronCoords);
                    rhotet = m/(6*fabs(Vtet)); //equation (6) in Abel, Hahn & Kaehler 2012
                }

                // 3- update the primordial stream density at the 4 vertices
                // overall, PRIMORDIALSTREAMDENSITY is updated exactly 24 times at each particle position
                if(doPRIMORDIALSTREAMDENSITY)
                {
                    threadedPRIMORDIALSTREAMDENSITY[t][mpA] += fabs(Vtet);
                    threadedPRIMORDIALSTREAMDENSITY[t][mpB] += fabs(Vtet);
                    threadedPRIMORDIALSTREAMDENSITY[t][mpC] += fabs(Vtet);
                    threadedPRIMORDIALSTREAMDENSITY[t][mpD] += fabs(Vtet);
                }

                // 4- project the tetrahedron to the Eulerian grid
                // weights for the number of secondary streams
                float_t fSS_A,fSS_B,fSS_C,fSS_D;
                if(doSECONDARYSTREAMDENSITY)
                    fSS_A=fSS_B=fSS_C=fSS_D = 1.;
                // weights for the final Eulerian density
                float_t fDT_A,fDT_B,fDT_C,fDT_D;
                if(needDENSITYTETRAHEDRA)
                    fDT_A=fDT_B=fDT_C=fDT_D = rhotet;
                // weights for the final Eulerian velocity field
                float_t fVX_A,fVX_B,fVX_C,fVX_D;
                float_t fVY_A,fVY_B,fVY_C,fVY_D;
                float_t fVZ_A,fVZ_B,fVZ_C,fVZ_D;
                if(doVELOCITY)
                {
                    fVX_A=P_FLOW.velocities[mpA].Vel[0]*rhotet, fVX_B=P_FLOW.velocities[mpB].Vel[0]*rhotet, fVX_C=P_FLOW.velocities[mpC].Vel[0]*rhotet, fVX_D=P_FLOW.velocities[mpD].Vel[0]*rhotet;
                    fVY_A=P_FLOW.velocities[mpA].Vel[1]*rhotet, fVY_B=P_FLOW.velocities[mpB].Vel[1]*rhotet, fVY_C=P_FLOW.velocities[mpC].Vel[1]*rhotet, fVY_D=P_FLOW.velocities[mpD].Vel[1]*rhotet;
                    fVZ_A=P_FLOW.velocities[mpA].Vel[2]*rhotet, fVZ_B=P_FLOW.velocities[mpB].Vel[2]*rhotet, fVZ_C=P_FLOW.velocities[mpC].Vel[2]*rhotet, fVZ_D=P_FLOW.velocities[mpD].Vel[2]*rhotet;
                }
                // weights for structure types
                float_t fV_A,fV_B,fV_C,fV_D;
                float_t fS_A,fS_B,fS_C,fS_D;
                float_t fF_A,fF_B,fF_C,fF_D;
                float_t fC_A,fC_B,fC_C,fC_D;
                if(doSTRUCTURES)
                {
                    fV_A=(OBJECTS[mpA]==0?rhotet:0.), fV_B=(OBJECTS[mpB]==0?rhotet:0.), fV_C=(OBJECTS[mpC]==0?rhotet:0.), fV_D=(OBJECTS[mpD]==0?rhotet:0.);
                    fS_A=(OBJECTS[mpA]==1?rhotet:0.), fS_B=(OBJECTS[mpB]==1?rhotet:0.), fS_C=(OBJECTS[mpC]==1?rhotet:0.), fS_D=(OBJECTS[mpD]==1?rhotet:0.);
                    fF_A=(OBJECTS[mpA]==2?rhotet:0.), fF_B=(OBJECTS[mpB]==2?rhotet:0.), fF_C=(OBJECTS[mpC]==2?rhotet:0.), fF_D=(OBJECTS[mpD]==2?rhotet:0.);
                    fC_A=(OBJECTS[mpA]==3?rhotet:0.), fC_B=(OBJECTS[mpB]==3?rhotet:0.), fC_C=(OBJECTS[mpC]==3?rhotet:0.), fC_D=(OBJECTS[mpD]==3?rhotet:0.);
                }

                if(doSECONDARYSTREAMDENSITY || needDENSITYTETRAHEDRA || doVELOCITY || doSTRUCTURES)
                    project_tetrahedron(TetrahedronCoords, N0, N1, N2, L0, L1, L2,
                        doSECONDARYSTREAMDENSITY,
                        threadedSECONDARYSTREAMDENSITY[t],fSS_A,fSS_B,fSS_C,fSS_D,
                        needDENSITYTETRAHEDRA,
                        threadedDENSITYTETRAHEDRA[t],fDT_A,fDT_B,fDT_C,fDT_D,
                        doVELOCITY,
                        threadedVELOCITY_X[t],fVX_A,fVX_B,fVX_C,fVX_D,
                        doVELOCITY,
                        threadedVELOCITY_Y[t],fVY_A,fVY_B,fVY_C,fVY_D,
                        doVELOCITY,
                        threadedVELOCITY_Z[t],fVZ_A,fVZ_B,fVZ_C,fVZ_D,
                        doSTRUCTURES,
                        threadedVOIDS[t],fV_A,fV_B,fV_C,fV_D,
                        doSTRUCTURES,
                        threadedSHEETS[t],fS_A,fS_B,fS_C,fS_D,
                        doSTRUCTURES,
                        threadedFILAMENTS[t],fF_A,fF_B,fF_C,fF_D,
                        doSTRUCTURES,
                        threadedCLUSTERS[t],fC_A,fC_B,fC_C,fC_D,
                        &mc0UsedMinTet[t],&mc0UsedMaxTet[t],&mc1UsedMinTet[t],&mc1UsedMaxTet[t],&mc2UsedMinTet[t],&mc2UsedMaxTet[t]);
            } //end loop on tetrahedra
        } //end loop on particles
    } //end omp parallel

    // perform the reduction and free memory for auxiliary arrays
    for(int t=1; t<Nthreads; t++) //start the loop at thread #1 here, as DENSITY already contains the result of thread #0
    {
        if (!threadActivated[t])
            continue;

        debugPrintInt(t);
        debugPrintInt(mc0UsedMinDensity[t]);
        debugPrintInt(mc0UsedMaxDensity[t]);
        debugPrintInt(mc0UsedMinTet[t]);
        debugPrintInt(mc0UsedMaxTet[t]);
        debugPrintInt(mc1UsedMinDensity[t]);
        debugPrintInt(mc1UsedMaxDensity[t]);
        debugPrintInt(mc1UsedMinTet[t]);
        debugPrintInt(mc1UsedMaxTet[t]);
        debugPrintInt(mc2UsedMinDensity[t]);
        debugPrintInt(mc2UsedMaxDensity[t]);
        debugPrintInt(mc2UsedMinTet[t]);
        debugPrintInt(mc2UsedMaxTet[t]);

        char name[50];
        if(doDENSITYMASSTRACERS)
        {
            #pragma omp parallel for schedule(dynamic,10000) collapse(3)
            for(int mc0=mc0UsedMinDensity[t]; mc0<=mc0UsedMaxDensity[t]; mc0++)
                for(int mc1=mc1UsedMinDensity[t]; mc1<=mc1UsedMaxDensity[t]; mc1++)
                    for(int mc2=mc2UsedMinDensity[t]; mc2<=mc2UsedMaxDensity[t]; mc2++)
                    {
                        cellIndex_t mc=get_index(mc0,mc1,mc2,N0,N1,N2);
                        DENSITYMASSTRACERS[mc] += threadedDENSITYMASSTRACERS[t][mc];
                    }

            sprintf(name, "threadedDENSITYMASSTRACERS[%d]", t); p_free(threadedDENSITYMASSTRACERS[t], name);
        }
        if(doSECONDARYSTREAMDENSITY)
        {
            #pragma omp parallel for schedule(dynamic,10000) collapse(3)
            for(int mc0=mc0UsedMinTet[t]; mc0<=mc0UsedMaxTet[t]; mc0++)
                for(int mc1=mc1UsedMinTet[t]; mc1<=mc1UsedMaxTet[t]; mc1++)
                    for(int mc2=mc2UsedMinTet[t]; mc2<=mc2UsedMaxTet[t]; mc2++)
                    {
                        cellIndex_t mc=get_index(mc0,mc1,mc2,N0,N1,N2);
                        SECONDARYSTREAMDENSITY[mc] += threadedSECONDARYSTREAMDENSITY[t][mc];
                    }

            sprintf(name, "threadedSECONDARYSTREAMDENSITY[%d]", t); p_free(threadedSECONDARYSTREAMDENSITY[t], name);
        }
        if(needDENSITYTETRAHEDRA)
        {
            #pragma omp parallel for schedule(dynamic,10000) collapse(3)
            for(int mc0=mc0UsedMinTet[t]; mc0<=mc0UsedMaxTet[t]; mc0++)
                for(int mc1=mc1UsedMinTet[t]; mc1<=mc1UsedMaxTet[t]; mc1++)
                    for(int mc2=mc2UsedMinTet[t]; mc2<=mc2UsedMaxTet[t]; mc2++)
                    {
                        cellIndex_t mc=get_index(mc0,mc1,mc2,N0,N1,N2);
                        DENSITYTETRAHEDRA[mc] += threadedDENSITYTETRAHEDRA[t][mc];
                    }

            sprintf(name, "threadedDENSITYTETRAHEDRA[%d]", t); p_free(threadedDENSITYTETRAHEDRA[t], name);
        }
        if(doVELOCITY)
        {
            #pragma omp parallel for schedule(dynamic,10000) collapse(3)
            for(int mc0=mc0UsedMinTet[t]; mc0<=mc0UsedMaxTet[t]; mc0++)
                for(int mc1=mc1UsedMinTet[t]; mc1<=mc1UsedMaxTet[t]; mc1++)
                    for(int mc2=mc2UsedMinTet[t]; mc2<=mc2UsedMaxTet[t]; mc2++)
                    {
                        cellIndex_t mc=get_index(mc0,mc1,mc2,N0,N1,N2);
                        VELOCITY_X[mc] += threadedVELOCITY_X[t][mc];
                        VELOCITY_Y[mc] += threadedVELOCITY_Y[t][mc];
                        VELOCITY_Z[mc] += threadedVELOCITY_Z[t][mc];
                    }

            sprintf(name, "threadedVELOCITY_X[%d]", t); p_free(threadedVELOCITY_X[t], name);
            sprintf(name, "threadedVELOCITY_Y[%d]", t); p_free(threadedVELOCITY_Y[t], name);
            sprintf(name, "threadedVELOCITY_Z[%d]", t); p_free(threadedVELOCITY_Z[t], name);
        }
        if(doSTRUCTURES)
        {
            #pragma omp parallel for schedule(dynamic,10000) collapse(3)
            for(int mc0=mc0UsedMinTet[t]; mc0<=mc0UsedMaxTet[t]; mc0++)
                for(int mc1=mc1UsedMinTet[t]; mc1<=mc1UsedMaxTet[t]; mc1++)
                    for(int mc2=mc2UsedMinTet[t]; mc2<=mc2UsedMaxTet[t]; mc2++)
                    {
                        cellIndex_t mc=get_index(mc0,mc1,mc2,N0,N1,N2);
                        VOIDS[mc] += threadedVOIDS[t][mc];
                        SHEETS[mc] += threadedSHEETS[t][mc];
                        FILAMENTS[mc] += threadedFILAMENTS[t][mc];
                        CLUSTERS[mc] += threadedCLUSTERS[t][mc];
                    }

            sprintf(name, "threadedVOIDS[%d]", t); p_free(threadedVOIDS[t], name);
            sprintf(name, "threadedSHEETS[%d]", t); p_free(threadedSHEETS[t], name);
            sprintf(name, "threadedFILAMENTS[%d]", t); p_free(threadedFILAMENTS[t], name);
            sprintf(name, "threadedCLUSTERS[%d]", t); p_free(threadedCLUSTERS[t], name);
        }
        if(doPRIMORDIALSTREAMDENSITY)
        {
            #pragma omp parallel for schedule(dynamic,10000)
            for(particleID_t mpF=0; mpF<NpF; mpF++)
            {
                PRIMORDIALSTREAMDENSITY[mpF] += threadedPRIMORDIALSTREAMDENSITY[t][mpF];
            }

            sprintf(name, "threadedPRIMORDIALSTREAMDENSITY[%d]", t); p_free(threadedPRIMORDIALSTREAMDENSITY[t], name);
        }
    }

    // divide velocity and structure fields by DENSITYTETRAHEDRA
    if(doVELOCITY)
    {
        #pragma omp parallel for schedule(static)
        for(cellIndex_t mc=0; mc<N; mc++)
        {
            if(DENSITYTETRAHEDRA[mc]>0)
            {
                VELOCITY_X[mc]/=DENSITYTETRAHEDRA[mc];
                VELOCITY_Y[mc]/=DENSITYTETRAHEDRA[mc];
                VELOCITY_Z[mc]/=DENSITYTETRAHEDRA[mc];
            }
        }
    }
    if(doSTRUCTURES)
    {
        #pragma omp parallel for schedule(static)
        for(cellIndex_t mc=0; mc<N; mc++)
        {
            if(DENSITYTETRAHEDRA[mc]>0)
            {
                VOIDS[mc]/=DENSITYTETRAHEDRA[mc];
                SHEETS[mc]/=DENSITYTETRAHEDRA[mc];
                FILAMENTS[mc]/=DENSITYTETRAHEDRA[mc];
                CLUSTERS[mc]/=DENSITYTETRAHEDRA[mc];
            }
        }
    }

    // get primordial stream density
    if(doPRIMORDIALSTREAMDENSITY)
    {
        #pragma omp parallel for schedule(static)
        for(particleID_t mpF=0; mpF<NpF; mpF++)
            PRIMORDIALSTREAMDENSITY[mpF] = 4*d*d*d / PRIMORDIALSTREAMDENSITY[mpF]; // (section 2.3 in Abel, Hahn & Kaehler 2012)
            // for the primordial local density, use 4*m instead of 4*d*d*d
    }
} //lagrangian_transport
///-------------------------------------------------------------------------------------
/** @fn get_density_masstracers
 * Computes the density contrast from mass tracer particles
 * @param P_FLOW input snapshot containing the mass tracers
 * @param Np0 number of particles x
 * @param Np1 number of particles y
 * @param Np2 number of particles z
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param DENSITYMASSTRACERS output density contrast from the mass tracers on the fly (Eulerian grid)
 */
void get_density_masstracers(const Snapshot P_FLOW, const int Np0, const int Np1, const int Np2, const int N0, const int N1, const int N2, float_t *DENSITYMASSTRACERS)
{
    int Nthreads = omp_get_max_threads();
    sprintf(G__msg__, "Getting density of mass tracers (using %d cores)...", Nthreads);
    PrintMessage(4, G__msg__);

    const bool doDENSITYMASSTRACERS = true;
    const bool doPRIMORDIALSTREAMDENSITY = false;
    const bool doSECONDARYSTREAMDENSITY = false;
    const bool doDENSITYTETRAHEDRA = false;
    const bool doVELOCITY = false;
    const bool doSTRUCTURES = false;
    float_t DUMMY[1];

    lagrangian_transport(P_FLOW, DUMMY, Np0, Np1, Np2, N0, N1, N2, doDENSITYMASSTRACERS, DENSITYMASSTRACERS, doPRIMORDIALSTREAMDENSITY, DUMMY, doSECONDARYSTREAMDENSITY, DUMMY, doDENSITYTETRAHEDRA, DUMMY, doVELOCITY, DUMMY, DUMMY, DUMMY, doSTRUCTURES, DUMMY, DUMMY, DUMMY, DUMMY);

    sprintf(G__msg__, "Getting density of mass tracers (using %d cores) done.", Nthreads);
    PrintMessage(4, G__msg__);
} //get_density_masstracers
///-------------------------------------------------------------------------------------
/** @fn get_density_tetrahedra
 * Computes the density contrast from tetrahedra
 * @param P_FLOW input snapshot containing the mass tracers
 * @param Np0 number of particles x
 * @param Np1 number of particles y
 * @param Np2 number of particles z
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param DENSITYTETRAHEDRA output density from projecting all tetrahedra (Eulerian grid)
 */
void get_density_tetrahedra(const Snapshot P_FLOW, const int Np0, const int Np1, const int Np2, const int N0, const int N1, const int N2, float_t *DENSITYTETRAHEDRA)
{
    int Nthreads = omp_get_max_threads();
    sprintf(G__msg__, "Getting density using tetrahedra (using %d cores)...", Nthreads);
    PrintMessage(4, G__msg__);

    const bool doDENSITYMASSTRACERS = false;
    const bool doPRIMORDIALSTREAMDENSITY = false;
    const bool doSECONDARYSTREAMDENSITY = false;
    const bool doDENSITYTETRAHEDRA = true;
    const bool doVELOCITY = false;
    const bool doSTRUCTURES = false;
    float_t DUMMY[1];

    lagrangian_transport(P_FLOW, DUMMY, Np0, Np1, Np2, N0, N1, N2, doDENSITYMASSTRACERS, DUMMY, doPRIMORDIALSTREAMDENSITY, DUMMY, doSECONDARYSTREAMDENSITY, DUMMY, doDENSITYTETRAHEDRA, DENSITYTETRAHEDRA, doVELOCITY, DUMMY, DUMMY, DUMMY, doSTRUCTURES, DUMMY, DUMMY, DUMMY, DUMMY);

    sprintf(G__msg__, "Getting density using tetrahedra (using %d cores) done.", Nthreads);
    PrintMessage(4, G__msg__);
} //get_density_tetrahedra
///-------------------------------------------------------------------------------------
/** @fn get_stream_density
 * Gets stream density (primordial on the Lagrangian mesh,
 * secondary on the Eulerian mesh)
 * @param P_FLOW input snapshot containing the mass tracers
 * @param Np0 number of particles x
 * @param Np1 number of particles y
 * @param Np2 number of particles z
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param PRIMORDIALSTREAMDENSITY output primordial stream density (Lagrangian grid)
 * @param SECONDARYSTREAMDENSITY output secondary stream density (Eulerian grid)
 */
void get_stream_density(const Snapshot P_FLOW, const int Np0, const int Np1, const int Np2, const int N0, const int N1, const int N2, float_t *PRIMORDIALSTREAMDENSITY, float_t *SECONDARYSTREAMDENSITY)
{
    int Nthreads = omp_get_max_threads();
    sprintf(G__msg__, "Getting stream density (using %d cores)...", Nthreads);
    PrintMessage(4, G__msg__);

    const bool doDENSITYMASSTRACERS = false;
    const bool doPRIMORDIALSTREAMDENSITY = true;
    const bool doSECONDARYSTREAMDENSITY = true;
    const bool doDENSITYTETRAHEDRA = false;
    const bool doVELOCITY = false;
    const bool doSTRUCTURES = false;
    float_t DUMMY[1];

    lagrangian_transport(P_FLOW, DUMMY, Np0, Np1, Np2, N0, N1, N2, doDENSITYMASSTRACERS, DUMMY, doPRIMORDIALSTREAMDENSITY, PRIMORDIALSTREAMDENSITY, doSECONDARYSTREAMDENSITY, SECONDARYSTREAMDENSITY, doDENSITYTETRAHEDRA, DUMMY, doVELOCITY, DUMMY, DUMMY, DUMMY, doSTRUCTURES, DUMMY, DUMMY, DUMMY, DUMMY);

    sprintf(G__msg__, "Getting stream density (using %d cores) done.", Nthreads);
    PrintMessage(4, G__msg__);
} //get_stream_density
///-------------------------------------------------------------------------------------
/** @fn lagrangian_transport_velocities
 * Lagrangian transport of velocities
 * @param P_FLOW input snapshot containing the mass tracers
 * @param Np0 number of particles x
 * @param Np1 number of particles y
 * @param Np2 number of particles z
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param VELOCITY_X output velocity field x (Eulerian grid)
 * @param VELOCITY_Y output velocity field y (Eulerian grid)
 * @param VELOCITY_Z output velocity field z (Eulerian grid)
 */
void lagrangian_transport_velocities(const Snapshot P_FLOW, const int Np0, const int Np1, const int Np2, const int N0, const int N1, const int N2, float_t *VELOCITY_X, float_t *VELOCITY_Y, float_t *VELOCITY_Z)
{
    int Nthreads = omp_get_max_threads();
    sprintf(G__msg__, "Computing Lagrangian transport of velocities (using %d cores)...", Nthreads);
    PrintMessage(4, G__msg__);

    const bool doDENSITYMASSTRACERS = false;
    const bool doPRIMORDIALSTREAMDENSITY = false;
    const bool doSECONDARYSTREAMDENSITY = false;
    const bool doDENSITYTETRAHEDRA = false;
    const bool doVELOCITY = true;
    const bool doSTRUCTURES = false;
    float_t DUMMY[1];
    float_t *DENSITYTETRAHEDRA;
    DENSITYTETRAHEDRA = (float_t *)p_malloc(N0*N1*N2*sizeof(float_t), VARNAME(DENSITYTETRAHEDRA));

    lagrangian_transport(P_FLOW, DUMMY, Np0, Np1, Np2, N0, N1, N2, doDENSITYMASSTRACERS, DUMMY, doPRIMORDIALSTREAMDENSITY, DUMMY, doSECONDARYSTREAMDENSITY, DUMMY, doDENSITYTETRAHEDRA, DENSITYTETRAHEDRA, doVELOCITY, VELOCITY_X, VELOCITY_Y, VELOCITY_Z, doSTRUCTURES, DUMMY, DUMMY, DUMMY, DUMMY);

    p_free(DENSITYTETRAHEDRA, VARNAME(DENSITYTETRAHEDRA));
    sprintf(G__msg__, "Computing Lagrangian transport of velocities (using %d cores) done.", Nthreads);
    PrintMessage(4, G__msg__);
} //lagrangian_transport_velocities
///-------------------------------------------------------------------------------------
/** @fn lagrangian_transport_structures
 * Lagrangian transport of structure types
 * @param P_FLOW input snapshot containing the mass tracers
 * @param Np0 number of particles x
 * @param Np1 number of particles y
 * @param Np2 number of particles z
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param VOIDS output probability voids (Eulerian grid)
 * @param SHEETS output probability sheets (Eulerian grid)
 * @param FILAMENTS output probability filaments (Eulerian grid)
 * @param CLUSTERS output probability clusters (Eulerian grid)
 */
void lagrangian_transport_structures(const Snapshot P_FLOW, const float_t *OBJECTS, const int Np0, const int Np1, const int Np2, const int N0, const int N1, const int N2, float_t *VOIDS, float_t *SHEETS, float_t *FILAMENTS, float_t *CLUSTERS)
{
    int Nthreads = omp_get_max_threads();
    sprintf(G__msg__, "Computing Lagrangian transport of structures (using %d cores)...", Nthreads);
    PrintMessage(4, G__msg__);

    const bool doDENSITYMASSTRACERS = false;
    const bool doPRIMORDIALSTREAMDENSITY = false;
    const bool doSECONDARYSTREAMDENSITY = false;
    const bool doDENSITYTETRAHEDRA = false;
    const bool doVELOCITY = false;
    const bool doSTRUCTURES = true;
    float_t DUMMY[1];
    float_t *DENSITYTETRAHEDRA;
    DENSITYTETRAHEDRA = (float_t *)p_malloc(N0*N1*N2*sizeof(float_t), VARNAME(DENSITYTETRAHEDRA));

    lagrangian_transport(P_FLOW, OBJECTS, Np0, Np1, Np2, N0, N1, N2, doDENSITYMASSTRACERS, DUMMY, doPRIMORDIALSTREAMDENSITY, DUMMY, doSECONDARYSTREAMDENSITY, DUMMY, doDENSITYTETRAHEDRA, DENSITYTETRAHEDRA, doVELOCITY, DUMMY, DUMMY, DUMMY, doSTRUCTURES, VOIDS, SHEETS, FILAMENTS, CLUSTERS);

    p_free(DENSITYTETRAHEDRA, VARNAME(DENSITYTETRAHEDRA));
    sprintf(G__msg__, "Computing Lagrangian transport of structures (using %d cores) done.", Nthreads);
    PrintMessage(4, G__msg__);
} //lagrangian_transport_structures
///-------------------------------------------------------------------------------------
/** @fn lagrangian_transport_streams_density_velocity
 * Lagrangian transport of streams, density and velocities
 * @param P_FLOW input snapshot containing the mass tracers
 * @param Np0 number of particles x
 * @param Np1 number of particles y
 * @param Np2 number of particles z
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param DENSITYMASSTRACERS output density contrast from the mass tracers on the fly (Eulerian grid)
 * @param PRIMORDIALSTREAMDENSITY output primordial stream density (Lagrangian grid)
 * @param SECONDARYSTREAMDENSITY output secondary stream density (Eulerian grid)
 * @param DENSITYTETRAHEDRA output density from projecting all tetrahedra (Eulerian grid)
 * @param VELOCITY_X output velocity field x (Eulerian grid)
 * @param VELOCITY_Y output velocity field y (Eulerian grid)
 * @param VELOCITY_Z output velocity field z (Eulerian grid)
 */
void lagrangian_transport_streams_density_velocity(const Snapshot P_FLOW, const int Np0, const int Np1, const int Np2, const int N0, const int N1, const int N2, float_t *DENSITYMASSTRACERS, float_t *PRIMORDIALSTREAMDENSITY, float_t *SECONDARYSTREAMDENSITY, float_t *DENSITYTETRAHEDRA, float_t *VELOCITY_X, float_t *VELOCITY_Y, float_t *VELOCITY_Z)
{
    int Nthreads = omp_get_max_threads();
    sprintf(G__msg__, "Computing Lagrangian transport (using %d cores)...", Nthreads);
    PrintMessage(4, G__msg__);

    const bool doDENSITYMASSTRACERS = true;
    const bool doPRIMORDIALSTREAMDENSITY = true;
    const bool doSECONDARYSTREAMDENSITY = true;
    const bool doDENSITYTETRAHEDRA = true;
    const bool doVELOCITY = true;
    const bool doSTRUCTURES = false;
    float_t DUMMY[1];

    lagrangian_transport(P_FLOW, DUMMY, Np0, Np1, Np2, N0, N1, N2, doDENSITYMASSTRACERS, DENSITYMASSTRACERS, doPRIMORDIALSTREAMDENSITY, PRIMORDIALSTREAMDENSITY, doSECONDARYSTREAMDENSITY, SECONDARYSTREAMDENSITY, doDENSITYTETRAHEDRA, DENSITYTETRAHEDRA, doVELOCITY, VELOCITY_X, VELOCITY_Y, VELOCITY_Z, doSTRUCTURES, DUMMY, DUMMY, DUMMY, DUMMY);

    sprintf(G__msg__, "Computing Lagrangian transport (using %d cores) done.", Nthreads);
    PrintMessage(4, G__msg__);
} //lagrangian_transport_streams_density_velocity
///-------------------------------------------------------------------------------------
