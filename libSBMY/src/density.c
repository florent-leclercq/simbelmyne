///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/src/density.c
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file density.c
 *  \brief Routines for dealing with density fields
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2013-2023
 */
#include "density.h"
///-------------------------------------------------------------------------------------
/** @fn get_density_pm_serial
 * Calculation of density contrast, serial version
 */
void get_density_pm_serial(const particleID_t Np, const particle_pos_t positions[Np], const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, float_t *DELTA)
{
    sprintf(G__msg__, "Getting density contrast (using 1 core, serial routine)...");
    PrintMessage(4, G__msg__);
    INDENT;

    const cellIndex_t N=N0*N1*N2;
    const double NMEAN=(double)Np/(double)N;

    // initialize DELTA
    #pragma omp parallel for schedule(static)
    for(cellIndex_t mc=0; mc<N; mc++)
        DELTA[mc]=0.;

    for(particleID_t mp=0; mp<Np; mp++)
    {
        int mc0min,mc0max,mc1min,mc1max,mc2min,mc2max;
        float_t this_x0,this_x1,this_x2;

        // get particle position
        this_x0=positions[mp].Pos[0];
        this_x1=positions[mp].Pos[1];
        this_x2=positions[mp].Pos[2];

        // assign particle to the mesh
        particle_to_mesh_periodic(this_x0,this_x1,this_x2,1.,DELTA,N0,N1,N2,d0,d1,d2,&mc0min,&mc0max,&mc1min,&mc1max,&mc2min,&mc2max);
    }

    // go from density to density contrast
    density_to_delta(DELTA, (const double)NMEAN, N0, N1, N2);

    UNINDENT;
    sprintf(G__msg__, "Getting density contrast (using 1 core, serial routine) done.");
    PrintMessage(4, G__msg__);
} //get_density_pm_serial
///-------------------------------------------------------------------------------------
/** @fn get_density_pm_parallel_1
 * Calculation of density contrast, parallel version 1
 */
void get_density_pm_parallel_1(const particleID_t Np, const particle_pos_t positions[Np], const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, float_t *DELTA)
{
    const int Nthreads_max = omp_get_max_threads();
    const cellIndex_t N=N0*N1*N2;
    const double NMEAN=(double)Np/(double)N;

    // declare and initialize auxiliary arrays
    float_t *threadedDensity[Nthreads_max];
    bool threadActivated[Nthreads_max];
    int mc0UsedMin[Nthreads_max], mc0UsedMax[Nthreads_max];
    int mc1UsedMin[Nthreads_max], mc1UsedMax[Nthreads_max];
    int mc2UsedMin[Nthreads_max], mc2UsedMax[Nthreads_max];
    for(int t=0; t<Nthreads_max; t++)
    {
        threadActivated[t]=false;
        threadedDensity[t]=0; // initialize pointers to zero to check if memory allocation went fine
        mc0UsedMin[t]=N0-1; mc1UsedMin[t]=N1-1; mc2UsedMin[t]=N2-1;
        mc0UsedMax[t]=0; mc1UsedMax[t]=0; mc2UsedMax[t]=0;
    }

    // get number of possible threads from available memory
    int Nthreads = get_Nthreads_from_available_memory((const long long)N, sizeof(float_t), Nthreads_max);

    sprintf(G__msg__, "Getting density contrast (using %d cores and %d arrays, parallel routine 1)...", Nthreads_max, Nthreads);
    PrintMessage(4, G__msg__);
    INDENT;

    // initialize DELTA
    #pragma omp parallel for schedule(static)
    for(cellIndex_t mc=0; mc<N; mc++)
        DELTA[mc]=0.;

    // do binning
    #if DEBUG
            debugPrintDiagnostic("Performing CiC binning...");
    #endif

    #pragma omp parallel num_threads(Nthreads) shared(DELTA,threadedDensity,threadActivated,mc0UsedMin,mc0UsedMax,mc1UsedMin,mc1UsedMax,mc2UsedMin,mc2UsedMax,positions)
    {
        int t = omp_get_thread_num(); // get rank of current process
        threadActivated[t] = true;

        if(t!=0)
        {
            // allocate threadedDensity and initialize to zero
            char name[20]; sprintf(name, "threadedDensity[%d]", t);
            threadedDensity[t] = (float_t*)p_calloc(N, sizeof(float_t), name);
        }
        else if(t==0)
        {
            // use the already existing and initialized array DELTA for thread #0
            threadedDensity[t] = DELTA;
        }

        #if DEBUG
            #pragma omp critical
            {
                sprintf(G__msg__, "Performing CiC binning with thread %d...", t);
                PrintMemory(G__msg__);
            }
        #endif

        #pragma omp for schedule(static)
        for(particleID_t mp=0; mp<Np; mp++)
        {
            int mc0min,mc0max,mc1min,mc1max,mc2min,mc2max;
            double this_x0,this_x1,this_x2;

            // get particle position
            this_x0=positions[mp].Pos[0];
            this_x1=positions[mp].Pos[1];
            this_x2=positions[mp].Pos[2];

            // assign particle to the mesh
            particle_to_mesh_periodic(this_x0,this_x1,this_x2,1.,threadedDensity[t],N0,N1,N2,d0,d1,d2,&mc0min,&mc0max,&mc1min,&mc1max,&mc2min,&mc2max);

            // update counters for this thread
            mc0UsedMin[t] = min(mc0UsedMin[t], mc0min);
            mc0UsedMax[t] = max(mc0UsedMax[t], mc0max);
            mc1UsedMin[t] = min(mc1UsedMin[t], mc1min);
            mc1UsedMax[t] = max(mc1UsedMax[t], mc1max);
            mc2UsedMin[t] = min(mc2UsedMin[t], mc2min);
            mc2UsedMax[t] = max(mc2UsedMax[t], mc2max);
        }

        #if DEBUG
            #pragma omp critical
            {
                sprintf(G__msg__, "Performing CiC binning with thread %d done.", t);
                PrintMemory(G__msg__);
            }
        #endif
    }
    #pragma omp barrier
    #if DEBUG
        debugPrintDiagnostic("Performing CiC done.");
    #endif

    #if DEBUG
        debugPrintDiagnostic("Performing CiC reduction...");
    #endif

    // perform the reduction and free memory for auxiliary arrays
    for(int t=1; t<Nthreads_max; t++) //start the loop at thread #1 here, as DELTA already contains the result of thread #0
    {
        if(!threadActivated[t])
            continue;

        if(threadedDensity[t] == 0)
        {
            sprintf(G__msg__, "Thread %d activated but threadedDensity[%d] unaccessible%s.", t, t, FONT_LIGHTRED);
            FatalError(G__msg__);
        }

        #pragma omp parallel for schedule(dynamic,10000) collapse(3)
        for(int mc0=mc0UsedMin[t]; mc0<=mc0UsedMax[t]; mc0++)
            for(int mc1=mc1UsedMin[t]; mc1<=mc1UsedMax[t]; mc1++)
                for(int mc2=mc2UsedMin[t]; mc2<=mc2UsedMax[t]; mc2++)
                {
                    cellIndex_t mc=get_index(mc0,mc1,mc2,N0,N1,N2);
                    DELTA[mc] += threadedDensity[t][mc];
                }

        char name[50]; sprintf(name, "threadedDensity[%d]", t);
        p_free(threadedDensity[t], name);
    }

    #if DEBUG
        PrintDiagnostic(DEBUG_VERBOSITY, "Performing CiC reduction done.");
    #endif

    // go from density to density contrast
    density_to_delta(DELTA, (const double)NMEAN, N0, N1, N2);

    UNINDENT;
    sprintf(G__msg__, "Getting density contrast (using %d cores and %d arrays, parallel routine 1) done.", Nthreads_max, Nthreads);
    PrintMessage(4, G__msg__);
} //get_density_pm_parallel_1
///-------------------------------------------------------------------------------------
/** @fn get_density_pm_parallel_2
 * Calculation of density contrast, parallel version 2
 * Only for CiC assignment!
 */
void get_density_pm_parallel_2(const particleID_t Np, const particle_pos_t positions[Np], const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, float_t *DELTA)
{
    int Nthreads = omp_get_max_threads();
    sprintf(G__msg__, "Getting density contrast (using %d cores, parallel routine 2)...", Nthreads);
    PrintMessage(4, G__msg__);
    INDENT;

    const cellIndex_t N=N0*N1*N2;
    const double NMEAN=(double)Np/(double)N;

    particleID_t *part_mesh; part_mesh = (particleID_t *)p_malloc(N*sizeof(particleID_t), VARNAME(part_mesh));
    particleID_t *part_list; part_list = (particleID_t *)p_malloc(Np*sizeof(particleID_t), VARNAME(part_list));

    // initialize arrays
    #pragma omp parallel for schedule(static)
    for(cellIndex_t mc=0; mc<N; mc++)
    {
        DELTA[mc]=0.;
        part_mesh[mc]=Np;
    }

    #pragma omp parallel for schedule(static)
    for(particleID_t mp=0; mp<Np; mp++)
    {
        part_list[mp]=Np;
    }

    // first build a chained list of particles at each mesh point (NGP scheme)
    #pragma omp parallel for schedule(static)
    for(particleID_t mp=0; mp<Np; mp++)
    {
        // get particle position
        const double this_x0=positions[mp].Pos[0];
        const double this_x1=positions[mp].Pos[1];
        const double this_x2=positions[mp].Pos[2];

        int mc0,mc1,mc2;
        const cellIndex_t mc = aux_particle_to_mesh_ngp_periodic(this_x0,this_x1,this_x2,N0,N1,N2,d0,d1,d2,&mc0,&mc1,&mc2);

        // add this particle at the beginning of the existing chain
        particleID_t previous_mp = __atomic_exchange_n(&part_mesh[mc], mp, __ATOMIC_RELAXED);
        if(previous_mp != Np)
            part_list[mp] = previous_mp;
    }

    // we built the list in the incorrect order, reverse it as fast as we can
    #pragma omp parallel for schedule(dynamic,10000)
    for(cellIndex_t mc=0; mc<N; mc++)
    {
        particleID_t current_mp = part_mesh[mc];
        if(current_mp < Np)
        {
            particleID_t next_mp = part_list[current_mp];
            part_list[current_mp] = Np;

            while(next_mp != Np)
            {
                particleID_t svg_mp = part_list[next_mp];
                part_list[next_mp] = current_mp;
                current_mp = next_mp;
                next_mp = svg_mp;
            }
            part_mesh[mc] = current_mp;
        }
    }

    // project to the grid
    #pragma omp parallel
    {
        for(int looper0 = 0; looper0 < 2; looper0++)
            for(int looper1 = 0; looper1 < 2; looper1++)
                for(int looper2 = 0; looper2 < 2; looper2++)
                {
                    #pragma omp barrier
                    #pragma omp for schedule(dynamic,10000) collapse(3)
                    for(int i=0; i<N0; i++)
                        for(int j=0; j<N1; j++)
                            for(int k=0; k<N2; k++)
                            {
                                cellIndex_t mc=get_index(i,j,k,N0,N1,N2);

                                // look up the mesh point to be updated
                                int i_loop,j_loop,k_loop;
                                i_loop = (int)p_mod(i+looper0,N0);
                                j_loop = (int)p_mod(j+looper1,N1);
                                k_loop = (int)p_mod(k+looper2,N2);
                                cellIndex_t mcu = get_index(i_loop,j_loop,k_loop,N0,N1,N2);

                                // compute the weight to be used
                                particleID_t mp = part_mesh[mc];
                                double w = 0;
                                while(mp != Np)
                                {
                                    double this_w;
                                    double this_x0,this_x1,this_x2;
                                    int this_mpx0,this_mpx1,this_mpx2;
                                    double dx,dy,dz, tx,ty,tz;

                                    // get particle position
                                    this_x0=positions[mp].Pos[0];
                                    this_x1=positions[mp].Pos[1];
                                    this_x2=positions[mp].Pos[2];

                                    // get the nearest grid point
                                    this_mpx0 = (int)(floor(this_x0/d0));
                                    this_mpx1 = (int)(floor(this_x1/d1));
                                    this_mpx2 = (int)(floor(this_x2/d2));

                                    // CiC scheme
                                    dx=this_x0/d0-(double)this_mpx0; tx=1.-dx;
                                    dy=this_x1/d1-(double)this_mpx1; ty=1.-dy;
                                    dz=this_x2/d2-(double)this_mpx2; tz=1.-dz;
                                    this_w  = (looper0 == 1) ? dx : tx;
                                    this_w *= (looper1 == 1) ? dy : ty;
                                    this_w *= (looper2 == 1) ? dz : tz;
                                    w += this_w;

                                    // go to next particle in the chain
                                    mp = part_list[mp];
                                }

                                // update DELTA
                                DELTA[mcu] += w;
                            }
                }
    }
    #pragma omp barrier

    p_free(part_mesh, VARNAME(part_mesh));
    p_free(part_list, VARNAME(part_list));

    // go from density to density contrast
    density_to_delta(DELTA, (const double)NMEAN, N0, N1, N2);

    UNINDENT;
    sprintf(G__msg__, "Getting density contrast (using %d cores, parallel routine 2) done.", Nthreads);
    PrintMessage(4, G__msg__);
} //get_density_pm_parallel_2
///-------------------------------------------------------------------------------------
/** @fn get_density_pm_parallel
 * Calculation of density contrast in parallel (wrapper)
 * for CiC: version 2 is used if NMEAN>5, version 1 otherwise
 * for non-CiC: version 1 is used
 */
void get_density_pm_parallel(const particleID_t Np, const particle_pos_t positions[Np], const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, float_t *DELTA)
{
    #if ASSIGNMENT == CIC
        const cellIndex_t N=N0*N1*N2;
        const int Nthreads_max = omp_get_max_threads();
        int Nthreads = get_Nthreads_from_available_memory((const long long)N, sizeof(float_t), Nthreads_max);
        const double NMEAN=(double)Np/(double)N;
        if(NMEAN>8. || Nthreads<=2)
            get_density_pm_parallel_2(Np,positions,N0,N1,N2,d0,d1,d2,DELTA);
        else
            get_density_pm_parallel_1(Np,positions,N0,N1,N2,d0,d1,d2,DELTA);
    #else
        get_density_pm_parallel_1(Np,positions,N0,N1,N2,d0,d1,d2,DELTA);
    #endif
} //get_density_pm_parallel
///-------------------------------------------------------------------------------------
/** @fn get_density_pm
 * This routine takes particle positions
 * and calculates density contrast (a wrapper)
 * @param Np number of particles
 * @param positions particles' positions
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param d0 conversion factor to code units x
 * @param d1 conversion factor to code units y
 * @param d2 conversion factor to code units z
 * @param *DELTA output density field
 */
void get_density_pm(const particleID_t Np, const particle_pos_t positions[Np], const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, float_t *DELTA)
{
//     get_density_pm_serial(Np,positions,N0,N1,N2,d0,d1,d2,DELTA);
    get_density_pm_parallel(Np,positions,N0,N1,N2,d0,d1,d2,DELTA);
} //get_density_pm
///-------------------------------------------------------------------------------------
/** @fn get_density_pm_snapshot
 * This routine takes a Gadget snapshot
 * and calculates density contrast
 * @param P input Gadget snapshot
 * @param N0 mesh size for interpolation
 * @param *DELTA pointer to the array containing the density field
 */
void get_density_pm_snapshot(const Snapshot P, const int N0, float_t *DELTA)
{
    const double L0=P.header.BoxSize, L1=P.header.BoxSize, L2=P.header.BoxSize;
    const int N1=N0, N2=N0; const particleID_t Np=(particleID_t)(P.header.npart[0]+P.header.npart[1]+P.header.npart[2]+P.header.npart[3]+P.header.npart[4]+P.header.npart[5]);
    const double d0=L0/(double)N0;
    const double d1=L1/(double)N1;
    const double d2=L2/(double)N2;

    get_density_pm(Np,P.positions,N0,N1,N2,d0,d1,d2,DELTA);
} //get_density_pm_snapshot
///-------------------------------------------------------------------------------------
/** @fn get_density_pm_nonperiodic_serial
 * Calculation of density contrast, serial version
 */
void get_density_pm_nonperiodic_serial(const particleID_t Np, const particle_pos_t positions[Np], const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, float_t *DELTA)
{
    sprintf(G__msg__, "Getting density contrast non-periodic (using 1 core, serial routine)...");
    PrintMessage(4, G__msg__);
    INDENT;

    const cellIndex_t N=N0*N1*N2;
    const double NMEAN=(double)Np/(double)N;
    int N_partial=0; double average_mass_deposited=0.;

    // initialize DELTA
    #pragma omp parallel for schedule(static)
    for(cellIndex_t mc=0; mc<N; mc++)
        DELTA[mc]=0.;

    for(particleID_t mp=0; mp<Np; mp++)
    {
        int mc0min,mc0max,mc1min,mc1max,mc2min,mc2max;
        float_t this_x0,this_x1,this_x2;
        double mass_deposited;

        // get particle position
        this_x0=positions[mp].Pos[0];
        this_x1=positions[mp].Pos[1];
        this_x2=positions[mp].Pos[2];

        // assign particle to the mesh
        particle_to_mesh_nonperiodic(this_x0,this_x1,this_x2,1.,DELTA,N0,N1,N2,d0,d1,d2,&mc0min,&mc0max,&mc1min,&mc1max,&mc2min,&mc2max,&mass_deposited);

        // count the number of particles that do not deposit all of their mass
        if(mass_deposited<1.)
        {
            N_partial++;
            average_mass_deposited+=mass_deposited;
        }
    }
    average_mass_deposited/=N_partial;

    // go from density to density contrast
    // WARNING this will not conserve mass here
    density_to_delta(DELTA, (const double)NMEAN, N0, N1, N2);

    sprintf(G__msg__, "%d particles (%g%%) didn't deposit all of their mass (%g%% on average).", N_partial, (float)N_partial/N,100*average_mass_deposited);
    PrintMessage(5, G__msg__);

    UNINDENT;
    sprintf(G__msg__, "Getting density contrast non-periodic (using 1 core, serial routine) done.");
    PrintMessage(4, G__msg__);
} //get_density_pm_nonperiodic_serial
///-------------------------------------------------------------------------------------
/** @fn get_density_pm_nonperiodic_parallel
 * Calculation of density contrast, parallel version 1
 */
void get_density_pm_nonperiodic_parallel(const particleID_t Np, const particle_pos_t positions[Np], const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, float_t *DELTA)
{
    const int Nthreads_max = omp_get_max_threads();
    const cellIndex_t N=N0*N1*N2;
    const double NMEAN=(double)Np/(double)N;
    int N_partial=0; double average_mass_deposited=0.;

    // declare and initialize auxiliary arrays
    float_t *threadedDensity[Nthreads_max];
    bool threadActivated[Nthreads_max];
    int mc0UsedMin[Nthreads_max], mc0UsedMax[Nthreads_max];
    int mc1UsedMin[Nthreads_max], mc1UsedMax[Nthreads_max];
    int mc2UsedMin[Nthreads_max], mc2UsedMax[Nthreads_max];
    for(int t=0; t<Nthreads_max; t++)
    {
        threadActivated[t]=false;
        threadedDensity[t]=0; // initialize pointers to zero to check if memory allocation went fine
        mc0UsedMin[t]=N0-1; mc1UsedMin[t]=N1-1; mc2UsedMin[t]=N2-1;
        mc0UsedMax[t]=0; mc1UsedMax[t]=0; mc2UsedMax[t]=0;
    }

    // get number of possible threads from available memory
    int Nthreads = get_Nthreads_from_available_memory((const long long)N, sizeof(float_t), Nthreads_max);

    sprintf(G__msg__, "Getting density contrast non-periodic (using %d cores and %d arrays, parallel routine)...", Nthreads_max, Nthreads);
    PrintMessage(4, G__msg__);
    INDENT;

    // initialize DELTA
    #pragma omp parallel for schedule(static)
    for(cellIndex_t mc=0; mc<N; mc++)
        DELTA[mc]=0.;

    // do binning
    #pragma omp parallel num_threads(Nthreads) shared(DELTA,threadedDensity,threadActivated,mc0UsedMin,mc0UsedMax,mc1UsedMin,mc1UsedMax,mc2UsedMin,mc2UsedMax,positions) reduction(+:N_partial,average_mass_deposited)
    {
        int t = omp_get_thread_num(); // get rank of current process
        threadActivated[t] = true;

        if(t!=0)
        {
            // allocate threadedDensity and initialize to zero
            char name[20]; sprintf(name, "threadedDensity[%d]", t);
            threadedDensity[t] = (float_t*)p_calloc(N, sizeof(float_t), name);
        }
        else if(t==0)
        {
            // use the already existing and initialized array DELTA for thread #0
            threadedDensity[t] = DELTA;
        }

        #if DEBUG
            #pragma omp critical
            {
                sprintf(G__msg__, "Performing CiC binning with thread %d...", t);
                debugPrintDiagnostic(G__msg__);
            }
        #endif

        #pragma omp for schedule(static)
        for(particleID_t mp=0; mp<Np; mp++)
        {
            int mc0min,mc0max,mc1min,mc1max,mc2min,mc2max;
            float_t this_x0,this_x1,this_x2;
            double mass_deposited;

            // get particle position
            this_x0=positions[mp].Pos[0];
            this_x1=positions[mp].Pos[1];
            this_x2=positions[mp].Pos[2];

            // assign particle to the mesh
            particle_to_mesh_nonperiodic(this_x0,this_x1,this_x2,1.,threadedDensity[t],N0,N1,N2,d0,d1,d2,&mc0min,&mc0max,&mc1min,&mc1max,&mc2min,&mc2max,&mass_deposited);

            // count the number of particles that do not deposit all of their mass
            if(mass_deposited<1.)
            {
                N_partial++;
                average_mass_deposited+=mass_deposited;
            }

            // update counters for this thread
            mc0UsedMin[t] = min(mc0UsedMin[t], mc0min);
            mc0UsedMax[t] = max(mc0UsedMax[t], mc0max);
            mc1UsedMin[t] = min(mc1UsedMin[t], mc1min);
            mc1UsedMax[t] = max(mc1UsedMax[t], mc1max);
            mc2UsedMin[t] = min(mc2UsedMin[t], mc2min);
            mc2UsedMax[t] = max(mc2UsedMax[t], mc2max);
        }

        #if DEBUG
            #pragma omp critical
            {
                sprintf(G__msg__, "Performing CiC binning with thread %d done.", t);
                debugPrintDiagnostic(G__msg__);
            }
        #endif
    }
    #pragma omp barrier

    #if DEBUG
        debugPrintDiagnostic("Performing CiC reduction...");
    #endif

    // perform the reduction and free memory for auxiliary arrays
    for(int t=1; t<Nthreads_max; t++) //start the loop at thread #1 here, as DELTA already contains the result of thread #0
    {
        if(!threadActivated[t])
            continue;

        if(threadedDensity[t] == 0)
        {
            sprintf(G__msg__, "Thread %d activated but threadedDensity[%d] unaccessible%s.", t, t, FONT_LIGHTRED);
            FatalError(G__msg__);
        }

        #pragma omp parallel for schedule(dynamic,10000) collapse(3)
        for(int mc0=mc0UsedMin[t]; mc0<=mc0UsedMax[t]; mc0++)
            for(int mc1=mc1UsedMin[t]; mc1<=mc1UsedMax[t]; mc1++)
                for(int mc2=mc2UsedMin[t]; mc2<=mc2UsedMax[t]; mc2++)
                {
                    cellIndex_t mc=get_index(mc0,mc1,mc2,N0,N1,N2);
                    DELTA[mc] += threadedDensity[t][mc];
                }

        char name[50]; sprintf(name, "threadedDensity[%d]", t);
        p_free(threadedDensity[t], name);
    }

    #if DEBUG
        PrintDiagnostic(DEBUG_VERBOSITY, "Performing CiC reduction done.");
    #endif

    average_mass_deposited/=N_partial;

    // go from density to density contrast
    // WARNING this will not conserve mass here
    density_to_delta(DELTA, (const double)NMEAN, N0, N1, N2);

    sprintf(G__msg__, "%d particles (%g%%) didn't deposit all of their mass (%g%% on average).", N_partial, (float)N_partial/N,100*average_mass_deposited);
    PrintMessage(5, G__msg__);

    UNINDENT;
    sprintf(G__msg__, "Getting density contrast non-periodic (using %d cores and %d arrays, parallel routine) done.", Nthreads_max, Nthreads);
    PrintMessage(4, G__msg__);
} //get_density_pm_nonperiodic_parallel
///-------------------------------------------------------------------------------------
/** @fn get_density_pm_nonperiodic
 * This routine takes particle positions
 * and calculates density contrast (a wrapper)
 * @param Np number of particles
 * @param positions particles' positions
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param d0 conversion factor to code units x
 * @param d1 conversion factor to code units y
 * @param d2 conversion factor to code units z
 * @param *DELTA output density field
 */
void get_density_pm_nonperiodic(const particleID_t Np, const particle_pos_t positions[Np], const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, float_t *DELTA)
{
//     get_density_pm_nonperiodic_serial(Np,positions,N0,N1,N2,d0,d1,d2,DELTA);
    get_density_pm_nonperiodic_parallel(Np,positions,N0,N1,N2,d0,d1,d2,DELTA);
} //get_density_pm_nonperiodic
///-------------------------------------------------------------------------------------
/** @fn delta_to_density
 * This routine takes the density contrast
 * and calculates returns the normalized density
 * @param *DELTA delta
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 */
void delta_to_density(float_t *DELTA, const int N0, const int N1, const int N2)
{
    const cellIndex_t N=N0*N1*N2;
    #pragma omp parallel for schedule(static)
    for(cellIndex_t mc=0; mc<N; mc++)
        DELTA[mc]+=1.;
} //delta_to_density
///-------------------------------------------------------------------------------------
/** @fn density_to_delta
 * This routine takes a density field
 * and returns the density contrast
 * @param *DENSITY density
 * @param MEAN_IN mean of the density field, recomputed if input is non-positive
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 */
void density_to_delta(float_t *DENSITY, const double MEAN_IN, const int N0, const int N1, const int N2)
{
    const cellIndex_t N=N0*N1*N2;

    // compute the mean if it has not been specified
    double MEAN=MEAN_IN;
    if(MEAN<=0.)
    {
        MEAN=0.;
        #pragma omp parallel for schedule(static) reduction(+:MEAN)
        for(cellIndex_t mc=0; mc<N; mc++)
            MEAN+=DENSITY[mc];
        MEAN/=(double)N;
    }

    // go from density to density contrast
    #pragma omp parallel for schedule(static)
    for(cellIndex_t mc=0; mc<N; mc++)
    {
        DENSITY[mc]/=MEAN;
        DENSITY[mc]-=1.;
    }
} //density_to_delta
///-------------------------------------------------------------------------------------
/** @fn density_to_delta_window
 * This routine takes a density field
 * and returns the density contrast
 * Accounts for a selection window
 * (puts masked cells to mean density)
 * @param *DENSITY density
 * @param *GSW galaxy selection window
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 */
void delta_to_density_window(float_t *DENSITY, const float_t *GSW, const int N0, const int N1, const int N2)
{
    const cellIndex_t N=N0*N1*N2;
    const double EPSILON_GSW=1e-5;

    // compute the mean
    double MEAN=0.; int NCOUNT=0;
    #pragma omp parallel for schedule(static) reduction(+:MEAN)
    for(cellIndex_t mc=0; mc<N; mc++)
        if(GSW[mc]>EPSILON_GSW)
        {
            MEAN+=DENSITY[mc];
            NCOUNT++;
        }
    MEAN/=(double)NCOUNT;

    // go from density to density contrast
    #pragma omp parallel for schedule(static)
    for(cellIndex_t mc=0; mc<N; mc++)
    {
        if(GSW[mc]>EPSILON_GSW)
        {
            DENSITY[mc]/=MEAN;
            DENSITY[mc]-=1.;
        }
        else
            DENSITY[mc]=0.;
    }
} //delta_to_density_window
///-------------------------------------------------------------------------------------
/** @fn update_and_output_density
 * Updates and outputs density field if necessary
 * @param Field field
 * @param P snapshot
 * @param UpdateDensity boolean
 * @param WriteDensity boolean
 * @param Time time
 * @param *OutputDensity pointer to the output filename
 */
void update_and_output_density(Field F, Snapshot P, const bool UpdateDensity, const bool WriteDensity, const double Time, const char *OutputDensity)
{
    if(UpdateDensity)
    {
        // update time
        double Current_time = Time;
        F.time = Current_time;

        // compute density field
        get_density_pm_snapshot(P, F.N0, F.data);

        // write density field
        if(WriteDensity)
            write_field(F, OutputDensity);
    }
} //update_and_output_density
///-------------------------------------------------------------------------------------
/** @fn test_density
 * This routine tests a density field: smoothes on 8 Mpc/h by convolving
 * with a tophat filter in Fourier space, then calculates mean, variance
 * and one-point distribution (printed in testpdf.txt)
 * @param *DENSITY density field to be tested
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 * \note For debug mode only
 */
void test_density(float_t *DENSITY, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2)
{
    #if DEBUG
        const int Nthreads = omp_get_max_threads();
        const cellIndex_t N=N0*N1*N2;
        const int N_HC=N0*N1*(N2/2+1);
        const int N2_HC=(N2/2+1);

        // check for negative density
        bool negative_density=false;
        for(cellIndex_t mc=0; mc<N; mc++)
            if(DENSITY[mc] < -1.)
                negative_density = true;

        // transform density field to Fourier space
        fftw_complex_t *AUX; AUX = (fftw_complex_t*)p_fftw_malloc(N_HC*sizeof(fftw_complex_t), VARNAME(AUX));
        FFT_r2c_3d(N0, N1, N2, DENSITY, AUX, Nthreads);

        // convolves with a top-hat filter kernel
        double rsmooth = 8.;    /* 8 Mpc/h */
        #pragma omp parallel for schedule(static) collapse(3)
        for(int l=0; l<N0; l++)
            for(int m=0; m<N1; m++)
                for(int n=0; n<N2_HC; n++)
                {
                    // Calculates k-mode
                    fourierMode_t mf = get_FourierMode_Id(l,m,n,N0,N1,N2);
                    double kmod = get_kmodulus(l,m,n,N0,L1,N2,L0,L1,L2);

                    double aux1 = rsmooth * kmod;

                    // Calculates window
                    double window = 3.0 * (sin(aux1) - aux1*cos(aux1))/aux1/aux1/aux1;
                    if(kmod<1e-6) window = 1.0;

                    AUX[mf] *= window;
                }
        // Jeans's swindle
        AUX[0]=0.;

        // transform back to real space
        float_t auxdensity[N];
        FFT_c2r_3d(N0, N1, N2, AUX, auxdensity, Nthreads);

        p_fftw_free(AUX, VARNAME(AUX));

        const int Nbin=200;
        const double deltamin=-2.;
        const double deltamax=+10.;
        const double ddelta=(deltamax-deltamin)/(double)Nbin;

        double Pdelta[Nbin];
        #pragma omp parallel for schedule(static)
        for(int i=0; i<Nbin; i++)
            Pdelta[i]=0.;

        int count=0; double var=0., mean=0.;
        #pragma omp parallel for schedule(static) reduction(+:count,mean,var)
        for(cellIndex_t mc=0; mc<N; mc++)
        {
            int bin=(int)((auxdensity[mc]-deltamin)/ddelta);
            if(bin<Nbin && bin>-1)
            {
                #pragma omp atomic
                Pdelta[bin]+=1.;
                count++;
            }
            mean += (double)auxdensity[mc];
            var += (double)(auxdensity[mc]*auxdensity[mc]);
        }
        mean=mean/(double)N;
        double sigma8 = sqrt(var/(double)N);

        FILE *fp;
        char outputFileName[] = "testpdf.txt";
        fp=fopen(outputFileName, "w");
        for(int k=0; k<Nbin; k++)
        {
            double delta = deltamin + (double)k*ddelta;
            fprintf(fp, "%lg %lg\n", delta, Pdelta[k]/(double)count);
        }
        fclose(fp);

        debugPrintMessage("Testing density field...");
        debugPrintFloattArray(DENSITY,N);
        debugPrintBool(negative_density,false);
        debugPrintDouble(mean); //Should be zero
        debugPrintDouble(sigma8); //Should be sigma8
        debugPrintMessage("Testing density field done.");
    #endif
} //test_density
///-------------------------------------------------------------------------------------
/** @fn compare_density_fields
 * This routine tests the equality between
 * two density fields
 * @param *DENSITY1 density field to be tested
 * @param *DENSITY2 density field to be tested
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param epsilon relative tolerance
 * @return bool
 */
bool compare_density_fields(const float_t *DENSITY1, const float_t *DENSITY2, const int N0, const int N1, const int N2, const double epsilon)
{
    const cellIndex_t N=N0*N1*N2;

    for(cellIndex_t mc=0; mc<N; mc++)
    {
        double diff = fabs(DENSITY1[mc]-DENSITY2[mc]);
        if( diff > max(epsilon * fabs(DENSITY1[mc]),1e-8) && diff > max(epsilon * fabs(DENSITY2[mc]),1e-8) )
        {
            debugPrintDouble(DENSITY1[mc]);
            debugPrintDouble(DENSITY2[mc]);
            debugPrintDouble(fabs(DENSITY1[mc]-DENSITY2[mc]));
            debugPrintDouble(epsilon * fabs(DENSITY1[mc]));
            debugPrintDouble(epsilon * fabs(DENSITY2[mc]));
            return false;
        }
    }

    return true;
} //compare_density_fields
///-------------------------------------------------------------------------------------
