///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/src/fd_schemes.c
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file fd_schemes.c
 *  \brief Routines for finite difference schemes
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2017-2023
 */
#include "fd_schemes.h"
///-------------------------------------------------------------------------------------
/** @fn laplace_DFT_1d
 * Laplacian operator in Fourier space in one dimension
 */
double laplace_DFT_1d(const double k, const double d)
{
    #if FDA==0
        UNUSED(d);
        return -k*k;
    #elif FDA==2
        // this is also -2*(cos(k)+1)
        return -4.*sin(k*d/2.)*sin(k*d/2.)/(d*d);
    #elif FDA==4
        // this is also -1/6*(cos(2*k)-16*cos(k)+15)
        return 1./3.*(sin(k*d)*sin(k*d) - 16.*sin(k*d/2.)*sin(k*d/2.))/(d*d);
    #elif FDA>=6
        // this is also -1/90*(-2*cos(3*k)+27*cos(2*k)-270*cos(k)+245)
        return -1./45.*(2.*sin(3*k*d/2.)*sin(3*k*d/2.) - 27.*sin(k*d)*sin(k*d) + 270.*sin(k*d/2.)*sin(k*d/2.))/(d*d);
    #endif
} //laplace_DFT_1d
///-------------------------------------------------------------------------------------
/** @fn laplace_DFT
 * Laplacian operator in Fourier space in three dimensions
 */
double laplace_DFT(const double k0, const double k1, const double k2, const double d0, const double d1, const double d2)
{
    return laplace_DFT_1d(k0,d0) + laplace_DFT_1d(k1,d1) + laplace_DFT_1d(k2,d2);
} //laplace_DFT
///-------------------------------------------------------------------------------------
/** @fn laplace_RODFT00_1d
 * Laplacian operator in sine space (DST-I) in one dimension
 */
double laplace_RODFT00_1d(const double k, const double d, const int N)
{
    #if FDA==0
        UNUSED(d);
        return -1./4.*(k*N/(N+1))*(k*N/(N+1));
    #elif FDA==2
        // this is also 2*(1-cos(k/2*N/(N+1)))
        return -4.*sin(k*d/4.*N/(N+1))*sin(k*d/4.*N/(N+1))/(d*d);
    #elif FDA==4
        // this is also -1/6*(cos(k*N/(N+1))-16*cos(k/2*N/(N+1))+15)
        return 1./3.*(sin(k*d/2.*N/(N+1))*sin(k*d/2.*N/(N+1)) - 16.*sin(k*d/4.*N/(N+1))*sin(k*d/4.*N/(N+1)))/(d*d);
    #elif FDA>=6
        // this is also -1/90*(-2*cos(3/2*k*N/(N+1))+27*cos(k*N/(N+1))-270*cos(k/2*N/(N+1))+245)
        return -1./45.*(2.*sin(3*k*d/4.*N/(N+1))*sin(3*k*d/4.*N/(N+1)) - 27.*sin(k*d/2.*N/(N+1))*sin(k*d/2.*N/(N+1)) + 270.*sin(k*d/4.*N/(N+1))*sin(k*d/4.*N/(N+1)))/(d*d);
    #endif
} //laplace_RODFT00_1d
///-------------------------------------------------------------------------------------
/** @fn laplace_RODFT00
 * Laplacian operator in sine space (DST-I) in three dimensions
 */
double laplace_RODFT00(const double k0, const double k1, const double k2, const double d0, const double d1, const double d2, const int N0, const int N1, const int N2)
{
    return laplace_RODFT00_1d(k0,d0,N0) + laplace_RODFT00_1d(k1,d1,N1) + laplace_RODFT00_1d(k2,d2,N2);
} //laplace_RODFT00
///-------------------------------------------------------------------------------------
/** @fn gradient_DFT_1d
 * Gradient operator in Fourier space in one dimension, yet to be multiplied by I
 */
double gradient_DFT_1d(const double k, const double d)
{
    #if FDA==0
        UNUSED(d);
        return -k;
    #elif FDA==2
        return -sin(k*d)/d;
    #elif FDA==4
        return -1./6.*(-sin(2*k*d) + 8.*sin(k*d))/d;
    #elif FDA>=6
        return -1./30.*(sin(3*k*d) - 9.*sin(2*k*d) + 45.*sin(k*d))/d;
    #endif
} //gradient_DFT_1d
///-------------------------------------------------------------------------------------
/** @fn gradient_RODFT00_1d
 * Gradient operator in Fourier space in one dimension, yet to be multiplied by I
 */
double gradient_RODFT00_1d(const double k, const double d, const int N)
{
    #if FDA==0
        UNUSED(d);
        return -k/2.*N/(N+1);
    #elif FDA==2
        return -sin(k*d/2.*N/(N+1))/d;
    #elif FDA==4
        return -1./6.*(-sin(k*d*N/(N+1)) + 8.*sin(k*d/2.*N/(N+1)))/d;
    #elif FDA>=6
        return -1./30.*(sin(3*k*d/2.*N/(N+1)) - 9.*sin(k*d*N/(N+1)) + 45.*sin(k*d/2.*N/(N+1)))/d;
    #endif
} //gradient_RODFT00_1d
///-------------------------------------------------------------------------------------
/** @fn get_fd_indices
 * Auxiliary function for 1d finite difference functions
 */
static inline void get_fd_indices(const int i, const int j, const int k, const int N0, const int N1, const int N2, int *ip1, int *im1, int *ip2, int *im2, int *ip3, int *im3, int *jp1, int *jm1, int *jp2, int *jm2, int *jp3, int *jm3, int *kp1, int *km1, int *kp2, int *km2, int * kp3, int *km3)
{
    *im1 = (int)p_mod(i-1,N0);
    *ip1 = (int)p_mod(i+1,N0);
    *jp1 = (int)p_mod(j+1,N1);
    *jm1 = (int)p_mod(j-1,N1);
    *kp1 = (int)p_mod(k+1,N2);
    *km1 = (int)p_mod(k-1,N2);
    #if FDA>=4 || FDA==0
        *ip2 = (int)p_mod(i+2,N0);
        *im2 = (int)p_mod(i-2,N0);
        *jp2 = (int)p_mod(j+2,N1);
        *jm2 = (int)p_mod(j-2,N1);
        *kp2 = (int)p_mod(k+2,N2);
        *km2 = (int)p_mod(k-2,N2);
    #else
        UNUSED(ip2);
        UNUSED(im2);
        UNUSED(jp2);
        UNUSED(jm2);
        UNUSED(kp2);
        UNUSED(km2);
    #endif
    #if FDA>=6 || FDA==0
        *ip3 = (int)p_mod(i+3,N0);
        *im3 = (int)p_mod(i-3,N0);
        *jp3 = (int)p_mod(j+3,N1);
        *jm3 = (int)p_mod(j-3,N1);
        *kp3 = (int)p_mod(k+3,N2);
        *km3 = (int)p_mod(k-3,N2);
    #else
        UNUSED(ip3);
        UNUSED(im3);
        UNUSED(jp3);
        UNUSED(jm3);
        UNUSED(kp3);
        UNUSED(km3);
    #endif
} //get_fd_indices
///-------------------------------------------------------------------------------------
/** @fn gradient_rs_1d
 * Gradient operator in real space in one dimension
 * Expressions in Hahn & Abel 2011, arXiv:1103.6031, table 1
 */
float_t gradient_rs_1d(const float_t *A, const int d, const int i, const int j, const int k, const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, const int boundary_conditions, const int N0_pad, const int N1_pad, const int N2_pad)
{
    const bool p=(boundary_conditions==1); // periodic boundary conditions
    const bool D=(boundary_conditions==3); // Dirichlet boundary conditions
    float_t res=0.;
    int ip1,im1, ip2,im2, ip3,im3;
    int jp1,jm1, jp2,jm2, jp3,jm3;
    int kp1,km1, kp2,km2, kp3,km3;

    get_fd_indices(i,j,k,N0,N1,N2,&ip1,&im1,&ip2,&im2,&ip3,&im3,&jp1,&jm1,&jp2,&jm2,&jp3,&jm3,&kp1,&km1,&kp2,&km2,&kp3,&km3);

    #if FDA==2
        const double FDC_grad_m1 = -1./2.;
        const double FDC_grad_p1 = +1./2.;
    #elif FDA==4
        const double FDC_grad_m2 = +1./12.;
        const double FDC_grad_m1 = -1./12.*8.;
        const double FDC_grad_p1 = +1./12.*8.;
        const double FDC_grad_p2 = -1./12.;
    #elif FDA>=6 || FDA==0
        const double FDC_grad_m3 = -1./60.;
        const double FDC_grad_m2 = +1./60.*9.;
        const double FDC_grad_m1 = -1./60.*45.;
        const double FDC_grad_p1 = +1./60.*45.;
        const double FDC_grad_p2 = -1./60.*9.;
        const double FDC_grad_p3 = +1./60.;
    #endif

    switch(d)
    {
        case 0:
        {
            #if FDA>=6 || FDA==0
                const float_t A_im3_j_k = p ? A[get_index(im3,j,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i-3,j,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./d0*FDC_grad_m3*A_im3_j_k;
            #endif
            #if FDA>=4 || FDA==0
                const float_t A_im2_j_k = p ? A[get_index(im2,j,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i-2,j,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./d0*FDC_grad_m2*A_im2_j_k;
            #endif
            const float_t A_im1_j_k = p ? A[get_index(im1,j,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i-1,j,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
            res += 1./d0*FDC_grad_m1*A_im1_j_k;
            const float_t A_ip1_j_k = p ? A[get_index(ip1,j,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i+1,j,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
            res += 1./d0*FDC_grad_p1*A_ip1_j_k;
            #if FDA>=4 || FDA==0
                const float_t A_ip2_j_k = p ? A[get_index(ip2,j,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i+2,j,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./d0*FDC_grad_p2*A_ip2_j_k;
            #endif
            #if FDA>=6 || FDA==0
                const float_t A_ip3_j_k = p ? A[get_index(ip3,j,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i+3,j,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./d0*FDC_grad_p3*A_ip3_j_k;
            #endif
        }
        break;
        case 1:
        {
            #if FDA>=6 || FDA==0
                const float_t A_i_jm3_k = p ? A[get_index(i,jm3,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j-3,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./d1*FDC_grad_m3*A_i_jm3_k;
            #endif
            #if FDA>=4 || FDA==0
                const float_t A_i_jm2_k = p ? A[get_index(i,jm2,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j-2,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./d1*FDC_grad_m2*A_i_jm2_k;
            #endif
            const float_t A_i_jm1_k = p ? A[get_index(i,jm1,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j-1,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
            res += 1./d1*FDC_grad_m1*A_i_jm1_k;
            const float_t A_i_jp1_k = p ? A[get_index(i,jp1,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j+1,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
            res += 1./d1*FDC_grad_p1*A_i_jp1_k;
            #if FDA>=4 || FDA==0
                const float_t A_i_jp2_k = p ? A[get_index(i,jp2,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j+2,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./d1*FDC_grad_p2*A_i_jp2_k;
            #endif
            #if FDA>=6 || FDA==0
                const float_t A_i_jp3_k = p ? A[get_index(i,jp3,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j+3,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./d1*FDC_grad_p3*A_i_jp3_k;
            #endif
        }
        break;
        case 2:
        {
            #if FDA>=6 || FDA==0
                const float_t A_i_j_km3 = p ? A[get_index(i,j,km3,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j,k-3,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./d2*FDC_grad_m3*A_i_j_km3;
            #endif
            #if FDA>=4 || FDA==0
                const float_t A_i_j_km2 = p ? A[get_index(i,j,km2,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j,k-2,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./d2*FDC_grad_m2*A_i_j_km2;
            #endif
            const float_t A_i_j_km1 = p ? A[get_index(i,j,km1,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j,k-1,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
            res += 1./d2*FDC_grad_m1*A_i_j_km1;
            const float_t A_i_j_kp1 = p ? A[get_index(i,j,kp1,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j,k+1,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
            res += 1./d2*FDC_grad_p1*A_i_j_kp1;
            #if FDA>=4 || FDA==0
                const float_t A_i_j_kp2 = p ? A[get_index(i,j,kp2,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j,k+2,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./d2*FDC_grad_p2*A_i_j_kp2;
            #endif
            #if FDA>=6 || FDA==0
                const float_t A_i_j_kp3 = p ? A[get_index(i,j,kp3,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j,k+3,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./d2*FDC_grad_p3*A_i_j_kp3;
            #endif
        }
        break;
        default:
        break;
    }
    return res;
} //gradient_rs_1d
///-------------------------------------------------------------------------------------
/** @fn laplace_rs_1d
 * Laplacian operator in real space in one dimension
 * Expressions in Hahn & Abel 2011, arXiv:1103.6031, table 1
 */
float_t laplace_rs_1d(const float_t *A, const int d, const int i, const int j, const int k, const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, const int boundary_conditions, const int N0_pad, const int N1_pad, const int N2_pad)
{
    const bool p=(boundary_conditions==1); // periodic boundary conditions
    const bool D=(boundary_conditions==3); // Dirichlet boundary conditions
    float_t res=0.;
    int ip1,im1, ip2,im2, ip3,im3;
    int jp1,jm1, jp2,jm2, jp3,jm3;
    int kp1,km1, kp2,km2, kp3,km3;

    get_fd_indices(i,j,k,N0,N1,N2,&ip1,&im1,&ip2,&im2,&ip3,&im3,&jp1,&jm1,&jp2,&jm2,&jp3,&jm3,&kp1,&km1,&kp2,&km2,&kp3,&km3);

    #if FDA==2
        const double FDC_laplace_m1 = +1.;
        const double FDC_laplace_00 = -1.;
        const double FDC_laplace_p1 = +1.;
    #elif FDA==4
        const double FDC_laplace_m2 = -1./12.;
        const double FDC_laplace_m1 = +1./12.*16.;
        const double FDC_laplace_00 = -1./12.*30.;
        const double FDC_laplace_p1 = +1./12.*16.;
        const double FDC_laplace_p2 = -1./12.;
    #elif FDA>=6 || FDA==0
        const double FDC_laplace_m3 = +1./180.*2.;
        const double FDC_laplace_m2 = -1./180.*27.;
        const double FDC_laplace_m1 = +1./180.*270.;
        const double FDC_laplace_00 = -1./180.*490.;
        const double FDC_laplace_p1 = +1./180.*270.;
        const double FDC_laplace_p2 = -1./180.*27.;
        const double FDC_laplace_p3 = +1./180.*2.;
    #endif

    switch(d)
    {
        case 0:
        {
            #if FDA>=6 || FDA==0
                const float_t A_im3_j_k = p ? A[get_index(im3,j,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i-3,j,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./(d0*d0)*FDC_laplace_m3*A_im3_j_k;
            #endif
            #if FDA>=4 || FDA==0
                const float_t A_im2_j_k = p ? A[get_index(im2,j,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i-2,j,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./(d0*d0)*FDC_laplace_m2*A_im2_j_k;
            #endif
            const float_t A_im1_j_k = p ? A[get_index(im1,j,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i-1,j,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
            res += 1./(d0*d0)*FDC_laplace_m1*A_im1_j_k;
            const float_t A_i_j_k = D ? A[get_index_in_paddedbox(i,j,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : A[get_index(i,j,k,N0,N1,N2)];
            res += 1./(d0*d0)*FDC_laplace_00*A_i_j_k;
            const float_t A_ip1_j_k = p ? A[get_index(ip1,j,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i+1,j,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
            res += 1./(d0*d0)*FDC_laplace_p1*A_ip1_j_k;
            #if FDA>=4 || FDA==0
                const float_t A_ip2_j_k = p ? A[get_index(ip2,j,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i+2,j,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./(d0*d0)*FDC_laplace_p2*A_ip2_j_k;
            #endif
            #if FDA>=6 || FDA==0
                const float_t A_ip3_j_k = p ? A[get_index(ip3,j,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i+3,j,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./(d0*d0)*FDC_laplace_p3*A_ip3_j_k;
            #endif
        }
        break;
        case 1:
        {
            #if FDA>=6 || FDA==0
                const float_t A_i_jm3_k = p ? A[get_index(i,jm3,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j-3,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./(d1*d1)*FDC_laplace_m3*A_i_jm3_k;
            #endif
            #if FDA>=4 || FDA==0
                const float_t A_i_jm2_k = p ? A[get_index(i,jm2,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j-2,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./(d1*d1)*FDC_laplace_m2*A_i_jm2_k;
            #endif
            const float_t A_i_jm1_k = p ? A[get_index(i,jm1,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j-1,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
            res += 1./(d1*d1)*FDC_laplace_m1*A_i_jm1_k;
            const float_t A_i_j_k = D ? A[get_index_in_paddedbox(i,j,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : A[get_index(i,j,k,N0,N1,N2)];
            res += 1./(d1*d1)*FDC_laplace_00*A_i_j_k;
            const float_t A_i_jp1_k = p ? A[get_index(i,jp1,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j+1,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
            res += 1./(d1*d1)*FDC_laplace_p1*A_i_jp1_k;
            #if FDA>=4 || FDA==0
                const float_t A_i_jp2_k = p ? A[get_index(i,jp2,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j+2,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./(d1*d1)*FDC_laplace_p2*A_i_jp2_k;
            #endif
            #if FDA>=6 || FDA==0
                const float_t A_i_jp3_k = p ? A[get_index(i,jp3,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j+3,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./(d1*d1)*FDC_laplace_p3*A_i_jp3_k;
            #endif
        }


        break;
        case 2:
        {
            #if FDA>=6 || FDA==0
                const float_t A_i_j_km3 = p ? A[get_index(i,j,km3,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j,k-3,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./(d2*d2)*FDC_laplace_m3*A_i_j_km3;
            #endif
            #if FDA>=4 || FDA==0
                const float_t A_i_j_km2 = p ? A[get_index(i,j,km2,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j,k-2,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./(d2*d2)*FDC_laplace_m2*A_i_j_km2;
            #endif
            const float_t A_i_j_km1 = p ? A[get_index(i,j,km1,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j,k-1,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
            res += 1./(d2*d2)*FDC_laplace_m1*A_i_j_km1;
            const float_t A_i_j_k = D ? A[get_index_in_paddedbox(i,j,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : A[get_index(i,j,k,N0,N1,N2)];
            res += 1./(d2*d2)*FDC_laplace_00*A_i_j_k;
            const float_t A_i_j_kp1 = p ? A[get_index(i,j,kp1,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j,k+1,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
            res += 1./(d2*d2)*FDC_laplace_p1*A_i_j_kp1;
            #if FDA>=4 || FDA==0
                const float_t A_i_j_kp2 = p ? A[get_index(i,j,kp2,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j,k+2,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./(d2*d2)*FDC_laplace_p2*A_i_j_kp2;
            #endif
            #if FDA>=6 || FDA==0
                const float_t A_i_j_kp3 = p ? A[get_index(i,j,kp3,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j,k+3,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./(d2*d2)*FDC_laplace_p3*A_i_j_kp3;
            #endif
        }
        break;
        default:
        break;
    }
    return res;
} //laplace_rs_1d
///-------------------------------------------------------------------------------------
/** @fn flux_rs_1d
 * Flux operator in Fourier space in one dimension
 * Expressions in Hahn & Abel 2011, arXiv:1103.6031, table 2
 */
float_t flux_rs_1d(const float_t *A, const int d, const int i, const int j, const int k, const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, const int boundary_conditions, const int N0_pad, const int N1_pad, const int N2_pad)
{
    const bool p=(boundary_conditions==1); // periodic boundary conditions
    const bool D=(boundary_conditions==3); // Dirichlet boundary conditions
    float_t res=0.;
    int ip1,im1, ip2,im2, ip3,im3;
    int jp1,jm1, jp2,jm2, jp3,jm3;
    int kp1,km1, kp2,km2, kp3,km3;

    get_fd_indices(i,j,k,N0,N1,N2,&ip1,&im1,&ip2,&im2,&ip3,&im3,&jp1,&jm1,&jp2,&jm2,&jp3,&jm3,&kp1,&km1,&kp2,&km2,&kp3,&km3);

    #if FDA==2
        const double FDC_flux_m1 = -1.;
        const double FDC_flux_p1 = +1.;
    #elif FDA==4
        const double FDC_flux_m2 = -1./12.;
        const double FDC_flux_m1 = +1./12.*15.;
        const double FDC_flux_p1 = -1./12.*15.;
        const double FDC_flux_p2 = +1./12.;
    #elif FDA>=6 || FDA==0
        const double FDC_flux_m3 = +1./180.*2.;
        const double FDC_flux_m2 = -1./180.*25.;
        const double FDC_flux_m1 = +1./180.*245.;
        const double FDC_flux_p1 = -1./180.*245.;
        const double FDC_flux_p2 = +1./180.*25.;
        const double FDC_flux_p3 = -1./180.*2.;
    #endif

    switch(d)
    {
        case 0:
        {
            #if FDA>=6 || FDA==0
                const float_t A_im3_j_k = p ? A[get_index(im3,j,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i-3,j,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./d0*FDC_flux_m3*A_im3_j_k;
            #endif
            #if FDA>=4 || FDA==0
                const float_t A_im2_j_k = p ? A[get_index(im2,j,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i-2,j,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./d0*FDC_flux_m2*A_im2_j_k;
            #endif
            const float_t A_im1_j_k = p ? A[get_index(im1,j,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i-1,j,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
            res += 1./d0*FDC_flux_m1*A_im1_j_k;
            const float_t A_ip1_j_k = p ? A[get_index(ip1,j,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i+1,j,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
            res += 1./d0*FDC_flux_p1*A_ip1_j_k;
            #if FDA>=4 || FDA==0
                const float_t A_ip2_j_k = p ? A[get_index(ip2,j,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i+2,j,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./d0*FDC_flux_p2*A_ip2_j_k;
            #endif
            #if FDA>=6 || FDA==0
                const float_t A_ip3_j_k = p ? A[get_index(ip3,j,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i+3,j,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./d0*FDC_flux_p3*A_ip3_j_k;
            #endif
        }
        break;
        case 1:
        {
            #if FDA>=6 || FDA==0
                const float_t A_i_jm3_k = p ? A[get_index(i,jm3,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j-3,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./d1*FDC_flux_m3*A_i_jm3_k;
            #endif
            #if FDA>=4 || FDA==0
                const float_t A_i_jm2_k = p ? A[get_index(i,jm2,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j-2,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./d1*FDC_flux_m2*A_i_jm2_k;
            #endif
            const float_t A_i_jm1_k = p ? A[get_index(i,jm1,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j-1,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
            res += 1./d1*FDC_flux_m1*A_i_jm1_k;
            const float_t A_i_jp1_k = p ? A[get_index(i,jp1,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j+1,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
            res += 1./d1*FDC_flux_p1*A_i_jp1_k;
            #if FDA>=4 || FDA==0
                const float_t A_i_jp2_k = p ? A[get_index(i,jp2,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j+2,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./d1*FDC_flux_p2*A_i_jp2_k;
            #endif
            #if FDA>=6 || FDA==0
                const float_t A_i_jp3_k = p ? A[get_index(i,jp3,k,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j+3,k,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./d1*FDC_flux_p3*A_i_jp3_k;
            #endif
        }
        break;
        case 2:
        {
            #if FDA>=6 || FDA==0
                const float_t A_i_j_km3 = p ? A[get_index(i,j,km3,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j,k-3,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./d2*FDC_flux_m3*A_i_j_km3;
            #endif
            #if FDA>=4 || FDA==0
                const float_t A_i_j_km2 = p ? A[get_index(i,j,km2,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j,k-2,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./d2*FDC_flux_m2*A_i_j_km2;
            #endif
            const float_t A_i_j_km1 = p ? A[get_index(i,j,km1,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j,k-1,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
            res += 1./d2*FDC_flux_m1*A_i_j_km1;
            const float_t A_i_j_kp1 = p ? A[get_index(i,j,kp1,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j,k+1,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
            res += 1./d2*FDC_flux_p1*A_i_j_kp1;
            #if FDA>=4 || FDA==0
                const float_t A_i_j_kp2 = p ? A[get_index(i,j,kp2,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j,k+2,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./d2*FDC_flux_p2*A_i_j_kp2;
            #endif
            #if FDA>=6 || FDA==0
                const float_t A_i_j_kp3 = p ? A[get_index(i,j,kp3,N0,N1,N2)] : ( D ? A[get_index_in_paddedbox(i,j,k+3,N0,N1,N2,N0_pad,N1_pad,N2_pad)] : 0. );
                res += 1./d2*FDC_flux_p3*A_i_j_kp3;
            #endif
        }
        break;
        default:
        break;
    }
    return res;
} //flux_rs_1d
///-------------------------------------------------------------------------------------
