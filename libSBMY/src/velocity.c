///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/src/velocity.c
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file velocity.c
 *  \brief Routines for dealing with velocity fields
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2013-2023
 */
#include "velocity.h"
///-------------------------------------------------------------------------------------
/** @fn get_velocities_pm_snapshot
 * Calculate velocity on the grid from particles with CiC scheme
 * @param P input: Gadget snapshot
 * @param N0 mesh size
 * @param *VELOCITY0 output: velocity x on the grid
 * @param *VELOCITY1 output: velocity y on the grid
 * @param *VELOCITY2 output: velocity z on the grid
 */
void get_velocities_pm_snapshot(const Snapshot P, const int N0, float_t *VELOCITY0, float_t *VELOCITY1, float_t *VELOCITY2)
{
    const int Nthreads_max = omp_get_max_threads();
    const double L0=P.header.BoxSize, L1=P.header.BoxSize, L2=P.header.BoxSize;
    const int N1=N0, N2=N0;
    const cellIndex_t N=N0*N1*N2;
    const particleID_t Np=(particleID_t)(P.header.npart[0]+P.header.npart[1]+P.header.npart[2]+P.header.npart[3]+P.header.npart[4]+P.header.npart[5]);

    const double d0=L0/(double)N0;
    const double d1=L1/(double)N1;
    const double d2=L2/(double)N2;
    float_t DENSITY[N];

    // declare initialize auxiliary arrays
    float_t *threadedDENSITY[Nthreads_max], *threadedVELOCITY0[Nthreads_max], *threadedVELOCITY1[Nthreads_max], *threadedVELOCITY2[Nthreads_max];
    bool threadActivated[Nthreads_max];
    int mc0UsedMin[Nthreads_max], mc0UsedMax[Nthreads_max];
    int mc1UsedMin[Nthreads_max], mc1UsedMax[Nthreads_max];
    int mc2UsedMin[Nthreads_max], mc2UsedMax[Nthreads_max];
    for(int t=0; t<Nthreads_max; t++)
    {
        threadActivated[t]=false;
        threadedDENSITY[t]=0; // initialize pointers to zero to check if memory allocation went fine
        threadedVELOCITY0[t]=0;
        threadedVELOCITY1[t]=0;
        threadedVELOCITY2[t]=0;
        mc0UsedMin[t]=N0-1; mc1UsedMin[t]=N1-1; mc2UsedMin[t]=N2-1;
        mc0UsedMax[t]=0; mc1UsedMax[t]=0; mc2UsedMax[t]=0;
    }

    // get number of possible threads from available memory
    int Nthreads = get_Nthreads_from_available_memory((const long long)(4*N), sizeof(float_t), Nthreads_max);

    sprintf(G__msg__, "Getting velocities on the mesh (using %d cores and %d sets of arrays)...", Nthreads_max, Nthreads);
    PrintMessage(4, G__msg__);
    INDENT;

    // initialize VELOCITY
    #pragma omp parallel for schedule(static)
    for(cellIndex_t mc=0; mc<N; mc++)
    {
        DENSITY[mc]=0.;
        VELOCITY0[mc]=0.;
        VELOCITY1[mc]=0.;
        VELOCITY2[mc]=0.;
    }

    // do binning
    #pragma omp parallel num_threads(Nthreads)
    {
        int t = omp_get_thread_num(); // get rank of current process
        threadActivated[t] = true;

        if(t!=0)
        {
            // allocate threadedDensity and initialize to zero
            char name[40];
            sprintf(name, "threadedDENSITY[%d]", t); threadedDENSITY[t] = (float_t*)p_calloc(N, sizeof(float_t), name);
            sprintf(name, "threadedVELOCITY0[%d]", t); threadedVELOCITY0[t] = (float_t*)p_calloc(N, sizeof(float_t), name);
            sprintf(name, "threadedVELOCITY1[%d]", t); threadedVELOCITY1[t] = (float_t*)p_calloc(N, sizeof(float_t), name);
            sprintf(name, "threadedVELOCITY2[%d]", t); threadedVELOCITY2[t] = (float_t*)p_calloc(N, sizeof(float_t), name);
        }
        else if(t==0)
        {
            // use the already existing and initialized array DENSITY for thread #0
            threadedDENSITY[t] = DENSITY;
            threadedVELOCITY0[t] = VELOCITY0;
            threadedVELOCITY1[t] = VELOCITY1;
            threadedVELOCITY2[t] = VELOCITY2;
        }

        #pragma omp for schedule(static)
        for(particleID_t mp=0; mp<Np; mp++)
        {
            int mc0min,mc0max,mc1min,mc1max,mc2min,mc2max;
            float_t x0 = P.positions[mp].Pos[0];
            float_t x1 = P.positions[mp].Pos[1];
            float_t x2 = P.positions[mp].Pos[2];
            float_t v0 = P.velocities[mp].Vel[0];
            float_t v1 = P.velocities[mp].Vel[1];
            float_t v2 = P.velocities[mp].Vel[2];

            // assign particle to the mesh
            particle_to_mesh_periodic(x0,x1,x2,1.,threadedDENSITY[t],N0,N1,N2,d0,d1,d2,&mc0min,&mc0max,&mc1min,&mc1max,&mc2min,&mc2max);
            particle_to_mesh_periodic(x0,x1,x2,v0,threadedVELOCITY0[t],N0,N1,N2,d0,d1,d2,&mc0min,&mc0max,&mc1min,&mc1max,&mc2min,&mc2max);
            particle_to_mesh_periodic(x0,x1,x2,v1,threadedVELOCITY1[t],N0,N1,N2,d0,d1,d2,&mc0min,&mc0max,&mc1min,&mc1max,&mc2min,&mc2max);
            particle_to_mesh_periodic(x0,x1,x2,v2,threadedVELOCITY2[t],N0,N1,N2,d0,d1,d2,&mc0min,&mc0max,&mc1min,&mc1max,&mc2min,&mc2max);

            // update counters for this thread
            mc0UsedMin[t] = min(mc0UsedMin[t], mc0min);
            mc0UsedMax[t] = max(mc0UsedMax[t], mc0max);
            mc1UsedMin[t] = min(mc1UsedMin[t], mc1min);
            mc1UsedMax[t] = max(mc1UsedMax[t], mc1max);
            mc2UsedMin[t] = min(mc2UsedMin[t], mc2min);
            mc2UsedMax[t] = max(mc2UsedMax[t], mc2max);
        }
    }

    // perform the reduction and free memory for auxiliary arrays
    for(int t=1; t<Nthreads_max; t++) //start the loop at thread #1 here, as DENSITY and VELOCITY* already contains the result of thread #0
    {
        if (!threadActivated[t])
            continue;

        if(threadedDENSITY[t] == 0)
        {
            sprintf(G__msg__, "Thread %d activated but threadedDENSITY[%d] unaccessible%s.", t, t, FONT_LIGHTRED);
            FatalError(G__msg__);
        }
        if(threadedVELOCITY0[t] == 0)
        {
            sprintf(G__msg__, "Thread %d activated but threadedVELOCITY0[%d] unaccessible%s.", t, t, FONT_LIGHTRED);
            FatalError(G__msg__);
        }
        if(threadedVELOCITY1[t] == 0)
        {
            sprintf(G__msg__, "Thread %d activated but threadedVELOCITY1[%d] unaccessible%s.", t, t, FONT_LIGHTRED);
            FatalError(G__msg__);
        }
        if(threadedVELOCITY2[t] == 0)
        {
            sprintf(G__msg__, "Thread %d activated but threadedVELOCITY2[%d] unaccessible%s.", t, t, FONT_LIGHTRED);
            FatalError(G__msg__);
        }

        #pragma omp parallel for schedule(dynamic,10000) collapse(3)
        for(int mc0=mc0UsedMin[t]; mc0<=mc0UsedMax[t]; mc0++)
            for(int mc1=mc1UsedMin[t]; mc1<=mc1UsedMax[t]; mc1++)
                for(int mc2=mc2UsedMin[t]; mc2<=mc2UsedMax[t]; mc2++)
                {
                    cellIndex_t mc=get_index(mc0,mc1,mc2,N0,N1,N2);
                    DENSITY[mc] += threadedDENSITY[t][mc];
                    VELOCITY0[mc] += threadedVELOCITY0[t][mc];
                    VELOCITY1[mc] += threadedVELOCITY1[t][mc];
                    VELOCITY2[mc] += threadedVELOCITY2[t][mc];
                }

        char name[40];
        sprintf(name, "threadedDENSITY[%d]", t); p_free(threadedDENSITY[t], name);
        sprintf(name, "threadedVELOCITY0[%d]", t); p_free(threadedVELOCITY0[t], name);
        sprintf(name, "threadedVELOCITY1[%d]", t); p_free(threadedVELOCITY1[t], name);
        sprintf(name, "threadedVELOCITY2[%d]", t); p_free(threadedVELOCITY2[t], name);
    }

    // divide by density
    #pragma omp parallel for schedule(static)
    for(cellIndex_t mc=0; mc<N; mc++)
    {
        if(DENSITY[mc]>0)
        {
            VELOCITY0[mc] /= DENSITY[mc];
            VELOCITY1[mc] /= DENSITY[mc];
            VELOCITY2[mc] /= DENSITY[mc];
        }
    }

    UNINDENT;
    sprintf(G__msg__, "Getting velocities on the mesh (using %d cores and %d sets of arrays) done.", Nthreads_max, Nthreads);
    PrintMessage(4, G__msg__);
} //get_velocities_pm_snapshot
///-------------------------------------------------------------------------------------
/** @fn get_divV_fs
 * Computes the divergence of the velocity field in Fourier space
 * @param *VELOCITY0 input: velocity field
 * @param *VELOCITY1 input: velocity field
 * @param *VELOCITY2 input: velocity field
 * @param *DIVV output: divergence of V
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 */
void get_divV_fs(const float_t *VELOCITY0, const float_t *VELOCITY1, const float_t *VELOCITY2, float_t *DIVV, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2)
{
    int Nthreads = omp_get_max_threads();
    sprintf(G__msg__, "Getting divergence of the velocity field in Fourier space (using %d cores)...", Nthreads);
    PrintMessage(4, G__msg__);
    INDENT;

    divergence_of_vector(VELOCITY0, VELOCITY1, VELOCITY2, DIVV, false, N0, N1, N2, L0, L1, L2);

    UNINDENT;
    sprintf(G__msg__, "Getting divergence of the velocity field in Fourier space (using %d cores) done.", Nthreads);
    PrintMessage(4, G__msg__);
} //get_divV_fs
///-------------------------------------------------------------------------------------
/** @fn get_rotV_fs
 * Computes the rotational of the velocity field in Fourier space
 * @param *VELOCITY0 input: velocity field
 * @param *VELOCITY1 input: velocity field
 * @param *VELOCITY2 input: velocity field
 * @param *ROTV0 output: rotational of V component x
 * @param *ROTV1 output: rotational of V component y
 * @param *ROTV2 output: rotational of V component z
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 * @param L0 box size x
 * @param L1 box size y
 * @param L2 box size z
 */
void get_rotV_fs(const float_t *VELOCITY0, const float_t *VELOCITY1, const float_t *VELOCITY2, float_t *ROTV0, float_t *ROTV1, float_t *ROTV2, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2)
{
    int Nthreads = omp_get_max_threads();
    sprintf(G__msg__, "Getting divergence of the velocity field in Fourier space (using %d cores)...", Nthreads);
    PrintMessage(4, G__msg__);
    INDENT;

    rotational_of_vector(VELOCITY0, VELOCITY1, VELOCITY2, ROTV0, ROTV1, ROTV2, false, N0, N1, N2, L0, L1, L2);

    UNINDENT;
    sprintf(G__msg__, "Getting divergence of the velocity field in Fourier space (using %d cores) done.", Nthreads);
    PrintMessage(4, G__msg__);
} //get_rotV_fs
///-------------------------------------------------------------------------------------
