///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/src/debug.c
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file debug.c
 *  \brief Generic routines for debugging
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2013-2023
 */
#include "debug.h"
///-------------------------------------------------------------------------------------
/** @fn debugPrintDiagnostic_
 * Prints a message only in debug mode
 * \note For debug mode only
 */
void debugPrintDiagnostic_(char *message, char *file, char *func, int line)
{
    #if DEBUG
        PrintDiagnostic_(DEBUG_VERBOSITY, message, file, func, line);
    #endif
} //debugPrintDiagnostic
/// An alias for this function
void debugPrintMessage_(char *message, char *file, char *func, int line){ debugPrintDiagnostic_(message, file, func, line); }
///-------------------------------------------------------------------------------------
/** @fn debugPrintStatus
 * Prints status of the debug mode
 * \note For debug mode only
 */
void debugPrintStatus(void)
{
    debugPrintDiagnostic("Debug mode ON");
} //debugPrintStatus
///-------------------------------------------------------------------------------------
/** @fn debugHelloWorld
 * Prints a "Hello World!" in debug mode
 * \note For debug mode only
 */
void debugHelloWorld(void)
{
    debugPrintDiagnostic("Hello World!");
}
///-------------------------------------------------------------------------------------
/** @fn debugPrintInt_
 * Prints an integer
 * \note For debug mode only
 */
void debugPrintInt_(char *name, int a)
{
    static int count=1;
    #if DEBUG
        G__ind__++;
        if(count <= DEBUG_MAX_CALLS)
        {
            sprintf(G__msg__, "%s=%d", name, a);
            debugPrintDiagnostic(G__msg__);
        }
        G__ind__--;
    #endif
    count++;
} //debugPrintInt_
///-------------------------------------------------------------------------------------
/** @fn debugPrintUnsignedInt_
 * Prints an unsigned integer
 * \note For debug mode only
 */
void debugPrintUnsignedInt_(char *name, unsigned int a)
{
    debugPrintInt_(name, (int) a);
} //debugPrintUnsignedInt_
///-------------------------------------------------------------------------------------
/** @fn debugPrintLong_
 * Prints a long integer
 * \note For debug mode only
 */
void debugPrintLong_(char *name, long l)
{
    static int count=1;
    #if DEBUG
        G__ind__++;
        if(count <= DEBUG_MAX_CALLS)
        {
            sprintf(G__msg__, "%s=%ld", name, l);
            debugPrintDiagnostic(G__msg__);
        }
        G__ind__--;
    #endif
    count++;
} //debugPrintLong_
///-------------------------------------------------------------------------------------
/** @fn debugPrintLongLong_
 * Prints a long long integer
 * \note For debug mode only
 */
void debugPrintLongLong_(char *name, long long l)
{
    static int count=1;
    #if DEBUG
        G__ind__++;
        if(count <= DEBUG_MAX_CALLS)
        {
            sprintf(G__msg__, "%s=%lld", name, l);
            debugPrintDiagnostic(G__msg__);
        }
        G__ind__--;
    #endif
    count++;
} //debugPrintLongLong_
///-------------------------------------------------------------------------------------
/** @fn debugPrintUInt64_
 * Prints a long long integer
 * \note For debug mode only
 */
void debugPrintUInt64_(char *name, uint64_t l)
{
    static int count=1;
    #if DEBUG
        G__ind__++;
        if(count <= DEBUG_MAX_CALLS)
        {
            sprintf(G__msg__, "%s=%ju", name, l);
            debugPrintDiagnostic(G__msg__);
        }
        G__ind__--;
    #endif
    count++;
} //debugPrintUInt64_
///-------------------------------------------------------------------------------------
/** @fn debugPrintBool_
 * Prints a boolean
 * \note For debug mode only
 */
void debugPrintBool_(char *name, bool b, bool expected)
{
    static int count=1;
    #if DEBUG
        G__ind__++;
        if(count <= DEBUG_MAX_CALLS)
        {
            if(b == expected) sprintf(G__msg__, "%s=%s%s%s", name, FONT_GREEN, b ? "true" : "false", FONT_NORMAL);
            else sprintf(G__msg__, "%s=%s%s%s", name, FONT_RED, b ? "true" : "false", FONT_NORMAL);

            debugPrintDiagnostic(G__msg__);
        }
        G__ind__--;
    #endif
    count++;
} //debugPrintBool_
///-------------------------------------------------------------------------------------
/** @fn debugPrintFloat_
 * Prints a float
 * \note For debug mode only
 */
void debugPrintFloat_(char *name, float f)
{
    static int count=1;
    #if DEBUG
        G__ind__++;
        if(count <= DEBUG_MAX_CALLS)
        {
            sprintf(G__msg__, "%s=%g", name, f);
            debugPrintDiagnostic(G__msg__);
        }
        G__ind__--;
    #endif
    count++;
} //debugPrintFloat_
///-------------------------------------------------------------------------------------
/** @fn debugPrintDouble_
 * Prints a double
 * \note For debug mode only
 */
void debugPrintDouble_(char *name, double d)
{
    static int count=1;
    #if DEBUG
        G__ind__++;
        if(count <= DEBUG_MAX_CALLS)
        {
            sprintf(G__msg__, "%s=%f", name, d);
            debugPrintDiagnostic(G__msg__);
        }
        G__ind__--;
    #endif
    count++;
} //debugPrintDouble_
///-------------------------------------------------------------------------------------
/** @fn debugPrintFloatt_
 * Prints a float_t
 * \note For debug mode only
 */
void debugPrintFloatt_(char *name, float_t f)
{
    #ifdef DOUBLEPRECISION
    debugPrintFloat_(name,f);
    #else
    debugPrintDouble_(name,f);
    #endif
} //debugPrintFloatt_
///-------------------------------------------------------------------------------------
/** @fn debugPrintChar_
 * Prints a character
 * \note For debug mode only
 */
void debugPrintChar_(char *name, char c)
{
    static int count=1;
    #if DEBUG
        G__ind__++;
        if(count <= DEBUG_MAX_CALLS)
        {
            sprintf(G__msg__, "%s=%c", name, c);
            debugPrintDiagnostic(G__msg__);
        }
        G__ind__--;
    #endif
    count++;
} //debugPrintChar_
///-------------------------------------------------------------------------------------
/** @fn debugPrintString_
 * Prints a string
 * \note For debug mode only
 */
void debugPrintString_(char *name, char *s)
{
    static int count=1;
    #if DEBUG
        G__ind__++;
        if(count <= DEBUG_MAX_CALLS)
        {
            sprintf(G__msg__, "%s=%s", name, s);
            debugPrintDiagnostic(G__msg__);
        }
        G__ind__--;
    #endif
    count++;
} //debugPrintString_
///-------------------------------------------------------------------------------------
/** @fn debugPrintParticleIDt_
 * Prints a character
 * \note For debug mode only
 */
void debugPrintParticleIDt_(char *name, particleID_t k)
{
    static int count=1;
    #if DEBUG
        G__ind__++;
        if(count <= DEBUG_MAX_CALLS)
        {
            #if LONGIDS
                sprintf(G__msg__, "%s=%llu", name, k);
            #else
                sprintf(G__msg__, "%s=%u", name, k);
            #endif
            debugPrintDiagnostic(G__msg__);
        }
        G__ind__--;
    #endif
    count++;
} //debugPrintParticleIDt_
///-------------------------------------------------------------------------------------
/** @fn debugPrintIntArray_
 * Prints an array of integer
 * \note For debug mode only
 */
void debugPrintIntArray_(char *name, int *a, int length)
{
    static int count=1;
    #if DEBUG
        G__ind__++;
        if(count <= DEBUG_MAX_CALLS)
        {
            sprintf(G__msg__, "%s=[", name);
            char __msg2__[100];
            int MAX=min(DEBUG_MAX_ARRAY_ELEMENTS, length);
            for(int i=0; i<MAX-1; i++)
            {
                sprintf(__msg2__, "%d ", a[i]);
                strcat(G__msg__, __msg2__);
            }
            if(length>=DEBUG_MAX_ARRAY_ELEMENTS)
            {
                sprintf(__msg2__, "%d ...]", a[MAX-1]);
                strcat(G__msg__, __msg2__);
            }
            else
            {
                sprintf(__msg2__, "%d]", a[MAX-1]);
                strcat(G__msg__, __msg2__);
            }
            debugPrintDiagnostic(G__msg__);
        }
        G__ind__--;
    #endif
    count++;
} //debugPrintIntArray_
///-------------------------------------------------------------------------------------
/** @fn debugPrintUnsignedIntArray_
 * Prints an array of integer
 * \note For debug mode only
 */
void debugPrintUnsignedIntArray_(char *name, unsigned int *a, int length)
{
    debugPrintIntArray_(name, (int*) a, length);
} //debugPrintUnsignedIntArray_
///-------------------------------------------------------------------------------------
/** @fn debugPrintLongArray_
 * Prints an array of long
 * \note For debug mode only
 */
void debugPrintLongArray_(char *name, long *l, int length)
{
    static int count=1;
    #if DEBUG
        G__ind__++;
        if(count <= DEBUG_MAX_CALLS)
        {
            sprintf(G__msg__, "%s=[", name);
            char __msg2__[100];
            int MAX=min(DEBUG_MAX_ARRAY_ELEMENTS, length);
            for(int i=0; i<MAX-1; i++)
            {
                sprintf(__msg2__, "%ld ", l[i]);
                strcat(G__msg__, __msg2__);
            }
            if(length>=DEBUG_MAX_ARRAY_ELEMENTS)
            {
                sprintf(__msg2__, "%ld ...]", l[MAX-1]);
                strcat(G__msg__, __msg2__);
            }
            else
            {
                sprintf(__msg2__, "%ld]", l[MAX-1]);
                strcat(G__msg__, __msg2__);
            }
            debugPrintDiagnostic(G__msg__);
        }
        G__ind__--;
    #endif
    count++;
} //debugPrintLongArray_
///-------------------------------------------------------------------------------------
/** @fn debugPrintLongLongArray_
 * Prints an array of long long
 * \note For debug mode only
 */
void debugPrintLongLongArray_(char *name, long long *l, int length)
{
    static int count=1;
    #if DEBUG
        G__ind__++;
        if(count <= DEBUG_MAX_CALLS)
        {
            sprintf(G__msg__, "%s=[", name);
            char __msg2__[100];
            int MAX=min(DEBUG_MAX_ARRAY_ELEMENTS, length);
            for(int i=0; i<MAX-1; i++)
            {
                sprintf(__msg2__, "%lld ", l[i]);
                strcat(G__msg__, __msg2__);
            }
            if(length>=DEBUG_MAX_ARRAY_ELEMENTS)
            {
                sprintf(__msg2__, "%lld ...]", l[MAX-1]);
                strcat(G__msg__, __msg2__);
            }
            else
            {
                sprintf(__msg2__, "%lld]", l[MAX-1]);
                strcat(G__msg__, __msg2__);
            }
            debugPrintDiagnostic(G__msg__);
        }
        G__ind__--;
    #endif
    count++;
} //debugPrintLongLongArray_
///-------------------------------------------------------------------------------------
/** @fn debugPrintUInt64Array_
 * Prints an array of uint64_t
 * \note For debug mode only
 */
void debugPrintUInt64Array_(char *name, uint64_t *l, int length)
{
    static int count=1;
    #if DEBUG
        G__ind__++;
        if(count <= DEBUG_MAX_CALLS)
        {
            sprintf(G__msg__, "%s=[", name);
            char __msg2__[100];
            int MAX=min(DEBUG_MAX_ARRAY_ELEMENTS, length);
            for(int i=0; i<MAX-1; i++)
            {
                sprintf(__msg2__, "%ju ", l[i]);
                strcat(G__msg__, __msg2__);
            }
            if(length>=DEBUG_MAX_ARRAY_ELEMENTS)
            {
                sprintf(__msg2__, "%ju ...]", l[MAX-1]);
                strcat(G__msg__, __msg2__);
            }
            else
            {
                sprintf(__msg2__, "%ju]", l[MAX-1]);
                strcat(G__msg__, __msg2__);
            }
            debugPrintDiagnostic(G__msg__);
        }
        G__ind__--;
    #endif
    count++;
} //debugPrintUInt64Array_
///-------------------------------------------------------------------------------------
/** @fn debugPrintBoolArray_
 * Prints an array of bool
 * \note For debug mode only
 */
void debugPrintBoolArray_(char *name, bool *b, int length)
{
    static int count=1;
    #if DEBUG
        G__ind__++;
        if(count <= DEBUG_MAX_CALLS)
        {
            sprintf(G__msg__, "%s=[", name);
            char __msg2__[100];
            int MAX=min(DEBUG_MAX_ARRAY_ELEMENTS, length);
            for(int i=0; i<MAX-1; i++)
            {
                if(b[i]) sprintf(__msg2__, "true ");
                else sprintf(__msg2__, "false ");
                strcat(G__msg__, __msg2__);
            }
            if(length>=DEBUG_MAX_ARRAY_ELEMENTS)
            {
                if(b[MAX-1]) sprintf(__msg2__, "true ...]");
                else sprintf(__msg2__, "false ...]");
                strcat(G__msg__, __msg2__);
            }
            else
            {
                if(b[MAX-1]) sprintf(__msg2__, "true]");
                else sprintf(__msg2__, "false]");
                strcat(G__msg__, __msg2__);
            }
            debugPrintDiagnostic(G__msg__);
        }
        G__ind__--;
    #endif
    count++;
} //debugPrintBoolArray_
///-------------------------------------------------------------------------------------
/** @fn debugPrintFloatArray_
 * Prints an array of float
 * \note For debug mode only
 */
void debugPrintFloatArray_(char *name, float *f, int length)
{
    static int count=1;
    #if DEBUG
        G__ind__++;
        if(count <= DEBUG_MAX_CALLS)
        {
            sprintf(G__msg__, "%s=[", name);
            char __msg2__[100];
            int MAX=min(DEBUG_MAX_ARRAY_ELEMENTS, length);
            for(int i=0; i<MAX-1; i++)
            {
                sprintf(__msg2__, "%g ", f[i]);
                strcat(G__msg__, __msg2__);
            }
            if(length>=DEBUG_MAX_ARRAY_ELEMENTS)
            {
                sprintf(__msg2__, "%g ...]", f[MAX-1]);
                strcat(G__msg__, __msg2__);
            }
            else
            {
                sprintf(__msg2__, "%g]", f[MAX-1]);
                strcat(G__msg__, __msg2__);
            }
            debugPrintDiagnostic(G__msg__);
        }
        G__ind__--;
    #endif
    count++;
} //debugPrintFloatArray_
///-------------------------------------------------------------------------------------
/** @fn debugPrintDoubleArray_
 * Prints an array of double
 * \note For debug mode only
 */
void debugPrintDoubleArray_(char *name, double *d, int length)
{
    static int count=1;
    #if DEBUG
        G__ind__++;
        if(count <= DEBUG_MAX_CALLS)
        {
            sprintf(G__msg__, "%s=[", name);
            char __msg2__[100];
            int MAX=min(DEBUG_MAX_ARRAY_ELEMENTS, length);
            for(int i=0; i<MAX-1; i++)
            {
                sprintf(__msg2__, "%lg ", d[i]);
                strcat(G__msg__, __msg2__);
            }
            if(length>=DEBUG_MAX_ARRAY_ELEMENTS)
            {
                sprintf(__msg2__, "%lg ...]", d[MAX-1]);
                strcat(G__msg__, __msg2__);
            }
            else
            {
                sprintf(__msg2__, "%lg]", d[MAX-1]);
                strcat(G__msg__, __msg2__);
            }
            debugPrintDiagnostic(G__msg__);
        }
        G__ind__--;
    #endif
    count++;
} //debugPrintDoubleArray_
///-------------------------------------------------------------------------------------
/** @fn debugPrintFloattArray_
 * Prints an array of double
 * \note For debug mode only
 */
void debugPrintFloattArray_(char *name, float_t *f, int length)
{
    #ifdef DOUBLEPRECISION
    debugPrintDoubleArray_(name, f, length);
    #else
    debugPrintFloatArray_(name, f, length);
    #endif
} //debugPrintFloattArray_
///-------------------------------------------------------------------------------------
/** @fn debugPrintStringArray_
 * Prints an array of string
 * \note For debug mode only
 */
void debugPrintStringArray_(char *name, char **s, int length)
{
    static int count=1;
    #if DEBUG
        G__ind__++;
        if(count <= DEBUG_MAX_CALLS)
        {
            sprintf(G__msg__, "%s=[", name);
            char __msg2__[100];
            int MAX=min(DEBUG_MAX_ARRAY_ELEMENTS, length);
            for(int i=0; i<MAX-1; i++)
            {
                sprintf(__msg2__, "%s ", s[i]);
                strcat(G__msg__, __msg2__);
            }
            if(length>=DEBUG_MAX_ARRAY_ELEMENTS)
            {
                sprintf(__msg2__, "%s ...]", s[MAX-1]);
                strcat(G__msg__, __msg2__);
            }
            else
            {
                sprintf(__msg2__, "%s]", s[MAX-1]);
                strcat(G__msg__, __msg2__);
            }
            debugPrintDiagnostic(G__msg__);
        }
        G__ind__--;
    #endif
    count++;
} //debugPrintStringArray_
///-------------------------------------------------------------------------------------
/** @fn debugPrintParticleIDtArray_
 * Prints an array of particleID_t
 * \note For debug mode only
 */
void debugPrintParticleIDtArray_(char *name, particleID_t *k, int length)
{
    #ifdef LONGIDS
    debugPrintIntArray_(name, (int*) k, length);
    #else
    debugPrintLongLongArray_(name, (long long*) k, length);
    #endif
} //debugPrintParticleIDtArray_
///-------------------------------------------------------------------------------------
