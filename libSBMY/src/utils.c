///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/src/utils.c
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file utils.c
 *  \brief Generic infrastructure routines
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2013-2023
 */
#include "utils.h"
///-------------------------------------------------------------------------------------
/** Definition of some global variables
 * \note For use in debug mode only
 */
#ifdef TIMERS
        CPUtime startm_simbelmyne, stopm_simbelmyne; double diff_t_cpu_simbelmyne;
        Walltime startw_simbelmyne, stopw_simbelmyne; double diff_t_wall_simbelmyne;
        CPUtime startm_module, stopm_module; double diff_t_cpu_module;
        Walltime startw_module, stopw_module; double diff_t_wall_module;
#endif
char G__msg__[5000]="";
int G__ind__=0;
int G__count_malloc__=0;
int G__count_free__=0;
FILE *stdlog; bool ISSET_LOGS;
///-------------------------------------------------------------------------------------
/** @fn exists
 * Tests if a file exists
 * @param *file
 * @return true if file exists, else false
 */
bool exists(const char *file)
{
    FILE *fp = NULL;
    fp = fopen(file, "r");
    if(fp == NULL)
    {
        return false;
    }
    fclose(fp);
    return true;
} //exists
///-------------------------------------------------------------------------------------
/** @fn p_mod
 * Replaces function modulo, treats differently negative values
 * @param x
 * @param y should be positive, else replaced by -y
 * @return value between 0 and y
 */
double p_mod(const double x, const double y)
{
    if(y==0.)
        return 0.;
    double yy = fabs(y);

    return fmod(x+yy,yy);
} //p_mod
///-------------------------------------------------------------------------------------
/** @fn p_malloc_
 * Replaces function malloc
 * @param size same as malloc
 * @param *name name of the variable
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 * @return same as malloc
 * \note Special behaviour in debug mode
 */
void* p_malloc_(const size_t size, char *name, char *file, char *func, int line)
{
    void *result;
    if( (result = malloc(size)) == NULL)
    {
        char buf[BUFLENGTH];
        FormatFunction_(buf, file, func, line);
        sprintf(G__msg__, "Memory allocation failed (%llu bytes) for %s in %s%s.", (unsigned long long)size, name, buf, FONT_LIGHTRED);
        FatalError(G__msg__);
    }
    #if DEBUG
        #pragma omp critical
        {
            G__count_malloc__++;
            char buf[BUFLENGTH];
            FormatFunction_(buf, file, func, line);
            sprintf(G__msg__, "Correctly allocated memory (%llu bytes) at %p for %s in %s%s.", (unsigned long long)size, result, name, buf, FONT_GREY);
            PrintMemory(G__msg__);
        }
    #endif
    return result;
} //p_malloc_
///-------------------------------------------------------------------------------------
/** @fn p_calloc_
 * Replaces function calloc
 * @param nitems same as calloc
 * @param size same as calloc
 * @param *name name of the variable
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 * @return same as calloc
 * \note Special behaviour in debug mode
 */
void* p_calloc_(const size_t nitems, const size_t size, char *name, char *file, char *func, int line)
{
    void *result;
    if( (result = calloc(nitems, size)) == NULL)
    {
        char buf[BUFLENGTH];
        FormatFunction_(buf, file, func, line);
        sprintf(G__msg__, "Memory allocation failed (%llu bytes) for %s in %s%s.", (unsigned long long)nitems*size, name, buf, FONT_LIGHTRED);
        FatalError(G__msg__);
    }
    #if DEBUG
        #pragma omp critical
        {
            G__count_malloc__++;
            char buf[BUFLENGTH];
            FormatFunction_(buf, file, func, line);
            sprintf(G__msg__, "Correctly allocated and initialized memory (%llu bytes) at %p for %s in %s%s.", (unsigned long long)nitems*size, result, name, buf, FONT_GREY);
            PrintMemory(G__msg__);
        }
    #endif
    return result;
} //p_calloc_
///-------------------------------------------------------------------------------------
/** @fn p_fftw_malloc_
 * Replaces function fftw_malloc
 * @param size same as fftw_malloc
 * @param *name name of the variable
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 * @return same as fftw_malloc
 * \note Special behaviour in debug mode
 */
void* p_fftw_malloc_(const size_t size, char *name, char *file, char *func, int line)
{
    void *result;
    #ifdef DOUBLEPRECISION_FFTW
    if( (result = fftw_malloc(size)) == NULL)
    #else
    if( (result = fftwf_malloc(size)) == NULL)
    #endif
    {
        char buf[BUFLENGTH];
        FormatFunction_(buf, file, func, line);
        sprintf(G__msg__, "Memory allocation failed (%llu bytes) for %s in %s%s.", (unsigned long long)size, name, buf, FONT_LIGHTRED);
        FatalError(G__msg__);
    }
    #if DEBUG
        #pragma omp critical
        {
            G__count_malloc__++;
            char buf[BUFLENGTH];
            FormatFunction_(buf, file, func, line);
            sprintf(G__msg__, "Correctly allocated memory (%llu bytes) at %p for %s in %s%s.", (unsigned long long)size, result, name, buf, FONT_GREY);
            PrintMemory(G__msg__);
        }
    #endif
    return result;
} //p_fftw_malloc_
///-------------------------------------------------------------------------------------
/** @fn p_free_
 * Replaces function free
 * @param pointer same as free
 * @param *name name of the variable
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 * \note Special behaviour in debug mode
 */
void p_free_(void *pointer, char *name, char *file, char *func, int line)
{
    if(pointer != NULL) // avoid double free
        free(pointer);
    #if DEBUG
        #pragma omp critical
        {
            G__count_free__++;
            char buf[BUFLENGTH];
            FormatFunction_(buf, file, func, line);
            sprintf(G__msg__,"Correctly freed memory allocated at %p for %s in %s%s.", pointer, name, buf, FONT_GREY);
            PrintMemory(G__msg__);
        }
    #endif
} //p_free_
///-------------------------------------------------------------------------------------
/** @fn p_fftw_free_
 * Replaces function free
 * @param pointer same as free
 * @param *name name of the variable
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 * \note Special behaviour in debug mode
 */
void p_fftw_free_(void *pointer, char *name, char *file, char *func, int line)
{
    #ifdef DOUBLEPRECISION_FFTW
    if(pointer != NULL) // avoid double free
        fftw_free(pointer);
    #else
    if(pointer != NULL) // avoid double free
        fftwf_free(pointer);
    #endif
    #if DEBUG
        #pragma omp critical
        {
            G__count_free__++;
            char buf[BUFLENGTH];
            FormatFunction_(buf, file, func, line);
            sprintf(G__msg__,"Correctly freed memory allocated at %p for %s in %s%s.", pointer, name, buf, FONT_GREY);
            PrintMemory(G__msg__);
        }
    #endif
} //p_fftw_free_
///-------------------------------------------------------------------------------------
/** @fn get_Nthreads_from_available_memory_
 * Get the possible number of threads for a memory consuming task,
 * given the available memory
 * @param nitems number of items per thread
 * @param size size of one item
 * @param Nthreads_max maximum number of threads to be used
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 * @return Nthreads to be used in parallel section
 */
int get_Nthreads_from_available_memory_(const long long nitems, const long long size, const int Nthreads_max, char *file, char *func, int line)
{
    // check available memory from system information
    struct sysinfo info;
    if(sysinfo(&info) != 0)
    {
        char buf[BUFLENGTH];
        FormatFunction_(buf, file, func, line);
        sprintf(G__msg__, "Unable to access system information in %s%s.", buf, FONT_LIGHTRED);
        FatalError(G__msg__);
    }
    unsigned long freeram = info.freeram * info.mem_unit;
    unsigned long bufferram = info.bufferram * info.mem_unit;
    unsigned long totalavailram = (freeram + bufferram - 10*8*1024*1024); // ensure that at least 10MB will remain

    // get the number of available arrays to be allocated
    double Nthreads_double = totalavailram / (nitems*size);
    int Nthreads_alloc = (int)floor(Nthreads_double);

    // for more safety, decrease the number of threads by 1 if less than 10% of the size of an array would remain
    if(Nthreads_double - Nthreads_alloc < 0.10) Nthreads_alloc--;

    // check for the maximum number of threads specified by the user
    int Nthreads = Nthreads_alloc>Nthreads_max ? Nthreads_max : Nthreads_alloc;

    #if DEBUG
        char buf[BUFLENGTH];
        FormatFunction_(buf, file, func, line);
        sprintf(G__msg__,"Given the available memory, %d threads could be allocated and %d threads will be used in %s%s.", Nthreads_alloc, Nthreads, buf, FONT_GREY);
        PrintMemory(G__msg__);
    #endif

    return Nthreads;
} //get_Nthreads_from_available_memory_
///-------------------------------------------------------------------------------------
/** @fn p_fwrite_
 * Replaces function fwrite
 * @param *ptr same as fwrite
 * @param size same as fwrite
 * @param nmemb same as fwrite
 * @param *stream same as fwrite
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 * @return same as fwrite
 */
size_t p_fwrite_(void *ptr, size_t size, size_t nmemb, FILE *stream, char *file, char *func, int line)
{
    size_t nwrite;
    if((nwrite = fwrite(ptr, size, nmemb, stream)) != nmemb)
    {
        char buf[BUFLENGTH];
        FormatFunction_(buf, file, func, line);
        sprintf(G__msg__, "I/O error (fwrite): incorrect writing in %s%s.", buf, FONT_LIGHTRED);
        FatalError(G__msg__);
    }
    return nwrite;
} //p_fwrite_
///-------------------------------------------------------------------------------------
/** @fn p_fread_
 * Replaces function fread
 * @param *ptr same as fread
 * @param size same as fread
 * @param nmemb same as fread
 * @param *stream same as fread
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 * @return same as fread
 */
size_t p_fread_(void *ptr, size_t size, size_t nmemb, FILE *stream, char *file, char *func, int line)
{
    size_t nread;
    if((nread = fread(ptr, size, nmemb, stream)) != nmemb)
    {
        char buf[BUFLENGTH];
        FormatFunction_(buf, file, func, line);
        sprintf(G__msg__, "I/O error (fread): incorrect reading in %s%s.", buf, FONT_LIGHTRED);
        FatalError(G__msg__);
    }
    return nread;
} //p_fread_
///-------------------------------------------------------------------------------------
/** @fn p_fgets_
 * Replaces function fgets
 * @param *s same as fgets
 * @param size same as fgets
 * @param *stream same as fgets
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 * @return same as fgets
 */
char* p_fgets_(char *s, int size, FILE *stream, char *file, char *func, int line)
{
    char *result = fgets(s, size, stream);
    if(!feof(stream) && result == NULL)
    {
        char buf[BUFLENGTH];
        FormatFunction_(buf, file, func, line);
        sprintf(G__msg__, "Incorrect reading in %s%s.", buf, FONT_LIGHTRED);
        PrintError(G__msg__);
        sprintf(G__msg__, "Attempting to read: '%s'%s.", s, FONT_LIGHTRED);
        PrintError(G__msg__);
        FatalError("I/O error (fgets)");
    }
    return result;
} //p_fgets_
///-------------------------------------------------------------------------------------
/** @fn strreplace
 * Replaces every occurence of a character
 * in a string by another character
 * @param *str the string
 * @param in character to be replaced
 * @param out character to replace with
 * @return the updated string
 */
char* strreplace(char *str, char in, char out)
{
    for(int i=0; i<(int)strlen(str); i++)
    {
        if((char)str[i] == in)
        {
            str[i] = out;
        }
    }
    return str;
} //strreplace
///-------------------------------------------------------------------------------------
/** @fn strremovechar
 * Removes every occurence of a character
 * in a string
 * @param *str the string
 * @param in character to be replaced
 * @return the updated string
 */
void strremovechar(char *str, char in)
{
    int writer = 0, reader = 0;
    while(str[reader])
    {
        if(str[reader] != in)
        {
        str[writer++] = str[reader];
        }

        reader++;
    }
    str[writer]=0;
} //strremovechar
///-------------------------------------------------------------------------------------
/** @fn file_compare_open
 * Compares two files already open
 * @param *file1 first file
 * @param *file2 second file
 * @return false if files are the same, else true
 */
static bool file_compare_open(FILE *file1, FILE *file2)
{
    const int LENGTH = 500;
    char buf1[LENGTH], buf2[LENGTH];
    while(fgets(buf1, LENGTH, file1) != NULL && fgets(buf2, LENGTH, file2) != NULL)
        if( strcmp(buf1, buf2) != 0 )
            return true;
    return false;
} //file_compare_open
///-------------------------------------------------------------------------------------
/** @fn file_compare_str
 * Compares two files
 * @param *n1 name of first file
 * @param *n2 name of second file
 * @return false if files are the same, else true
 */
bool file_compare_str(const char *n1, const char *n2)
{
    FILE *f1 = NULL, *f2 = NULL;

    if( (f1 = fopen(n1, "r")) == NULL )
    {
        sprintf(G__msg__, "Reading error with file '%s'", n1);
        FatalError(G__msg__);
    }

    if( (f2 = fopen(n2, "r")) == NULL )
    {
        sprintf(G__msg__, "Reading error with file '%s'", n2);
        FatalError(G__msg__);
    }

    int res = file_compare_open(f1, f2);

    fclose(f1);
    fclose(f2);

    return res;
} //file_compare_str
///-------------------------------------------------------------------------------------
/** @fn compareDoubleArray
 * Compares two arrays of double
 * @param *t1 first array
 * @param *t2 second array
 * @param N length
 * @return true/false
 */
bool compareDoubleArray(const double *t1, const double *t2, const int N)
{
    bool res = true;

    for(int i=0; i<N; i++)
        if((int)t1[i] != (int)t2[i])
            res = false;

    return res;
} //compareDoubleArray
///-------------------------------------------------------------------------------------
/** @fn change_rm2cm
 * Change row major to column major in 3D arrays
 * @param *A input array, row major
 * @param *B output array, column major
 * @param N0 mesh size x
 * @param N1 mesh size y
 * @param N2 mesh size z
 */
void change_rm2cm(const double *A, double *B, const int N0, const int N1, const int N2)
{
    sprintf(G__msg__, "change_rm2cm: all arrays should be row major, so this routine should normally not be used.");
    PrintWarning(G__msg__);

    #pragma omp parallel for schedule(static) collapse(3)
    for(int i=0; i<N0; i++)
        for(int j=0; j<N1; j++)
            for(int k=0; k<N2; k++)
            {
                B[i+N0*(j+N1*k)]=A[k+N2*(j+N1*i)];
            }
}//change_rm2cm
///-------------------------------------------------------------------------------------
/** @fn get_longlong_indices
 * Get mapping from 1D array to 3D array
 * @param Id 1D index
 * @param N0 array size x
 * @param N1 array size y
 * @param N2 array size z
 * @param *indices output indices
 * \warning Assumes row major!
 */
void get_longlong_indices(const long long Id, const int N0, const int N1, const int N2, int *indices)
{
    // assumes Id = k+N2*(j+N1*i)
    int i = p_mod(Id/(N1*N2),N0);
    int j = p_mod((Id-N1*N2*i)/N2,N1);
    int k = p_mod((Id-N2*j-N2*N1*i),N2);

    indices[0]=i;
    indices[1]=j;
    indices[2]=k;
} //get_longlong_indices
///-------------------------------------------------------------------------------------
/** @fn get_indices
 * Get mapping from 1D array to 3D array
 * @param Id 1D index
 * @param N0 array size x
 * @param N1 array size y
 * @param N2 array size z
 * @param *indices output indices
 * \warning Assumes row major!
 */
void get_indices(const int Id, const int N0, const int N1, const int N2, int *indices)
{
    get_longlong_indices((const long long)Id,N0,N1,N2,indices);
} //get_indices
///-------------------------------------------------------------------------------------
/** @fn get_index
 * Get mapping from 3D to 1D array
 * @param mc0 index x
 * @param mc1 index y
 * @param mc2 index z
 * @param N0 array size x
 * @param N1 array size y
 * @param N2 array size z
 * @return index
 * \warning Assumes row major!
 */
cellIndex_t get_index(const int mc0, const int mc1, const int mc2, const int N0, const int N1, const int N2)
{
    #if DEBUG
    bool mc0_out = (mc0<0 || mc0>=N0); bool mc1_out = (mc1<0 || mc1>=N1); bool mc2_out = (mc2<0 || mc2>=N2);
    if(mc0_out || mc1_out || mc2_out)
    {
        #pragma omp critical
        {
            debugPrintBool(mc0_out,false);
            debugPrintBool(mc1_out,false);
            debugPrintBool(mc2_out,false);
        }
        sprintf(G__msg__, "Attempting to access field value with incorrect indices: mc0=%d, mc1=%d, mc2=%d with N0=%d, N1=%d, N2=%d%s.", mc0, mc1, mc2, N0, N1, N2, FONT_LIGHTRED);
        FatalError(G__msg__);
    }
    #else
    UNUSED(N0);
    #endif
    return (cellIndex_t)mc2+N2*((cellIndex_t)mc1+N1*(cellIndex_t)mc0);
} //get_index
///-------------------------------------------------------------------------------------
/** @fn get_index_in_paddedbox
 * Gets mapping from 3D to 1D array, if the box is padded
 * @param mc0 index x
 * @param mc1 index y
 * @param mc2 index z
 * @param N0 array size x
 * @param N1 array size y
 * @param N2 array size z
 * @param N0_pad number of additional cells to the West
 * @param N1_pad number of additional cells to the South
 * @param N2_pad number of additional cells to the Bottom
 * @return index
 */
cellIndex_t get_index_in_paddedbox(const int mc0, const int mc1, const int mc2, const int N0, const int N1, const int N2, const int N0_pad, const int N1_pad, const int N2_pad)
{
    #if DEBUG
    bool mc0_out = (mc0<-N0_pad || mc0>=N0+N0_pad); bool mc1_out = (mc1<-N1_pad || mc1>=N1+N1_pad); bool mc2_out = (mc2<-N2_pad || mc2>=N2+N2_pad);
    if(mc0_out || mc1_out || mc2_out)
    {
        #pragma omp critical
        {
            debugPrintBool(mc0_out,false);
            debugPrintBool(mc1_out,false);
            debugPrintBool(mc2_out,false);
        }
        sprintf(G__msg__, "Attempting to access field value with incorrect indices: mc0=%d, mc1=%d, mc2=%d with N0=%d, N1=%d, N2=%d, N0_pad=%d, N1_pad=%d, N2_pad=%d%s.", mc0, mc1, mc2, N0, N1, N2, N0_pad, N1_pad, N2_pad, FONT_LIGHTRED);
        FatalError(G__msg__);
    }
    #endif
    return get_index(mc0+N0_pad,mc1+N1_pad,mc2+N2_pad,N0+2*N0_pad,N1+2*N1_pad,N2+2*N2_pad);
} //get_index_in_paddedbox
///-------------------------------------------------------------------------------------
/** @fn FormatFunction_
 * Prints the state of execution on screen
 * @param *buf buffer
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
void FormatFunction_(char *buf, char *file, char *func, int line)
{
    char *pos = strstr(file, "src/");
    if(pos == NULL)
        pos = file;
    else
        pos += 4;

    sprintf(buf, "%s%s%s:%s%s%s:%s%d%s", FONT_YELLOW, pos, FONT_CYAN, FONT_LIGHTERGREEN, func, FONT_CYAN, FONT_GREEN, line, FONT_NORMAL);
} //FormatFunction_
///-------------------------------------------------------------------------------------
/** @fn PrintLeftType
 * Prints left "type of message" inscription on screen
 * @param *std standard output
 * @param *type type of message
 * @param *FONT_COLOR font color
 */
void PrintLeftType(struct _IO_FILE *std, char *type, char *FONT_COLOR)
{
    time_t timer; struct tm* tm_info; time(&timer); tm_info = localtime(&timer); char buf[12];
    strftime(buf, 12, "%H:%M:%S", tm_info);
    fprintf(std, "[");
    fprintf(std, "%s", buf);
    fprintf(std, "%s", FONT_NORMAL);
    fprintf(std, "|");
    fprintf(std, "%s", FONT_COLOR);
    fprintf(std, "%s", type);
    fprintf(std, "%s", FONT_NORMAL);
    fprintf(std, "]");
    for(int ind=0; ind<G__ind__; ind++)
        fprintf(std, "==");
    fprintf(std, "|");
} //PrintLeftType
///-------------------------------------------------------------------------------------
/** @fn PrintBareMessage_
 * Prints a message without console formatting
 * @param *message message
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
void PrintBareMessage_(char *message, char *file, char *func, int line)
{
    UNUSED(file); UNUSED(func); UNUSED(line);
    if(SCREEN_VERBOSE_LEVEL>0)
    {
        fprintf(stdout, "%s", message);
        fflush(stdout);
    }
    if(ISSET_LOGS && LOGS_VERBOSE_LEVEL>0)
    {
        fprintf(stdlog, "%s", message);
        fflush(stdlog);
    }
} //PrintBareMessage_
///-------------------------------------------------------------------------------------
/** @fn PrintMessage_
 * Prints a message
 * @param verbosity verbosity level of the message
 * @param *message message
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
void PrintMessage_(int verbosity, char *message, char *file, char *func, int line)
{
    UNUSED(file); UNUSED(func); UNUSED(line);
    if(SCREEN_VERBOSE_LEVEL>=verbosity)
    {
        PrintLeftType(stdout, "STATUS    ", FONT_LIGHTGREEN);
        fprintf(stdout, "%s\n", message);
        fflush(stdout);
    }
    if(ISSET_LOGS && LOGS_VERBOSE_LEVEL>=verbosity)
    {
        PrintLeftType(stdlog, "STATUS    ", FONT_LIGHTGREEN);
        fprintf(stdlog, "%s\n", message);
        fflush(stdlog);
    }
} //PrintMessage_
///-------------------------------------------------------------------------------------
/** @fn PrintMessageNoBreak_
 * Prints a message (no endline)
 * @param verbosity verbosity level of the message
 * @param *message message
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
void PrintMessageNoBreak_(int verbosity, char *message, char *file, char *func, int line)
{
    UNUSED(file); UNUSED(func); UNUSED(line);
    if(SCREEN_VERBOSE_LEVEL>=verbosity)
    {
        PrintLeftType(stdout, "STATUS    ", FONT_LIGHTGREEN);
        fprintf(stdout, "%s", message);
        fflush(stdout);
    }
    if(ISSET_LOGS && LOGS_VERBOSE_LEVEL>=verbosity)
    {
        PrintLeftType(stdlog, "STATUS    ", FONT_LIGHTGREEN);
        fprintf(stdlog, "%s", message);
        fflush(stdlog);
    }
} //PrintMessageNoBreak_
///-------------------------------------------------------------------------------------
/** @fn PrintInfo_
 * Prints an info message
 * @param *message message
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
void PrintInfo_(char *message, char *file, char *func, int line)
{
    UNUSED(file); UNUSED(func); UNUSED(line);
    if(SCREEN_VERBOSE_LEVEL>=INFO_VERBOSITY)
    {
        PrintLeftType(stdout, "INFO      ", FONT_LIGHTCYAN);
        fprintf(stdout, "%s\n", message);
        fflush(stdout);
    }
    if(ISSET_LOGS && LOGS_VERBOSE_LEVEL>=INFO_VERBOSITY)
    {
        PrintLeftType(stdlog, "INFO      ", FONT_LIGHTCYAN);
        fprintf(stdlog, "%s\n", message);
        fflush(stdlog);
    }
} //PrintInfo_
///-------------------------------------------------------------------------------------
/** @fn PrintInfoNoBreak_
 * Prints an info message (no endline)
 * @param *message message
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
void PrintInfoNoBreak_(char *message, char *file, char *func, int line)
{
    UNUSED(file); UNUSED(func); UNUSED(line);
    if(SCREEN_VERBOSE_LEVEL>=INFO_VERBOSITY)
    {
        PrintLeftType(stdout, "INFO      ", FONT_LIGHTCYAN);
        fprintf(stdout, "%s\n", message);
        fflush(stdout);
    }
    if(ISSET_LOGS && LOGS_VERBOSE_LEVEL>=INFO_VERBOSITY)
    {
        PrintLeftType(stdlog, "INFO      ", FONT_LIGHTCYAN);
        fprintf(stdlog, "%s\n", message);
        fflush(stdlog);
    }
} //PrintInfoNoBreak_
///-------------------------------------------------------------------------------------
/** @fn PrintModule_
 * Prints a module message
 * @param *message message
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
void PrintModule_(char *message, char *file, char *func, int line)
{
    UNUSED(file); UNUSED(func); UNUSED(line);
    if(SCREEN_VERBOSE_LEVEL>=MODULE_VERBOSITY)
    {
        PrintLeftType(stdout, "MODULE    ", FONT_LIGHTPURPLE);
        fprintf(stdout, FONT_LIGHTPURPLE);
        fprintf(stdout, "%s", message);
        fprintf(stdout, FONT_NORMAL);
        fprintf(stdout, "\n");
        fflush(stdout);
    }
    if(ISSET_LOGS && LOGS_VERBOSE_LEVEL>=MODULE_VERBOSITY)
    {
        PrintLeftType(stdlog, "MODULE    ", FONT_LIGHTPURPLE);
        fprintf(stdlog, FONT_LIGHTPURPLE);
        fprintf(stdlog, "%s", message);
        fprintf(stdlog, FONT_NORMAL);
        fprintf(stdlog, "\n");
        fflush(stdlog);
    }
} //PrintModule_
///-------------------------------------------------------------------------------------
/** @fn PrintModuleNoBreak_
 * Prints a module message (no endline)
 * @param *message message
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
void PrintModuleNoBreak_(char *message, char *file, char *func, int line)
{
    UNUSED(file); UNUSED(func); UNUSED(line);
    if(SCREEN_VERBOSE_LEVEL>=MODULE_VERBOSITY)
    {
        PrintLeftType(stdout, "MODULE    ", FONT_LIGHTPURPLE);
        fprintf(stdout, FONT_LIGHTPURPLE);
        fprintf(stdout, "%s", message);
        fprintf(stdout, FONT_NORMAL);
        fflush(stdout);
    }
    if(ISSET_LOGS && LOGS_VERBOSE_LEVEL>=MODULE_VERBOSITY)
    {
        PrintLeftType(stdlog, "MODULE    ", FONT_LIGHTPURPLE);
        fprintf(stdlog, FONT_LIGHTPURPLE);
        fprintf(stdlog, "%s", message);
        fprintf(stdlog, FONT_NORMAL);
        fflush(stdlog);
    }
} //PrintModuleNoBreak_
///-------------------------------------------------------------------------------------
/** @fn PrintDiagnostic_
 * Prints an diagnotic message on screen
 * @param verbosity verbosity level of the message
 * @param *message message
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
void PrintDiagnostic_(int verbosity, char *message, char *file, char *func, int line)
{
    UNUSED(file); UNUSED(func); UNUSED(line);
    if(SCREEN_VERBOSE_LEVEL>=verbosity)
    {
        PrintLeftType(stdout, "DIAGNOSTIC", FONT_GREY);
        fprintf(stdout, FONT_GREY);
        fprintf(stdout, "%s", message);
        fprintf(stdout, FONT_NORMAL);
        fprintf(stdout, "\n");
        fflush(stdout);
    }
    if(ISSET_LOGS && LOGS_VERBOSE_LEVEL>=verbosity)
    {
        PrintLeftType(stdlog, "DIAGNOSTIC", FONT_GREY);
        fprintf(stdlog, FONT_GREY);
        fprintf(stdlog, "%s", message);
        fprintf(stdlog, FONT_NORMAL);
        fprintf(stdlog, "\n");
        fflush(stdlog);
    }
} //PrintDiagnostic_
///-------------------------------------------------------------------------------------
/** @fn PrintDiagnosticNoBreak_
 * Prints a diagnotic (no endline)
 * @param verbosity verbosity level of the message
 * @param *message message
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
void PrintDiagnosticNoBreak_(int verbosity, char *message, char *file, char *func, int line)
{
    UNUSED(file); UNUSED(func); UNUSED(line);
    if(SCREEN_VERBOSE_LEVEL>=verbosity)
    {
        PrintLeftType(stdout, "DIAGNOSTIC", FONT_GREY);
        fprintf(stdout, FONT_GREY);
        fprintf(stdout, "%s", message);
        fprintf(stdout, FONT_NORMAL);
        fflush(stdout);
    }
    if(ISSET_LOGS && LOGS_VERBOSE_LEVEL>=verbosity)
    {
        PrintLeftType(stdlog, "DIAGNOSTIC", FONT_GREY);
        fprintf(stdlog, FONT_GREY);
        fprintf(stdlog, "%s", message);
        fprintf(stdlog, FONT_NORMAL);
        fflush(stdlog);
    }
} //PrintDiagnosticNoBreak_
///-------------------------------------------------------------------------------------
/** @fn PrintMemory_
 * Prints a message for memory management
 * @param *message message
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
void PrintMemory_(char *message, char *file, char *func, int line)
{
    UNUSED(file); UNUSED(func); UNUSED(line);
    if(SCREEN_VERBOSE_LEVEL>=MEMORY_VERBOSITY)
    {
        PrintLeftType(stdout, "MEMORY    ", FONT_GREY);
        fprintf(stdout, FONT_GREY);
        fprintf(stdout, "%s", message);
        fprintf(stdout, FONT_NORMAL);
        fprintf(stdout, "\n");
        fflush(stdout);
    }
    if(ISSET_LOGS && LOGS_VERBOSE_LEVEL>=MEMORY_VERBOSITY)
    {
        PrintLeftType(stdlog, "MEMORY    ", FONT_GREY);
        fprintf(stdlog, FONT_GREY);
        fprintf(stdlog, "%s", message);
        fprintf(stdlog, FONT_NORMAL);
        fprintf(stdlog, "\n");
        fflush(stdlog);
    }
} //PrintMemory_
///-------------------------------------------------------------------------------------
/** @fn PrintTimer_
 * Prints timer value
 */
void PrintTimer_(char *message, char *file, char *func, int line)
{
    UNUSED(file); UNUSED(func); UNUSED(line);
    if(SCREEN_VERBOSE_LEVEL>=TIMER_VERBOSITY)
    {
        PrintLeftType(stdout, "TIMER     ", FONT_LIGHTERBLUE);
        fprintf(stdout, "%s\n", message);
        fflush(stdout);
    }
    if(ISSET_LOGS && LOGS_VERBOSE_LEVEL>=TIMER_VERBOSITY)
    {
        PrintLeftType(stdlog, "TIMER     ", FONT_LIGHTERBLUE);
        fprintf(stdlog, "%s\n", message);
        fflush(stdlog);
    }
} //PrintTimer_
///-------------------------------------------------------------------------------------
/** @fn PrintCommandLine
 * Prints the command line
 * @param argc number of arguments
 * @param **argv arguments
 */
void PrintCommandLine(int argc, char **argv)
{
    if(SCREEN_VERBOSE_LEVEL>=COMMAND_VERBOSITY)
    {
        PrintLeftType(stdout, "COMMAND   ", FONT_YELLOW);
        fprintf(stdout, FONT_YELLOW);
        fprintf(stdout, "%s", argv[0]);
        for(int i=1; i<argc; i++) fprintf(stdout, " %s", argv[i]);
        fprintf(stdout, FONT_NORMAL);
        fprintf(stdout, "\n");
        fflush(stdout);
    }
    if(ISSET_LOGS && LOGS_VERBOSE_LEVEL>=COMMAND_VERBOSITY)
    {
        PrintLeftType(stdlog, "COMMAND   ", FONT_YELLOW);
        fprintf(stdlog, FONT_YELLOW);
        fprintf(stdlog, "%s", argv[0]);
        for(int i=1; i<argc; i++) fprintf(stdlog, " %s", argv[i]);
        fprintf(stdlog, FONT_NORMAL);
        fprintf(stdlog, "\n");
        fflush(stdlog);
    }
} //PrintCommandLine
///-------------------------------------------------------------------------------------
/** @fn PrintWarning_
 * Prints a warning message
 * @param *message message
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
void PrintWarning_(char *message, char *file, char *func, int line)
{
    UNUSED(file); UNUSED(func); UNUSED(line);
    if(SCREEN_VERBOSE_LEVEL>=WARNING_VERBOSITY)
    {
        PrintLeftType(stdout, "WARNING   ", FONT_ORANGE);
        fprintf(stdout, FONT_ORANGE);
        fprintf(stdout, "Warning: %s", message);
        fprintf(stdout, FONT_NORMAL);
        fprintf(stdout, "\n");
        fflush(stdout);
    }
    if(ISSET_LOGS && LOGS_VERBOSE_LEVEL>=WARNING_VERBOSITY)
    {
        PrintLeftType(stdlog, "WARNING   ", FONT_ORANGE);
        fprintf(stdlog, FONT_ORANGE);
        fprintf(stdlog, "Warning: %s", message);
        fprintf(stdlog, FONT_NORMAL);
        fprintf(stdlog, "\n");
        fflush(stdlog);
    }
} //PrintWarning_
///-------------------------------------------------------------------------------------
/** @fn PrintWarning_NoBreak
 * Prints a warning message (no endline)
 * @param *message message
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
void PrintWarning_NoBreak(char *message, char *file, char *func, int line)
{
    UNUSED(file); UNUSED(func); UNUSED(line);
    if(SCREEN_VERBOSE_LEVEL>=WARNING_VERBOSITY)
    {
        PrintLeftType(stdout, "WARNING   ", FONT_ORANGE);
        fprintf(stdout, FONT_ORANGE);
        fprintf(stdout, "Warning: %s", message);
        fprintf(stdout, FONT_NORMAL);
        fflush(stdout);
    }
    if(ISSET_LOGS && LOGS_VERBOSE_LEVEL>=WARNING_VERBOSITY)
    {
        PrintLeftType(stdlog, "WARNING   ", FONT_ORANGE);
        fprintf(stdlog, FONT_ORANGE);
        fprintf(stdlog, "Warning: %s", message);
        fprintf(stdlog, FONT_NORMAL);
        fflush(stdlog);
    }
} //PrintWarning_NoBreak
///-------------------------------------------------------------------------------------
/** @fn PrintError_
 * Prints an error message
 * @param *message message
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
void PrintError_(char *message, char *file, char *func, int line)
{
    if(SCREEN_VERBOSE_LEVEL>=ERROR_VERBOSITY)
    {
        PrintLeftType(stderr, "ERROR     ", FONT_LIGHTRED);
        fprintf(stderr, FONT_LIGHTRED);
        fprintf(stderr, "Error in ");
        char buf[BUFLENGTH];
        FormatFunction_(buf, file, func, line);
        fprintf(stderr, "%s", buf);
        fprintf(stderr, FONT_LIGHTRED);
        fprintf(stderr, ": %s", message);
        fprintf(stderr, FONT_NORMAL);
        fprintf(stderr, "\n");
        fflush(stderr);
    }
    if(ISSET_LOGS && LOGS_VERBOSE_LEVEL>=ERROR_VERBOSITY)
    {
        PrintLeftType(stdlog, "ERROR     ", FONT_LIGHTRED);
        fprintf(stdlog, FONT_LIGHTRED);
        fprintf(stdlog, "Error in ");
        char buf[BUFLENGTH];
        FormatFunction_(buf, file, func, line);
        fprintf(stdlog, "%s", buf);
        fprintf(stdlog, FONT_LIGHTRED);
        fprintf(stdlog, ": %s", message);
        fprintf(stdlog, FONT_NORMAL);
        fprintf(stdlog, "\n");
        fflush(stdlog);
    }
} //PrintError_
///-------------------------------------------------------------------------------------
/** @fn PrintErrorNoBreak_
 * Prints an error message (no endline)
 * @param *message message
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
void PrintErrorNoBreak_(char *message, char *file, char *func, int line)
{
    if(SCREEN_VERBOSE_LEVEL>=ERROR_VERBOSITY)
    {
        PrintLeftType(stderr, "ERROR     ", FONT_LIGHTRED);
        fprintf(stderr, FONT_LIGHTRED);
        fprintf(stderr, "Error in ");
        char buf[BUFLENGTH];
        FormatFunction_(buf, file, func, line);
        fprintf(stderr, "%s", buf);
        fprintf(stderr, FONT_LIGHTRED);
        fprintf(stderr, ": %s", message);
        fprintf(stderr, FONT_NORMAL);
        fflush(stderr);
    }
    if(ISSET_LOGS && LOGS_VERBOSE_LEVEL>=ERROR_VERBOSITY)
    {
        PrintLeftType(stdlog, "ERROR     ", FONT_LIGHTRED);
        fprintf(stdlog, FONT_LIGHTRED);
        fprintf(stdlog, "Error in ");
        char buf[BUFLENGTH];
        FormatFunction_(buf, file, func, line);
        fprintf(stdlog, "%s", buf);
        fprintf(stdlog, FONT_LIGHTRED);
        fprintf(stdlog, ": %s", message);
        fprintf(stdlog, FONT_NORMAL);
        fflush(stdlog);
    }
} //PrintErrorNoBreak_
///-------------------------------------------------------------------------------------
/** @fn FatalError_
 * Calls a fatal error and exits program
 * @param *message message
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
void FatalError_(char *message, char *file, char *func, int line)
{
    #pragma omp critical
    PrintError_(message, file, func, line);
    exit(EXIT_FAILURE);
} //FatalError_
///-------------------------------------------------------------------------------------
/** @fn FatalErrorNoMessage_
 * Calls a fatal error and exits program (no message displayed)
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
void FatalErrorNoMessage_(char *file, char *func, int line)
{
    UNUSED(file); UNUSED(func); UNUSED(line);
    exit(EXIT_FAILURE);
} //FatalErrorNoMessage_
///-------------------------------------------------------------------------------------
/** @fn p_assert__
 * Asserts a condition, aborts otherwise
 * @param condition string description
 * @param value bool
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
void p_assert__(char *condition, bool value, char *file, char *func, int line)
{
    if(!value)
    {
        sprintf(G__msg__, "Assertion '%s' failed.", condition);
        PrintError_(G__msg__, file, func, line);
        exit(EXIT_FAILURE);
    }
} //p_assert__
///-------------------------------------------------------------------------------------
/** @fn StartCPUTimer
 * Starts a CPU timer
 * @param *startm output: starting CPU time (CPUtime)
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
void StartCPUTimer(CPUtime *startm, char *file, char *func, int line)
{
    #if TIMERS
        if( (*startm = clock()) == -1)
            FatalError_("Error calling CPU clock", file, func, line);
    #else
        UNUSED(startm); UNUSED(file); UNUSED(func); UNUSED(line);
    #endif
} //StartCPUTimer
///-------------------------------------------------------------------------------------
/** @fn StopCPUTimer
 * Stops a CPU time
 * @param startm starting CPU time (CPUtime)
 * @param *diff_t_cpu output: elapsed CPU time (double)
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
void StopCPUTimer(CPUtime startm, double *diff_t_cpu, char *file, char *func, int line)
{
    #if TIMERS
        CPUtime stopm;
        if( (stopm = clock()) == -1)
            FatalError_("Error calling CPU clock", file, func, line);
        *diff_t_cpu = (double)(stopm-startm)/CLOCKS_PER_SEC;
    #else
        UNUSED(startm); UNUSED(diff_t_cpu); UNUSED(file); UNUSED(func); UNUSED(line);
    #endif
} //StopCPUTimer
///-------------------------------------------------------------------------------------
/** @fn StartWallTimer
 * Starts a wall timer
 * @param *startw output: starting wall time (Walltime)
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
void StartWallTimer(Walltime *startw, char *file, char *func, int line)
{
    #if TIMERS
        if( gettimeofday(startw,0) == -1)
            FatalError_("Error calling wallclock", file, func, line);
    #else
        UNUSED(startw); UNUSED(file); UNUSED(func); UNUSED(line);
    #endif
} //StartWallTimer
///-------------------------------------------------------------------------------------
/** @fn StopWallTimer
 * Stops a wall timer
 * @param startw starting wall time (Walltime)
 * @param *diff_t_wall output: elapsed wall time (double)
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
void StopWallTimer(Walltime startw, double *diff_t_wall, char *file, char *func, int line)
{
    #if TIMERS
        Walltime stopw;
        if( gettimeofday(&stopw,0) == -1)
            FatalError_("Error calling wallclock", file, func, line);
        *diff_t_wall = (double)((stopw.tv_sec - startw.tv_sec) * 1000.0f + (stopw.tv_usec - startw.tv_usec) / 1000.0f)/1000.0f;
    #else
        UNUSED(startw); UNUSED(diff_t_wall); UNUSED(file); UNUSED(func); UNUSED(line);
    #endif
} //StopWallTimer
///-------------------------------------------------------------------------------------
/** @fn StartTimers_
 * Starts CPU and wall timers
 * @param *startm output: starting CPU time (CPUtime)
 * @param *startw output: starting wall time (Walltime)
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
void StartTimers_(CPUtime *startm, Walltime *startw, char *file, char *func, int line)
{
    StartCPUTimer(startm, file, func, line);
    StartWallTimer(startw, file, func, line);
} //StartTimers_
///-------------------------------------------------------------------------------------
/** @fn StopTimers_
 * Stops CPU and wall timers
 * @param startm starting CPU time (CPUtime)
 * @param startw starting wall time (Walltime)
 * @param *diff_t_cpu output: elapsed CPU time (double)
 * @param *diff_t_wall output: elapsed wall time (double)
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
void StopTimers_(CPUtime startm, Walltime startw, double *diff_t_cpu, double *diff_t_wall, char *file, char *func, int line)
{
    StopCPUTimer(startm, diff_t_cpu, file, func, line);
    StopWallTimer(startw, diff_t_wall, file, func, line);
} //StopTimers_
///-------------------------------------------------------------------------------------
/** @fn PrintTimers_
 * Prints CPU and wall timers
 * @param *type type of timer
 * @param diff_t_cpu elapsed CPU time (double)
 * @param diff_t_wall elapsed wall time (double)
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
void PrintTimers_(char *type, double diff_t_cpu, double diff_t_wall, char *file, char *func, int line)
{
    #if TIMERS
        sprintf(G__msg__, "%s: %.3f CPU - %.3f wallclock seconds used.", type, diff_t_cpu, diff_t_wall);
        PrintTimer_(G__msg__, file, func, line);
    #else
        UNUSED(type); UNUSED(diff_t_cpu); UNUSED(diff_t_wall); UNUSED(file); UNUSED(func); UNUSED(line);
    #endif
} //PrintTimers_
///-------------------------------------------------------------------------------------
/** @fn StartModuleTimers_
 * Starts module timer
 */
void StartModuleTimers_(char *file, char *func, int line)
{
    UNUSED(file); UNUSED(func); UNUSED(line);
    #if TIMERS
        StartModuleWallTimer();
        StartModuleCPUTimer();
    #endif
} //StartModuleTimers_
///-------------------------------------------------------------------------------------
/** @fn StopPrintModuleTimers_
 * Stops and prints module timer value
 */
void StopPrintModuleTimers_(char *ModuleName, char *file, char *func, int line)
{
    UNUSED(file); UNUSED(func); UNUSED(line);
    #if TIMERS
        StopModuleWallTimer();
        StopModuleCPUTimer();
        PrintTimers_(ModuleName, diff_t_cpu_module, diff_t_wall_module, file, func, line);
    #else
        UNUSED(ModuleName);
    #endif
} //StopPrintModuleTimers_
///-------------------------------------------------------------------------------------
/** @fn Begin_
 * Begins the program
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
void Begin_(char *file, char *func, int line)
{
    UNUSED(file); UNUSED(func); UNUSED(line);
    // Time monitoring
    #if TIMERS
        StartSimbelmyneCPUTimer();
        StartSimbelmyneWallTimer();
    #endif
} //Begin_
///-------------------------------------------------------------------------------------
/** @fn EndSuccess_
 * Ends the program in case of success
 * @param *file file where the function is called
 * @param *func function where the function is called
 * @param line line where the function is called
 */
void EndSuccess_(char *file, char *func, int line)
{
    #if DEBUG
        // Memory management
        sprintf(G__msg__, "%d calls to function malloc.", G__count_malloc__);
        PrintDiagnostic_(MEMORY_VERBOSITY, G__msg__, file, func, line);
        sprintf(G__msg__, "%d calls to function free.", G__count_free__);
        PrintDiagnostic_(MEMORY_VERBOSITY, G__msg__, file, func, line);
        if(G__count_malloc__ != G__count_free__)
        {
            sprintf(G__msg__, "Memory management should be checked.");
            PrintWarning_(G__msg__, file, func, line);
        }
    #endif
    #if TIMERS
        // Time monitoring
        StopSimbelmyneCPUTimer();
        StopSimbelmyneWallTimer();
        PrintTimers_("Simbelmynë", diff_t_cpu_simbelmyne, diff_t_wall_simbelmyne, file, func, line);
    #endif
    sprintf(G__msg__, "Everything done successfully, exiting.");
    PrintInfo_(G__msg__, file, func, line);
} //EndSuccess_
///-------------------------------------------------------------------------------------
