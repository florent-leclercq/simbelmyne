///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/include/assignments.h
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file assignments.h
 *  \brief Header for assignments.c
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2014-2023
 */
#ifndef header_assignments
#define header_assignments

    #include "fd_schemes.h"
    #include "math_utils.h"
    #include "utils.h"

/* -------------------------------------------------------------------------------------
 * ------------------------------------ NGP --------------------------------------------
 * -------------------------------------------------------------------------------------
 */
    #define NGP 1
    cellIndex_t aux_particle_to_mesh_ngp_periodic(const float_t x0, const float_t x1, const float_t x2, const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, int *mc0, int *mc1, int *mc2);
    void particle_to_mesh_ngp(const float_t x0, const float_t x1, const float_t x2, const double weight, float_t *A, const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, const int boundary_conditions, int *mc0min, int *mc0max, int *mc1min, int *mc1max, int *mc2min, int *mc2max);
    float_t mesh_to_particle_ngp(const float_t *A, const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, const float_t x0, const float_t x1, const float_t x2, const double weight, const int boundary_conditions, const int N0_pad, const int N1_pad, const int N2_pad);

/* -------------------------------------------------------------------------------------
 * ------------------------------------ CiC --------------------------------------------
 * -------------------------------------------------------------------------------------
 */
    #define CIC 2
    void particle_to_mesh_cic(const float_t x0, const float_t x1, const float_t x2, const double weight, float_t *A, const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, const int boundary_conditions, int *mc0min, int *mc0max, int *mc1min, int *mc1max, int *mc2min, int *mc2max, double *mass_deposited);
    void particle_to_mesh_cic_periodic(const float_t x0, const float_t x1, const float_t x2, const float_t weight, float_t *A, const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, int *mc0min, int *mc0max, int *mc1min, int *mc1max, int *mc2min, int *mc2max);
    float_t mesh_to_particle_cic(const float_t *A, const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, const float_t x0, const float_t x1, const float_t x2, const double weight, const int boundary_conditions, const int N0_pad, const int N1_pad, const int N2_pad);

/* -------------------------------------------------------------------------------------
 * ------------------------------------ TSC --------------------------------------------
 * -------------------------------------------------------------------------------------
 */
    #define TSC 3

/* -------------------------------------------------------------------------------------
 * ---------------------------------- WRAPPERS -----------------------------------------
 * -------------------------------------------------------------------------------------
 */
    void particle_to_mesh_periodic(const float_t x0, const float_t x1, const float_t x2, const double weight, float_t *A, const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, int *mc0min, int *mc0max, int *mc1min, int *mc1max, int *mc2min, int *mc2max);
    void particle_to_mesh_nonperiodic(const float_t x0, const float_t x1, const float_t x2, const double weight, float_t *A, const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, int *mc0min, int *mc0max, int *mc1min, int *mc1max, int *mc2min, int *mc2max, double *mass_deposited);
    float_t mesh_to_particle_periodic(const float_t *A, const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, const float_t x0, const float_t x1, const float_t x2, const double weight);
    float_t mesh_to_particle(const float_t *A, const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, const float_t x0, const float_t x1, const float_t x2, const double weight, const int boundary_conditions, const int N0_pad, const int N1_pad, const int N2_pad);

#endif
