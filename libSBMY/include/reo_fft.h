///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/include/reo_fft.h
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file reo_fft.h
 *  \brief Header for reo_fft.c
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2019-2023
 */
#ifndef header_reo_fft
#define header_reo_fft

    #include "fd_schemes.h"
    #include "fft.h"
    #include "utils.h"

    void FFT_forward_r2r_3d(int N0, int N1, int N2, float_t *in, float_t *out, fftw_r2r_kind kind0, fftw_r2r_kind kind1, fftw_r2r_kind kind2, int Nthreads);
    void FFT_backward_r2r_3d(int N0, int N1, int N2, float_t *in, float_t *out, fftw_r2r_kind kind0, fftw_r2r_kind kind1, fftw_r2r_kind kind2, int Nthreads);
    double get_kmode_RODFT00(const int l, const int N, const double L);
    double get_kmode_grad_RODFT00(const int l, const int m, const int n, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2, const double d0, const double d1, const double d2, const int axis);

#endif
