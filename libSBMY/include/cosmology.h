///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/include/cosmology.h
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file cosmology.h
 *  \brief Header for cosmology.c
 *  \authors Florent Leclercq
 *  \version 0.5.0
 *  \date 2014-2023
 */
#ifndef header_cosmology
#define header_cosmology

    #include "gsl.h"
    #include "utils.h"

    // Physical constants
    #ifndef P_UnitLength_in_cm
    #define P_UnitLength_in_cm 3.085678e24 // 1.0 Mpc in cm
    #endif
    #ifndef P_UnitMass_in_g
    #define P_UnitMass_in_g 1.989e43 // 1.0e10 solar masses in g
    #endif
    #ifndef P_UnitVelocity_in_cm_per_s
    #define P_UnitVelocity_in_cm_per_s 1e5 // 1 km/sec in cm/sec
    #endif
    #ifndef P_UnitTime_in_s
    #define P_UnitTime_in_s P_UnitLength_in_cm / P_UnitVelocity_in_cm_per_s
    #endif
    #ifndef P_GRAVITY
    #define P_GRAVITY 6.67384e-8
    #endif
    #ifndef P_G
    #define P_G P_GRAVITY / pow(P_UnitLength_in_cm, 3) * P_UnitMass_in_g * pow(P_UnitTime_in_s, 2)
    #endif
    #ifndef P_c_light_Mpc_s
    #define P_c_light_Mpc_s 9.71560348808e-15
    #endif
    #ifndef P_c_light_cm_s
    #define P_c_light_cm_s 29979245800
    #endif
    #ifndef P_Hubble
    #define P_Hubble 100. // so that HubbleParam is in units of 100 km/sec/Mpc
    #endif
    #ifndef P_H100_s
    #define P_H100_s 3.24077648681e-18
    #endif

    // Number of cosmological parameters
    #ifndef N_COSMOPAR
    #define N_COSMOPAR 10
    #endif

    #define eps 1.0e-6 /* numerical accuracy for integrations */
    #define epsabs eps
    #define epsrel eps
    #define AMIN 1e-4
    #define NEVAL 1000

    extern const double a_ref;

    double a2z(const double a);
    double z2a(const double z);
    double t2a(const int TimeStepDistribution, const double t);
    double a2t(const int TimeStepDistribution, const double a);
    double hubble(const double a, const double cosmo[N_COSMOPAR]);
    double hubble_of_z(const double z, const double cosmo[N_COSMOPAR]);
    double z2dcom(double z, const double cosmo[N_COSMOPAR]);
    double dcom2z(double dcom, const double cosmo[N_COSMOPAR]);
    double FHubble(const double a, const double cosmo[N_COSMOPAR]);
    double w_of_a(const double a, const double cosmo[N_COSMOPAR]);
    double d_plus(const double a_start, const double a_end, const double cosmo[N_COSMOPAR]);
    double d2_plus(const double a_start, const double a_end, const double cosmo[N_COSMOPAR]);
    double d_plus_prime(const double a_start, const double a, const double cosmo[N_COSMOPAR]);
    double g_plus(const double a, const double cosmo[N_COSMOPAR]);
    double f2(const double f1);
    double* lpt_prefactors(const double a, const double cosmo[N_COSMOPAR]);
    double D_of_a(double a, const double cosmo[N_COSMOPAR]);
    double dtx(const bool ModifiedDiscretization, double ai, double af, double ac, double n_LPT, const double cosmo[N_COSMOPAR]);
    double K_of_a(double a, const double cosmo[N_COSMOPAR]);
    double dtp(const bool ModifiedDiscretization, double ai, double af, double ac, double n_LPT, const double cosmo[N_COSMOPAR]);
    double* dp_lpt(const double beta_a, const double a_c, const double cosmo[N_COSMOPAR]);
    double* dx_lpt(const double a_i, const double a_f, const double cosmo[N_COSMOPAR]);

#endif
