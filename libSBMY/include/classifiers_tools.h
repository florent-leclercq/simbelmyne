///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/include/classifiers_tools.h
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file classifiers_tools.h
 *  \brief Header for classifiers_tools.c
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2015-2023
 */
#ifndef header_classifiers_tools
#define header_classifiers_tools

    #include "assignments.h"
    #include "math_utils.h"
    #include "structures.h"
    #include "utils.h"

    int classify_object(double EIGVAL0, double EIGVAL1, double EIGVAL2, double EIGVALTH);
    void structures_l2e(const float_t *OBJECTS, const Snapshot P, float_t *VOIDS, float_t *SHEETS, float_t *FILAMENTS, float_t *CLUSTERS, const particleID_t Np, const int N0, const int N1, const int N2);

#endif
