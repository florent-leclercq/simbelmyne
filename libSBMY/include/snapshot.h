///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/include/snapshot.h
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file snapshot.h
 *  \brief Header for snapshot.c
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2013-2023
 */
#ifndef header_snapshot
#define header_snapshot

    #include "assignments.h"
    #include "cosmology.h"
    #include "structures.h"
    #include "utils.h"

    void get_Lagrangian_indices(const particleID_t Id, const int Np0, const int Np1, const int Np2, int *indices);
    void get_Lagrangian_grid_position(const int i, const int j, const int k, const double d0, const double d1, const double d2, float_t[3]);
    particleID_t get_Lagrangian_Id(const int i, const int j, const int k, const int Np0, const int Np1, const int Np2);
    void get_Psi(const float_t finalPosition[3], const float_t initialPosition[3], const double L0, const double L1, const double L2, float_t[3]) ;
    double periodic_wrap(const double x, const double xmin, const double L);
    double clipping_wrap(const double x, const double xmin, const double L, const double d);

    Snapshot return_grid_snapshot_DM(const int Np0, const int Np1, const int Np2, const double BoxSize, const int num_files, const double cosmo[N_COSMOPAR]);
    #define allocate_snapshot(P, Np, name) allocate_snapshot_(P, Np, name, __POS__)
    void allocate_snapshot_(Snapshot *P, const particleID_t Np, char *name, char *file, char *func, int line);

    void modify_snapshot_update_time(Snapshot *P, const double redshift);
    void modify_snapshot_drift_mesh(Snapshot *P, const float_t *DRIFT_X, const float_t *DRIFT_Y, const float_t *DRIFT_Z, const int N0, const int N1, const int N2);
    void modify_snapshot_drift_particles(Snapshot *P, const float_t *DRIFT_X, const float_t *DRIFT_Y, const float_t *DRIFT_Z);
    void modify_snapshot_kick_mesh(Snapshot *P, const float_t *KICK_X, const float_t *KICK_Y, const float_t *KICK_Z, const int N0, const int N1, const int N2);
    void modify_snapshot_kick_particles(Snapshot *P, const float_t *KICK_X, const float_t *KICK_Y, const float_t *KICK_Z);

#endif
