///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/include/poisson_solvers.h
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file poisson_solvers.h
 *  \brief Header for poisson_solvers.c
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2019-2023
 */
#ifndef header_poisson_solvers
#define header_poisson_solvers

    #include "utils.h"
    #include "fd_schemes.h"
    #include "fft.h"
    #include "reo_fft.h"

    #ifndef ASMTH
    #define ASMTH 1.25
    // ASMTH gives the smoothing scale for short-range forces in units of FFT-mesh cells
    #endif

    double dgf_laplace_DFT(const int mf0, const int mf1, const int mf2, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, const bool smooth, const double normphi);
    void multiply_by_dgf_laplace_DFT(fftw_complex_t *AUX, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, const bool smooth, const double normphi);
    void solve_poisson_DFT(float_t *IN, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, const bool smooth);
    void solve_poisson_periodic(float_t *density, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, const bool smooth);
    void get_2ndderivative_potential_rs_DFT(fftw_complex_t *POT, float_t *OUT, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, const int axis_a, const int axis_b);

    double dgf_laplace_RODFT00(const int mf0, const int mf1, const int mf2, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, const bool smooth, const double normphi);
    void multiply_by_dgf_laplace_RODFT00(float_t *AUX, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, const bool smooth, const double normphi);
    void solve_poisson_RODFT00(float_t *IN, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, const bool smooth);
    void solve_poisson_zerobc(float_t *density, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, const bool smooth);
    void get_2ndderivative_potential_rs_RODFT00(float_t *POT, float_t *OUT, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, const int axis_a, const int axis_b);

    void modify_interior_dirichletbc(float_t *IN, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, float_t *PhiB_W, float_t *PhiB_E, float_t *PhiB_S, float_t *PhiB_N, float_t *PhiB_B, float_t *PhiB_T);
    void solve_poisson_dirichletbc(float_t *IN, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, const bool smooth, float_t *PhiB_W, float_t *PhiB_E, float_t *PhiB_S, float_t *PhiB_N, float_t *PhiB_B, float_t *PhiB_T);

#endif
