///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/include/correlations.h
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file correlations.h
 *  \brief Header for correlations.c
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2014-2023
 */
#ifndef header_correlations
#define header_correlations

    #include "assignments.h"
    #include "fft.h"
    #include "utils.h"

    void get_crosscorrelation_fs(const float_t *A1, const float_t *A2, const int Nk, const int *k_keys, const int k_nmodes[Nk], float_t P12k[Nk], float_t V12k[Nk], float_t Rk[Nk], float_t VRk[Nk], const int N0, const int N1, const int N2, const double L0, const double L1, const double L2, const bool AliasingCorr);
    void get_autocorrelation_fs(const float_t *A, const int Nk, const int *k_keys, const int k_nmodes[Nk], float_t Pk[Nk], float_t Vk[Nk], const int N0, const int N1, const int N2, const double L0, const double L1, const double L2, const bool AliasingCorr);

#endif
