///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/include/gsl.h
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file gsl.h
 *  \brief Header to include gsl functions
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2012-2023
 */
#ifndef header_gsl
#define header_gsl

    #include <gsl/gsl_const_num.h>
    #include <gsl/gsl_const_mksa.h>
    #include <gsl/gsl_randist.h>
    #include <gsl/gsl_rng.h>
    #include <gsl/gsl_integration.h>
    #include <gsl/gsl_roots.h>
    #include <gsl/gsl_linalg.h>
    #include <gsl/gsl_blas.h>
    #include <gsl/gsl_errno.h>
    #include <gsl/gsl_odeiv.h>
    #include <gsl/gsl_sf.h>
    #include <gsl/gsl_math.h>
    #include <gsl/gsl_min.h>

#endif
