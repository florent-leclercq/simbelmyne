///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/include/random.h
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file random.h
 *  \brief Header for random.c
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2016-2023
 */
#ifndef header_random
#define header_random

    #include "gsl.h"
    #include "utils.h"

    void reset_rng(const char *fname, gsl_rng * gBaseRand);
    void set_rng(gsl_rng *random_generator, const char *InputRngState);
    void save_rng_state(gsl_rng *random_generator, const char *OutputRngState);
    #define generate_seed_table(random_generator, length, name) generate_seed_table_(random_generator, length, name, __POS__)
    uint64_t* generate_seed_table_(gsl_rng *random_generator, const int length, char *name, char *file, char *func, int line);
    #define seed_table_from_rngstate(InputRngState, length, OutputRngState, name) seed_table_from_rngstate_(InputRngState, length, OutputRngState, name, __POS__)
    uint64_t* seed_table_from_rngstate_(const char *InputRngState, const int length, const char *OutputRngState, char *name, char *file, char *func, int line);

#endif
