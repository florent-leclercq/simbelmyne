///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/include/structures.h
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file structures.h
 *  \brief Header defining Simbelmynë internal structures
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2013-2023
 */
#ifndef header_structures
#define header_structures

    #include "cosmology.h"

    // The order of the variables matters for snapshot-related structures, when used with python ctypes!
    // Gadget-2 version
    struct __header_G2
    {
        unsigned int npart[6];                 /*!< number of particles of each type in this file */
        double mass[6];                        /*!< mass of particles of each type. If 0, then the masses are explicitly stored in the mass-block of the snapshot file, otherwise they are omitted */
        double time;                           /*!< time of snapshot file */
        double redshift;                       /*!< redshift of snapshot file */
        int flag_sfr;                          /*!< flags whether the simulation was including star formation */
        int flag_feedback;                     /*!< flags whether feedback was included (obsolete) */
        unsigned int npartTotal[6];            /*!< total number of particles of each type in this snapshot. This can be different from npart if one is dealing with a multi-file snapshot. */
        int flag_cooling;                      /*!< flags whether cooling was included  */
        int num_files;                         /*!< number of files in multi-file snapshot */
        double BoxSize;                        /*!< box-size of simulation in case periodic boundaries were used */
        double Omega0;                         /*!< matter density in units of critical density */
        double OmegaLambda;                    /*!< cosmological constant parameter */
        double HubbleParam;                    /*!< Hubble parameter in units of 100 km/sec/Mpc */
        int flag_stellarage;                   /*!< flags whether the file contains formation times of star particles */
        int flag_metals;                       /*!< flags whether the file contains metallicity values for gas and star particles */
        unsigned int npartTotalHighWord[6];    /*!< High word of the total number of particles of each type */
        int flag_entropy_instead_u;            /*!< flags that IC-file contains entropy instead of u */
        char fill[256 - 6*sizeof(unsigned int) - 6*sizeof(double) - 2*sizeof(double) - 2*sizeof(int) - 6*sizeof(unsigned int) - 2*sizeof(int) - 4*sizeof(double) - 2*sizeof(int) - 6*sizeof(unsigned int) - sizeof(int)];            /*!< fills to 256 Bytes */
    };
    typedef struct __header_G2 header_G2_t;

    // Gadget-3 version
    struct __header_G3
    {
        unsigned int npart[6];                 /*!< number of particles of each type in this file */
        double mass[6];                        /*!< mass of particles of each type. If 0, then the masses are explicitly stored in the mass-block of the snapshot file, otherwise they are omitted */
        #ifdef COSMIC_RAYS
        double SpectralIndex_CR_Pop[NUMCRPOP]; /*!< spectral indices of cosmic ray populations */
        #endif
        double time;                           /*!< time of snapshot file */
        double redshift;                       /*!< redshift of snapshot file */
        int flag_sfr;                          /*!< flags whether the simulation was including star formation */
        int flag_feedback;                     /*!< flags whether feedback was included (obsolete) */
        unsigned int npartTotal[6];            /*!< total number of particles of each type in this snapshot. This can be different from npart if one is dealing with a multi-file snapshot. */
        int flag_cooling;                      /*!< flags whether cooling was included  */
        int num_files;                         /*!< number of files in multi-file snapshot */
        double BoxSize;                        /*!< box-size of simulation in case periodic boundaries were used */
        double Omega0;                         /*!< matter density in units of critical density */
        double OmegaLambda;                    /*!< cosmological constant parameter */
        double HubbleParam;                    /*!< Hubble parameter in units of 100 km/sec/Mpc */
        int flag_stellarage;                   /*!< flags whether the file contains formation times of star particles */
        int flag_metals;                       /*!< flags whether the file contains metallicity values for gas and star particles */
        unsigned int npartTotalHighWord[6];    /*!< High word of the total number of particles of each type */
        int flag_entropy_instead_u;            /*!< flags that IC-file contains entropy instead of u */
        // Specific to Gadget-3:
        int flag_doubleprecision;              /*!< flags that snapshot contains double-precision instead of single precision */
        int flag_ic_info;                      /*!< flag to inform whether IC files are generated with ordinary Zeldovich approximation, or whether they ocontains 2nd order
lagrangian perturbation theory initial conditions. For snapshots files, the value informs whether the simulation was evolved from Zeldoch or 2lpt ICs. Encoding is as follows:
                            FLAG_ZELDOVICH_ICS     (1)   -IC file based on Zeldovich
                            FLAG_SECOND_ORDER_ICS  (2)   -Special IC-file containing 2lpt masses
                            FLAG_EVOLVED_ZELDOVICH (3)   -snapshot evolved from Zeldovich ICs
                            FLAG_EVOLVED_2LPT      (4)   -snapshot evolved from 2lpt ICs
                            FLAG_NORMALICS_2LPT    (5)   -standard gadget file format with 2lpt ICs
                            All other values, including 0 are interpreted as "don't know" for backwards compatibility.*/
        float lpt_scalingfactor;               /*!< scaling factor for 2lpt initial conditions */
        #ifdef COSMIC_RAYS
        char fill[18-NUMCRPOP*sizeof(double)]; /*!< fills to 256 Bytes */
        #else
        char fill[18];                         /*!< fills to 256 Bytes */
        #endif
        char names[15][2];
    };
    typedef struct __header_G3 header_G3_t;
    typedef struct __header_G3 header_t;

    #ifdef HAVE_TYPE_AND_MASS
    struct __particle_type
    {
        float_t Type;
    };
    typedef struct __particle_type particle_type_t;

    struct __particle_mass
    {
        float_t Mass;
    };
    typedef struct __particle_mass particle_mass_t;
    #endif

    struct __particle_pos
    {
        float_t Pos[3];
    };
    typedef struct __particle_pos particle_pos_t;

    struct __particle_vel
    {
        float_t Vel[3];
    };
    typedef struct __particle_vel particle_vel_t;

    struct __particle_lpt
    {
        float_t Psi1[3];
        #ifndef ONLY_ZA
        float_t Psi2[3];
        #endif
    };
    typedef struct __particle_lpt particle_lpt_t;

    struct __particle_id
    {
        particleID_t Id;
    };
    typedef struct __particle_id particle_id_t;

    #ifdef HAVE_EXTRAS
    struct __particle_extras
    {
        float_t U, Rho, Hsml;
    };
    typedef struct __particle_extras particle_extras_t;
    #endif

    struct __Snapshot
    {
        header_t header;
        #ifdef HAVE_TYPE_AND_MASS
        particle_type_t *types;
        particle_mass_t *masses;
        #endif
        particle_pos_t *positions;
        particle_vel_t *velocities;
        particle_id_t *ids;
        #ifdef HAVE_EXTRAS
        particle_extras_t *extras;
        #endif
    };
    typedef struct __Snapshot Snapshot;

    #ifndef KGRIDMETA
        #define KGRIDMETA          \
            double L0;             \
            double L1;             \
            double L2;             \
            int N0;                \
            int N1;                \
            int N2;                \
            int N2_HC;             \
            int N_HC;              \
            int NUM_MODES;         \
            double kmax;
        #define KGRIDMETA_ARGS     \
            double L0,             \
            double L1,             \
            double L2,             \
            int N0,                \
            int N1,                \
            int N2,                \
            int N2_HC,             \
            int N_HC,              \
            int NUM_MODES,         \
            double kmax
        #define KGRIDMETA_PTR_ARGS \
            double *L0,            \
            double *L1,            \
            double *L2,            \
            int *N0,               \
            int *N1,               \
            int *N2,               \
            int *N2_HC,            \
            int *N_HC,             \
            int *NUM_MODES,        \
            double *kmax
    #endif
    #ifndef KGRID
        #define KGRID              \
            KGRIDMETA              \
            float_t *k_modes;      \
            int* k_nmodes;         \
            int* k_keys;
        #define KGRID_ARGS         \
            KGRIDMETA_ARGS,        \
            float_t *k_modes,      \
            int* k_nmodes,         \
            int* k_keys
    #endif

    struct __FourierGrid
    {
        KGRID
    };
    typedef struct __FourierGrid FourierGrid;

    struct __PowerStruct
    {
        KGRID
        float_t *powerspectrum;
    };
    typedef struct __PowerStruct PowerSpectrum;

    #ifndef BASEFIELDMETA
        #define BASEFIELDMETA          \
            double L0;                 \
            double L1;                 \
            double L2;                 \
            double corner0;            \
            double corner1;            \
            double corner2;            \
            int rank;                  \
            int N0;                    \
            int N1;                    \
            int N2;
        #define BASEFIELDMETA_ARGS     \
            double L0,                 \
            double L1,                 \
            double L2,                 \
            double corner0,            \
            double corner1,            \
            double corner2,            \
            int rank,                  \
            int N0,                    \
            int N1,                    \
            int N2
        #define BASEFIELDMETA_PTR_ARGS \
            double *L0,                \
            double *L1,                \
            double *L2,                \
            double *corner0,           \
            double *corner1,           \
            double *corner2,           \
            int *N0,                   \
            int *N1,                   \
            int *N2,                   \
            int *rank
    #endif
    #ifndef BASEFIELD
        #define BASEFIELD              \
            BASEFIELDMETA              \
            float_t  *data;
    #endif

    struct __BaseField
    {
        BASEFIELD
    };
    typedef struct __BaseField BaseField;

    struct __Field
    {
        BASEFIELD
        double time;
    };
    typedef struct __Field Field;

    struct __Mock
    {
        BASEFIELD
        int N_BIAS;
        double *galaxy_bias;
        double galaxy_nmean;
    };
    typedef struct __Mock Mock;

    #ifndef SURVEYGEOMETRYMETA
        #define SURVEYGEOMETRYMETA \
        double cosmo[N_COSMOPAR];  \
        double *bright_cut;        \
        double *faint_cut;         \
        double *rmin;              \
        double *rmax;              \
        double *zmin;              \
        double *zmax;              \
        int N_BIAS;                \
        double *galaxy_bias_mean;  \
        double *galaxy_bias_std;   \
        double *galaxy_nmean_mean; \
        double *galaxy_nmean_std;
    #endif


    struct __SurveyGeometry
    {
        BASEFIELDMETA
        SURVEYGEOMETRYMETA
        int N_CAT;
        float_t *galaxy_sel_window;
    };
    typedef struct __SurveyGeometry SurveyGeometry;

    #ifndef GALAXYSELWINDOWMETA
        #define GALAXYSELWINDOWMETA \
        double cosmo[N_COSMOPAR];   \
        double bright_cut;          \
        double faint_cut;           \
        double rmin;                \
        double rmax;                \
        double zmin;                \
        double zmax;                \
        int N_BIAS;                 \
        double *galaxy_bias_mean;   \
        double *galaxy_bias_std;    \
        double galaxy_nmean_mean;   \
        double galaxy_nmean_std;
    #endif

    struct __GalaxySelectionWindow
    {
        BASEFIELD
        GALAXYSELWINDOWMETA
        float_t *galaxy_sel_window;
    };
    typedef struct __GalaxySelectionWindow GalaxySelectionWindow;

#endif
