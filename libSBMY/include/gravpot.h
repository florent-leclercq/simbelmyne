///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/include/gravpot.h
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file gravpot.h
 *  \brief Header for gravpot.c
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2014-2023
 */
#ifndef header_gravpot
#define header_gravpot

    #include "assignments.h"
    #include "utils.h"
    #include "fd_schemes.h"
    #include "fft.h"
    #include "poisson_solvers.h"

    float_t* get_gravitational_potential_periodic(float_t *density, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, const bool smooth, const double a, const double Omega_m);
    void get_gravitational_potential_derivatives_periodic(float_t *density, float_t *phi, float_t *dphidx, float_t *dphidy, float_t *dphidz, const int Npm0, const int Npm1, const int Npm2, const double L0, const double L1, const double L2, const bool smooth, const double a, const double Omega_m);

#endif
