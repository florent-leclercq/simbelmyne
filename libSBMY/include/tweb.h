///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/include/tweb.h
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file tweb.h
 *  \brief Header for tweb.c
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2015-2023
 */
#ifndef header_tweb
#define header_tweb

    #include "utils.h"
    #include "fft.h"

    #include "classifiers_tools.h"
    #include "snapshot.h"
    #include "density.h"
    #include "io.h"

    void tweb_analysis(const float_t *DENSITY, float_t *OBJECTS, float_t *LAMBDA1, float_t *LAMBDA2, float_t *LAMBDA3, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2);

#endif
