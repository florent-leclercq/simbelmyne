///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/include/math_utils.h
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file math_utils.h
 *  \brief Header for math_utils.c
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2015-2023
 */
#ifndef header_math_utils
#define header_math_utils

    #include "utils.h"

    #define min(a,b) (((a)<(b))?(a):(b))
    #define max(a,b) (((a)>(b))?(a):(b))
    #define sgn(x) ((x>0)?1:((x<0)?-1:0))

    #ifndef M_PI
    #define M_PI 3.14159265358979323846
    #endif

    bool sameSign(float_t x, float_t y);
    int even_ceil(double x);
    int odd_ceil(double x);
    void swap_int(int *x, int *y);
    void swap_float_t(float_t *x, float_t *y);
    void sort_array(float_t a[], int n);
    void solve_quadratic(float_t a, float_t b, float_t c, float_t RESULTS[2]);
    float_t sqrt3rd(float_t x);
    void solve_cubic(float_t a, float_t b, float_t c, float_t d, float_t RESULTS[3]);
    void get_eigvals_real(float_t T00, float_t T11, float_t T22, float_t T01, float_t T02, float_t T12, float_t EIGVALS[3]);
    float_t determinant3x3(float_t a, float_t b, float_t c, float_t d, float_t e, float_t f, float_t g, float_t h, float_t i);
    float_t determinant4x4(float_t m00, float_t m01, float_t m02, float_t m03, float_t m10, float_t m11, float_t m12, float_t m13, float_t m20, float_t m21, float_t m22, float_t m23, float_t m30, float_t m31, float_t m32, float_t m33);
    float_t linear_interpolation(const float_t* x, const float_t* y, const int l, const float_t xt);

    // External functions
    double qromb(double (*func)(double), double a, double b);

#endif
