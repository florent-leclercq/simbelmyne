///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/include/velocity.h
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file velocity.h
 *  \brief Header for velocity.c
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2013-2023
 */
#ifndef header_velocity
#define header_velocity

    #include "assignments.h"
    #include "fft.h"
    #include "structures.h"
    #include "utils.h"

    void get_velocities_pm_snapshot(const Snapshot P, const int N0, float_t *VELOCITY0, float_t *VELOCITY1, float_t *VELOCITY2);
    void get_divV_fs(const float_t *VELOCITY0, const float_t *VELOCITY1, const float_t *VELOCITY2, float_t *DIVV, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2);
    void get_rotV_fs(const float_t *VELOCITY0, const float_t *VELOCITY1, const float_t *VELOCITY2, float_t *ROTV0, float_t *ROTV1, float_t *ROTV2, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2);

#endif
