///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/include/density.h
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file density.h
 *  \brief Header for density.c
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2013-2023
 */
#ifndef header_density
#define header_density

    #include "assignments.h"
    #include "fft.h"
    #include "io.h"
    #include "structures.h"
    #include "utils.h"

    void get_density_pm_serial(const particleID_t Np, const particle_pos_t positions[Np], const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, float_t *DELTA);
    void get_density_pm_parallel_1(const particleID_t Np, const particle_pos_t positions[Np], const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, float_t *DELTA);
    void get_density_pm_parallel_2(const particleID_t Np, const particle_pos_t positions[Np], const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, float_t *DELTA);
    void get_density_pm_parallel(const particleID_t Np, const particle_pos_t positions[Np], const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, float_t *DELTA);
    void get_density_pm(const particleID_t Np, const particle_pos_t positions[Np], const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, float_t *DELTA);
    void get_density_pm_snapshot(const Snapshot P, const int N0, float_t *DELTA);
    void get_density_pm_nonperiodic_serial(const particleID_t Np, const particle_pos_t positions[Np], const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, float_t *DELTA);
    void get_density_pm_nonperiodic_parallel(const particleID_t Np, const particle_pos_t positions[Np], const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, float_t *DELTA);
    void get_density_pm_nonperiodic(const particleID_t Np, const particle_pos_t positions[Np], const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, float_t *DELTA);
    void delta_to_density(float_t *DELTA, const int N0, const int N1, const int N2);
    void delta_to_density_window(float_t *DENSITY, const float_t *GSW, const int N0, const int N1, const int N2);
    void density_to_delta(float_t *DENSITY, const double MEAN_IN, const int N0, const int N1, const int N2);
    void update_and_output_density(Field F, Snapshot P, const bool UpdateDensity, const bool WriteDensity, const double Time, const char* OutputDensity);
    void test_density(float_t *DENSITY, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2);
    bool compare_density_fields(const float_t *DENSITY1, const float_t *DENSITY2, const int N0, const int N1, const int N2, const double epsilon);

#endif
