///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/include/grf.h
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file grf.h
 *  \brief Header for grf.c
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2014-2023
 */
#ifndef header_grf
#define header_grf

    #include "fft.h"
    #include "random.h"
    #include "utils.h"

    void generate_white_noise_array(float_t *WN, const int N0, const int N1, const int N2, const uint64_t *seedtable, const int Nthreads);
    void upsample_wn(const float_t *WN_IN, const int N0, float_t *WN_OUT, const int N0_target, const char *InputRngState, const bool SaveRngState, const char *SavedRngState, const char *OutputRngState);
    void tint_or_whiten_GRF(const bool tint, float_t *FIELD, const int *k_keys, const float_t *powerspectrum, const int NUM_MODES, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2);

#endif
