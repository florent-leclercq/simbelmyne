///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/include/displfield.h
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file displfiled.h
 *  \brief Header for displfiled.c
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2015-2023
 */
#ifndef header_displfiled
#define header_displfiled

    #include "fft.h"
    #include "math_utils.h"
    #include "snapshot.h"
    #include "structures.h"
    #include "utils.h"

    void get_displfield_onecomponent(const Snapshot P, float_t *PSI, const int axis, const int Np0, const int Np1, const int Np2);
    void get_displfield(const Snapshot P, float_t *PSI0, float_t *PSI1, float_t *PSI2, const int Np0, const int Np1, const int Np2);
    void get_divPsi_fs(const float_t *PSI0, const float_t *PSI1, const float_t *PSI2, float_t *DIVPSI, const int Np0, const int Np1, const int Np2, const double L0, const double L1, const double L2);
    void get_potentialPsi_fs(const float_t *PSI0, const float_t *PSI1, const float_t *PSI2, float_t *POTENTIALPSI, const int Np0, const int Np1, const int Np2, const double L0, const double L1, const double L2);
    void inversegrad_divPsi(const float_t *DIVPSI, float_t *PSI0, float_t *PSI1, float_t *PSI2, const int Np0, const int Np1, const int Np2, const double L0, const double L1, const double L2);
    void get_normcurlpartPsi_fullminusscalar(const float_t *PSI0, const float_t *PSI1, const float_t *PSI2, float_t *NORM_CURLPARTPSI, const int Np0, const int Np1, const int Np2, const double L0, const double L1, const double L2);
    void get_normcurlpartPsi_vector(const float_t *PSI0, const float_t *PSI1, const float_t *PSI2, float_t *NORM_CURLPARTPSI, const int Np0, const int Np1, const int Np2, const double L0, const double L1, const double L2);
    void get_normcurlpartPsi(const float_t *PSI0, const float_t *PSI1, const float_t *PSI2, float_t *NORM_CURLPARTPSI, const int Np0, const int Np1, const int Np2, const double L0, const double L1, const double L2);
    void get_shearPsi(const float_t *PSI0, const float_t *PSI1, const float_t *PSI2, float_t *R00, float_t *R11, float_t *R22, float_t *R01, float_t *R02, float_t *R12, float_t *R10, float_t *R20, float_t *R21, const bool symmetric, const int Np0, const int Np1, const int Np2, const double L0, const double L1, const double L2);
    void get_shearPsi_symmetric(const float_t *PSI0, const float_t *PSI1, const float_t *PSI2, float_t *R00, float_t *R11, float_t *R22, float_t *R01, float_t *R02, float_t *R12, const int Np0, const int Np1, const int Np2, const double L0, const double L1, const double L2);
    void jacobian_l2e_potential(const float_t *PSI0, const float_t *PSI1, const float_t *PSI2, float_t* JACOBIAN, const int Np0, const int Np1, const int Np2, const double L0, const double L1, const double L2);

#endif
