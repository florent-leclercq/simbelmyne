///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/include/io.h
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file io.h
 *  \brief Header for io.c
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2013-2023
 */
#ifndef header_io
#define header_io

    #include "density.h"
    #include "hdf5.h"
    #include "snapshot.h"
    #include "utils.h"

    // Snapshot format
    #ifndef SNAPFORMAT
        #if FORMAT2
            #define SNAPFORMAT 2
        #else
            #define SNAPFORMAT 1
        #endif
    #endif

    #define IO_NBLOCKS 11   /*!< total number of defined information blocks for snapshot files.
                                 Must be equal to the number of entries in "enum iofields" */

    enum iofields /*!< this enumeration lists the defined output blocks in snapshot files. Not all of them need to be present. */
    {
        IO_POS,
        IO_VEL,
        IO_ID,
        IO_MASS,
        IO_U,
        IO_RHO,
        IO_HSML,
        IO_POT,
        IO_ACCEL,
        IO_DTENTR,
        IO_TSTP,
    };

    extern char Tab_IO_Labels[IO_NBLOCKS][4]; /*!< This table holds four-byte character tags used for fileformat 2 */

    #define allocate_basefield(rank, N0, N1, N2, corner0, corner1, corner2, L0, L1, L2, name) allocate_basefield_(rank, N0, N1, N2, corner0, corner1, corner2, L0, L1, L2, name, __POS__)
    BaseField allocate_basefield_(const int rank, const int N0, const int N1, const int N2, const double corner0, const double corner1, const double corner2, const double L0, const double L1, const double L2, char *name, char *file, char *func, int line);
    #define allocate_scalar_basefield(N0, N1, N2, corner0, corner1, corner2, L0, L1, L2, name) allocate_scalar_basefield_(N0, N1, N2, corner0, corner1, corner2, L0, L1, L2, name, __POS__)
    BaseField allocate_scalar_basefield_(const int N0, const int N1, const int N2, const double corner0, const double corner1, const double corner2, const double L0, const double L1, const double L2, char *name, char *file, char *func, int line);
    #define allocate_field(rank, N0, N1, N2, corner0, corner1, corner2, L0, L1, L2, Time, name) allocate_field_(rank, N0, N1, N2, corner0, corner1, corner2, L0, L1, L2, Time, name, __POS__)
    Field allocate_field_(const int rank, const int N0, const int N1, const int N2, const double corner0, const double corner1, const double corner2, const double L0, const double L1, const double L2, const double Time, char *name, char *file, char *func, int line);
    #define allocate_scalar_field(N0, N1, N2, corner0, corner1, corner2, L0, L1, L2, Time, name) allocate_scalar_field_(N0, N1, N2, corner0, corner1, corner2, L0, L1, L2, Time, name, __POS__)
    Field allocate_scalar_field_(const int N0, const int N1, const int N2, const double corner0, const double corner1, const double corner2, const double L0, const double L1, const double L2, const double Time, char *name, char *file, char *func, int line);
    Field field_from_data(const float_t *data, const int rank, const int N0, const int N1, const int N2, const double corner0, const double corner1, const double corner2, const double L0, const double L1, const double L2, const double Time);
    Field scalar_field_from_data(const float_t *data, const int N0, const int N1, const int N2, const double corner0, const double corner1, const double corner2, const double L0, const double L1, const double L2, const double Time);
    #define free_basefield(BF, name) free_basefield_(BF, name, __POS__)
    void free_basefield_(BaseField BF, char *name, char *file, char *func, int line);
    #define free_field(F, name) free_field_(F, name, __POS__)
    void free_field_(Field F, char *name, char *file, char *func, int line);

    void write_basefield(BaseField BF, const char *fname);
    #define read_basefield(fname, name) read_basefield_(fname, name, __POS__)
    BaseField read_basefield_(const char *fname, char *name, char *file, char *func, int line);
    BaseField read_basefield_header(const char *fname);

    void write_field(Field F, const char *fname);
    #define read_field(fname, name) read_field_(fname, name, __POS__)
    Field read_field_(const char *fname, char *name, char *file, char *func, int line);
    Field read_field_header(const char *fname);

    void write_basefield_hdf5(BaseField BF, const char *fname);
    #define read_basefield_hdf5(fname, name) read_basefield_hdf5_(fname, name, __POS__)
    BaseField read_basefield_hdf5_(const char *fname, char *name, char *file, char *func, int line);
    BaseField read_basefield_hdf5_header(const char *fname);

    void write_field_hdf5(Field F, const char *fname);
    #define read_field_hdf5(fname, name) read_field_hdf5_(fname, name, __POS__)
    Field read_field_hdf5_(const char *fname, char *name, char *file, char *func, int line);
    Field read_field_hdf5_header(const char *fname);

    void write_scalar_field_3D(Field F, const char *fname);
    void write_scalar_field_3D_hdf5(Field F, const char *fname);
    #define read_field_chunk_3D_periodic(fname, N0_chunk, N1_chunk, N2_chunk, offset_0, offset_1, offset_2, name) read_field_chunk_3D_periodic_(fname, N0_chunk, N1_chunk, N2_chunk, offset_0, offset_1, offset_2, name, __POS__)
    Field read_field_chunk_3D_periodic_(const char *fname, const int N0_chunk, const int N1_chunk, const int N2_chunk, const int offset_0, const int offset_1, const int offset_2, char *name, char *file, char *func, int line);
    #define read_field_chunk_3D_periodic_hdf5(fname, N0_chunk, N1_chunk, N2_chunk, offset_0, offset_1, offset_2, name, __POS__) read_field_chunk_3D_periodic_hdf5_(fname, N0_chunk, N1_chunk, N2_chunk, offset_0, offset_1, offset_2, name)
    Field read_field_chunk_3D_periodic_hdf5_(const char *fname, const int N0_chunk, const int N1_chunk, const int N2_chunk, const int offset_0, const int offset_1, const int offset_2, char *name, char *file, char *func, int line);
    void replace_field_chunk_3D_periodic(Field F, int offset_0, int offset_1, int offset_2, const char *fname);
    void replace_field_chunk_3D_periodic_hdf5(Field F, int offset_0, int offset_1, int offset_2, const char *fname);

    void write_particle_pos_hdf5(particleID_t Np, particle_pos_t x[Np], int Np0, int Np1, int Np2, char *fname);
    void read_particle_pos_hdf5(particleID_t Np, particle_pos_t x[Np], int Np0, int Np1, int Np2, char *fname);
    void write_particle_vel_hdf5(particleID_t Np, particle_vel_t p[Np], int Np0, int Np1, int Np2, char *fname);
    void read_particle_vel_hdf5(particleID_t Np, particle_vel_t p[Np], int Np0, int Np1, int Np2, char *fname);

    void write_tile_hdf5(particleID_t Np_tile, particle_pos_t Psi_tile[Np_tile], particle_vel_t p_tile[Np_tile], particleID_t mp_tile_corner_full, int Np0, int Np1, int Np2, int Np0_tile, int Np1_tile, int Np2_tile, double L0, double L1, double L2, double L0_tile, double L1_tile, double L2_tile, char *fname);
    void read_tile_hdf5(particleID_t Np_tile, particle_pos_t Psi_tile[Np_tile], particle_vel_t p_tile[Np_tile], particleID_t mp_tile_corner_full, int Np0, int Np1, int Np2, int Np0_tile, int Np1_tile, int Np2_tile, double L0, double L1, double L2, double L0_tile, double L1_tile, double L2_tile, char *fname);

    #define allocate_mock(N0, N1, N2, corner0, corner1, corner2, L0, L1, L2, galaxy_bias, N_BIAS, galaxy_nmean, name) allocate_mock_(N0, N1, N2, corner0, corner1, corner2, L0, L1, L2, galaxy_bias, N_BIAS, galaxy_nmean, name, __POS__)
    Mock allocate_mock_(const int N0, const int N1, const int N2, const double corner0, const double corner1, const double corner2, const double L0, const double L1, const double L2, const double *galaxy_bias, const int N_BIAS, const double galaxy_nmean, char *name, char *file, char *func, int line);
    #define free_mock(M) free_mock_(M, VARNAME(M), __POS__)
    void free_mock_(Mock M, char *name, char *file, char *func, int line);
    void write_mock(Mock M, const char *fname);

    #define free_survey_geometry_header(SG) free_survey_geometry_header_(SG, VARNAME(SG), __POS__)
    void free_survey_geometry_header_(SurveyGeometry SG, char *name, char *file, char *func, int line);
    #define read_survey_geometry_header(fname, name) read_survey_geometry_header_(fname, name, __POS__)
    SurveyGeometry read_survey_geometry_header_(const char *fname, char *name, char *file, char *func, int line);

    #define free_galaxy_selection_window(GSW) free_galaxy_selection_window_(GSW, VARNAME(GSW), __POS__)
    void free_galaxy_selection_window_(GalaxySelectionWindow GSW, char *name, char *file, char *func, int line);
    #define read_galaxy_selection_window(fname, ICAT, name) read_galaxy_selection_window_(fname, ICAT, name, __POS__)
    GalaxySelectionWindow read_galaxy_selection_window_(const char *fname, const int ICAT, char *name, char *file, char *func, int line);

    #define free_FourierGrid(Gk) free_FourierGrid_(Gk, VARNAME(Gk), __POS__)
    void free_FourierGrid_(FourierGrid Gk, char *name, char *file, char *func, int line);
    void write_FourierGrid(FourierGrid Gk, const char *fname);
    #define read_FourierGrid(fname, name) read_FourierGrid_(fname, name, __POS__)
    FourierGrid read_FourierGrid_(const char *fname, char *name, char *file, char *func, int line);

    #define free_PowerSpectrum(Pk) free_PowerSpectrum_(Pk, VARNAME(Pk), __POS__)
    void free_PowerSpectrum_(PowerSpectrum Pk, char *name, char *file, char *func, int line);
    void write_PowerSpectrum(PowerSpectrum Pk, const char *fname);
    #define read_PowerSpectrum(fname, name) read_PowerSpectrum_(fname, name, __POS__)
    PowerSpectrum read_PowerSpectrum_(const char *fname, char *name, char *file, char *func, int line);

    void write_ss(FourierGrid Gk, const int N_CAT, const int N_NOISE, const int N_BIAS, double galaxy_bias[N_CAT][N_NOISE][N_BIAS], double galaxy_nmean[N_CAT][N_NOISE], float_t Pk[N_CAT][N_NOISE][Gk.NUM_MODES], float_t Vk[N_CAT][N_NOISE][Gk.NUM_MODES], const char *fname);

    #define free_snapshot(P, name) free_snapshot_(P, name, __POS__)
    void free_snapshot_(Snapshot P, char *name, char *file, char *func, int line);
    header_t read_header(const char* fname, const int SnapFormat);
    Snapshot read_snapshot(const char* fname, const int SnapFormat);
    void write_snapshot(Snapshot P, const char* fname, const int files, const int SnapFormat);
    void output_snapshot(const bool WriteSnapshot, const Snapshot P, const char* OutputSnapshot, const int NumFilesPerSnapshot, const int SnapFormat);

#endif
