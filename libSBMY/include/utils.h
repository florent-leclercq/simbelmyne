///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/include/utils.h
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file utils.h
 *  \brief Header for utils.c
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2013-2023
 */
#ifndef header_utils
#define header_utils

    #include <assert.h>
    #include <complex.h>
    #include <fftw3.h>
    #include <math.h>
    #include <mm_malloc.h>
    #include <omp.h>
    #include <stdlib.h>
    #include <stdint.h>
    #include <stdbool.h>
    #include <stdio.h>
    #include <string.h>
    #include <sys/sysinfo.h>
    #include <sys/time.h>
    #include <sys/resource.h>
    #include <time.h>
    #include <unistd.h>

    #define VARNAME(v) #v
    #define UNUSED(x) (void)(x)
    #define __POS__ (char *)__FILE__, (char *)__func__, (int)__LINE__

    #ifdef DOUBLEPRECISION
        #define float_t double
        #define H5T_FLOAT_T H5T_NATIVE_DOUBLE
    #else
        #define float_t float
        #define H5T_FLOAT_T H5T_NATIVE_FLOAT
    #endif

    #ifdef LONGIDS
        typedef unsigned long long particleID_t;
        typedef unsigned long long fourierMode_t;
        typedef unsigned long long cellIndex_t;
        #define H5T_PARTICLEID_T H5T_NATIVE_ULLONG
    #else
        typedef unsigned int particleID_t;
        typedef unsigned int fourierMode_t;
        typedef unsigned int cellIndex_t;
        #define H5T_PARTICLEID_T H5T_NATIVE_UINT
    #endif
    typedef unsigned long long massparticleID_t;
    typedef clock_t CPUtime;
    typedef struct timeval Walltime;

    // Print in color on the terminal
    #define FONT_BLACK           "\033[0;30m"
    #define FONT_RED             "\033[0;31m"
    #define FONT_LIGHTRED        "\033[0;91m"
    #define FONT_BURGUNDY        "\033[38;5;169m"
    #define FONT_GREEN           "\033[0;32m"
    #define FONT_LIGHTGREEN      "\033[38;5;113m"
    #define FONT_LIGHTERGREEN    "\033[38;5;192m"
    #define FONT_ORANGE          "\033[38;5;215m"
    #define FONT_YELLOW          "\033[38;5;227m"
    #define FONT_BLUE            "\033[0;34m"
    #define FONT_LIGHTBLUE       "\033[38;5;38m"
    #define FONT_LIGHTERBLUE     "\033[38;5;159m"
    #define FONT_PURPLE          "\033[0;35m"
    #define FONT_LIGHTPURPLE     "\033[38;5;147m"
    #define FONT_PINK            "\033[38;5;207m"
    #define FONT_PYTHONPINK      "\033[38;5;219m"
    #define FONT_CYAN            "\033[0;36m"
    #define FONT_LIGHTCYAN       "\033[38;5;117m"
    #define FONT_GREY            "\033[38;5;246m"

    #define FONT_BOLDBLACK       "\033[1;30m"
    #define FONT_BOLDRED         "\033[1;31m"
    #define FONT_BOLDGREEN       "\033[1;32m"
    #define FONT_BOLDYELLOW      "\033[1;33m"
    #define FONT_BOLDBLUE        "\033[1;34m"
    #define FONT_BOLDPURPLE      "\033[1;35m"
    #define FONT_BOLDCYAN        "\033[1;36m"
    #define FONT_BOLDGREY        "\033[1;37m"

    #define FONT_SIMBELMYNE      "\033[1;38;5;157m"
    #define FONT_NORMAL          "\033[00m"

    // Global timers
    #ifdef TIMERS
        extern CPUtime startm_simbelmyne, stopm_simbelmyne; extern double diff_t_cpu_simbelmyne;
        #define StartSimbelmyneCPUTimer() if( (startm_simbelmyne = clock()) == -1) {FatalError_("Error calling CPU clock", "include/utils.h", "debugStartSimbelmyneCPUTimer", 89);}
        #define StopSimbelmyneCPUTimer() if( (stopm_simbelmyne = clock()) == -1) {FatalError_("Error calling CPU clock", "include/utils.h", "debugStopSimbelmyneCPUTimer", 90);} else {diff_t_cpu_simbelmyne = ((double)stopm_simbelmyne-startm_simbelmyne)/CLOCKS_PER_SEC;}
        extern Walltime startw_simbelmyne, stopw_simbelmyne; extern double diff_t_wall_simbelmyne;
        #define StartSimbelmyneWallTimer() if( gettimeofday(&startw_simbelmyne,0) == -1) {FatalError_("Error calling wallclock", "include/utils.h", "debugStartSimbelmyneWallTimer", 92);}
        #define StopSimbelmyneWallTimer() if( gettimeofday(&stopw_simbelmyne,0) == -1) {FatalError_("Error calling wallclock", "include/utils.h", "debugStopSimbelmyneWallTimer", 93);} else {diff_t_wall_simbelmyne = ((stopw_simbelmyne.tv_sec - startw_simbelmyne.tv_sec) * 1000.0f + (stopw_simbelmyne.tv_usec - startw_simbelmyne.tv_usec) / 1000.0f)/1000.0f;}

        extern CPUtime startm_module, stopm_module; extern double diff_t_cpu_module;
        #define StartModuleCPUTimer() if( (startm_module = clock()) == -1) {FatalError_("Error calling CPU clock", "include/utils.h", "debugStartModuleCPUTimer", 96);}
        #define StopModuleCPUTimer() if( (stopm_module = clock()) == -1) {FatalError_("Error calling CPU clock", "include/utils.h", "debugStopModuleCPUTimer", 97);} else {diff_t_cpu_module = ((double)stopm_module-startm_module)/CLOCKS_PER_SEC;}
        extern Walltime startw_module, stopw_module; extern double diff_t_wall_module;
        #define StartModuleWallTimer() if( gettimeofday(&startw_module,0) == -1) {FatalError_("Error calling wallclock", "include/utils.h", "debugStartModuleWallTimer", 99);}
        #define StopModuleWallTimer() if( gettimeofday(&stopw_module,0) == -1) {FatalError_("Error calling wallclock", "include/utils.h", "debugStopModuleWallTimer", 100);} else {diff_t_wall_module = ((stopw_module.tv_sec - startw_module.tv_sec) * 1000.0f + (stopw_module.tv_usec - startw_module.tv_usec) / 1000.0f)/1000.0f;}
    #endif

    bool exists(const char *file);
    double p_mod(const double x, const double y);
    #define p_malloc(size, name) p_malloc_(size, name, __POS__);
    void* p_malloc_(const size_t size, char *name, char *file, char *func, int line);
    #define p_calloc(nitems, size, name) p_calloc_(nitems, size, name, __POS__);
    void* p_calloc_(const size_t nitems, const size_t size, char *name, char *file, char *func, int line);
    #define p_fftw_malloc(size, name) p_fftw_malloc_(size, name, __POS__);
    void* p_fftw_malloc_(const size_t size, char *name, char *file, char *func, int line);
    #define p_free(pointer, name) p_free_(pointer, name, __POS__);
    void p_free_(void *pointer, char *name, char *file, char *func, int line);
    #define p_fftw_free(pointer, name) p_fftw_free_(pointer, name, __POS__);
    void p_fftw_free_(void *pointer, char *name, char *file, char *func, int line);
    #define get_Nthreads_from_available_memory(nitems, size, Nthreads_max) get_Nthreads_from_available_memory_(nitems, size, Nthreads_max, __POS__)
    int get_Nthreads_from_available_memory_(const long long nitems, const long long size, const int Nthreads_max, char *file, char *func, int line);
    #define p_fwrite(ptr, size, nmemb, stream) p_fwrite_(ptr, size, nmemb, stream, __POS__);
    size_t p_fwrite_(void *ptr, size_t size, size_t nmemb, FILE *stream, char *file, char *func, int line);
    #define p_fread(ptr, size, nmemb, stream) p_fread_(ptr, size, nmemb, stream, __POS__);
    size_t p_fread_(void *ptr, size_t size, size_t nmemb, FILE *stream, char *file, char *func, int line);
    #define p_fgets(s, size, stream) p_fgets_(s, size, stream, __POS__);
    char* p_fgets_(char *s, int size, FILE *stream, char *file, char *func, int line);
    char* strreplace(char *str, char in, char out);
    void strremovechar(char *str, char in);
    bool file_compare_str(const char *n1, const char *n2);
    bool compareDoubleArray(const double *t1, const double *t2, const int N);
    void change_rm2cm(const double *A, double *B, const int N0, const int N1, const int N2);
    void get_longlong_indices(const long long Id, const int N0, const int N1, const int N2, int *indices);
    void get_indices(const int Id, const int N0, const int N1, const int N2, int *indices);
    cellIndex_t get_index(const int mc0, const int mc1, const int mc2, const int N0, const int N1, const int N2);
    cellIndex_t get_index_in_paddedbox(const int mc0, const int mc1, const int mc2, const int N0, const int N1, const int N2, const int N0_pad, const int N1_pad, const int N2_pad);
    #define FormatFunction(buf) FormatFunction_(buf, __POS__);
    void FormatFunction_(char *buf, char *file, char *func, int line);
    void PrintState(char *file, char *func, int line);
    void PrintLeftType(struct _IO_FILE *std, char *type, char *FONT_COLOR);
    void PrintBareMessage(char *message, char *file, char *func, int line);
    #define PrintMessage(verbosity, message) PrintMessage_(verbosity, message, __POS__);
    void PrintMessage_(int verbosity, char *message, char *file, char *func, int line);
    #define PrintMessageNoBreak(verbosity, message) PrintMessageNoBreak_(verbosity, message, __POS__);
    void PrintMessageNoBreak_(int verbosity, char *message, char *file, char *func, int line);
    #define PrintInfo(message) PrintInfo_(message, __POS__);
    void PrintInfo_(char *message, char *file, char *func, int line);
    #define PrintInfoNoBreak(message) PrintInfoNoBreak_(message, __POS__);
    void PrintInfoNoBreak_(char *message, char *file, char *func, int line);
    #define PrintModule(message) PrintModule_(message, __POS__);
    void PrintModule_(char *message, char *file, char *func, int line);
    #define PrintModuleNoBreak(message) PrintModuleNoBreak_(message, __POS__);
    void PrintModuleNoBreak_(char *message, char *file, char *func, int line);
    #define PrintDiagnostic(verbosity, message) PrintDiagnostic_(verbosity, message, __POS__);
    void PrintDiagnostic_(int verbosity, char *message, char *file, char *func, int line);
    #define PrintDiagnosticNoBreak(verbosity, message) PrintDiagnosticNoBreak_(verbosity, message, __POS__);
    void PrintDiagnosticNoBreak_(int verbosity, char *message, char *file, char *func, int line);
    #define PrintMemory(message) PrintMemory_(message, __POS__);
    void PrintMemory_(char *message, char *file, char *func, int line);
    #define PrintTimer(message) PrintTimer_(message, __POS__);
    void PrintTimer_(char *message, char *file, char *func, int line);
    void PrintCommandLine(int argc, char **argv);
    #define PrintWarning(message) PrintWarning_(message, __POS__);
    void PrintWarning_(char *message, char *file, char *func, int line);
    #define PrintWarningNoBreak(message) PrintWarningNoBreak_(message, __POS__);
    void PrintWarningNoBreak_(char *message, char *file, char *func, int line);
    #define PrintError(message) PrintError_(message, __POS__);
    void PrintError_(char *message,  char *in_file, char *in_func, int in_line);
    #define PrintErrorNoBreak(message) PrintErrorNoBreak_(message, __POS__);
    void PrintErrorNoBreak_(char *message, char *in_file, char *in_func, int in_line);
    #define FatalError(message) FatalError_(message, __POS__);
    void FatalError_(char *message, char *file, char *func, int line);
    #define FatalErrorNoMessage(message) FatalErrorNoMessage_(message, __POS__);
    void FatalErrorNoMessage_(char *file, char *func, int line);
    #define p_assert(value) p_assert__(VARNAME(value), value, __POS__);
    #define p_assert_(condition, value) p_assert__(condition, value, __POS__);
    void p_assert__(char *condition, bool value, char *file, char *func, int line);
    void StartCPUTimer(CPUtime *startm, char *file, char *func, int line);
    void StopCPUTimer(CPUtime startm, double *diff_t_cpu, char *file, char *func, int line);
    void StartWallTimer(Walltime *startw, char *file, char *func, int line);
    void StopWallTimer(Walltime startw, double *diff_t_wall, char *file, char *func, int line);
    #define StartTimers(startm, startw) StartTimers_(startm, startw, __POS__)
    void StartTimers_(CPUtime *startm, Walltime *startw, char *file, char *func, int line);
    #define StopTimers(startm, startw, diff_t_cpu, diff_t_wall) StopTimers_(startm, startw, diff_t_cpu, diff_t_wall, __POS__)
    void StopTimers_(CPUtime startm, Walltime startw, double *diff_t_cpu, double *diff_t_wall, char *file, char *func, int line);
    #define PrintTimers(type, diff_t_cpu, diff_t_wall) PrintTimers_(type, diff_t_cpu, diff_t_wall, __POS__)
    void PrintTimers_(char *type, double diff_t_cpu, double diff_t_wall, char *file, char *func, int line);
    #define StartModuleTimers() StartModuleTimers_(__POS__)
    void StartModuleTimers_(char *file, char *func, int line);
    #define StopPrintModuleTimers(ModuleName) StopPrintModuleTimers_(ModuleName, __POS__)
    void StopPrintModuleTimers_(char *ModuleName, char *file, char *func, int line);
    #define Begin() Begin_(__POS__)
    void Begin_(char *file, char *func, int line);
    #define EndSuccess() EndSuccess_(__POS__)
    void EndSuccess_(char *file, char *func, int line);

    // Global variable for printing messages to screen
    #define BUFLENGTH 1024
    extern char G__msg__[5000]; extern int G__ind__;
    #define INDENT {G__ind__++;}
    #define UNINDENT {G__ind__--;}
    // Global variables for counting calls to malloc and free
    extern int G__count_malloc__, G__count_free__;
    // Global variables for logs
    extern FILE *stdlog; extern bool ISSET_LOGS;
    // Global variables for verbosity
    #define COMMAND_VERBOSITY 1
    #define ERROR_VERBOSITY 1
    #define INFO_VERBOSITY 1
    #define MODULE_VERBOSITY 2
    #define WARNING_VERBOSITY 3
    #define TIMER_VERBOSITY 4
    #define DEBUG_VERBOSITY 6
    #define MEMORY_VERBOSITY 7

    // External functions
    extern char *strdup(const char *s);

    #include "debug.h"

#endif
