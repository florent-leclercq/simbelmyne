///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/include/diva.h
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file diva.h
 *  \brief Header for diva.c
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2015-2023
 */
#ifndef header_diva
#define header_diva

    #include "assignments.h"
    #include "fft.h"
    #include "math_utils.h"
    #include "utils.h"

    #include "classifiers_tools.h"
    #include "displfield.h"
    #include "io.h"
    #include "snapshot.h"

    void diva_analysis(const float_t *PSI0, const float_t *PSI1, const float_t *PSI2, float_t *OBJECTS, float_t *LAMBDA1, float_t *LAMBDA2, float_t *LAMBDA3, const int Np0, const int Np1, const int Np2, const double L0, const double L1, const double L2);

#endif
