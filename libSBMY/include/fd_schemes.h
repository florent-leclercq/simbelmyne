///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/include/fd_schemes.h
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file fd_schemes.h
 *  \brief Header for fd_schemes.c
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2017-2023
 */
#ifndef header_fd_schemes
#define header_fd_schemes

    #include "utils.h"

    #ifndef Nghost
        #if FDA==2
        #define Nghost 1
        #elif FDA==4
        #define Nghost 2
        #elif FDA>=6 || FDA==0
        #define Nghost 3
        #endif
    #endif

    double laplace_DFT_1d(const double k, const double d);
    double laplace_DFT(const double kx, const double ky, const double kz, const double d0, const double d1, const double d2);
    double laplace_RODFT00_1d(const double k, const double d, const int N);
    double laplace_RODFT00(const double kx, const double ky, const double kz, const double d0, const double d1, const double d2, const int N0, const int N1, const int N2);
    double gradient_DFT_1d(const double k, const double d);
    double gradient_RODFT00_1d(const double k, const double d, const int N);
    float_t gradient_rs_1d(const float_t *A, const int d, const int i, const int j, const int k, const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, const int boundary_conditions, const int N0_pad, const int N1_pad, const int N2_pad);
    float_t laplace_rs_1d(const float_t *A, const int d, const int i, const int j, const int k, const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, const int boundary_conditions, const int N0_pad, const int N1_pad, const int N2_pad);
    float_t flux_rs_1d(const float_t *A, const int d, const int i, const int j, const int k, const int N0, const int N1, const int N2, const double d0, const double d1, const double d2, const int boundary_conditions, const int N0_pad, const int N1_pad, const int N2_pad);

#endif
