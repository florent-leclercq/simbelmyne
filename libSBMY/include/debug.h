///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/include/debug.h
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file debug.h
 *  \brief Header for debug.c
 *  \authors Florent Leclercq
 *  \version 0.5.0
 *  \date 2013-2023
 */
#ifndef header_debug
#define header_debug

    #include "math_utils.h"
    #include "utils.h"

    #define DEBUG_MAX_CALLS 30000
    #define DEBUG_MAX_ARRAY_ELEMENTS 100

    #define debugPrintDiagnostic(message) debugPrintDiagnostic_(message, __POS__);
    void debugPrintDiagnostic_(char *message, char *file, char *func, int line);
    #define debugPrintMessage(message) debugPrintMessage_(message, __POS__);
    void debugPrintMessage_(char *message, char *file, char *func, int line);
    void debugPrintStatus(void);
    void debugHelloWorld(void);

    #define debugPrintInt(a) debugPrintInt_(VARNAME(a), a);
    void debugPrintInt_(char *name, int a);
    #define debugPrintUnsignedInt(a) debugPrintUnsignedInt_(VARNAME(a), a);
    void debugPrintUnsignedInt_(char *name, unsigned int a);
    #define debugPrintLong(l) debugPrintLong_(VARNAME(l), l);
    void debugPrintLong_(char *name, long l);
    #define debugPrintLongLong(l) debugPrintLongLong_(VARNAME(l), l);
    void debugPrintLongLong_(char *name, long long l);
    #define debugPrintUInt64(l) debugPrintUInt64_(VARNAME(l), l);
    void debugPrintUInt64_(char *name, uint64_t l);
    #define debugPrintBool(b, expected) debugPrintBool_(VARNAME(b), b, expected);
    void debugPrintBool_(char *name, bool b, bool expected);
    #define debugPrintFloat(f) debugPrintFloat_(VARNAME(f), f);
    void debugPrintFloat_(char *name, float f);
    #define debugPrintDouble(d) debugPrintDouble_(VARNAME(d), d);
    void debugPrintDouble_(char *name, double d);
    #define debugPrintFloatt(f) debugPrintFloatt_(VARNAME(f), f);
    void debugPrintFloatt_(char *name, float_t f);
    #define debugPrintChar(c) debugPrintChar_(VARNAME(c), c);
    void debugPrintChar_(char *name, char c);
    #define debugPrintString(s) debugPrintString_(VARNAME(s), s);
    void debugPrintString_(char *name, char *s);
    #define debugPrintParticleIDt(k) debugPrintParticleIDt_(VARNAME(k), k);
    void debugPrintParticleIDt_(char *name, particleID_t k);

    #define debugPrintIntArray(a, length) debugPrintIntArray_(VARNAME(a), a, length);
    void debugPrintIntArray_(char *name, int *a, int length);
    #define debugPrintUnsignedIntArray(a, length) debugPrintUnsignedIntArray_(VARNAME(a), a, length);
    void debugPrintUnsignedIntArray_(char *name, unsigned int *a, int length);
    #define debugPrintLongArray(l, length) debugPrintLongArray(VARNAME(l), l, length);
    void debugPrintLongArray_(char *name, long *l, int length);
    #define debugPrintLongLongArray(l, length) debugPrintLongLongArray_(VARNAME(l), l, length);
    void debugPrintLongLongArray_(char *name, long long *l, int length);
    #define debugPrintUInt64Array(l, length) debugPrintUInt64Array_(VARNAME(l), l, length);
    void debugPrintUInt64Array_(char *name, uint64_t *l, int length);
    #define debugPrintBoolArray(b, length) debugPrintBoolArray_(VARNAME(b), b, length);
    void debugPrintBoolArray_(char *name, bool *b, int length);
    #define debugPrintFloatArray(f, length) debugPrintFloatArray_(VARNAME(f), f, length);
    void debugPrintFloatArray_(char *name, float *f, int length);
    #define debugPrintDoubleArray(d, length) debugPrintDoubleArray_(VARNAME(d), d, length);
    void debugPrintDoubleArray_(char *name, double *d, int length);
    #define debugPrintFloattArray(f, length) debugPrintFloattArray_(VARNAME(f), f, length);
    void debugPrintFloattArray_(char *name, float_t *f, int length);
    #define debugPrintStringArray(s, length) debugPrintStringArray_(VARNAME(s), s, length);
    void debugPrintStringArray_(char *name, char **s, int length);
    #define debugPrintParticleIDtArray(k, length) debugPrintParticleIDtArray_(VARNAME(k), k, length);
    void debugPrintParticleIDtArray_(char *name, particleID_t *k, int length);

#endif
