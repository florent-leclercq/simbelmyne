///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/include/fft.h
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file fft.h
 *  \brief Header for fft.c
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2013-2023
 */
#ifndef header_fft
#define header_fft

    #include <complex.h>
    #include <fftw3.h>

    #include "fd_schemes.h"
    #include "utils.h"

    #ifdef DOUBLEPRECISION_FFTW
        #define fftw_complex_t fftw_complex
        #define fftw_plan_t fftw_plan
        #define fftw_real double
    #else
        #define fftw_complex_t fftwf_complex
        #define fftw_plan_t fftwf_plan
        #define fftw_real float
    #endif

    #define FFTW_OPTION FFTW_ESTIMATE
    #define re(c) creal(c)
    #define im(c) cimag(c)

    void FFT_r2c_3d(int N0, int N1, int N2, float_t *in, fftw_complex_t *out, int Nthreads);
    void FFT_c2r_3d(int N0, int N1, int N2, fftw_complex_t *in, float_t *out, int Nthreads);
    double get_kmode(const int l, const int N, const double L);
    double get_ksquared(const int mf0, const int mf1, const int mf2, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2);
    double get_kmodulus(const int mf0, const int mf1, const int mf2, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2);
    double get_kmax(const int N0, const int N1, const int N2, const double L0, const double L1, const double L2);
    double get_knyq(const int N0, const int N1, const int N2, const double L0, const double L1, const double L2);
    double get_kmode_grad_DFT(const int mf0, const int mf1, const int mf2, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2, const double d0, const double d1, const double d2, const int axis);
    fourierMode_t get_FourierMode_Id(const int mf0, const int mf1, const int mf2, const int N0, const int N1, const int N2);
    void get_FourierGrid(float_t *FOURIERGRID, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2);
    void get_FourierGrid_from_k_modes(const int Nk, const float_t k_modes[Nk], const float_t kmax, int *k_keys, int k_nmodes[Nk], const int N0, const int N1, const int N2, const double L0, const double L1, const double L2);

    void gaussian_smoother(const float_t *OLD, float_t *NEW, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2, const double rsmooth);
    void gaussian_smoother_inplace(float_t *FIELD, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2, const double rsmooth);
    void boxcar_smoother(const float_t *OLD, float_t *NEW, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2, const double rsmooth);
    void boxcar_smoother_inplace(float_t *FIELD, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2, const double rsmooth);

    void gradient_of_scalar(const float_t *SCALAR, float_t *VECTOR0, float_t *VECTOR1, float_t *VECTOR2, const bool inverseLaplacian, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2);
    void shear_of_scalar(const float_t *SCALAR, float_t *T00, float_t *T11, float_t *T22, float_t *T01, float_t *T02, float_t *T12, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2);
    void divergence_of_vector(const float_t *VECTOR0, const float_t *VECTOR1, const float_t *VECTOR2, float_t *SCALAR, const bool inverseLaplacian, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2);
    void rotational_of_vector(const float_t *VECTOR0, const float_t *VECTOR1, const float_t *VECTOR2, float_t *ROTATIONAL0, float_t *ROTATIONAL1, float_t *ROTATIONAL2, const bool inverseLaplacian, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2);
    void vectorpotential_of_vector(const float_t *VECTOR0, const float_t *VECTOR1, const float_t *VECTOR2, float_t *VECTOR_POTENTIAL0, float_t *VECTOR_POTENTIAL1, float_t *VECTOR_POTENTIAL2, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2);
    void curlpart_of_vector(const float_t *VECTOR0, const float_t *VECTOR1, const float_t *VECTOR2, float_t *CURL_PART0, float_t *CURL_PART1, float_t *CURL_PART2, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2);
    void normcurlpart_of_vector(const float_t *VECTOR0, const float_t *VECTOR1, const float_t *VECTOR2, float_t *NORM_CURL_PART, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2);
    void shear_of_vector(const float_t *VECTOR0, const float_t *VECTOR1, const float_t *VECTOR2, float_t *R00, float_t *R11, float_t *R22, float_t *R01, float_t *R02, float_t *R12, float_t *R10, float_t *R20, float_t *R21, const bool symmetric, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2);

#endif
