///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/include/dmsheet.h
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file dmsheet.h
 *  \brief Header for dmsheet.c
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2016-2023
 */
#ifndef header_dmsheet
#define header_dmsheet

    #include "assignments.h"
    #include "displfield.h"
    #include "math_utils.h"
    #include "snapshot.h"
    #include "utils.h"

    void get_tetrahedron_indices(const particleID_t Id, const int itet, const int Np0, const int Np1, const int Np2, particleID_t *TetrahedronIndices);
    void get_tetrahedron_coords(const Snapshot P, const int mpA, const int mpB, const int mpC, const int mpD, float_t *TetrahedronCoords);
    float_t get_tetrahedron_volume(const float_t *TetrahedronCoords);
    void get_masstracer_coords(const float_t *TetrahedronCoords, const int iside, float_t *MassTracerCoords);
    bool isInTetrahedron(const float_t *TetrahedronCoords, const float_t D0, const float_t xP, const float_t yP, const float_t zP);
    void project_tetrahedron(const float_t *TetrahedronCoords, const int N0, const int N1, const int N2, const double L0, const double L1, const double L2, const bool SWITCH1, float_t *GRID1, const float_t f1A, const float_t f1B, const float_t f1C, const float_t f1D, const bool SWITCH2, float_t *GRID2, const float_t f2A, const float_t f2B, const float_t f2C, const float_t f2D, const bool SWITCH3, float_t *GRID3, const float_t f3A, const float_t f3B, const float_t f3C, const float_t f3D, const bool SWITCH4, float_t *GRID4, const float_t f4A, const float_t f4B, const float_t f4C, const float_t f4D, const bool SWITCH5, float_t *GRID5, const float_t f5A, const float_t f5B, const float_t f5C, const float_t f5D, const bool SWITCH6, float_t *GRID6, const float_t f6A, const float_t f6B, const float_t f6C, const float_t f6D, const bool SWITCH7, float_t *GRID7, const float_t f7A, const float_t f7B, const float_t f7C, const float_t f7D, const bool SWITCH8, float_t *GRID8, const float_t f8A, const float_t f8B, const float_t f8C, const float_t f8D, const bool SWITCH9, float_t *GRID9, const float_t f9A, const float_t f9B, const float_t f9C, const float_t f9D, int *mc0Min, int *mc0Max, int *mc1Min, int *mc1Max, int *mc2Min, int *mc2Max);
    Snapshot get_masstracers(const Snapshot P_FLOW, const int Np0, const int Np1, const int Np2);
    void lagrangian_transport(const Snapshot P_FLOW, const float_t *OBJECTS, const int Np0, const int Np1, const int Np2, const int N0, const int N1, const int N2, const bool doDENSITYMASSTRACERS, float_t *DENSITYMASSTRACERS, const bool doPRIMORDIALSTREAMDENSITY, float_t *PRIMORDIALSTREAMDENSITY, const bool doSECONDARYSTREAMDENSITY, float_t *SECONDARYSTREAMDENSITY, const bool doDENSITYTETRAHEDRA, float_t *DENSITYTETRAHEDRA, const bool doVELOCITY, float_t *VELOCITY_X, float_t *VELOCITY_Y, float_t *VELOCITY_Z, const bool doSTRUCTURES, float_t *VOIDS, float_t *SHEETS, float_t *FILAMENTS, float_t *CLUSTERS);
    void get_density_masstracers(const Snapshot P_FLOW, const int Np0, const int Np1, const int Np2, const int N0, const int N1, const int N2, float_t *DENSITYMASSTRACERS);
    void get_density_tetrahedra(const Snapshot P_FLOW, const int Np0, const int Np1, const int Np2, const int N0, const int N1, const int N2, float_t *DENSITYTETRAHEDRA);
    void get_stream_density(const Snapshot P_FLOW, const int Np0, const int Np1, const int Np2, const int N0, const int N1, const int N2, float_t *PRIMORDIALSTREAMDENSITY, float_t *SECONDARYSTREAMDENSITY);
    void lagrangian_transport_velocities(const Snapshot P_FLOW, const int Np0, const int Np1, const int Np2, const int N0, const int N1, const int N2, float_t *VELOCITY_X, float_t *VELOCITY_Y, float_t *VELOCITY_Z);
    void lagrangian_transport_structures(const Snapshot P_FLOW, const float_t *OBJECTS, const int Np0, const int Np1, const int Np2, const int N0, const int N1, const int N2, float_t *VOIDS, float_t *SHEETS, float_t *FILAMENTS, float_t *CLUSTERS);

#endif
