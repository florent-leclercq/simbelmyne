///-------------------------------------------------------------------------------------
/*! Simbelmynë v0.5.0 -- libSBMY/include/origami.h
 * Copyright (C) 2012-2023 Florent Leclercq.
 *
 * This file is part of the Simbelmynë distribution
 * (https://bitbucket.org/florent-leclercq/simbelmyne/)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * The text of the license is located in the root directory of the source package.
 */
///-------------------------------------------------------------------------------------
/*! \file origami.h
 *  \brief Header for origami.c
 *  \author Florent Leclercq
 *  \version 0.5.0
 *  \date 2015-2023
 */
#ifndef header_origami
#define header_origami

    #include "fft.h"
    #include "utils.h"

    #include "classifiers_tools.h"
    #include "io.h"
    #include "snapshot.h"

    #define goodmod(A,B) (((A) >= (B)) ? (A-B):(((A) < 0) ? (A+B):(A)))

    void origami_analysis(Snapshot P, float_t *OBJECTS, int Np0, int Np1, int Np2, double L0, double L1, double L2);

#endif
