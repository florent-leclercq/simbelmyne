#-------------------------------------------------------------------------------------
# Simbelmynë v0.5.0 -- makefile
# Copyright (C) 2012-2023 Florent Leclercq.
#
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# The text of the license is located in the root directory of the source package.
#-------------------------------------------------------------------------------------
# \file makefile
# \brief Makefile for Simbelmynë. In principle it shouldn't
#     be necessary to edit this file.
# \authors Florent Leclercq
# \version 0.5.0
# \date 2012-2023

#--------------------------------------- Define objects, headers, sources

HDRSDIR				= include
SRCSDIR				= src
BUILDEXECDIR			= build
BUILDSODIR			= pysbmy/build
EXTERNALDIR			= external
EXTENSIONSDIR			= extensions

NRDIR				= $(EXTERNALDIR)/nrsrc

LIBSBMYHDRSDIR			= libSBMY/include
LIBSBMYSRCSDIR			= libSBMY/src

NRHDRS				= $(NRDIR)/nrutil.h

LIBSBMYSRCS			= assignments.c classifiers_tools.c correlations.c cosmology.c debug.c density.c displfield.c diva.c dmsheet.c fd_schemes.c fft.c gravpot.c grf.c io.c lich.c math_utils.c poisson_solvers.c origami.c random.c reo_fft.c snapshot.c tweb.c utils.c velocity.c

SRCS				= lpt.c main.c mock.c pmcola.c power.c read_param.c rsd.c scola.c
NRSRCS				= nrutil.c qromb.c polint.c trapzd.c

OBJS				= $(LIBSBMYSRCS:.c=.o) $(SRCS:.c=.o) $(NRSRCS:.c=.o)
OBJS_SBMY			= $(OBJS) main_sbmy.o
OBJS_SCOLA			= $(OBJS) main_scola.o

SBMY				= $(BUILDEXECDIR)/simbelmyne
SCOLA				= $(BUILDEXECDIR)/scola
SO				= $(BUILDSODIR)/simbelmyne.so

HASH				= $(shell git rev-parse --verify HEAD)
OPT				+= -DHASH='"$(HASH)"'

MKCLEANLIST			= $(BUILDEXECDIR) $(BUILDSODIR) $(LIBSBMYHDRSDIR)/*~ $(HDRSDIR)/*~ $(LIBSBMYSRCSDIR)/*~ $(SRCSDIR)/*~ dist/ pysbmy.egg-info/ pysbmy/__pycache__ extensions/__pycache__ extensions/*/pysbmy/__pycache__ *~

#---------------------------------------

include config.mk

EXTENSIONSMKFILES		= $(addprefix $(EXTENSIONSDIR)/, $(addsuffix /load_ext.mk, $(EXTENSIONS)))
-include $(EXTENSIONSMKFILES)

INCLUDES			+= -I $(HDRSDIR) -I $(LIBSBMYHDRSDIR)

SUFFIXES			?= .c .o
.SUFFIXES: $(SUFFIXES) .

.PHONY: all call exec toolsso pysbmy opt clean cleanexec cleantoolsso cleanall

CFLAGS				= $(OPTIMIZE) $(INCLUDES) $(OPT)

#--------------------------------------- Define targets

all: $(SBMY) $(SCOLA) $(SO)

call: cleanall all

exec: $(SBMY) $(SCOLA)

toolsso: $(SO)

pysbmy:
	$(PIP) install .

$(BUILDEXECDIR)/%.o:$(LIBSBMYSRCSDIR)/%.c $(LIBSBMYHDRSDIR)/*.h config.mk makefile
	$(MKDIR) $(BUILDEXECDIR)
	$(CC) $(CFLAGS) $(PYTHONFLAGS) -c $< -o $@

$(BUILDEXECDIR)/%.o:$(SRCSDIR)/%.c $(LIBSBMYHDRSDIR)/*.h $(HDRSDIR)/*.h config.mk makefile
	$(MKDIR) $(BUILDEXECDIR)
	$(CC) $(CFLAGS) $(PYTHONFLAGS) -c $< -o $@

$(BUILDEXECDIR)/%.o:$(NRDIR)/%.c $(NRHDRS) config.mk makefile
	$(MKDIR) $(BUILDEXECDIR)
	$(CC) $(PYTHONFLAGS) -c $< -o $@

$(SBMY):$(addprefix $(BUILDEXECDIR)/, $(OBJS_SBMY))
	$(CC) $(CFLAGS) $(LDFLAGS) $^ -o $@ $(LDLIBS)

$(SCOLA):$(addprefix $(BUILDEXECDIR)/, $(OBJS_SCOLA))
	$(CC) $(CFLAGS) $(LDFLAGS) $^ -o $@ $(LDLIBS)

$(SO):$(addprefix $(BUILDEXECDIR)/, $(OBJS))
	$(MKDIR) $(BUILDSODIR)
	$(CC) $(CFLAGS) $(PYTHONFLAGS) $(LDFLAGS) $^ -o $@ $(LDLIBS)

opt:config.mk makefile
	$(ECHO) "$(OPT)"

clean:
	$(RM) $(MKCLEANLIST)

cleanexec:
	$(RM) $(SBMY) $(SCOLA)

cleantoolsso:
	$(RM) $(SO)

cleanall: clean cleanexec cleantoolsso
