# Simbelmynė #

"*How fair are the bright eyes in the grass! Evermind they are called, simbelmynė in this land of Men, for they blossom in all the seasons of the year, and grow where dead men rest.*"  
Gandalf, The Two Towers III 6, The King of the Golden Hall

![Simbelmynė logo](http://www.florent-leclercq.eu/images/Simbelmyne.jpg)

Simbelmynė is a cosmological software package written in C and Python. It is a hierarchical probabilistic simulator to generate synthetic galaxy survey data.

### Documentation ###

The code's homepage is [http://simbelmyne.florent-leclercq.eu](http://simbelmyne.florent-leclercq.eu). The documentation is available on readthedocs at [https://simbelmyne.readthedocs.io/](https://simbelmyne.readthedocs.io/). Limited user-support may be asked from the main author, Florent Leclercq.

### Key features ###

Current Simbelmynė key features are:  

* simulation of the physics of structure formation via LPT, PM or COLA
* galaxy bias
* redshift-space distortions
* survey geometry
* selection effects

### Contributors ###

* Florent Leclercq, florent.leclercq@polytechnique.org  
* Baptiste Faure  
* Mariem Magdy Ali Mohamed

### License ###

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. By downloading and using Simbelmynė, you agree to the [LICENSE](LICENSE), distributed with the source code in a text file of the same name.

