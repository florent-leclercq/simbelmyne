#!/usr/bin/env python
# -*- coding: latin-1 -*-
#-------------------------------------------------------------------------------------
# Simbelmynë v0.5.0 -- pysbmy/tools/whiten
# Copyright (C) 2012-2023 Florent Leclercq.
#
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# The text of the license is located in the root directory of the source package.
#-------------------------------------------------------------------------------------

"""A tool to whiten a Gaussian random field (divide by square root
of a power spectrum in Fourier space)
"""

__author__  = "Florent Leclercq"
__version__ = "0.5.0"
__date__    = "2014-2023"
__license__ = "GPLv3"

import argparse
parser = argparse.ArgumentParser(description="\"Whiten\" a Gaussian random field, i.e. compute white noise from a given field and its power spectrum.")
parser.add_argument("fname_in_field", help="Input field file")
parser.add_argument("fname_in_Pk", help="Input power spectrum file")
parser.add_argument("fname_out", help="Output field file")
args = parser.parse_args()

from pysbmy.field import Field, read_field
from pysbmy.power import read_powerspectrum
from pysbmy.utils import PrintPythonCommand

PrintPythonCommand()
FIELD = read_field(args.fname_in_field)
Pk = read_powerspectrum(args.fname_in_Pk)
WN = FIELD.whiten(Pk)
WN.write(args.fname_out)
