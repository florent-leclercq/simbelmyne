#!/usr/bin/env python
# -*- coding: latin-1 -*-
#-------------------------------------------------------------------------------------
# Simbelmynë v0.5.0 -- pysbmy/tools/snapshot_to_psi
# Copyright (C) 2012-2023 Florent Leclercq.
#
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# The text of the license is located in the root directory of the source package.
#-------------------------------------------------------------------------------------

"""A tool to compute the displacement field from a Gadget snapshot
"""

__author__  = "Florent Leclercq"
__version__ = "0.5.0"
__date__    = "2015-2023"
__license__ = "GPLv3"

import argparse
parser = argparse.ArgumentParser(description="Compute Lagrangian displacement field from snapshot file.")
parser.add_argument("fname_in", help="Input snapshot file")
parser.add_argument("fname_out", help="Output field file")
parser.add_argument("--corner0", help="Position of corner 0 in code units")
parser.add_argument("--corner1", help="Position of corner 1 in code units")
parser.add_argument("--corner2", help="Position of corner 2 in code units")
parser.add_argument("--transpose", help="Transpose output fields?", action='store_true')
parser.set_defaults(corner0=0.)
parser.set_defaults(corner1=0.)
parser.set_defaults(corner2=0.)
args = parser.parse_args()

from pysbmy.displfield import get_displfield
from pysbmy.snapshot import read_snapshot
from pysbmy.utils import PrintPythonCommand

PrintPythonCommand()
P = read_snapshot(args.fname_in)
PSI = get_displfield(P, corner0=float(args.corner0), corner1=float(args.corner1), corner2=float(args.corner2))
PSI.write(args.fname_out,transpose=args.transpose)
