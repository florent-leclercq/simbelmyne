#!/usr/bin/env python
# -*- coding: latin-1 -*-
#-------------------------------------------------------------------------------------
# Simbelmynë v0.5.0 -- pysbmy/tools/origami
# Copyright (C) 2012-2023 Florent Leclercq.
#
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# The text of the license is located in the root directory of the source package.
#-------------------------------------------------------------------------------------

"""A tool to classify structures from a Gadget snapshot, using the ORIGAMI algorithm
"""

__author__  = "Florent Leclercq"
__version__ = "0.5.0"
__date__    = "2014-2023"
__license__ = "GPLv3"

import argparse
parser = argparse.ArgumentParser(description="Perform ORIGAMI analysis from a snapshot file.")
parser.add_argument("fname_in", help="Input Snapshot file")
parser.add_argument("mesh", help="Mesh size")
parser.add_argument("prefix_out", help="Prefix for output files")
parser.add_argument("suffix_out", help="Suffix for output files")
parser.add_argument("--corner0", help="Position of corner 0 in code units")
parser.add_argument("--corner1", help="Position of corner 1 in code units")
parser.add_argument("--corner2", help="Position of corner 2 in code units")
parser.add_argument("--transpose", help="Transpose output fields?", action='store_true')
parser.set_defaults(corner0=0.)
parser.set_defaults(corner1=0.)
parser.set_defaults(corner2=0.)
args = parser.parse_args()

from pysbmy.classifiers import origami_analysis, structures_l2e
from pysbmy.utils import PrintPythonCommand, PrintInfo
from pysbmy.snapshot import read_snapshot

PrintPythonCommand()
P = read_snapshot(args.fname_in)
OBJECTS = origami_analysis(P, corner0=float(args.corner0), corner1=float(args.corner1), corner2=float(args.corner2))
VOIDS, SHEETS, FILAMENTS, CLUSTERS = structures_l2e(OBJECTS, P, int(args.mesh))
OBJECTS.write(args.prefix_out+"objects"+args.suffix_out,transpose=args.transpose)
VOIDS.write(args.prefix_out+"voids_final"+args.suffix_out,transpose=args.transpose)
SHEETS.write(args.prefix_out+"sheets_final"+args.suffix_out,transpose=args.transpose)
FILAMENTS.write(args.prefix_out+"filaments_final"+args.suffix_out,transpose=args.transpose)
CLUSTERS.write(args.prefix_out+"clusters_final"+args.suffix_out,transpose=args.transpose)
