#!/usr/bin/env python
# -*- coding: latin-1 -*-
#-------------------------------------------------------------------------------------
# Simbelmynë v0.5.0 -- pysbmy/fft.py
# Copyright (C) 2012-2023 Florent Leclercq.
#
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# The text of the license is located in the root directory of the source package.
#-------------------------------------------------------------------------------------

"""Routines for fast Fourier transforms
"""

__author__  = "Florent Leclercq"
__version__ = "0.5.0"
__date__    = "2013-2023"
__license__ = "GPLv3"

def FFT_r2c_3d(A):
    """3D Forward Fourier transform of a real scalar quantity

    Parameters
    ----------
    A (BaseField or Field) : input real BaseField/Field structure

    Returns
    -------
    B (BaseField or Field) : output complex BaseField/Field structure

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, c_fftw_complex_t, POINTER
    from pysbmy.field import BaseField, Field
    N0=A.N0; N1=A.N1; N2=A.N2
    N=N0*N1*N2
    N2_HC=N2//2+1
    N_HC=N0*N1*N2_HC
    out = np.zeros(N_HC, dtype=c_fftw_complex_t)
    Ain = A.data.reshape(N)
    libSBMY.FFT_r2c_3d.argtypes = [c_int, c_int, c_int, POINTER(c_float_t), POINTER(c_fftw_complex_t), c_int]
    libSBMY.FFT_r2c_3d.restype = None
    libSBMY.FFT_r2c_3d(N0, N1, N2, Ain.ctypes.data_as(POINTER(c_float_t)), out.ctypes.data_as(POINTER(c_fftw_complex_t)), 2)
    out = out.reshape(N0,N1,N2_HC,2)
    if isinstance(A,Field):
        return Field(A.L0,A.L1,A.L2,A.corner0,A.corner1,A.corner2,2,N0,N1,N2_HC,A.time,out)
    elif isinstance(A,BaseField):
        return BaseField(A.L0,A.L1,A.L2,A.corner0,A.corner1,A.corner2,2,N0,N1,N2_HC,out)

def FFT_c2r_3d(A):
    """3D Inverse Fourier transform of a real scalar quantity

    Parameters
    ----------
    A (BaseField or Field) : input complex BaseField/Field structure

    Returns
    -------
    B (BaseField or Field) : output real BaseField/Field structure

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, c_fftw_complex_t, POINTER
    from pysbmy.field import BaseField, Field
    N0=A.N0; N1=A.N1; N2_HC=A.N2;
    N_HC=N0*N1*N2_HC
    N2=2*(N2_HC-1)
    N=N0*N1*N2
    out = np.zeros(N, dtype=c_float_t)
    Ain = A.data.reshape(N_HC,2)
    libSBMY.FFT_c2r_3d.argtypes = [c_int, c_int, c_int, POINTER(c_fftw_complex_t), POINTER(c_float_t), c_int]
    libSBMY.FFT_c2r_3d.restype = None
    libSBMY.FFT_c2r_3d(N0, N1, N2, Ain.ctypes.data_as(POINTER(c_fftw_complex_t)), out.ctypes.data_as(POINTER(c_float_t)), 2)
    out = out.reshape(N0,N1,N2)
    if isinstance(A,Field):
        return Field(A.L0,A.L1,A.L2,A.corner0,A.corner1,A.corner2,1,N0,N1,N2,A.time,out)
    elif isinstance(A,BaseField):
        return BaseField(A.L0,A.L1,A.L2,A.corner0,A.corner1,A.corner2,1,N0,N1,N2,out)

class FourierGridMeta(object):
    """This class contains meta-parameters of for a Fourier grid or power spectrum object:
        L0 (double) : box size x
        L1 (double) : box size y
        L2 (double) : box size z
        N0 (int) : mesh size x
        N1 (int) : mesh size y
        N2 (int) : mesh size z
        N2_HC (int) : mesh size z in Fourier space. Since fields are real, only half of the Fourier
        grid is necessary and N2_HC should be (N2/2+1) (N2 should be even)
        N_HC (int) : total number of points in the Fourier grid, should be N0*N1*N2_HC
        NUM_MODES (int) : number of k modes in the Fourier grid object
        kmax (double) : maximal value of k in the Fourier grid object
    """

    # Initialization
    def __init__(self,L0,L1,L2,N0,N1,N2,N2_HC=None,N_HC=None,NUM_MODES=0,kmax=1e6):
        """Initializes a FourierGridMeta object

        Parameters
        ----------
        L0 (double) : box size x
        L1 (double) : box size y
        L2 (double) : box size z
        N0 (int) : mesh size x
        N1 (int) : mesh size y
        N2 (int) : mesh size z
        N2_HC (optional, int, default=N2//2+1 : mesh size z of the Fourier grid
        N_HC (optional, int, default=N0*N1*N2_HC) : total number of points in the Fourier grid
        NUM_MODES (optional, int, default=0) : number of k modes in the Fourier grid
        kmax (optional, double, default=1e6) : maximal value of k in the Fourier grid object

        Returns
        -------
        G (FourierGridMeta)

        """
        self.L0=L0
        self.L1=L1
        self.L2=L2
        self.N0=N0
        self.N1=N1
        self.N2=N2
        self.N2_HC=N2_HC or (N2//2+1) #N2 should be a power of 2
        self.N_HC=N_HC or N0*N1*self.N2_HC
        self.NUM_MODES=NUM_MODES
        self.kmax=kmax

    # Representation
    def __repr__(self):
        ans="ranges: L0={}, L1={}, L2={}\n".format(self.L0,self.L1,self.L2)
        ans+="grid: N0={}, N1={}, N2={}, N2_HC={}, N_HC={}, NUM_MODES={}\n".format(self.N0,self.N1,self.N2,self.N2_HC,self.N_HC,self.NUM_MODES)
        ans+="kmax={}\n".format(self.kmax)
        return ans

    # Equality
    def __eq__(self,other):
        import numpy as np
        return self.L0==other.L0 and self.L1==other.L1 and self.L2==other.L2 and self.N0==other.N0 and self.N1==other.N1 and self.N2==other.N2 and self.N2_HC==other.N2_HC and self.N_HC==other.N_HC and self.NUM_MODES==other.NUM_MODES and self.kmax==other.kmax

    # Writing
    def write(self,fname):
        """Writes a FourierGridMeta object in an external hdf5 file

        Parameters
        ----------
        fname (string) : filename

        """
        import h5py as h5
        from pysbmy import c_int, c_double
        with h5.File(fname, 'w') as hf:
            hf.attrs.create('/info/scalars/L0', self.L0, dtype=c_double)
            hf.attrs.create('/info/scalars/L1', self.L1, dtype=c_double)
            hf.attrs.create('/info/scalars/L2', self.L2, dtype=c_double)
            hf.attrs.create('/info/scalars/N0', self.N0, dtype=c_int)
            hf.attrs.create('/info/scalars/N1', self.N1, dtype=c_int)
            hf.attrs.create('/info/scalars/N2', self.N2, dtype=c_int)
            hf.attrs.create('/info/scalars/N2_HC', self.N2_HC, dtype=c_int)
            hf.attrs.create('/info/scalars/N_HC', self.N_HC, dtype=c_int)
            hf.attrs.create('/info/scalars/NUM_MODES', self.NUM_MODES, dtype=c_int)
            hf.attrs.create('/info/scalars/kmax', self.kmax, dtype=c_double)

    # Reading
    @staticmethod
    def read(fname):
        """Reads a FourierGridMeta object in an external hdf5 file

        Parameters
        ----------
        fname (string) : filename

        Returns
        -------
        G (FourierGridMeta)

        """
        import h5py as h5
        with h5.File(fname,'r') as hf:
            L0 = hf.attrs['/info/scalars/L0']
            L1 = hf.attrs['/info/scalars/L1']
            L2 = hf.attrs['/info/scalars/L2']
            N0 = hf.attrs['/info/scalars/N0']
            N1 = hf.attrs['/info/scalars/N1']
            N2 = hf.attrs['/info/scalars/N2']
            N2_HC = hf.attrs['/info/scalars/N2_HC']
            N_HC = hf.attrs['/info/scalars/N_HC']
            NUM_MODES = hf.attrs['/info/scalars/NUM_MODES']
            kmax = hf.attrs['/info/scalars/kmax']
        return L0,L1,L2,N0,N1,N2,N2_HC,N_HC,NUM_MODES,kmax
# end class(FourierGridMeta)

class FourierGrid(FourierGridMeta):
    """Superclass of FourierGridMeta containing additionally the main arrays:
        k_keys (array, int, dimensions=(N0,N1,N2_HC)) : the keys to be used as a function of the Fourier mode (k_keys[i][j][k]=b)
        k_modes (array, float_t, dimension=NUM_MODES) : value of the wavenumber for this key (k[b])
        k_nmodes (array, int, dimension=NUM_MODES) : number of modes for this key (N_k[b])
    """

    # Initialization
    def __init__(self,L0,L1,L2,N0,N1,N2,N2_HC=None,N_HC=None,NUM_MODES=0,kmax=1e6,k_keys=None,k_modes=None,k_nmodes=None,trim_bins=True,trim_threshold=0):
        """Initializes a FourierGridMeta object

        Parameters
        ----------
        L0 (double) : box size x
        L1 (double) : box size y
        L2 (double) : box size z
        N0 (int) : mesh size x
        N1 (int) : mesh size y
        N2 (int) : mesh size z
        N2_HC (optional, int, default=N2//2+1 : mesh size z of the Fourier grid
        N_HC (optional, int, default=N0*N1*N2_HC) : total number of points in the Fourier grid
        NUM_MODES (optional, int, default=0) : number of k modes in the Fourier grid
        kmax (optional, double, default=1e6) : maximal value of k in the Fourier grid object
        k_keys (optional, array, int, dimensions=(N0,N1,N2_HC), computed if not specified) : the keys to be used as a function of the Fourier mode (k_keys[i][j][k]=b)
        k_modes (optional, array, float_t, dimension=NUM_MODES, computed if not specified) : value of the wavenumber for this key (k[b])
        k_nmodes (optional, array, int, dimension=NUM_MODES, computed if not specified) : number of modes for this key (N_k[b])
        trim_bins (optional, boolean, default=True) : trim bins in the input k_modes for the final FourierGrid object if they have less than trim_threshold modes?
        trim_threshold (optional, int, default=0) : minimun number of k modes in a bin

        Returns
        -------
        G (FourierGrid)

        """
        super(FourierGrid, self).__init__(L0,L1,L2,N0,N1,N2,N2_HC,N_HC,NUM_MODES,kmax)
        if k_keys is not None and k_modes is not None and k_nmodes is not None:
            from pysbmy import c_float_t, c_int
            from pysbmy.utils import FatalError
            if len(k_modes)!=len(k_nmodes):FatalError("Dimensions of k_modes and k_nmodes should match.")
            if k_keys.shape!=((self.N0,self.N1,self.N2_HC)):FatalError("Dimensions of k_keys are incorrect.")
            self.__init__FourierGridArrays(len(k_modes),k_keys,k_modes,k_nmodes)
        elif k_modes is not None:
            self.setup_FourierGrid_from_k_modes(k_modes, kmax, trim_bins, trim_threshold)
        else:
            self.setup_FourierGrid_all()

    def __init__FourierGridArrays(self,NUM_MODES,k_keys,k_modes,k_nmodes):
        from pysbmy import c_float_t, c_int
        self.NUM_MODES=NUM_MODES
        self.k_keys=k_keys.astype(c_int)
        self.k_modes=k_modes.astype(c_float_t)
        self.k_nmodes=k_nmodes.astype(c_int)

    # Setup Fourier space
    def setup_FourierGrid_from_k_modes(self, k_modes, kmax, trim_bins=True, trim_threshold=0):
        import numpy as np
        from pysbmy.utils import PrintMessage

        def get_FourierGrid_from_k_modes(NUM_MODES, k_modes, kmax, N0, N1, N2, N2_HC, L0, L1, L2):
            import numpy as np
            from pysbmy import libSBMY, c_float_t, c_int, c_double, POINTER
            k_modes = k_modes.astype(c_float_t)
            k_keys = np.zeros(N0*N1*N2_HC, dtype=c_int)
            k_nmodes = np.zeros(NUM_MODES, dtype=c_int)
            libSBMY.get_FourierGrid_from_k_modes.argtypes = [c_int, POINTER(c_float_t), c_float_t, POINTER(c_int), POINTER(c_int), c_int, c_int, c_int, c_double, c_double, c_double]
            libSBMY.get_FourierGrid_from_k_modes.restype = None
            libSBMY.get_FourierGrid_from_k_modes(NUM_MODES, k_modes.ctypes.data_as(POINTER(c_float_t)), kmax, k_keys.ctypes.data_as(POINTER(c_int)), k_nmodes.ctypes.data_as(POINTER(c_int)), N0, N1, N2, L0, L1, L2)
            k_keys = k_keys.reshape((N0,N1,N2_HC))
            return k_keys, k_nmodes

        N0=self.N0
        N1=self.N1
        N2=self.N2
        N2_HC=self.N2_HC
        L0=self.L0
        L1=self.L1
        L2=self.L2
        NUM_MODES=len(k_modes)

        PrintMessage(3, "Setting up Fourier grid...")

        k_keys, k_nmodes = get_FourierGrid_from_k_modes(NUM_MODES,k_modes,kmax,N0,N1,N2,N2_HC,L0,L1,L2)
        if np.sum(np.where(k_nmodes<trim_threshold))>0 and trim_bins:
            k_modes_new=np.array([k_modes[0]])
            k_nmodes_new=np.array([k_nmodes[0]])
            ik_new=0
            ik_old=1
            while ik_old<NUM_MODES:
                if k_nmodes_new[ik_new]<trim_threshold:
                    k_nmodes_new[ik_new]+=k_nmodes[ik_old]
                else:
                    k_modes_new=np.append(k_modes_new,[k_modes[ik_old]])
                    k_nmodes_new=np.append(k_nmodes_new,[k_nmodes[ik_old]])
                    ik_new+=1
                ik_old+=1

            k_modes=k_modes_new
            NUM_MODES=len(k_modes)
            k_keys, k_nmodes = get_FourierGrid_from_k_modes(NUM_MODES,k_modes,kmax,N0,N1,N2,N2_HC,L0,L1,L2)

        self.__init__FourierGridArrays(NUM_MODES,k_keys,k_modes,k_nmodes)

        PrintMessage(3, "Setting up Fourier grid done.")

    def setup_FourierGrid_all(self):
        import numpy as np
        from pysbmy import c_float_t, c_int
        from pysbmy.utils import PrintMessage

        def get_FourierGrid(N0, N1, N2, L0, L1, L2):
            import numpy as np
            from pysbmy import libSBMY, c_float_t, c_int, c_double, POINTER
            N2_HC=(N2//2+1)
            FOURIERGRID = np.zeros(N0*N1*N2_HC, dtype=c_float_t)
            libSBMY.get_FourierGrid.argtypes = [POINTER(c_float_t), c_int, c_int, c_int, c_double, c_double, c_double]
            libSBMY.get_FourierGrid.restype = None
            libSBMY.get_FourierGrid(FOURIERGRID.ctypes.data_as(POINTER(c_float_t)), N0, N1, N2, L0, L1, L2)
            return FOURIERGRID

        N0=self.N0
        N1=self.N1
        N2=self.N2
        N2_HC=self.N2_HC
        L0=self.L0
        L1=self.L1
        L2=self.L2

        PrintMessage(3, "Setting up Fourier grid...")

        F = get_FourierGrid(N0,N1,N2,L0,L1,L2)
        k_modes, k_keys, k_nmodes = np.unique(F,return_inverse=True,return_counts=True)
        k_keys = k_keys.reshape((N0,N1,N2_HC)).astype(c_int)
        k_modes = k_modes.astype(c_float_t)
        k_nmodes = k_nmodes.astype(c_int)
        self.__init__FourierGridArrays(len(k_modes),k_keys,k_modes,k_nmodes)
        self.kmax=k_modes.max()                                 # Replace the value of kmax consistently with the k_modes here

        PrintMessage(3, "Setting up Fourier grid done.")

    @property
    def FourierGridMeta(self):
        return FourierGridMeta(self.L0,self.L1,self.L2,self.N0,self.N1,self.N2,self.N2_HC,self.N_HC,self.NUM_MODES,self.kmax)

    # Representation
    def __repr__(self):
        ans=super(FourierGrid, self).__repr__()
        ans+="k_keys=np.array({}, dtype={})\n".format(self.k_keys.shape,self.k_keys.dtype)
        ans+="k_modes=np.array({}, dtype={})\n".format(self.k_modes.shape,self.k_modes.dtype)
        ans+="k_nmodes=np.array({}, dtype={})\n".format(self.k_nmodes.shape,self.k_nmodes.dtype)
        return ans

    # Equality
    def __eq__(self,other):
        import numpy as np
        return super(FourierGrid, self).__eq__(other) and np.array_equal(self.k_keys,other.k_keys) and np.array_equal(self.k_modes, other.k_modes) and np.array_equal(self.k_nmodes,other.k_nmodes)

    # Writing
    def write_FourierGrid_hdf5(self,fname):
        import h5py as h5
        from pysbmy import c_int, c_float
        super(FourierGrid, self).write(fname)
        with h5.File(fname, 'a') as hf:
            hf.create_dataset('/info/scalars/k_keys', data=self.k_keys, dtype=c_int)
            hf.create_dataset('/info/scalars/k_modes', data=self.k_modes.astype(c_float), dtype=c_float)
            hf.create_dataset('/info/scalars/k_nmodes', data=self.k_nmodes, dtype=c_int)

    def write(self,fname):
        """Writes a FourierGrid object in an external hdf5 file

        Parameters
        ----------
        fname (string) : filename

        """
        from pysbmy.utils import INDENT, UNINDENT, PrintMessage, PrintDiagnostic
        PrintMessage(3, "Write Fourier grid in data file '{}'...".format(fname))
        INDENT()
        PrintDiagnostic(4, "L0={}, L1={}, L2={}".format(self.L0,self.L1,self.L2))
        PrintDiagnostic(4, "N0={}, N1={}, N2={}, N2_HC={}, N_HC={}, NUM_MODES={}".format(self.N0,self.N1,self.N2,self.N2_HC,self.N_HC,self.NUM_MODES))

        self.write_FourierGrid_hdf5(fname)

        UNINDENT()
        PrintMessage(3, "Write Fourier grid in data file '{}' done.".format(fname))

    # Reading
    @staticmethod
    def read_FourierGrid_hdf5(fname):
        import h5py as h5
        import numpy as np
        from pysbmy import c_float_t, c_int, c_float
        L0,L1,L2,N0,N1,N2,N2_HC,N_HC,NUM_MODES,kmax = super(FourierGrid, FourierGrid).read(fname)
        with h5.File(fname,'r') as hf:
            k_keys = np.array(hf.get('info/scalars/k_keys'), dtype=c_int)
            k_modes = np.array(hf.get('info/scalars/k_modes'), dtype=c_float).astype(c_float_t)
            k_nmodes = np.array(hf.get('info/scalars/k_nmodes'), dtype=c_int)
        kg = FourierGrid(L0,L1,L2,N0,N1,N2,N2_HC,N_HC,NUM_MODES,kmax,k_keys,k_modes,k_nmodes)
        return kg

    @staticmethod
    def read(fname):
        """Reads a FourierGrid object in an external hdf5 file

        Parameters
        ----------
        fname (string) : filename

        Returns
        -------
        G (FourierGrid)

        """
        from pysbmy.utils import INDENT, UNINDENT, PrintMessage, PrintDiagnostic
        PrintMessage(3, "Read Fourier grid in data file '{}'...".format(fname))
        INDENT()

        kg = FourierGrid.read_FourierGrid_hdf5(fname)

        PrintDiagnostic(4, "L0={}, L1={}, L2={}".format(kg.L0,kg.L1,kg.L2))
        PrintDiagnostic(4, "N0={}, N1={}, N2={}, N2_HC={}, N_HC={}, NUM_MODES={}".format(kg.N0,kg.N1,kg.N2,kg.N2_HC,kg.N_HC,kg.NUM_MODES))
        UNINDENT()
        PrintMessage(3, "Read Fourier grid in data file '{}' done.".format(fname))
        return kg
# end class(FourierGrid)

read_FourierGrid=FourierGrid.read

def get_kmode(l, N, L):
    """Computes the value of a k mode in the Fourier grid

    Parameters
    ----------
    l (int) : index in one direction
    N (int) : mesh size in this direction
    L (double) : size of the box in this direction

    Returns
    -------
    k (double) : value of k mode

    """
    from pysbmy import libSBMY, c_int, c_double
    libSBMY.get_kmode.argtypes = [c_int, c_int, c_double]
    libSBMY.get_kmode.restype = c_double
    return libSBMY.get_kmode(l, N, L)

def get_ksquared(l, m, n, N0, N1, N2, L0, L1, L2):
    """Computes the squared value of a k mode in the Fourier grid

    Parameters
    ----------
    l (int) : index in direction x
    m (int) : index in direction y
    n (int) : index in direction z
    N0 (int) : mesh size x
    N1 (int) : mesh size y
    N2 (int) : mesh size z
    L0 (double) : box size x
    L1 (double) : box size y
    L2 (double) : box size z

    Returns
    -------
    ksquared (double) : value of k squared

    """
    from pysbmy import libSBMY, c_int, c_double
    libSBMY.get_ksquared.argtypes = [c_int, c_int, c_int, c_int, c_int, c_int, c_double, c_double, c_double]
    libSBMY.get_ksquared.restype = c_double
    return libSBMY.get_ksquared(l, m, n, N0, N1, N2, L0, L1, L2)

def get_kmodulus(l, m, n, N0, N1, N2, L0, L1, L2):
    """Computes the modulus of a k mode in the Fourier grid

    Parameters
    ----------
    l (int) : index in direction x
    m (int) : index in direction y
    n (int) : index in direction z
    N0 (int) : mesh size x
    N1 (int) : mesh size y
    N2 (int) : mesh size z
    L0 (double) : box size x
    L1 (double) : box size y
    L2 (double) : box size z

    Returns
    -------
    kmodulus (double) : value of k modulus

    """
    from pysbmy import libSBMY, c_int, c_double
    libSBMY.get_kmodulus.argtypes = [c_int, c_int, c_int, c_int, c_int, c_int, c_double, c_double, c_double]
    libSBMY.get_kmodulus.restype = c_double
    return libSBMY.get_kmodulus(l, m, n, N0, N1, N2, L0, L1, L2)

def get_kmax(N0, N1, N2, L0, L1, L2):
    """Computes the maximum k mode of a Fourier grid

    Parameters
    ----------
    N0 (int) : mesh size x
    N1 (int) : mesh size y
    N2 (int) : mesh size z
    L0 (double) : box size x
    L1 (double) : box size y
    L2 (double) : box size z

    Returns
    -------
    kmax (double) : maximum k

    """
    from pysbmy import libSBMY, c_int, c_double
    libSBMY.get_kmax.argtypes = [c_int, c_int, c_int, c_double, c_double, c_double]
    libSBMY.get_kmax.restype = c_double
    return libSBMY.get_kmax(N0, N1, N2, L0, L1, L2)

def get_knyq(N0, N1, N2, L0, L1, L2):
    """Computes the Nyquist wavenumber of a Fourier grid

    Parameters
    ----------
    N0 (int) : mesh size x
    N1 (int) : mesh size y
    N2 (int) : mesh size z
    L0 (double) : box size x
    L1 (double) : box size y
    L2 (double) : box size z

    Returns
    -------
    knyq (double) : Nyquist wavenumber of the grid

    """
    from pysbmy import libSBMY, c_int, c_double
    libSBMY.get_knyq.argtypes = [c_int, c_int, c_int, c_double, c_double, c_double]
    libSBMY.get_knyq.restype = c_double
    return libSBMY.get_knyq(N0, N1, N2, L0, L1, L2)

def sharpk_filter(SCALAR, k_cut, lowpass=True):
    """

    Parameters
    ----------
    SCALAR (Field, rank=1) : input scalar field

    Returns
    -------

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, c_double, c_bool, POINTER
    from pysbmy.field import Field
    N0=SCALAR.N0
    N1=SCALAR.N1
    N2=SCALAR.N2
    L0=SCALAR.L0
    L1=SCALAR.L1
    L2=SCALAR.L2
    corner0=SCALAR.corner0
    corner1=SCALAR.corner1
    corner2=SCALAR.corner2
    time=SCALAR.time
    NEW = np.zeros_like(SCALAR.data, dtype=c_float_t)
    libSBMY.sharpk_filter.argtypes = [POINTER(c_float_t), POINTER(c_float_t), c_int, c_int, c_int, c_double, c_double, c_double, c_double, c_bool]
    libSBMY.sharpk_filter.restype = None
    libSBMY.sharpk_filter(SCALAR.data.ctypes.data_as(POINTER(c_float_t)), NEW.ctypes.data_as(POINTER(c_float_t)), N0, N1, N2, L0, L1, L2, k_cut, lowpass)

    return Field(L0, L1, L2, corner0, corner1, corner2, 1, N0, N1, N2, time, NEW)

def gaussian_smoother(SCALAR, rsmooth):
    """Smoothes a scalar field with a filter of radius rsmooth
    (Multiplication by exp(-1/2 k^2/ksmooth^2) in Fourier space,
    ksmooth = 2pi/rsmooth)

    Parameters
    ----------
    SCALAR (Field, rank=1) : input scalar field
    rsmooth (double) : smoothing radius in Mpc/h

    Returns
    -------
    SMOOTH_SCALAR (Field, rank=1) : smoothed field (scalar field)

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, c_double, POINTER
    from pysbmy.field import Field
    N0=SCALAR.N0
    N1=SCALAR.N1
    N2=SCALAR.N2
    L0=SCALAR.L0
    L1=SCALAR.L1
    L2=SCALAR.L2
    corner0=SCALAR.corner0
    corner1=SCALAR.corner1
    corner2=SCALAR.corner2
    time=SCALAR.time
    NEW = np.zeros_like(SCALAR.data, dtype=c_float_t)
    libSBMY.gaussian_smoother.argtypes = [POINTER(c_float_t), POINTER(c_float_t), c_int, c_int, c_int, c_double, c_double, c_double, c_double]
    libSBMY.gaussian_smoother.restype = None
    libSBMY.gaussian_smoother(SCALAR.data.ctypes.data_as(POINTER(c_float_t)), NEW.ctypes.data_as(POINTER(c_float_t)), N0, N1, N2, L0, L1, L2, rsmooth)

    return Field(L0, L1, L2, corner0, corner1, corner2, 1, N0, N1, N2, time, NEW)

def gradient_of_scalar(SCALAR, inverseLaplacian=False):
    """Computes the gradient of a scalar field
    (Multiplication by I*vec{k} in Fourier space)

    Parameters
    ----------
    SCALAR (Field, rank=1) : input scalar field
    inverseLaplacian (optional, boolean, default=False) : divide by -1/k^2 if true

    Returns
    -------
    GRAD (Field, rank=3) : gradient of the scalar field (vector field)

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, c_double, c_bool, POINTER
    from pysbmy.field import Field
    N0=SCALAR.N0
    N1=SCALAR.N1
    N2=SCALAR.N2
    L0=SCALAR.L0
    L1=SCALAR.L1
    L2=SCALAR.L2
    corner0=SCALAR.corner0
    corner1=SCALAR.corner1
    corner2=SCALAR.corner2
    time=SCALAR.time
    VECTOR0 = np.zeros(N0*N1*N2, dtype=c_float_t)
    VECTOR1 = np.zeros(N0*N1*N2, dtype=c_float_t)
    VECTOR2 = np.zeros(N0*N1*N2, dtype=c_float_t)
    libSBMY.gradient_of_scalar.argtypes = [POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), c_bool, c_int, c_int, c_int, c_double, c_double, c_double]
    libSBMY.gradient_of_scalar.restype = None
    libSBMY.gradient_of_scalar(SCALAR.data.ctypes.data_as(POINTER(c_float_t)), VECTOR0.ctypes.data_as(POINTER(c_float_t)), VECTOR1.ctypes.data_as(POINTER(c_float_t)), VECTOR2.ctypes.data_as(POINTER(c_float_t)), inverseLaplacian, N0, N1, N2, L0, L1, L2)
    return Field(L0,L1,L2,corner0,corner1,corner2,3,N0,N1,N2,time,np.array((VECTOR0.reshape(N0,N1,N2),VECTOR1.reshape(N0,N1,N2),VECTOR2.reshape(N0,N1,N2))))

def shear_of_scalar(SCALAR):
    """Computes the shear of a scalar field
    (Multiplication by k_a*k_b/k**2 in Fourier space, a,b=0,1,2)

    Parameters
    ----------
    SCALAR (Field, rank=1) : input scalar field

    Returns
    -------
    SHEAR (Field, rank=6) : shear of the scalar field (tensor field)

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, c_double, POINTER
    from pysbmy.field import Field
    N0=SCALAR.N0
    N1=SCALAR.N1
    N2=SCALAR.N2
    L0=SCALAR.L0
    L1=SCALAR.L1
    L2=SCALAR.L2
    corner0=SCALAR.corner0
    corner1=SCALAR.corner1
    corner2=SCALAR.corner2
    time=SCALAR.time
    T00 = np.zeros(N0*N1*N2, dtype=c_float_t)
    T11 = np.zeros(N0*N1*N2, dtype=c_float_t)
    T22 = np.zeros(N0*N1*N2, dtype=c_float_t)
    T01 = np.zeros(N0*N1*N2, dtype=c_float_t)
    T02 = np.zeros(N0*N1*N2, dtype=c_float_t)
    T12 = np.zeros(N0*N1*N2, dtype=c_float_t)
    libSBMY.shear_of_scalar.argtypes = [POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), c_int, c_int, c_int, c_double, c_double, c_double]
    libSBMY.shear_of_scalar.restype = None
    libSBMY.shear_of_scalar(SCALAR.data.ctypes.data_as(POINTER(c_float_t)), T00.ctypes.data_as(POINTER(c_float_t)), T11.ctypes.data_as(POINTER(c_float_t)), T22.ctypes.data_as(POINTER(c_float_t)), T01.ctypes.data_as(POINTER(c_float_t)), T02.ctypes.data_as(POINTER(c_float_t)), T12.ctypes.data_as(POINTER(c_float_t)), N0, N1, N2, L0, L1, L2)
    T=np.array((T00.reshape(N0,N1,N2),T11.reshape(N0,N1,N2),T22.reshape(N0,N1,N2),T01.reshape(N0,N1,N2),T02.reshape(N0,N1,N2),T12.reshape(N0,N1,N2)))
    return Field(L0,L1,L2,corner0,corner1,corner2,6,N0,N1,N2,time,T)

def divergence_of_vector(VECTOR, inverseLaplacian=False):
    """Computes the divergence of a vector field
    (Dot product I.vec{k}.vec{VECTOR} in Fourier space)

    Parameters
    ----------
    VECTOR (Field, rank=3) : input vector field
    inverseLaplacian (optional, boolean, default=False) : divide by -1/k^2 if true

    Returns
    -------
    DIV (Field, rank=1) : divergence of the vector field (scalar field)

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, c_double, c_bool, POINTER
    from pysbmy.field import Field
    from pysbmy.utils import FatalError
    if not VECTOR.rank==3:FatalError("Input field should be a vector.")
    N0=VECTOR.N0
    N1=VECTOR.N1
    N2=VECTOR.N2
    L0=VECTOR.L0
    L1=VECTOR.L1
    L2=VECTOR.L2
    corner0=VECTOR.corner0
    corner1=VECTOR.corner1
    corner2=VECTOR.corner2
    time=VECTOR.time
    SCALAR = np.zeros(N0*N1*N2, dtype=c_float_t)
    libSBMY.divergence_of_vector.argtypes = [POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), c_bool, c_int, c_int, c_int, c_double, c_double, c_double]
    libSBMY.divergence_of_vector.restype = None
    libSBMY.divergence_of_vector(VECTOR.data[0].ctypes.data_as(POINTER(c_float_t)), VECTOR.data[1].ctypes.data_as(POINTER(c_float_t)), VECTOR.data[2].ctypes.data_as(POINTER(c_float_t)), SCALAR.ctypes.data_as(POINTER(c_float_t)), inverseLaplacian, N0, N1, N2, L0, L1, L2)
    return Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,SCALAR.reshape(N0,N1,N2))

def rotational_of_vector(VECTOR, inverseLaplacian=False):
    """Computes the rotational of a vector field
    (Vector product I.vec{k} x vec{VECTOR} in Fourier space)

    Parameters
    ----------
    VECTOR (Field, rank=3) : input vector field
    inverseLaplacian (optional, boolean, default=False) : divide by -1/k^2 if true

    Returns
    -------
    ROT (Field, rank=3) : rotational of the vector field (vector field)

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, c_double, c_bool, POINTER
    from pysbmy.field import Field
    from pysbmy.utils import FatalError
    if not VECTOR.rank==3:FatalError("Input field should be a vector.")
    N0=VECTOR.N0
    N1=VECTOR.N1
    N2=VECTOR.N2
    L0=VECTOR.L0
    L1=VECTOR.L1
    L2=VECTOR.L2
    corner0=VECTOR.corner0
    corner1=VECTOR.corner1
    corner2=VECTOR.corner2
    time=VECTOR.time
    ROTATIONAL0 = np.zeros(N0*N1*N2, dtype=c_float_t)
    ROTATIONAL1 = np.zeros(N0*N1*N2, dtype=c_float_t)
    ROTATIONAL2 = np.zeros(N0*N1*N2, dtype=c_float_t)
    libSBMY.rotational_of_vector.argtypes = [POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), c_bool, c_int, c_int, c_int, c_double, c_double, c_double]
    libSBMY.rotational_of_vector.restype = None
    libSBMY.rotational_of_vector(VECTOR.data[0].ctypes.data_as(POINTER(c_float_t)), VECTOR.data[1].ctypes.data_as(POINTER(c_float_t)), VECTOR.data[2].ctypes.data_as(POINTER(c_float_t)), ROTATIONAL0.ctypes.data_as(POINTER(c_float_t)), ROTATIONAL1.ctypes.data_as(POINTER(c_float_t)), ROTATIONAL2.ctypes.data_as(POINTER(c_float_t)), inverseLaplacian, N0, N1, N2, L0, L1, L2)
    return Field(L0,L1,L2,corner0,corner1,corner2,3,N0,N1,N2,time,np.array((ROTATIONAL0.reshape(N0,N1,N2),ROTATIONAL1.reshape(N0,N1,N2),ROTATIONAL2.reshape(N0,N1,N2))))

def vectorpotential_of_vector(VECTOR):
    """Computes the vector potential of a vector field
    (Vector product I.vec{k} x vec{VECTOR}/k**2 in Fourier space)

    Parameters
    ----------
    VECTOR (Field, rank=3) : input vector field

    Returns
    -------
    POT (Field, rank=3) : vector potential of the vector field (vector field)

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, c_double, POINTER
    from pysbmy.field import Field
    from pysbmy.utils import FatalError
    if not VECTOR.rank==3:FatalError("Input field should be a vector.")
    N0=VECTOR.N0
    N1=VECTOR.N1
    N2=VECTOR.N2
    L0=VECTOR.L0
    L1=VECTOR.L1
    L2=VECTOR.L2
    corner0=VECTOR.corner0
    corner1=VECTOR.corner1
    corner2=VECTOR.corner2
    time=VECTOR.time
    VECTOR_POTENTIAL0 = np.zeros(N0*N1*N2, dtype=c_float_t)
    VECTOR_POTENTIAL1 = np.zeros(N0*N1*N2, dtype=c_float_t)
    VECTOR_POTENTIAL2 = np.zeros(N0*N1*N2, dtype=c_float_t)
    libSBMY.vectorpotential_of_vector.argtypes = [POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), c_int, c_int, c_int, c_double, c_double, c_double]
    libSBMY.vectorpotential_of_vector.restype = None
    libSBMY.vectorpotential_of_vector(VECTOR.data[0].ctypes.data_as(POINTER(c_float_t)), VECTOR.data[1].ctypes.data_as(POINTER(c_float_t)), VECTOR.data[2].ctypes.data_as(POINTER(c_float_t)), VECTOR_POTENTIAL0.ctypes.data_as(POINTER(c_float_t)), VECTOR_POTENTIAL1.ctypes.data_as(POINTER(c_float_t)), VECTOR_POTENTIAL2.ctypes.data_as(POINTER(c_float_t)), N0, N1, N2, L0, L1, L2)
    return Field(L0,L1,L2,corner0,corner1,corner2,3,N0,N1,N2,time,np.array((VECTOR_POTENTIAL0.reshape(N0,N1,N2),VECTOR_POTENTIAL1.reshape(N0,N1,N2),VECTOR_POTENTIAL2.reshape(N0,N1,N2))))

def curlpart_of_vector(VECTOR):
    """Computes the curl part of a vector field
    (Vector product I.vec{k} x potential(vec{VECTOR}) in Fourier space)

    Parameters
    ----------
    VECTOR (Field, rank=3) : input vector field

    Returns
    -------
    CURL (Field, rank=3) : curl part of the vector field (vector field)

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, c_double, POINTER
    from pysbmy.field import Field
    from pysbmy.utils import FatalError
    if not VECTOR.rank==3:FatalError("Input field should be a vector.")
    N0=VECTOR.N0
    N1=VECTOR.N1
    N2=VECTOR.N2
    L0=VECTOR.L0
    L1=VECTOR.L1
    L2=VECTOR.L2
    corner0=VECTOR.corner0
    corner1=VECTOR.corner1
    corner2=VECTOR.corner2
    time=VECTOR.time
    CURL_PART0 = np.zeros(N0*N1*N2, dtype=c_float_t)
    CURL_PART1 = np.zeros(N0*N1*N2, dtype=c_float_t)
    CURL_PART2 = np.zeros(N0*N1*N2, dtype=c_float_t)
    libSBMY.curlpart_of_vector.argtypes = [POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), c_int, c_int, c_int, c_double, c_double, c_double]
    libSBMY.curlpart_of_vector.restype = None
    libSBMY.curlpart_of_vector(VECTOR.data[0].ctypes.data_as(POINTER(c_float_t)), VECTOR.data[1].ctypes.data_as(POINTER(c_float_t)), VECTOR.data[2].ctypes.data_as(POINTER(c_float_t)), CURL_PART0.ctypes.data_as(POINTER(c_float_t)), CURL_PART1.ctypes.data_as(POINTER(c_float_t)), CURL_PART2.ctypes.data_as(POINTER(c_float_t)), N0, N1, N2, L0, L1, L2)
    return Field(L0,L1,L2,corner0,corner1,corner2,3,N0,N1,N2,time,np.array((CURL_PART0.reshape(N0,N1,N2),CURL_PART1.reshape(N0,N1,N2),CURL_PART2.reshape(N0,N1,N2))))

def normcurlpart_of_vector(VECTOR):
    """Computes the norm of the curl part of a vector field
    (Norm of I.vec{k} x potential(vec{VECTOR}) in Fourier space)

    Parameters
    ----------
    VECTOR (Field, rank=3) : input vector field

    Returns
    -------
    NORMCURL (Field, rank=1) : norm of the curl part of the vector field (scalar field)

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, c_double, POINTER
    from pysbmy.field import Field
    from pysbmy.utils import FatalError
    if not VECTOR.rank==3:FatalError("Input field should be a vector.")
    N0=VECTOR.N0
    N1=VECTOR.N1
    N2=VECTOR.N2
    L0=VECTOR.L0
    L1=VECTOR.L1
    L2=VECTOR.L2
    corner0=VECTOR.corner0
    corner1=VECTOR.corner1
    corner2=VECTOR.corner2
    time=VECTOR.time
    NORM_CURL_PART = np.zeros(N0*N1*N2, dtype=c_float_t)
    libSBMY.normcurlpart_of_vector.argtypes = [POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), c_int, c_int, c_int, c_double, c_double, c_double]
    libSBMY.normcurlpart_of_vector.restype = None
    libSBMY.normcurlpart_of_vector(VECTOR.data[0].ctypes.data_as(POINTER(c_float_t)), VECTOR.data[1].ctypes.data_as(POINTER(c_float_t)), VECTOR.data[2].ctypes.data_as(POINTER(c_float_t)), NORM_CURL_PART.ctypes.data_as(POINTER(c_float_t)), N0, N1, N2, L0, L1, L2)
    return Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,NORM_CURL_PART.reshape(N0,N1,N2))

def shear_of_vector(VECTOR, symmetric=False):
    """Computes the shear of a vector field
    (Dot product by I.vec{k} in Fourier space)

    Parameters
    ----------
    VECTOR (Field, rank=3) : input vector field
    symmetric (optional, boolean, default=False) : assume that the shear matrix is symmetric?

    Returns
    -------
    SHEAR (Field, rank=9) : shear of the vector field (tensor field)

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, c_double, c_bool, POINTER
    from pysbmy.field import Field
    from pysbmy.utils import FatalError
    if not VECTOR.rank==3:FatalError("Input field should be a vector.")
    N0=VECTOR.N0
    N1=VECTOR.N1
    N2=VECTOR.N2
    L0=VECTOR.L0
    L1=VECTOR.L1
    L2=VECTOR.L2
    corner0=VECTOR.corner0
    corner1=VECTOR.corner1
    corner2=VECTOR.corner2
    time=VECTOR.time
    R00 = np.zeros(N0*N1*N2, dtype=c_float_t)
    R11 = np.zeros(N0*N1*N2, dtype=c_float_t)
    R22 = np.zeros(N0*N1*N2, dtype=c_float_t)
    R01 = np.zeros(N0*N1*N2, dtype=c_float_t)
    R02 = np.zeros(N0*N1*N2, dtype=c_float_t)
    R12 = np.zeros(N0*N1*N2, dtype=c_float_t)
    R10 = np.zeros(N0*N1*N2, dtype=c_float_t)
    R20 = np.zeros(N0*N1*N2, dtype=c_float_t)
    R21 = np.zeros(N0*N1*N2, dtype=c_float_t)
    libSBMY.shear_of_vector.argtypes = [POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), c_bool, c_int, c_int, c_int, c_double, c_double, c_double]
    libSBMY.shear_of_vector.restype = None
    libSBMY.shear_of_vector(VECTOR.data[0].ctypes.data_as(POINTER(c_float_t)), VECTOR.data[1].ctypes.data_as(POINTER(c_float_t)), VECTOR.data[2].ctypes.data_as(POINTER(c_float_t)), R00.ctypes.data_as(POINTER(c_float_t)), R11.ctypes.data_as(POINTER(c_float_t)), R22.ctypes.data_as(POINTER(c_float_t)), R01.ctypes.data_as(POINTER(c_float_t)), R02.ctypes.data_as(POINTER(c_float_t)), R12.ctypes.data_as(POINTER(c_float_t)), R10.ctypes.data_as(POINTER(c_float_t)), R20.ctypes.data_as(POINTER(c_float_t)), R21.ctypes.data_as(POINTER(c_float_t)), symmetric, N0, N1, N2, L0, L1, L2)
    R=np.array((R00.reshape(N0,N1,N2),R11.reshape(N0,N1,N2),R22.reshape(N0,N1,N2),R01.reshape(N0,N1,N2),R02.reshape(N0,N1,N2),R12.reshape(N0,N1,N2),R10.reshape(N0,N1,N2),R20.reshape(N0,N1,N2),R21.reshape(N0,N1,N2)))
    return Field(L0,L1,L2,corner0,corner1,corner2,9,N0,N1,N2,time,R)
