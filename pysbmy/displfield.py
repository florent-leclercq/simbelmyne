#!/usr/bin/env python
# -*- coding: latin-1 -*-
#-------------------------------------------------------------------------------------
# Simbelmynë v0.5.0 -- pysbmy/displfield.py
# Copyright (C) 2012-2023 Florent Leclercq.
#
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# The text of the license is located in the root directory of the source package.
#-------------------------------------------------------------------------------------

"""Routines for working with the Lagrangian displacement field
"""

__author__  = "Florent Leclercq"
__version__ = "0.5.0"
__date__    = "2015-2023"
__license__ = "GPLv3"

def get_displfield_onecomponent(P, axis, corner0=0., corner1=0., corner2=0.):
    """Gets one component of the displacement field from information in snapshot

    Parameters
    ----------
    P (Snapshot) : input snapshot object
    axis (int) : desired axis (should be 0, 1 or 2)
    corner0 (double) : x-position of the corner of the box with respect to the observer (which is at (0,0,0))
    corner1 (double) : y-position of the corner of the box with respect to the observer (which is at (0,0,0))
    corner2 (double) : z-position of the corner of the box with respect to the observer (which is at (0,0,0))

    Returns
    -------
    PSI (Field) : one component of the displacement field on the particle grid (scalar field)

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, POINTER
    from pysbmy.field import Field
    from pysbmy.snapshot import Snapshot
    Np0=P.Np0
    Np1=P.Np1
    Np2=P.Np2
    L0=P.L0
    L1=P.L1
    L2=P.L2
    time=P.time
    PSI = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    libSBMY.get_displfield_onecomponent.argtypes = [Snapshot, POINTER(c_float_t), c_int, c_int, c_int, c_int]
    libSBMY.get_displfield_onecomponent.restype = None
    libSBMY.get_displfield_onecomponent(P, PSI.ctypes.data_as(POINTER(c_float_t)), component, Np0, Np1, Np2)
    return Field(L0,L1,L2,corner0,corner1,corner2,1,Np0,Np1,Np2,time,PSI.reshape(Np2,Np1,Np0))

def get_displfield(P, corner0=0., corner1=0., corner2=0.):
    """Gets the displacement field from information in snapshot

    Parameters
    ----------
    P (Snapshot) : input snapshot object
    corner0 (double) : x-position of the corner of the box with respect to the observer (which is at (0,0,0))
    corner1 (double) : y-position of the corner of the box with respect to the observer (which is at (0,0,0))
    corner2 (double) : z-position of the corner of the box with respect to the observer (which is at (0,0,0))

    Returns
    -------
    PSI (Field) : displacement field on the particle grid (vector field)

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, POINTER
    from pysbmy.field import Field
    from pysbmy.snapshot import Snapshot
    Np0=P.Np0
    Np1=P.Np1
    Np2=P.Np2
    L0=P.L0
    L1=P.L1
    L2=P.L2
    time=P.time
    PSI0 = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    PSI1 = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    PSI2 = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    libSBMY.get_displfield.argtypes = [Snapshot, POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), c_int, c_int, c_int]
    libSBMY.get_displfield.restype = None
    libSBMY.get_displfield(P, PSI0.ctypes.data_as(POINTER(c_float_t)), PSI1.ctypes.data_as(POINTER(c_float_t)), PSI2.ctypes.data_as(POINTER(c_float_t)), Np0, Np1, Np2)
    PSI = np.array((PSI0.reshape(Np2,Np1,Np0),PSI1.reshape(Np2,Np1,Np0),PSI2.reshape(Np2,Np1,Np0)))
    return Field(L0,L1,L2,corner0,corner1,corner2,3,Np0,Np1,Np2,time,PSI)

def get_divPsi_fs(PSI):
    """Computes the divergence of the displacement field in Fourier space

    Parameters
    ----------
    PSI (Field) : input displacement field (vector field)

    Returns
    -------
    DIVPSI (Field) : divergence of the displacement field (scalar field)

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, c_double, POINTER
    from pysbmy.field import Field
    from pysbmy.utils import FatalError
    if not PSI.rank==3:FatalError("Input field should be a vector.")
    Np0=PSI.N0
    Np1=PSI.N1
    Np2=PSI.N2
    L0=PSI.L0
    L1=PSI.L1
    L2=PSI.L2
    corner0=PSI.corner0
    corner1=PSI.corner1
    corner2=PSI.corner2
    time=PSI.time
    DIVPSI = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    libSBMY.get_divPsi_fs.argtypes = [POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), c_int, c_int, c_int, c_double, c_double, c_double]
    libSBMY.get_divPsi_fs.restype = None
    libSBMY.get_divPsi_fs(PSI.data[0].ctypes.data_as(POINTER(c_float_t)), PSI.data[1].ctypes.data_as(POINTER(c_float_t)), PSI.data[2].ctypes.data_as(POINTER(c_float_t)), DIVPSI.ctypes.data_as(POINTER(c_float_t)), Np0, Np1, Np2, L0, L1, L2)
    return Field(L0,L1,L2,corner0,corner1,corner2,1,Np0,Np1,Np2,time,DIVPSI.reshape(Np2,Np1,Np0))

def get_potentialPsi_fs(PSI):
    """Computes the potential of the displacement field in Fourier space

    Parameters
    ----------
    PSI (Field) : input displacement field (vector field)

    Returns
    -------
    POTENTIALPSI (Field) : potential of the displacement field (scalar field)

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, c_double, POINTER
    from pysbmy.field import Field
    from pysbmy.utils import FatalError
    if not PSI.rank==3:FatalError("Input field should be a vector.")
    Np0=PSI.N0
    Np1=PSI.N1
    Np2=PSI.N2
    L0=PSI.L0
    L1=PSI.L1
    L2=PSI.L2
    corner0=PSI.corner0
    corner1=PSI.corner1
    corner2=PSI.corner2
    time=PSI.time
    POTENTIALPSI = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    libSBMY.get_potentialPsi_fs.argtypes = [POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), c_int, c_int, c_int, c_double, c_double, c_double]
    libSBMY.get_potentialPsi_fs.restype = None
    libSBMY.get_potentialPsi_fs(PSI.data[0].ctypes.data_as(POINTER(c_float_t)), PSI.data[1].ctypes.data_as(POINTER(c_float_t)), PSI.data[2].ctypes.data_as(POINTER(c_float_t)), POTENTIALPSI.ctypes.data_as(POINTER(c_float_t)), Np0, Np1, Np2, L0, L1, L2)
    return Field(L0,L1,L2,corner0,corner1,corner2,1,Np0,Np1,Np2,time,POTENTIALPSI.reshape(Np2,Np1,Np0))

def inversegrad_divPsi(DIVPSI):
    """Reconstructs the displacement field from its divergence in Fourier space

    Parameters
    ----------
    DIVPSI (Field) : divergence of the displacement field (scalar field)

    Returns
    -------
    PSI (Field) : output displacement field (vector field)

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, c_double, POINTER
    from pysbmy.field import Field
    Np0=DIVPSI.N0
    Np1=DIVPSI.N1
    Np2=DIVPSI.N2
    L0=DIVPSI.L0
    L1=DIVPSI.L1
    L2=DIVPSI.L2
    corner0=DIVPSI.corner0
    corner1=DIVPSI.corner1
    corner2=DIVPSI.corner2
    time=DIVPSI.time
    PSI0 = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    PSI1 = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    PSI2 = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    libSBMY.inversegrad_divPsi.argtypes = [POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t),  c_int, c_int, c_int, c_double, c_double, c_double]
    libSBMY.inversegrad_divPsi.restype = None
    libSBMY.inversegrad_divPsi(DIVPSI.data.reshape(Np2*Np1*Np0).ctypes.data_as(POINTER(c_float_t)), PSI0.ctypes.data_as(POINTER(c_float_t)), PSI1.ctypes.data_as(POINTER(c_float_t)), PSI2.ctypes.data_as(POINTER(c_float_t)), Np0, Np1, Np2, L0, L1, L2)
    PSI = np.array((PSI0.reshape(Np2,Np1,Np0),PSI1.reshape(Np2,Np1,Np0),PSI2.reshape(Np2,Np1,Np0)))
    return Field(L0,L1,L2,corner0,corner1,corner2,3,Np0,Np1,Np2,time,PSI)

def get_normcurlpartPsi_fullminusscalar(PSI):
    """Gets the norm of the curl of the displacement field as input minus scalar part

    Parameters
    ----------
    PSI (Field) : input displacement field (vector field)

    Returns
    -------
    NORM_CURLPARTPSI (Field) : norm of the curl of the displacement field (scalar field)

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, c_double, POINTER
    from pysbmy.field import Field
    from pysbmy.utils import FatalError
    if not PSI.rank==3:FatalError("Input field should be a vector.")
    Np0=PSI.N0
    Np1=PSI.N1
    Np2=PSI.N2
    L0=PSI.L0
    L1=PSI.L1
    L2=PSI.L2
    corner0=PSI.corner0
    corner1=PSI.corner1
    corner2=PSI.corner2
    time=PSI.time
    NORM_CURLPARTPSI = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    libSBMY.get_normcurlpartPsi_fullminusscalar.argtypes = [POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), c_int, c_int, c_int, c_double, c_double, c_double]
    libSBMY.get_normcurlpartPsi_fullminusscalar.restype = None
    libSBMY.get_normcurlpartPsi_fullminusscalar(PSI.data[0].ctypes.data_as(POINTER(c_float_t)), PSI.data[1].ctypes.data_as(POINTER(c_float_t)), PSI.data[2].ctypes.data_as(POINTER(c_float_t)), NORM_CURLPARTPSI.ctypes.data_as(POINTER(c_float_t)), Np0, Np1, Np2, L0, L1, L2)
    return Field(L0,L1,L2,corner0,corner1,corner2,1,Np0,Np1,Np2,time,NORM_CURLPARTPSI.reshape(Np2,Np1,Np0))

def get_normcurlpartPsi_vector(PSI):
    """Gets the norm of the curl of the displacement field directly as vector field

    Parameters
    ----------
    PSI (Field) : input displacement field (vector field)

    Returns
    -------
    NORM_CURLPARTPSI (Field) : norm of the curl of the displacement field (scalar field)

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, c_double, POINTER
    from pysbmy.field import Field
    from pysbmy.utils import FatalError
    if not PSI.rank==3:FatalError("Input field should be a vector.")
    Np0=PSI.N0
    Np1=PSI.N1
    Np2=PSI.N2
    L0=PSI.L0
    L1=PSI.L1
    L2=PSI.L2
    corner0=PSI.corner0
    corner1=PSI.corner1
    corner2=PSI.corner2
    time=PSI.time
    NORM_CURLPARTPSI = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    libSBMY.get_normcurlpartPsi_vector.argtypes = [POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), c_int, c_int, c_int, c_double, c_double, c_double]
    libSBMY.get_normcurlpartPsi_vector.restype = None
    libSBMY.get_normcurlpartPsi_vector(PSI.data[0].ctypes.data_as(POINTER(c_float_t)), PSI.data[1].ctypes.data_as(POINTER(c_float_t)), PSI.data[2].ctypes.data_as(POINTER(c_float_t)), NORM_CURLPARTPSI.ctypes.data_as(POINTER(c_float_t)), Np0, Np1, Np2, L0, L1, L2)
    return Field(L0,L1,L2,corner0,corner1,corner2,1,Np0,Np1,Np2,time,NORM_CURLPARTPSI.reshape(Np2,Np1,Np0))

def get_normcurlpartPsi(PSI):
    """Gets the norm of the curl of the displacement field

    Parameters
    ----------
    PSI (Field) : input displacement field (vector field)

    Returns
    -------
    NORM_CURLPARTPSI (Field) : norm of the curl of the displacement field (scalar field)

    """
    #return get_normcurlpartPsi_fullminusscalar(PSI)
    return get_normcurlpartPsi_vector(PSI)

def get_shearPsi(PSI, symmetric=False):
    """Computes the shear of the displacement field

    Parameters
    ----------
    PSI (Field) : input displacement field (vector field)
    symmetric (boolean, optional, default=False) : assume that the displacement field is symmetric?

    Returns
    -------
    R (Field) : shear of the displacement field (tensor field, 9 components)

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, c_double, c_bool, POINTER
    from pysbmy.field import Field
    from pysbmy.utils import FatalError
    if not PSI.rank==3:FatalError("Input field should be a vector.")
    Np0=PSI.N0
    Np1=PSI.N1
    Np2=PSI.N2
    L0=PSI.L0
    L1=PSI.L1
    L2=PSI.L2
    corner0=PSI.corner0
    corner1=PSI.corner1
    corner2=PSI.corner2
    time=PSI.time
    R00 = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    R11 = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    R22 = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    R01 = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    R02 = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    R12 = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    R10 = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    R20 = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    R21 = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    libSBMY.get_shearPsi.argtypes = [POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), c_bool, c_int, c_int, c_int, c_double, c_double, c_double]
    libSBMY.get_shearPsi.restype = None
    libSBMY.get_shearPsi(PSI.data[0].ctypes.data_as(POINTER(c_float_t)), PSI.data[1].ctypes.data_as(POINTER(c_float_t)), PSI.data[2].ctypes.data_as(POINTER(c_float_t)), R00.ctypes.data_as(POINTER(c_float_t)), R11.ctypes.data_as(POINTER(c_float_t)), R22.ctypes.data_as(POINTER(c_float_t)), R01.ctypes.data_as(POINTER(c_float_t)), R02.ctypes.data_as(POINTER(c_float_t)), R12.ctypes.data_as(POINTER(c_float_t)), R10.ctypes.data_as(POINTER(c_float_t)), R20.ctypes.data_as(POINTER(c_float_t)), R21.ctypes.data_as(POINTER(c_float_t)), symmetric, Np0, Np1, Np2, L0, L1, L2)
    R=np.array((R00.reshape(Np2,Np1,Np0),R11.reshape(Np2,Np1,Np0),R22.reshape(Np2,Np1,Np0),R01.reshape(Np2,Np1,Np0),R02.reshape(Np2,Np1,Np0),R12.reshape(Np2,Np1,Np0),R10.reshape(Np2,Np1,Np0),R20.reshape(Np2,Np1,Np0),R21.reshape(Np2,Np1,Np0)))
    return Field(L0,L1,L2,corner0,corner1,corner2,9,Np0,Np1,Np2,time,R)

def get_shearPsi_symmetric(PSI):
    """Computes the shear of the displacement field, assuming it is symmetric

    Parameters
    ----------
    PSI (Field) : input displacement field (vector field)

    Returns
    -------
    R (Field) : shear of the displacement field (tensor field, 6 components)

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, c_double, POINTER
    from pysbmy.field import Field
    from pysbmy.utils import FatalError
    if not PSI.rank==3:FatalError("Input field should be a vector.")
    Np0=PSI.N0
    Np1=PSI.N1
    Np2=PSI.N2
    L0=PSI.L0
    L1=PSI.L1
    L2=PSI.L2
    corner0=PSI.corner0
    corner1=PSI.corner1
    corner2=PSI.corner2
    time=PSI.time
    R00 = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    R11 = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    R22 = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    R01 = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    R02 = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    R12 = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    libSBMY.get_shearPsi_symmetric.argtypes = [POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), c_int, c_int, c_int, c_double, c_double, c_double]
    libSBMY.get_shearPsi_symmetric.restype = None
    libSBMY.get_shearPsi_symmetric(PSI.data[0].ctypes.data_as(POINTER(c_float_t)), PSI.data[1].ctypes.data_as(POINTER(c_float_t)), PSI.data[2].ctypes.data_as(POINTER(c_float_t)), R00.ctypes.data_as(POINTER(c_float_t)), R11.ctypes.data_as(POINTER(c_float_t)), R22.ctypes.data_as(POINTER(c_float_t)), R01.ctypes.data_as(POINTER(c_float_t)), R02.ctypes.data_as(POINTER(c_float_t)), R12.ctypes.data_as(POINTER(c_float_t)), Np0, Np1, Np2, L0, L1, L2)
    R=np.array((R00.reshape(Np2,Np1,Np0),R11.reshape(Np2,Np1,Np0),R22.reshape(Np2,Np1,Np0),R01.reshape(Np2,Np1,Np0),R02.reshape(Np2,Np1,Np0),R12.reshape(Np2,Np1,Np0)))
    return Field(L0,L1,L2,corner0,corner1,corner2,6,Np0,Np1,Np2,time,R)

def jacobian_l2e_potential(PSI):
    """Computes the Jacobian of the transformation from Lagrangian to Eulerian coordinates

    Parameters
    ----------
    PSI (Field) : input displacement field (vector field)

    Returns
    -------
    JACOBIAN (Field) : Jacobian of the transformation (scalar field)

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, c_double, POINTER
    from pysbmy.field import Field
    from pysbmy.utils import FatalError
    if not PSI.rank==3:FatalError("Input field should be a vector.")
    Np0=PSI.N0
    Np1=PSI.N1
    Np2=PSI.N2
    L0=PSI.L0
    L1=PSI.L1
    L2=PSI.L2
    corner0=PSI.corner0
    corner1=PSI.corner1
    corner2=PSI.corner2
    time=PSI.time
    JACOBIAN = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    libSBMY.jacobian_l2e_potential.argtypes =  [POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), c_int, c_int, c_int, c_double, c_double, c_double]
    libSBMY.jacobian_l2e_potential.restype = None
    libSBMY.jacobian_l2e_potential(PSI.data[0].ctypes.data_as(POINTER(c_float_t)), PSI.data[1].ctypes.data_as(POINTER(c_float_t)), PSI.data[2].ctypes.data_as(POINTER(c_float_t)), JACOBIAN.ctypes.data_as(POINTER(c_float_t)), Np0, Np1, Np2, L0, L1, L2)
    return Field(L0,L1,L2,corner0,corner1,corner2,1,Np0,Np1,Np2,time,JACOBIAN.reshape(Np2,Np1,Np0))
