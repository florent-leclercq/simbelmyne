#!/usr/bin/env python
# -*- coding: latin-1 -*-
#-------------------------------------------------------------------------------------
# Simbelmynë v0.5.0 -- pysbmy/density.py
# Copyright (C) 2012-2023 Florent Leclercq.
#
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# The text of the license is located in the root directory of the source package.
#-------------------------------------------------------------------------------------

"""Routines for dealing with density fields
"""

__author__  = "Florent Leclercq"
__version__ = "0.5.0"
__date__    = "2013-2023"
__license__ = "GPLv3"

def get_density_pm_snapshot(P, N0, N1=None, N2=None, corner0=0., corner1=0., corner2=0.):
    """Computes the density contrast field from particles in a Snapshot structure

    Parameters
    ----------
    P (Snapshot) : input particles
    N0 (int) : mesh size of the desired density grid (x direction or all directions)
    N1 (optional, int, default=N0) : mesh size of the desired density grid (y direction)
    N2 (optional, int, default=N0) : mesh size of the desired density grid (z direction)
    corner0 (optional, double, default=0.) : x-position of the corner of the box with respect to the observer (which is at (0,0,0))
    corner1 (optional, double, default=0.) : y-position of the corner of the box with respect to the observer (which is at (0,0,0))
    corner2 (optional, double, default=0.) : z-position of the corner of the box with respect to the observer (which is at (0,0,0))

    Returns
    -------
    F (Field) : the density contrast field

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, POINTER
    from pysbmy.field import Field
    from pysbmy.snapshot import Snapshot
    N1=N1 or N0
    N2=N2 or N0
    L0=P.L0
    L1=P.L1
    L2=P.L2
    time=P.time
    DENSITYFLOWTRACERS = np.zeros(N0*N1*N2, dtype=c_float_t)
    libSBMY.get_density_pm_snapshot.argtypes = [Snapshot, c_int, POINTER(c_float_t)]
    libSBMY.get_density_pm_snapshot.restype = None
    libSBMY.get_density_pm_snapshot(P, N0, DENSITYFLOWTRACERS.ctypes.data_as(POINTER(c_float_t)))
    return Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,DENSITYFLOWTRACERS.reshape(N0,N1,N2))

get_density_flowtracers=get_density_pm_snapshot
get_density=get_density_pm_snapshot

def test_density(A):
    """Computes diagnostics of a density contrast field
    (Check unphysical values, computes the one-point distribution and sigma_8)

    Parameters
    ----------
    A (Field) : input field

    """
    from pysbmy import libSBMY, c_float_t, c_int, c_double, POINTER
    N0=A.N0
    N1=A.N1
    N2=A.N2
    L0=A.L0
    L1=A.L1
    L2=A.L2
    libSBMY.test_density.argtypes = [POINTER(c_float_t), c_int, c_int, c_int, c_double, c_double, c_double]
    libSBMY.test_density.restype = None
    libSBMY.test_density(A.data.reshape(N0*N1*N2).ctypes.data_as(POINTER(c_float_t)), N0, N1, N2, L0, L1, L2)
