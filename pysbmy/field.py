#!/usr/bin/env python
# -*- coding: latin-1 -*-
#-------------------------------------------------------------------------------------
# Simbelmynë v0.5.0 -- pysbmy/field.py
# Copyright (C) 2012-2023 Florent Leclercq.
#
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# The text of the license is located in the root directory of the source package.
#-------------------------------------------------------------------------------------

"""Routines for working with Simbelmynë fields
"""

__author__  = "Florent Leclercq"
__version__ = "0.5.0"
__date__    = "2014-2023"
__license__ = "GPLv3"

class BaseFieldMeta(object):
    """This class contains meta-parameters for a field object:
        L0 (double) : box size x
        L1 (double) : box size y
        L2 (double) : box size z
        corner0 (double) : x-position of the corner of the box with respect to the observer (which is at (0,0,0))
        corner1 (double) : y-position of the corner of the box with respect to the observer (which is at (0,0,0))
        corner2 (double) : z-position of the corner of the box with respect to the observer (which is at (0,0,0))
        rank (int) : number of components of the field
        N0 (int) : mesh size x
        N1 (int) : mesh size y
        N2 (int) : mesh size z
    """

    # Initialization
    def __init__(self,L0,L1,L2,corner0,corner1,corner2,rank,N0,N1,N2):
        """Initializes a BaseFieldMeta object

        Parameters
        ----------
        L0 (double) : box size x
        L1 (double) : box size y
        L2 (double) : box size z
        corner0 (double) : x-position of the corner of the box with respect to the observer (which is at (0,0,0))
        corner1 (double) : y-position of the corner of the box with respect to the observer (which is at (0,0,0))
        corner2 (double) : z-position of the corner of the box with respect to the observer (which is at (0,0,0))
        rank (int) : number of components of the field
        N0 (int) : mesh size x
        N1 (int) : mesh size y
        N2 (int) : mesh size z

        Returns
        -------
        F (BaseFieldMeta)

        """
        self.L0=L0
        self.L1=L1
        self.L2=L2
        self.corner0=corner0
        self.corner1=corner1
        self.corner2=corner2
        self.rank=rank
        self.N0=N0
        self.N1=N1
        self.N2=N2

    # Properties
    @property
    def ranges(self):
        return [self.corner0,self.corner0+self.L0,self.corner1,self.corner1+self.L1,self.corner2,self.corner2+self.L2,self.N0,self.N1,self.N2]

    # Representation
    def __repr__(self):
        ans="dimensions: L0={}, L1={}, L2={}\n".format(self.L0,self.L1,self.L2)
        ans+="corner: corner0={}, corner1={}, corner2={}\n".format(self.corner0,self.corner1,self.corner2)
        ans+="grid: rank={}, N0={}, N1={}, N2={}\n".format(self.rank,self.N0,self.N1,self.N2)
        ans+="ranges={}\n".format(self.ranges)
        return ans

    # Equality
    def __eq__(self,other):
        return self.L0==other.L0 and self.L1==other.L1 and self.L2==other.L2 and self.corner0==other.corner0 and self.corner1==other.corner1 and self.corner2==other.corner2 and self.rank==other.rank and self.N0==other.N0 and self.N1==other.N1 and self.N2==other.N2

    # Writing
    def write(self,fname):
        """Writes a BaseFieldMeta object in an external hdf5 file

        Parameters
        ----------
        fname (string) : filename

        """
        import h5py as h5
        from pysbmy import c_int, c_double
        with h5.File(fname, 'w') as hf:
            hf.attrs.create('/info/scalars/L0', self.L0, dtype=c_double)
            hf.attrs.create('/info/scalars/L1', self.L1, dtype=c_double)
            hf.attrs.create('/info/scalars/L2', self.L2, dtype=c_double)
            hf.attrs.create('/info/scalars/corner0', self.corner0, dtype=c_double)
            hf.attrs.create('/info/scalars/corner1', self.corner1, dtype=c_double)
            hf.attrs.create('/info/scalars/corner2', self.corner2, dtype=c_double)
            hf.attrs.create('/info/scalars/rank', self.rank, dtype=c_int)
            hf.attrs.create('/info/scalars/N0', self.N0, dtype=c_int)
            hf.attrs.create('/info/scalars/N1', self.N1, dtype=c_int)
            hf.attrs.create('/info/scalars/N2', self.N2, dtype=c_int)

    # Reading
    @staticmethod
    def read(fname):
        """Reads a BaseFieldMeta object in an external hdf5 file

        Parameters
        ----------
        fname (string) : filename

        Returns
        -------
        F (BaseFieldMeta)

        """
        import h5py as h5
        with h5.File(fname,'r') as hf:
            L0 = hf.attrs['/info/scalars/L0']
            L1 = hf.attrs['/info/scalars/L1']
            L2 = hf.attrs['/info/scalars/L2']
            corner0 = hf.attrs['/info/scalars/corner0']
            corner1 = hf.attrs['/info/scalars/corner1']
            corner2 = hf.attrs['/info/scalars/corner2']
            rank = hf.attrs['/info/scalars/rank']
            N0 = hf.attrs['/info/scalars/N0']
            N1 = hf.attrs['/info/scalars/N1']
            N2 = hf.attrs['/info/scalars/N2']
        return L0,L1,L2,corner0,corner1,corner2,rank,N0,N1,N2
#end class(BaseFieldMeta)

class BaseField(BaseFieldMeta):
    """Superclass of BaseFieldMeta containing additionally the main array:
    data (array, float_t, dimensions=(N0,N1,N2)) : the field
    """

    # Initialization
    def __init__(self,L0,L1,L2,corner0,corner1,corner2,rank,N0,N1,N2,data):
        """Initializes a BaseField object

        Parameters
        ----------
        L0 (double) : box size x
        L1 (double) : box size y
        L2 (double) : box size z
        corner0 (double) : x-position of the corner of the box with respect to the observer (which is at (0,0,0))
        corner1 (double) : y-position of the corner of the box with respect to the observer (which is at (0,0,0))
        corner2 (double) : z-position of the corner of the box with respect to the observer (which is at (0,0,0))
        rank (int) : number of components of the field
        N0 (int) : mesh size x
        N1 (int) : mesh size y
        N2 (int) : mesh size z
        data (array, float_t, dimensions=(N0,N1,N2)) : the field

        Returns
        -------
        F (BaseField)

        """
        from pysbmy import c_float_t
        super(BaseField, self).__init__(L0,L1,L2,corner0,corner1,corner2,rank,N0,N1,N2)
        self.data=data.astype(c_float_t)

    @classmethod
    def from_ranges(cls,data,ranges):
        """Initializes a BaseField object from ranges

        Parameters
        ----------
        data (array, float_t, dimensions=(N0,N1,N2)) : the field
        ranges (array, double, dimension=9) : the ranges of the field in the form [xmin, xmax, ymin, ymax, zmin, zmax, N0, N1, N2]

        Returns
        -------
        F (BaseField)

        """
        L0=ranges[1]-ranges[0]
        L1=ranges[3]-ranges[2]
        L2=ranges[5]-ranges[4]
        corner0=ranges[0]
        corner1=ranges[2]
        corner2=ranges[4]
        rank=1
        N0=ranges[6]
        N1=ranges[7]
        N2=ranges[8]
        return cls(L0,L1,L2,corner0,corner1,corner2,rank,N0,N1,N2,data)

    # Properties
    @property
    def BaseFieldMeta(self):
        return BaseFieldMeta(self.L0,self.L1,self.L2,self.corner0,self.corner1,self.corner2,self.rank,self.N0,self.N1,self.N2)

    # Representation
    def __repr__(self):
        ans=super(BaseField, self).__repr__()
        ans+="data=np.array({}, dtype={})\n".format(self.data.shape,self.data.dtype)
        return ans

    # Equality
    def __eq__(self,other):
        import numpy as np
        return super(BaseField, self).__eq__(other) and np.array_equal(self.data,other.data)

    # Writing
    def write(self,fname,field_address='/scalars/field',transpose=False):
        """Writes a BaseField object in an external hdf5 file

        Parameters
        ----------
        fname (string) : filename
        field_address (optional, string, default='/scalars/field') : address of the field in the hdf5 file
        transpose (optional, boolean, default=False) : transpose field before writing?

        """
        import h5py as h5
        from pysbmy import c_float
        super(BaseField, self).write(fname)
        data=self.data.astype(c_float)
        if transpose:
            if self.rank>1:
                data=data.transpose((0,3,2,1))
            else:
                data=data.T
        with h5.File(fname, 'a') as hf:
            hf.create_dataset(field_address, data=data, dtype=c_float)

    # Reading
    @staticmethod
    def read(fname,field_address='/scalars/field'):
        """Reads a BaseField object in an external hdf5 file

        Parameters
        ----------
        fname (string) : filename
        field_address (optional, string, default='/scalars/field') : address of the field in the hdf5 file

        Returns
        -------
        F (BaseField)

        """
        import h5py as h5
        import numpy as np
        from pysbmy import c_float_t, c_float
        from pysbmy.utils import INDENT, UNINDENT, PrintMessage, PrintDiagnostic

        PrintMessage(3, "Read field in data file '{}'...".format(fname))
        INDENT()

        split=fname.split(".")
        ext=split[len(split)-1]
        L0,L1,L2,corner0,corner1,corner2,rank,N0,N1,N2=super(BaseField, BaseField).read(fname)
        with h5.File(fname,'r') as hf:
            data = np.array(hf.get(field_address), dtype=c_float).astype(c_float_t)
        BF=BaseField(L0,L1,L2,corner0,corner1,corner2,rank,N0,N1,N2,data)

        PrintDiagnostic(4, "ranges={}".format(BF.ranges))
        UNINDENT()
        PrintMessage(3, "Read field in data file '{}' done.".format(fname))

        return BF
#end class(BaseField)

class Field(BaseField):
    """Superclass of BaseField containing additionally:
    time (double) : cosmic time at which the field is defined
    """

    # Initialization
    def __init__(self,L0,L1,L2,corner0,corner1,corner2,rank,N0,N1,N2,time,data):
        """Initializes a Field object

        Parameters
        ----------
        L0 (double) : box size x
        L1 (double) : box size y
        L2 (double) : box size z
        corner0 (double) : x-position of the corner of the box with respect to the observer (which is at (0,0,0))
        corner1 (double) : y-position of the corner of the box with respect to the observer (which is at (0,0,0))
        corner2 (double) : z-position of the corner of the box with respect to the observer (which is at (0,0,0))
        rank (int) : number of components of the field
        N0 (int) : mesh size x
        N1 (int) : mesh size y
        N2 (int) : mesh size z
        time (double) : cosmic time at which the field is defined
        data (array, float_t, dimensions=(N0,N1,N2)) : the field

        Returns
        -------
        F (Field)

        """
        super(Field, self).__init__(L0,L1,L2,corner0,corner1,corner2,rank,N0,N1,N2,data)
        self.time=time

    @classmethod
    def from_ranges(cls,data,ranges,time):
        """Initializes a Field object from ranges

        Parameters
        ----------
        data (array, float_t, dimensions=(N0,N1,N2)) : the field
        ranges (array, double, dimension=9) : the ranges of the field in the form [xmin, xmax, ymin, ymax, zmin, zmax, N0, N1, N2]
        time (double) : cosmic time at which the field is defined

        Returns
        -------
        F (Field)

        """
        BF=BaseField.from_ranges(data,ranges)
        return cls(BF.L0,BF.L1,BF.L2,BF.corner0,BF.corner1,BF.corner2,BF.rank,BF.N0,BF.N1,BF.N2,time,BF.data)

    @classmethod
    def WN(cls,L0,L1,L2,corner0,corner1,corner2,N0,N1,N2,time=1e-3,seed=None):
        """Produces a scalar white noise field using numpy.random

        Parameters
        ----------
        L0 (double) : box size x
        L1 (double) : box size y
        L2 (double) : box size z
        corner0 (double) : x-position of the corner of the box with respect to the observer (which is at (0,0,0))
        corner1 (double) : y-position of the corner of the box with respect to the observer (which is at (0,0,0))
        corner2 (double) : z-position of the corner of the box with respect to the observer (which is at (0,0,0))
        N0 (int) : mesh size x
        N1 (int) : mesh size y
        N2 (int) : mesh size z
        time (optional, double, default=1e-3) : cosmic time at which the field is defined
        seed (optional, int, default=None) : seed for the random number generator. Use the current state is set to None.

        Returns
        -------
        WN (Field) : a white noise field

        """
        import numpy as np
        import scipy.stats as ss
        from pysbmy import c_float_t
        if seed is not None: np.random.seed(seed)
        data=ss.norm.rvs(size=(N0,N1,N2)).astype(c_float_t)
        rank=1
        return cls(L0,L1,L2,corner0,corner1,corner2,rank,N0,N1,N2,time,data)

    @classmethod
    def GRF(cls,L0,L1,L2,corner0,corner1,corner2,N0,N1,N2,Pk,time=1e-3,seed=None):
        """Produces a scalar Gaussian random field with a specified power spectrum

        Parameters
        ----------
        L0 (double) : box size x
        L1 (double) : box size y
        L2 (double) : box size z
        corner0 (double) : x-position of the corner of the box with respect to the observer (which is at (0,0,0))
        corner1 (double) : y-position of the corner of the box with respect to the observer (which is at (0,0,0))
        corner2 (double) : z-position of the corner of the box with respect to the observer (which is at (0,0,0))
        N0 (int) : mesh size x
        N1 (int) : mesh size y
        N2 (int) : mesh size z
        Pk (PowerSpectrum) : input power spectrum
        time (optional, double, default=1e-3) : cosmic time at which the field is defined
        seed (optional, int, default=None) : seed for the random number generator. Use the current state is set to None.

        Returns
        -------
        WN (Field) : a white noise field

        """
        from pysbmy.power import tint
        GRF=cls.WN(L0,L1,L2,corner0,corner1,corner2,N0,N1,N2,time,seed)
        tint(GRF,Pk)
        return GRF

    # Representation
    def __repr__(self):
        ans=BaseFieldMeta.__repr__(self)
        ans+="time={}\n".format(self.time)
        ans+="data=np.array({}, dtype={})\n".format(self.data.shape,self.data.dtype)
        return ans

    # Equality
    def __eq__(self,other):
        return super(Field, self).__eq__(other) and self.time==other.time

    # Properties
    @property
    def min(self):
        return self.data.min()

    @property
    def max(self):
        return self.data.max()

    @property
    def mean(self):
        return self.data.mean()

    @property
    def std(self):
        return self.data.std()

    @property
    def var(self):
        return self.data.var()

    # Writing
    def add_time(self,fname):
        import h5py as h5
        import numpy as np
        from pysbmy import c_double
        with h5.File(fname, 'a') as hf:
            hf.attrs.create('/info/scalars/time', self.time, dtype=c_double)

    def write_field_hdf5(self,fname,field_address='/scalars/field',transpose=False):
        from pysbmy.utils import INDENT, UNINDENT, PrintMessage, PrintDiagnostic
        PrintMessage(3, "Write field in data file '{}'...".format(fname))
        INDENT()
        PrintDiagnostic(4, "ranges={}".format(self.ranges))
        PrintDiagnostic(4, "time={}".format(self.time))

        super(Field, self).write(fname,field_address,transpose)
        self.add_time(fname)

        UNINDENT()
        PrintMessage(3, "Write field in data file '{}' done.".format(fname))

    def write(self,fname,field_address='/scalars/field',transpose=False):
        """Writes a Field object in an external hdf5 file

        Parameters
        ----------
        fname (string) : filename
        field_address (optional, string, default='/scalars/field') : address of the field in the hdf5 file
        transpose (optional, boolean, default=False) : transpose field before writing?

        """
        split=fname.split(".")
        ext=split[len(split)-1]
        self.write_field_hdf5(fname,field_address,transpose)

    # Reading
    @staticmethod
    def read_time(fname):
        import h5py as h5
        import numpy as np
        with h5.File(fname,'r') as hf:
            time = hf.attrs['/info/scalars/time']
        return time

    @staticmethod
    def read_field_hdf5(fname,field_address='/scalars/field'):
        BF=super(Field, Field).read(fname,field_address='/scalars/field')
        time=Field.read_time(fname)
        F=Field(BF.L0,BF.L1,BF.L2,BF.corner0,BF.corner1,BF.corner2,BF.rank,BF.N0,BF.N1,BF.N2,time,BF.data)
        return F

    @staticmethod
    def read(fname,field_address='/scalars/field'):
        """Reads a Field object in an external hdf5 file

        Parameters
        ----------
        fname (string) : filename
        field_address (optional, string, default='/scalars/field') : address of the field in the hdf5 file

        Returns
        -------
        F (Field)

        """
        split=fname.split(".")
        ext=split[len(split)-1]
        F=Field.read_field_hdf5(fname)
        return F

    # Data processing
    def ud_grade(self,N0_out,N1_out=None,N2_out=None):
        """Upgrades or downgrades a field to the given resolution

        Parameters
        ----------
        N0_out (int) : new mesh size x
        N1_out (optional, int, default=N0) : new mesh size y
        N2_out (optional, int, default=N0) : new mesh size z

        """
        import numpy as np
        from pysbmy.utils import PrintMessage
        N1_out = N1_out or N0_out
        N2_out = N2_out or N0_out
        N0_in = self.N0
        N1_in = self.N1
        N2_in = self.N2
        map_in = self.data

        if(N0_out != N0_in or N1_out != N1_in or N2_out != N2_in):
            PrintMessage(3, "Upgrading/Downgrading field...")
            map_out = np.zeros((N0_out, N1_out, N2_out))

            rat0 = max(N0_out//N0_in,N0_in//N0_out)
            rat1 = max(N1_out//N1_in,N1_in//N1_out)
            rat2 = max(N2_out//N2_in,N2_in//N2_out)

            for i in range(max(N0_out,N0_in)):
                for j in range(max(N1_out,N1_in)):
                    for k in range(max(N2_out,N2_in)):
                        if(N0_out > N0_in):
                            i_out = i
                            i_in = i//rat0
                        elif(N0_out < N0_in):
                            i_out = i//rat0
                            i_in = i

                        if(N1_out > N1_in):
                            j_out = j
                            j_in = j//rat1
                        elif(N1_out < N1_in):
                            j_out = j//rat1
                            j_in = j

                        if(N2_out > N2_in):
                            k_out = k
                            k_in = k//rat2
                        elif(N2_out < N2_in):
                            k_out = k//rat2
                            k_in = k

                        map_out[i_out,j_out,k_out] += map_in[i_in,j_in,k_in]

            if(N0_out < N0_in):
                map_out /= rat0
            if(N1_out < N1_in):
                map_out /= rat1
            if(N2_out < N2_in):
                map_out /= rat2

            PrintMessage(3, "Upgrading/Downgrading field done.")

            # Update object
            return Field(self.L0,self.L1,self.L2,self.corner0,self.corner1,self.corner2,self.rank,N0_out,N1_out,N2_out,self.time,map_out)
        else:
            return self

    def tint(self,Pk):
        """Tints a Gaussian random field (multiply by square root
        of a power spectrum in Fourier space)

        Parameters
        ----------
        Pk (PowerSpectrum) : input power spectrum

        """
        from pysbmy.power import tint
        return tint(self, Pk)

    def whiten(self,Pk):
        """Whitens a Gaussian random field (divide by square root
        of a power spectrum in Fourier space)

        Parameters
        ----------
        Pk (PowerSpectrum) : input power spectrum

        """
        from pysbmy.power import whiten
        return whiten(self, Pk)
#end class(Field)

read_basefield=BaseField.read
read_field=Field.read
Field_from_ranges=Field.from_ranges
compare_ranges=BaseFieldMeta.__eq__
