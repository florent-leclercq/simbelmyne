#!/usr/bin/env python
# -*- coding: latin-1 -*-
#-------------------------------------------------------------------------------------
# Simbelmynë v0.5.0 -- pysbmy/grf.py
# Copyright (C) 2012-2023 Florent Leclercq.
#
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# The text of the license is located in the root directory of the source package.
#-------------------------------------------------------------------------------------

"""Routines for dealing with Gaussian random fields
"""

__author__  = "Florent Leclercq"
__version__ = "0.5.0"
__date__    = "2013-2023"
__license__ = "GPLv3"

def generate_white_noise_from_seedtable(seedtable, L0, N0, L1=None, L2=None, N1=None, N2=None, corner0=0., corner1=0., corner2=0., time=1e-3):
    """Generates a white noise field from a seed table
    (will only generate in parallel if -DRANDOM_PARALLEL is enabled at compilation)

    Parameters
    ----------
    seedtable (array, double, dimension=Nthreads) : seed table for parallel random number generation
    L0 (double) : box size x
    N0 (int) : mesh size x
    L1 (optional, double, default=L0) : box size y
    L2 (optional, double, default=L0) : box size z
    N1 (optional, int, default=N0) : mesh size y
    N2 (optional, int, default=N0) : mesh size z
    corner0 (optional, double, default=0.) : x-position of the corner of the box with respect to the observer (which is at (0,0,0))
    corner1 (optional, double, default=0.) : y-position of the corner of the box with respect to the observer (which is at (0,0,0))
    corner2 (optional, double, default=0.) : z-position of the corner of the box with respect to the observer (which is at (0,0,0))
    time (optional, double, default=1e-3) : cosmic time at which the field is defined

    Returns
    -------
    WN (Field) : a white noise field

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, c_uint64, POINTER
    from pysbmy.field import Field
    N1=N1 or N0
    N2=N2 or N0
    L1=L1 or L0
    L2=L2 or L0
    Nthreads=len(seedtable)
    rank=1
    WN = np.zeros(N0*N1*N2, dtype=c_float_t)
    libSBMY.generate_white_noise_array.argtypes = [POINTER(c_float_t), c_int, c_int, c_int, POINTER(c_uint64), c_int]
    libSBMY.generate_white_noise_array.restype = None
    libSBMY.generate_white_noise_array(WN.ctypes.data_as(POINTER(c_float_t)), N0, N1, N2, seedtable.ctypes.data_as(POINTER(c_uint64)), Nthreads)
    return Field(L0,L1,L2,corner0,corner1,corner2,rank,N0,N1,N2,time,WN.reshape(N0,N1,N2))

def generate_white_noise(InputRngState, OutputRngState, L0, N0, L1=None, L2=None, N1=None, N2=None, corner0=0., corner1=0., corner2=0., time=1e-3, Nthreads=1):
    """Generates a white noise field in parallel
    (will only generate in parallel if -DRANDOM_PARALLEL is enabled at compilation)

    Parameters
    ----------
    InputRngState (string) : input state of the random number generator
    OutputRngState (string) : output state of the random number generator after generating the seed table
    L0 (double) : box size x
    N0 (int) : mesh size x
    L1 (optional, double, default=L0) : box size y
    L2 (optional, double, default=L0) : box size z
    N1 (optional, int, default=N0) : mesh size y
    N2 (optional, int, default=N0) : mesh size z
    corner0 (optional, double, default=0.) : x-position of the corner of the box with respect to the observer (which is at (0,0,0))
    corner1 (optional, double, default=0.) : y-position of the corner of the box with respect to the observer (which is at (0,0,0))
    corner2 (optional, double, default=0.) : z-position of the corner of the box with respect to the observer (which is at (0,0,0))
    time (optional, double, default=1e-3) : cosmic time at which the field is defined
    Nthreads (optional, int, default=1) : number of threads

    Returns
    -------
    WN (Field) : a white noise field

    """
    from pysbmy.rgen import seed_table_from_rngstate
    seedtable = seed_table_from_rngstate(InputRngState, Nthreads, OutputRngState)
    return generate_white_noise_from_seedtable(seedtable, L0, N0, L1, L2, N1, N2, corner0, corner1, corner2, time)

def upsample_wn(WN_IN, N0_target, InputRngState, SaveRngState, SavedRngState, OutputRngState):
    """Upsamples a white noise field

    Parameters
    ----------
    WN_IN (Field) : input white noise field
    N0_target (int) : desired output resolution
    InputRngState (string) : input RNG state
    SaveRngState (boolean) : save the RNG state before processing?
    SavedRngState (string) : output RNG state at the beginning
    OutputRngState (string) : output RNG state at the end

    Returns
    -------
    WN (Field) : upsampled white noise field

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, c_char, c_bool, POINTER
    from pysbmy.field import Field
    N0=WN_IN.N0
    N1=WN_IN.N1
    N2=WN_IN.N2
    L0=WN_IN.L0
    L1=WN_IN.L1
    L2=WN_IN.L2
    corner0=WN_IN.corner0
    corner1=WN_IN.corner1
    corner2=WN_IN.corner2
    rank=1
    time=WN_IN.time
    N2_target=N1_target=N0_target
    WN_OUT = np.zeros(N2_target*N1_target*N0_target, dtype=c_float_t)
    InputRngState = InputRngState.encode('ascii')
    SavedRngState = SavedRngState.encode('ascii')
    OutputRngState = OutputRngState.encode('ascii')
    libSBMY.upsample_wn.argtypes = [POINTER(c_float_t), c_int, POINTER(c_float_t), c_int, POINTER(c_char), c_bool, POINTER(c_char), POINTER(c_char)]
    libSBMY.upsample_wn.restype = None
    libSBMY.upsample_wn(WN_IN.data.reshape(N0,N1,N2).ctypes.data_as(POINTER(c_float_t)), N0, WN_OUT.ctypes.data_as(POINTER(c_float_t)), N0_target, InputRngState, SaveRngState, SavedRngState, OutputRngState)
    return Field(L0,L1,L2,corner0,corner1,corner2,rank,N0,N1,N2,time,WN_OUT.reshape(N2_target,N1_target,N0_target))

def tint_or_whiten_GRF(FIELD, Pk, tint):
    """Tints or whitens a Gaussian random field
    (multiply/divide by square root of a power spectrum in Fourier space)

    Parameters
    ----------
    FIELD (Field) : a Field structure containing a Gaussian random field
    Pk (PowerSpectrum) : a PowerSpectrum structure
    tint (boolean) : true=tint, false=whiten

    Returns
    -------
    F (Field) : the tinted/whitened Field

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, c_double, c_bool, POINTER
    from pysbmy.field import Field
    from pysbmy.utils import FatalError
    if(FIELD.N0 != Pk.N0 or FIELD.N1 != Pk.N1 or FIELD.N2 != Pk.N2 or FIELD.L0 != Pk.L0 or FIELD.L1 != Pk.L1 or FIELD.L2 != Pk.L2):FatalError("Dimensions should match.")
    N0=Pk.N0; N1=Pk.N1; N2=Pk.N2; N2_HC=Pk.N2_HC; N_HC=Pk.N_HC; NUM_MODES=Pk.NUM_MODES
    L0=Pk.L0; L1=Pk.L1; L2=Pk.L2
    corner0=FIELD.corner0
    corner1=FIELD.corner1
    corner2=FIELD.corner2
    time=FIELD.time
    RESULT=FIELD.data.reshape(N2*N1*N0)
    k_keys=Pk.k_keys.reshape(N2_HC*N1*N0)
    powerspectrum=Pk.powerspectrum
    libSBMY.tint_or_whiten_GRF.argtypes = [c_bool, POINTER(c_float_t), POINTER(c_int), POINTER(c_float_t), c_int, c_int, c_int, c_int, c_double, c_double, c_double]
    libSBMY.tint_or_whiten_GRF.restype = None
    libSBMY.tint_or_whiten_GRF(tint, RESULT.ctypes.data_as(POINTER(c_float_t)), k_keys.ctypes.data_as(POINTER(c_int)), powerspectrum.ctypes.data_as(POINTER(c_float_t)), NUM_MODES, N0, N1, N2, L0, L1, L2)
    return Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,RESULT.reshape(N0,N1,N2))

def tint(FIELD, Pk):
    """Tints a Gaussian random field (multiply by square root
    of a power spectrum in Fourier space)

    Parameters
    ----------
    FIELD (Field) : a Field structure containing a Gaussian random field
    Pk (PowerSpectrum) : a PowerSpectrum structure

    Returns
    -------
    F (Field) : the tinted Field

    """
    return tint_or_whiten_GRF(FIELD,Pk,True)

def whiten(FIELD, Pk):
    """Whitens a Gaussian random field (divide by square root
    of a power spectrum in Fourier space)

    Parameters
    ----------
    FIELD (Field) : a Field structure containing a Gaussian random field
    Pk (PowerSpectrum) : a PowerSpectrum structure

    Returns
    -------
    F (Field) : the whitened Field

    """
    return tint_or_whiten_GRF(FIELD,Pk,False)
