#!/usr/bin/env python
# -*- coding: latin-1 -*-
#-------------------------------------------------------------------------------------
# Simbelmynë v0.5.0 -- pysbmy/survey_geometry.py
# Copyright (C) 2012-2023 Florent Leclercq.
#
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# The text of the license is located in the root directory of the source package.
#-------------------------------------------------------------------------------------

"""Routines for working with Simbelmynë survey geometry structures
"""

__author__  = "Florent Leclercq"
__version__ = "0.5.0"
__date__    = "2015-2023"
__license__ = "GPLv3"

from pysbmy.field import BaseFieldMeta,BaseField

class SurveyGeometryMeta(object):
    """ This class contains meta-parameters for a survey geometry object:
        cosmo (array, double, dimension=N_COSMOPAR) : cosmological parameters
        bright_cut (array, double, dimension=N_CAT) : lower value of the cuts in magnitude in each subcatalog
        faint_cut (array, double, dimension=N_CAT) : upper value of the cuts in magnitude in each subcatalog
        rmin (array, double, dimension=N_CAT) : minimum comoving distance of objects in each subcatalog
        rmax (array, double, dimension=N_CAT) : maximum comoving distance of objects in each subcatalog
        zmin (array, double, dimension=N_CAT) : minimum redshift of objects in each subcatalog
        zmax (array, double, dimension=N_CAT) : maximum redshift of objects in each subcatalog
        N_BIAS (int) : number of bias parameters
        galaxy_bias_mean (array, double, dimensions=(N_CAT,N_BIAS)) : mean values of bias parameters in each subcatalog
        galaxy_bias_std (array, double, dimensions=(N_CAT,N_BIAS)) : standard deviations of bias parameters in each subcatalog
        galaxy_nmean_mean (array, double, dimension=N_CAT) : mean value of the expected number of galaxies in each subcatalog
        galaxy_nmean_std (array, double, dimension=N_CAT) : standard deviation of the expected number of galaxies in each subcatalog
    """

    # Initialization, properties and representation
    def __init__(self,cosmo,bright_cut,faint_cut,rmin,rmax,zmin,zmax,N_BIAS,galaxy_bias_mean,galaxy_bias_std,galaxy_nmean_mean,galaxy_nmean_std):
        """Initializes a SurveyGeometryMeta object

        Parameters
        ----------
        cosmo (array, double, dimension=N_COSMOPAR) : cosmological parameters
        bright_cut (array, double, dimension=N_CAT) : lower value of the cuts in magnitude in each subcatalog
        faint_cut (array, double, dimension=N_CAT) : upper value of the cuts in magnitude in each subcatalog
        rmin (array, double, dimension=N_CAT) : minimum comoving distance of objects in each subcatalog
        rmax (array, double, dimension=N_CAT) : maximum comoving distance of objects in each subcatalog
        zmin (array, double, dimension=N_CAT) : minimum redshift of objects in each subcatalog
        zmax (array, double, dimension=N_CAT) : maximum redshift of objects in each subcatalog
        N_BIAS (int) : number of bias parameters
        galaxy_bias_mean (array, double, dimensions=(N_CAT,N_BIAS)) : mean values of bias parameters in each subcatalog
        galaxy_bias_std (array, double, dimensions=(N_CAT,N_BIAS)) : standard deviations of bias parameters in each subcatalog
        galaxy_nmean_mean (array, double, dimension=N_CAT) : mean value of the expected number of galaxies in each subcatalog
        galaxy_nmean_std (array, double, dimension=N_CAT) : standard deviation of the expected number of galaxies in each subcatalog

        Returns
        -------
        SG (SurveyGeometryMeta)

        """
        self.cosmo=cosmo
        self.bright_cut=bright_cut
        self.faint_cut=faint_cut
        self.rmin=rmin
        self.rmax=rmax
        self.zmin=zmin
        self.zmax=zmax
        self.N_BIAS=N_BIAS
        self.galaxy_bias_mean=galaxy_bias_mean
        self.galaxy_bias_std=galaxy_bias_std
        self.galaxy_nmean_mean=galaxy_nmean_mean
        self.galaxy_nmean_std=galaxy_nmean_std

    def __repr__(self):
        ans="cosmo={}\n".format(self.cosmo)
        ans+="bright_cut={}\n".format(self.bright_cut)
        ans+="faint_cut={}\n".format(self.faint_cut)
        ans+="rmin={}\n".format(self.rmin)
        ans+="rmax={}\n".format(self.rmax)
        ans+="zmin={}\n".format(self.zmin)
        ans+="zmax={}\n".format(self.zmax)
        ans+="galaxy_bias_mean={}\n".format(self.galaxy_bias_mean)
        ans+="galaxy_bias_std={}\n".format(self.galaxy_bias_std)
        ans+="galaxy_nmean_mean={}\n".format(self.galaxy_nmean_mean)
        ans+="galaxy_nmean_std={}\n".format(self.galaxy_nmean_std)
        return ans

    # Equality
    def __eq__(self,other):
        import numpy as np
        return self.cosmo==other.cosmo and np.array_equal(self.bright_cut,other.bright_cut) and np.array_equal(self.faint_cut,other.faint_cut) and np.array_equal(self.rmin,other.rmin) and np.array_equal(self.rmax,other.rmax) and np.array_equal(self.zmin,other.zmin) and np.array_equal(self.zmax,other.zmax) and np.array_equal(self.N_BIAS,other.N_BIAS) and np.array_equal(self.galaxy_bias_mean,other.galaxy_bias_mean) and np.array_equal(self.galaxy_bias_std,other.galaxy_bias_std) and np.array_equal(self.galaxy_nmean_mean,other.galaxy_nmean_mean) and np.array_equal(self.galaxy_nmean_std,other.galaxy_nmean_std)

    # Writing
    def write(self,fname):
        """Writes a SurveyGeometryMeta object in an external hdf5 file

        Parameters
        ----------
        fname (string) : filename

        """
        import h5py as h5
        import numpy as np
        from pysbmy import c_int, c_double
        from pysbmy.cosmology import N_COSMOPAR, cosmo_dict_to_array
        N_COSMOPAR=N_COSMOPAR
        cosmo_array=cosmo_dict_to_array(self.cosmo)

        with h5.File(fname, 'a') as hf:
            hf.attrs.create('/info/scalars/N_COSMOPAR', N_COSMOPAR, dtype=c_int)
            hf.attrs.create('/info/scalars/cosmo', cosmo_array, dtype=c_double)
            hf.attrs.create('/info/scalars/bright_cut', self.bright_cut, dtype=c_double)
            hf.attrs.create('/info/scalars/faint_cut', self.faint_cut, dtype=c_double)
            hf.attrs.create('/info/scalars/rmin', self.rmin, dtype=c_double)
            hf.attrs.create('/info/scalars/rmax', self.rmax, dtype=c_double)
            hf.attrs.create('/info/scalars/zmin', self.zmin, dtype=c_double)
            hf.attrs.create('/info/scalars/zmax', self.zmax, dtype=c_double)
            hf.attrs.create('/info/scalars/N_BIAS', self.N_BIAS, dtype=c_int)
            hf.attrs.create('/info/scalars/galaxy_bias_mean', self.galaxy_bias_mean, dtype=c_double)
            hf.attrs.create('/info/scalars/galaxy_bias_std', self.galaxy_bias_std, dtype=c_double)
            hf.attrs.create('/info/scalars/galaxy_nmean_mean', self.galaxy_nmean_mean, dtype=c_double)
            hf.attrs.create('/info/scalars/galaxy_nmean_std', self.galaxy_nmean_std, dtype=c_double)

    # Reading
    @staticmethod
    def read(fname):
        """Reads a SurveyGeometryMeta object in an external hdf5 file

        Parameters
        ----------
        fname (string) : filename

        Returns
        -------
        SG (SurveyGeometryMeta)

        """
        import h5py as h5
        import numpy as np
        from pysbmy.cosmology import N_COSMOPAR, cosmo_array_to_dict
        with h5.File(fname,'r') as hf:
            N_COSMOPAR_READ = hf.attrs['/info/scalars/N_COSMOPAR']
            cosmo_array = hf.attrs['/info/scalars/cosmo']
            bright_cut = hf.attrs['/info/scalars/bright_cut']
            faint_cut = hf.attrs['/info/scalars/faint_cut']
            rmin = hf.attrs['/info/scalars/rmin']
            rmax = hf.attrs['/info/scalars/rmax']
            zmin = hf.attrs['/info/scalars/zmin']
            zmax = hf.attrs['/info/scalars/zmax']
            N_BIAS = hf.attrs['/info/scalars/N_BIAS']
            galaxy_bias_mean = hf.attrs['/info/scalars/galaxy_bias_mean']
            galaxy_bias_std = hf.attrs['/info/scalars/galaxy_bias_std']
            galaxy_nmean_mean = hf.attrs['/info/scalars/galaxy_nmean_mean']
            galaxy_nmean_std = hf.attrs['/info/scalars/galaxy_nmean_std']

        if N_COSMOPAR_READ!=N_COSMOPAR:
            PrintError("Number of cosmological parameters should match N_COSMOPAR, found {}, expected {}.".format(N_COSMOPAR_READ,N_COSMOPAR))
            raise ValueError("Number of cosmological parameters should match N_COSMOPAR, found {}, expected {}.".format(N_COSMOPAR_READ,N_COSMOPAR))
        else:
            cosmo=cosmo_array_to_dict(cosmo_array)
            return cosmo,bright_cut,faint_cut,rmin,rmax,zmin,zmax,N_BIAS,galaxy_bias_mean,galaxy_bias_std,galaxy_nmean_mean,galaxy_nmean_std
#end class(SurveyGeometryMeta)

class SurveyGeometry(BaseFieldMeta, SurveyGeometryMeta):
    """This is a superclass of BaseFieldMeta and SurveyGeometryMeta,
    containing all the meta-information for a SurveyGeometry object, and :
        galaxy_sel_window (array, Field, dimension=N_CAT) : the galaxy selection window (a Field object) for each subcatalog
    """

    # Initialization, properties and representation
    def __init__(self,L0,L1,L2,corner0,corner1,corner2,N0,N1,N2,N_CAT,cosmo,bright_cut,faint_cut,rmin,rmax,zmin,zmax,N_BIAS,galaxy_bias_mean,galaxy_bias_std,galaxy_nmean_mean,galaxy_nmean_std,galaxy_sel_window):
        """Initializes a SurveyGeometry object

        Parameters
        ----------
        L0 (double) : box size x
        L1 (double) : box size y
        L2 (double) : box size z
        corner0 (double) : x-position of the corner of the box with respect to the observer (which is at (0,0,0))
        corner1 (double) : y-position of the corner of the box with respect to the observer (which is at (0,0,0))
        corner2 (double) : z-position of the corner of the box with respect to the observer (which is at (0,0,0))
        N0 (int) : mesh size x
        N1 (int) : mesh size y
        N2 (int) : mesh size z
        N_CAT (int) : number of subcatalogs
        cosmo (array, double, dimension=N_COSMOPAR) : cosmological parameters
        bright_cut (array, double, dimension=N_CAT) : lower value of the cuts in magnitude in each subcatalog
        faint_cut (array, double, dimension=N_CAT) : upper value of the cuts in magnitude in each subcatalog
        rmin (array, double, dimension=N_CAT) : minimum comoving distance of objects in each subcatalog
        rmax (array, double, dimension=N_CAT) : maximum comoving distance of objects in each subcatalog
        zmin (array, double, dimension=N_CAT) : minimum redshift of objects in each subcatalog
        zmax (array, double, dimension=N_CAT) : maximum redshift of objects in each subcatalog
        N_BIAS (int) : number of bias parameters
        galaxy_bias_mean (array, double, dimensions=(N_CAT,N_BIAS)) : mean values of bias parameters in each subcatalog
        galaxy_bias_std (array, double, dimensions=(N_CAT,N_BIAS)) : standard deviations of bias parameters in each subcatalog
        galaxy_nmean_mean (array, double, dimension=N_CAT) : mean value of the expected number of galaxies in each subcatalog
        galaxy_nmean_std (array, double, dimension=N_CAT) : standard deviation of the expected number of galaxies in each subcatalog
        galaxy_sel_window (array, Field, dimension=N_CAT) : the galaxy selection window (a Field object) for each subcatalog

        Returns
        -------
        SG (SurveyGeometry)

        """
        from pysbmy.utils import PrintError
        rank=1
        BaseFieldMeta.__init__(self,L0,L1,L2,corner0,corner1,corner2,rank,N0,N1,N2)
        SurveyGeometryMeta.__init__(self,cosmo,bright_cut,faint_cut,rmin,rmax,zmin,zmax,N_BIAS,galaxy_bias_mean,galaxy_bias_std,galaxy_nmean_mean,galaxy_nmean_std)
        self.N_CAT=N_CAT
        if len(galaxy_sel_window)!=N_CAT:
            PrintError("Number of input scalar fields should match N_CAT, given {} field, N_CAT={}.".format(len(galaxy_sel_window),N_CAT))
            raise ValueError("Number of input scalar fields should match N_CAT, given {} field, N_CAT={}.".format(len(galaxy_sel_window),N_CAT))
        else:
            for ICAT in range(N_CAT):
                setattr(self,'galaxy_sel_window_'+str(ICAT),galaxy_sel_window[ICAT])

    def __repr__(self):
        ans=BaseFieldMeta.__repr__(self)
        ans+="N_CAT={}\n".format(self.N_CAT)
        ans+=SurveyGeometryMeta.__repr__(self)
        for ICAT in range(self.N_CAT):
            ans+="galaxy_sel_window_{}=np.array({}, dtype={})\n".format(ICAT,getattr(self,'galaxy_sel_window_'+str(ICAT)).shape,getattr(self,'galaxy_sel_window_'+str(ICAT)).dtype)
        return ans

    # Equality
    def __eq__(self,other):
        import numpy as np
        ans=BaseFieldMeta.__eq__(self,other) and SurveyGeometryMeta.__eq__(self,other) and self.N_CAT==other.N_CAT
        if not ans:
            return False
        else:
            for ICAT in range(self.N_CAT):
                ans=ans and np.array_equal(getattr(self,'galaxy_sel_window_'+str(ICAT)), getattr(other,'galaxy_sel_window_'+str(ICAT)))
            return ans

    # Writing
    def add_survey_geometry(self,fname):
        import h5py as h5
        import numpy as np
        from pysbmy import c_int, c_float
        with h5.File(fname, 'a') as hf:
            hf.attrs.create('/info/scalars/N_CAT', self.N_CAT, dtype=c_int)
            for ICAT in range(self.N_CAT):
                hf.create_dataset('/scalars/galaxy_sel_window_'+str(ICAT), data=getattr(self,'galaxy_sel_window_'+str(ICAT)).astype(c_float), dtype=c_float)

    def write(self,fname):
        """Writes a SurveyGeometry object in an external hdf5 file

        Parameters
        ----------
        fname (string) : filename

        """
        from pysbmy.utils import INDENT, UNINDENT, PrintMessage, PrintDiagnostic
        PrintMessage(3, "Write 3D survey geometry in data file '{}'...".format(fname))
        INDENT()

        BaseFieldMeta.write(self,fname)
        SurveyGeometryMeta.write(self,fname)
        self.add_survey_geometry(fname)

        PrintDiagnostic(4, "ranges={}".format(self.ranges))
        UNINDENT()
        PrintMessage(3, "Write 3D survey geometry in data file '{}' done.".format(fname))

    # Reading
    @staticmethod
    def read_survey_geometry(fname):
        import h5py as h5
        import numpy as np
        from pysbmy import c_float_t, c_int, c_float
        with h5.File(fname,'r') as hf:
            N_CAT = hf.attrs['/info/scalars/N_CAT']
            galaxy_sel_window = {}
            for ICAT in range(N_CAT):
                galaxy_sel_window[ICAT] = np.array(hf.get('scalars/galaxy_sel_window_'+str(ICAT)), dtype=c_float).astype(c_float_t)
        return N_CAT, galaxy_sel_window

    @staticmethod
    def read(fname):
        """Reads a SurveyGeometry object in an external hdf5 file

        Parameters
        ----------
        fname (string) : filename

        Returns
        -------
        SG (SurveyGeometry)

        """
        from pysbmy.utils import INDENT, UNINDENT, PrintMessage, PrintDiagnostic
        PrintMessage(3, "Read 3D survey geometry in data file '{}'...".format(fname))
        INDENT()

        L0,L1,L2,corner0,corner1,corner2,rank,N0,N1,N2=BaseFieldMeta.read(fname)
        cosmo,bright_cut,faint_cut,rmin,rmax,zmin,zmax,N_BIAS,galaxy_bias_mean,galaxy_bias_std,galaxy_nmean_mean,galaxy_nmean_std=SurveyGeometryMeta.read(fname)
        N_CAT, galaxy_sel_window=SurveyGeometry.read_survey_geometry(fname)
        SG=SurveyGeometry(L0,L1,L2,corner0,corner1,corner2,N0,N1,N2,N_CAT,cosmo,bright_cut,faint_cut,rmin,rmax,zmin,zmax,N_BIAS,galaxy_bias_mean,galaxy_bias_std,galaxy_nmean_mean,galaxy_nmean_std,galaxy_sel_window)

        PrintDiagnostic(4, "ranges={}".format(SG.ranges))
        UNINDENT()
        PrintMessage(3, "Read 3D survey geometry in data file '{}' done.".format(fname))
        return SG
#end class(SurveyGeometry)

class GalaxySelectionWindow(BaseField, SurveyGeometryMeta):
    """This is a superclass of BaseFieldMeta and SurveyGeometryMeta,
    containing all the meta-information for a GalaxySelectionWindow object, and :
        data (Field) : a scalar field (typically the galaxy selection window in ONE subcatalog)
    """

    # Initialization, properties and representation
    def __init__(self,L0,L1,L2,corner0,corner1,corner2,N0,N1,N2,data,cosmo,bright_cut,faint_cut,rmin,rmax,zmin,zmax,N_BIAS,galaxy_bias_mean,galaxy_bias_std,galaxy_nmean_mean,galaxy_nmean_std):
        """Initializes a GalaxySelectionWindow object

        Parameters
        ----------
        L0 (double) : box size x
        L1 (double) : box size y
        L2 (double) : box size z
        corner0 (double) : x-position of the corner of the box with respect to the observer (which is at (0,0,0))
        corner1 (double) : y-position of the corner of the box with respect to the observer (which is at (0,0,0))
        corner2 (double) : z-position of the corner of the box with respect to the observer (which is at (0,0,0))
        N0 (int) : mesh size x
        N1 (int) : mesh size y
        N2 (int) : mesh size z
        data (Field) : a scalar field (typically the galaxy selection window in ONE subcatalog)
        cosmo (array, double, dimension=N_COSMOPAR) : cosmological parameters
        bright_cut (array, double, dimension=1) : lower value of the cuts in magnitude in this subcatalog
        faint_cut (array, double, dimension=1) : upper value of the cuts in magnitude in this subcatalog
        rmin (array, double, dimension=1) : minimum comoving distance of objects in this subcatalog
        rmax (array, double, dimension=1) : maximum comoving distance of objects in this subcatalog
        zmin (array, double, dimension=1) : minimum redshift of objects in this subcatalog
        zmax (array, double, dimension=1) : maximum redshift of objects in this subcatalog
        N_BIAS (int) : number of bias parameters
        galaxy_bias_mean (array, double, dimensions=(1,N_BIAS)) : mean values of bias parameters in this subcatalog
        galaxy_bias_std (array, double, dimensions=(1,N_BIAS)) : standard deviations of bias parameters in this subcatalog
        galaxy_nmean_mean (array, double, dimension=1) : mean value of the expected number of galaxies in this subcatalog
        galaxy_nmean_std (array, double, dimension=1) : standard deviation of the expected number of galaxies in this subcatalog

        Returns
        -------
        GSW (GalaxySelectionWindow)

        """
        rank=1
        BaseField.__init__(self,L0,L1,L2,corner0,corner1,corner2,rank,N0,N1,N2,data)
        SurveyGeometryMeta.__init__(self,cosmo,bright_cut,faint_cut,rmin,rmax,zmin,zmax,N_BIAS,galaxy_bias_mean,galaxy_bias_std,galaxy_nmean_mean,galaxy_nmean_std)

    def __repr__(self):
        ans=BaseFieldMeta.__repr__(self)
        ans+=SurveyGeometryMeta.__repr__(self)
        ans+="data=np.array({}, dtype={})\n".format(self.data.shape,self.data.dtype)
        return ans

    # Equality
    def __eq__(self,other):
        return BaseField.__eq__(self,other) and SurveyGeometryMeta.__eq__(self,other)

    # Writing
    def write(self,fname):
        """Writes a GalaxySelectionWindow object in an external hdf5 file

        Parameters
        ----------
        fname (string) : filename

        """
        from pysbmy.utils import INDENT, UNINDENT, PrintMessage
        PrintMessage(3, "Write galaxy selection window in data file '{}'...".format(fname))
        INDENT()

        BaseField.write(self,fname)
        SurveyGeometryMeta.write(self,fname)

        UNINDENT()
        PrintMessage(3, "Write galaxy selection window in data file '{}' done.".format(fname))

    # Reading
    @staticmethod
    def read(fname,ICAT=None):
        """Reads a GalaxySelectionWindow object in an external hdf5 file

        Parameters
        ----------
        fname (string) : filename
        ICAT (optional, string, default=None) : index of subcatalog.
            The field address is '/scalars/galaxy_sel_window_ICAT' if ICAT is set,
            '/scalars/field' otherwise

        Returns
        -------
        GSW (GalaxySelectionWindow)

        """
        from pysbmy.utils import INDENT, UNINDENT, PrintMessage
        PrintMessage(3, "Read galaxy selection window in data file '{}'...".format(fname))
        INDENT()

        if ICAT is not None:
            field_address='/scalars/galaxy_sel_window_'+str(ICAT)
        else:
            field_address='/scalars/field'
        L0,L1,L2,corner0,corner1,corner2,rank,N0,N1,N2,data=BaseField.read(fname,field_address)
        cosmo,bright_cut,faint_cut,rmin,rmax,zmin,zmax,N_BIAS,galaxy_bias_mean,galaxy_bias_std,galaxy_nmean_mean,galaxy_nmean_std=SurveyGeometryMeta.read(fname)
        if ICAT is not None:
            bright_cut=bright_cut[ICAT]
            faint_cut=faint_cut[ICAT]
            rmin=rmin[ICAT]
            rmax=rmax[ICAT]
            zmin=zmin[ICAT]
            zmax=zmax[ICAT]
            galaxy_bias_mean=galaxy_bias_mean[ICAT]
            galaxy_bias_std=galaxy_bias_std[ICAT]
            galaxy_nmean_mean=galaxy_nmean_mean[ICAT]
            galaxy_nmean_std=galaxy_nmean_std[ICAT]
        W=GalaxySelectionWindow(L0,L1,L2,corner0,corner1,corner2,N0,N1,N2,data,cosmo,bright_cut,faint_cut,rmin,rmax,zmin,zmax,N_BIAS,galaxy_bias_mean,galaxy_bias_std,galaxy_nmean_mean,galaxy_nmean_std)

        UNINDENT()
        PrintMessage(3, "Read galaxy selection window in data file '{}' done.".format(fname))
        return W

read_survey_geometry=SurveyGeometry.read
read_galaxy_sel_window=GalaxySelectionWindow.read
