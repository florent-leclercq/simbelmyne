#!/usr/bin/env python
# -*- coding: latin-1 -*-
#-------------------------------------------------------------------------------------
# Simbelmynë v0.5.0 -- pysbmy/__init__.py
# Copyright (C) 2012-2023 Florent Leclercq.
#
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# The text of the license is located in the root directory of the source package.
#-------------------------------------------------------------------------------------

"""pysbmy __init__
"""

__author__  = "Florent Leclercq"
__version__ = "0.5.0"
__date__    = "2018-2023"
__license__ = "GPLv3"

#
# Function to test if a module exists
#
def module_exists(module_name):
    """Tests if a module exists

    Parameters
    ----------
    module_name (string) : module name

    Returns
    -------
    result (boolean) : whether the module can be imported

    """
    try:
        __import__(module_name)
    except ImportError:
        return False
    else:
        return True

#
# 1- Load the compilation options
#
# FIXME get compilation options at compilation time
SCREEN_VERBOSE_LEVEL = 7
HAVE_TYPE_AND_MASS = False
HAVE_EXTRAS = False
DOUBLEPRECISION = False
DEBUG = True

#
# 2- Load ctypes variables
#
from ctypes import CDLL, c_float, c_double, c_short, c_int, c_uint, c_uint64, c_bool, c_char, c_wchar, Structure, POINTER, CFUNCTYPE
c_float_t = c_double if DOUBLEPRECISION else c_float
c_fftw_complex = c_double*2
c_fftwf_complex = c_float*2
c_fftw_complex_t = c_float_t*2

#
# 3- Load the Simbelmynë shared object library
#
import pkg_resources, os
from .utils import FatalError
SBMY_LIB_FILE = pkg_resources.resource_filename('pysbmy', 'build/simbelmyne.so')
if not os.path.exists(SBMY_LIB_FILE):FatalError("simbelmyne.so file not found.")
libSBMY = CDLL(SBMY_LIB_FILE)

#
# 4- Load pysbmy main functions
#
from .pysbmy import *

#
# 5- Load modules including Simbelmynë extensions if they exist
#
def import_submodules(package, recursive=True):
    """ Import all submodules of a module, recursively, including subpackages

    Parameters
    ----------
    package (string): package (name or actual module)
    recursive (optional, boolean, default=True) : import submodules recursively?

    Returns
    -------
    results (dict[str, types.ModuleType])
    """
    import importlib, pkgutil
    if isinstance(package, str):
        package = importlib.import_module(package)
    results = {}
    for loader, name, is_pkg in pkgutil.walk_packages(package.__path__):
        full_name = package.__name__ + '.' + name
        results[full_name] = importlib.import_module(full_name)
        if recursive and is_pkg:
            results.update(import_submodules(full_name))
    return results

__all__ = import_submodules(__name__).keys()
