#!/usr/bin/env python
# -*- coding: latin-1 -*-
#-------------------------------------------------------------------------------------
# Simbelmynë v0.5.0 -- pysbmy/correlations.py
# Copyright (C) 2012-2023 Florent Leclercq.
#
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# The text of the license is located in the root directory of the source package.
#-------------------------------------------------------------------------------------

"""Routines for computing correlation functions
"""

__author__  = "Florent Leclercq"
__version__ = "0.5.0"
__date__    = "2014-2023"
__license__ = "GPLv3"

def get_crosscorrelation(A, B, G, AliasingCorr=False):
    """Computes the cross-correlation between two fields

    Parameters
    ----------
    A (Field) : first field
    B (Field) : second field
    G (FourierGrid) : input Fourier grid
    AliasingCorr (optional, boolean, default=False): use Jing's correction factor: Jing (2005), ApJ 620, 559
    assumes isotropic aliasing (approx. true for k < kmax=knyquist)
    the formula is for CIC interpolation scheme, which we assume

    Returns
    -------
    PABk (array) : cross-correlation mean
    VABk (array) : cross-correlation variance
    Rk (array) : cross-correlation coefficient mean
    VRk (array) : cross-correlation coefficient variance
    (R is defined as P_AB/sqrt(P_A*P_B))

    """
    def aux_get_crosscorrelation(A, B, k_keys, k_nmodes, NUM_MODES, AliasingCorr):
        import numpy as np
        from pysbmy import libSBMY, c_float_t, c_int, c_double, c_bool, POINTER
        from pysbmy.field import compare_ranges
        from pysbmy.utils import FatalError
        if not compare_ranges(A,B):FatalError("Dimensions of fields should match.")
        N0=A.N0
        N1=A.N1
        N2=A.N2
        L0=A.L0
        L1=A.L1
        L2=A.L2
        k_keys = k_keys.astype(c_int)
        k_nmodes = k_nmodes.astype(c_int)
        PABk = np.zeros(NUM_MODES, dtype=c_float_t)
        VABk = np.zeros(NUM_MODES, dtype=c_float_t)
        Rk = np.zeros(NUM_MODES, dtype=c_float_t)
        VRk = np.zeros(NUM_MODES, dtype=c_float_t)
        libSBMY.get_crosscorrelation_fs.argtypes = [POINTER(c_float_t), POINTER(c_float_t), c_int, POINTER(c_int), POINTER(c_int), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), c_int, c_int, c_int, c_double, c_double, c_double, c_bool]
        libSBMY.get_crosscorrelation_fs.restype = None
        libSBMY.get_crosscorrelation_fs(A.data.reshape(N0*N1*N2).ctypes.data_as(POINTER(c_float_t)), B.data.reshape(N0*N1*N2).ctypes.data_as(POINTER(c_float_t)), NUM_MODES, k_keys.ctypes.data_as(POINTER(c_int)), k_nmodes.ctypes.data_as(POINTER(c_int)), PABk.ctypes.data_as(POINTER(c_float_t)), VABk.ctypes.data_as(POINTER(c_float_t)), Rk.ctypes.data_as(POINTER(c_float_t)), VRk.ctypes.data_as(POINTER(c_float_t)), N0, N1, N2, L0, L1, L2, AliasingCorr)
        return PABk, VABk, Rk, VRk

    from pysbmy.field import compare_ranges
    from pysbmy.utils import FatalError
    if not compare_ranges(A,B):FatalError("Dimensions of fields should match.")
    if A.N0!=G.N0 or A.N1!=G.N1 or A.N2!=G.N2 or A.L0!=G.L0 or A.L1!=G.L1 or A.L2!=G.L2:FatalError("Dimensions of field and Fourier grid should match.")
    PABk, VABk, Rk, VRk = aux_get_crosscorrelation(A, B, G.k_keys, G.k_nmodes, G.NUM_MODES, AliasingCorr)
    return PABk, VABk, Rk, VRk

def get_autocorrelation(A, G, AliasingCorr=True):
    """Computes the auto-correlation of a field

    Parameters
    ----------
    A (Field) : field
    G (FourierGrid) : input Fourier grid
    AliasingCorr (optional, boolean, default=True): use Jing's correction factor: Jing (2005), ApJ 620, 559
    assumes isotropic aliasing (approx. true for k < kmax=knyquist)
    the formula is for CIC interpolation scheme, which we assume

    Returns
    -------
    Pk (array) : power spectrum mean
    Vk (array) : power spectrum variance

    """
    def aux_get_autocorrelation(A, k_keys, k_nmodes, NUM_MODES, AliasingCorr):
        import numpy as np
        from pysbmy import libSBMY, c_float_t, c_int, c_double, c_bool, POINTER
        N0=A.N0
        N1=A.N1
        N2=A.N2
        L0=A.L0
        L1=A.L1
        L2=A.L2
        k_keys = k_keys.astype(c_int)
        k_nmodes = k_nmodes.astype(c_int)
        Pk = np.zeros(NUM_MODES, dtype=c_float_t)
        Vk = np.zeros(NUM_MODES, dtype=c_float_t)
        libSBMY.get_autocorrelation_fs.argtypes = [POINTER(c_float_t), c_int, POINTER(c_int), POINTER(c_int), POINTER(c_float_t), POINTER(c_float_t), c_int, c_int, c_int, c_double, c_double, c_double, c_bool]
        libSBMY.get_autocorrelation_fs.restype = None
        libSBMY.get_autocorrelation_fs(A.data.reshape(N0*N1*N2).ctypes.data_as(POINTER(c_float_t)), NUM_MODES, k_keys.ctypes.data_as(POINTER(c_int)), k_nmodes.ctypes.data_as(POINTER(c_int)), Pk.ctypes.data_as(POINTER(c_float_t)), Vk.ctypes.data_as(POINTER(c_float_t)), N0, N1, N2, L0, L1, L2, AliasingCorr)
        return Pk, Vk

    from pysbmy.utils import FatalError
    if A.N0!=G.N0 or A.N1!=G.N1 or A.N2!=G.N2 or A.L0!=G.L0 or A.L1!=G.L1 or A.L2!=G.L2:FatalError("Dimensions of field and Fourier grid should match.")
    Pk, Vk = aux_get_autocorrelation(A, G.k_keys, G.k_nmodes, G.NUM_MODES, AliasingCorr)
    return Pk, Vk
