#!/usr/bin/env python
# -*- coding: latin-1 -*-
#-------------------------------------------------------------------------------------
# Simbelmynë v0.5.0 -- pysbmy/lpt.py
# Copyright (C) 2012-2023 Florent Leclercq.
#
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# The text of the license is located in the root directory of the source package.
#-------------------------------------------------------------------------------------

"""General routines for Lagrangian perturbation theory
and routines for calculation of LPT displacement fields
"""

__author__  = "Florent Leclercq"
__version__ = "0.5.0"
__date__    = "2012-2023"
__license__ = "GPLv3"

def compute_LPT_displacements(DELTA, Np0, Np1=None, Np2=None):
    """Computes Lagrangian perturbation theory (LPT) displacements
        from a given initial density field

    Parameters
    ----------
    DELTA (Field) : input density contrast field
    Np0 (int) : particle grid size x
    Np1 (optional, int, default=Np0) : particle grid size y
    Np2 (optional, int, default=Np0) : particle grid size z

    Returns
    -------
    PSI_1 (Field) : First-order LPT displacements
    PSI_2 (Field) : Second-order LPT displacements

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, c_double, POINTER
    from pysbmy.field import Field
    N2=DELTA.N0
    N1=DELTA.N1
    N0=DELTA.N2
    L0=DELTA.L0
    L1=DELTA.L1
    L2=DELTA.L2
    corner0=DELTA.corner0
    corner1=DELTA.corner1
    corner2=DELTA.corner2
    time=DELTA.time
    rank=3
    Np1=Np1 or Np0
    Np2=Np2 or Np0
    psix_1=np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    psiy_1=np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    psiz_1=np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    psix_2=np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    psiy_2=np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    psiz_2=np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    libSBMY.compute_LPT_displacements.argtypes = [POINTER(c_float_t), c_int, c_int, c_int, c_double, c_double, c_double, c_int, c_int, c_int, POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t)]
    libSBMY.compute_LPT_displacements.restype = None
    libSBMY.compute_LPT_displacements(DELTA.data.reshape(N0*N1*N2).ctypes.data_as(POINTER(c_float_t)), N0, N1, N2, L0, L1, L2, Np0, Np1, Np2, psix_1.ctypes.data_as(POINTER(c_float_t)), psiy_1.ctypes.data_as(POINTER(c_float_t)), psiz_1.ctypes.data_as(POINTER(c_float_t)), psix_2.ctypes.data_as(POINTER(c_float_t)), psiy_2.ctypes.data_as(POINTER(c_float_t)), psiz_2.ctypes.data_as(POINTER(c_float_t)))
    PSI_1_DATA=np.array((psix_1.reshape(Np2,Np1,Np0),psiy_1.reshape(Np2,Np1,Np0),psiz_1.reshape(Np2,Np1,Np0)))
    PSI_1 = Field(L0,L1,L2,corner0,corner1,corner2,rank,Np0,Np1,Np2,time,PSI_1_DATA)
    PSI_2_DATA=np.array((psix_2.reshape(Np2,Np1,Np0),psiy_2.reshape(Np2,Np1,Np0),psiz_2.reshape(Np2,Np1,Np0)))
    PSI_2 = Field(L0,L1,L2,corner0,corner1,corner2,rank,Np0,Np1,Np2,time,PSI_2_DATA)
    return PSI_1, PSI_2
