#!/usr/bin/env python
# -*- coding: latin-1 -*-
#-------------------------------------------------------------------------------------
# Simbelmynë v0.5.0 -- pysbmy/pysbmy.py
# Copyright (C) 2012-2023 Florent Leclercq.
#
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# The text of the license is located in the root directory of the source package.
#-------------------------------------------------------------------------------------

"""Main infrastructure script for Simbelmynë
"""

__author__  = "Florent Leclercq"
__version__ = "0.5.0"
__date__    = "2013-2023"
__license__ = "GPLv3"

from pysbmy import Structure

#
# Define the Simbelmynë parameter file class and the python wrapper
#
class param_file(Structure):
    """This class represents a Simbelmynë parameter file
    See include/config.h for the list of parameters
    """
    from collections import OrderedDict
    from pysbmy import c_int, c_wchar, c_double

    MAXSTRINGLENGTH = 500
    SBMY_PARAMFILE_NCATEGORY = 6
    SBMY_PARAMFILE_DEFAULT=OrderedDict()

    SBMY_PARAMFILE_DEFAULT[0]="Setup"
    SBMY_PARAMFILE_DEFAULT["fields0"]=OrderedDict()
    SBMY_PARAMFILE_DEFAULT["fields0"]["SnapFormat"]=[c_int,3]
    SBMY_PARAMFILE_DEFAULT["fields0"]["NumFilesPerSnapshot"]=[c_int,1]

    SBMY_PARAMFILE_DEFAULT[1]="Module LPT"
    SBMY_PARAMFILE_DEFAULT["fields1"]=OrderedDict()
    SBMY_PARAMFILE_DEFAULT["fields1"]["ModuleLPT"]=[c_int,1]
    SBMY_PARAMFILE_DEFAULT["fields1"]["InputRngStateLPT"]=[c_wchar*MAXSTRINGLENGTH,"0"]
    SBMY_PARAMFILE_DEFAULT["fields1"]["COMMENT0"]="# Set this to '0' to reset the random number generator"
    SBMY_PARAMFILE_DEFAULT["fields1"]["OutputRngStateLPT"]=[c_wchar*MAXSTRINGLENGTH,"dummy.rng"]
    SBMY_PARAMFILE_DEFAULT["fields1"]["COMMENT1"]="\n# Basic setup: -------------------------"
    SBMY_PARAMFILE_DEFAULT["fields1"]["Particles"]=[c_int,128]
    SBMY_PARAMFILE_DEFAULT["fields1"]["COMMENT2"]="# Number of particles in each dimension of the initial grid"
    SBMY_PARAMFILE_DEFAULT["fields1"]["Mesh"]=[c_int,128]
    SBMY_PARAMFILE_DEFAULT["fields1"]["COMMENT3"]="# The grid on which densities are computed"
    SBMY_PARAMFILE_DEFAULT["fields1"]["BoxSize"]=[c_double,200.]
    SBMY_PARAMFILE_DEFAULT["fields1"]["corner0"]=[c_double,-100.]
    SBMY_PARAMFILE_DEFAULT["fields1"]["corner1"]=[c_double,-100.]
    SBMY_PARAMFILE_DEFAULT["fields1"]["corner2"]=[c_double,-100.]
    SBMY_PARAMFILE_DEFAULT["fields1"]["COMMENT4"]="\n# Initial conditions: ------------------"
    SBMY_PARAMFILE_DEFAULT["fields1"]["ICsMode"]=[c_int,0]
    SBMY_PARAMFILE_DEFAULT["fields1"]["COMMENT5"]="# 0 : the codes generates white noise, then initial conditions"
    SBMY_PARAMFILE_DEFAULT["fields1"]["COMMENT6"]="# 1 : external white noise specified, the code multiplies by the power spectrum"
    SBMY_PARAMFILE_DEFAULT["fields1"]["COMMENT7"]="# 2 : external initial conditions specified"
    SBMY_PARAMFILE_DEFAULT["fields1"]["WriteICsRngState"]=[c_int,0]
    SBMY_PARAMFILE_DEFAULT["fields1"]["OutputICsRngState"]=[c_wchar*MAXSTRINGLENGTH,"dummy.rng"]
    SBMY_PARAMFILE_DEFAULT["fields1"]["WriteWhiteNoise"]=[c_int,0]
    SBMY_PARAMFILE_DEFAULT["fields1"]["OutputWhiteNoise"]=[c_wchar*MAXSTRINGLENGTH,"initial_density_white.h5"]
    SBMY_PARAMFILE_DEFAULT["fields1"]["COMMENT8"]="# Only used if ICsMode=0"
    SBMY_PARAMFILE_DEFAULT["fields1"]["InputWhiteNoise"]=[c_wchar*MAXSTRINGLENGTH,"initial_density_white.h5"]
    SBMY_PARAMFILE_DEFAULT["fields1"]["COMMENT9"]="# Only used if ICsMode=1"
    SBMY_PARAMFILE_DEFAULT["fields1"]["InputInitialConditions"]=[c_wchar*MAXSTRINGLENGTH,"initial_density.h5"]
    SBMY_PARAMFILE_DEFAULT["fields1"]["COMMENT10"]="# Only used if ICsMode=2"
    SBMY_PARAMFILE_DEFAULT["fields1"]["WriteInitialConditions"]=[c_int,0]
    SBMY_PARAMFILE_DEFAULT["fields1"]["OutputInitialConditions"]=[c_wchar*MAXSTRINGLENGTH,"initial_density.h5"]
    SBMY_PARAMFILE_DEFAULT["fields1"]["COMMENT11"]="# At a scale factor of a=1e-3"
    SBMY_PARAMFILE_DEFAULT["fields1"]["COMMENT12"]="\n# Power spectrum: ----------------------"
    SBMY_PARAMFILE_DEFAULT["fields1"]["InputPowerSpectrum"]=[c_wchar*MAXSTRINGLENGTH,"input_power.h5"]
    SBMY_PARAMFILE_DEFAULT["fields1"]["COMMENT13"]="# Only used if ICsMode=0 or 1"
    SBMY_PARAMFILE_DEFAULT["fields1"]["COMMENT14"]="\n# Final conditions: --------------------"
    SBMY_PARAMFILE_DEFAULT["fields1"]["RedshiftLPT"]=[c_double,0.]
    SBMY_PARAMFILE_DEFAULT["fields1"]["WriteLPTSnapshot"]=[c_int,1]
    SBMY_PARAMFILE_DEFAULT["fields1"]["OutputLPTSnapshot"]=[c_wchar*MAXSTRINGLENGTH,"lpt_particles.gadget3"]
    SBMY_PARAMFILE_DEFAULT["fields1"]["WriteLPTDensity"]=[c_int,1]
    SBMY_PARAMFILE_DEFAULT["fields1"]["OutputLPTDensity"]=[c_wchar*MAXSTRINGLENGTH,"lpt_density.h5"]

    SBMY_PARAMFILE_DEFAULT[2]="Module PM/COLA"
    SBMY_PARAMFILE_DEFAULT["fields2"]=OrderedDict()
    SBMY_PARAMFILE_DEFAULT["fields2"]["ModulePMCOLA"]=[c_int,0]
    SBMY_PARAMFILE_DEFAULT["fields2"]["EvolutionMode"]=[c_int,2]
    SBMY_PARAMFILE_DEFAULT["fields2"]["COMMENT0"]="# 1 : PM"
    SBMY_PARAMFILE_DEFAULT["fields2"]["COMMENT1"]="# 2 : tCOLA"
    SBMY_PARAMFILE_DEFAULT["fields2"]["COMMENT2"]="# 3 : sCOLA"
    SBMY_PARAMFILE_DEFAULT["fields2"]["ParticleMesh"]=[c_int,128]
    SBMY_PARAMFILE_DEFAULT["fields2"]["COMMENT3"]="# The grid used in the particle-mesh code (N for PM/tCOLA or N+1 for each sCOLA box)"
    SBMY_PARAMFILE_DEFAULT["fields2"]["COMMENT4"]="# If EvolutionMode=1 or 2 (PM/tCOLA): number of grid points per dimension (N) for FFTs"
    SBMY_PARAMFILE_DEFAULT["fields2"]["COMMENT5"]="# If EvolutionMode=3 (sCOLA): number of grid points per dimension (N+1) for DSTs in each box"
    SBMY_PARAMFILE_DEFAULT["fields2"]["NumberOfTimeSteps"]=[c_int,10]
    SBMY_PARAMFILE_DEFAULT["fields2"]["TimeStepDistribution"]=[c_int,0]
    SBMY_PARAMFILE_DEFAULT["fields2"]["COMMENT6"]="# 0 : linear in the scale factor"
    SBMY_PARAMFILE_DEFAULT["fields2"]["COMMENT7"]="# 1 : logarithmic in the scale factor"
    SBMY_PARAMFILE_DEFAULT["fields2"]["COMMENT8"]="# 2 : exponential in the scale factor"
    SBMY_PARAMFILE_DEFAULT["fields2"]["ModifiedDiscretization"]=[c_int,1]
    SBMY_PARAMFILE_DEFAULT["fields2"]["COMMENT9"]="# Use modified discretization of Kick and Drift operators in COLA"
    SBMY_PARAMFILE_DEFAULT["fields2"]["n_LPT"]=[c_double,-2.5]
    SBMY_PARAMFILE_DEFAULT["fields2"]["COMMENT10"]="# Exponent for the Ansatz in the modified Kick and Drift operators in COLA, only used if ModifiedDiscretization=1"
    SBMY_PARAMFILE_DEFAULT["fields2"]["COMMENT11"]="\n# Intermediate snapshots: --------------"
    SBMY_PARAMFILE_DEFAULT["fields2"]["WriteSnapshots"]=[c_int,0]
    SBMY_PARAMFILE_DEFAULT["fields2"]["OutputSnapshotsBase"]=[c_wchar*MAXSTRINGLENGTH,"particles_"]
    SBMY_PARAMFILE_DEFAULT["fields2"]["OutputSnapshotsExt"]=[c_wchar*MAXSTRINGLENGTH,".gadget3"]
    SBMY_PARAMFILE_DEFAULT["fields2"]["WriteDensities"]=[c_int,0]
    SBMY_PARAMFILE_DEFAULT["fields2"]["OutputDensitiesBase"]=[c_wchar*MAXSTRINGLENGTH,"density_"]
    SBMY_PARAMFILE_DEFAULT["fields2"]["OutputDensitiesExt"]=[c_wchar*MAXSTRINGLENGTH,".h5"]
    SBMY_PARAMFILE_DEFAULT["fields2"]["COMMENT12"]="\n# Final snapshot: ----------------------"
    SBMY_PARAMFILE_DEFAULT["fields2"]["RedshiftFCs"]=[c_double,0.]
    SBMY_PARAMFILE_DEFAULT["fields2"]["WriteFinalSnapshot"]=[c_int,0]
    SBMY_PARAMFILE_DEFAULT["fields2"]["OutputFinalSnapshot"]=[c_wchar*MAXSTRINGLENGTH,"final_particles.gadget3"]
    SBMY_PARAMFILE_DEFAULT["fields2"]["WriteFinalDensity"]=[c_int,1]
    SBMY_PARAMFILE_DEFAULT["fields2"]["OutputFinalDensity"]=[c_wchar*MAXSTRINGLENGTH,"final_density.h5"]
    SBMY_PARAMFILE_DEFAULT["fields2"]["COMMENT13"]="\n# COCA specific parameters: -----------"
    SBMY_PARAMFILE_DEFAULT["fields2"]["WriteReferenceFrame"]=[c_int,0]
    SBMY_PARAMFILE_DEFAULT["fields2"]["OutputKickBase"]=[c_wchar*MAXSTRINGLENGTH,"output_kick_"]
    SBMY_PARAMFILE_DEFAULT["fields2"]["OutputKickExt"]=[c_wchar*MAXSTRINGLENGTH,".h5"]
    SBMY_PARAMFILE_DEFAULT["fields2"]["OutputDriftBase"]=[c_wchar*MAXSTRINGLENGTH,"output_drift_"]
    SBMY_PARAMFILE_DEFAULT["fields2"]["OutputDriftExt"]=[c_wchar*MAXSTRINGLENGTH,".h5"]
    SBMY_PARAMFILE_DEFAULT["fields2"]["ReadReferenceFrame"]=[c_int,0]
    SBMY_PARAMFILE_DEFAULT["fields2"]["InputKickBase"]=[c_wchar*MAXSTRINGLENGTH,"input_kick_"]
    SBMY_PARAMFILE_DEFAULT["fields2"]["InputKickExt"]=[c_wchar*MAXSTRINGLENGTH,".h5"]
    SBMY_PARAMFILE_DEFAULT["fields2"]["InputDriftBase"]=[c_wchar*MAXSTRINGLENGTH,"input_drift_"]
    SBMY_PARAMFILE_DEFAULT["fields2"]["InputDriftExt"]=[c_wchar*MAXSTRINGLENGTH,".h5"]
    SBMY_PARAMFILE_DEFAULT["fields2"]["COMMENT14"]="\n# sCOLA specific parameters: ----------"
    SBMY_PARAMFILE_DEFAULT["fields2"]["NumberOfTilesPerDimension"]=[c_int,2]
    SBMY_PARAMFILE_DEFAULT["fields2"]["NumberOfParticlesInBuffer"]=[c_int,16]
    SBMY_PARAMFILE_DEFAULT["fields2"]["OutputLPTPotential1"]=[c_wchar*MAXSTRINGLENGTH,"lpt_potential_1.h5"]
    SBMY_PARAMFILE_DEFAULT["fields2"]["OutputLPTPotential2"]=[c_wchar*MAXSTRINGLENGTH,"lpt_potential_2.h5"]
    SBMY_PARAMFILE_DEFAULT["fields2"]["OutputTilesBase"]=[c_wchar*MAXSTRINGLENGTH,"scola_particles_tile_"]
    SBMY_PARAMFILE_DEFAULT["fields2"]["OutputTilesExt"]=[c_wchar*MAXSTRINGLENGTH,".h5"]

    SBMY_PARAMFILE_DEFAULT[3]="Module RSD"
    SBMY_PARAMFILE_DEFAULT["fields3"]=OrderedDict()
    SBMY_PARAMFILE_DEFAULT["fields3"]["ModuleRSD"]=[c_int,0]
    SBMY_PARAMFILE_DEFAULT["fields3"]["DoNonLinearMapping"]=[c_int,0]
    SBMY_PARAMFILE_DEFAULT["fields3"]["vobs0"]=[c_double,0.]
    SBMY_PARAMFILE_DEFAULT["fields3"]["vobs1"]=[c_double,0.]
    SBMY_PARAMFILE_DEFAULT["fields3"]["vobs2"]=[c_double,0.]
    SBMY_PARAMFILE_DEFAULT["fields3"]["COMMENT0"]="# Velocity of the observer with respect to the CMB/galaxies frame"
    SBMY_PARAMFILE_DEFAULT["fields3"]["alpha1RSD"]=[c_double,1.]
    SBMY_PARAMFILE_DEFAULT["fields3"]["DoLPTSplit"]=[c_int,0]
    SBMY_PARAMFILE_DEFAULT["fields3"]["alpha2RSD"]=[c_double,1.]
    SBMY_PARAMFILE_DEFAULT["fields3"]["COMMENT1"]="# RSD model:"
    SBMY_PARAMFILE_DEFAULT["fields3"]["COMMENT2"]="# if doLPTSplit=false:"
    SBMY_PARAMFILE_DEFAULT["fields3"]["COMMENT3"]="# \tz_obs = z_cosmo + (1+z_cosmo) * alpha1RSD * z_pec"
    SBMY_PARAMFILE_DEFAULT["fields3"]["COMMENT4"]="# else:"
    SBMY_PARAMFILE_DEFAULT["fields3"]["COMMENT5"]="# \tz_obs = z_cosmo + (1+z_cosmo) * (alpha1RSD * z_pec_LPT + alpha2RSD * (z_pec-z_pec_LPT))"
    SBMY_PARAMFILE_DEFAULT["fields3"]["COMMENT6"]="# alpha1RSD=alpha2RSD=1 in LCDM"

    SBMY_PARAMFILE_DEFAULT["fields3"]["COMMENT7"]="\n# Reshift-space snapshot: --------------"
    SBMY_PARAMFILE_DEFAULT["fields3"]["WriteRSSnapshot"]=[c_int,0]
    SBMY_PARAMFILE_DEFAULT["fields3"]["OutputRSSnapshot"]=[c_wchar*MAXSTRINGLENGTH,"rs_particles.gadget3"]
    SBMY_PARAMFILE_DEFAULT["fields3"]["WriteRSDensity"]=[c_int,1]
    SBMY_PARAMFILE_DEFAULT["fields3"]["OutputRSDensity"]=[c_wchar*MAXSTRINGLENGTH,"rs_density.h5"]

    SBMY_PARAMFILE_DEFAULT[4]="Module Mock Catalogs"
    SBMY_PARAMFILE_DEFAULT["fields4"]=OrderedDict()
    SBMY_PARAMFILE_DEFAULT["fields4"]["ModuleMocks"]=[c_int,0]
    SBMY_PARAMFILE_DEFAULT["fields4"]["InputRngStateMocks"]=[c_wchar*MAXSTRINGLENGTH,"0"]
    SBMY_PARAMFILE_DEFAULT["fields4"]["COMMENT0"]="# Set this to '0' to reset the random number generator"
    SBMY_PARAMFILE_DEFAULT["fields4"]["OutputRngStateMocks"]=[c_wchar*MAXSTRINGLENGTH,"dummy.rng"]
    SBMY_PARAMFILE_DEFAULT["fields4"]["WriteMocksRngState"]=[c_int,0]
    SBMY_PARAMFILE_DEFAULT["fields4"]["OutputMocksRngState"]=[c_wchar*MAXSTRINGLENGTH,"dummy.rng"]
    SBMY_PARAMFILE_DEFAULT["fields4"]["NoiseModel"]=[c_int,1]
    SBMY_PARAMFILE_DEFAULT["fields4"]["COMMENT1"]="# 0 : Gaussian linear model"
    SBMY_PARAMFILE_DEFAULT["fields4"]["COMMENT2"]="# 1 : Poisson model"
    SBMY_PARAMFILE_DEFAULT["fields4"]["NumberOfNoiseRealizations"]=[c_int,5]
    SBMY_PARAMFILE_DEFAULT["fields4"]["COMMENT3"]="# Should be about NumberOfkBins^2 for a Poisson noise model"
    SBMY_PARAMFILE_DEFAULT["fields4"]["COMMENT4"]="\n# Inputs: ------------------------------"
    SBMY_PARAMFILE_DEFAULT["fields4"]["InputDensityMocks"]=[c_wchar*MAXSTRINGLENGTH,"lpt_density.h5"]
    SBMY_PARAMFILE_DEFAULT["fields4"]["COMMENT5"]="# Only used if ModuleLPT is switched off, uses final density produced by LPT/PM/COLA otherwise"
    SBMY_PARAMFILE_DEFAULT["fields4"]["InputSurveyGeometry"]=[c_wchar*MAXSTRINGLENGTH,"input_survey_geometry.h5"]
    SBMY_PARAMFILE_DEFAULT["fields4"]["InputSummaryStatskGrid"]=[c_wchar*MAXSTRINGLENGTH,"input_ss_k_grid.h5"]
    SBMY_PARAMFILE_DEFAULT["fields4"]["COMMENT6"]="# Only mandatory if WriteSummaryStats=1"
    SBMY_PARAMFILE_DEFAULT["fields4"]["COMMENT7"]="\n# Output mocks: ------------------------"
    SBMY_PARAMFILE_DEFAULT["fields4"]["WriteMocks"]=[c_int,1]
    SBMY_PARAMFILE_DEFAULT["fields4"]["OutputMockBase"]=[c_wchar*MAXSTRINGLENGTH,"output_mock_"]
    SBMY_PARAMFILE_DEFAULT["fields4"]["OutputMockExt"]=[c_wchar*MAXSTRINGLENGTH,".h5"]
    SBMY_PARAMFILE_DEFAULT["fields4"]["COMMENT8"]="\n# Output summaries: --------------------"
    SBMY_PARAMFILE_DEFAULT["fields4"]["WriteSummaryStats"]=[c_int,1]
    SBMY_PARAMFILE_DEFAULT["fields4"]["OutputSummaryStats"]=[c_wchar*MAXSTRINGLENGTH,"output_ss.h5"]

    SBMY_PARAMFILE_DEFAULT[5]="Cosmological model"
    SBMY_PARAMFILE_DEFAULT["fields5"]=OrderedDict()
    SBMY_PARAMFILE_DEFAULT["fields5"]["COMMENT0"]="# Planck 2015 cosmological parameters (Planck 2015 XIII, p31, table 4, last column)"
    SBMY_PARAMFILE_DEFAULT["fields5"]["COMMENT1"]="# The equation of state of dark energy is parametrized by w(a) = w0_fld + (1-a)/a0 * wa_fld"
    SBMY_PARAMFILE_DEFAULT["fields5"]["h"]=[c_double,0.6774]
    SBMY_PARAMFILE_DEFAULT["fields5"]["Omega_r"]=[c_double,0.]
    SBMY_PARAMFILE_DEFAULT["fields5"]["Omega_q"]=[c_double,0.6911]
    SBMY_PARAMFILE_DEFAULT["fields5"]["Omega_b"]=[c_double,0.0486]
    SBMY_PARAMFILE_DEFAULT["fields5"]["Omega_m"]=[c_double,0.3089]
    SBMY_PARAMFILE_DEFAULT["fields5"]["Omega_k"]=[c_double,0.]
    SBMY_PARAMFILE_DEFAULT["fields5"]["n_s"]=[c_double,0.9667]
    SBMY_PARAMFILE_DEFAULT["fields5"]["sigma8"]=[c_double,0.8159]
    SBMY_PARAMFILE_DEFAULT["fields5"]["w0_fld"]=[c_double,-1.]
    SBMY_PARAMFILE_DEFAULT["fields5"]["wa_fld"]=[c_double,0.]

    _fields_ = []
    for CAT in range(SBMY_PARAMFILE_NCATEGORY):
        for key, value in SBMY_PARAMFILE_DEFAULT["fields"+str(CAT)].items():
            if not "COMMENT" in key:
                _fields_.append((key, value[0]))

    def __init__(self, **kwargs):
        SBMY_PARAMFILE_NCATEGORY=self.SBMY_PARAMFILE_NCATEGORY
        SBMY_PARAMFILE_DEFAULT=self.SBMY_PARAMFILE_DEFAULT
        for CAT in range(SBMY_PARAMFILE_NCATEGORY):
            for key, default_value in SBMY_PARAMFILE_DEFAULT["fields"+str(CAT)].items():
                if not "COMMENT" in key:
                    if key in kwargs:
                        setattr(self, key, kwargs[key])
                    else:
                        setattr(self, key, default_value[1])

    def __repr__(self):
        SBMY_PARAMFILE_NCATEGORY=self.SBMY_PARAMFILE_NCATEGORY
        SBMY_PARAMFILE_DEFAULT=self.SBMY_PARAMFILE_DEFAULT
        ans="""\
## ----------------------------------------------------------------------------------------------------------------------- ##
## -----------------------------      _           _          _                             ------------------------------- ##
## -----------------------------     (_)         | |        | |                            ------------------------------- ##
## -----------------------------  ___ _ _ __ ___ | |__   ___| |_ __ ___  _   _ _ __   ___  ------------------------------- ##
## ----------------------------- / __| | '_ ` _ \| '_ \ / _ \ | '_ ` _ \| | | | '_ \ / _ \ ------------------------------- ##
## ----------------------------- \__ \ | | | | | | |_) |  __/ | | | | | | |_| | | | |  __/ ------------------------------- ##
## ----------------------------- |___/_|_| |_| |_|_.__/ \___|_|_| |_| |_|\__, |_| |_|\___| ------------------------------- ##
## -----------------------------                                          __/ |            ------------------------------- ##
## -----------------------------                                         |___/             ------------------------------- ##
## ----------------------------------------------------------------------------------------------------------------------- ##\n"""
        for CAT in range(SBMY_PARAMFILE_NCATEGORY):
            ans+="\n## ----------------------------------------------------------------------------------------------------------------------- ##\n"
            ans+="## {} {} ##\n".format(SBMY_PARAMFILE_DEFAULT[CAT],"-"*(126-len(SBMY_PARAMFILE_DEFAULT[CAT])-8))
            ans+="## ----------------------------------------------------------------------------------------------------------------------- ##\n"
            for key, value in self.SBMY_PARAMFILE_DEFAULT["fields"+str(CAT)].items():
                if "COMMENT" in key:
                    ans+="{}\n".format(value)
                else:
                    ans+="{0:50}{1}\n".format(key,getattr(self,key))
        return ans[:-1]

    def write(self, fname):
        """Writes a Simbelmynë parameter file to an ascii file

        Parameters
        ----------
        fname (string) : filename

        """
        from pysbmy.utils import PrintMessage
        message="Writing parameter file in '{}'...".format(fname)
        PrintMessage(4, message)
        sbmyfile = open(fname, "w")
        sbmyfile.write(self.__repr__())
        message="Writing parameter file in '{}' done.".format(fname)
        PrintMessage(4, message)
        sbmyfile.close()

def pySbmy(fname_sbmyfile, fname_logs=None):
    """Calls the Simbelmynë executable

    Parameters
    ----------
    fname_sbmyfile (string) : name of the Simbelmynë configuration file
    fname_logs (optional, string, default=None) : filename of the logs
        Logs are not saved if fname_logs is not specified.

    """
    from pysbmy import libSBMY, c_int, c_char, POINTER
    from ctypes import create_string_buffer

    LP_c_char = POINTER(c_char)
    LP_LP_c_char = POINTER(LP_c_char)

    argc = 3 if fname_logs is not None else 2
    argv = (LP_c_char * (argc + 1))()
    argv[0] = create_string_buffer("simbelmyne".encode('ascii'))
    argv[1] = create_string_buffer(fname_sbmyfile.encode('ascii'))
    if fname_logs is not None: argv[2] = create_string_buffer(fname_logs.encode('ascii'))

    libSBMY.main_sbmy.argtypes = [c_int, LP_LP_c_char]
    libSBMY.main_sbmy.restype = c_int
    return libSBMY.main_sbmy(argc, argv)
