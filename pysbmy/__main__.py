#!/usr/bin/env python
# -*- coding: latin-1 -*-
#-------------------------------------------------------------------------------------
# Simbelmynë v0.5.0 -- pysbmy/__main__.py
# Copyright (C) 2012-2023 Florent Leclercq.
#
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# The text of the license is located in the root directory of the source package.
#-------------------------------------------------------------------------------------

"""pysbmy __init__
"""

__author__  = "Florent Leclercq"
__version__ = "0.5.0"
__date__    = "2018-2023"
__license__ = "GPLv3"

def simbelmyne(args=None):
    """A wrapper to call the Simbelmynë executable

    """
    import sys
    from pysbmy import libSBMY, c_int, c_char, POINTER
    from ctypes import create_string_buffer

    LP_c_char = POINTER(c_char)
    LP_LP_c_char = POINTER(LP_c_char)

    argc = len(sys.argv)
    argv = (LP_c_char * (argc + 1))()
    for i, arg in enumerate(sys.argv):
        enc_arg = arg.encode('ascii')
        argv[i] = create_string_buffer(enc_arg)

    libSBMY.main_sbmy.argtypes = [c_int, LP_LP_c_char]
    libSBMY.main_sbmy.restype = c_int
    return libSBMY.main_sbmy(argc, argv)

def scola(args=None):
    """A wrapper to call the Simbelmynë scola executable

    """
    import sys
    from pysbmy import libSBMY, c_int, c_char, POINTER
    from ctypes import create_string_buffer
    import argparse

    parser = argparse.ArgumentParser(description="Evolve a list of sCOLA boxes.")
    parser.add_argument("parameter_file", help="Input parameter file")
    parser.add_argument("log_file", help="Output log file (optional)", nargs='?')
    parser.add_argument('-b','--boxes', nargs='+', help="List of sCOLA box numbers (0 to NumberOfTilesPerDimension^3-1)", required=True)
    args = parser.parse_args()

    LP_c_char = POINTER(c_char)
    LP_LP_c_char = POINTER(LP_c_char)
    libSBMY.main_scola.argtypes = [c_int, LP_LP_c_char]
    libSBMY.main_scola.restype = c_int

    for box in args.boxes:
        if(args.log_file):
            argc = 4
            argv = (LP_c_char * (argc + 1))()
            for i, arg in enumerate([sys.argv[0],args.parameter_file,str(box),args.log_file+'_'+str(box)]):
                enc_arg = arg.encode('ascii')
                argv[i] = create_string_buffer(enc_arg)
        else:
            argc = 3
            argv = (LP_c_char * (argc + 1))()
            for i, arg in enumerate([sys.argv[0],args.parameter_file,str(box)]):
                enc_arg = arg.encode('ascii')
                argv[i] = create_string_buffer(enc_arg)

        libSBMY.main_scola(argc, argv)

if __name__ == "__main__":
    simbelmyne()
