#!/usr/bin/env python
# -*- coding: latin-1 -*-
#-------------------------------------------------------------------------------------
# Simbelmynë v0.5.0 -- pysbmy/velocity.py
# Copyright (C) 2012-2023 Florent Leclercq.
#
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# The text of the license is located in the root directory of the source package.
#-------------------------------------------------------------------------------------

"""Routines for dealing with velocity fields
"""

__author__  = "Florent Leclercq"
__version__ = "0.5.0"
__date__    = "2013-2023"
__license__ = "GPLv3"

def get_velocities_pm_snapshot(P, N0, N1=None, N2=None, corner0=0., corner1=0., corner2=0.):
    """Computes the velocity field from standard particle assignment to the mesh

    Parameters
    ----------
    P (Snapshot) : input particles
    N0 (int) : mesh size of the desired velocity grid (x direction or all directions)
    N1 (optional, int, default=N0) : mesh size of the desired velocity grid (y direction)
    N2 (optional, int, default=N0) : mesh size of the desired velocity grid (z direction)
    corner0 (optional, double, default=0.) : x-position of the corner of the box with respect to the observer (which is at (0,0,0))
    corner1 (optional, double, default=0.) : y-position of the corner of the box with respect to the observer (which is at (0,0,0))
    corner2 (optional, double, default=0.) : z-position of the corner of the box with respect to the observer (which is at (0,0,0))

    Returns
    -------
    V (Field) : the velocity field

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, POINTER
    from pysbmy.field import Field
    from pysbmy.snapshot import Snapshot
    N1=N1 or N0
    N2=N2 or N0
    L0=P.L0
    L1=P.L1
    L2=P.L2
    time=P.time
    VELOCITY_X = np.zeros(N0*N1*N2, dtype=c_float_t)
    VELOCITY_Y = np.zeros(N0*N1*N2, dtype=c_float_t)
    VELOCITY_Z = np.zeros(N0*N1*N2, dtype=c_float_t)
    libSBMY.get_velocities_pm_snapshot.argtypes = [Snapshot, c_int, POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t)]
    libSBMY.get_velocities_pm_snapshot.restype = None
    libSBMY.get_velocities_pm_snapshot(P, N0, VELOCITY_X.ctypes.data_as(POINTER(c_float_t)), VELOCITY_Y.ctypes.data_as(POINTER(c_float_t)), VELOCITY_Z.ctypes.data_as(POINTER(c_float_t)))
    return Field(L0,L1,L2,corner0,corner1,corner2,3,N0,N1,N2,time,np.array((VELOCITY_X.reshape(N0,N1,N2),VELOCITY_Y.reshape(N0,N1,N2),VELOCITY_Z.reshape(N0,N1,N2))))

def get_divV_fs(VELOCITY):
    """Gets the divergence of the velocity field

    Parameters
    ----------
    VELOCITY (Field) : the input velocity field (vector field)

    Returns
    -------
    DIV_V (Field) : the output divergence of the velocity field (scalar field)

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, c_double, POINTER
    from pysbmy.field import Field
    from pysbmy.utils import FatalError
    if not VELOCITY.rank==3:FatalError("Input field should be a vector.")
    N0=VELOCITY.N0
    N1=VELOCITY.N1
    N2=VELOCITY.N2
    L0=VELOCITY.L0
    L1=VELOCITY.L1
    L2=VELOCITY.L2
    corner0=VELOCITY.corner0
    corner1=VELOCITY.corner1
    corner2=VELOCITY.corner2
    rank=1
    time=VELOCITY.time
    DIVV = np.zeros(N0*N1*N2, dtype=c_float_t)
    libSBMY.get_divV_fs.argtypes = [POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), c_int, c_int, c_int, c_double, c_double, c_double]
    libSBMY.get_divV_fs.restype = None
    libSBMY.get_divV_fs(VELOCITY.data[0].ctypes.data_as(POINTER(c_float_t)), VELOCITY.data[1].ctypes.data_as(POINTER(c_float_t)), VELOCITY.data[2].ctypes.data_as(POINTER(c_float_t)), DIVV.ctypes.data_as(POINTER(c_float_t)), N0, N1, N2, L0, L1, L2)
    DIV_V = Field(L0,L1,L2,corner0,corner1,corner2,rank,N0,N1,N2,time,DIVV.reshape(N0,N1,N2))
    return DIV_V

def get_rotV_fs(VELOCITY):
    """Gets the rotational of the velocity field

    Parameters
    ----------
    VELOCITY (Field) : the input velocity field (vector field)

    Returns
    -------
    ROT_V (Field) : the rotational of the velocity field (vector field)

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, c_double, POINTER
    from pysbmy.field import Field
    from pysbmy.utils import FatalError
    if not VELOCITY.rank==3:FatalError("Input field should be a vector.")
    N0=VELOCITY.N0
    N1=VELOCITY.N1
    N2=VELOCITY.N2
    L0=VELOCITY.L0
    L1=VELOCITY.L1
    L2=VELOCITY.L2
    corner0=VELOCITY.corner0
    corner1=VELOCITY.corner1
    corner2=VELOCITY.corner2
    rank=3
    time=VELOCITY.time
    ROTV_X = np.zeros(N0*N1*N2, dtype=c_float_t)
    ROTV_Y = np.zeros(N0*N1*N2, dtype=c_float_t)
    ROTV_Z = np.zeros(N0*N1*N2, dtype=c_float_t)
    libSBMY.get_rotV_fs.argtypes = [POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), c_int, c_int, c_int, c_double, c_double, c_double]
    libSBMY.get_rotV_fs.restype = None
    libSBMY.get_rotV_fs(VELOCITY.data[0].ctypes.data_as(POINTER(c_float_t)), VELOCITY.data[1].ctypes.data_as(POINTER(c_float_t)), VELOCITY.data[2].ctypes.data_as(POINTER(c_float_t)), ROTV_X.ctypes.data_as(POINTER(c_float_t)), ROTV_Y.ctypes.data_as(POINTER(c_float_t)), ROTV_Z.ctypes.data_as(POINTER(c_float_t)), N0, N1, N2, L0, L1, L2)
    ROT_V = Field(L0,L1,L2,corner0,corner1,corner2,rank,N0,N1,N2,time,np.array((ROTV_X.reshape(N0,N1,N2),ROTV_Y.reshape(N0,N1,N2),ROTV_Z.reshape(N0,N1,N2))))
    return ROT_V
