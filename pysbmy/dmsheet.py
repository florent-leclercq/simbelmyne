#!/usr/bin/env python
# -*- coding: latin-1 -*-
#-------------------------------------------------------------------------------------
# Simbelmynë v0.5.0 -- pysbmy/dmsheet.py
# Copyright (C) 2012-2023 Florent Leclercq.
#
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# The text of the license is located in the root directory of the source package.
#-------------------------------------------------------------------------------------

"""Routines for working with the Lagrangian displacement field
"""

__author__  = "Florent Leclercq"
__version__ = "0.5.0"
__date__    = "2015-2023"
__license__ = "GPLv3"

def get_masstracers(P):
    """Computes the mass tracers from a snapshot of flow tracers

    Parameters
    ----------
    P (Snapshot) : input snapshot containing the flow tracers

    Returns
    -------
    P_MASS (Snapshot) : output snapshot containing the mass tracers

    """
    from pysbmy import libSBMY, c_int
    from pysbmy.snapshot import Snapshot
    Np0=P.Np0
    Np1=P.Np1
    Np2=P.Np2
    libSBMY.get_masstracers.argtypes = [Snapshot, c_int, c_int, c_int]
    libSBMY.get_masstracers.restype = Snapshot
    P_MASS = libSBMY.get_masstracers(P, Np0, Np1, Np2)
    return P_MASS

def lagrangian_transport(P, OBJECTS, N0, N1=None, N2=None):
    """Does the Lagrangian transport of various quantities

    Parameters
    ----------
    P (Snapshot) : input snapshot containing the flow tracers
    OBJECTS (Field) : input structure types on the Lagrangian grid
    N0 (int) : mesh size of the output Eulerian grid (x direction or all directions)
    N1 (optional, int, default=N0) : mesh size of the output Eulerian grid (y direction)
    N2 (optional, int, default=N0) : mesh size of the output Eulerian grid (z direction)

    Returns
    -------
    DENSITYMASSTRACERS (Field) : output density contrast from the mass tracers (scalar field, Eulerian grid)
    PRIMORDIALSTREAMDENSITY (Field) : output primordial stream density (scalar field, Lagrangian grid)
    SECONDARYSTREAMDENSITY (Field) : output secondary stream density (scalar field, Eulerian grid)
    DENSITYTETRAHEDRA (Field) : output density from projecting all tetrahedra (scalar field, Eulerian grid)
    VELOCITY (Field) : output velocity field (vector field, Eulerian grid)
    VOIDS (Field) : output probability voids (scalar field, Eulerian grid)
    SHEETS (Field) : output probability sheets (scalar field, Eulerian grid)
    FILAMENTS (Field) : output probability filaments (scalar field, Eulerian grid)
    CLUSTERS (Field) : output probability clusters (scalar field, Eulerian grid)

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, c_bool, POINTER
    from pysbmy.field import Field
    from pysbmy.snapshot import Snapshot
    from pysbmy.utils import FatalError
    Np=P.Np
    Np0=OBJECTS.N0
    Np1=OBJECTS.N1
    Np2=OBJECTS.N2
    if(Np!=Np2*Np1*Np0):FatalError("Dimension of OBJECTS should match the number of particles in P.")
    N1=N1 or N0
    N2=N2 or N0
    L0=OBJECTS.L0
    L1=OBJECTS.L1
    L2=OBJECTS.L2
    corner0=OBJECTS.corner0
    corner1=OBJECTS.corner1
    corner2=OBJECTS.corner2
    time=OBJECTS.time
    doDENSITYMASSTRACERS = True
    DENSITYMASSTRACERS = np.zeros(N0*N1*N2, dtype=c_float_t)
    doPRIMORDIALSTREAMDENSITY = True
    PRIMORDIALSTREAMDENSITY = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    doSECONDARYSTREAMDENSITY = True
    SECONDARYSTREAMDENSITY = np.zeros(N0*N1*N2, dtype=c_float_t)
    doDENSITYTETRAHEDRA = True
    DENSITYTETRAHEDRA = np.zeros(N0*N1*N2, dtype=c_float_t)
    doVELOCITY = True
    VELOCITY_X = np.zeros(N0*N1*N2, dtype=c_float_t)
    VELOCITY_Y = np.zeros(N0*N1*N2, dtype=c_float_t)
    VELOCITY_Z = np.zeros(N0*N1*N2, dtype=c_float_t)
    doSTRUCTURES = True
    VOIDS = np.zeros(N0*N1*N2, dtype=c_float_t)
    SHEETS = np.zeros(N0*N1*N2, dtype=c_float_t)
    FILAMENTS = np.zeros(N0*N1*N2, dtype=c_float_t)
    CLUSTERS = np.zeros(N0*N1*N2, dtype=c_float_t)
    libSBMY.lagrangian_transport.argtypes = [Snapshot, POINTER(c_float_t), c_int, c_int, c_int, c_int, c_int, c_int, c_bool, POINTER(c_float_t), c_bool, POINTER(c_float_t), c_bool, POINTER(c_float_t), c_bool, POINTER(c_float_t), c_bool, POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), c_bool, POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t)]
    libSBMY.lagrangian_transport.restype = None
    libSBMY.lagrangian_transport(P, OBJECTS.data.ctypes.data_as(POINTER(c_float_t)), Np0, Np1, Np2, N0, N1, N2, doDENSITYMASSTRACERS, DENSITYMASSTRACERS.ctypes.data_as(POINTER(c_float_t)), doPRIMORDIALSTREAMDENSITY, PRIMORDIALSTREAMDENSITY.ctypes.data_as(POINTER(c_float_t)), doSECONDARYSTREAMDENSITY, SECONDARYSTREAMDENSITY.ctypes.data_as(POINTER(c_float_t)), doDENSITYTETRAHEDRA, DENSITYTETRAHEDRA.ctypes.data_as(POINTER(c_float_t)), doVELOCITY, VELOCITY_X.ctypes.data_as(POINTER(c_float_t)), VELOCITY_Y.ctypes.data_as(POINTER(c_float_t)), VELOCITY_Z.ctypes.data_as(POINTER(c_float_t)), doSTRUCTURES, VOIDS.ctypes.data_as(POINTER(c_float_t)), SHEETS.ctypes.data_as(POINTER(c_float_t)), FILAMENTS.ctypes.data_as(POINTER(c_float_t)), CLUSTERS.ctypes.data_as(POINTER(c_float_t)))
    F_DENSITYMASSTRACERS=Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,DENSITYMASSTRACERS.reshape(N0,N1,N2))
    F_PRIMORDIALSTREAMDENSITY=Field(L0,L1,L2,corner0,corner1,corner2,1,Np0,Np1,Np2,0.001,PRIMORDIALSTREAMDENSITY.reshape(Np2,Np1,Np0))
    F_SECONDARYSTREAMDENSITY=Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,SECONDARYSTREAMDENSITY.reshape(N0,N1,N2))
    F_DENSITYTETRAHEDRA=Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,DENSITYTETRAHEDRA.reshape(N0,N1,N2))
    F_VELOCITY=Field(L0,L1,L2,corner0,corner1,corner2,3,N0,N1,N2,time,np.array((VELOCITY_X.reshape(N0,N1,N2),VELOCITY_Y.reshape(N0,N1,N2),VELOCITY_Z.reshape(N0,N1,N2))))
    F_VOIDS=Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,VOIDS.reshape(N0,N1,N2))
    F_SHEETS=Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,SHEETS.reshape(N0,N1,N2))
    F_FILAMENTS=Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,FILAMENTS.reshape(N0,N1,N2))
    F_CLUSTERS=Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,CLUSTERS.reshape(N0,N1,N2))
    return F_DENSITYMASSTRACERS, F_PRIMORDIALSTREAMDENSITY, F_SECONDARYSTREAMDENSITY, F_DENSITYTETRAHEDRA, F_VELOCITY, F_VOIDS, F_SHEETS, F_FILAMENTS, F_CLUSTERS

def get_density_masstracers(P, N0, N1=None, N2=None, corner0=0., corner1=0., corner2=0.):
    """Computes the density contrast from mass tracer particles

    Parameters
    ----------
    P (Snapshot) : input snapshot
    N0 (int) : mesh size of the desired density grid (x direction or all directions)
    N1 (optional, int, default=N0) : mesh size of the desired density grid (y direction)
    N2 (optional, int, default=N0) : mesh size of the desired density grid (z direction)
    corner0 (optional, double, default=0.) : x-position of the corner of the box with respect to the observer (which is at (0,0,0))
    corner1 (optional, double, default=0.) : y-position of the corner of the box with respect to the observer (which is at (0,0,0))
    corner2 (optional, double, default=0.) : z-position of the corner of the box with respect to the observer (which is at (0,0,0))

    Returns
    -------
    DENSITYMASSTRACERS (Field) : output density contrast from the mass tracers (scalar field, Eulerian grid)

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, POINTER
    from pysbmy.field import Field
    from pysbmy.snapshot import Snapshot
    Np0=P.Np0
    Np1=P.Np1
    Np2=P.Np2
    N1=N1 or N0
    N2=N2 or N0
    L0=P.L0
    L1=P.L1
    L2=P.L2
    time=P.time
    DENSITYMASSTRACERS = np.zeros(N0*N1*N2, dtype=c_float_t)
    libSBMY.get_density_masstracers.argtypes = [Snapshot, c_int, c_int, c_int, c_int, c_int, c_int, POINTER(c_float_t)]
    libSBMY.get_density_masstracers.restype = None
    libSBMY.get_density_masstracers(P, Np0, Np1, Np2, N0, N1, N2, DENSITYMASSTRACERS.ctypes.data_as(POINTER(c_float_t)))
    return Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,DENSITYMASSTRACERS.reshape(N0,N1,N2))

def get_density_tetrahedra(P, N0, N1=None, N2=None, corner0=0., corner1=0., corner2=0.):
    """Computes the density contrast from tetrahedra

    Parameters
    ----------
    P (Snapshot) : input snapshot
    N0 (int) : mesh size of the desired density grid (x direction or all directions)
    N1 (optional, int, default=N0) : mesh size of the desired density grid (y direction)
    N2 (optional, int, default=N0) : mesh size of the desired density grid (z direction)
    corner0 (optional, double, default=0.) : x-position of the corner of the box with respect to the observer (which is at (0,0,0))
    corner1 (optional, double, default=0.) : y-position of the corner of the box with respect to the observer (which is at (0,0,0))
    corner2 (optional, double, default=0.) : z-position of the corner of the box with respect to the observer (which is at (0,0,0))

    Returns
    -------
    DENSITYTETRAHEDRA (Field) : output density from projecting all tetrahedra (scalar field, Eulerian grid)

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, POINTER
    from pysbmy.field import Field
    from pysbmy.snapshot import Snapshot
    Np0=P.Np0
    Np1=P.Np1
    Np2=P.Np2
    N1=N1 or N0
    N2=N2 or N0
    L0=P.L0
    L1=P.L1
    L2=P.L2
    time=P.time
    DENSITYTETRAHEDRA = np.zeros(N0*N1*N2, dtype=c_float_t)
    libSBMY.get_density_tetrahedra.argtypes = [Snapshot,  c_int, c_int, c_int, c_int, c_int, c_int, POINTER(c_float_t)]
    libSBMY.get_density_tetrahedra.restype = None
    libSBMY.get_density_tetrahedra(P, Np0, Np1, Np2, N0, N1, N2, DENSITYTETRAHEDRA.ctypes.data_as(POINTER(c_float_t)))
    return Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,DENSITYTETRAHEDRA.reshape(N0,N1,N2))

def get_stream_density(P, N0, N1=None, N2=None, corner0=0., corner1=0., corner2=0.):
    """Gets stream density (primordial on the Lagrangian mesh, secondary on the Eulerian mesh)

    Parameters
    ----------
    P (Snapshot) : input snapshot
    N0 (int) : mesh size of the output Eulerian grid (x direction or all directions)
    N1 (optional, int, default=N0) : mesh size of the output Eulerian grid (y direction)
    N2 (optional, int, default=N0) : mesh size of the output Eulerian grid (z direction)
    corner0 (optional, double, default=0.) : x-position of the corner of the box with respect to the observer (which is at (0,0,0))
    corner1 (optional, double, default=0.) : y-position of the corner of the box with respect to the observer (which is at (0,0,0))
    corner2 (optional, double, default=0.) : z-position of the corner of the box with respect to the observer (which is at (0,0,0))

    Returns
    -------
    PRIMORDIALSTREAMDENSITY (Field) : output primordial stream density (scalar field, Lagrangian grid)
    SECONDARYSTREAMDENSITY (Field) : output secondary stream density (scalar field, Eulerian grid)

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, POINTER
    from pysbmy.field import Field
    from pysbmy.snapshot import Snapshot
    Np0=P.Np0
    Np1=P.Np1
    Np2=P.Np2
    N1=N1 or N0
    N2=N2 or N0
    L0=P.L0
    L1=P.L1
    L2=P.L2
    time=P.time
    PRIMORDIALSTREAMDENSITY = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    SECONDARYSTREAMDENSITY = np.zeros(N0*N1*N2, dtype=c_float_t)
    libSBMY.get_stream_density.argtypes = [Snapshot, c_int, c_int, c_int, c_int, c_int, c_int, POINTER(c_float_t), POINTER(c_float_t)]
    libSBMY.get_stream_density.restype = None
    libSBMY.get_stream_density(P, Np0, Np1, Np2, N0, N1, N2, PRIMORDIALSTREAMDENSITY.ctypes.data_as(POINTER(c_float_t)), SECONDARYSTREAMDENSITY.ctypes.data_as(POINTER(c_float_t)))
    F_PRIMORDIALSTREAMDENSITY=Field(L0,L1,L2,corner0,corner1,corner2,1,Np0,Np1,Np2,0.001,PRIMORDIALSTREAMDENSITY.reshape(Np2,Np1,Np0))
    F_SECONDARYSTREAMDENSITY=Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,SECONDARYSTREAMDENSITY.reshape(N0,N1,N2))
    return F_PRIMORDIALSTREAMDENSITY, F_SECONDARYSTREAMDENSITY

def lagrangian_transport_velocities(P, N0, N1=None, N2=None, corner0=0., corner1=0., corner2=0.):
    """Lagrangian transport of velocities

    Parameters
    ----------
    P (Snapshot) : input snapshot
    N0 (int) : mesh size of the output Eulerian grid (x direction or all directions)
    N1 (optional, int, default=N0) : mesh size of the output Eulerian grid (y direction)
    N2 (optional, int, default=N0) : mesh size of the output Eulerian grid (z direction)
    corner0 (optional, double, default=0.) : x-position of the corner of the box with respect to the observer (which is at (0,0,0))
    corner1 (optional, double, default=0.) : y-position of the corner of the box with respect to the observer (which is at (0,0,0))
    corner2 (optional, double, default=0.) : z-position of the corner of the box with respect to the observer (which is at (0,0,0))

    Returns
    -------
    VELOCITY (Field) : output velocity field (vector field, Eulerian grid)

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, POINTER
    from pysbmy.field import Field
    from pysbmy.snapshot import Snapshot
    Np0=P.Np0
    Np1=P.Np1
    Np2=P.Np2
    N1=N1 or N0
    N2=N2 or N0
    L0=P.L0
    L1=P.L1
    L2=P.L2
    time=P.time
    VELOCITY_X = np.zeros(N0*N1*N2, dtype=c_float_t)
    VELOCITY_Y = np.zeros(N0*N1*N2, dtype=c_float_t)
    VELOCITY_Z = np.zeros(N0*N1*N2, dtype=c_float_t)
    libSBMY.lagrangian_transport_velocities.argtypes = [Snapshot, c_int, c_int, c_int, c_int, c_int, c_int, POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t)]
    libSBMY.lagrangian_transport_velocities.restype = None
    libSBMY.lagrangian_transport_velocities(P, Np0, Np1, Np2, N0, N1, N2, VELOCITY_X.ctypes.data_as(POINTER(c_float_t)), VELOCITY_Y.ctypes.data_as(POINTER(c_float_t)), VELOCITY_Z.ctypes.data_as(POINTER(c_float_t)))
    return Field(L0,L1,L2,corner0,corner1,corner2,3,N0,N1,N2,time,np.array((VELOCITY_X.reshape(N0,N1,N2),VELOCITY_Y.reshape(N0,N1,N2),VELOCITY_Z.reshape(N0,N1,N2))))

def lagrangian_transport_structures(P, OBJECTS, N0, N1=None, N2=None):
    """Lagrangian transport of structure types

    Parameters
    ----------
    P (Snapshot) : input snapshot
    OBJECTS (Field) : input structure types on the Lagrangian grid
    N0 (int) : mesh size of the output Eulerian grid (x direction or all directions)
    N1 (optional, int, default=N0) : mesh size of the output Eulerian grid (y direction)
    N2 (optional, int, default=N0) : mesh size of the output Eulerian grid (z direction)

    Returns
    -------
    VOIDS (Field) : output probability voids (scalar field, Eulerian grid)
    SHEETS (Field) : output probability sheets (scalar field, Eulerian grid)
    FILAMENTS (Field) : output probability filaments (scalar field, Eulerian grid)
    CLUSTERS (Field) : output probability clusters (scalar field, Eulerian grid)

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, POINTER
    from pysbmy.field import Field
    from pysbmy.snapshot import Snapshot
    from pysbmy.utils import FatalError
    Np=P.Np
    Np0=OBJECTS.N0
    Np1=OBJECTS.N1
    Np2=OBJECTS.N2
    if(Np!=Np2*Np1*Np0):FatalError("Dimension of OBJECTS should match the number of particles in P.")
    N1=N1 or N0
    N2=N2 or N0
    L0=OBJECTS.L0
    L1=OBJECTS.L1
    L2=OBJECTS.L2
    corner0=OBJECTS.corner0
    corner1=OBJECTS.corner1
    corner2=OBJECTS.corner2
    time=OBJECTS.time
    VOIDS = np.zeros(N0*N1*N2, dtype=c_float_t)
    SHEETS = np.zeros(N0*N1*N2, dtype=c_float_t)
    FILAMENTS = np.zeros(N0*N1*N2, dtype=c_float_t)
    CLUSTERS = np.zeros(N0*N1*N2, dtype=c_float_t)
    libSBMY.lagrangian_transport_structures.argtypes = [Snapshot, POINTER(c_float_t), c_int, c_int, c_int, c_int, c_int, c_int, POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t)]
    libSBMY.lagrangian_transport_structures.restype = None
    libSBMY.lagrangian_transport_structures(P, OBJECTS.data.ctypes.data_as(POINTER(c_float_t)), Np0, Np1, Np2, N0, N1, N2, VOIDS.ctypes.data_as(POINTER(c_float_t)), SHEETS.ctypes.data_as(POINTER(c_float_t)), FILAMENTS.ctypes.data_as(POINTER(c_float_t)), CLUSTERS.ctypes.data_as(POINTER(c_float_t)))
    F_VOIDS=Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,VOIDS.reshape(N0,N1,N2))
    F_SHEETS=Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,SHEETS.reshape(N0,N1,N2))
    F_FILAMENTS=Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,FILAMENTS.reshape(N0,N1,N2))
    F_CLUSTERS=Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,CLUSTERS.reshape(N0,N1,N2))
    return F_VOIDS, F_SHEETS, F_FILAMENTS, F_CLUSTERS

def lagrangian_transport_streams_density_velocity(P, N0, N1=None, N2=None, corner0=0., corner1=0., corner2=0.):
    """Lagrangian transport of streams, density and velocities

    Parameters
    ----------
    P (Snapshot) : input snapshot
    N0 (int) : mesh size of the output Eulerian grid (x direction or all directions)
    N1 (optional, int, default=N0) : mesh size of the output Eulerian grid (y direction)
    N2 (optional, int, default=N0) : mesh size of the output Eulerian grid (z direction)
    corner0 (optional, double, default=0.) : x-position of the corner of the box with respect to the observer (which is at (0,0,0))
    corner1 (optional, double, default=0.) : y-position of the corner of the box with respect to the observer (which is at (0,0,0))
    corner2 (optional, double, default=0.) : z-position of the corner of the box with respect to the observer (which is at (0,0,0))

    Returns
    -------
    DENSITYMASSTRACERS (Field) : output density contrast from the mass tracers (scalar field, Eulerian grid)
    PRIMORDIALSTREAMDENSITY (Field) : output primordial stream density (scalar field, Lagrangian grid)
    SECONDARYSTREAMDENSITY (Field) : output secondary stream density (scalar field, Eulerian grid)
    DENSITYTETRAHEDRA (Field) : output density from projecting all tetrahedra (scalar field, Eulerian grid)
    VELOCITY (Field) : output velocity field (vector field, Eulerian grid)

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, POINTER
    from pysbmy.field import Field
    from pysbmy.snapshot import Snapshot
    Np0=P.Np0
    Np1=P.Np1
    Np2=P.Np2
    N1=N1 or N0
    N2=N2 or N0
    L0=P.L0
    L1=P.L1
    L2=P.L2
    time=P.time
    DENSITYMASSTRACERS = np.zeros(N0*N1*N2, dtype=c_float_t)
    PRIMORDIALSTREAMDENSITY = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    SECONDARYSTREAMDENSITY = np.zeros(N0*N1*N2, dtype=c_float_t)
    DENSITYTETRAHEDRA = np.zeros(N0*N1*N2, dtype=c_float_t)
    VELOCITY_X = np.zeros(N0*N1*N2, dtype=c_float_t)
    VELOCITY_Y = np.zeros(N0*N1*N2, dtype=c_float_t)
    VELOCITY_Z = np.zeros(N0*N1*N2, dtype=c_float_t)
    libSBMY.lagrangian_transport_streams_density_velocity.argtypes = [Snapshot, c_int, c_int, c_int, c_int, c_int, c_int, POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t)]
    libSBMY.lagrangian_transport_streams_density_velocity.restype = None
    libSBMY.lagrangian_transport_streams_density_velocity(P, Np0, Np1, Np2, N0, N1, N2, DENSITYMASSTRACERS.ctypes.data_as(POINTER(c_float_t)), PRIMORDIALSTREAMDENSITY.ctypes.data_as(POINTER(c_float_t)), SECONDARYSTREAMDENSITY.ctypes.data_as(POINTER(c_float_t)), DENSITYTETRAHEDRA.ctypes.data_as(POINTER(c_float_t)), VELOCITY_X.ctypes.data_as(POINTER(c_float_t)), VELOCITY_Y.ctypes.data_as(POINTER(c_float_t)), VELOCITY_Z.ctypes.data_as(POINTER(c_float_t)))
    F_DENSITYMASSTRACERS=Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,DENSITYMASSTRACERS.reshape(N0,N1,N2))
    F_PRIMORDIALSTREAMDENSITY=Field(L0,L1,L2,corner0,corner1,corner2,1,Np0,Np1,Np2,0.001,PRIMORDIALSTREAMDENSITY.reshape(Np2,Np1,Np0))
    F_SECONDARYSTREAMDENSITY=Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,SECONDARYSTREAMDENSITY.reshape(N0,N1,N2))
    F_DENSITYTETRAHEDRA=Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,DENSITYTETRAHEDRA.reshape(N0,N1,N2))
    F_VELOCITY=Field(L0,L1,L2,corner0,corner1,corner2,3,N0,N1,N2,time,np.array((VELOCITY_X.reshape(N0,N1,N2),VELOCITY_Y.reshape(N0,N1,N2),VELOCITY_Z.reshape(N0,N1,N2))))
    return F_DENSITYMASSTRACERS, F_PRIMORDIALSTREAMDENSITY, F_SECONDARYSTREAMDENSITY, F_DENSITYTETRAHEDRA, F_VELOCITY
