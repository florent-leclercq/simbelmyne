#!/usr/bin/env python
# -*- coding: latin-1 -*-
#-------------------------------------------------------------------------------------
# Simbelmynë v0.5.0 -- pysbmy/classifiers.py
# Copyright (C) 2012-2023 Florent Leclercq.
#
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# The text of the license is located in the root directory of the source package.
#-------------------------------------------------------------------------------------

"""Routines for cosmic web classifications
"""

__author__  = "Florent Leclercq"
__version__ = "0.5.0"
__date__    = "2014-2023"
__license__ = "GPLv3"

def structures_l2e(OBJECTS, P, N0, N1=None, N2=None):
    """Tranports Lagrangian structures (DIVA or ORIGAMI) to Eulerian coordinates

    Parameters
    ----------
    OBJECTS (Field) : input objects on the Lagrangian mesh
    P (Snapshot) : input snapshot to get particles' displacements
    N0 (int) : mesh size of the desired Eulerian grid (x direction or all directions)
    N1 (optional, int, default=N0) : mesh size of the desired Eulerian grid (y direction)
    N2 (optional, int, default=N0) : mesh size of the desired Eulerian grid (z direction)

    Returns
    -------
    VOIDS (Field) : void detection probability on the Eulerian grid
    SHEETS (Field) : sheet detection probability on the Eulerian grid
    FILAMENTS (Field) : filament detection probability on the Eulerian grid
    CLUSTERS (Field) : cluster detection probability on the Eulerian grid

    """
    import numpy as np
    from pysbmy import libSBMY, c_int, c_float_t, POINTER
    from pysbmy.field import Field
    from pysbmy.snapshot import Snapshot
    from pysbmy.utils import FatalError
    Np=P.Np
    Np0=OBJECTS.N0
    Np1=OBJECTS.N1
    Np2=OBJECTS.N2
    if(Np!=Np2*Np1*Np0):FatalError("Dimension of OBJECTS should match the number of particles in P.")
    N1=N1 or N0
    N2=N2 or N0
    L0=OBJECTS.L0
    L1=OBJECTS.L1
    L2=OBJECTS.L2
    corner0=OBJECTS.corner0
    corner1=OBJECTS.corner1
    corner2=OBJECTS.corner2
    time=OBJECTS.time
    VOIDS = np.zeros(N2*N1*N0, dtype=c_float_t)
    SHEETS = np.zeros(N2*N1*N0, dtype=c_float_t)
    FILAMENTS = np.zeros(N2*N1*N0, dtype=c_float_t)
    CLUSTERS = np.zeros(N2*N1*N0, dtype=c_float_t)
    libSBMY.structures_l2e.argtypes = [POINTER(c_float_t), Snapshot, POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), c_int, c_int, c_int, c_int]
    libSBMY.structures_l2e.restype = None
    libSBMY.structures_l2e(OBJECTS.data.reshape(Np).ctypes.data_as(POINTER(c_float_t)), P, VOIDS.ctypes.data_as(POINTER(c_float_t)), SHEETS.ctypes.data_as(POINTER(c_float_t)), FILAMENTS.ctypes.data_as(POINTER(c_float_t)), CLUSTERS.ctypes.data_as(POINTER(c_float_t)), Np, N0, N1, N2)
    F_VOIDS=Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,VOIDS.reshape(N0,N1,N2))
    F_SHEETS=Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,SHEETS.reshape(N0,N1,N2))
    F_FILAMENTS=Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,FILAMENTS.reshape(N0,N1,N2))
    F_CLUSTERS=Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,CLUSTERS.reshape(N0,N1,N2))
    return F_VOIDS, F_SHEETS, F_FILAMENTS, F_CLUSTERS

def diva_analysis(PSI):
    """Analyzes a Lagrangian displacement field with the DIVA algorithm

    Parameters
    ----------
    PSI (Field) : input Lagrangian displacement field

    Returns
    -------
    OBJECTS (Field) : DIVA classification of particles on the Lagrangian mesh
    LAMBDA1 (Field) : First eigenvalue of the shear of the Lagrangian diplacement field
    LAMBDA2 (Field) : Second eigenvalue of the shear of the Lagrangian diplacement field
    LAMBDA3 (Field) : Third eigenvalue of the shear of the Lagrangian diplacement field

    """
    import numpy as np
    from pysbmy import libSBMY, c_int, c_double, c_float_t, POINTER
    from pysbmy.field import Field
    from pysbmy.utils import FatalError
    if not PSI.rank==3:FatalError("Input field should be a vector.")
    Np0=PSI.N0
    Np1=PSI.N1
    Np2=PSI.N2
    L0=PSI.L0
    L1=PSI.L1
    L2=PSI.L2
    corner0=PSI.corner0
    corner1=PSI.corner1
    corner2=PSI.corner2
    time=PSI.time
    OBJECTS = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    LAMBDA1 = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    LAMBDA2 = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    LAMBDA3 = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    libSBMY.diva_analysis.argtypes = [POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), c_int, c_int, c_int, c_double, c_double, c_double]
    libSBMY.diva_analysis.restype = None
    libSBMY.diva_analysis(PSI.data[0].ctypes.data_as(POINTER(c_float_t)), PSI.data[1].ctypes.data_as(POINTER(c_float_t)), PSI.data[2].ctypes.data_as(POINTER(c_float_t)), OBJECTS.ctypes.data_as(POINTER(c_float_t)), LAMBDA1.ctypes.data_as(POINTER(c_float_t)), LAMBDA2.ctypes.data_as(POINTER(c_float_t)), LAMBDA3.ctypes.data_as(POINTER(c_float_t)), Np0, Np1, Np2, L0, L1, L2)
    F_OBJECTS = Field(L0,L1,L2,corner0,corner1,corner2,1,Np0,Np1,Np2,time,OBJECTS.reshape(Np2,Np1,Np0))
    F_LAMBDA1 = Field(L0,L1,L2,corner0,corner1,corner2,1,Np0,Np1,Np2,time,LAMBDA1.reshape(Np2,Np1,Np0))
    F_LAMBDA2 = Field(L0,L1,L2,corner0,corner1,corner2,1,Np0,Np1,Np2,time,LAMBDA2.reshape(Np2,Np1,Np0))
    F_LAMBDA3 = Field(L0,L1,L2,corner0,corner1,corner2,1,Np0,Np1,Np2,time,LAMBDA3.reshape(Np2,Np1,Np0))
    return F_OBJECTS, F_LAMBDA1, F_LAMBDA2, F_LAMBDA3

def lich_analysis(PSI):
    """Analyzes a Lagrangian displacement field with the LICH algorithm

    Parameters
    ----------
    PSI (Field) : input Lagrangian displacement field

    Returns
    -------
    OBJECTS (Field) : LICH classification of particles on the Lagrangian mesh
    S1 (Field) : First Lagrangian invariant on the Lagrangian mesh
    S2 (Field) : Second Lagrangian invariant on the Lagrangian mesh
    S3 (Field) : Third Lagrangian invariant on the Lagrangian mesh

    """
    import numpy as np
    from pysbmy import libSBMY, c_int, c_double, c_float_t, POINTER
    from pysbmy.field import Field
    from pysbmy.utils import FatalError
    if not PSI.rank==3:FatalError("Input field should be a vector.")
    Np0=PSI.N0
    Np1=PSI.N1
    Np2=PSI.N2
    L0=PSI.L0
    L1=PSI.L1
    L2=PSI.L2
    corner0=PSI.corner0
    corner1=PSI.corner1
    corner2=PSI.corner2
    time=PSI.time
    OBJECTS = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    S1 = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    S2 = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    S3 = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    libSBMY.lich_analysis.argtypes = [POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), c_int, c_int, c_int, c_double, c_double, c_double]
    libSBMY.lich_analysis.restype = None
    libSBMY.lich_analysis(PSI.data[0].ctypes.data_as(POINTER(c_float_t)), PSI.data[1].ctypes.data_as(POINTER(c_float_t)), PSI.data[2].ctypes.data_as(POINTER(c_float_t)), OBJECTS.ctypes.data_as(POINTER(c_float_t)), S1.ctypes.data_as(POINTER(c_float_t)), S2.ctypes.data_as(POINTER(c_float_t)), S3.ctypes.data_as(POINTER(c_float_t)), Np0, Np1, Np2, L0, L1, L2)
    F_OBJECTS = Field(L0,L1,L2,corner0,corner1,corner2,1,Np0,Np1,Np2,time,OBJECTS.reshape(Np2,Np1,Np0))
    F_S1 = Field(L0,L1,L2,corner0,corner1,corner2,1,Np0,Np1,Np2,time,S1.reshape(Np2,Np1,Np0))
    F_S2 = Field(L0,L1,L2,corner0,corner1,corner2,1,Np0,Np1,Np2,time,S2.reshape(Np2,Np1,Np0))
    F_S3 = Field(L0,L1,L2,corner0,corner1,corner2,1,Np0,Np1,Np2,time,S3.reshape(Np2,Np1,Np0))
    return F_OBJECTS, F_S1, F_S2, F_S3

def lich_structures_l2e(OBJECTS, P, N0, N1=None, N2=None):
    """Tranports Lagrangian LICH structures to Eulerian coordinates

    Parameters
    ----------
    OBJECTS (Field) : input objects on the Lagrangian mesh
    P (Snapshot) : input snapshot to get particles' displacements
    N0 (int) : mesh size of the desired Eulerian grid (x direction or all directions)
    N1 (optional, int, default=N0) : mesh size of the desired Eulerian grid (y direction)
    N2 (optional, int, default=N0) : mesh size of the desired Eulerian grid (z direction)

    Returns
    -------
    POTENTIAL_VOIDS (Field) : potential void detection probability on the Eulerian grid
    POTENTIAL_SHEETS (Field) : potential sheet detection probability on the Eulerian grid
    POTENTIAL_FILAMENTS (Field) : potential filament detection probability on the Eulerian grid
    POTENTIAL_CLUSTERS (Field) : potential cluster detection probability on the Eulerian grids
    VORTICAL_VOIDS (Field) : vortical void detection probability on the Eulerian grid
    VORTICAL_SHEETS (Field) : vortical sheet detection probability on the Eulerian grid
    VORTICAL_FILAMENTS (Field) : vortical filament detection probability on the Eulerian grid
    VORTICAL_CLUSTERS (Field) : vortical cluster detection probability on the Eulerian grid

    """
    import numpy as np
    from pysbmy import libSBMY, c_int, c_float_t, POINTER
    from pysbmy.field import Field
    from pysbmy.snapshot import Snapshot
    from pysbmy.utils import FatalError
    Np=P.Np
    Np0=OBJECTS.N0
    Np1=OBJECTS.N1
    Np2=OBJECTS.N2
    if(Np!=Np2*Np1*Np0):FatalError("Dimension of OBJECTS should match the number of particles in P.")
    N1=N1 or N0
    N2=N2 or N0
    L0=OBJECTS.L0
    L1=OBJECTS.L1
    L2=OBJECTS.L2
    corner0=OBJECTS.corner0
    corner1=OBJECTS.corner1
    corner2=OBJECTS.corner2
    time=OBJECTS.time
    POTENTIAL_VOIDS = np.zeros(N2*N1*N0, dtype=c_float_t)
    POTENTIAL_SHEETS = np.zeros(N2*N1*N0, dtype=c_float_t)
    POTENTIAL_FILAMENTS = np.zeros(N2*N1*N0, dtype=c_float_t)
    POTENTIAL_CLUSTERS = np.zeros(N2*N1*N0, dtype=c_float_t)
    VORTICAL_VOIDS = np.zeros(N2*N1*N0, dtype=c_float_t)
    VORTICAL_SHEETS = np.zeros(N2*N1*N0, dtype=c_float_t)
    VORTICAL_FILAMENTS = np.zeros(N2*N1*N0, dtype=c_float_t)
    VORTICAL_CLUSTERS = np.zeros(N2*N1*N0, dtype=c_float_t)
    libSBMY.lich_structures_l2e.argtypes = [POINTER(c_float_t), Snapshot, POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), c_int, c_int, c_int, c_int]
    libSBMY.lich_structures_l2e.restype = None
    libSBMY.lich_structures_l2e(OBJECTS.data.reshape(Np).ctypes.data_as(POINTER(c_float_t)), P, POTENTIAL_VOIDS.ctypes.data_as(POINTER(c_float_t)), POTENTIAL_SHEETS.ctypes.data_as(POINTER(c_float_t)), POTENTIAL_FILAMENTS.ctypes.data_as(POINTER(c_float_t)), POTENTIAL_CLUSTERS.ctypes.data_as(POINTER(c_float_t)), VORTICAL_VOIDS.ctypes.data_as(POINTER(c_float_t)), VORTICAL_SHEETS.ctypes.data_as(POINTER(c_float_t)), VORTICAL_FILAMENTS.ctypes.data_as(POINTER(c_float_t)), VORTICAL_CLUSTERS.ctypes.data_as(POINTER(c_float_t)), Np, N0, N1, N2)
    F_POTENTIAL_VOIDS=Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,POTENTIAL_VOIDS.reshape(N0,N1,N2))
    F_POTENTIAL_SHEETS=Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,POTENTIAL_SHEETS.reshape(N0,N1,N2))
    F_POTENTIAL_FILAMENTS=Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,POTENTIAL_FILAMENTS.reshape(N0,N1,N2))
    F_POTENTIAL_CLUSTERS=Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,POTENTIAL_CLUSTERS.reshape(N0,N1,N2))
    F_VORTICAL_VOIDS=Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,VORTICAL_VOIDS.reshape(N0,N1,N2))
    F_VORTICAL_SHEETS=Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,VORTICAL_SHEETS.reshape(N0,N1,N2))
    F_VORTICAL_FILAMENTS=Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,VORTICAL_FILAMENTS.reshape(N0,N1,N2))
    F_VORTICAL_CLUSTERS=Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,VORTICAL_CLUSTERS.reshape(N0,N1,N2))
    return F_POTENTIAL_VOIDS, F_POTENTIAL_SHEETS, F_POTENTIAL_FILAMENTS, F_POTENTIAL_CLUSTERS, F_VORTICAL_VOIDS, F_VORTICAL_SHEETS, F_VORTICAL_FILAMENTS, F_VORTICAL_CLUSTERS

def origami_analysis(P, corner0=0., corner1=0., corner2=0.):
    """Analyzes a snapshot with the ORIGAMI algorithm

    Parameters
    ----------
    P (Snapshot) : input snapshot to get particles' displacements
    corner0 (optional, double, default=0.) : x-position of the corner of the box with respect to the observer (which is at (0,0,0))
    corner1 (optional, double, default=0.) : y-position of the corner of the box with respect to the observer (which is at (0,0,0))
    corner2 (optional, double, default=0.) : z-position of the corner of the box with respect to the observer (which is at (0,0,0))

    Returns
    -------
    OBJECTS (Field) : ORIGAMI classification of particles on the Lagrangian mesh

    """
    import numpy as np
    from pysbmy import libSBMY, c_int, c_double, c_float_t, POINTER
    from pysbmy.field import Field
    from pysbmy.snapshot import Snapshot
    from pysbmy.utils import FatalError
    Np0=P.Np0
    Np1=P.Np1
    Np2=P.Np2
    L0=P.L0
    L1=P.L1
    L2=P.L2
    time=P.time
    OBJECTS = np.zeros(Np2*Np1*Np0, dtype=c_float_t)
    libSBMY.origami_analysis.argtypes = [Snapshot, POINTER(c_float_t), c_int, c_int, c_int, c_double, c_double, c_double]
    libSBMY.origami_analysis.restype = None
    libSBMY.origami_analysis(P, OBJECTS.ctypes.data_as(POINTER(c_float_t)), Np0, Np1, Np2, L0, L1, L2)
    return Field(L0,L1,L2,corner0,corner1,corner2,1,Np0,Np1,Np2,time,OBJECTS.reshape(Np2,Np1,Np0))

def tweb_analysis(DENSITY):
    """Analyzes a density contrast field with the T-web algorithm

    Parameters
    ----------
    DENSITY (Field) : input density contrast field on the Eulerian grid

    Returns
    -------
    OBJECTS (Field) : T-web classification of the Eulerian grid
    LAMBDA1 (Field) : First eigenvalue of the tidal tensor
    LAMBDA2 (Field) : Second eigenvalue of the tidal tensor
    LAMBDA3 (Field) : Third eigenvalue of the tidal tensor

    """
    import numpy as np
    from pysbmy import libSBMY, c_int, c_double, c_float_t, POINTER
    from pysbmy.field import Field
    from pysbmy.utils import FatalError
    N0=DENSITY.N0
    N1=DENSITY.N1
    N2=DENSITY.N2
    L0=DENSITY.L0
    L1=DENSITY.L1
    L2=DENSITY.L2
    corner0=DENSITY.corner0
    corner1=DENSITY.corner1
    corner2=DENSITY.corner2
    time=DENSITY.time
    OBJECTS = np.zeros(N2*N1*N0, dtype=c_float_t)
    LAMBDA1 = np.zeros(N2*N1*N0, dtype=c_float_t)
    LAMBDA2 = np.zeros(N2*N1*N0, dtype=c_float_t)
    LAMBDA3 = np.zeros(N2*N1*N0, dtype=c_float_t)
    libSBMY.tweb_analysis.argtypes = [POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), POINTER(c_float_t), c_int, c_int, c_int, c_double, c_double, c_double]
    libSBMY.tweb_analysis.restype = None
    libSBMY.tweb_analysis(DENSITY.data.ctypes.data_as(POINTER(c_float_t)), OBJECTS.ctypes.data_as(POINTER(c_float_t)), LAMBDA1.ctypes.data_as(POINTER(c_float_t)), LAMBDA2.ctypes.data_as(POINTER(c_float_t)), LAMBDA3.ctypes.data_as(POINTER(c_float_t)), N0, N1, N2, L0, L1, L2)
    F_OBJECTS = Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,OBJECTS.reshape(N0,N1,N2))
    F_LAMBDA1 = Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,LAMBDA1.reshape(N0,N1,N2))
    F_LAMBDA2 = Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,LAMBDA2.reshape(N0,N1,N2))
    F_LAMBDA3 = Field(L0,L1,L2,corner0,corner1,corner2,1,N0,N1,N2,time,LAMBDA3.reshape(N0,N1,N2))
    return F_OBJECTS, F_LAMBDA1, F_LAMBDA2, F_LAMBDA3
