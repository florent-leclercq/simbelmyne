#!/usr/bin/env python
# -*- coding: latin-1 -*-
#-------------------------------------------------------------------------------------
# Simbelmynë v0.5.0 -- pysbmy/power.py
# Copyright (C) 2012-2023 Florent Leclercq.
#
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# The text of the license is located in the root directory of the source package.
#-------------------------------------------------------------------------------------

"""Routines for computing the power spectrum
"""

__author__  = "Florent Leclercq"
__version__ = "0.5.0"
__date__    = "2012-2023"
__license__ = "GPLv3"

from pysbmy.fft import FourierGrid

class PowerSpectrum(FourierGrid):
    """Superclass of FourierGrid containing additionally:
        cosmo (dictionary) : cosmological parameters (and some infrastructure parameters)
        powerspectrum (array, double, dimension=NUM_MODES) : values of the power spectrum at the given k modes
    """

    # Initialization
    cosmo_default={'h':0.6774, 'Omega_r':0., 'Omega_q':0.6911, 'Omega_b':0.0486, 'Omega_m':0.3089, 'm_ncdm':0., 'Omega_k':0., 'tau_reio':0.066, 'n_s':0.9667, 'sigma8':0.8159, 'w0_fld':-1., 'wa_fld':0., 'k_max':10.0, 'WhichSpectrum':1} # Default values: Planck 2015 cosmological parameters (Planck 2015 XIII, p31, table 4, last column)

    def __init__(self,L0,L1,L2,N0,N1,N2,cosmo=cosmo_default,N2_HC=None,N_HC=None,NUM_MODES=None,kmax=1e6,k_keys=None,k_modes=None,k_nmodes=None,trim_bins=True,trim_threshold=0,powerspectrum=None):
        """Initializes a PowerSpectrum object

        Parameters
        ----------
        L0 (double) : box size x
        L1 (double) : box size y
        L2 (double) : box size z
        N0 (int) : mesh size x
        N1 (int) : mesh size y
        N2 (int) : mesh size z
        cosmo (optional, dictionary, default=cosmo_default (Planck 2015 values)) : cosmological parameters (and some infrastructure parameters)
        N2_HC (optional, int, default=N2//2+1 : mesh size z of the Fourier grid
        N_HC (optional, int, default=N0*N1*N2_HC) : total number of points in the Fourier grid
        NUM_MODES (optional, int, default=0) : number of k modes in the Fourier grid
        kmax (optional, double, default=1e6) : maximal value of k in the Fourier grid object
        k_keys (optional, array, int, dimensions=(N0,N1,N2_HC), computed if not specified) : the keys to be used as a function of the Fourier mode (k_keys[i][j][k]=b)
        k_modes (optional, array, float_t, dimension=NUM_MODES, computed if not specified) : value of the wavenumber for this key (k[b])
        k_nmodes (optional, array, int, dimension=NUM_MODES, computed if not specified) : number of modes for this key (N_k[b])
        trim_bins (optional, boolean, default=True) : trim bins in the input k_modes for the final FourierGrid object if they have less than trim_threshold modes?
        trim_threshold (optional, int, default=0) : minimun number of k modes in a bin
        powerspectrum (optional, array, double, dimension=NUM_MODES, default=None) : values of the power spectrum at the given k modes. Computed if not specified

        Returns
        -------
        Pk (PowerSpectrum)

        """
        super(PowerSpectrum, self).__init__(L0,L1,L2,N0,N1,N2,N2_HC,N_HC,NUM_MODES,kmax,k_keys,k_modes,k_nmodes,trim_bins,trim_threshold)
        if powerspectrum is not None:
            from pysbmy import c_float_t
            self.powerspectrum=powerspectrum.astype(c_float_t)
        else:
            self.setup_power_spectrum(cosmo=cosmo)

    @classmethod
    def from_FourierGrid(cls,G,cosmo=cosmo_default,powerspectrum=None):
        """Initializes a PowerSpectrum object from a FourierGrid object

        Parameters
        ----------
        G (FourierGrid) : input Fourier grid
        cosmo (optional, dictionary, default=cosmo_default (Planck 2015 values)) : cosmological parameters (and some infrastructure parameters)
        powerspectrum (optional, array, double, dimension=G.NUM_MODES, default=None) : values of the power spectrum at the given k modes. Computed if not specified

        Returns
        -------
        Pk (PowerSpectrum)

        """
        return cls(G.L0,G.L1,G.L2,G.N0,G.N1,G.N2,cosmo,G.N2_HC,G.N_HC,G.NUM_MODES,G.kmax,G.k_keys,G.k_modes,G.k_nmodes,powerspectrum=powerspectrum)

    # Representation
    def __repr__(self):
        ans=super(PowerSpectrum, self).__repr__()
        ans+="powerspectrum=np.array({}, dtype={})".format(self.powerspectrum.shape,self.powerspectrum.dtype)
        return ans

    # Properties
    @property
    def FourierGrid(self):
        return FourierGrid(self.L0,self.L1,self.L2,self.N0,self.N1,self.N2,self.N2_HC,self.N_HC,self.NUM_MODES,self.kmax,self.k_keys,self.k_modes,self.k_nmodes)

    # Equality
    def __eq__(self,other):
        import numpy as np
        return super(PowerSpectrum, self).__eq__(other) and np.array_equal(self.powerspectrum,other.powerspectrum)

    # Setup power spectrum
    @staticmethod
    def get_Pk_fitting(k_modes,WhichSpectrum,cosmo):
        import numpy as np
        from pysbmy import c_double
        from pysbmy.cosmology import cosmo_dict_to_array

        def setup_Pk(k_modes, Nk, params, WhichSpectrum=1):
            import numpy as np
            from pysbmy import libSBMY, c_float_t, c_int, c_double, POINTER
            k_modes = k_modes.astype(c_float_t)
            params = params.astype(c_double)
            powerspectrum = np.zeros(Nk, dtype=c_float_t)
            libSBMY.setup_Pk.argtypes = [c_int, POINTER(c_float_t), POINTER(c_double), c_int, POINTER(c_float_t)]
            libSBMY.setup_Pk.restype = None
            libSBMY.setup_Pk(Nk, k_modes.ctypes.data_as(POINTER(c_float_t)), params.ctypes.data_as(POINTER(c_double)), WhichSpectrum, powerspectrum.ctypes.data_as(POINTER(c_float_t)))
            return powerspectrum

        params=cosmo_dict_to_array(cosmo)
        rsmooth=8                       # 8 Mpc/h
        params=np.array(np.append(params, rsmooth), dtype=c_double)

        Pk_z0=setup_Pk(k_modes, len(k_modes), params, WhichSpectrum)
        return Pk_z0

    @staticmethod
    def get_Pk_PL(k_modes,cosmo):
        """Computes the power spectrum from a power-law spectrum, k**n_S

        Parameters
        ----------
        k_modes (array, double, dimension=NUM_MODES) : specified k modes
        cosmo (dictionary) : cosmological parameters (and some infrastructure parameters)

        Returns
        -------
        Pk (array, double, dimension=NUM_MODES) : P(k) at specified k modes

        """
        return PowerSpectrum.get_Pk_fitting(k_modes,-1,cosmo)

    @staticmethod
    def get_Pk_BBKS(k_modes,cosmo):
        """Computes the power spectrum from the BBKS fitting function
        (pure cold dark matter)

        Reference: Bardeen et al. 1986

        Parameters
        ----------
        k_modes (array, double, dimension=NUM_MODES) : specified k modes
        cosmo (dictionary) : cosmological parameters (and some infrastructure parameters)

        Returns
        -------
        Pk (array, double, dimension=NUM_MODES) : P(k) at specified k modes

        """
        return PowerSpectrum.get_Pk_fitting(k_modes,0,cosmo)

    @staticmethod
    def get_Pk_EH(k_modes,cosmo):
        """Computes the power spectrum from the EH fitting function
        (cold dark matter including baryonic acoustic oscillations)

        Reference: Eisenstein & Hu 1998, routine courtesy of W. Percival

        Parameters
        ----------
        k_modes (array, double, dimension=NUM_MODES) : specified k modes
        cosmo (dictionary) : cosmological parameters (and some infrastructure parameters)

        Returns
        -------
        Pk (array, double, dimension=NUM_MODES) : P(k) at specified k modes

        """
        return PowerSpectrum.get_Pk_fitting(k_modes,1,cosmo)

    @staticmethod
    def get_Pk_CLASS(k_modes,cosmo):
        """Computes the power spectrum from the CLASS Boltzmann solver
        (the python classy module must be installed)

        Parameters
        ----------
        k_modes (array, double, dimension=NUM_MODES) : specified k modes
        cosmo (dictionary) : cosmological parameters (and some infrastructure parameters)

        Returns
        -------
        Pk (array, double, dimension=NUM_MODES) : P(k) at specified k modes

        """
        from pysbmy import module_exists
        if module_exists("classy"):
            import numpy as np
            from classy import Class
            from pysbmy import c_float_t
            from pysbmy.cosmology import get_CLASS_params

            # Prepare CLASS inputs
            params = get_CLASS_params(cosmo)

            # Call CLASS
            ClassObj = Class()
            ClassObj.set(params)
            ClassObj.compute()

            # Unit conversion Mpc->Mpc/h
            Pkfac = cosmo['h']*cosmo['h']*cosmo['h']
            k_modes_invMpc=cosmo['k_modes_invMpc'] if 'k_modes_invMpc' in cosmo else False
            if k_modes_invMpc:
                kfac = 1.               # k modes given in 1/Mpc
            else:
                kfac = cosmo['h']       # k modes given in h/Mpc, converting to 1/Mpc

            # Setup power spectrum
            Pk_z0 = np.array([ClassObj.pk(kk,0.)*Pkfac for kk in k_modes*kfac], dtype=c_float_t)

            # Clean-up CLASS
            ClassObj.struct_cleanup()

            return Pk_z0
        else:
            from pysbmy.utils import FatalError
            FatalError("Module classy not found.")

    @staticmethod
    def get_Pk(k_modes,cosmo):
        """Computes the power spectrum using the prescription cosmo['WhichSpectrum']

        Parameters
        ----------
        k_modes (array, double, dimension=NUM_MODES) : specified k modes
        cosmo (dictionary) : cosmological parameters (and some infrastructure parameters)

        Returns
        -------
        Pk (array, double, dimension=NUM_MODES) : P(k) at specified k modes

        """
        import numpy as np
        from pysbmy.cosmology import WhichSpectrum_from_cosmo_dict
        k_modes=np.atleast_1d(k_modes)
        WhichSpectrum=WhichSpectrum_from_cosmo_dict(cosmo)
        if WhichSpectrum==-1:
            return PowerSpectrum.get_Pk_PL(k_modes,cosmo)
        elif WhichSpectrum==0:
            return PowerSpectrum.get_Pk_BBKS(k_modes,cosmo)
        elif WhichSpectrum==1:
            return PowerSpectrum.get_Pk_EH(k_modes,cosmo)
        elif WhichSpectrum==2:
            return PowerSpectrum.get_Pk_CLASS(k_modes,cosmo)

    def setup_Pk_fitting(self,WhichSpectrum,cosmo):
        Pk_z0=PowerSpectrum.get_Pk_fitting(self.k_modes,WhichSpectrum,cosmo)
        self.powerspectrum=Pk_z0

    def setup_Pk_PL(self,cosmo):
        """Computes the power spectrum from a power-law spectrum, k**n_S

        Parameters
        ----------
        cosmo (dictionary) : cosmological parameters (and some infrastructure parameters)

        """
        self.setup_Pk_fitting(-1,cosmo)


    def setup_Pk_BBKS(self,cosmo):
        """Computes the power spectrum from the BBKS fitting function
        (pure cold dark matter)

        Parameters
        ----------
        cosmo (dictionary) : cosmological parameters (and some infrastructure parameters)

        """
        self.setup_Pk_fitting(0,cosmo)

    def setup_Pk_EH(self,cosmo):
        """Computes the power spectrum from the EH fitting function
        (cold dark matter including baryonic acoustic oscillations)

        Reference: Eisenstein & Hu 1998, routine courtesy of W. Percival

        Parameters
        ----------
        cosmo (dictionary) : cosmological parameters (and some infrastructure parameters)

        """
        self.setup_Pk_fitting(1,cosmo)

    def setup_Pk_CLASS(self,cosmo):
        """Computes the power spectrum from the CLASS Boltzmann solver
        (the python classy module must be installed)

        Parameters
        ----------
        cosmo (dictionary) : cosmological parameters (and some infrastructure parameters)

        """
        Pk_z0=PowerSpectrum.get_Pk_CLASS(self.k_modes,cosmo)
        self.powerspectrum=Pk_z0

    def setup_power_spectrum(self,cosmo):
        """Computes the power spectrum using the prescription cosmo['WhichSpectrum']

        Parameters
        ----------
        cosmo (dictionary) : cosmological parameters (and some infrastructure parameters)

        """
        from pysbmy.cosmology import WhichSpectrum_from_cosmo_dict
        WhichSpectrum=WhichSpectrum_from_cosmo_dict(cosmo)
        if WhichSpectrum==-1:
            self.setup_Pk_PL(cosmo)
        elif WhichSpectrum==0:
            self.setup_Pk_BBKS(cosmo)
        elif WhichSpectrum==1:
            self.setup_Pk_EH(cosmo)
        elif WhichSpectrum==2:
            self.setup_Pk_CLASS(cosmo)

    # Plotting
    def plot(self):
        """Plots the power spectrum in log-log scale
        """
        import matplotlib.pyplot as plt
        plt.loglog(self.k_modes,self.powerspectrum,linewidth=2)
        plt.show()

    # Writing
    def write(self,fname):
        """Writes a PowerSpectrum object in an external hdf5 file

        Parameters
        ----------
        fname (string) : filename

        """
        import h5py as h5
        from pysbmy import c_float
        from pysbmy.utils import INDENT, UNINDENT, PrintMessage, PrintDiagnostic
        PrintMessage(3, "Write power spectrum in data file '{}'...".format(fname))
        INDENT()
        PrintDiagnostic(4, "L0={}, L1={}, L2={}".format(self.L0,self.L1,self.L2))
        PrintDiagnostic(4, "N0={}, N1={}, N2={}, N2_HC={}, N_HC={}, NUM_MODES={}".format(self.N0,self.N1,self.N2,self.N2_HC,self.N_HC,self.NUM_MODES))

        super(PowerSpectrum, self).write_FourierGrid_hdf5(fname)
        with h5.File(fname, 'a') as hf:
            hf.create_dataset('/scalars/powerspectrum', data=self.powerspectrum.astype(c_float), dtype=c_float)

        UNINDENT()
        PrintMessage(3, "Write power spectrum in data file '{}' done.".format(fname))

    # Reading
    @staticmethod
    def read(fname):
        """Reads a PowerSpectrum object in an external hdf5 file

        Parameters
        ----------
        fname (string) : filename

        Returns
        -------
        Pk (PowerSpectrum)

        """
        import h5py as h5
        import numpy as np
        from pysbmy import c_float_t, c_float
        from pysbmy.utils import INDENT, UNINDENT, PrintMessage, PrintDiagnostic
        PrintMessage(3, "Read power spectrum in data file '{}'...".format(fname))
        INDENT()

        kg = super(PowerSpectrum, PowerSpectrum).read_FourierGrid_hdf5(fname)
        with h5.File(fname,'r') as hf:
            powerspectrum = np.array(hf.get('scalars/powerspectrum'), dtype=c_float).astype(c_float_t)
        Pk=PowerSpectrum(kg.L0,kg.L1,kg.L2,kg.N0,kg.N1,kg.N2,N2_HC=kg.N2_HC,N_HC=kg.N_HC,NUM_MODES=kg.NUM_MODES,kmax=kg.kmax,k_keys=kg.k_keys,k_modes=kg.k_modes,k_nmodes=kg.k_nmodes,powerspectrum=powerspectrum)

        PrintDiagnostic(4, "L0={}, L1={}, L2={}".format(kg.L0,kg.L1,kg.L2))
        PrintDiagnostic(4, "N0={}, N1={}, N2={}, N2_HC={}, N_HC={}, NUM_MODES={}".format(kg.N0,kg.N1,kg.N2,kg.N2_HC,kg.N_HC,kg.NUM_MODES))
        UNINDENT()
        PrintMessage(3, "Read power spectrum in data file '{}' done.".format(fname))
        return Pk
# end class(PowerSpectrum)

read_powerspectrum=PowerSpectrum.read
get_Pk=PowerSpectrum.get_Pk
get_Pk_BBKS=PowerSpectrum.get_Pk_BBKS
get_Pk_EH=PowerSpectrum.get_Pk_EH
get_Pk_CLASS=PowerSpectrum.get_Pk_CLASS
