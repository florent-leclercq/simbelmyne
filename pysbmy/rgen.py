#!/usr/bin/env python
# -*- coding: latin-1 -*-
#-------------------------------------------------------------------------------------
# Simbelmynë v0.5.0 -- pysbmy/rgen.py
# Copyright (C) 2012-2023 Florent Leclercq.
#
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# The text of the license is located in the root directory of the source package.
#-------------------------------------------------------------------------------------

"""Routines for random number generation
"""

__author__  = "Florent Leclercq"
__version__ = "0.5.0"
__date__    = "2016-2023"
__license__ = "GPLv3"

def seed_table_from_rngstate(InputRngState, length, OutputRngState):
    """Generates a seed table for random number generation in parallel

    Parameters
    ----------
    InputRngState (string) : input state of the random number generator
    length (int) : length of the seed table
    OutputRngState (string) : output state of the random number generator after generating the seed table

    Returns
    -------
    seedtable (array, int, dimension=length) : generated seed table

    """
    from pysbmy import libSBMY, c_int, c_char, c_uint64, POINTER
    from numpy.ctypeslib import ndpointer
    InputRngState = InputRngState.encode('ascii')
    OutputRngState = OutputRngState.encode('ascii')
    libSBMY.seed_table_from_rngstate.argtypes = [POINTER(c_char), c_int, POINTER(c_char)]
    libSBMY.seed_table_from_rngstate.restype = ndpointer(dtype=c_uint64, shape=(length))
    seedtable = libSBMY.seed_table_from_rngstate(InputRngState, length, OutputRngState)
    return seedtable
