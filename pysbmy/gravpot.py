#!/usr/bin/env python
# -*- coding: latin-1 -*-
#-------------------------------------------------------------------------------------
# Simbelmynë v0.5.0 -- pysbmy/gravpot.py
# Copyright (C) 2012-2023 Florent Leclercq.
#
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# The text of the license is located in the root directory of the source package.
#-------------------------------------------------------------------------------------

"""Routines for dealing with the gravitational potential
"""

__author__  = "Florent Leclercq"
__version__ = "0.5.0"
__date__    = "2019-2023"
__license__ = "GPLv3"

def solve_poisson_periodic(density, smooth=False):
    """Solves the Poisson equation on the grid, replaces the input field delta
    The quantity returned is Delta^-1 delta without any physical prefactor!
    This routine assumes periodic boundary conditions
    (see appendix B of Leclercq, PhD thesis, 2015)

    Parameters
    ----------
    density (Field) : input and output field
    smooth (optional, boolean, default=False) : smooth the gravitational potential?

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, c_double, c_bool, POINTER
    Npm0=density.N0
    Npm1=density.N1
    Npm2=density.N2
    L0=density.L0
    L1=density.L1
    L2=density.L2
    libSBMY.solve_poisson_periodic.argtypes = [POINTER(c_float_t), c_int, c_int, c_int, c_double, c_double, c_double, c_bool]
    libSBMY.solve_poisson_periodic.restype = None
    libSBMY.solve_poisson_periodic(density.data.ctypes.data_as(POINTER(c_float_t)), Npm0, Npm1, Npm2, L0, L1, L2, smooth)

def solve_poisson_zerobc(density, smooth=False):
    """Solves the Poisson equation on the grid, replaces the input field delta
    The quantity returned is Delta^-1 delta without any physical prefactor!
    This routine assumes a Dirichlet problem with zero boundary conditions

    Parameters
    ----------
    density (Field) : input and output field
    smooth (optional, boolean, default=False) : smooth the gravitational potential?

    """
    import numpy as np
    from pysbmy import libSBMY, c_float_t, c_int, c_double, c_bool, POINTER
    Npm0=density.N0
    Npm1=density.N1
    Npm2=density.N2
    L0=density.L0
    L1=density.L1
    L2=density.L2
    libSBMY.solve_poisson_zerobc.argtypes = [POINTER(c_float_t), c_int, c_int, c_int, c_double, c_double, c_double, c_bool]
    libSBMY.solve_poisson_zerobc.restype = None
    libSBMY.solve_poisson_zerobc(density.data.ctypes.data_as(POINTER(c_float_t)), Npm0, Npm1, Npm2, L0, L1, L2, smooth)
