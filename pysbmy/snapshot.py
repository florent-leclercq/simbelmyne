#!/usr/bin/env python
# -*- coding: latin-1 -*-
#-------------------------------------------------------------------------------------
# Simbelmynë v0.5.0 -- pysbmy/snapshot.py
# Copyright (C) 2012-2023 Florent Leclercq.
#
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# The text of the license is located in the root directory of the source package.
#-------------------------------------------------------------------------------------

"""Routines for working with Simbelmynë snapshots
"""

__author__  = "Florent Leclercq"
__version__ = "0.5.0"
__date__    = "2013-2023"
__license__ = "GPLv3"

from pysbmy import Structure

class header(Structure):
    """This class contains the header information of a Snapshot
    (see the Gadget manual and structures.h for definitions):
    """

    # Initialization, properties and representation
    from pysbmy import c_int, c_uint, c_double, c_float, c_char
    _fields_ = [
            ("npart", c_uint*6),
            ("mass", c_double*6),
            ("time", c_double),
            ("redshift", c_double),
            ("flag_sfr", c_int),
            ("flag_feedback", c_int),
            ("npartTotal", c_uint*6),
            ("flag_cooling", c_int),
            ("num_files", c_int),
            ("BoxSize", c_double),
            ("Omega0", c_double),
            ("OmegaLambda", c_double),
            ("HubbleParam", c_double),
            ("flag_stellarage", c_int),
            ("flag_metals", c_int),
            ("npartTotalHighWord", c_uint*6),
            ("flag_entropy_instead_u", c_int),
            # Specific to Gadget-3:
            ("flag_doubleprecision", c_int),
            ("flag_ic_info", c_int),
            ("lpt_scalingfactor", c_float),
            ("fill", c_char*18 ),
            ("names", c_char*15*2 )
            ]

    def __repr__(self):
        ans="npart: [{} {} {} {} {} {}]\n".format(self.npart[0],self.npart[1],self.npart[2],self.npart[3],self.npart[4],self.npart[5])
        ans+="mass: [{} {} {} {} {} {}]\n".format(self.mass[0],self.mass[1],self.mass[2],self.mass[3],self.mass[4],self.mass[5])
        ans+="time: {}\n".format(self.time)
        ans+="redshift: {}\n".format(self.redshift)
        ans+="flag_sfr: {}\n".format(self.flag_sfr)
        ans+="flag_feedback: {}\n".format(self.flag_feedback)
        ans+="npartTotal: [{} {} {} {} {} {}]\n".format(int(self.npartTotal[0]),int(self.npartTotal[1]),int(self.npartTotal[2]),int(self.npartTotal[3]),int(self.npartTotal[4]),int(self.npartTotal[5]))
        ans+="flag_cooling: {}\n".format(self.flag_cooling)
        ans+="num_files: {}\n".format(self.num_files)
        ans+="BoxSize: {}\n".format(self.BoxSize)
        ans+="Omega0: {}\n".format(self.Omega0)
        ans+="OmegaLambda: {}\n".format(self.OmegaLambda)
        ans+="HubbleParam: {}\n".format(self.HubbleParam)
        ans+="flag_stellarage: {}\n".format(self.flag_stellarage)
        ans+="flag_metals: {}\n".format(self.flag_metals)
        ans+="npartTotalHighWord: [{} {} {} {} {} {}]\n".format(int(self.npartTotalHighWord[0]),int(self.npartTotalHighWord[1]),int(self.npartTotalHighWord[2]),int(self.npartTotalHighWord[3]),int(self.npartTotalHighWord[4]),int(self.npartTotalHighWord[5]))
        ans+="flag_entropy_instead_u: {}\n".format(self.flag_entropy_instead_u)
        ans+="flag_doubleprecision: {}\n".format(self.flag_doubleprecision)
        ans+="flag_ic_info: {}\n".format(self.flag_ic_info)
        ans+="lpt_scalingfactor: {}".format(self.lpt_scalingfactor)
        return ans

    # Equality
    def __eq__(self,other):
        ans = True
        ans = ans and self.npart[0]==other.npart[0] and self.npart[1]==other.npart[1] and self.npart[2]==other.npart[2] and self.npart[3]==other.npart[3] and self.npart[4]==other.npart[4] and self.npart[5]==other.npart[5]
        ans = ans and self.mass[0]==other.mass[0] and self.mass[1]==other.mass[1] and self.mass[2]==other.mass[2] and self.mass[3]==other.mass[3] and self.mass[4]==other.mass[4] and self.mass[5]==other.mass[5]
        ans = ans and self.time==other.time and self.redshift==other.redshift and self.flag_sfr==other.flag_sfr and self.flag_feedback==other.flag_feedback
        ans = ans and self.npartTotal[0]==other.npartTotal[0] and self.npartTotal[1]==other.npartTotal[1] and self.npartTotal[2]==other.npartTotal[2] and self.npartTotal[3]==other.npartTotal[3] and self.npartTotal[4]==other.npartTotal[4] and self.npartTotal[5]==other.npartTotal[5]
        ans = ans and self.flag_cooling==other.flag_cooling and self.num_files==other.num_files and self.BoxSize==other.BoxSize and self.Omega0==other.Omega0 and self.flag_cooling==other.flag_cooling and self.OmegaLambda==other.OmegaLambda and self.HubbleParam==other.HubbleParam and self.flag_stellarage==other.flag_stellarage and self.flag_metals==other.flag_metals
        ans = ans and self.npartTotalHighWord[0]==other.npartTotalHighWord[0] and self.npartTotalHighWord[1]==other.npartTotalHighWord[1] and self.npartTotalHighWord[2]==other.npartTotalHighWord[2] and self.npartTotalHighWord[3]==other.npartTotalHighWord[3] and self.npartTotalHighWord[4]==other.npartTotalHighWord[4] and self.npartTotalHighWord[5]==other.npartTotalHighWord[5]
        ans = ans and self.flag_entropy_instead_u==other.flag_entropy_instead_u and self.flag_doubleprecision==other.flag_doubleprecision and self.flag_ic_info==other.flag_ic_info and self.lpt_scalingfactor==other.lpt_scalingfactor
        return ans

    # Reading
    @staticmethod
    def read_header_hdf5(fname):
        import h5py as h5
        import numpy as np
        from pysbmy import c_int, c_uint, c_double, c_float
        with h5.File(fname,'r') as hf:
            npart = hf['Header'].attrs['NumPart_ThisFile'].astype(c_uint)
            mass = hf['Header'].attrs['MassTable'].astype(c_double)
            time = hf['Header'].attrs['Time']
            redshift = hf['Header'].attrs['Redshift']
            flag_sfr = hf['Header'].attrs['Flag_Sfr']
            flag_feedback = hf['Header'].attrs['Flag_Feedback']
            npartTotal = hf['Header'].attrs['NumPart_Total'].astype(c_uint)
            flag_cooling = hf['Header'].attrs['Flag_Cooling']
            num_files = hf['Header'].attrs['NumFilesPerSnapshot']
            BoxSize = hf['Header'].attrs['BoxSize']
            Omega0 = hf['Header'].attrs['Omega0']
            OmegaLambda = hf['Header'].attrs['OmegaLambda']
            HubbleParam = hf['Header'].attrs['HubbleParam']
            flag_stellarage = hf['Header'].attrs['Flag_StellarAge']
            flag_metals = hf['Header'].attrs['Flag_Metals']
            npartTotalHighWord = hf['Header'].attrs['NumPart_Total_HighWord'].astype(c_uint)
            flag_entropy_instead_u = 0
            # Specific to Gadget-3:
            try:
                flag_doubleprecision = hf['Header'].attrs['Flag_Double_Precision']
            except KeyError:
                flag_doubleprecision = 0
            try:
                flag_ic_info = hf['Header'].attrs['Flag_IC_Info']
            except KeyError:
                flag_ic_info = 0
            try:
                lpt_scalingfactor = hf['Header'].attrs['LPT_ScalingFactor']
            except KeyError:
                lpt_scalingfactor = 0
        npart = np.ctypeslib.as_ctypes(npart)
        mass = np.ctypeslib.as_ctypes(mass)
        time = c_double(time)
        redshift = c_double(redshift)
        flag_sfr = c_int(flag_sfr)
        flag_feedback = c_int(flag_feedback)
        npartTotal = np.ctypeslib.as_ctypes(npartTotal)
        flag_cooling = c_int(flag_cooling)
        num_files = c_int(num_files)
        BoxSize = c_double(BoxSize)
        Omega0 = c_double(Omega0)
        OmegaLambda = c_double(OmegaLambda)
        HubbleParam = c_double(HubbleParam)
        flag_stellarage = c_int(flag_stellarage)
        flag_metals = c_int(flag_metals)
        npartTotalHighWord = np.ctypeslib.as_ctypes(npartTotalHighWord)
        flag_entropy_instead_u = c_int(flag_entropy_instead_u)
        flag_doubleprecision = c_int(flag_doubleprecision)
        flag_ic_info = c_int(flag_ic_info)
        lpt_scalingfactor = c_float(lpt_scalingfactor)
        return header(npart,mass,time,redshift,flag_sfr,flag_feedback,npartTotal,flag_cooling,num_files,BoxSize,Omega0,OmegaLambda,HubbleParam,flag_stellarage,flag_metals,npartTotalHighWord,flag_entropy_instead_u,flag_doubleprecision,flag_ic_info,lpt_scalingfactor)

    @staticmethod
    def read_header(fname):
        """Reads a header object in an external hdf5 file

        Parameters
        ----------
        fname (string) : filename

        Returns
        -------
        H (header)

        """
        from pysbmy import libSBMY, c_int, c_char, POINTER
        SnapFormat = int(fname[len(fname)-1])
        SnapFile = fname.encode('ascii')
        libSBMY.read_header.argtypes = [POINTER(c_char),c_int]
        libSBMY.read_header.restype = header
        H = libSBMY.read_header(SnapFile, SnapFormat)
        return H
#end class(header)

class particle_type_data(Structure):
    """This class contains the type of a particle
    """

    # Initialization, properties and representation
    from pysbmy import c_short
    _fields_=[("Type", c_short)]

    def __repr__(self):
        return "Type: {}\n".format(self.Type)

    # Equality
    def __eq__(self,other):
        return self.Type==other.Type
#end class(particle_type_data)

class particle_mass_data(Structure):
    """This class contains the mass of a particle
    """

    # Initialization, properties and representation
    from pysbmy import c_float_t
    _fields_=[("Mass", c_float_t)]

    def __repr__(self):
        return "Mass: {}\n".format(self.Mass)

    # Equality
    def __eq__(self,other):
        return self.Mass==other.Mass

    # Reading
    @staticmethod
    def read_particle_mass_data_hdf5(fname,PartIndex,Type=1):
        import h5py as h5
        import numpy as np
        from pysbmy import c_float_t
        with h5.File(fname,'r') as hf:
            Mass=hf['PartType'+str(Type)]['Mass']
        args=[c_float_t(Mass)]
        return particle_pos_data(*args)
#end class(particle_mass_data)

class particle_pos_data(Structure):
    """This class contains the position of a particle
    """

    # Initialization, properties and representation
    from pysbmy import c_float_t
    _fields_=[("Pos", c_float_t*3)]

    def __repr__(self):
        return "Pos: [{} {} {}]\n".format(self.Pos[0], self.Pos[1], self.Pos[2])

    # Equality
    def __eq__(self,other):
        return self.Pos[0]==other.Pos[0] and self.Pos[1]==other.Pos[1] and self.Pos[2]==other.Pos[2]

    # Reading
    @staticmethod
    def read_particle_pos_data_hdf5(fname,PartIndex,Type=1):
        import h5py as h5
        import numpy as np
        from pysbmy import c_float_t
        with h5.File(fname,'r') as hf:
            Pos=hf['PartType'+str(Type)]['Coordinates'][PartIndex].astype(c_float_t)
        args=[np.ctypeslib.as_ctypes(Pos)]
        return particle_pos_data(*args)
#end class(particle_pos_data)

class particle_vel_data(Structure):
    """This class contains the velocity of a particle
    """

    # Initialization, properties and representation
    from pysbmy import c_float_t
    _fields_=[("Vel", c_float_t*3)]

    def __repr__(self):
        return "Vel: [{} {} {}]\n".format(self.Vel[0], self.Vel[1], self.Vel[2])

    # Equality
    def __eq__(self,other):
        return self.Vel[0]==other.Vel[0] and self.Vel[1]==other.Vel[1] and self.Vel[2]==other.Vel[2]

    # Reading
    @staticmethod
    def read_particle_vel_data_hdf5(fname,PartIndex,Type=1):
        import h5py as h5
        import numpy as np
        from pysbmy import c_float_t
        with h5.File(fname,'r') as hf:
            Vel=hf['PartType'+str(Type)]['Velocities'][PartIndex].astype(c_float_t)
        args=[np.ctypeslib.as_ctypes(Vel)]
        return particle_vel_data(*args)
#end class(particle_vel_data)

class particle_id_data(Structure):
    """This class contains the Id of a particle
    """

    # Initialization, properties and representation
    from pysbmy import c_uint
    _fields_=[("Id", c_uint)]

    def __repr__(self):
        return "Id: {}\n".format(int(self.Id))

    # Equality
    def __eq__(self,other):
        return self.Id==other.Id

    # Reading
    @staticmethod
    def read_particle_id_data_hdf5(fname,PartIndex,Type=1):
        import h5py as h5
        import numpy as np
        from pysbmy import c_float_t
        with h5.File(fname,'r') as hf:
            Id=hf['PartType'+str(Type)]['ParticleIDs'][PartIndex]
        args=[np.ctypeslib.as_ctypes(Id)]
        return particle_id_data(*args)
#end class(particle_id_data)

class particle_extras_data(Structure):
    """This class contains additional information (U, Rho, Hsml) for a particle
    """

    # Initialization, properties and representation
    from pysbmy import c_float_t
    _fields_=[    ("U", c_float_t),
            ("Rho", c_float_t),
            ("Hsml", c_float_t)]

    def __repr__(self):
        return "U: {}\nRho: {}\nHsml: {}\n".format(self.U, self.Rho, self.Hsml)

    # Equality
    def __eq__(self,other):
        return self.U==other.U and self.Rho==other.Rho and self.Hsml==other.Hsml

    # Reading
    @staticmethod
    def read_particle_extra_data_hdf5(fname,PartIndex,Type=1):
        import h5py as h5
        import numpy as np
        from pysbmy import c_float_t
        with h5.File(fname,'r') as hf:
            U=hf['PartType'+str(Type)]['U']
            Rho=hf['PartType'+str(Type)]['Rho']
            Hsml=hf['PartType'+str(Type)]['Hsml']
        args=[c_float_t(U),c_float_t(Rho),c_float_t(Hsml)]
        return particle_extras_data(*args)
#end class(particle_extras_data)

class Snapshot(Structure):
    """This is the class to represent a Snapshot in python,
    including a header and particle information
    """

    # Initialization, properties and representation
    from pysbmy import POINTER, HAVE_TYPE_AND_MASS, HAVE_EXTRAS
    _fields_=[("header", header)]
    if HAVE_TYPE_AND_MASS:
        _fields_.append(("types", POINTER(particle_type_data)))
        _fields_.append(("masses", POINTER(particle_mass_data)))
    _fields_.append(("positions", POINTER(particle_pos_data)))
    _fields_.append(("velocities", POINTER(particle_vel_data)))
    _fields_.append(("ids", POINTER(particle_id_data)))
    if HAVE_EXTRAS:
        _fields_.append(("extras", POINTER(particle_extras_data)))

    @property
    def time(self):
        return self.header.time
    @property
    def redshift(self):
        return self.header.redshift
    @property
    def BoxSize(self):
        return self.header.BoxSize
    @property
    def L0(self):
        return self.header.BoxSize
    @property
    def L1(self):
        return self.header.BoxSize
    @property
    def L2(self):
        return self.header.BoxSize
    @property
    def Np(self):
        return int(self.header.npart[0]+self.header.npart[1]+self.header.npart[2]+self.header.npart[3]+self.header.npart[4]+self.header.npart[5])
    @property
    def Np0(self):
        return int(round(pow(self.Np,1./3.)))
    @property
    def Np1(self):
        return int(round(pow(self.Np,1./3.)))
    @property
    def Np2(self):
        return int(round(pow(self.Np,1./3.)))
    @property
    def dp0(self):
        return self.L0/self.Np0
    @property
    def dp1(self):
        return self.L1/self.Np1
    @property
    def dp2(self):
        return self.L2/self.Np2

    def __repr__(self):
        return self.header.__repr__()

    # Equality
    def __eq__(self,other):
        ans = header.__eq__(self.header,other.header)
        if not ans:
            return False
        else:
            for mp in range(self.Np):
                ans = ans and particle_pos_data.__eq__(self.positions[mp],other.positions[mp])
                ans = ans and particle_vel_data.__eq__(self.velocities[mp],other.velocities[mp])
                ans = ans and particle_id_data.__eq__(self.ids[mp],other.ids[mp])
        return ans

    # Writing
    @staticmethod
    def write_snapshot(P,fname,files=1):
        """Writes a Snapshot object in an external file

        Parameters
        ----------
        P (Snapshot) : snapshot to be written
        fname (string) : filename
        files (optional, int, default=1) : number of files per snapshot

        """
        from pysbmy import libSBMY, c_int, c_char, POINTER
        SnapFormat = int(fname[len(fname)-1])
        SnapFile = fname.encode('ascii')
        libSBMY.write_snapshot.argtypes = [Snapshot,POINTER(c_char),c_int,c_int]
        libSBMY.write_snapshot.restype = None
        libSBMY.write_snapshot(P,SnapFile,files,SnapFormat)

    def write(self,fname,files=1):
        """Writes a Snapshot object in an external file

        Parameters
        ----------
        fname (string) : filename
        files (optional, int, default=1) : number of files per snapshot

        """
        self.write_snapshot(self,fname,files)

    # Reading
    @staticmethod
    def read_snapshot(fname):
        """Reads a Snapshot object in an external file

        Parameters
        ----------
        fname (string) : filename

        Returns
        -------
        P (Snapshot)

        """
        from pysbmy import libSBMY, c_int, c_char, POINTER
        SnapFormat = int(fname[len(fname)-1])
        SnapFile = fname.encode('ascii')
        libSBMY.read_snapshot.argtypes = [POINTER(c_char),c_int]
        libSBMY.read_snapshot.restype = Snapshot
        P = libSBMY.read_snapshot(SnapFile, SnapFormat)
        return P

    # Manipulating indices
    def get_Lagrangian_indices(self,mp):
        """Gets indices on the Lagrangian grid from a particle index

        Parameters
        ----------
        mp (int) : particle index

        Returns
        -------
        i (int) : x-position on the particle grid
        j (int) : y-position on the particle grid
        k (int) : z-position on the particle grid

        """
        from pysbmy.utils import get_indices
        Id=self.ids[mp].Id
        return get_indices(Id,self.Np0,self.Np1,self.Np2)

    def get_Lagrangian_Id(self,i,j,k):
        """Gets the Id of particle given its Lagrangian position

        Parameters
        ----------
        i (int) : x-position on the particle grid
        j (int) : y-position on the particle grid
        k (int) : z-position on the particle grid

        Returns
        -------
        Id (int) : particle Id

        """
        from pysbmy.utils import get_index
        return get_index(i,j,k,self.Np1,self.Np2,self.Np0)

    def get_Lagrangian_grid_position(self,mp):
        """Gets the (physical) Lagrangian position from a particle index

        Parameters
        ----------
        mp (int) : particle index

        Returns
        -------
        x (double) : x-position in physical units
        y (double) : y-position in physical units
        z (double) : z-position in physical units

        """
        Id=self.ids[mp].Id
        i,j,k=self.get_Lagrangian_indices(Id)
        return (i*self.dp0,j*self.dp1,k*self.dp2)

    # Computing Lagrangian displacement field
    def get_Psi(self,mp):
        """Gets the Lagrangian displacement field from a particle index

        Parameters
        ----------
        mp (int) : particle index

        Returns
        -------
        PSI (array, double, dimension=3) : Lagrangian displacement field

        """
        import numpy as np
        # Assumes that the initial position corresponds to the convention in snapshot.c
        (LagPos0, LagPos1, LagPos2) = self.get_Lagrangian_grid_position(mp)
        (PSI0, PSI1, PSI2) = (self.positions[mp].Pos[0]-LagPos0, self.positions[mp].Pos[1]-LagPos1, self.positions[mp].Pos[2]-LagPos2)

        # Check if the particle has crossed the periodic boundaries of the box
        PSI0 = np.copysign(self.L0-np.absolute(PSI0),-PSI0) if np.absolute(self.L0-np.absolute(PSI0)) < np.absolute(PSI0) else PSI0
        PSI1 = np.copysign(self.L1-np.absolute(PSI1),-PSI1) if np.absolute(self.L1-np.absolute(PSI1)) < np.absolute(PSI1) else PSI1
        PSI2 = np.copysign(self.L2-np.absolute(PSI2),-PSI2) if np.absolute(self.L2-np.absolute(PSI2)) < np.absolute(PSI2) else PSI2

        return (PSI0, PSI1, PSI2)
#end class(Snapshot)

read_snapshot_header=header.read_header
read_snapshot=Snapshot.read_snapshot
