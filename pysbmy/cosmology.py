#!/usr/bin/env python
# -*- coding: latin-1 -*-
#-------------------------------------------------------------------------------------
# Simbelmynë v0.5.0 -- pysbmy/cosmology.py
# Copyright (C) 2012-2023 Florent Leclercq.
#
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# The text of the license is located in the root directory of the source package.
#-------------------------------------------------------------------------------------

"""Physical and cosmological equations
"""

__author__  = "Florent Leclercq"
__version__ = "0.5.0"
__date__    = "2014-2023"
__license__ = "GPLv3"

N_COSMOPAR=10

def cosmo_array_to_dict(cosmo_array):
    """Converts an array containing cosmological parameters in standard order to a python dictionary

    Parameters
    ----------
    cosmo_array (array) : cosmological parameters in standard order
    (h, Omega_r, Omega_q, Omega_b, Omega_m, Omega_k, n_s, sigma8, w0_fld, wa_fld)

    Returns
    -------
    cosmo (dictionary) : cosmological parameters

    """
    from collections import OrderedDict
    cosmo=OrderedDict()
    cosmo['h']=cosmo_array[0]; cosmo['Omega_r']=cosmo_array[1]; cosmo['Omega_q']=cosmo_array[2]; cosmo['Omega_b']=cosmo_array[3]; cosmo['Omega_m']=cosmo_array[4]; cosmo['Omega_k']=cosmo_array[5]; cosmo['n_s']=cosmo_array[6]; cosmo['sigma8']=cosmo_array[7]; cosmo['w0_fld']=cosmo_array[8]; cosmo['wa_fld']=cosmo_array[9]
    return cosmo

def cosmo_dict_to_array(cosmo):
    """Converts a python dictionary containing cosmological parameters into an array with parameters in the standard order
    (h, Omega_r, Omega_q, Omega_b, Omega_m, Omega_k, n_s, sigma8, w0_fld, wa_fld)

    Parameters
    ----------
    cosmo (dictionary) : cosmological parameters

    Returns
    -------
    cosmo_array (array) : cosmological parameters in standard order

    """
    import numpy as np
    from pysbmy import c_double
    return np.array((cosmo['h'], cosmo['Omega_r'], cosmo['Omega_q'], cosmo['Omega_b'], cosmo['Omega_m'], cosmo['Omega_k'], cosmo['n_s'], cosmo['sigma8'], cosmo['w0_fld'], cosmo['wa_fld']), dtype=c_double)

def WhichSpectrum_from_cosmo_dict(cosmo):
    """Converts cosmo['WhichSpectrum'] into its standard integer value:
    0 : BBKS
    1 : Eisenstein & Hu
    2 : CLASS

    Parameters
    ----------
    cosmo (dictionary) : cosmological parameters

    Returns
    -------
    WhichSpectrum (int) : which spectrum to use

    """
    if cosmo['WhichSpectrum'] in {-1,"pl","PL","power-law","powerlaw"}:
        return -1
    elif cosmo['WhichSpectrum'] in {0,"bbks","BBKS"}:
        return 0
    elif cosmo['WhichSpectrum'] in {1,"eh","EH"}:
        return 1
    elif cosmo['WhichSpectrum'] in {2,"class","Class","CLASS"}:
        return 2
    else:
        from pysbmy.utils import FatalError
        FatalError("WhichSpectrum is invalid.")

def get_CLASS_params(cosmo):
    """Converts a Simbelmyne python dictionary containing cosmological parameters into a classy-readable dictionary

    Parameters
    ----------
    cosmo (dictionary) : cosmological parameters (and some infrastructure parameters)

    Returns
    -------
    params (dictionary) : classy-readable parameters

    """
    from pysbmy.utils import PrintValue
    params={}
    params['h']=cosmo['h']
    params['Omega_b']=cosmo['Omega_b']
    params['Omega_cdm']=cosmo['Omega_m']-cosmo['Omega_b']
    params['Omega_k']=cosmo['Omega_k']
    if cosmo['m_ncdm']>0.:
        params['m_ncdm']=cosmo['m_ncdm']
        params['N_ur']=2.0328   #to get Neff=3.046 in the early Universe with one massive neutrino species
        params['N_ncdm']=1
    else:
        params['N_ur']=3.046
        params['N_ncdm']=0.
    params['tau_reio']=cosmo['tau_reio']
    params['n_s']=cosmo['n_s']
    params['sigma8']=cosmo['sigma8']
    params['ic']='ad'
    params['output']='mPk'
    params['P_k_max_h/Mpc']=cosmo['k_max']/cosmo['h']
    params['z_pk']='0.'
    if (cosmo['w0_fld'],cosmo['wa_fld'])!=(-1.,0.):
        params['Omega_Lambda']=0.
        params['w0_fld']=cosmo['w0_fld']
        params['wa_fld']=cosmo['wa_fld']

    #PrintValue("CLASS parameters", params)

    return params

def hubble(a, cosmo):
    """ The Hubble parameter as a function of the scale factor
    in units of h (normalized so that hubble_of_a(1)=100)

    Parameters
    ----------
    a (array-like) : scale factors
    cosmo (dictionary) : cosmological parameters

    Returns
    -------
    hubble (array-like) : the Hubble factors

    """
    def aux(a, cosmo):
        from pysbmy import libSBMY, c_double, POINTER
        cosmo = cosmo.astype(c_double)
        libSBMY.hubble.argtypes = [c_double, POINTER(c_double)]
        libSBMY.hubble.restype = c_double
        hubble = libSBMY.hubble(a, cosmo.ctypes.data_as(POINTER(c_double)))
        return hubble

    import numpy as np
    hubble_func = np.vectorize(aux)
    hubble_func.excluded.add(1)
    return hubble_func(a, cosmo_dict_to_array(cosmo))

def d_plus(a_start, a_end, cosmo):
    """Computes the cosmological growth factor D_+

    Parameters
    ----------
    a_start (array-like) : starting scale factors
    a_end (array-like) : ending scale factors
    cosmo (dictionary) : cosmological parameters

    Returns
    -------
    d_plus (array-like) : the growth factors

    """
    def aux(a_start, a_end, cosmo):
        from pysbmy import libSBMY, c_double, POINTER
        cosmo = cosmo.astype(c_double)
        libSBMY.d_plus.argtypes = [c_double, c_double, POINTER(c_double)]
        libSBMY.d_plus.restype = c_double
        d_plus = libSBMY.d_plus(a_start, a_end, cosmo.ctypes.data_as(POINTER(c_double)))
        return d_plus

    import numpy as np
    d_plus_func = np.vectorize(aux)
    d_plus_func.excluded.add(2)
    return d_plus_func(a_start, a_end, cosmo_dict_to_array(cosmo))

def d_plus_prime(a_start, a_end, cosmo):
    """Computes the derivative of the growth factor, d(D_+)/da

    Parameters
    ----------
    a_start (array-like) : starting scale factors
    a_end (array-like) : ending scale factors
    cosmo (dictionary) : cosmological parameters

    Returns
    -------
    d_plus_prime (array-like) : the derivatives of the growth factor

    """
    def aux(a_start, a_end, cosmo):
        from pysbmy import libSBMY, c_double, POINTER
        cosmo = cosmo.astype(c_double)
        libSBMY.d_plus_prime.argtypes = [c_double, c_double, POINTER(c_double)]
        libSBMY.d_plus_prime.restype = c_double
        d_plus_prime = libSBMY.d_plus_prime(a_start, a_end, cosmo.ctypes.data_as(POINTER(c_double)))
        return d_plus_prime

    import numpy as np
    d_plus_prime_func = np.vectorize(aux)
    d_plus_prime_func.excluded.add(2)
    return d_plus_prime_func(a_start, a_end, cosmo_dict_to_array(cosmo))

def g_plus(a_start, a_end, cosmo):
    """Computes of the logarithmic derivative of the growth factor, g_plus =dln(d_plus(a))/dln(a) = a/d_plus * dd_plus/da

    Parameters
    ----------
    a_start (array-like) : starting scale factors
    a_end (array-like) : ending scale factors
    cosmo (dictionary) : cosmological parameters

    Returns
    -------
    g_plus (array-like) : the growth factors

    """
    def aux(a_start, a_end, cosmo):
        from pysbmy import libSBMY, c_double, POINTER
        cosmo = cosmo.astype(c_double)
        libSBMY.g_plus.argtypes = [c_double, c_double, POINTER(c_double)]
        libSBMY.g_plus.restype = c_double
        g_plus = libSBMY.g_plus(a_start, a_end, cosmo.ctypes.data_as(POINTER(c_double)))
        return g_plus

    import numpy as np
    g_plus_func = np.vectorize(aux)
    g_plus_func.excluded.add(2)
    return g_plus_func(a_start, a_end, cosmo_dict_to_array(cosmo))

def lpt_prefactors(a, cosmo):
    def aux(a, cosmo):
        from pysbmy import libSBMY, c_double, POINTER
        cosmo = cosmo.astype(c_double)
        libSBMY.lpt_prefactors.argtypes = [c_double, POINTER(c_double)]
        libSBMY.lpt_prefactors.restype = POINTER(c_double*4)
        lpt_prefactors = libSBMY.lpt_prefactors(a, cosmo.ctypes.data_as(POINTER(c_double))).contents
        return lpt_prefactors

    import numpy as np
    return np.fromiter((aux(this_a, cosmo_dict_to_array(cosmo)) for this_a in a), np.dtype((float,4)))

def D_of_a(a, cosmo):
    """"Drift prefactor" (cf appendix B of F. Leclercq PhD Thesis, 2015 - just after equation B.8)

    Parameters
    ----------
    a (array-like) : scale factors
    cosmo (dictionary) : cosmological parameters

    Returns
    -------
    D_of_a (array-like) : the drift prefactors

    """
    def aux(a, cosmo):
        from pysbmy import libSBMY, c_double, POINTER
        cosmo = cosmo.astype(c_double)
        libSBMY.D_of_a.argtypes = [c_double, POINTER(c_double)]
        libSBMY.D_of_a.restype = c_double
        D_of_a = libSBMY.D_of_a(a, cosmo.ctypes.data_as(POINTER(c_double)))
        return D_of_a

    import numpy as np
    D_of_a_func = np.vectorize(aux)
    D_of_a_func.excluded.add(1)
    return D_of_a_func(a, cosmo_dict_to_array(cosmo))

def K_of_a(a, cosmo):
    """"Kick prefactor" (cf appendix B of F. Leclercq PhD Thesis, 2015 - just after equation B.8)

    Parameters
    ----------
    a (array-like) : scale factors
    cosmo (dictionary) : cosmological parameters

    Returns
    -------
    K_of_a (array-like) : the kick prefactors

    """
    def aux(a, cosmo):
        from pysbmy import libSBMY, c_double, POINTER
        cosmo = cosmo.astype(c_double)
        libSBMY.K_of_a.argtypes = [c_double, POINTER(c_double)]
        libSBMY.K_of_a.restype = c_double
        K_of_a = libSBMY.K_of_a(a, cosmo.ctypes.data_as(POINTER(c_double)))
        return K_of_a

    import numpy as np
    K_of_a_func = np.vectorize(aux)
    K_of_a_func.excluded.add(1)
    return K_of_a_func(a, cosmo_dict_to_array(cosmo))

# Astropy cosmolopy wrappers
from pysbmy import module_exists
if module_exists("astropy.cosmology"):
    def astropy_cosmo(cosmo):
        """A wrapper for astropy w0waCDM cosmology

        Parameters
        ----------
        cosmo (dictionary) : cosmological parameters

        Returns
        -------
        w0waCDM : an instance of astropy.cosmology object w0waCDM

        """
        from astropy.cosmology import w0waCDM
        return w0waCDM(H0=cosmo['h']*100., Om0=cosmo['Omega_m'], Ode0=cosmo['Omega_q'], Ob0=cosmo['Omega_b'], w0=cosmo['w0_fld'], wa=cosmo['wa_fld'])

    def efunc(z, **cosmo):
        """A wrapper for astropy function efunc
        Function used to calculate H(z), the Hubble parameter.

        Parameters
        ----------
        z (array-like) : input redshifts
        cosmo (dictionary) : cosmological parameters

        Returns
        -------
        E (ndarray, or float if input scalar) : the redshift scaling of the Hubble constant

        """
        cosmoObj = astropy_cosmo(cosmo)
        return cosmoObj.efunc(z).value

    def hubble_z(z, **cosmo):
        """A wrapper for astropy function H
        Hubble parameter (km/s/Mpc) at redshift ``z``.

        Parameters
        ----------
        z (array-like) : input redshifts
        cosmo (dictionary) : cosmological parameters

        Returns
        -------
        hubble_z (astropy.units.Quantity) : Hubble parameter at each input redshift

        """
        from pysbmy.cosmology import astropy_cosmo
        cosmoObj = astropy_cosmo(cosmo)
        return cosmoObj.H(z).value

    def comoving_distance(z, **cosmo):
        """A wrapper for astropy function comoving_distance
        Comoving line-of-sight distance in Mpc at a given
        redshift.

        The comoving distance along the line-of-sight between two
        objects remains constant with time for objects in the Hubble
        flow.

        Parameters
        ----------
        z (array-like) : input redshifts
        cosmo (dictionary) : cosmological parameters

        Returns
        -------
        d (astropy.units.Quantity) : comoving distance in Mpc to each input redshift

        """
        cosmoObj = astropy_cosmo(cosmo)
        return cosmoObj.comoving_distance(z).value

    def comoving_transverse_distance(z, **cosmo):
        """A wrapper for astropy function comoving_transverse_distance
        Comoving transverse distance in Mpc at a given redshift.

        This value is the transverse comoving distance at redshift ``z``
        corresponding to an angular separation of 1 radian. This is
        the same as the comoving distance if omega_k is zero (as in
        the current concordance lambda CDM model).

        Parameters
        ----------
        z (array-like) : input redshifts
        cosmo (dictionary) : cosmological parameters

        Returns
        -------
        d (astropy.units.Quantity) : comoving transverse distance in Mpc at each input redshift

        """
        cosmoObj = astropy_cosmo(cosmo)
        return cosmoObj.comoving_transverse_distance(z).value

    def angular_diameter_distance(z, **cosmo):
        """A wrapper for astropy function angular_diameter_distance
        Angular diameter distance in Mpc at a given redshift.

        This gives the proper (sometimes called 'physical') transverse
        distance corresponding to an angle of 1 radian for an object
        at redshift ``z``.

        Weinberg, 1972, pp 421-424; Weedman, 1986, pp 65-67; Peebles,
        1993, pp 325-327.

        Parameters
        ----------
        z (array-like) : input redshifts
        cosmo (dictionary) : cosmological parameters

        Returns
        -------
        d (astropy.units.Quantity) : angular diameter distance in Mpc at each input redshift

        """
        cosmoObj = astropy_cosmo(cosmo)
        return cosmoObj.angular_diameter_distance(z).value

    def luminosity_distance(z, **cosmo):
        """A wrapper for astropy function luminosity_distance
        Luminosity distance in Mpc at redshift ``z``.

        This is the distance to use when converting between the
        bolometric flux from an object at redshift ``z`` and its
        bolometric luminosity.

        Parameters
        ----------
        z (array-like) : input redshifts
        cosmo (dictionary) : cosmological parameters

        Returns
        -------
        d (astropy.units.Quantity) : luminosity distance in Mpc at each input redshift

        """
        cosmoObj = astropy_cosmo(cosmo)
        return cosmoObj.luminosity_distance(z).value

    def comoving_volume(z, **cosmo):
        """A wrapper for astropy function comoving_volume
        Comoving volume in cubic Mpc at redshift ``z``.

        This is the volume of the universe encompassed by redshifts less
        than ``z``. For the case of omega_k = 0 it is a sphere of radius
        `comoving_distance` but it is less intuitive
        if omega_k is not 0.

        Parameters
        ----------
        z (array-like) : input redshifts
        cosmo (dictionary) : cosmological parameters

        Returns
        -------
        d (astropy.units.Quantity) : comoving volume in :math:`Mpc^3` at each input redshift

        """
        cosmoObj = astropy_cosmo(cosmo)
        return cosmoObj.comoving_volume(z).value

    def lookback_time(z, **cosmo):
        """A wrapper for astropy function lookback_time
        Lookback time in Gyr to redshift ``z``.

        The lookback time is the difference between the age of the
        Universe now and the age at redshift ``z``.

        Parameters
        ----------
        z (array-like) : input redshifts
        cosmo (dictionary) : cosmological parameters

        Returns
        -------
        d (astropy.units.Quantity) : lookback time in Gyr to each input redshift

        """
        cosmoObj = astropy_cosmo(cosmo)
        return cosmoObj.lookback_time(z).value

    def lookback_distance(z, **cosmo):
        """A wrapper for astropy function lookback_distance
        The lookback distance is the light travel time distance to a given
        redshift. It is simply c * lookback_time.  It may be used to calculate
        the proper distance between two redshifts, e.g. for the mean free path
        to ionizing radiation.

        Parameters
        ----------
        z (array-like) : input redshifts
        cosmo (dictionary) : cosmological parameters

        Returns
        -------
        d (astropy.units.Quantity) : Lookback distance in Mpc

        """
        cosmoObj = astropy_cosmo(cosmo)
        return cosmoObj.lookback_distance(z).value

    def age(z, **cosmo):
        """A wrapper for astropy function age
        Age of the universe in Gyr at redshift ``z``.

        Parameters
        ----------
        z (array-like) : input redshifts
        cosmo (dictionary) : cosmological parameters

        Returns
        -------
        d (astropy.units.Quantity) : the age of the universe in Gyr at each input redshift

        """
        cosmoObj = astropy_cosmo(cosmo)
        return cosmoObj.age(z).value

    def aux_redshift_from_comoving_distance(dcom, **cosmo):
        """Redshift from comoving distance

        Parameters
        ----------
        dcom (array-like) : input comoving distances
        cosmo (dictionary) : cosmological parameters

        Returns
        -------
        z (array-like) : redshifts

        """
        from scipy.optimize import minimize_scalar
        def f(z):
            return abs(dcom - comoving_distance(z, **cosmo))
        res = minimize_scalar(f)
        return res.x

    from numpy import vectorize
    redshift_from_comoving_distance = vectorize(aux_redshift_from_comoving_distance)
