#-------------------------------------------------------------------------------------
# Simbelmynë v0.5.0 -- config.mk
# Copyright (C) 2012-2023 Florent Leclercq.
#
# This file is part of the Simbelmynë distribution
# (https://bitbucket.org/florent-leclercq/simbelmyne/)
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, version 3.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# The text of the license is located in the root directory of the source package.
#-------------------------------------------------------------------------------------
# \file config.mk
# \brief Configuration for Simbelmynë. This should be adjusted by the user.
# \authors Florent Leclercq
# \version 0.5.0
# \date 2012-2023

#--------------------------------------- Select target computer and optimization mode

# SYSTYPE="infinityDebug"
# SYSTYPE="infinityProd"
SYSTYPE="absolutionDebug"
# SYSTYPE="absolutionProd"
# SYSTYPE="blackthornDebug"
# SYSTYPE="blackthornProd"

# DEBUGMEM = "true"
DEBUGMEM = "false"

#--------------------------------------- Adjust settings for target computer

ifeq ($(SYSTYPE),"infinityDebug")
        CC              = gcc
        RM              = rm -rf
        ECHO            = echo
        MKDIR           = @mkdir -p
        LDFLAGS         = -L $(HOME)/.conda/envs/simbelmyne/lib
        LDLIBS          = -lm -lgsl -lgslcblas -lfftw3 -lfftw3_threads -lfftw3f -lfftw3f_threads -lhdf5
        INCLUDES        = -I $(HOME)/.conda/envs/simbelmyne/include
        PIP             = pip
        PYTHONFLAGS     = -fPIC -shared
        DEBUG           = "true"
endif

ifeq ($(SYSTYPE),"infinityProd")
	CC              = gcc
	RM              = rm -rf
	ECHO            = echo
	MKDIR           = @mkdir -p
	LDFLAGS         = -L $(HOME)/.conda/envs/simbelmyne/lib
	LDLIBS          = -lm -lgsl -lgslcblas -lfftw3 -lfftw3_threads -lfftw3f -lfftw3f_threads -lhdf5
	INCLUDES        = -I $(HOME)/.conda/envs/simbelmyne/include
	PIP             = pip
	PYTHONFLAGS     = -fPIC -shared
	DEBUG           = "false"
endif

ifeq ($(SYSTYPE),"absolutionDebug")
	CC		= gcc
	RM		= rm -rf
	ECHO		= echo
	MKDIR		= @mkdir -p
	LDFLAGS		= -L $(HOME)/.conda/envs/simbelmyne/lib
	LDLIBS		= -lm -lgsl -lgslcblas -lfftw3 -lfftw3_threads -lfftw3f -lfftw3f_threads -lhdf5
	INCLUDES	= -I $(HOME)/.conda/envs/simbelmyne/include
	PIP		= pip
	PYTHONFLAGS	= -fPIC -shared
	DEBUG		= "true"
endif

ifeq ($(SYSTYPE),"absolutionProd")
	CC		= gcc
	RM		= rm -rf
	ECHO		= echo
	MKDIR		= @mkdir -p
	LDFLAGS		= -L $(HOME)/.conda/envs/simbelmyne/lib
	LDLIBS		= -lm -lgsl -lgslcblas -lfftw3 -lfftw3_threads -lfftw3f -lfftw3f_threads -lhdf5
	INCLUDES	= -I $(HOME)/.conda/envs/simbelmyne/include
	PIP		= pip
	PYTHONFLAGS	= -fPIC -shared
	DEBUG		= "false"
endif

ifeq ($(SYSTYPE),"blackthornDebug")
	CC		= gcc
	RM		= rm -rf
	ECHO		= echo
	MKDIR		= @mkdir -p
	LDFLAGS		= -L $(HOME)/.conda/envs/simbelmyne/lib
	LDLIBS		= -lm -lgsl -lgslcblas -lfftw3 -lfftw3_threads -lfftw3f -lfftw3f_threads -lhdf5
	INCLUDES	= -I $(HOME)/.conda/envs/simbelmyne/include
	PIP		= pip
	PYTHONFLAGS	= -fPIC -shared
	DEBUG		= "true"
endif

ifeq ($(SYSTYPE),"blackthornProd")
        CC              = gcc
        RM              = rm -rf
        ECHO            = echo
        MKDIR           = @mkdir -p
        LDFLAGS         = -L $(HOME)/.conda/envs/simbelmyne/lib
        LDLIBS          = -lm -lgsl -lgslcblas -lfftw3 -lfftw3_threads -lfftw3f -lfftw3f_threads -lhdf5
        INCLUDES        = -I $(HOME)/.conda/envs/simbelmyne/include
        PIP             = pip
        PYTHONFLAGS     = -fPIC -shared
        DEBUG           = "false"
endif

ifeq ($(DEBUG),"true")
	OPTIMIZE	= -fopenmp -g3 -ggdb -W -Wall -pedantic -pedantic-errors -std=c99
	OPT		+= -DDEBUG
ifeq ($(DEBUGMEM),"true")
	OPT		+= -DSCREEN_VERBOSE_LEVEL=7 -DLOGS_VERBOSE_LEVEL=7
else
	OPT             += -DSCREEN_VERBOSE_LEVEL=6 -DLOGS_VERBOSE_LEVEL=6
endif
else
	OPTIMIZE	= -fopenmp -O3 -std=c99
	OPT		+= -DSCREEN_VERBOSE_LEVEL=4 -DLOGS_VERBOSE_LEVEL=6
endif

#--------------------------------------- Options

# OPT				+= -DRANDOM_PARALLEL
# Allow a parallel generation of random numbers, using a seed table.
# Switch this off if you want to be sure to get good random numbers

# OPT				+= -DLONGIDS
# Switch this on if you want 64-bits IDs

# OPT				+= -DDOUBLEPRECISION
# Double precision calculations

# OPT				+= -DDOUBLEPRECISION_FFTW
# Double precision Fourier transforms

# OPT				+= -DHAVE_TYPE_AND_MASS
# Switch this on to store type and mass in particle data

# OPT				+= -DHAVE_EXTRAS
# Switch this on to store extras in particle data

# OPT				+= -DONLY_ZA
# Swith this on if you want ZA initial conditions (2LPT otherwise)

# OPT				+= -DNOGRAVITY
# Switch this on if you want to disable forces in PM/tCOLA/sCOLA

OPT				+= -DTIMERS
# Print timers to screen and logs

OPT				+= -DASSIGNMENT=2
# Mass assignement scheme to be used (1=NGP, 2=CiC, 3=TSC)

OPT				+= -DFDA=2
# Finite difference approximation order for forces (0, 2, 4 or 6)

OPT				+= -DASMTH=1.25
# Define the smoothing scale for short-range forces in units of FFT-mesh cells, default 1.25

#--------------------------------------- Extensions

# Add below the list of extensions to be loaded, placed in extensions/
EXTENSIONS			= legacy plots
